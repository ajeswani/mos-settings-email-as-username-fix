/*
 * Copyright (c) 2013 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service;

import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

public interface SAMLService {

    /**
     * Interface method to create SAML authentication request.
     *
     * @param assertion customer service URL acsURL
     * @return SAML authentication request
     * @throws MxOSException
     */
    String createSAMLAuthenticationRequest(String acsURL)
            throws MxOSException;

    /**
     * Interface method to decode SAML assertion.
     *
     * @param assertion SAML assertion
     * @return a map of meta attributes from the SAML assertion based on configuration
     *
     * @throws MxOSException in case any exceptions.
     */
    Map<String, String> decodeSAMLAssertion(String assertion) throws MxOSException;

}
