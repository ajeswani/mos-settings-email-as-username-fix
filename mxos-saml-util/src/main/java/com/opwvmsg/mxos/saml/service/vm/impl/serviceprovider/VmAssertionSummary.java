/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider;

import java.util.Map;

/**
 * The assertion summary is a bean that provides
 * information about the assertion.
 * 
 * @author mxos-dev
 *
 */
public class VmAssertionSummary {
	
	private String username;
	private String domain;
	private String relayState;
	private Map<String, String> attributeMap;
	
	/**
	 * Constructor.
	 */
	public VmAssertionSummary() {
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getRelayState() {
		return relayState;
	}

	public void setRelayState(String relayState) {
		this.relayState = relayState;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setAttributeMap(Map<String, String> attributeMap) {
		this.attributeMap = attributeMap;
	}
	
	public final Map<String, String> getAttributeMap() {
		return attributeMap;
	}

}
