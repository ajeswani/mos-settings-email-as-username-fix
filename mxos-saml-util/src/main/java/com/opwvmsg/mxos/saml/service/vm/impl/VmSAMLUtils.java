/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.DOMBuilder;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.xml.sax.SAXException;

import com.opwvmsg.mxos.saml.exception.SAMLException;

/**
 * SAML Utilities.
 * 
 * @author mxos-dev
 * 
 */
public class VmSAMLUtils {

    private static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static Random random = new Random();
    private static final char[] charMapping = { 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p' };

    /**
     * Converts a java Date object into a SAML-compliant date string.
     * 
     * @param date
     * @return
     */
    public static String toSAMLDate(Date date) {

        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

        String formattedDate = dateFormat.format(date);

        return formattedDate;

    }

    /**
     * Returns a Java date from a date string.
     * 
     * @param dateString
     * @return
     * @throws SAMLException
     */
    public static Date toSAMLDate(String dateString) throws SAMLException {

        Date date;

        try {
            DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            date = dateFormat.parse(dateString);

        } catch (ParseException ex) {
            throw new SAMLException(ex.getMessage());
        }

        return date;
    }

    /**
     * Utility method for building a JDOM xml document.
     * 
     * @param xmlString
     * @return
     * @throws SAMLException
     */
    public static Document createJdomDoc(String xmlString) throws SAMLException {

        try {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(new ByteArrayInputStream(xmlString
                    .getBytes()));
            return doc;
        } catch (IOException e) {
            throw new SAMLException(
                    "Error creating JDOM document from XML string: "
                            + e.getMessage());
        } catch (JDOMException e) {
            throw new SAMLException(
                    "Error creating JDOM document from XML string: "
                            + e.getMessage());
        }

    }

    /**
     * Converts a JDOM Element to a W3 DOM Element
     * 
     * @param element JDOM Element
     * @return W3 DOM Element if converted successfully, null otherwise
     */
    public static org.w3c.dom.Element toDom(org.jdom.Element element)
            throws SAMLException {
        return toDom(element.getDocument()).getDocumentElement();
    }

    /**
     * Converts a JDOM Document to a W3 DOM document.
     * 
     * @param doc JDOM Document
     * @return W3 DOM Document if converted successfully, null otherwise
     */
    public static org.w3c.dom.Document toDom(org.jdom.Document doc)
            throws SAMLException {

        try {
            XMLOutputter xmlOutputter = new XMLOutputter();
            StringWriter elemStrWriter = new StringWriter();
            xmlOutputter.output(doc, elemStrWriter);
            byte[] xmlBytes = elemStrWriter.toString().getBytes();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);

            return dbf.newDocumentBuilder().parse(
                    new ByteArrayInputStream(xmlBytes));

        } catch (IOException e) {
            throw new SAMLException(
                    "Error converting JDOM document to W3 DOM document: "
                            + e.getMessage());
        } catch (ParserConfigurationException e) {
            throw new SAMLException(
                    "Error converting JDOM document to W3 DOM document: "
                            + e.getMessage());
        } catch (SAXException e) {
            throw new SAMLException(
                    "Error converting JDOM document to W3 DOM document: "
                            + e.getMessage());
        }

    }

    /**
     * Converts a W3 DOM Element to a JDOM Element
     * 
     * @param e W3 DOM Element
     * @return JDOM Element
     */
    public static org.jdom.Element toJdom(org.w3c.dom.Element e) {
        DOMBuilder builder = new DOMBuilder();
        org.jdom.Element jdomElem = builder.build(e);
        return jdomElem;
    }

    /**
     * Returns a String containing the contents of the file located at the
     * specified path.
     * 
     * @param path location of file to be read
     * @return String containing contents of file, null if error reading file
     * @throws IOException
     */
    public static String readFileContents(String filepath) throws SAMLException {

        StringBuffer contents = new StringBuffer();
        BufferedReader input = null;

        try {
            input = new BufferedReader(new FileReader(filepath));
            String line = null;
            while ((line = input.readLine()) != null) {
                contents.append(line);
            }
            input.close();
            return contents.toString();
        } catch (FileNotFoundException e) {
            throw new SAMLException("File not found: " + filepath);
        } catch (IOException e) {
            throw new SAMLException("Error reading file: " + filepath);
        }

    }

    /**
     * Create a randomly generated string conforming to the xsd:ID datatype.
     * containing 160 bits of non-cryptographically strong pseudo-randomness, as
     * suggested by SAML 2.0 core 1.2.3. This will also apply to version 1.1
     * 
     * @return the randomly generated string
     */
    public static String createID() {

        byte[] bytes = new byte[20]; // 160 bits
        random.nextBytes(bytes);

        char[] chars = new char[40];

        for (int i = 0; i < bytes.length; i++) {
            int left = (bytes[i] >> 4) & 0x0f;
            int right = bytes[i] & 0x0f;
            chars[i * 2] = charMapping[left];
            chars[i * 2 + 1] = charMapping[right];
        }

        return String.valueOf(chars);
    }

    /**
     * Creates a PublicKey from the specified public key file and algorithm.
     * Returns null if failure to generate PublicKey.
     * 
     * @param publicKeyFilepath location of public key file
     * @param algorithm algorithm of specified key file
     * @return PublicKey object representing contents of specified public key
     *         file, null if error in generating key or invalid file specified
     */
    public static PublicKey getPublicKey(String publicKeyFilepath,
            String algorithm) throws SAMLException {

        try {
            InputStream pubKey = null;

            try {
                URL url = new URL(publicKeyFilepath);
                pubKey = url.openStream();
            } catch (MalformedURLException e) {
                pubKey = new FileInputStream(publicKeyFilepath);
            }

            byte[] bytes = new byte[pubKey.available()];
            pubKey.read(bytes);
            pubKey.close();
            X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(algorithm);

            return factory.generatePublic(pubSpec);
        } catch (FileNotFoundException e) {
            throw new SAMLException("ERROR: Public key file not found - "
                    + publicKeyFilepath);
        } catch (IOException e) {
            throw new SAMLException("ERROR: Invalid public key file - "
                    + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            throw new SAMLException("ERROR: Invalid public key algorithm - "
                    + e.getMessage());
        } catch (InvalidKeySpecException e) {
            throw new SAMLException("ERROR: Invalid public key spec - "
                    + e.getMessage());
        }
    }

    /**
     * Creates a PrivateKey from the specified public key file and algorithm.
     * Returns null if failure to generate PrivateKey.
     * 
     * @param PrivateKeyFilepath location of public key file
     * @param algorithm algorithm of specified key file
     * @return PrivateKey object representing contents of specified private key
     *         file, null if error in generating key or invalid file specified
     */
    public static PrivateKey getPrivateKey(String privateKeyFilepath,
            String algorithm) throws SAMLException {

        try {
            InputStream privKey = null;

            try {
                URL url = new URL(privateKeyFilepath);
                privKey = url.openStream();
            } catch (MalformedURLException e) {
                privKey = new FileInputStream(privateKeyFilepath);
            }

            byte[] bytes = new byte[privKey.available()];
            privKey.read(bytes);
            privKey.close();
            PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(algorithm);

            return factory.generatePrivate(privSpec);

        } catch (FileNotFoundException e) {
            throw new SAMLException("ERROR: Private key file not found - "
                    + privateKeyFilepath);
        } catch (IOException e) {
            throw new SAMLException("ERROR: Invalid private key file - "
                    + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            throw new SAMLException("ERROR: Invalid private key algorithm - "
                    + e.getMessage());
        } catch (InvalidKeySpecException e) {
            throw new SAMLException("ERROR: Invalid private key spec - "
                    + e.getMessage());
        }
    }

    /**
     * Encodes the given string.
     * 
     * @param message
     */
    public static String getEncodedMessage(String message) {
        String urlEncodedMessage = null;
        try {
            byte[] xmlBytes = message.getBytes("UTF-8");
            ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
            DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(
                    byteOutputStream);
            deflaterOutputStream.write(xmlBytes, 0, xmlBytes.length);
            deflaterOutputStream.close();

            // Base64 encode the deflated string
            Base64 base64Encoder = new Base64();
            byte[] base64EncodedByteArray = base64Encoder
                    .encode(byteOutputStream.toByteArray());
            String base64EncodedMessage = new String(base64EncodedByteArray);

            // URL-encode the deflated, Base64-encoded string
            urlEncodedMessage = URLEncoder
                    .encode(base64EncodedMessage, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            // this should never happen as UTF-8 will always be supported
            System.out
                    .println("The URL encoder was unable to handle the UTF-8 encoding format");
            ex.printStackTrace();
            throw new RuntimeException(ex);
        } catch (IOException ex) {
            System.out
                    .println("Unable to deflate the SAML authentication request");
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return urlEncodedMessage;
    }

    /**
     * Decodes the given string.
     * 
     * @param message
     */
    public static String getDecodedMessage(String message) {
        String urlDecodedMessage = null;
        ByteArrayInputStream byteInputStream = null;
        InflaterInputStream inflaterInputStream = null;
        try {
            urlDecodedMessage = URLDecoder.decode(message, "UTF-8");
            byte[] samlBytes = Base64.decodeBase64(urlDecodedMessage
                    .getBytes("UTF-8"));

            byteInputStream = new ByteArrayInputStream(samlBytes);
            inflaterInputStream = new InflaterInputStream(byteInputStream);

            byte[] defaltedBytes = new byte[0];
            byte[] buff = new byte[1024];
            int k = -1;
            while ((k = inflaterInputStream.read(buff, 0, buff.length)) > -1) {
                // temp buffer size = bytes already read + bytes last read
                byte[] tempBuff = new byte[defaltedBytes.length + k];
                // copy previous bytes
                System.arraycopy(defaltedBytes, 0, tempBuff, 0,
                        defaltedBytes.length);
                // copy current lot
                System.arraycopy(buff, 0, tempBuff, defaltedBytes.length, k);
                // call the temp buffer as your result buff
                defaltedBytes = tempBuff;
            }
            urlDecodedMessage = new String(defaltedBytes);
            inflaterInputStream.close();
            byteInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (inflaterInputStream != null) {
                    inflaterInputStream.close();
                }
                if (byteInputStream != null) {
                    byteInputStream.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return urlDecodedMessage;
    }
    
    /**
    *
    * @param attributeList
    * @param attributeMap
    * @return
    */
   public static String getNonEncryptedQueryString(
            final Map<String, String> attributeMap) {
        StringBuffer buffer = new StringBuffer("?");
        for (String key : attributeMap.keySet()) {
            String value = attributeMap.get(key);
            buffer.append(key).append("=").append(value).append("&");
        }
        return buffer.toString();
    }
    /**
     *
     * @param attributeList
     * @param attributeMap
     * @return
     */
    public static String getBase64CompressedQueryString(
            final Map<String, String> attributeMap) {
        StringBuffer buffer = new StringBuffer("?");
        for (String key : attributeMap.keySet()) {
            String value = attributeMap.get(key);
            value = VmSAMLUtils.getEncodedMessage(value);
            buffer.append(key).append("=").append(value).append("&");
        }
        return buffer.toString();
    }

    /**
     *
     * @param attributeMap
     * @return
     */
    public static String getPublicKeyEncryptedQueryString(
            final Map<String, String> attributeMap) {
        return null;
    }

}
