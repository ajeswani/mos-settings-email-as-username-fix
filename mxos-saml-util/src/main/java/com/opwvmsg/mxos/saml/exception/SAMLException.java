/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.exception;

/**
 * Exception class for handling SAML exceptions.
 * 
 * @author mxos-dev
 */
public class SAMLException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 2966712712349800512L;
    protected String code = "";
    protected String message = "";

    /**
     * Default constructor.
     * 
     */
    public SAMLException() {
    }

    /**
     * Constructor.
     * 
     * @param e
     */
    public SAMLException(Throwable e) {
        super(e);
    }

    /**
     * Constructor.
     * 
     * @param message
     */
    public SAMLException(String message) {
        this.message = message;
    }
    
    /**
     * Default constructor with ErrorCode and Message.
     *
     * @param code - error code
     * @param message - error message
     */
    public SAMLException(final String code, final String message) {
        super(message);
        this.code = code;
        this.message = message;
    }
    
    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public String toString() {
        return "SAML exception: " + message;
    }

}