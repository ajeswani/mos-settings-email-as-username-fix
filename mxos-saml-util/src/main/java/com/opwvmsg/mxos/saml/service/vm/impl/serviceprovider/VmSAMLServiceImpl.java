/*
 * Copyright (c) 2013 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.SamlError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.saml.exception.SAMLException;
import com.opwvmsg.mxos.saml.service.SAMLService;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLFactory;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLUtils;
import com.opwvmsg.mxos.saml.service.SamlProperty;

public class VmSAMLServiceImpl implements SAMLService {
    public static final String BASE64_ENC = "base64Compressed";
    public static final String MXOS_HOME = "MXOS_HOME";
    public static final String COMMA = ",";

    private static Logger logger = Logger.getLogger(VmSAMLServiceImpl.class);

    @Override
    public String createSAMLAuthenticationRequest(String acsURL)
            throws MxOSException {
        String authRequest = null;
        try {

            if (logger.isDebugEnabled()) {
                logger.debug("acsURL is : " + acsURL);
            }
            String providerName = System
                    .getProperty(SamlProperty.samlAuthenticationProvider.name());
            String samlIssuer = System.getProperty(SamlProperty.samlAuthenticationIssuer
                    .name());
            authRequest = VmSAMLFactory.createAuthenticationRequest(
                    providerName, acsURL, samlIssuer);
        } catch (SAMLException e) {
            logger.error(e);
            throw new MxOSException(
                    SamlError.SAML_UNABLE_TO_CREATE_AUTHENTICATION_REQUEST
                            .name(),
                    e.getMessage());
        }  catch (Exception e) {
            logger.error(e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e.getMessage());
        }
        String encryption = System
                .getProperty(SamlProperty.samlRequestBase64Compressed.name());
        if (Boolean.parseBoolean(encryption)) {
            authRequest = VmSAMLUtils.getEncodedMessage(authRequest);
        }
        return authRequest;
    }

    @Override
    public Map<String, String> decodeSAMLAssertion(String samlAssertion)
            throws MxOSException {
        Map<String, String> queryAttributeMap = null;
        try {
            String publicKeyPath = System.getProperty(MXOS_HOME) + "/config/"
                    + System.getProperty(SamlProperty.samlPublicKey.name());
            VmAssertionConsumerService acs = new VmAssertionConsumerService(
                    publicKeyPath);
            VmAssertionSummary assertionSummary = acs
                    .consumeSAMLResponse(samlAssertion);

            String requiredAttrs = System
                    .getProperty(SamlProperty.samlAttributes.name());
            List<String> configuredAttributeList = new ArrayList<String>();
            if (null != requiredAttrs) {
                String[] samlAttributes = requiredAttrs.split(COMMA);
                if (samlAttributes.length > 0) {
                    for (int i = 0; i < samlAttributes.length; i++) {
                        configuredAttributeList.add(samlAttributes[i]);
                    }
                }
            }

            Map<String, String> assersionAttributeMap = assertionSummary
                    .getAttributeMap();
            queryAttributeMap = new HashMap<String, String>();
            for (String attribute : configuredAttributeList) {
                if (assersionAttributeMap.containsKey(attribute)) {
                    queryAttributeMap.put(attribute,
                            assersionAttributeMap.get(attribute));
                }
            }
        } catch (SAMLException e) {
            logger.error(e);
            throw new MxOSException(e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e.getMessage());
        }
        return queryAttributeMap;
    }

}
