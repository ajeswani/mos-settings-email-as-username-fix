/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl;

import java.io.StringWriter;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeQuery;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.saml2.core.AuthnContext;
import org.opensaml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.xml.Namespace;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.schema.impl.XSStringBuilder;
import org.w3c.dom.Element;

import com.opwvmsg.mxos.saml.exception.SAMLException;

/**
 * Builds SAML responses (e.g. authentication response).
 * 
 * @author mxos-dev
 *
 */
public class VmSAMLFactory extends VmSAMLObject {
	
	private static String responseTemplate;
	private static XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
	
	private static Logger logger = LoggerFactory.getLogger(VmSAMLFactory.class);
			
	/**
	 * Do not use this method. Use the overloaded method of the same name. 
     * This method is a legacy method that builds SAML responses from a template
     * (based on the published code from the Google reference implementation).
     * The overloaded method does not require templates. Instead it uses the SAML API
     * directly through OpenSAML.
	 * 
	 * @param username
	 * @param authnInstant
	 * @return
	 * @deprecated
	 */
	public static String createSamlResponse(String username, String acsUrl, Date authnInstant, 
			String templatePath) 
	throws SAMLException {
						
		Date now = new Date();
		String dateNowString = VmSAMLUtils.toSAMLDate(now);		
		String authnInstantString = VmSAMLUtils.toSAMLDate(authnInstant);
		
		Date notOnOrAfter = buildNotOnOrAfter();		
		String notOnOrAfterString = VmSAMLUtils.toSAMLDate(notOnOrAfter);
		Date notBefore = buildNotBefore();
		String notBeforeString = VmSAMLUtils.toSAMLDate(notBefore);
				
		try {
			if(responseTemplate == null) {
				responseTemplate = VmSAMLUtils.readFileContents(templatePath);				
			}						
		} catch(SAMLException ex) {
			logger.error(ex.getMessage());
			throw ex;
		}		
		// hard-coded date: assertion issue instant
		String samlResponse = responseTemplate;
		samlResponse = samlResponse.replace("<USERNAME_STRING>", username);
		samlResponse = samlResponse.replace("<RESPONSE_ID>", VmSAMLUtils.createID());
		samlResponse = samlResponse.replace("<ISSUE_INSTANT>", dateNowString);
		samlResponse = samlResponse.replace("<AUTHN_INSTANT>", dateNowString);		
		samlResponse = samlResponse.replace("<NOT_BEFORE>", notBeforeString);
		samlResponse = samlResponse.replace("<NOT_ON_OR_AFTER>", notOnOrAfterString);		
		samlResponse = samlResponse.replace("<ASSERTION_ID>", VmSAMLUtils.createID());
		
		// ACS URL is the one specified in the authentication request and passed into this method
		samlResponse = samlResponse.replace("<ACS_URL>", acsUrl); 
				
		if (logger.isDebugEnabled()) {
		    logger.debug("Outputting the unsigned SAML response:");
		    logger.debug(samlResponse);
		}
		
		return samlResponse;
		
	}
			
	/**
	 * Digitally signs the SAML message.
	 * 
	 * @param saml
	 * @return
	 */
	public static String signSaml(String saml, String publicKeyPath, String privateKeyPath) {
		
		String signedSamlResponse = "";		
		
		try {
			DSAPublicKey publicKey = (DSAPublicKey)VmSAMLUtils.getPublicKey(publicKeyPath, "DSA");
			DSAPrivateKey privateKey = (DSAPrivateKey)VmSAMLUtils.getPrivateKey(privateKeyPath, "DSA");
			signedSamlResponse = VmXmlDigitalSigner.signXML(saml, publicKey, privateKey);
						
		} catch(SAMLException ex) {
			
			logger.error("Unable to sign the SAML response: {}", ex.getMessage());			
		}
				
		return signedSamlResponse;		
	}
	
	/**
	 * Digitally signs the SAML message.
	 * 
	 * @param saml
	 * @param publicKey
	 * @param privateKey
	 * @return
	 */
	public static String signSaml(String saml, PublicKey publicKey, PrivateKey privateKey) {
		
		String signedSamlResponse = "";		
		
		try {
			DSAPublicKey dsaPublicKey = (DSAPublicKey)publicKey;
			DSAPrivateKey dsaPrivateKey = (DSAPrivateKey)privateKey;
			signedSamlResponse = VmXmlDigitalSigner.signXML(saml, dsaPublicKey, dsaPrivateKey);
						
		} catch(SAMLException ex) {
			
			logger.error("Unable to sign the SAML response: {}", ex.getMessage());			
		}
				
		return signedSamlResponse;
		
	}
	
	/**
	 * Creates a SAML authentication request using the OpenSAML toolkit.
	 * 
	 * @param username
	 * @param authnInstant
	 * @return
	 */
	public static String createAuthenticationRequest(
			String providerName, 
			String acsUrl,
			String samlIssuer) throws SAMLException {
		
		return createAuthenticationRequest(providerName, acsUrl, samlIssuer, null);		
	}
	
	
	
	/**
	 * Creates a SAML authentication request using the OpenSAML toolkit.
	 * 
	 * @param username
	 * @param authnInstant
	 * @return
	 */
	public static String createAuthenticationRequest(
			String providerName, 
			String acsUrl,
			String samlIssuer,
			String subject) throws SAMLException {

        StringWriter stw = new StringWriter();
	
		logger.trace("Initialising OpenSAML AuthnRequest builder");
				
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(AuthnRequest.DEFAULT_ELEMENT_NAME);
		
		AuthnRequest authnRequest = (AuthnRequest)builder.buildObject();
					
		// parameters tested successfully in Google 
		authnRequest.setAssertionConsumerServiceURL(acsUrl);		
		authnRequest.setProviderName(providerName);
		authnRequest.setProtocolBinding(VmSAMLConstants.Namespaces.REDIRECT_PROTOCOL_BINDING);
		authnRequest.setVersion(SAMLVersion.VERSION_20);
		authnRequest.setIssueInstant(new DateTime());
		authnRequest.setID(VmSAMLUtils.createID());
		authnRequest.setIsPassive(false);
		authnRequest.setIssuer(buildIssuer(samlIssuer));
		
		if(subject != null) {
			authnRequest.setSubject(buildSubject(subject));			
		}		
						
		MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
		Marshaller marshaller = marshallerFactory.getMarshaller(authnRequest);
		
		try {
			Element authnRequestElement = marshaller.marshall(authnRequest);
	        Transformer serializer = TransformerFactory.newInstance().newTransformer();
	        serializer.transform(new DOMSource(authnRequestElement), new StreamResult(stw));
	        
	        if (logger.isDebugEnabled()) {
	            logger.debug("Outputting the unsigned XML authentication request:");
	            logger.debug(stw.toString());
	        }
	        			
		} catch(MarshallingException ex) {
			
			logger.error("The OpenSAML toolkit was unable to marshall the authentication request into XML");
			throw new SAMLException(ex);
			
		} catch(TransformerConfigurationException ex) {
			
			logger.error("The system was unable to configure the XML transformer for the authentication request");
			throw new SAMLException(ex);			
		
		} catch(TransformerException ex) {
			logger.error("The system was unable to serialize the authnRequest");
			throw new SAMLException(ex);	
		}
		
		return stw.toString();
			
	}

	/**
	 * Creates a SAML attribute query using the OpenSAML toolkit.
	 * 
	 * @param email
	 * @param attributes
	 * @param acsURL
	 * @param samlIssuer
	 * @return
	 */
	public static String createAttributeQuery(String email, List<Attribute> attributes, String samlIssuer) 
	throws SAMLException {
		
		StringWriter stw = new StringWriter();
		
		logger.trace("Initialising OpenSAML AttributeQuery builder");
								
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(AttributeQuery.DEFAULT_ELEMENT_NAME);
		
		AttributeQuery attrQuery = (AttributeQuery)builder.buildObject();
		
		attrQuery.setVersion(SAMLVersion.VERSION_20);
		attrQuery.setIssueInstant(new DateTime());
		attrQuery.setIssuer(buildIssuer(samlIssuer));
		attrQuery.setSubject(buildSubject(email));		
		attrQuery.getAttributes().addAll(attributes);	
		
		MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
		Marshaller marshaller = marshallerFactory.getMarshaller(attrQuery);
		
		try {
			Element authnRequestElement = marshaller.marshall(attrQuery);
	        Transformer serializer = TransformerFactory.newInstance().newTransformer();
	        serializer.transform(new DOMSource(authnRequestElement), new StreamResult(stw));
	        
	        if (logger.isDebugEnabled()) {
                logger.debug("Outputting the unsigned XML attribute query:");
                logger.debug(stw.toString());
            }
	        			
		} catch(MarshallingException ex) {
			
			logger.error("The OpenSAML toolkit was unable to marshall the attribute query into XML");
			throw new SAMLException(ex);
			
		} catch(TransformerConfigurationException ex) {
			
			logger.error("The system was unable to configure the XML transformer for the attribute query");
			throw new SAMLException(ex);			
		
		} catch(TransformerException ex) {
			logger.error("The system was unable to serialize the attribute query");
			throw new SAMLException(ex);	
		}
		
		return stw.toString();
		
	}
	
	
	/**
	 * Builds a SAML response using the OpenSAML API.
	 * 
	 * @param username
	 * @param acsUrl
	 * @param authnInstant
	 * @return
	 * @throws SAMLException
	 */			
	public static String createSamlResponse(String username, String recipientURL, 
			List<Attribute> attributes, String samlResponseStatus, int validBefore, int validAfter, boolean buildAttributeStatement) throws SAMLException {
		
		logger.trace("Initialising OpenSAML Response builder");
		
		StringWriter stw = new StringWriter();				
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Response.DEFAULT_ELEMENT_NAME);		
		Response response = (Response)builder.buildObject();
		
		response.addNamespace(new Namespace("urn:oasis:names:tc:SAML:2.0:assertion", null));
		response.addNamespace(new Namespace("http://www.w3.org/2001/04/xmlenc#", "xenc"));
		
		DateTime now = new DateTime();
		DateTime issueInstant = now.minusHours(2);
			
		// parameters tested successfully in Google		
		response.setIssueInstant(issueInstant);
		response.setID(VmSAMLUtils.createID());
		response.setVersion(SAMLVersion.VERSION_20);	
		response.setStatus(buildStatus(samlResponseStatus));
		Assertion assertion;
		
		if(attributes.isEmpty() || !buildAttributeStatement) {
			logger.debug("Building assertion without attribute statement (none requested)");
			assertion = buildAssertion(username, recipientURL, validBefore, validAfter);
						
		} else {
			logger.debug("Building assertion with attribute statement");
			assertion = buildAssertion(username, recipientURL, buildAttributeStatement(attributes), validBefore, validAfter);
		}
		
		response.getAssertions().add(assertion);
				
		// parameters untested in Google SSO
		//authnRequest.setDestination("http://www.the.destination");
		//authnRequest.setAssertionConsumerServiceIndex(new Integer(1));
				
		MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
		Marshaller marshaller = marshallerFactory.getMarshaller(response);
		
		try {		
			
			Element responseElement = marshaller.marshall(response);
	        Transformer serializer = TransformerFactory.newInstance().newTransformer();
	        serializer.transform(new DOMSource(responseElement), new StreamResult(stw));
	        
	        if (logger.isDebugEnabled()) {
                logger.debug("Outputting the unsigned SAML response:");
                logger.debug(stw.toString());
            }	        
	        	        			
		} catch(MarshallingException ex) {
			
			logger.error("The OpenSAML toolkit was unable to marshall the SAML response into XML");
			throw new SAMLException(ex);
			
		} catch(TransformerConfigurationException ex) {
			
			logger.error("The system was unable to configure the XML transformer for the SAML response");
			throw new SAMLException(ex);			
		
		} catch(TransformerException ex) {
			logger.error("The system was unable to serialize the SAML response");
			throw new SAMLException(ex);	
		}
		
		return stw.toString();
				
	}
		
	/**
	 * Builds a SAML response using the OpenSAML API.
	 * 
	 * @return
	 */
	public static String createAttributeQueryResponse(String username, String recipientURL, 
			AttributeStatement attributeStatement, String samlResponseStatus, int validBefore, int validAfter) throws SAMLException {
		
		logger.trace("Initialising OpenSAML Response builder");
		
		StringWriter stw = new StringWriter();				
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Response.DEFAULT_ELEMENT_NAME);		
		Response response = (Response)builder.buildObject();
		
		//response.addNamespace(new Namespace("urn:oasis:names:tc:SAML:2.0:assertion", null));
		//response.addNamespace(new Namespace("http://www.w3.org/2001/04/xmlenc#", "xenc"));

		DateTime now = new DateTime();
		DateTime issueInstant = now.minusHours(2);
			
		// parameters tested successfully in Google		
		response.setIssueInstant(issueInstant);
		response.setID(VmSAMLUtils.createID());
		response.setVersion(SAMLVersion.VERSION_20);		
		response.setStatus(buildStatus(samlResponseStatus));
		
		Assertion assertion = buildAssertion(username, recipientURL, attributeStatement, validBefore, validAfter);	
		
		response.getAssertions().add(assertion);
								
		MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
		Marshaller marshaller = marshallerFactory.getMarshaller(response);
		
		try {		
			
			Element responseElement = marshaller.marshall(response);
	        Transformer serializer = TransformerFactory.newInstance().newTransformer();
	        serializer.transform(new DOMSource(responseElement), new StreamResult(stw));
	        
	        if (logger.isDebugEnabled()) {
                logger.debug("Outputting the unsigned SAML response:");
                logger.debug(stw.toString());
            }	        
	        	        			
		} catch(MarshallingException ex) {
			
			logger.error("The OpenSAML toolkit was unable to marshall the SAML response into XML");
			throw new SAMLException(ex);
			
		} catch(TransformerConfigurationException ex) {
			
			logger.error("The system was unable to configure the XML transformer for the SAML response");
			throw new SAMLException(ex);			
		
		} catch(TransformerException ex) {
			logger.error("The system was unable to serialize the SAML response");
			throw new SAMLException(ex);	
		}
		
		return stw.toString();
		
	}
	
	/**
	 * Builds an attribute statement XML element.
	 * 
	 * @param map
	 * @return
	 */
	private static AttributeStatement buildAttributeStatement(List<Attribute> attributes) {
						
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME);		
		AttributeStatement attributeStatement = (AttributeStatement)builder.buildObject();		
		attributeStatement.getAttributes().addAll(attributes);		
			
		return attributeStatement;
	}
	
	/**
	 * Creates an attribute with a value from an attribute without
	 * a value. 
	 * 
	 * @param attribute
	 * @param value
	 * @return
	 */
	public static Attribute createAttribute(Attribute template, String value) {
		
		// build the attribute
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Attribute.DEFAULT_ELEMENT_NAME);		
		Attribute attribute = (Attribute)builder.buildObject();		
		attribute.setNameFormat(template.getNameFormat());
		attribute.setName(template.getName());
		
		// assign the value				
		XSStringBuilder stringBuilder = (XSStringBuilder) Configuration.getBuilderFactory().getBuilder(XSString.TYPE_NAME);
		XSString stringValue = stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
		stringValue.setValue(value);
		attribute.getAttributeValues().add(stringValue);
				
		return attribute;		
		
	}
	
	/**
	 * Builds an Attribute element using the "basic" format.
	 * This method is used to create an attribute without a value.
	 * 
	 * @param name
	 */
	public static Attribute createBasicAttribute(String name) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Attribute.DEFAULT_ELEMENT_NAME);		
		Attribute attribute = (Attribute)builder.buildObject();		
		attribute.setNameFormat(Attribute.BASIC);
		attribute.setName(name);
		
		return attribute;		
	}
	
	/**
	 * Builds an Attribute element using the "basic" format.
	 * This method is used to create an attribute with a value.
	 * 
	 * @param name
	 */
	public static Attribute createBasicAttribute(String name, String value) {
		
		// build the attribute
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Attribute.DEFAULT_ELEMENT_NAME);		
		Attribute attribute = (Attribute)builder.buildObject();		
		attribute.setNameFormat(Attribute.BASIC);
		attribute.setName(name);
		
		// assign the value				
		XSStringBuilder stringBuilder = (XSStringBuilder) Configuration.getBuilderFactory().getBuilder(XSString.TYPE_NAME);
		XSString stringValue = stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
		stringValue.setValue(value);
		attribute.getAttributeValues().add(stringValue);
				
		return attribute;		
	}
	
	/**
	 * Build the attribute value object.
	 * 
	 * @param value
	 * @return
	 */
	public static XSString createXSString(String value) {
		
		XSStringBuilder stringBuilder = (XSStringBuilder) Configuration.getBuilderFactory().getBuilder(XSString.TYPE_NAME);
		XSString stringValue = stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
		stringValue.setValue(value);
		
		return stringValue;	
	}
	
	/**
	 * The assertion consumer service will validate the NotOnOrAfter attribute after it has 
	 * parsed the assertion. If the ACS clock and the SSO clock are set to a different time,
	 * the assertion may be rejected. Some leeway is therefore provided in this method. 
	 * The amount of padding is configurable.
	 *  
	 * @return
	 */
	private static Date buildNotOnOrAfter() {
			
		Calendar calendar = new GregorianCalendar();			
		int validAfter = 61;
		calendar.add(Calendar.MINUTE, validAfter);
				
		return calendar.getTime();
		
	}
	
	/**
	 * The assertion consumer service will validate the NotBefore attribute after it has 
	 * parsed the assertion. If the ACS clock and the SSO clock are set to a different time,
	 * the assertion may be rejected. Some leeway is therefore provided in this method. 
	 * The amount of padding is configurable.
	 *  
	 * @return
	 */
	private static Date buildNotBefore() {
		
		Calendar calendar = new GregorianCalendar();		
		int validBefore = 61;		
		calendar.add(Calendar.MINUTE, -validBefore);
				
		return calendar.getTime();
		
	}
		
	/**
	 * Builds the Status XML element.
	 * 
	 * @return
	 */
	private static Status buildStatus(String samlResponseStatus) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Status.DEFAULT_ELEMENT_NAME);		
		Status status = (Status)builder.buildObject();		
		status.setStatusCode(buildStatusCode(samlResponseStatus));
		
		return status;		
	}
	
	/**
	 * Builds the StatusCode XML element.
	 * 
	 * @return
	 */
	private static StatusCode buildStatusCode(String samlResponseStatus) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(StatusCode.DEFAULT_ELEMENT_NAME);		
		StatusCode statusCode = (StatusCode)builder.buildObject();		
		statusCode.setValue(samlResponseStatus);
		
		return statusCode;				
	}
	
	/**
	 * Builds the Assertion XML element.
	 * 
	 * @param samlIssuer
	 * @return
	 */
	private static Assertion buildAssertion(String email,
			String recipientURL, AttributeStatement attributeStatement, int validBefore, int validAfter) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Assertion.DEFAULT_ELEMENT_NAME);	
		Assertion assertion = (Assertion)builder.buildObject();
				
		assertion.setID(VmSAMLUtils.createID());
		assertion.setSubject(buildSubject(email, recipientURL));
		assertion.setVersion(SAMLVersion.VERSION_20);
		assertion.setIssuer(buildIssuer(VmSAMLConstants.SAML_ISSUER_IDP));
		assertion.setIssueInstant(new DateTime("2003-04-17T00:46:02Z"));		
		assertion.setConditions(buildConditions(validBefore, validAfter));
		assertion.getAuthnStatements().add(buildAuthenticationStatement());
		
		if(attributeStatement != null) {
			assertion.getAttributeStatements().add(attributeStatement);
		}						
			
		return assertion;		
	}
	
	/**
	 * Builds the assertion XML element. Convenience method for building 
	 * assertions that do not contain attribute statements.
	 * 
	 * @param email
	 * @return
	 */
	private static Assertion buildAssertion(String email, String recipientURL, int validBefore, int validAfter) {
		
		return buildAssertion(email, recipientURL, null, validBefore, validAfter);		
	}
	
	/**
	 * Builds the Conditions XML element.
	 * 
	 * @return
	 */
	private static Conditions buildConditions(int validBefore, int validAfter) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Conditions.DEFAULT_ELEMENT_NAME);
		
		DateTime now = new DateTime();		
		DateTime notBefore = now.minusMinutes(validBefore);		
		DateTime notOnOrAfter = now.plusMinutes(validAfter);
				
		Conditions conditions = (Conditions)builder.buildObject();
		
		conditions.setNotBefore(notBefore);
		conditions.setNotOnOrAfter(notOnOrAfter);
		
		return conditions;		
	}
	
	/**
	 * Builds the AuthnStatement XML element.
	 *  
	 * @return
	 */
	private static AuthnStatement buildAuthenticationStatement() {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(AuthnStatement.DEFAULT_ELEMENT_NAME);		
		AuthnStatement authnStatement = (AuthnStatement)builder.buildObject();
		authnStatement.setAuthnInstant(new DateTime());
		authnStatement.setAuthnContext(buildAuthnContext());
		
		return authnStatement;		
	}
	
	/**
	 * Builds the AuthnContext XML element.
	 * 
	 * @return
	 */
	private static AuthnContext buildAuthnContext() {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(AuthnContext.DEFAULT_ELEMENT_NAME);		
		AuthnContext authnContext = (AuthnContext)builder.buildObject();
		authnContext.setAuthnContextClassRef(buildAuthnContextClassRef());
		
		return authnContext;		
	}
	
	/**
	 * Builds the AuthnContextClassRef XML element.
	 * 
	 * @return
	 */
	private static AuthnContextClassRef buildAuthnContextClassRef() {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(AuthnContextClassRef.DEFAULT_ELEMENT_NAME);		
		AuthnContextClassRef authnContextClassRef = (AuthnContextClassRef)builder.buildObject();
		authnContextClassRef.setAuthnContextClassRef("urn:oasis:names:tc:SAML:2.0:ac:classes:Password");
		
		return authnContextClassRef;		
	}
	
	
	/**
	 * Builds the subject XML element.
	 * 
	 * @return
	 */
	private static Subject buildSubject(String email, String recipientURL) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Subject.DEFAULT_ELEMENT_NAME);		
		Subject subject = (Subject)builder.buildObject();		
		subject.setNameID(buildNameID(email));
		
		if(recipientURL != null) {			
			subject.getSubjectConfirmations().add(buildSubjectConfirmation(recipientURL));
		}			
				
		return subject;		
	}
	
	/**
	 * Builds the subject XML element without a subject confirmation.
	 * 
	 * @param email
	 * @return
	 */
	private static Subject buildSubject(String email) {
		return buildSubject(email, null);
	}
	
	/**
	 * Builds the SubjectConfirmation XML element.
	 * 
	 * @return
	 */
	private static SubjectConfirmation buildSubjectConfirmation(String recipientURL) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);		
		SubjectConfirmation subjectConfirmation = (SubjectConfirmation)builder.buildObject();		
		subjectConfirmation.setMethod("urn:oasis:names:tc:SAML:2.0:cm:bearer");
		subjectConfirmation.setSubjectConfirmationData(buildSubjectConfirmationData(recipientURL));
		
		return subjectConfirmation;		
	}
	
	/**
	 * Builds the SubjectConfirmationData XML element.
	 * 
	 * @param acsURL
	 * @return
	 */
	private static SubjectConfirmationData buildSubjectConfirmationData(String recipientURL) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(SubjectConfirmationData.DEFAULT_ELEMENT_NAME);		
		SubjectConfirmationData subjectConfirmationData = (SubjectConfirmationData)builder.buildObject();		
		subjectConfirmationData.setRecipient(recipientURL);
		
		return subjectConfirmationData;		
	}
	
	/**
	 * Builds the NameID XML element.
	 * 
	 * @param email
	 * @return
	 */
	private static NameID buildNameID(String email) {
		
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(NameID.DEFAULT_ELEMENT_NAME);
		
		NameID nameID = (NameID)builder.buildObject();
		nameID.setFormat("urn:oasis:names:tc:SAML:2.0:nameid-format:emailAddress");
		nameID.setValue(email);
		
		return nameID;				
	}
	
	/**
	 * Builds a SAML issuer using the OpenSAML toolkit.
	 * The issuer is passed into this method because it can be
	 * called by both identity provider and service provider. 
	 * 
	 * @param samlIssuer
	 * @return
	 */	
	private static Issuer buildIssuer(String samlIssuer) {
			
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
		Issuer issuer = (Issuer)builder.buildObject();
		issuer.setValue(samlIssuer);
			
		return issuer;		
	}
		
	/*
	private static void signAssertion(Assertion assertion, String publicKeyPath, String privateKeyPath) {
		
		Credential signingCredential = getSigningCredential(publicKeyPath, privateKeyPath);
				
		SAMLObjectBuilder builder = (SAMLObjectBuilder)builderFactory.getBuilder(Signature.DEFAULT_ELEMENT_NAME);

		Signature signature = (Signature)builder.buildObject(Signature.DEFAULT_ELEMENT_NAME);
		
		signature.setSigningCredential(signingCredential);
		signature.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_DSA);
		signature.setCanonicalizationAlgorithm(SignatureConstants.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
		signature.setKeyInfo(getKeyInfo(signingCredential));
		
		KeyInfoGenerator kiGenerator = SecurityHelper.getKeyInfoGenerator(signingCredential, null, null);
		KeyInfo keyInfo = kiGenerator.generate(signingCredential);
		KeyInfoHelper.addCertificate(keyInfo, (org.opensaml.xml.signature.X509Certificate));
				                      
		signature.setKeyInfo(keyInfo);
		assertion.setSignature(signature);

		try {
		    Configuration.getMarshallerFactory().getMarshaller(assertion).marshall(assertion);
		} catch (MarshallingException e) {
		    e.printStackTrace();
		}

		try {
		    Signer.signObject(signature);
		} catch (SignatureException ex) {
		    ex.printStackTrace();
		}
		
	}
	
	private static Credential getSigningCredential(String publicKeyPath, String privateKeyPath) 
	throws SAMLException {
		
		DSAPublicKey publicKey = (DSAPublicKey)SAMLUtils.getPublicKey(publicKeyPath, "DSA");
		DSAPrivateKey privateKey = (DSAPrivateKey)SAMLUtils.getPrivateKey(privateKeyPath, "DSA");		
		
		BasicCredential credential = new BasicCredential();
		credential.setPrivateKey(privateKey);
		credential.setPublicKey(publicKey);
		
		return credential;
		
	}
	
	private static Signature getSignature() {
		
	}
	*/
	
		

}
