/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * UniqueItemsValidator class for Unique items validator.
 *
 * @author mxos-dev
 * 
 */
public class UniqueItemsValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -2453061999914008143L;
    private static Logger LOG = Logger.getLogger(UniqueItemsValidator.class);
    public static final String PROPERTY = "uniqueItems";
    protected boolean unique = false;

    /**
     * default constructor.
     *
     * @param uniqueItemsNode
     *            uniqueItemsNode
     */
    public UniqueItemsValidator(JsonNode uniqueItemsNode) {
        if (uniqueItemsNode.isBoolean()) {
            this.unique = uniqueItemsNode.getBooleanValue();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        List<String> errors = new ArrayList<String>();

        if (this.unique) {
            Set<JsonNode> set = new HashSet<JsonNode>();
            for (JsonNode n : node) {
                set.add(n);
            }

            if (set.size() < node.size()) {
                errors.add(at + ": the items in the array must be unique");
            }
        }
        return errors;
    }
}
