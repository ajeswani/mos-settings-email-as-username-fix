/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema.impl;

import java.util.List;
import org.codehaus.jackson.JsonNode;

/**
 * JSONValidator is schema validator.
 *
 * @author mxos-dev
 *
 */
public interface JSONValidator {
    String AT_ROOT = "$";
    /**
     * Method to validate.
     * @param paramJsonNode paramJsonNode
     * @param paramString paramString
     * @return list list of errors
     */
    List<String> validate(JsonNode paramJsonNode, String paramString);
    /**
     * Method to validate.
     * @param paramJsonNode1 paramJsonNode1
     * @param paramJsonNode2 paramJsonNode2
     * @param paramString paramString
     * @return list list of errors
     */
    List<String> validate(JsonNode paramJsonNode1, JsonNode paramJsonNode2,
            String paramString);
}
