/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.misc;

import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.SystemProperty;

/**
 * Utility class containing utility related methods for MxOS.
 *
 * @author mxos-dev
 */
public class MxOSSystemConfigUtil {
    private static MxOSSystemConfigUtil instance;
    private Map<String, String> cache;

    private MxOSSystemConfigUtil() {
        init();
    }

    /**
     * Method init.
     *
     */
    private void init() {
        cache = new HashMap<String, String>();
        for (SystemProperty entry : SystemProperty.values()) {
            cache.put(entry.name(), System.getProperty(entry.name()));
        }
    }

    /**
     * Method to set count
     *
     * @param count
     * 
     * @return instance
     */
    public synchronized static MxOSSystemConfigUtil getInstance() {
        if (null == instance) {
            instance = new MxOSSystemConfigUtil();
        }
        return instance;
    }

    /**
     * Method to get max allowed Ips.
     *
     * @return int
     */
    public int getMaxAllowedIps() {
        int returnVal = 0;
        String value = cache.get(SystemProperty.maxMailAccessAllowedIps.name());
        if (null != value) {
            try {
                returnVal = Integer.valueOf(value);
            } catch (NumberFormatException e) {
            }
        }
        return returnVal;
    }
}
