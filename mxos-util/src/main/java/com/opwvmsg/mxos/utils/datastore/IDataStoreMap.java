package com.opwvmsg.mxos.utils.datastore;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public interface IDataStoreMap<V, K> {

    /**
     * Associates the specified value with the specified key in this map
     * (optional operation). If the map previously contained a mapping for this
     * key, the old value is replaced by the specified value.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key with which the specified value is to be associated.
     * @param value - value to be associated with the specified key.
     * @return - previous value associated with specified key, or null if there
     *         was no mapping for key.
     */
    V put(String mapId, K key, V value);
    
    /**
     * Puts an entry into this map with a given ttl (time to live) value. Entry
     * will expire and get evicted after the ttl. If ttl is 0, then the entry
     * lives forever.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key of the entry
     * @param value - value of the entry
     * @param ttl - maximum time for this entry to stay in the map
     * @param timeunit - time unit for the ttl
     * @return old value of the entry
     */
    V put(String mapId, K key, V value, long ttl, TimeUnit timeunit);
    
    /**
     * Copies all of the mappings from the provided map to Distributed map.
     * 
     * @param mapId - Name or Id of Distributed map.
     * @param map
     */
    void putAll(String mapId, Map<? extends K,? extends V> map);

    /**
     * Puts an entry into this map with a given ttl (time to live) value. Entry
     * will expire and get evicted after the ttl. If ttl is 0, then the entry
     * lives forever. Its similar to put operation except that set doesn't
     * return the old value which is more efficient.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key of the entry
     * @param value - value of the entry
     * @param ttl - maximum time for this entry to stay in the map
     * @param timeunit - time unit for the ttl
     */
    void set(String mapId, K key, V value, long ttl, TimeUnit timeunit); 
    
    /**
     * This method returns a clone of original value, modifying the returned
     * value does not change the actual value in the map. One should put
     * modified value back to make changes visible to all nodes.
     * 
     * @param mapId - Name or Id of Distributed map.
     * @param keys - keys to get
     * @param value - the value to which this map maps the specified key, or
     *            null if the map contains no mapping for this key.
     * @return
     */
    V get(String mapId, Object key);
    
    /**
     * Returns the entries for the given keys from Distributed map. Warning: The
     * returned map is NOT backed by the original map, so changes to the
     * original map are NOT reflected in the returned map, and vice-versa.
     * 
     * @param mapId - Name or Id of Distributed map.
     * @param keys - set of keys to get
     * @return - map of entries
     */
    Map<K,V> getAll(String mapId, Set<K> keys);

    /**
     * Removes the mapping for this key from this map if it is present.
     * 
     * @param mapId - Name or Id of Distributed map.
     * @param key - key whose mapping is to be removed from the map.
     * @return previous value associated with specified key, or null if there
     *         was no mapping for key.
     */
    V remove(String mapId, Object key);
    
    /**
     * Evicts the specified key from this map. If a MapStore defined for this
     * map, then the entry is not deleted from the underlying MapStore
     * (persistence), evict only removes the entry from the memory.
     * @param mapId - Name or Id of Distributed map.
     * @param key - key to evict
     * 
     * @return true if the key is evicted, false otherwise.
     */
    boolean evict(String mapId, K key);
    
    /**
     * Removes all mappings from this map.
     * 
     * @param mapId - Name or Id of Distributed map.
     */
    void clear(String mapId);
    
    /**
     * Returns the number of key-value mappings in this map.
     * 
     * @param mapId - Name or Id of Distributed map.
     * @return
     */
    int size(String mapId);
}
