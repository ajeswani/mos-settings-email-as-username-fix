/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.net.URL;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchema;
import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaException;
import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaProvider;

/**
 * JacksonSchemaProvider is schema provider.
 *
 * @author mxos-dev
 * 
 */
public class JacksonSchemaProvider implements JSONSchemaProvider, Serializable {
    private static final long serialVersionUID = 7600020194154713323L;
    private static Logger LOG = Logger.getLogger(JacksonSchemaProvider.class);
    protected ObjectMapper mapper;

    /**
     * Defualt constructor.
     *
     * @param mapper
     *            mapper
     */
    public JacksonSchemaProvider(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * Method to get Schema.
     *
     * @param schema
     *            schema
     * @return jsonSchema
     */
    @Override
    public JSONSchema getSchema(String schema) {
        try {
            JsonNode schemaNode = this.mapper.readTree(schema);
            return new JacksonSchema(this.mapper, schemaNode);
        } catch (IOException ioe) {
            LOG.error("Failed to load json schema!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }

    /**
     * Method to get Schema.
     *
     * @param schemaStream
     *            schemaStream
     * @return jsonSchema
     */
    @Override
    public JSONSchema getSchema(InputStream schemaStream) {
        try {
            JsonNode schemaNode = this.mapper.readTree(schemaStream);
            return new JacksonSchema(this.mapper, schemaNode);
        } catch (IOException ioe) {
            LOG.error("Failed to load json schema!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }

    /**
     * Method to get Schema.
     *
     * @param schemaReader
     *            schemaReader
     * @return jsonSchema
     */
    @Override
    public JSONSchema getSchema(Reader schemaReader) {
        try {
            JsonNode schemaNode = this.mapper.readTree(schemaReader);
            return new JacksonSchema(this.mapper, schemaNode);
        } catch (IOException ioe) {
            LOG.error("Failed to load json schema!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }

    /**
     * Method to get Schema.
     *
     * @param schemaURL
     *            schemaURL
     * @return jsonSchema
     */
    @Override
    public JSONSchema getSchema(URL schemaURL) {
        try {
            JsonNode schemaNode = this.mapper.readTree(schemaURL.openStream());
            return new JacksonSchema(this.mapper, schemaNode);
        } catch (IOException ioe) {
            LOG.error("Failed to load json schema!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }
    
    @Override
    public JsonNode getNode(InputStream schemaStream) {
        try {
            JsonNode schemaNode = this.mapper.readTree(schemaStream);
            return schemaNode;
        } catch (IOException ioe) {
            LOG.error("Failed to get json node!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }
}
