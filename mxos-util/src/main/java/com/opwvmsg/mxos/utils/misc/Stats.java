/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.utils.misc;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.Operation;

/**
 * Stats class for recording the stat events.
 *
 * The Stats library created two container for the statEvents. At a time only
 * one container will be used for recording the stat events. This for unblocking
 * the application threads so that while the Stats library processes the
 * existing statEvent the application threads can add the stat event to the
 * second container.
 *
 * @author mxos-dev
 **/
public class Stats implements Runnable {

    private static StatEventsContainer eventsContainer1;
    private static StatEventsContainer eventsContainer2;
    private static boolean switchContainer;
    private static boolean threadRun;
    private static long statsInterval;
    private long timeT1, timeT2;
    private static Thread statThreadinstance;
    private static boolean isStatRunning;
    private static Logger logger = Logger.getLogger(Stats.class);

    /**
     * Constructor.
     *
     * @param statInterval statInterval in seconds.
     */
    private Stats(long statInterval) {
        statsInterval = statInterval * 1000;
        switchContainer = true;
        threadRun = true;
        eventsContainer1 = new StatEventsContainer();
        eventsContainer2 = new StatEventsContainer();
    }

    /**
     * Stats thread run method.
     *
     * The stats thread will process and print the stats details after each
     * statsInterval.
     *
     */
    public void run() {
        try {
            Thread.sleep(statsInterval);
            while (threadRun) {
                switchContainer = false;
                timeT1 = System.currentTimeMillis();
                processStats();
                timeT2 = System.currentTimeMillis();
                Thread.sleep(getSleepTime(timeT1, timeT2));
                switchContainer = true;
                timeT1 = System.currentTimeMillis();
                processStats();
                timeT2 = System.currentTimeMillis();
                Thread.sleep(getSleepTime(timeT1, timeT2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long getSleepTime(long timeT1, long timeT2) {
        long sleepTime = statsInterval - (timeT2 - timeT1);
        if (sleepTime < 0) {
            sleepTime = 0;
            logger.warn("Too many stat events to process in statsInterval");
        }
        return sleepTime;
    }

    /**
     * Method processing all the statEvent in the container and print the
     * summary.
     *
     */
    private void processStats() {
        if (switchContainer) {
            eventsContainer1.processEvents();
        } else {
            eventsContainer2.processEvents();
        }

    }

    /**
     * Init method for Stats library.
     *
     * Any application which wants to use the Stats library has to call the init
     * method.
     *
     * @param statsInterval StatsInterval in seconds.
     *
     */
    public static void init(long statsInterval) {
        if (!isStatRunning) {
            statThreadinstance = new Thread(new Stats(statsInterval));
            isStatRunning = true;
            statThreadinstance.start();
            logger.info("#Stats format");
            logger.info("#<ServiceName> <Operation> "
                    + "<min/max/avg> ns <err/timeout/total>");
        }
    }

    /**
     * Method for starting the timer for any stat.
     *
     * Application thread should use this method to start the timer for any
     * stat.
     *
     * @return startTime startTime in nano seconds.
     *
     */
    public static long startTimer() {
        long startTime = System.nanoTime();
        return startTime;
    }

    /**
     * Method for stopping the timer the thread has started in startTimer method
     * for any stat. This method will compute the time the application took for
     * doing the operation specified by the statName and then it will call the
     * one of the containers addEvent methos to record the statEvent.
     *
     * @param statName ServiceEnum.
     * @param serviceOpr Operation.
     * @param startTime long.
     * @param statStatus StatStatus.
     *
     */
    public static void stopTimer(ServiceEnum statName, Operation serviceOpr,
            long startTime, StatStatus statStatus) {
        long totalTime = System.nanoTime() - startTime;
        if (!switchContainer) {
            eventsContainer1.addEvent(statName, serviceOpr, totalTime,
                    statStatus);
        } else {
            eventsContainer2.addEvent(statName, serviceOpr, totalTime,
                    statStatus);
        }
    }

    /**
     * Method for stopping the statsLogger thread.
     */

    public static void stopStatlogger() {
        threadRun = false;
    }
}
