/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/config/MxOSConfig.java#1 $
 */

package com.opwvmsg.mxos.utils.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.config.intermail.IntermailConfig;
import com.opwvmsg.utils.paf.util.Utilities;

/**
 * A concrete singleton class that provides access to configuration information.
 * This class is a wrapper around the PAF MxOSConfig API to provide MxOS
 * specific configuration behavior.
 * <p/>
 * This class is thread safe.
 *
 * @author Aricent (partially used from RM)
 */
public class MxOSConfig {

    private static Logger logger = Logger.getLogger(MxOSConfig.class);

    /**
     * Default Constructor.
     */
    private MxOSConfig() {
    }

    /**
     * delimiters.
     */
    public static final char[] DELIMITERS = { ':', '\n', '\t', ',', ' ' };
    public static final String DEFAULT_MXOS_CONTEXT_TYPE = "BACKEND";

    public static final boolean CONST_BOOLEAN_TRUE = true;
    public static final boolean CONST_BOOLEAN_FALSE = false;
    public static final int CONST_ZERO = 0;
    public static final int CONST_HUNDRED = 100;
    public static final String DEFAULT_COS_ID = "default";
    public static final String DEFAULT_COS_DN = "cn={cosId},cn=admin root";
    public static final String DEFAULT_MAILBOX_STATUS = "active";
    public static final String DEFAULT_PASSWORD_STORE_TYPE = "clear";
    public static final String DEFAULT_PREENCRYPTED_PASSWORD_STORE_TYPE = "sha1";
    public static final String DEFAULT_CREATE_MSSMAILBOX_ON_PROXY_STATUS = "true";
    public static final int DEFAULT_STATS_NUM_EVENTS = 10;
    public static final int DEFAULT_STATS_TIME_DURATION = 10;
    public static final int DEFAULT_SEARCH_MAX_ROWS = 1000;
    public static final int DEFAULT_SEARCH_MAX_TIME_OUT = 5000;
    public static final String DEFAULT_MAILBOX_CUSTOM_FIELDS_CLASS =
        "com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxCustomProperty";
    public static final String DEFAULT_DOMAIN_CUSTOM_FIELDS_CLASS =
        "com.opwvmsg.mxos.backend.crud.ldap.LDAPDomainCustomProperty";
    public static final String DEFAULT_MYSQL_CONNECTION_URL =
            "jdbc:mysql:replication://localhost,localhost/mxos";
    public static final String DEFAULT_MYSQL_USER = "mxos";
    public static final String DEFAULT_MYSQL_PASSWORD = "mxos";
    public static final int DEFAULT_META_MAX_CONNECTIONS = 10;
    public static final String DEFAULT_MYSQL_META_SQL_FILE_PATH = "";
    public static final String DEFAULT_META_BACKEND = "mss";
    public static final String DEFAULT_ADDRESS_BOOK_BACKEND = "we";
    public static final String DEFAULT_TASKS_BACKEND = "ox";
    public static final String DEFAULT_MAILBOX_BACKEND = "ldap";
    public static final String DEFAULT_BLOB_BACKEND = "mss";
    public static final String DEFAULT_NOTIFY_BACKEND = "hazelcast";    
    public static final boolean DEFAULT_TRUSTED_CLIENT = true;
    public static final int DEFAULT_MESSAGE_TYPE = 0;
    public static final int DEFAULT_MESSAGE_PRIORITY = 0;
    public static final int DEFAULT_DELIVER_NDR = 0;
    public static final String DEFAULT_COMPRESSION_TYPE = "quicklz";
    public static final int DEFAULT_MESSAGE_EXP_TIME_OFFSET_IN_SEC =
            30 * 24 * 60 * 60; // 30 days

    public static final String[] DEFAULT_SYSTEM_FOLDERS = { "INBOX" };
    
    public static final int DEFAULT_POSIX_MAX_BUCKETS = 10;
    public static final String DEFAULT_POSIX_DB_ROOT_FOLDER = "/tmp/mxos";

    public static final int DEFAULT_BAD_PASSWORD_WINDOW_MINS = 120;
    public static final int DEFAULT_AUTHENTICATION_LOCK_TIMEOUT = 5;
    public static final int DEFAULT_BAD_PASSWORD_DELAY_SECONDS = 5;
    public static final String DEFAULT_MAILBOX_ATTR = "userName,domain,firstName,lastName,email,mailboxId,cosId,maxNumAliases";
    public static ArrayList<String> migratedDomains = null;
    
    private static final String NOTIFY_HTTPPOOL_MAXSIZE = "10";
    private static final String NOTIFY_HTTPCON_TIMEOUT_DEF = "10000";
    private static final String NOTIFY_HTTPREAD_TIMEOUT_DEF = "60000";
    private static final String NOTIFY_THREADPOOL_MAXSIZE = "10";
    public static final String PROVIDER_HOST = "host";

    /**
     * Specifies that MxOS will dynamically accomodate changes to the config
     * key.
     */
    public static final Integer CFG_TRIVIAL = new Integer(0);

    /**
     * Specifies that MxOS will have to be restarted in order to pick up changes
     * to the value in configuration store.
     * <p>
     *
     * When using imconfedit and assessing changes to values for keys with this
     * impact a message is displayed indicating that webedge will need to be
     * restarted. If the changes are committed imconfedit will prompt to restart
     * all the affected servers.
     */
    public static final Integer CFG_RESTART = new Integer(1);

    public static final Integer CFG_NEVER = new Integer(2);
    /**
     * common.
     */
    public static final String APP_COMMON = "common";

    /**
     * default application.
     */
    public static final String APP_DEFAULT = "mxos";

    /**
     * default Search application.
     */
    public static final String APP_SEARCH_DEFAULT = "mxossearch";

    /**
     * imdirserv.
     */
    public static final String APP_IMDIRSERV = "imdirserv";

    /**
     * imdircacheserv.
     */
    public static final String APP_IMDIRCACHESERV = "imdircacheserv";
    
    private static final String MASTER_SERVER_ROLE_PRIMARY = "1";

    /**
     * Keys.
     */
    public static final class Keys {

        /**
         * siteMode.
         */
        public static final String SITE_MODE = "siteMode";

        /**
         * siteName.
         */
        public static final String SITE_NAME = "siteName";

        /**
         * contextType - Context Class Name with package.
         */
        public static final String MXOS_CONTEXT_TYPE = "contextType";
        /**
         * defaultDirServer.
         */
        public static final String DEFAULT_DIR_SERVER = "defaultDirServer";

        /**
         * extensionsOptionalCoSAttributes
         */
        public static final String EXTENSIONS_OPTIONAL_COS_ATTRIBUTES = "extensionsOptionalCoSAttributes";

        /**
         * ldapServerHosts.
         */
        public static final String LDAP_SERVER_HOSTS = "ldapServerHosts";
        /**
         * ldapPort.
         */
        public static final String LDAP_PORT = "ldapPort";
        /**
         * defaultBindDN.
         */
        public static final String DEFAULT_BIND_DN = "defaultBindDN";
        /**
         * defaultBindPassword.
         */
        public static final String DEFAULT_BIND_PASSWORD =
                "defaultBindPassword";
        /**
         * masterServerRole.
         */
        public static final String MASTER_SERVER_ROLE = "masterServerRole";
        /**
         * configChangeCallbackEnabled.
         */
        public static final String CONFIG_CHANGE_CALLBACK_ENABLED =
                "configChangeCallbackEnabled";
        /**
         * configChangeCallbackBasePort.
         */
        public static final String CONFIG_CHANGE_CALLBACK_BASE_PORT =
                "configChangeCallbackBasePort";
        /**
         * configChangeCallbackNumPorts.
         */
        public static final String CONFIG_CHANGE_CALLBACK_NUM_PORTS =
                "configChangeCallbackNumPorts";
        /*  Start of Mailbox Config params */
        /**
         * defaultCosId.
         */
        public static final String DEFAULT_COS_ID = "defaultCosId";
        /**
         * defaultMailboxStatus.
         */
        public static final String DEFAULT_MAILBOX_STATUS =
            "defaultMailboxStatus";
        /**
         * defaultPasswordStoreType.
         */
        public static final String DEFAULT_PASSWORD_STORE_TYPE =
            "defaultPasswordStoreType";
        /**
         * defaultPasswordStoreType.
         */
        public static final String DEFAULT_PREENCRYPTED_PASSWORD_STORE_TYPE =
            "defaultPreEncryptedPasswordStoreType";        
        /**
         * systemFolderIds.
         */
        public static final String SYSTEM_FOLDERS = "systemFolderIds";
        /**
         * cryptoKeyList.
         */
        public static final String KEY_B_LIST = "cryptoKeyList";
        /**
         * enableSoftDelete.
         */
        public static final String ENABLE_SOFT_DELETE = "enableSoftDelete";
        /**
         * mailboxObjectClasses.
         */
        public static final String MAILBOX_OBJECT_CLASSES =
                "mailboxObjectClasses";
        /**
         * mailboxCustomFieldsClass - Class Name with package.
         */
        public static final String MAILBOX_CUSTOM_FIELDS_CLASS =
                "mailboxCustomFieldsClass";
        /**
         * domainCustomFieldsClass - Class Name with package.
         */
        public static final String DOMAIN_CUSTOM_FIELDS_CLASS =
                "domainCustomFieldsClass";

        /**
         * domainObjectClasses.
         */
        public static final String PARENT_DOMAIN_OBJECT_CLASSES =
                "parentDomainObjectClasses";
        /**
         * domainObjectClasses.
         */
        public static final String CHILD_DOMAIN_OBJECT_CLASSES =
                "childDomainObjectClasses";
        /**
         * statsEnabled.
         */
        public static final String STATS_ENABLED = "statsEnabled";
        /**
         * statsNumEvents.
         */
        public static final String STATS_NUM_EVENTS = "statsNumEvents";
        /**
         * statsTimeDuration.
         */
        public static final String STATS_TIME_DURATION = "statsTimeDuration";
        /**
         * searchMaxRows.
         */
        public static final String SEARCH_MAX_ROWS = "searchMaxRows";
        /**
         * searchTimeOut.
         */
        public static final String SEARCH_MAX_TIME_OUT = "searchMaxTimeOut";
        /**
         * List of MessageStore Host Names.
         */
        public static final String MESSAGE_STORE_HOSTS = "messageStoreHosts";
        
        /**
         * disableRedirectToSurrogateMSS
         */
        public static final String DISABLE_REDIRECT_TO_SURROGATE_MSS = 
                "disableRedirectToSurrogateMSS";

        /**
         * searchMaxRows.
         */
        public static final String META_MAX_CONNECTIONS =
                "metaMaxConnections";
        public static final String META_BACKEND = "metaBackend";
        public static final String ADDRESS_BOOK_BACKEND = "addressBookBackend";
        public static final String TASKS_BACKEND = "tasksBackend";
        public static final String MAILBOX_BACKEND = "mailboxBackend";
        public static final String BLOB_BACKEND = "blobBackend";
        public static final String TRUSTED_CLIENT = "trustedClient";
        public static final String MESSAGE_TYPE = "defaultMessageType";
        public static final String MESSAGE_PRIORITY = "messagePriority";
        public static final String DELIVER_NDR = "deliverNDR";
        public static final String COMPRESSION_TYPE = "compressionType";
        public static final String MESSAGE_EXP_TIME_OFFSET_IN_SEC =
                "messageExpTimeInSec";


        public static final String POSIX_MAX_BUCKETS = "posixMaxBuckets";
        public static final String POSIX_DB_ROOT_FOLDER = "posixDBRootFolder";
        
        public static final String SYSTEM_FOLDRS = "systemFolders";

        /**
         * badPasswordDelay.
         */
        public static final String SEARCH_BAD_PASSWORD_DELAY = "badPasswordDelay";
        /**
         * badPasswordWindow.
         */
        public static final String SEARCH_BAD_PASSWORD_WINDOW = "badPasswordWindow";
        /**
         * badPasswordWindow.
         */
        public static final String AUTHENTICATION_LOCK_TIMEOUT = "autheticationLockTimeout";
        /**
         * bgcPrimaryDomains.
         */
        public static final String BGC_PRIMARY_DOMAINS = "bgcPrimaryDomains";
        /**
         * mailApiAdaptorUrl.
         */
        public static final String MAIL_API_ADAPTOR_URL = "mailApiAdaptorUrl";
        /**
         * MAAappLogin.
         */
        public static final String MAIL_API_ADAPTOR_LOGIN = "MAAappLogin";
        /**
         * MAAappPassword.
         */
        public static final String MAIL_API_ADAPTOR_PASSWORD = "MAAappPassword";
    }

    // Set up the map for impacts
    protected static final Map<String, Object> KEY_IMPACTS =
            new HashMap<String, Object>();
    static {
        KEY_IMPACTS.put(Keys.DEFAULT_DIR_SERVER, CFG_RESTART);
        KEY_IMPACTS.put(Keys.LDAP_SERVER_HOSTS, CFG_RESTART);
        KEY_IMPACTS.put(Keys.LDAP_PORT, CFG_RESTART);
        KEY_IMPACTS.put(Keys.DEFAULT_BIND_DN, CFG_RESTART);
        KEY_IMPACTS.put(Keys.DEFAULT_BIND_PASSWORD, CFG_RESTART);
        KEY_IMPACTS.put(Keys.MASTER_SERVER_ROLE, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.MESSAGE_STORE_HOSTS, CFG_RESTART);

        KEY_IMPACTS.put(Keys.CONFIG_CHANGE_CALLBACK_ENABLED, CFG_RESTART);
        KEY_IMPACTS.put(Keys.CONFIG_CHANGE_CALLBACK_BASE_PORT, CFG_RESTART);
        KEY_IMPACTS.put(Keys.CONFIG_CHANGE_CALLBACK_NUM_PORTS, CFG_RESTART);

        KEY_IMPACTS.put(Keys.SITE_NAME, CFG_RESTART);
        KEY_IMPACTS.put(Keys.SITE_MODE, CFG_RESTART);

        // Mailbox Keys
        KEY_IMPACTS.put(Keys.MXOS_CONTEXT_TYPE, CFG_RESTART);
        KEY_IMPACTS.put(Keys.DEFAULT_COS_ID, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.DEFAULT_MAILBOX_STATUS, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.DEFAULT_PASSWORD_STORE_TYPE, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.KEY_B_LIST, CFG_RESTART);
        KEY_IMPACTS.put(Keys.ENABLE_SOFT_DELETE, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.MAILBOX_OBJECT_CLASSES, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.MAILBOX_CUSTOM_FIELDS_CLASS, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.PARENT_DOMAIN_OBJECT_CLASSES, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.CHILD_DOMAIN_OBJECT_CLASSES, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.STATS_ENABLED, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.STATS_NUM_EVENTS, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.STATS_TIME_DURATION, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.SEARCH_MAX_ROWS, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.SEARCH_MAX_TIME_OUT, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.SEARCH_BAD_PASSWORD_WINDOW, CFG_TRIVIAL);
        KEY_IMPACTS.put(Keys.SEARCH_BAD_PASSWORD_DELAY, CFG_TRIVIAL);

        KEY_IMPACTS.put(Keys.META_MAX_CONNECTIONS, CFG_RESTART);
        KEY_IMPACTS.put(Keys.META_BACKEND, CFG_RESTART);
        KEY_IMPACTS.put(Keys.ADDRESS_BOOK_BACKEND, CFG_RESTART);
        KEY_IMPACTS.put(Keys.TASKS_BACKEND, CFG_RESTART);
        KEY_IMPACTS.put(Keys.MAILBOX_BACKEND, CFG_RESTART);
        KEY_IMPACTS.put(Keys.BLOB_BACKEND, CFG_RESTART);
        KEY_IMPACTS.put(Keys.TRUSTED_CLIENT, CFG_RESTART);
        KEY_IMPACTS.put(Keys.MESSAGE_TYPE, CFG_RESTART);
        KEY_IMPACTS.put(Keys.MESSAGE_PRIORITY, CFG_RESTART);
        KEY_IMPACTS.put(Keys.DELIVER_NDR, CFG_RESTART);
        KEY_IMPACTS.put(Keys.COMPRESSION_TYPE, CFG_RESTART);
        KEY_IMPACTS.put(Keys.MESSAGE_EXP_TIME_OFFSET_IN_SEC, CFG_RESTART);

        KEY_IMPACTS.put(Keys.SYSTEM_FOLDRS, CFG_RESTART);
        KEY_IMPACTS.put(Keys.DISABLE_REDIRECT_TO_SURROGATE_MSS, CFG_RESTART);
        KEY_IMPACTS.put(Keys.EXTENSIONS_OPTIONAL_COS_ATTRIBUTES, CFG_RESTART);
    }

    // setup the map for default values
    protected static final Map<String, Object> DEFAULT_VALUES =
            new HashMap<String, Object>();

    // Set up the custom map for impacts (Ex. BGC, KDDI)
    protected static final Map<String, Object> CUSTOM_KEY_IMPACTS =
            new HashMap<String, Object>();

    // setup the custom map for default values (Ex. BGC, KDDI)
    protected static final Map<String, Object> CUSTOM_DEFAULT_VALUES =
            new HashMap<String, Object>();
    private static final String DEFAULT_REGEX_VALIDATION_TIMEOUT = "2000";
    private static final String DEFAULT_FOLDERS = "INBOX,Drafts,SentMail,Trash,Spam";
    private static final String DEFAULT_CN = "cn=allocCounts";

    /**
     * Gets the first directory host from the list of ':' separated hosts.
     *
     * @param hosts the absolute path to the desired node
     * @return String firstDirHost
     */
    public static String getFirstDirHost(String hosts) {
        if (hosts == null) {
            return null;
        }

        int firstDelim = hosts.indexOf(':');
        if (firstDelim == -1) {
            return hosts;
        } else {
            return hosts.substring(0, firstDelim);
        }
    }

    /**
     * Gets all directory hosts from the list of ':' separated hosts.
     *
     * @param hosts the absolute path to the desired node
     * @return String Array All dir hosts.
     */
    public static String[] getAllDirHosts(String hosts) {
        if (hosts == null) {
            return null;
        }

        // char[] delimeters = {':', '\n', '\t', ',', ' '};
        return Utilities.dsvToArray(hosts, DELIMITERS, false);
    }

    /**
     * Gets the list of appropriate LDAP Server hosts. Format of each string in
     * the array is: <host>:<port>.
     *
     * @return String array of <host>:<port> strings
     */
    public static String[] getLdapServers() {
        String defaultHost = getDefaultHost();
        logger.info("defaultHost: " + defaultHost);
        String defaultDirServer = getDefaultDirectoryServer();
        logger.info("defaultDirServer: " + defaultDirServer);

        String ldapServerHosts =
                getString(MxOSConfig.Keys.LDAP_SERVER_HOSTS, "");
        if (defaultDirServer != null && !defaultDirServer.equals("")) {
            ldapServerHosts =
                    getString(defaultHost, defaultDirServer,
                            MxOSConfig.Keys.LDAP_SERVER_HOSTS, "");
        }
        String[] hosts = getAllDirHosts(ldapServerHosts);

        // No LDAP hosts configured
        if (hosts == null || hosts.length == 0) {
            logger.error("No LDAP hosts configured"
                    + ", mOS will not run correctly if LDAP hosts are not properly configured"
                    + ", please check the configurations.");
            throw new RuntimeException(
                    "No LDAP hosts configured," +
                    " mOS will not run correctly if LDAP hosts are not properly configured," +
                    " please check the configurations.");
        }

        List<String> ldapHosts = new ArrayList<String>();
        for (int i = 0; i < hosts.length; i++) {
            if (isMasterServerRoleEnabled()
                    && defaultDirServer.equals(APP_IMDIRSERV)) {
                logger.info("MasterServerRole config is enabled");
                String masterServerRole = getString(hosts[i], defaultDirServer,
                        MxOSConfig.Keys.MASTER_SERVER_ROLE);
                if (logger.isDebugEnabled()) {
                    logger.debug("masterServerRole for host: " + hosts[i]
                            + " is: " + masterServerRole);
                }
                if (masterServerRole != null
                        && masterServerRole.equals(MASTER_SERVER_ROLE_PRIMARY)) {
                    ldapHosts.add(hosts[i]
                            + ":"
                            + getString(hosts[i], defaultDirServer,
                                    MxOSConfig.Keys.LDAP_PORT));
                    break;
                }
            } else {
                logger.info("MasterServerRole is disabled"
                        + ", considering all the hosts configured"
                        + " in 'ldapServerHosts' as active hosts");
                ldapHosts.add(hosts[i]
                        + ":"
                        + getString(hosts[i], defaultDirServer,
                                MxOSConfig.Keys.LDAP_PORT));
            }
        }

        if (ldapHosts.size() == 0) {
            if (isMasterServerRoleEnabled()
                    && defaultDirServer.equals(APP_IMDIRSERV)) {
                logger.error("None of the LDAP servers are configured with masterServerRole:[1]");
                throw new RuntimeException(
                        "None of the LDAP servers are configured with masterServerRole:[1]");
            } else {
                logger.error("No LDAP hosts configured,"
                        + " mOS will not run correctly if LDAP hosts are not properly configured,"
                        + " please check the configurations.");
                throw new RuntimeException(
                        "No LDAP hosts configured,"
                                + " mOS will not run correctly if LDAP hosts are not properly configured,"
                                + " please check the configurations.");
            }
        }

        String activeLdapHosts[] = ldapHosts.toArray(new String[ldapHosts
                .size()]);
        logger.info("Active LDAP server hosts");
        for (String activeHost : activeLdapHosts) {
            logger.info("activeLdapHost: " + activeHost);
        }
        return activeLdapHosts;
    }

    /**
     * Gets the list of appropriate LDAP Server hosts. Format of each string in
     * the array is: <host>:<port>.
     *
     * @return String array of <host>:<port> strings
     */
    public static String[] getLdapSearchServers() {
        String defaultHost = getDefaultHost();
        String defaultSearchDirServer =
                getDefaultSearchDirectoryServer();
        String ldapServerHosts =
                getString(MxOSConfig.Keys.LDAP_SERVER_HOSTS, "");
        if (defaultSearchDirServer != null
                && !defaultSearchDirServer.equals("")) {
            ldapServerHosts =
                    getString(defaultHost, defaultSearchDirServer,
                            MxOSConfig.Keys.LDAP_SERVER_HOSTS, "");
        }
        String[] hosts = getAllDirHosts(ldapServerHosts);

        for (int i = 0; i < hosts.length; i++) {
            hosts[i] =
                    hosts[i]
                            + ":"
                            + getString(hosts[i],
                                    defaultSearchDirServer,
                                    MxOSConfig.Keys.LDAP_PORT);
        }
        return hosts;
    }

    public static boolean isLdapServerAMaster(String host) {
        boolean flag = true;
        if (isMasterServerRoleEnabled()) {
            String defaultDirServer = getDefaultDirectoryServer();
            logger.info("defaultDirServer: " + defaultDirServer);

            if (APP_IMDIRSERV.equals(defaultDirServer)) {
                logger.info("MasterServerRole is enabled");
                String masterServerRole = getString(host, defaultDirServer,
                        MxOSConfig.Keys.MASTER_SERVER_ROLE);
                if (logger.isDebugEnabled()) {
                    logger.debug("masterServerRole for host: " + host + " is: "
                            + masterServerRole);
                }
                flag = masterServerRole != null
                        && masterServerRole.equals(MASTER_SERVER_ROLE_PRIMARY);
            }
        }
        return flag;
    }

    /**
     * Given a String[] as passed back from MxOSConfig.getArray(), determine if
     * it contains any values.
     *
     * @param configArray the array of Strings to check, as returned from
     *            MxOSConfig.getArray()
     * @return true if the array contains values, otherwise false
     */
    public static boolean isConfigArrayValid(String[] configArray) {
        if (configArray != null) {
            for (int i = 0; i < configArray.length; i++) {
                if (configArray[i].length() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Method to get Key_Impacts.
     *
     * @return Map for key_impacts.
     */
    public static Map<String, Object> getKeyImpacts() {
        if (CUSTOM_KEY_IMPACTS != null && !CUSTOM_KEY_IMPACTS.isEmpty()) {
            KEY_IMPACTS.putAll(CUSTOM_KEY_IMPACTS);
        }
        return Collections.unmodifiableMap(KEY_IMPACTS);
    }

    /**
     * Gets the default values.
     * <p/>
     *
     * @return A map of the default values.
     */
    public static synchronized Map<String, Object> getDefaultValues() {
        if (CUSTOM_DEFAULT_VALUES != null && !CUSTOM_DEFAULT_VALUES.isEmpty()) {
            DEFAULT_VALUES.putAll(CUSTOM_DEFAULT_VALUES);
        }
        return Collections.unmodifiableMap(DEFAULT_VALUES);
    }

    /**
     * Returns true if a default value exists for the given key.
     *
     * @param key The name of this configuration item
     * @return true if a default value exists for the given key
     */
    public static synchronized boolean containsDefault(String key) {
        return DEFAULT_VALUES.containsKey(key);
    }

    /**
     * Gets the default value for the given key.
     *
     * @param key The name of this configuration item.
     * @return The default value or <code>null</code> if no default exists.
     */
    public static synchronized String getDefault(String key) {
        return (String) DEFAULT_VALUES.get(key);
    }

    private static com.opwvmsg.utils.paf.config.Config getConfig() {
        return com.opwvmsg.utils.paf.config.Config.getInstance();
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as a String. Leading and trailing white space is trimmed. If the value is
     * empty, the empty string (rather than null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the first value of a
     * multi-valued entry is returned by this method.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     * @throws ConfigException for any errors.
     */
    public static String getString(String host, String app, String key)
        throws ConfigException {
        try {
            return getConfig().get(host, app, key);
        } catch (ConfigException ce) {
            int dotPos = host.indexOf('.');
            if (dotPos != -1) {
                String hostname = host.substring(0, dotPos);
                try {
                    return getConfig().get(hostname, app, key);
                } catch (ConfigException cex) {
                    logger.warn(cex);
                }
            }
            if (MxOSConfig.containsDefault(key)) {
                return MxOSConfig.getDefault(key);
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as a String. Leading and trailing white space is trimmed. If the value is
     * empty, the empty string (rather than null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the first value of a
     * multi-valued entry is returned by this method.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public static String getString(String host, String app, String key,
            String def) {
        return getConfig().get(host, app, key, def);
    }

    /**
     * Gets the configuration value with the given key, as a String. The current
     * default host and application values are used. Leading and trailing white
     * space is trimmed. If the value is empty, the empty string (rather than
     * null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the first value of a
     * multi-valued entry is returned by this method.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @return The value of the configuration item with this key.
     * @throws ConfigException If the key does not exist.
     */
    public static String getString(String key) throws ConfigException {
        try {
            return getConfig().get(key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                return MxOSConfig.getDefault(key);
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given key, as a String. The current
     * default host and application values are used. Leading and trailing white
     * space is trimmed. If the value is empty, the empty string (rather than
     * null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the first value of a
     * multi-valued entry is returned by this method.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public static String getString(String key, String def) {
        return getConfig().get(key, def);
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as a boolean. This method follows the Java convention whereby the string
     * "true" evaluates to true, and anything else is false.
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return A value of true or false. Returns false if the value is empty.
     * @throws ConfigException If the key does not exist.
     */
    public static boolean getBoolean(String host, String app, String key)
        throws ConfigException {
        try {
            return getConfig().getBoolean(host, app, key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                return Boolean.getBoolean(MxOSConfig.getDefault(key));
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as a boolean. This method follows the Java convention whereby the string
     * "true" evaluates to true, and anything else is false.
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return A value of true or false. Returns false if the value is empty, or
     *         the provided default value if the key does not exist.
     */
    public static boolean getBoolean(String host, String app, String key,
            boolean def) {
        return getConfig().getBoolean(host, app, key, def);
    }

    /**
     * Gets the configuration value with the given key, as a boolean. The
     * current default host and application values are used. This method follows
     * the Java convention whereby the string "true" evaluates to true, and
     * anything else is false.
     *
     * @param key The name of a configuration key.
     * @return A value of true or false. Returns false if the value is empty.
     * @throws ConfigException If the key does not exist.
     */
    public static boolean getBoolean(String key) throws ConfigException {
        try {
            return getConfig().getBoolean(key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                return Boolean.getBoolean(MxOSConfig.getDefault(key));
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given key, as a boolean. The
     * current default host and application values are used. This method follows
     * the Java convention whereby the string "true" evaluates to true, and
     * anything else is false.
     *
     * @param key The name of a configuration key.
     * @param def The default value.
     * @return A value of true or false. Returns false if the value is empty, or
     *         the provided default value if the key does not exist.
     */
    public static boolean getBoolean(String key, boolean def) {
        return getConfig().getBoolean(key, def);
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as an integer.
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The integer value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the value cannot
     *             be parsed as an integer.
     */
    public static int getInt(String host, String app, String key)
        throws ConfigException {
        try {
            return getConfig().getInt(host, app, key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                try {
                    return Integer.parseInt(MxOSConfig.getDefault(key));
                } catch (NumberFormatException e) {
                    logger.warn(key + ": integer conversion error: "
                            + getDefault(key), e);
                    throw ce;
                }
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as an integer.
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The integer value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public static int getInt(String host, String app, String key, int def) {
        return getConfig().getInt(host, app, key, def);
    }

    /**
     * Gets the configuration value with the given key, as an integer. The
     * current default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @return The integer value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the value cannot
     *             be parsed as an integer.
     */
    public static int getInt(String key) throws ConfigException {
        try {
            return getConfig().getInt(key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                try {
                    return Integer.parseInt(MxOSConfig.getDefault(key));
                } catch (NumberFormatException e) {
                    logger.warn(key + ": integer conversion error: "
                            + getDefault(key), e);
                    throw ce;
                }
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given key, as an integer. The
     * current default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The integer value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public static int getInt(String key, int def) {
        return getConfig().getInt(key, def);
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as a float.
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The float value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the value cannot
     *             be parsed as a float.
     */
    public static float getFloat(String host, String app, String key)
        throws ConfigException {
        try {
            return getConfig().getFloat(host, app, key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                try {
                    return Float.parseFloat(MxOSConfig.getDefault(key));
                } catch (NumberFormatException e) {
                    logger.warn(key + ": floating point conversion error: "
                            + getDefault(key), e);
                    throw ce;
                }
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given host, application, and key,
     * as a float.
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The float value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public static float getFloat(String host, String app, String key, float def) {
        return getConfig().getFloat(host, app, key, def);
    }

    /**
     * Gets the configuration value with the given key, as a float. The current
     * default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @return The float value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the value cannot
     *             be parsed as a float.
     */
    public static float getFloat(String key) throws ConfigException {
        try {
            return getConfig().getFloat(key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                try {
                    return Float.parseFloat(MxOSConfig.getDefault(key));
                } catch (NumberFormatException e) {
                    logger.warn(key + ": floating point conversion error: "
                            + getDefault(key), e);
                    throw ce;
                }
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the configuration value with the given key, as a float. The current
     * default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The float value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public static float getFloat(String key, float def) {
        return getConfig().getFloat(key, def);
    }

    /**
     * Gets the multi-valued configuration value with the given host,
     * application, and key as a String array.
     * <p/>
     * If a provider doesn't support multi-valued entries or the entry is not
     * multi-valued, then the value is returned in a String array of length one.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The array of values for the configuration item with this key, or
     *         the provided default value if the key does not exist
     * @deprecated Replaced by {@link #getArray(String,String,String,String[])}
     */
    @Deprecated
    public static String[] getArray(String host, String app, String key,
            String def) {
        return getConfig().getArray(host, app, key, def);
    }

    /**
     * Gets the multi-valued configuration value with the given host,
     * application, and key as a String array.
     * <p/>
     * If a provider doesn't support multi-valued entries or the entry is not
     * multi-valued, the value is returned in a String array of length one.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default values.
     * @return The array of values for the configuration item with this key, or
     *         the provided default value if the key does not exist.
     */
    public static String[] getArray(String host, String app, String key,
            String[] def) {
        return getConfig().getArray(host, app, key, def);
    }

    /**
     * Gets the multi-valued configuration value with the given host,
     * application, and key as a String array.
     * <p/>
     * If a provider doesn't support multi-valued entries or the entry is not
     * multi-valued, the value is returned in a String array of length one.
     * <p/>
     * 
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The array of values for the configuration item with this key
     * @throws ConfigException If the key does not exist.
     */
    public static String[] getArray(String host, String app, String key)
        throws ConfigException {
        try {
            return getConfig().getArray(host, app, key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                return Utilities.dsvToArray(MxOSConfig.getDefault(key), '\n',
                        '\\');
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the multi-valued configuration value with the given key, as a
     * String. The current default host and application values are used. Leading
     * and trailing white space is trimmed. If the value is empty, the empty
     * string (rather than null) is returned.
     * <p/>
     * If a provider doesn't support multi-valued entries or the entry is not
     * multi-valued, the value is returned in a String array of length one.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @return The array of values of the configuration item with this key.
     * @throws ConfigException If the key does not exist.
     */
    public static String[] getArray(String key) throws ConfigException {
        try {
            return getConfig().getArray(key);
        } catch (ConfigException ce) {
            if (MxOSConfig.containsDefault(key)) {
                return Utilities.dsvToArray(MxOSConfig.getDefault(key), '\n',
                        '\\');
            } else {
                throw ce;
            }
        }
    }

    /**
     * Gets the multi-valued configuration value with the given key, as a String
     * array. The current default host and application are used. Leading and
     * trailing white space is trimmed. If the value is empty, the empty string
     * (rather than null) is returned.
     * <p/>
     * If a provider doesn't support multi-valued entries or the entry is not
     * multi-valued, the value is returned in a String array of length one.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @param def The default value to use for this key.
     * @return The value of the configuration item with this key.
     * @deprecated Replaced by {@link #getArray(String,String[])}
     */
    @Deprecated
    public static String[] getArray(String key, String def) {
        return getConfig().getArray(key, def);
    }

    /**
     * Gets the multi-valued configuration value with the given key, as a String
     * array. The current default host and application are used. Leading and
     * trailing white space is trimmed. If the value is empty, the empty string
     * (rather than null) is returned.
     * <p/>
     * If a provider doesn't support multi-valued entries or the entry is not
     * multi-valued, the value is returned in a String array of length one.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @param def The default values to use for this key.
     * @return The value of the configuration item with this key.
     */
    public static String[] getArray(String key, String[] def) {
        return getConfig().getArray(key, def);
    }

    /**
     * Returns a value of true if a if the given host, application, and key
     * combination exists.
     *
     * @param host The host associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     *            <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return A value of <code>true</code> if the given key exists.
     */
    public static boolean contains(String host, String app, String key) {
        return getConfig().contains(host, app, key);
    }

    /**
     * Returns a value of true if if the given key exists. The current default
     * host and application values are used.
     *
     * @param key The name of this configuration item.
     * @return boolean - A value of <code>true</code> if the given key exists.
     */
    public static boolean contains(String key) {
        return getConfig().contains(key);
    }

    /**
     * Removes a cached value from the configuration cache, so it can be
     * retrieved from the configuration store next time the host, application,
     * and key combination is requested.
     *
     * @param host The host part of the cache key.
     * @param app The application part of the cache key.
     * @param key The name of the key.
     */
    public static void removeCachedValue(String host, String app, String key) {
        getConfig().removeCachedValue(host, app, key);
    }

    /**
     * 
     * 
     * @return
     */
    public static String[] getExtensionsOptionalCosAttributes() {
        String extAttr = getAndParseExtensionsOptionalCosAttibutes();
        if (null != extAttr && extAttr.indexOf(MxOSConstants.COMMA) != -1) {
            return extAttr.split(MxOSConstants.COMMA);
        } else if (null != extAttr && extAttr.length() > 0) {
            return new String[] { extAttr.trim() };
        }
        return new String[0];
    }

    /**
     * @return the extensionsOptionalCoSAttributes
     */
    private static String getAndParseExtensionsOptionalCosAttibutes() {
        logger.info("Get Optionals Cos Attributes");
        String extAttr = null;
        try {
            final String[] extAttrFrmConf = 
                getArray(Keys.EXTENSIONS_OPTIONAL_COS_ATTRIBUTES);
            if (extAttrFrmConf == null || extAttrFrmConf.length < 1) {
                logger.error(Keys.EXTENSIONS_OPTIONAL_COS_ATTRIBUTES
                        + " key value is null or no any value ");
            }
            String prefix = "";
            for (String extAttribute : extAttrFrmConf) {
                String tmpVal = ignoreServiceName(extAttribute);
                if (null != tmpVal) {
                    extAttr = (null == extAttr) ? (prefix + tmpVal) : (extAttr
                            + prefix + tmpVal);
                    prefix = MxOSConstants.COMMA;
                }
            }
            if (logger.isDebugEnabled()) {
            logger.debug(Keys.EXTENSIONS_OPTIONAL_COS_ATTRIBUTES + ": "
                    + extAttr);
            }
        } catch (ConfigException ce) {
            if (logger.isDebugEnabled()) {
                logger.debug(Keys.EXTENSIONS_OPTIONAL_COS_ATTRIBUTES
                        + " : no such key  ");
            }
        }
        return extAttr;
    }

    /**
     * 
     * @param extAttrFrmConf
     * @return String
     */
    private static String ignoreServiceName(
            String extAttrFrmConf) {
        String[] extAttrFrmConfArr = null;
        if (extAttrFrmConf.indexOf(MxOSConstants.SPACE) != 1) {
            extAttrFrmConfArr = extAttrFrmConf.split(MxOSConstants.SPACE);
            if (extAttrFrmConfArr.length > 1) {
                return extAttrFrmConfArr[1].trim();
            }
        }
        return null;
    }

    /**
     * @return the defaultDirServer
     */
    public static String getDefaultDirectoryServer() {
        return getString(getDefaultHost(), getDefaultApp(),
                Keys.DEFAULT_DIR_SERVER, APP_IMDIRSERV);
    }

    /**
     * @return the defaultSearchDirServer
     */
    public static String getDefaultSearchDirectoryServer() {
        return getString(getDefaultHost(), getDefaultSearchApp(),
                Keys.DEFAULT_DIR_SERVER, APP_IMDIRCACHESERV);
    }
    /**
     * Method to get default cosID used when on create mailbox.
     * @return String defaultCosId
     */
    public static String getDefaultCosId() {
        return getString(Keys.DEFAULT_COS_ID,
                DEFAULT_COS_ID);
    }
    /**
     * Method to get default status used when on create mailbox.
     * @return String defaultMailboxStatus
     */
    public static String getDefaultMailboxStatus() {
        return getString(Keys.DEFAULT_MAILBOX_STATUS,
                DEFAULT_MAILBOX_STATUS);
    }
    /**
     * Method to get default password store type used when on create mailbox.
     * @return String defaultPasswordStoreType
     */
    public static String getDefaultPasswordStoreType() {
        return getString(Keys.DEFAULT_PASSWORD_STORE_TYPE,
                DEFAULT_PASSWORD_STORE_TYPE);
    }
    
    /**
     * Method to get Create Mailbox on MSS if status is Proxy.
     * 
     * @return String createMssMailboxOnProxyStatus
     */
    public static String getCreateMssMailboxOnProxyStatus() {
        return System.getProperty(
                SystemProperty.createMssMailboxOnProxyStatus.name(),
                DEFAULT_CREATE_MSSMAILBOX_ON_PROXY_STATUS);
    }
    
    /**
     * Method to get default preencrypted password store type used when on
     * create mailbox.
     * 
     * @return String defaultPreEncryptedPasswordStoreType
     */
    public static String getDefaultPreEncyrptedPasswordStoreType() {
        return System.getProperty(
                SystemProperty.defaultPreEncyrptedPasswordStoreType.name(),
                DEFAULT_PREENCRYPTED_PASSWORD_STORE_TYPE);
    }

    /**
     * Method to get default CN
     * 
     * @return String defaultCN
     */
    public static String getDefaultCN() {
        return System.getProperty(
                SystemProperty.defaultCN.name(),
                DEFAULT_CN);
    }
    /**
     * Method to get default folders.
     * 
     * @return String getDefaultFolders
     */
    public static List<String> getDefaultFolders() {
        String defaultFolders = System.getProperty(
                SystemProperty.systemFolders.name(), DEFAULT_FOLDERS);
        List<String> systemfolders = new ArrayList<String>();
        if (defaultFolders != null) {
            String[] temp = defaultFolders.split(MxOSConstants.COMMA);
            for (int i = 0; i < temp.length; i++) {
                if(null != temp[i])
                systemfolders.add(temp[i].trim());
            }
        }
        return systemfolders;
    }

    /**
     * Method to get regex validation timeout.
     * 
     * @return String getRegexValidationTimeout
     */
    public static Integer getRegexValidationTimeout() {
        try {
            return Integer.parseInt(System
                    .getProperty(SystemProperty.regexValidationTimeout.name(),
                    DEFAULT_REGEX_VALIDATION_TIMEOUT));
        } catch (Exception e) {
            logger.warn("property"
                    + " 'regexValidationTimeout' not configured properly," +
                    		"considering the default value "
                    + DEFAULT_REGEX_VALIDATION_TIMEOUT);
            return Integer.parseInt(DEFAULT_REGEX_VALIDATION_TIMEOUT);
        }
    }
    /**
     * Method to get KeyA.
     *
     * @return keyB keyA
     */
    public static byte[] getKeyA() {
        return "!C'*a_B0>9PaO: N".getBytes();
    }

    /**
     * Method to return crypto key list from configuration.
     *
     * @return the cryptoKeyList
     * @throws ConfigException ConfigException
     */
    public static List<byte[]> getCryptoKeyList() throws ConfigException {
        ArrayList<byte[]> keyBList = new ArrayList<byte[]>();
        /*
         * String[] list = getArray(Keys.KEY_B_LIST, new String[] {
         * "0,1214567890123456", "1,5678956789543219", "2,4545465456789876",
         * "3,2345678987654323", "4,7897897897897898" });
         */
        String[] list = getArray(Keys.KEY_B_LIST);
        if (list != null) {
            logger.debug("list length" + " : " + list.length);
            if (list.length == CONST_ZERO || list.length >= CONST_HUNDRED) {
                throw new ConfigException(
                        "KeyBs configured is either zero or more than 99.");
            }
        } else {
            throw new ConfigException("Invalid keyBList configured.");
        }

        logger.debug(Keys.KEY_B_LIST + " : " + Arrays.toString(list));

        for (String keyB : list) {
            String[] temp = keyB.split(",");
            if (temp[1].length() != 16) {
                throw new ConfigException("Invalid KeyB configured");
            }
            keyBList.add(temp[1].getBytes());
            logger.debug("KEY_B_Value" + " : " + temp[1].toString());
        }
        return keyBList;
    }

    /**
     * @return the enableSoftDelete
     */
    public static boolean isEnableSoftDelete() {
        return getBoolean(Keys.ENABLE_SOFT_DELETE, CONST_BOOLEAN_TRUE);
    }

    /**
     * @return the maxAddressesForLocalDelivery
     */
    public static int getMaxAddressesForLocalDelivery() {
        Integer value = 10;
        try {
            value = Integer.parseInt(System
                    .getProperty("maxAddAddressesForLocalDelivery"));
        } catch (Exception e) {

        }
        return value;
    }

    /**
     * @return the maxAllowedSendersList
     */
    public static int getMaxAllowedSendersList() {
        Integer value = 10;
        try {
            value = Integer.parseInt(System
                    .getProperty("maxAddAllowedSendersList"));
        } catch (Exception e) {

        }
        return value;
    }

    /**
     * @return the maxBlockedSendersList
     */
    public static int getMaxBlockedSendersList() {
        Integer value = 10;
        try {
            value = Integer.parseInt(System
                    .getProperty("maxAddBlockedSendersList"));
        } catch (Exception e) {

        }
        return value;
    }
    
    /**
     * Get the maximum admin blocked senders list property
     * 
     * @return the maxAdminBlockedSendersList
     */
    public static int getMaxAdminBlockedSendersList() {
        Integer value = 10;
        try {
            value = Integer.parseInt(System
                    .getProperty("maxAddAdminBlockedSendersList"));
        } catch (Exception e) {
            logger.error("maxAddAdminBlockedSendersList not configured-picking up default value");
        }
        return value;
    }
    
    /**
     * Get the maximum admin approved senders list property
     * 
     * @return the maxAdminApprovedSendersList
     */
    public static int getMaxAdminApprovedSendersList() {
        Integer value = 10;
        try {
            value = Integer.parseInt(System
                    .getProperty("maxAddAdminApprovedSendersList"));
        } catch (Exception e) {
            logger.error("maxAddAdminApprovedSendersList not configured-picking up default value");
        }
        return value;
    }

    /**
     * @return the maxSieveFiltersBlockedSenders
     */
    public static int getMaxSieveFiltersBlockedSenders() {
        Integer value = 10;
        try {
            value = Integer.parseInt(System.getProperty("maxAddSieveBlockedSenders"));
        } catch (Exception e) {

        }
        return value;
    }

    /**
     * @return the getCosObjectClasses
     */
    public static String[] getCosObjectClasses() {
        String[] defaultValues = System.getProperty(
                SystemProperty.ldapCosObjectClasses.name()).split(",");
        return defaultValues;
    }
    /**
     * @return the mailboxObjectClasses
     */
    public static String[] getMailboxObjectClasses() {
        String[] defaultValues =
                new String[] { "top", "person", "mailuser", "mailuserprefs",
                        "maillocaluserprefs", "msguser", "msguserprefs" };
        String[] confValues =
                getArray(Keys.MAILBOX_OBJECT_CLASSES, defaultValues);
        if (confValues == null || confValues.length == CONST_ZERO) {
            return defaultValues;
        } else {
            return confValues;
        }
    }
    
    /**
     * @return the mailboxCustomFieldsClass
     */
    public static String getMailboxCustomFieldsClass() {
        return getString(Keys.MAILBOX_CUSTOM_FIELDS_CLASS,
                DEFAULT_MAILBOX_CUSTOM_FIELDS_CLASS);
    }
    
    /**
     * @return the domainCustomFieldsClass
     */
    public static String getDomainCustomFieldsClass() {
        return getString(Keys.DOMAIN_CUSTOM_FIELDS_CLASS,
                DEFAULT_DOMAIN_CUSTOM_FIELDS_CLASS);
    }

    /**
     * @return the parentDomainObjectClasses
     */
    public static String[] getParentDomainObjectClasses() {
        String[] defaultValues = new String[] { "top", "person" };
        String[] confValues =
                getArray(Keys.PARENT_DOMAIN_OBJECT_CLASSES, defaultValues);
        if (confValues == null || confValues.length == CONST_ZERO) {
            return defaultValues;
        } else {
            return confValues;
        }
    }

    /**
     * @return the childDomainObjectClasses
     */
    public static String[] getChildDomainObjectClasses() {
        String[] defaultValues =
                new String[] { "top", "domain", "mailDomain", "organization" };
        String[] confValues =
                getArray(Keys.CHILD_DOMAIN_OBJECT_CLASSES, defaultValues);
        if (confValues == null || confValues.length == CONST_ZERO) {
            return defaultValues;
        } else {
            return confValues;
        }
    }

    /**
     * @return the contextName
     */
    public static String getContextType() {
        return getString(Keys.MXOS_CONTEXT_TYPE,
                DEFAULT_MXOS_CONTEXT_TYPE);
    }

    /**
     * @return the statsEnabled
     */
    public static boolean isStatsEnabled() {
        return getBoolean(Keys.STATS_ENABLED, CONST_BOOLEAN_TRUE);
    }
    
    /**
     * @return the jmx snmp agent path
     */
    public static String getSNMPAgentPath() {
    	return System.getProperty("snmpAgentPath");
    }

    /**
     * @return the statsNumEvents
     */
    public static int getStatsNumEvents() {
        return getInt(Keys.STATS_NUM_EVENTS, DEFAULT_STATS_NUM_EVENTS);
    }

    /**
     * @return the statsTimeDuration
     */
    public static int getStatsTimeDuration() {
        return getInt(Keys.STATS_TIME_DURATION, DEFAULT_STATS_TIME_DURATION);
    }

    /**
     * @return the searchMaxRows
     */
    public static int getSearchMaxRows() {
        return getInt(Keys.SEARCH_MAX_ROWS, DEFAULT_SEARCH_MAX_ROWS);
    }

    /**
     * @return the searchMaxTimeOut
     */
    public static int getSearchMaxTimeOut() {
        return getInt(Keys.SEARCH_MAX_TIME_OUT, DEFAULT_SEARCH_MAX_TIME_OUT);
    }
    
    /**
     * @return the badPasswordDelay
     */
    public static int getBadPasswordDelay() {
        return getInt(Keys.SEARCH_BAD_PASSWORD_DELAY, DEFAULT_BAD_PASSWORD_DELAY_SECONDS);
    }
    
    /**
     * @return the badPasswordWindow
     */
    public static int getBadPasswordWindow() {
        return getInt(Keys.SEARCH_BAD_PASSWORD_WINDOW, DEFAULT_BAD_PASSWORD_WINDOW_MINS);
    }
    
    /**
     * @return the Autheticate Lock Timeout
     */
    public static int getAutheticationLockTimeout() {
        return getInt(Keys.AUTHENTICATION_LOCK_TIMEOUT,
                DEFAULT_AUTHENTICATION_LOCK_TIMEOUT);
    }
    
    /**
     * @return the list of Message Store host names
     */
    public static String[] getMessageStoreHostNames() {
        return getArray(Keys.MESSAGE_STORE_HOSTS);
    }

    /**
     * @return the configHost
     */
    public static String getConfigHost() {
        return System.getProperty("configHost");
    }

    /**
     * @return the configPort
     */
    public static String getConfigPort() {
        return System.getProperty("configPort");
    }

    /**
     * @return the defaultApp
     */
    public static String getDefaultApp() {
        return System.getProperty("defaultApp");
    }

    /**
     * @return the defaultSearchApp
     */
    public static String getDefaultSearchApp() {
        return System.getProperty("defaultSearchApp");
    }

    /**
     * @return the defaultHost
     */
    public static String getDefaultHost() {
        return Config.getDefaultHost();
    }

    /**
     * @return the masterServerRoleEnabled
     */
    public static Boolean isMasterServerRoleEnabled() {
        String value = System
                .getProperty(SystemProperty.masterServerRoleEnabled.name());
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return false;
        }
    }

    /**
     * @return the msgBodyBlobBase64Encoded
     */
    public static Boolean isMsgBodyBlobBase64Encoded() {
        String value = System
                .getProperty(SystemProperty.msgBodyBlobBase64Encoded.name());
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return false;
        }
    }
    
    /**
     * @return the msgSubjectBase64Encoded
     */
    public static Boolean isMsgSubjectBase64Encoded() {
        String value = System
                .getProperty(SystemProperty.msgSubjectBase64Encoded.name());
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return false;
        }
    }
    
    /**
     * @return the intermailDir
     */
    public static String getIntermailDir() {
        return System.getProperty("intermailDir");
    }
    
    /**
     * @return the configConnectAttempts
     */
    public static int getConfigConnectAttempts() {
        return Integer.parseInt(System.getProperty("configConnectAttempts"));
    }

    /**
     * @return the configConnectInterval
     */
    public static int getConfigConnectInterval() {
        return Integer.parseInt(System.getProperty("configConnectInterval"));
    }

    /**
     * @return the groupMailboxEnabled
     */
    public static Boolean isGroupMailboxEnabled() {
        String value = System.getProperty(SystemProperty.groupMailboxEnabled
                .name());
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return false;
        }
    }
    
    /**
     * @return the includeGroupAdminToAllocations
     */
    public static Boolean isGroupAdminIncluded() {
        String value = System.getProperty(SystemProperty.includeGroupAdminToAllocations
                .name());
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return false;
        }
    }

    /**
     * @return the getMailboxReturnLdapAttributes
     */
    public static String getMailboxReturnLdapAttributes() {
        if (System.getProperty(SystemProperty.getMailboxReturnLdapAttributes
                .name()) != null
                && !System.getProperty(
                        SystemProperty.getMailboxReturnLdapAttributes.name())
                        .equals("")) {
            return System
                    .getProperty(SystemProperty.getMailboxReturnLdapAttributes
                            .name());
        } else {
            return DEFAULT_MAILBOX_ATTR;
        }
    }

    /**
     * @return the searchMaxRows
     */
    public static int getMetaMaxConnections() {
        return getInt(Keys.META_MAX_CONNECTIONS, DEFAULT_META_MAX_CONNECTIONS);
    }

    /**
     * @return the searchMaxRows
     */
    public static String getMetaBackend() {
        return getString(Keys.META_BACKEND, DEFAULT_META_BACKEND);
    }

    /**
     * @return the addressBookBackend
     */
    public static String getAddressBookBackend() {
        return getString(Keys.ADDRESS_BOOK_BACKEND, DEFAULT_ADDRESS_BOOK_BACKEND);
    }

    /**
     * @return the tasksDBbackend
     */
    public static String getTasksBackend() {
        return getString(Keys.TASKS_BACKEND, DEFAULT_TASKS_BACKEND);
    }

    /**
     * @return the searchMaxRows
     */
    public static String getProvisioningBackend() {
        return getString(Keys.MAILBOX_BACKEND,
                DEFAULT_MAILBOX_BACKEND);
    }

    /**
     * @return the searchMaxRows
     */
    public static String getBlobBackend() {
        return getString(Keys.BLOB_BACKEND, DEFAULT_BLOB_BACKEND);
    }

    /**
     * @return the searchMaxRows
     */
    public static String getNotifyBackend() {
        return System.getProperty(SystemProperty.notifyBackend.name(),
                DEFAULT_NOTIFY_BACKEND);
    }
    /**
     * @return the searchMaxRows
     */
    public static boolean isTrustedClient() {
        return getBoolean(Keys.TRUSTED_CLIENT, DEFAULT_TRUSTED_CLIENT);
    }

    /**
     * @return the searchMaxRows
     */    
    public static int getMessageType() {
        return getInt(Keys.MESSAGE_TYPE, DEFAULT_MESSAGE_TYPE);
    }

    /**
     * @return the searchMaxRows
     */
    public static int getMessagePriority() {
        return getInt(Keys.MESSAGE_PRIORITY, DEFAULT_MESSAGE_PRIORITY);
    }

    /**
     * @return the searchMaxRows
     */
    public static int getDeliverNDR() {
        return getInt(Keys.DELIVER_NDR, DEFAULT_DELIVER_NDR);
    }

    /**
     * @return the searchMaxRows
     */
    public static String getCompressionType() {
        return getString(Keys.COMPRESSION_TYPE, DEFAULT_COMPRESSION_TYPE);
    }

    /**
     * @return the searchMaxRows
     */
    public static int getMessageExpSecOffset() {
        return getInt(Keys.MESSAGE_EXP_TIME_OFFSET_IN_SEC,
                DEFAULT_MESSAGE_EXP_TIME_OFFSET_IN_SEC);
    }

    /**
     * @return the SystemFolders
     */
    public static String[] getSystemFolders() {
        return getArray(Keys.SYSTEM_FOLDRS, DEFAULT_SYSTEM_FOLDERS);
    }

    /**
     * @return the PosixmaxBuckets
     */
    public static int getPosixmaxBuckets() {
        return getInt(Keys.POSIX_MAX_BUCKETS, DEFAULT_POSIX_MAX_BUCKETS);
    }

    /**
     * @return the PosixDbRootFolder
     */
    public static String getPosixDbRootFolder() {
        return getString(Keys.POSIX_DB_ROOT_FOLDER,
                DEFAULT_POSIX_DB_ROOT_FOLDER);
    }

    /**
     * @return the LoadRulesOrder
     */
    public static String getLoadRulesOrder() {
        return System.getProperty(SystemProperty.loadRulesOrder.name());
    }

    /**
     * @return the MigratedDomains
     */
    public static ArrayList<String> getMigratedDomains() {
        if (null == migratedDomains) {
            migratedDomains = new ArrayList<String>();
            String value = System.getProperty(SystemProperty.migratedDomains
                    .name());
            if (null != value) {
                String[] domains = value.split(",");
                for (String domain : domains) {
                    migratedDomains.add(domain);
                }
            }
        }
        return migratedDomains;
    }

    /**
     * @return the DeploymentMode
     */
    public static DeploymentMode getDeploymentMode() {
        return DeploymentMode.getMode(System
                .getProperty(SystemProperty.deploymentMode.name()));
    }

    /**
     * @return the MssVersion
     */
    public static String getMssVersion() {
        try {
            return System.getProperty(SystemProperty.mssVersion.name());
        } catch (Exception e) {
            return "4";
        }
    }

    /**
     * @return the searchMaxRows
     */
    public static String getLdapCosBaseDn() {
        String cosDn = System.getProperty(SystemProperty.ldapCosBaseDn.name());
        if (cosDn == null || cosDn.equals("")) {
            // Default CosDn
            cosDn = DEFAULT_COS_DN;
        }
        return cosDn;
    }

    /**
     * @return the ldapConnRetryInterval
     */
    public static int getLdapConnRetryInterval() {
        int ldapConnRetryInterval = 1000;
        try {
            ldapConnRetryInterval = Integer.parseInt(System
                    .getProperty(SystemProperty.ldapConnRetryInterval.name()));
        } catch (Exception e) {
            logger.warn("Invalid ldapConnRetryInterval configured", e);
        }
        logger.info("ldapConnRetryInterval = " + ldapConnRetryInterval);
        return ldapConnRetryInterval;
    }

    /**
     * @return the ldapPoolMaxSize
     */
    public static int getLdapPoolMaxSize() {
        int ldapPoolMaxSize = 10;
        try {
            ldapPoolMaxSize = Integer.parseInt(System
                    .getProperty(SystemProperty.ldapPoolMaxSize.name()));
        } catch (Exception e) {
            logger.warn("Invalid ldapPoolMaxSize configured", e);
        }
        logger.info("ldapPoolMaxSize = " + ldapPoolMaxSize);
        return ldapPoolMaxSize;
    }

    /**
     * @return the ldapConnectTimeout
     */
    public static int getLdapConnectTimeout() {
        int ldapConnectTimeout = 500;
        try {
            ldapConnectTimeout = Integer.parseInt(System
                    .getProperty(SystemProperty.ldapConnectTimeout.name()));
        } catch (Exception e) {
            logger.warn("Invalid ldapConnectTimeout configured", e);
        }
        logger.info("ldapConnectTimeout = " + ldapConnectTimeout);
        return ldapConnectTimeout;
    }

    /**
     * @return the mailApiAdaptorUrl
     */
    public static String getMailApiAdaptorUrl() {
        String defaultValue = "http://localhost:8080/maa-sim/mailbox";
        String confValue = getString(Keys.MAIL_API_ADAPTOR_URL, defaultValue);
        if (confValue == null || confValue.equals("")) {
            if (logger.isDebugEnabled()) {
                logger.debug(
                        Keys.MAIL_API_ADAPTOR_URL + " --> " + defaultValue);
            }
            return defaultValue;
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug(Keys.MAIL_API_ADAPTOR_URL + " --> " + confValue);
            }
            return confValue;
        }
    }

    /**
     * @return the mailApiAdaptorLogin
     */
    public static String getMailApiAdaptorLogin() {
        String defaultValue = "applogin";
        String confValue = getString(Keys.MAIL_API_ADAPTOR_LOGIN,
                defaultValue);
        if (confValue == null || confValue.equals("")) {
            if (logger.isDebugEnabled()) {
                logger.debug(Keys.MAIL_API_ADAPTOR_LOGIN + " --> "
                        + defaultValue);
            }
            return defaultValue;
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug(
                        Keys.MAIL_API_ADAPTOR_LOGIN + " --> " + confValue);
            }
            return confValue;
        }
    }

    /**
     * @return the mailApiAdaptorPassword
     */
    public static String getMailApiAdaptorPassword() {
        String defaultValue = "apppassword";
        String confValue = getString(
                Keys.MAIL_API_ADAPTOR_PASSWORD, defaultValue);
        if (confValue == null || confValue.equals("")) {
            if (logger.isDebugEnabled()) {
                logger.debug(Keys.MAIL_API_ADAPTOR_PASSWORD + " --> "
                        + defaultValue);
            }
            return defaultValue;
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug(Keys.MAIL_API_ADAPTOR_PASSWORD + " --> "
                        + confValue);
            }
            return confValue;
        }
    }

    /**
     * @return the bgcPrimaryDomains
     */
    public static String[] getBgcPrimaryDomains() {
        String[] defaultValues = new String[] {"skynetmail.be",
            "belgacom.net", "belgacom.net.sky.be", "belgacomtv.net",
            "busmail.net", "cbcfree.net", "compaqnet.be",
            "dexiainvestor.net", "e2e.proximus.be", "freeciel.skynet.be",
            "kbcmail.net", "kidcity.be", "kidcity.fr", "mmsa.proximus.be",
            "mmsa.e2e.proximus.be", "mmsa.pre.proximus.be",
            "mmsa.tst.proximus.be", "photohall.skynet.be",
            "pre.proximus.be", "proximus.be", "reddevils-fanclub.be",
            "rsca.com,selexion.net", "skybel.net,skynet.be",
            "skynet.be.sky.be", "swing.be", "tst.proximus.be" };
        String[] confValues = getArray(Keys.BGC_PRIMARY_DOMAINS, defaultValues);
        if (confValues == null || confValues.length == CONST_ZERO) {
            if (logger.isDebugEnabled()) {
                logger.debug(Keys.BGC_PRIMARY_DOMAINS + " --> "
                        + Arrays.toString(defaultValues));
            }
            return defaultValues;
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug(Keys.BGC_PRIMARY_DOMAINS + " --> "
                        + Arrays.toString(confValues));
            }
            return confValues;
        }
    }

    public static String[] getClusterHashMap() {

        try {
            String[] clusterHashmap = getArray(IntermailConfig.CLUSTER_HASH_MAP);

            if (clusterHashmap == null || clusterHashmap.length < 1) {
                logger.error(IntermailConfig.CLUSTER_HASH_MAP
                        + " key value is null or no any value ");
            }
            if (logger.isDebugEnabled()) {
                logger.debug(IntermailConfig.CLUSTER_HASH_MAP + " --> "
                        + Arrays.toString(clusterHashmap));
            }
            return clusterHashmap;
        } catch (ConfigException ce) {
            logger.error(IntermailConfig.CLUSTER_HASH_MAP + " : no such key  ");
        }
        return null;
    }

    public static boolean getDisableRedirectToSurrogateMSS() {
        
        boolean disableRedirectToSurrogateMSS = getBoolean(
                IntermailConfig.DISABLE_REDIRECT_TO_SURROGATE_MSS, false);
        
        if (logger.isDebugEnabled()) {
            logger.debug(IntermailConfig.DISABLE_REDIRECT_TO_SURROGATE_MSS + " --> "
                    + disableRedirectToSurrogateMSS);
        }
        return disableRedirectToSurrogateMSS;
        
    } 
    
    public static boolean getEnableActiveList() {
        
        boolean enableActiveList = getBoolean(
                IntermailConfig.ENABLE_ACTIVELIST, false);
        
        if (logger.isDebugEnabled()) {
            logger.debug(IntermailConfig.ENABLE_ACTIVELIST + " --> "
                    + enableActiveList);
        }
        return enableActiveList;
        
    }    
    
    public static boolean isNotifyHttpConPoolEnabled(){
        final String isNotifyEnabled = System.getProperty(
                SystemProperty.notifyPublishHttpConnectionPoolEnabled.name(), "true");

        return Boolean.valueOf(isNotifyEnabled);
    }
    
    /**
     * @return Size of HttpClientPool used for sending Notify HttpPost to IMAPServ
     */
    public static int getNotifyHttpPoolMaxSize() {
        final int size = getIntProp(
                SystemProperty.notifyPublishHttpPoolMaxSize.name(),
                NOTIFY_HTTPPOOL_MAXSIZE);

        if (size < 0) {
            logger.warn("Negative value configured for poolsize key: notifyPublishHttpPoolMaxSize.");
            throw new NumberFormatException();
        }

        return size;
    }
    
    /**
     * @return Timeout value in milliseconds
     */
    public static int getNotifyHttpConTimeout() {
        final int timeout = getIntProp(
                SystemProperty.notifyPublishHttpConTimeoutMS.name(),
                NOTIFY_HTTPCON_TIMEOUT_DEF);

        if (timeout < 0) {
            logger.warn("Negative value configured for timeout key: notifyPublishHttpConTimeoutMS.");
            throw new NumberFormatException();
        }

        return timeout;
    }
    
    /**
     * @return Timeout value in milliseconds
     */
    public static int getNotifyHttpReadTimeout(){
        final int timeout = getIntProp(
                SystemProperty.notifyPublishHttpReadTimeout.name(),
                NOTIFY_HTTPREAD_TIMEOUT_DEF);

        if (timeout < 0) {
            logger.warn("Negative value configured for timeout key: notifyPublishHttpReadTimeout.");
            throw new NumberFormatException();
        }

        return timeout;
    }
    
    /**
     * @return Size of thread pool used for sending Notify request to IMAPServ
     */
    public static int getNotifyThreadPoolMaxSize(){
        final int size = getIntProp(
                SystemProperty.notifyPublishThreadPoolMaxSize.name(),
                NOTIFY_THREADPOOL_MAXSIZE);

        if (size < 0) {
            logger.warn("Negative value configured for timeout key: notifyPublishThreadPoolMaxSize.");
            throw new NumberFormatException();
        }

        return size;
    }
    
    /**
     * Reads an Integer property from loaded System Properties
     * 
     * @param key - property key
     * @param defaultVal - default value in case key not found, pass "null" if
     *            no default
     * @return int
     */
    private static int getIntProp(String key, String defaultVal) {
        final String valStr = System.getProperty(key, defaultVal);
        try {
            return Integer.parseInt(valStr);
        } catch (NumberFormatException e) {
            logger.warn("Integer conversion error for  key: " + key, e);
            throw e;
        }
    }
}
