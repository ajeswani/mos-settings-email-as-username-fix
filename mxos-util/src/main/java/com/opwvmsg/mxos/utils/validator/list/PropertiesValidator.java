/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JacksonSchema;

/**
 * PropertiesValidator class for properties validator.
 *
 * @author mxos-dev
 * 
 */
public class PropertiesValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -7054176202856839164L;
    private static Logger LOG = Logger.getLogger(PropertiesValidator.class);
    public static final String PROPERTY = "properties";
    protected Map<String, JacksonSchema> schemas;

    /**
     * default constructor.
     *
     * @param propertiesNode
     *            propertiesNode
     */
    public PropertiesValidator(JsonNode propertiesNode) {
        this.schemas = new HashMap<String, JacksonSchema>();
        for (Iterator<String> it =
            propertiesNode.getFieldNames(); it.hasNext();) {
            String pname = it.next();
            this.schemas.put(pname,
                    new JacksonSchema(propertiesNode.get(pname)));
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        for (String key : this.schemas.keySet()) {
            JacksonSchema propertySchema = this.schemas.get(key);
            JsonNode propertyNode = node.get(key);
            if (propertyNode != null) {
                errors.addAll(propertySchema.validate(propertyNode, node, at
                        + "." + key));
            } else if (!propertySchema.isOptional()) {
                errors.add(at + "." + key
                        + ": is missing and it is not optional");
            }
        }

        return errors;
    }
}
