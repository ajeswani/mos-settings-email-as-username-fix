/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.misc;

/**
 * Singleton class to store overall request summary stats.
 *
 * @author ajeswani
 *
 */
public enum RequestStats {

    INSTANCE;

    private long min = Long.MAX_VALUE;
    private long max = Long.MIN_VALUE;
    private long avg;
    private long count;

    /**
     * Method to get count.
     */
    public long getMin() {
        return (min == Long.MAX_VALUE ? 0 : min);
    }

    /**
     * Method to get count.
     *
     * @param min Long
     * @param call Long
     */
    public void setMin(long min, long calls) {
        if (min < this.min && calls > 0) {
            this.min = min;
        }
    }

    /**
     * Method to get count.
     */
    public long getMax() {
        return (max == Long.MIN_VALUE ? 0 : max);
    }

    /**
     * Method to set count.
     *
     * @param max Long
     * @param calls Long
     */
    public void setMax(long max, long calls) {
        if (max > this.max) {
            this.max = max;
        }
    }

    /**
     * Method to get count.
     */
    public long getAvg() {
        return avg;
    }

    /**
     * Method to set count.
     *
     * @param count
     */
    public void setAvg(long tot, long calls) {
        if (calls > 0) {
            this.avg = (this.avg * (count - calls) + tot) / (count);
        }
    }

    /**
     * Method to get count.
     */
    public long getCount() {
        return count;
    }

    /**
     * Method to get count.
     *
     * @param count Long
     */
    public void setCount(long count) {
        this.count += count;
    }

    /**
     * Method to reset min.
     */
    public void resetMin() {
        this.min = Long.MAX_VALUE;
    }

    /**
     * Method to reset max.
     */
    public void resetMax() {
        this.max = Long.MIN_VALUE;
    }
}
