/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * FolderDepthValidator class for folder Depth validation.
 * 
 * @author mxos-dev
 * 
 */
public class FolderDepthValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -6065577788738619244L;
    private static Logger logger = Logger.getLogger(FolderDepthValidator.class);
    public static final String FOLDER_DEPTH_STRING = "folderDepth";
    private Number folderDepth;

    /**
     * default constructor.
     * 
     * @param maximumNode maximumNode
     */
    public FolderDepthValidator(JsonNode maximumNode) {
        if ((maximumNode != null) && (maximumNode.isNumber())) {
            this.folderDepth = maximumNode.getNumberValue();
        }
    }

    /**
     * validate method.
     * 
     * @param node node
     * @param at at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        logger.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     * 
     * @param node node
     * @param parent parent
     * @param at at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        logger.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();
        if (this.folderDepth == null) {
            return errors;
        }
        if (null != node && null != node.getTextValue()
                && node.getTextValue().length() > 0) {
            int value = 0;
            String[] strArray = node.getTextValue().split("/");
            value = strArray.length;
            boolean greaterThanMax = false;
            if (value > this.folderDepth.intValue()) {
                greaterThanMax = true;
            }
            if ((greaterThanMax)) {
                errors.add(at + ": must have a folderDepth value of "
                        + this.folderDepth);
            }
        }
        return errors;
    }
}
