/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * MinItemsValidator class for minimum items validator.
 *
 * @author mxos-dev
 * 
 */
public class MinItemsValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = 5843869888459032277L;
    private static Logger LOG = Logger.getLogger(MinItemsValidator.class);
    public static final String PROPERTY = "minItems";
    private int min = 0;

    /**
     * default constructor.
     *
     * @param minItemsNode
     *            minItemsNode
     */
    public MinItemsValidator(JsonNode minItemsNode) {
        if (minItemsNode.isIntegralNumber()) {
            this.min = minItemsNode.getIntValue();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");

        List<String> errors = new ArrayList<String>();

        if ((node.isArray()) && (node.size() < this.min)) {
            errors.add(at + ": there must be a minimum of " + this.min
                    + " items in the array");
        }

        return errors;
    }
}
