/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.utils.datastore;

import java.util.List;

public interface IDataStoreMultiMap<K, V> {
    
    /**
     * Stores an entry with list-of-values into a cluster wide distributed map
     * instance (with the specified name).
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key with which the specified value is to be associated.
     * @param value - value to be associated with the specified key.
     */
    void putEntry(String mapId, K key, V value);

    /**
     * Get whole list of values associated to a given key stored in the
     * distributed map.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key with which the specified value is to be associated.
     * @return List of values mapped to given key, or null if no mapping found.
     */
    List<V> get(String mapId, K key);

    /**
     * Removes the key and all mappings associated to it from distributed map.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key with which the specified value is to be associated.
     * @return the previous value associated with key, or null if there was no
     *         mapping for key.
     */
    List<V> remove(String mapId, K key);
    
    /**
     * Removes given value mapping for the given key.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key with which the specified value is to be associated.
     * @param value - value to be associated with the specified key.
     * @return true if the size of the multimap changed after the remove
     *         operation, false otherwise.
     */
    boolean removeEntry(String mapId, K key, V value);
    
    /**
     * Returns whether the multimap contains an entry with the key.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key with which the specified value is to be associated.
     * @return true if the multimap contains an entry with the key, false otherwise.
     */
    boolean containsKey(String mapId, K key);
    
    /**
     * Returns whether the multimap contains the given key-value pair.
     * 
     * @param mapId - Unique name or Id of Distributed map.
     * @param key - key with which the specified value is to be associated.
     * @param value - value to be associated with the specified key.
     * @return - true if the multimap contains the key-value pair, false otherwise.
     */
    boolean containsEntry(String mapId, K key, V value);
    
    /**
     * Removes all mappings from this multimap.
     * 
     * @param mapId - Name or Id of Distributed map.
     */
    void clear(String mapId);
    
    /**
     *  Returns the number of key-value pairs in the multimap.
     * 
     * @param mapId - Name or Id of Distributed map.
     * @return
     */
    int size(String mapId);
}
