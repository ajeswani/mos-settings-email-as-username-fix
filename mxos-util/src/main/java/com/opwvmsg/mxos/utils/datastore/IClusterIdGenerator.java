/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.utils.datastore;

/**
 * Cluster-wide unique id generator.
 * 
 * @author 
 */
public interface IClusterIdGenerator {

    /**
     * Generates and returns cluster-wide unique id. Generated ids are
     * guaranteed to be unique for the entire cluster as long as the cluster is
     * live. If the cluster restarts then id generation will start from 0.
     * 
     * @return  cluster-wide unique id.
     */
    long newId();

}
