/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * EmptyValueValidator class for empty value validator.
 *
 * @author mxos-dev
 * 
 */
public class EmptyAllowedValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = 44234300749213244L;
    private static Logger LOG = Logger.getLogger(EmptyAllowedValidator.class);
    public static final String PROPERTY = "emptyAllowed";
    protected String value;
    static enum booleanType { yes, no };

    /**
     * default constructor.
     *
     * @param patternNode
     *            patternNode
     */
    public EmptyAllowedValidator(JsonNode patternNode) {
        this.value = booleanType.no.name();
        if ((patternNode != null) && (patternNode.isTextual())) {
            this.value = booleanType.valueOf(
                    patternNode.getTextValue()).name();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();
        try {
            String data = node.getTextValue();
            if (data == null || data.equals("")) {
                // case emptyValue : "no"
                if (this.value.equalsIgnoreCase(booleanType.no.name())) {
                    errors.add(at + "Attribute " + data + "is not valid");                    
                }
            }
        } catch (Exception pse) {
            LOG.error("Failed to apply emptyValue check on " + at, pse);
        }
        return errors;
    }
}
