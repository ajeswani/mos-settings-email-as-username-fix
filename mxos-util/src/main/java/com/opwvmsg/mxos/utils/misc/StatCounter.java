/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.utils.misc;

import java.util.EnumMap;
import java.util.Iterator;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.Operation;

/**
 * Class for storing and logging various response times and counts for any
 * statEvent. The class also has getter methods for getting the statistics.
 *
 * @author mxos-dev
 **/
public class StatCounter {
    private EnumMap<Operation, Statistics> serviceOprMap;
    private ServiceEnum statName;

    /**
     * Constructor.
     *
     * @param statName StatName for which this counter will be used.
     */
    public StatCounter(ServiceEnum statName) {
        this.setStatName(statName);
        serviceOprMap = new EnumMap<Operation, Statistics>(Operation.class);
    }

    /**
     * Method to update the Counter.
     *
     * @param statEvent StatEvent containing the statName, responce time and
     *            status.The existing counter will be updated with this new
     *            statEvent.
     */
    public void updateCounter(StatEvent statEvent) {
        setStatName(statEvent.getName());
        if (!serviceOprMap.containsKey(statEvent.getOperation())) {
            serviceOprMap.put(statEvent.getOperation(), new Statistics(
                    statEvent));
        } else {
            serviceOprMap.get(statEvent.getOperation()).updateStats(statEvent);
        }
    }

    /**
     * Method to print and reset the Counters after each statsIntervel in
     * seconds as set in config.db.
     */
    public void printAndResetCounters() {
        Iterator<Operation> keySetIterator = serviceOprMap.keySet().iterator();
        while (keySetIterator.hasNext()) {
            Operation tmpOpr = keySetIterator.next();
            serviceOprMap.get(tmpOpr).printAndResetStats();
        }
    }

    /**
     * Method to set stat name
     */
    public void setStatName(ServiceEnum statName) {
        this.statName = statName;
    }

    /**
     * Method to get stat name
     */
    public ServiceEnum getStatName() {
        return statName;
    }
}
