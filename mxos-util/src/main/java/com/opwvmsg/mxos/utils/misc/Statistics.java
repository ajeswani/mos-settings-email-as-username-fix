package com.opwvmsg.mxos.utils.misc;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.jmx.counter.RequestStats;

public class Statistics {
    private long min;
    private long max;
    private long err;
    private long timeout;
    private long tot;
    private long avg;
    private long calls;
    private Operation serviceOpr;
    private ServiceEnum statName;
    private final String DUMP_FMT = " %s %s %d/%d/%d ns %d/%d/%d ";
    private static Logger logger = Logger.getLogger(Statistics.class);

    public Statistics(StatEvent statEvent) {
        serviceOpr = statEvent.getOperation();
        statName = statEvent.getName();
        updateStats(statEvent);
    }

    public void updateStats(StatEvent statEvent) {
        calls++;
        if (statEvent.getStatus() == StatStatus.fail) {
            err++;
        } else if (statEvent.getStatus() == StatStatus.timeout) {
            timeout++;
        }
        long duration = statEvent.getTime();

        if (calls == 1) {
            min = duration;
            max = duration;
        }
        if (duration < min) {
            min = duration;
        }
        if (duration > max) {
            max = duration;
        }
        tot += duration;

    }

    public long getMin() {
        return min;
    }

    /**
     * Method to get the maximum response time for the counter.
     * 
     * @return max maximum response time for the counter.
     */
    public long getMax() {
        return max;
    }

    /**
     * Method to get the average response time for the counter.
     * 
     * @return avg average response time for the counter.
     */
    public long getAvg() {
        avg = 0;
        if (calls > 0) {
            avg = tot / calls;
        }
        return avg;
    }

    /**
     * Method to get the error count for the counter.
     * 
     * @return err error count for the counter.
     */
    public long getErr() {
        return err;
    }

    /**
     * Method to get the timeout count for the counter.
     * 
     * @return timeout timeout count for the counter.
     */
    public long getTimeOut() {
        return timeout;
    }

    /**
     * Method to get the count for the operation which are successfully
     * executed.
     * 
     * @return pass number of successful operations.
     */
    public long getPass() {
        long pass = tot - timeout - err;
        return pass;
    }

    /**
     * Method to get the total count for the operations for this counter.
     * 
     * @return calls number of total operations.
     */
    public long getCount() {
        return calls;
    }

    /**
     * Method to get the formatted string containing the stats for an event.
     * 
     * @return formatted string containing the stats for an event.
     */
    private String getStatsSummary() {
        return String.format(DUMP_FMT, statName.name(), serviceOpr.name(), min,
                max, getAvg(), err, timeout, calls);
    }

    private void updateJMXStats() {
        RequestStats reqStats = RequestStats.INSTANCE;

        // count needs to be set first
        reqStats.setCount(calls);
        reqStats.setMin(min, calls);
        reqStats.setMax(max, calls);
        reqStats.setAvg(tot, calls);
    }

    public void printAndResetStats() {
        updateJMXStats();
        logger.info(getStatsSummary());
        min = 0;
        max = 0;
        err = 0;
        avg = 0;
        calls = 0;
        timeout = 0;
        tot = 0;
    }
}
