/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaException;
import com.opwvmsg.mxos.utils.validator.json.schema.TYPE;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JacksonSchema;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.TYPEFactory;

/**
 * UnionTypeValidator class for Union type validator.
 *
 * @author mxos-dev
 * 
 */
public class UnionTypeValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -7954619972059351060L;
    private static Logger LOG = Logger.getLogger(UnionTypeValidator.class);
    public static final String PROPERTY = "type";
    protected List<JSONValidator> schemas;
    private String error;

    /**
     * default constructor.
     *
     * @param typeNode
     *            typeNode
     */
    public UnionTypeValidator(JsonNode typeNode) {
        this.schemas = new ArrayList<JSONValidator>();
        String sep = "";
        this.error = "[";

        if (!typeNode.isArray()) {
            throw new JSONSchemaException("Expected array for type property"
                    + " on Union Type Definition.");
        }
        for (JsonNode n : typeNode) {
            TYPE t = TYPEFactory.getType(n);
            this.error = (this.error + sep + t);
            sep = ", ";

            if (n.isObject()) {
                this.schemas.add(new JacksonSchema(n));
            } else {
                this.schemas.add(new TypeValidator(n));
            }
        }
        this.error += "]";
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        TYPE nodeType = TYPEFactory.getNodeType(node);

        List<String> result = new ArrayList<String>();
        boolean valid = false;

        for (JSONValidator schema : this.schemas) {
            List<String> errors = schema.validate(node, at);
            if ((errors == null) || (errors.size() == 0)) {
                valid = true;
                break;
            }
        }

        if (!valid) {
            result.add(at + ": " + nodeType + " found, but " + this.error
                    + " is required");
        }

        return result;
    }
}
