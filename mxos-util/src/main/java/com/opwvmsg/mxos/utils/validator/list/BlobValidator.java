/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchema;
import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaUtil;
import com.opwvmsg.mxos.utils.validator.json.schema.TYPE;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.TYPEFactory;

/**
 * BlobValidator class for blob validator.
 *
 * @author mxos-dev
 * 
 */
public class BlobValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = 4460940200749213244L;
    private static Logger LOG = Logger.getLogger(BlobValidator.class);
    public static final String PROPERTY = "blob";
    protected String blobSeperator;
    protected String valueSeperator;

    /**
     * default constructor.
     *
     * @param patternNode
     *            patternNode
     */
    public BlobValidator(JsonNode patternNode) {
        this.blobSeperator = "\\|";
        this.valueSeperator = ":";
        if ((patternNode != null) && (patternNode.isTextual())) {
            String temp = patternNode.getTextValue();
            if (temp != null && temp.length() >= 2) {
                // add \\ for \,. to make support regexpression.
                char t = temp.charAt(0);
                if (t == '|' || t == '.') {
                    this.valueSeperator = new String("\\" + t);
                } else {
                    this.valueSeperator = new String("" + t);
                }
                t = temp.charAt(1);
                if (t == '|' || t == '.') {
                    this.blobSeperator = new String("\\" + t);
                } else {
                    this.blobSeperator = new String("" + t);
                }
            }
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");

        List<String> errors = new ArrayList<String>();

        TYPE nodeType = TYPEFactory.getNodeType(node);
        if (nodeType != TYPE.STRING) {
            errors.add(at + ": cannot match a " + nodeType
                    + " against a blob seperator (" + this.blobSeperator + ")");
            return errors;
        }
        try {
            //TODO: Need to have generic empty value check, 
            // only for cos attribute.
            // split the blob into individual sub-attributes
            String[] attributes = node.getTextValue().split(this.blobSeperator);
            for (String attribute : attributes) {
                String[] temp = attribute.split(this.valueSeperator);
                if (temp == null || temp.length < 1) {
                    // No key and no value.
                    errors.add("Custom Attribute " + attribute + "is not valid");
                    continue;
                } if (temp.length == 1) {
                    // Received only the key.  Value is empty.
                    continue;
                } else {
                    // Received both key and value
                    String param = temp[0];
                    String value = temp[1];
                    // validate each sub-attribute against its resp. shcema.
                    JSONSchema schema = JSONSchemaUtil.getJSONSchema(param);
                    String jsonParam = "\"" + value + "\"";
                    List<String> tempErrors = schema.validate(jsonParam);
                    if (tempErrors != null && tempErrors.size() > 0) {
                        errors.addAll(tempErrors);
                    }
                }
            }
        } catch (PatternSyntaxException pse) {
            LOG.error("Failed to apply blob on " + at
                    + ": Invalid RE syntax [param1:value1" + this.blobSeperator
                    + "param2:value3]", pse);
        } catch (Exception e) {
            LOG.error("Failed to apply blob on " + at
                    + ": Invalid RE syntax [param1:value1" + this.blobSeperator
                    + "param2:value3]", e);
        }
        return errors;
    }
}
