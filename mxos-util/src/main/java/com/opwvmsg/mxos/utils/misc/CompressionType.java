/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.utils.misc;

/**
 * Enum of all possible compression algorithm for blob data.
 *
 * @author Jaydeep
 */
public enum CompressionType {
    /**
     * <tt>NotApplicable</tt>: If compression is not applied then use this
     *  type.
     *
     * @since mxos 2.0
     */
    notapplicable,

    /**
     * <tt>QuickLZ</tt>: QuickLZ compression algorithm type
     *  (http://www.quicklz.com/).
     *
     * @since mxos 2.0
     */
    quicklz
}
