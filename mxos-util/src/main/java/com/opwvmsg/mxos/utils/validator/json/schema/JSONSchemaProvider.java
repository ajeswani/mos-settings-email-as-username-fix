/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema;

import java.io.InputStream;
import java.io.Reader;
import java.net.URL;

import org.codehaus.jackson.JsonNode;

/**
 * JSONSchemaProvider is provider for json schema validation.
 *
 * @author mxos-dev
 *
 */
public interface JSONSchemaProvider {

    /**
     * Get Schema.
     * @param paramString paramString
     * @return JSONSchema JSONSchema
     */
    JSONSchema getSchema(String paramString);

    /**
     * Get Schema.
     * @param paramInputStream paramInputStream
     * @return JSONSchema JSONSchema
     */
    JSONSchema getSchema(InputStream paramInputStream);

    /**
     * Get Schema.
     * @param paramReader paramReader
     * @return JSONSchema JSONSchema
     */
    JSONSchema getSchema(Reader paramReader);

    /**
     * Get Schema.
     * @param paramURL paramURL
     * @return JSONSchema JSONSchema
     */
    JSONSchema getSchema(URL paramURL);
    
    /**
     * Method to get Node.
     * @param schemaStream
     * @return
     */
    JsonNode getNode(InputStream schemaStream);
}
