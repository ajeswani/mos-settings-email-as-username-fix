/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.utils.datastore.hazelcast;

import org.apache.log4j.Logger;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IdGenerator;
import static com.opwvmsg.mxos.data.enums.MxOSConstants.MXOS_HOME;

/**
 * Single instance of HazelCast per VM.
 * @author 
 */
final class HazelcastProvider {
    
    /*
     * Hazelcast config (hazelcast-mxos.xml) is provided by System Property
     * "hazelcast.config".
     */
    private static final HazelcastInstance SINGLE_INSTANCE;
    private static final IdGenerator CLUSTER_ID_GENERATOR;
    
    static {
        /* get the correct hz-config file */
        final String hzPropKey = "hazelcast.config";
        String hzCfgPath = System.getProperty(hzPropKey);
        if (hzCfgPath == null || hzCfgPath.isEmpty()) {
            final String home = System.getProperty(MXOS_HOME);
            if (home == null) {
                throw new RuntimeException("${MXOS_HOME} must be set.");
            }
            hzCfgPath = home + "/config/hazelcast/hazelcast-mxos.xml";
            System.setProperty(hzPropKey, hzCfgPath);
        }

        Logger.getLogger(HazelcastProvider.class).debug(
                "Starting Hazelcast with hazelcast config at:" + hzCfgPath);
        
        SINGLE_INSTANCE = Hazelcast.newHazelcastInstance();
        CLUSTER_ID_GENERATOR = SINGLE_INSTANCE.getIdGenerator("HZ_CLUSTER_ID");
    }
    
    /**
     * Hidden constructor
     */
    private HazelcastProvider() {
        throw new UnsupportedOperationException(
                "Use SingletonDataStore.getInstance().");
    }
    
    /**
     * Provided single unique instance of HazelCast on this VM.
     * @return
     */
    static HazelcastInstance getInstance() {
        return SINGLE_INSTANCE;
    }
    
    
    /**
     * Shuts down all running Hazelcast instances on this JVM, including
     * the default one if it is running. 
     */
    static void shutdownAll() {
        Hazelcast.shutdownAll();
    }
    
    static IdGenerator getIdGenerator(){
        return CLUSTER_ID_GENERATOR;
    }
    
    /**
     * @return true - if Hazelcast instance is created.
     */
    static boolean instanceCreated(){
        return (SINGLE_INSTANCE != null); 
    }
}
