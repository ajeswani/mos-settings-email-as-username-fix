/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JacksonSchema;

/**
 * RequiresValidator class for requires validator.
 *
 * @author mxos-dev
 * 
 */
public class RequiresValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = 2662078423654448119L;
    private static Logger LOG = Logger.getLogger(RequiresValidator.class);
    public static final String PROPERTY = "requires";
    public static final int MODE_NONE = 0;
    public static final int MODE_STR = 1;
    public static final int MODE_OBJ = 2;
    private String requiredPropertyName;
    private JacksonSchema schema;
    private int mode;

    /**
     * default constructor.
     *
     * @param requiresNode
     *            requiresNode
     */
    public RequiresValidator(JsonNode requiresNode) {
        this.schema = null;
        this.mode = 0;

        if (requiresNode != null) {
            if (requiresNode.isTextual()) {
                this.requiredPropertyName = requiresNode.getTextValue();
                this.mode = 1;
            }

            if (requiresNode.isObject()) {
                this.schema = new JacksonSchema(requiresNode);
                this.mode = 2;
            }
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        if (parent == null) {
            return errors;
        }
        if (this.mode == 1) {
            JsonNode requiredProperty = parent.get(this.requiredPropertyName);
            if ((requiredProperty == null) || (requiredProperty.isNull())) {
                errors.add(at
                        + ": the presence of this property requires that "
                        + this.requiredPropertyName + " also be present");
            }
        }

        if (this.mode == 2) {
            errors.addAll(this.schema.validate(node, at));
        }

        return errors;
    }
}
