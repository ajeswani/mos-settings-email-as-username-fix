/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser.NumberType;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * MaximumValidator class for maximum validator.
 *
 * @author mxos-dev
 * 
 */
public class MaximumValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -6065577788738619222L;
    private static Logger LOG = Logger.getLogger(MaximumValidator.class);
    public static final String PROPERTY = "maximum";
    public static final String PROPERTY_CANEQUAL = "maximumCanEqual";
    protected Number maximum;
    protected NumberType numberType;
    protected boolean canEqual = true;

    /**
     * default constructor.
     *
     * @param maximumNode
     *            maximumNode
     * @param maximumCanEqualNode
     *            maximumCanEqualNode
     */
    public MaximumValidator(JsonNode maximumNode, JsonNode maximumCanEqualNode)
    {
        if ((maximumNode != null) && (maximumNode.isNumber())) {
            this.maximum = maximumNode.getNumberValue();
            this.numberType = maximumNode.getNumberType();
        }

        if ((maximumCanEqualNode != null) && (maximumCanEqualNode.isBoolean())) {
            this.canEqual = maximumCanEqualNode.getBooleanValue();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        if (this.maximum == null) {
            return errors;
        }
        if (null != node && null != node.getTextValue()
                && node.getTextValue().length() > 0) {
            // numberType = node.getNumberType();
            long value = 0;
            try {
                value = Long.parseLong(node.getTextValue());
            } catch (Exception e) {
                errors.add(at + ": Unable to parse");
                return errors;
            }
            boolean greaterThanMax = false;
            if (value > this.maximum.longValue()) {
                greaterThanMax = true;
            }

            /*
             * switch (numberType) { case LONG: if
             * (node.getDecimalValue().compareTo( (BigDecimal) this.maximum) <=
             * 0) { break; } greaterThanMax = true; break; case DOUBLE: if
             * (node.getBigIntegerValue().compareTo( (BigInteger) this.maximum)
             * <= 0) { break; } greaterThanMax = true; break; case INT: if
             * (node.getDoubleValue() <= this.maximum.doubleValue()) { break; }
             * greaterThanMax = true; break; case FLOAT: if
             * (node.getDoubleValue() <= this.maximum.doubleValue()) { break; }
             * greaterThanMax = true; break; case BIG_DECIMAL: if
             * (node.getIntValue() <= this.maximum.intValue()) { break; }
             * greaterThanMax = true; break; case BIG_INTEGER: if
             * (node.getLongValue() <= this.maximum.longValue()) { break; }
             * greaterThanMax = true; default: }
             */
            if ((greaterThanMax)
                    || ((!this.canEqual) && value == this.maximum.longValue()))
            {
                errors.add(at + ": must have a maximum value of "
                        + this.maximum);
            }
        }
        return errors;
    }
}
