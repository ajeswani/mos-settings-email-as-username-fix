/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * EnumValidator class for enum validator.
 *
 * @author mxos-dev
 * 
 */
public class EnumValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -7163667264068815707L;
    private static Logger logger = Logger.getLogger(EnumValidator.class);
    public static final String PROPERTY = "enum";
    protected List<JsonNode> nodes;
    protected String error;

    /**
     * Default constructor.
     *
     * @param enumNode
     *            enumNode
     */
    @SuppressWarnings("deprecation")
    public EnumValidator(JsonNode enumNode) {
        this.nodes = new ArrayList<JsonNode>();
        this.error = "[none]";

        if ((enumNode != null) && (enumNode.isArray())) {
            this.error = "[";
            int i = 0;
            for (JsonNode n : enumNode) {
                this.nodes.add(n);

                String v = n.getValueAsText();
                this.error = (this.error + (i == 0 ? "" : ", ") + v);
                i++;
            }

            this.error += "]";
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        logger.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        logger.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();
        if (null != node && null != node.getTextValue()
                && node.getTextValue().length() > 0) {
            for (JsonNode nd : nodes) {
                if (nd.getTextValue().equalsIgnoreCase(node.getTextValue())) {
                    return errors;
                }
            }
            errors.add(at + ": does not have a value in the enumeration "
                    + this.error);
        }
        return errors;
    }
}
