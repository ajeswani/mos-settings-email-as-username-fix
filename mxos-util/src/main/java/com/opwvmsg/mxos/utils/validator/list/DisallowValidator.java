/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.TYPE;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.TYPEFactory;

/**
 * DisallowValidator class for disallow json validator.
 *
 * @author mxos-dev
 * 
 */
public class DisallowValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -7563984149808796291L;
    private static Logger LOG = Logger.getLogger(DisallowValidator.class);
    public static final String PROPERTY = "disallow";
    protected TYPE[] disallowedTypes;

    /**
     * default constructor.
     *
     * @param disallowNode
     *            disallowNode
     */
    public DisallowValidator(JsonNode disallowNode) {
        if ((disallowNode.isObject()) || (disallowNode.isTextual())) {
            this.disallowedTypes = new TYPE[] { TYPEFactory
                    .getType(disallowNode) };
        }

        if (disallowNode.isArray()) {
            int nb = disallowNode.size();
            this.disallowedTypes = new TYPE[nb];
            for (int i = 0; i < nb; i++) {
                this.disallowedTypes[i] = TYPEFactory.getType(disallowNode
                        .get(i));
            }
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        TYPE nodeType = TYPEFactory.getNodeType(node);
        for (TYPE t : this.disallowedTypes) {
            if (t == nodeType) {
                errors.add(at + ": " + t
                        + " is not a type allowed on this property");
                break;
            }

            if ((nodeType == TYPE.INTEGER) && (t == TYPE.LONG)
                    && (t == TYPE.DOUBLE) && (t == TYPE.FLOAT)) {
                errors.add(at + ": " + t
                        + " is not an allowed type for this property");
                break;
            }
        }
        return errors;
    }
}
