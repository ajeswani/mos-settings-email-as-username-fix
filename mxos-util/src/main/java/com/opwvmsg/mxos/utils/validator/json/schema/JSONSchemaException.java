/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */


package com.opwvmsg.mxos.utils.validator.json.schema;

import java.io.Serializable;

/**
 * JSONSchemaException class for json schema validation exception.
 *
 * @author mxos-dev
 *
 */
public class JSONSchemaException extends RuntimeException implements
        Serializable {
    private static final long serialVersionUID = -6253311365554811087L;
    /**
     * default constructor.
     */
    public JSONSchemaException() {
    }
    /**
     * constructor with message param.
     * @param message message
     */
    public JSONSchemaException(String message) {
        super(message);
    }
    /**
     * constructor with throwable param.
     * @param throwable throwable
     */
    public JSONSchemaException(Throwable throwable) {
        super(throwable);
    }
    /**
     * constructor with message and throwable params.
     * @param message message
     * @param throwable throwable
     */
    public JSONSchemaException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
