/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/config/ConfigServiceImpl.java#1 $
 */

package com.opwvmsg.mxos.utils.config;

import java.util.Arrays;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig.Keys;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;

/**
 * ConfigServie Implementation.
 * 
 * @author Aricent (partially used from RM)
 * 
 */
public class ConfigServiceImpl implements ConfigService {
    private static Logger logger = Logger.getLogger(ConfigServiceImpl.class);

    /*
     * private String configHost; private String configPort; private String
     * defaultApp; private String defaultHost; private int
     * configConnectAttempts; private int configConnectInterval;
     */

    private Config config;

    /**
     * Default Constructor.
     */
    public ConfigServiceImpl() {
        super();
    }

    /**
     * GetConfig to get MxOSConfig instance.
     */
    public void getConfig() {
        logger.debug("Setting config.........");
        HashMap<String, Object> configdbMap = new HashMap<String, Object>(
                MxOSConfig.getDefaultValues());

        logger.debug("getConfigHost() = " + MxOSConfig.getConfigHost());
        configdbMap.put("confServHost", MxOSConfig.getConfigHost());
        logger.debug("getConfigPort() = " + MxOSConfig.getConfigPort());
        configdbMap.put("confServPort", MxOSConfig.getConfigPort());
        logger.debug("getIntermailDir() = " + MxOSConfig.getIntermailDir());
        configdbMap.put("intermailDir", MxOSConfig.getIntermailDir());
        logger.debug("getDefaultApp() = " + MxOSConfig.getDefaultApp());
        configdbMap.put("defaultApp", MxOSConfig.getDefaultApp());
        Config.setConfig("intermail", configdbMap);
        config = Config.getInstance();

        String logDir = null;
        int attempts = MxOSConfig.getConfigConnectAttempts();
        while (logDir == null && attempts-- > 0) {
            try {
                logDir = Config.getInstance().get("defaultDirServer");
            } catch (ConfigException e) {
                logger.error("Exception while getting Config instance. Message: "
                        + e.getMessage());
                logDir = null;
            } finally {
                if (attempts > 0 && logDir == null) {
                    try {
                        logger.info("sleeping "
                                + MxOSConfig.getConfigConnectInterval()
                                + " secs before next connect "
                                + "attempt to config");
                        Thread.sleep(MxOSConfig.getConfigConnectInterval() * 1000);
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage());
                        break;
                    }
                }
            }
        }

        if (logDir != null) {
            logger.info("Successfully connected to config db");
            // set Key Impacts
            Config.setKeyImpacts(MxOSConfig.getKeyImpacts());

            // Get additional configurations to initialize the cache
            try {
                initConfigCache();
            } catch (ConfigException e) {
                String errorMessage = "Could not able"
                        + " to config cache for additional configurations";
                logger.error(errorMessage, e);
                throw new RuntimeException(errorMessage);
            }
        } else {
            ConnectionErrorStats.CONFIGDB.increment();
            String errorMessage = "Could not able to connect to config.db"
                    + " server...";
            logger.error(errorMessage);
            throw new RuntimeException(errorMessage);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully connected to config.db " + configdbMap);

            logger.trace("Successfully connected to config.db "
                    + config.toString() + " :: Default App:"
                    + MxOSConfig.getDefaultApp() + " :: Default Host:"
                    + MxOSConfig.getDefaultHost());
        }
    }

    /**
     * Get additional configurations from config.db to initialize the cache.
     * 
     * @throws ConfigException
     */
    private void initConfigCache() throws ConfigException {
        String pwdType = MxOSConfig.getDefaultPasswordStoreType();
        logger.debug("pwdType --> " + pwdType);
        if (MxosEnums.PasswordStoreType.CUSTOM_1.toString().equals(pwdType)) {
            logger.debug(Keys.KEY_B_LIST + " --> "
                    + MxOSConfig.getCryptoKeyList());
        }
        logger.debug(Keys.ENABLE_SOFT_DELETE + " --> "
                + MxOSConfig.isEnableSoftDelete());
        logger.debug(Keys.MAILBOX_OBJECT_CLASSES + " --> "
                + Arrays.toString(MxOSConfig.getMailboxObjectClasses()));
        logger.debug(Keys.PARENT_DOMAIN_OBJECT_CLASSES + " --> "
                + Arrays.toString(MxOSConfig.getParentDomainObjectClasses()));
        logger.debug(Keys.CHILD_DOMAIN_OBJECT_CLASSES + " --> "
                + Arrays.toString(MxOSConfig.getChildDomainObjectClasses()));
        logger.debug(Keys.STATS_ENABLED + " --> " + MxOSConfig.isStatsEnabled());
        logger.debug(Keys.STATS_NUM_EVENTS + " --> "
                + MxOSConfig.getStatsNumEvents());
        logger.debug(Keys.STATS_TIME_DURATION + " --> "
                + MxOSConfig.getStatsTimeDuration());
    }
}
