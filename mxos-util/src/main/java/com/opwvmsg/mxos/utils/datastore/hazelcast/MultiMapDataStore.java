/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.utils.datastore.hazelcast;

import java.util.ArrayList;
import java.util.List;

import com.hazelcast.core.MultiMap;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;

import static com.opwvmsg.mxos.utils.datastore.hazelcast.HazelcastProvider.getInstance;

/**
 * Provides Hazelcast {@link MultiMap} support for {@link IDataStoreMultiMap} 
 * @author 
 *
 * @param <K>
 * @param <V>
 */
class MultiMapDataStore<K, V> implements IDataStoreMultiMap<K, V> {

    @Override
    public void putEntry(String mapId, K key, V value) {
        final MultiMap<K, V> clusterMultiMap = getInstance().getMultiMap(mapId);
        clusterMultiMap.put(key, value);
    }

    @Override
    public List<V> get(String mapId, K key) {
        final MultiMap<K, V> clusterMultiMap = getInstance().getMultiMap(mapId);
        return new ArrayList<V>(clusterMultiMap.get(key));
    }

    @Override
    public List<V> remove(String mapId, K key) {
        final MultiMap<K, V> clusterMultiMap = getInstance().getMultiMap(mapId);
        return new ArrayList<V>(clusterMultiMap.remove(key));
    }

    @Override
    public boolean removeEntry(String mapId, K key, V value) {
        final MultiMap<K, V> clusterMultiMap = getInstance().getMultiMap(mapId);
        return clusterMultiMap.remove(key, value);
    }

    @Override
    public boolean containsEntry(String mapId, K key, V value) {
        final MultiMap<K, V> clusterMultiMap = getInstance().getMultiMap(mapId);
        return clusterMultiMap.containsEntry(key, value);
    }

    @Override
    public boolean containsKey(String mapId, K key) {
        final MultiMap<K, V> clusterMultiMap = getInstance().getMultiMap(mapId);
        return clusterMultiMap.containsKey(key);
    }

    @Override
    public void clear(String mapId) {
        getInstance().getMultiMap(mapId).clear();        
    }

    @Override
    public int size(String mapId) {
        return getInstance().getMultiMap(mapId).size();
    }
}
