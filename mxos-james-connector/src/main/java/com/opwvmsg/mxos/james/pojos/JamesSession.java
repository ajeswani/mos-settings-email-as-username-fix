/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.pojos;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.james.mailbox.store.SimpleMailboxSession;
import org.slf4j.Logger;


/**
 * Describes mailbox session of Mxos implementation.
 */
public class JamesSession extends SimpleMailboxSession {

    private final String email;
    private final Long mailboxId;
    /**
     * Flag to record reading of all folders.
     */
    private boolean foldersLoadded;
    /**
     * Flag to record reading of all folders.
     */
    private boolean messagesLoadded;
    /**
     * Map of uid to messageId of each folder.
     */
    private final Map<Integer, JamesMailbox> folders;
    /**
     * Default Constructor.
     * @param sessionId sessionId
     * @param email email
     * @param mailboxId mailboxId
     * @param log log
     * @param localePreferences localePreferences
     * @param pathSeparator pathSeparator
     * @param type type
     */
    public JamesSession(final long sessionId, final String email,
            final Long mailboxId, final Logger log,
            final List<Locale> localePreferences, char pathSeparator,
            SessionType type) {
        super(sessionId, email, null, log, localePreferences,
                new ArrayList<String>(), null, pathSeparator, type);
        this.email = email;
        this.mailboxId = mailboxId;
        folders = new ConcurrentHashMap<Integer, JamesMailbox>();
        foldersLoadded = false;
        messagesLoadded = false;
        System.out.println("JamesSession created... : " + mailboxId);
    }
    /**
     * Method to get Email.
     * @return email of String type
     */
    public String getEmail() {
        return this.email;
    }
    /**
     * Method to get MailboxId.
     * @return mailboxId of Long type.
     */
    public Long getMailboxId() {
        return this.mailboxId;
    }

    /**
     * Method to set the folder name with id.
     * 
     * @param folderId folderId
     * @param folderName folderName
     */
    public void setFolder(final Integer folderId, JamesMailbox mailbox) {
        this.folders.put(folderId, mailbox);
    }

    /**
     * Method to get the folderId.
     * 
     * @param folderName folderName
     */
    public Integer getFolderId(String folderName) {
        for (Entry<Integer, JamesMailbox> entry : this.folders.entrySet()) {
            if (entry.getValue().getName().equals(folderName)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * Method to get the folderId.
     * 
     * @param folderId folderId
     */
    public String getFolderName(final Integer folderId) {
        return this.folders.get(folderId).getName();
    }

    /**
     * Method to remove the folderId mapping.
     *
     */
    public void removeFolder(final Integer folderId) {
        this.folders.remove(folderId);
    }
    /**
     * Method to clear the folderId mapping.
     */
    public void clearFolderMap() {
        this.folders.clear();
    }
    /**
     * @return the foldersLoadded
     */
    public boolean isFoldersLoadded() {
        return foldersLoadded;
    }
    /**
     * @param foldersLoadded the foldersLoadded to set
     */
    public void setFoldersLoadded(boolean foldersLoadded) {
        this.foldersLoadded = foldersLoadded;
    }
    /**
     * Method to set messageID from given uid.
     * @param folderId folderId
     * @param uid uid
     * @param messageId string
     */
    public void setMessageID(Integer folderId, Long uid, String messageId) {
        JamesMailbox folder = this.folders.get(folderId);
        folder.getUidToMessageIdMap().put(uid, messageId);
    }
    /**
     * Method to get messageID from given uid.
     * @param folderId folderId
     * @param uid uid
     * @return messageId string
     */
    public String getMessageID(Integer folderId, Long uid) {
        JamesMailbox folder = this.folders.get(folderId);
        return folder.getUidToMessageIdMap().get(uid);
    }
    /**
     * Method to get nextUid from given folder.
     * @param folderId messageId
     * @param uid uid
     * @return nextUId long
     */
    public Long getFolderNextUID(Integer folderId) {
        return this.folders.get(folderId).getNextUid();
    }
    /**
     * Method to get nextUid from given folder.
     * @param folderName folderName
     * @return nextUId long
     */
    public Long getFolderNextUID(String folderName) {
        for (Entry<Integer, JamesMailbox> entry : this.folders.entrySet()) {
            if (entry.getValue().getName().equals(folderName)) {
                return entry.getValue().getNextUid();
            }
        }
        return null;
    }
    /**
     * Method to get nextUid from given folder.
     * @param folderId folderId
     * @return nextUId long
     */
    public Long getFolderLastUID(Integer folderId) {
        return this.folders.get(folderId).getNextUid();
    }
    /**
     * Method to get nextUid from given folder.
     * @param folderName folderName
     * @return nextUId long
     */
    public Long getFolderLastUID(String folderName) {
        return getFolderNextUID(folderName);
    }
    /**
     * @return the folders
     */
    public JamesMailbox getFolder(String name) {
        for (Entry<Integer, JamesMailbox> entry : this.folders.entrySet()) {
            if (entry.getValue().getName().equals(name)) {
                return entry.getValue();
            }
        }
        return null;
    }
    /**
     * @return the folders
     */
    public JamesMailbox getFolder(Integer folderId) {
        return this.folders.get(folderId);
    }
    /**
     * @return the messagesLoadded
     */
    public boolean isMessagesLoadded() {
        return messagesLoadded;
    }
    /**
     * @param messagesLoadded the messagesLoadded to set
     */
    public void setMessagesLoadded(boolean messagesLoadded) {
        this.messagesLoadded = messagesLoadded;
    }
    /**
     * @return the folders
     */
    public Map<Integer, JamesMailbox> getFolders() {
        return folders;
    }
}
