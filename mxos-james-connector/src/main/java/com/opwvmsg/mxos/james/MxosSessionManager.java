/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james;

import java.util.ArrayList;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.james.mailbox.BadCredentialsException;
import org.apache.james.mailbox.MailboxException;
import org.apache.james.mailbox.MailboxNotFoundException;
import org.apache.james.mailbox.MailboxSession;
import org.apache.james.mailbox.MailboxSession.SessionType;
import org.apache.james.mailbox.store.Authenticator;
import org.apache.james.mailbox.store.JVMMailboxPathLocker;
import org.apache.james.mailbox.store.MailboxSessionMapperFactory;
import org.apache.james.mailbox.store.StoreMailboxManager;
import org.apache.james.mailbox.store.StoreMessageManager;
import org.slf4j.Logger;

import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.james.folder.MxosFolderMapper;
import com.opwvmsg.mxos.james.message.MxosMessageManager;
import com.opwvmsg.mxos.james.pojos.MxosRequestMap;
import com.opwvmsg.mxos.james.pojos.JamesSession;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * This base class of an {@link MailboxManager} implementation provides a
 * high-level api for writing your own {@link MailboxManager} implementation. If
 * you plan to write your own {@link MailboxManager} its most times so easiest
 * to extend just this class or use it directly.
 *
 * If you need a more low-level api just implement {@link MailboxManager}
 * directly
 *
 * @author mxos-dev
 */
public class MxosSessionManager extends StoreMailboxManager<Integer> {

    @Resource(name = "mailboxBaseService")
    protected IMailboxBaseService mailboxBaseService;

    @Resource(name = "credentialService")
    protected ICredentialService credentialService;

    /**
     * Constructor with factory and authenticator.
     *
     * @param mailboxSessionMapperFactory MailboxSessionMapperFactory<Integer>
     * @param authenticator Authenticator
     */
    public MxosSessionManager(
            MailboxSessionMapperFactory<Integer> mailboxSessionMapperFactory,
            final Authenticator authenticator) {
        super(mailboxSessionMapperFactory, authenticator,
                new JVMMailboxPathLocker());
    }

    /**
     * This method is being called during SMPT drop message.
     *
     * @param userName String
     * @param log Logger
     * @return MailboxSession MailboxSession
     */
    @Override
    public MailboxSession createSystemSession(String userName, Logger log) {
        try {
            return createSession(userName, log, SessionType.System);
        } catch (MailboxException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create Session.
     *
     * @param email String
     * @param log Logger
     * @return type SessionType
     */
    private MailboxSession createSession(final String email, final Logger log,
            final SessionType type) throws MailboxException {
        JamesSession session = null;
        if (email != null && !"".equals(email)) {
            // Get Mailbox API Call to get mailbox pojo.
            if (mailboxBaseService != null) {
                // TODO get Mailbox
                try {
                    final MxosRequestMap map = new MxosRequestMap();
                    map.add(MailboxProperty.email, email);
                    System.out.println("Get Mailbox Request : " + map);
                    final Base mailbox = mailboxBaseService.read(map
                            .getMultivaluedMap());
                    if (mailbox != null) {
                        session = new JamesSession(randomId(),
                                mailbox.getEmail(), mailbox.getMailboxId(),
                                log, new ArrayList<Locale>(), getDelimiter(),
                                type);
                        MxosFolderMapper mailboxMapper =
                            (MxosFolderMapper) getMapperFactory()
                                .getMailboxMapper(session);
                        mailboxMapper.list();
                    } else {
                        throw new MailboxNotFoundException(email);
                    }
                } catch (MxOSException e) {
                    throw new MailboxNotFoundException(e.getMessage());
                }
            } else {
                throw new MailboxException("MailboxService Not found");
            }
        }
        return session;
    }

    /**
     * Log in the user with the given userid and password.
     *
     * @param userid the username
     * @param passwd the password
     * @return true if login success
     */
    private boolean login(final String userid, final String passwd)
        throws BadCredentialsException, MailboxException {
        if (credentialService != null) {
            try {
                // TODO connect Mxos SDK to authenticate and get mailbox.
                final MxosRequestMap map = new MxosRequestMap();
                map.add(MailboxProperty.email, userid);
                map.add(MailboxProperty.password, passwd);
                System.out.println("Authentication Request Sent : " + userid);
                credentialService.authenticate(map.getMultivaluedMap());
                return true;
            } catch (Exception e) {
                // Exception while communicating with MxOS
                throw new BadCredentialsException();
            }
        } else {
            // Credential Service not found
            throw new MailboxException("CredentialService not found");
        }
    }

    @Override
    public MailboxSession login(final String userid, final String passwd,
            Logger log) throws BadCredentialsException, MailboxException {
        if (login(userid, passwd)) {
            return createSession(userid, log, SessionType.User);
        } else {
            throw new BadCredentialsException();
        }
    }

    @Override
    public void logout(MailboxSession session, boolean force) {
        System.out.println("MailboxSession.logout : "
                + session.getUser().getUserName());
        session.close();
    }

    @Override
    protected StoreMessageManager<Integer> createMessageManager(
            org.apache.james.mailbox.store.mail.model.Mailbox<Integer> mboxRow,
            MailboxSession session) throws MailboxException {
        StoreMessageManager<Integer> result = new MxosMessageManager(
                getMapperFactory(), getMessageSearchIndex(),
                getEventDispatcher(), mboxRow);
        return result;
    }
}
