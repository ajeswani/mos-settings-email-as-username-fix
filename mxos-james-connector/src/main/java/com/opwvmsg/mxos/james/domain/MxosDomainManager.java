/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.james.domainlist.api.DomainListException;
import org.apache.james.domainlist.lib.AbstractDomainList;

import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.domain.IDomainService;
import com.opwvmsg.mxos.james.pojos.MxosRequestMap;

/**
 * MxOS API implementation of the DomainList.
 *
 * @author mxos-dev
 */
public class MxosDomainManager extends AbstractDomainList {
    @Resource(name = "domainService")
    protected IDomainService domainService = null;

    /**
     * Default method.
     */
    @PostConstruct
    public void init() {
    }

    @Override
    protected List<String> getDomainListInternal() throws DomainListException {
        final List<String> domains = new ArrayList<String>();
        try {
            // Get Domain List
            final MxosRequestMap map = new MxosRequestMap();
            map.add(DomainProperty.domain, "*");
            if (domainService != null) {
                System.out.println("Read Domain Request : " + map);
                final List<Domain> domainsList = domainService.search(map
                        .getMultivaluedMap());
                for (Domain domain : domainsList) {
                    domains.add(domain.getDomain());
                }
            } else {
                throw new DomainListException("DomainService Not Found");
            }
            Collections.sort(domains);
        } catch (MxOSException e) {
            throw new DomainListException("Unable to search domains", e);
        } catch (Exception e) {
            throw new DomainListException("Unable to search domains", e);
        }
        if (domains.size() == 0) {
            return null;
        } else {
            return new ArrayList<String>(domains);
        }
    }

    @Override
    public boolean containsDomain(final String domain)
        throws DomainListException {
        try {
            // Get Domain API
            return getDomainListInternal().contains(domain);
        } catch (Exception e) {
            throw new DomainListException("Unable to retrieve : " + domain, e);
        }
    }

    @Override
    public void addDomain(final String domain) throws DomainListException {
        try {
            // Create Domain API
            final MxosRequestMap map = new MxosRequestMap();
            map.add(DomainProperty.domain, domain);
            map.add(DomainProperty.type, "local");
            System.out.println("Create Domain Request : " + map);
            if (domainService != null) {
                domainService.create(map.getMultivaluedMap());
            } else {
                throw new DomainListException("DomainService Not Found");
            }
        } catch (Exception e) {
            throw new DomainListException("Unable to add : " + domain, e);
        }
    }

    @Override
    public void removeDomain(final String domain) throws DomainListException {
        try {
            // Remove Domain API
            final MxosRequestMap map = new MxosRequestMap();
            map.add(DomainProperty.domain, domain);
            System.out.println("Remove Domain Request : " + map);
            if (domainService != null) {
                domainService.delete(map.getMultivaluedMap());
            } else {
                throw new DomainListException("DomainService Not Found");
            }
        } catch (Exception e) {
            throw new DomainListException("Unable to remove : " + domain, e);
        }
    }

}
