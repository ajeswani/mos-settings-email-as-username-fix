/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.user;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.james.user.api.UsersRepositoryException;
import org.apache.james.user.api.model.User;
import org.apache.james.user.lib.AbstractUsersRepository;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.james.pojos.MxosRequestMap;

/**
 * Mxos API based UserRepository.
 *
 * @author mxos-dev
 */
public class MxosUserManager extends AbstractUsersRepository {

    @Resource(name = "mailboxService")
    protected IMailboxService mailboxService;
    @Resource(name = "mailboxBaseService")
    protected IMailboxBaseService mailboxBaseService;

    private String passwordType;
    private int count;

    /**
     * Get the user object with the specified user name. Return null if no such
     * user.
     *
     * @param name the name of the user to retrieve
     * @return the user being retrieved, null if the user doesn't exist
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public User getUserByName(String name) throws UsersRepositoryException {
        // Get Mailbox API
        final MxosRequestMap map = new MxosRequestMap();
        map.add(MailboxProperty.email, name);
        if (mailboxBaseService != null) {
            try {
                final Base mailbox = mailboxBaseService
                        .read(map.getMultivaluedMap());
                if (mailbox != null) {
                    final User user = new MxosUser(mailbox);
                    return user;
                } else {
                    return null;
                }
            } catch (MxOSException e) {
                return null;
            }
        } else {
            throw new UsersRepositoryException("MailboxService Not found.");
        }
    }

    /**
     * Returns the user name of the user matching name on an equalsIgnoreCase
     * basis. Returns null if no match.
     *
     * @param name the name to case-correct
     * @return the case-correct name of the user, null if the user doesn't exist
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public String getRealName(final String name)
        throws UsersRepositoryException {
        final User u = getUserByName(name);
        if (u != null) {
            u.getUserName();
        }
        return null;
    }

    /**
     * Update the repository with the specified user object. A user object with
     * this user must already exist.
     *
     * @param user the name to update.
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public void updateUser(final User user) throws UsersRepositoryException {
        try {
            // TODO
            return;
        } catch (Exception e) {
            throw new UsersRepositoryException("Failed to update user "
                    + user.getUserName(), e);
        }
    }

    /**
     * Removes a user from the repository.
     *
     * @param name the user to remove from the repository
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public void removeUser(final String name) throws UsersRepositoryException {
        try {
            final MxosRequestMap map = new MxosRequestMap();
            map.add(MailboxProperty.email, name);
            System.out.println("Delete Mailbox Request : " + map);
            if (mailboxService != null) {
                mailboxService.delete(map.getMultivaluedMap());
            } else {
                throw new UsersRepositoryException("MailboxService Not found.");
            }
        } catch (Exception e) {
            throw new UsersRepositoryException("Failed to remove " + name, e);
        }
    }

    /**
     * Returns whether or not this user is in the repository.
     *
     * @param name the name to check in the repository
     * @return whether the user is in the repository
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public boolean contains(final String name) throws UsersRepositoryException {
        try {
            return (getUserByName(name) != null) ? true : false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Test if user with name 'name' has password 'password'.
     *
     * @param name the name of the user to be tested
     * @param password the password to be tested
     * @return true if the test is successful, false if the user doesn't exist
     *         or if the password is incorrect
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public boolean test(final String name, final String password)
        throws UsersRepositoryException {
        final User user = getUserByName(name);
        final boolean result;
        if (user == null) {
            result = false;
        } else {
            result = user.verifyPassword(password);
        }
        return result;
    }

    /**
     * Returns a count of the users in the repository.
     *
     * @return the number of users in the repository
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public int countUsers() throws UsersRepositoryException {
        try {
            return count;
        } catch (Exception e) {
            throw new UsersRepositoryException("Failed to count users", e);
        }
    }

    /**
     * List users in repository.
     *
     * @return Iterator over a collection of Strings, each being one user in the
     *         repository.
     * @throws UsersRepositoryException UsersRepositoryException
     */
    public Iterator<String> list() throws UsersRepositoryException {
        try {
            final List<String> result = new ArrayList<String>();
            final MxosRequestMap map = new MxosRequestMap();
            map.add(MailboxProperty.email, "*");
            if (mailboxBaseService != null) {
                final List<Base> mailboxes = mailboxBaseService.search(map
                        .getMultivaluedMap());
                if (mailboxes != null) {
                    for (final Base mailbox : mailboxes) {
                        result.add(mailbox.getEmail());
                    }
                }
                this.count = result.size();
                return result.iterator();
            } else {
                throw new UsersRepositoryException("MailboxService Not found.");
            }
        } catch (Exception e) {
            throw new UsersRepositoryException("Failed to list users", e);
        }
    }

    @Override
    public void doConfigure(HierarchicalConfiguration config)
        throws ConfigurationException {
        passwordType = config.getString("algorithm", "clear");
        super.doConfigure(config);
    }

    @Override
    protected void doAddUser(final String username, final String password)
        throws UsersRepositoryException {
        try {
            if (mailboxService != null) {
                final MxosUser user = new MxosUser();
                user.setEmail(username);
                user.setPassword(password);
                user.setPasswordType(passwordType);
                final MxosRequestMap map = new MxosRequestMap();
                map.add(MailboxProperty.email, username);
                map.add(MailboxProperty.password, password);
                System.out.println("Create Mailbox Request : " + map);
                long mailboxId = mailboxService.create(map.getMultivaluedMap());
                System.out.println("User Created : " + mailboxId);
            } else {
                throw new UsersRepositoryException("MailboxService Not found.");
            }
        } catch (Exception e) {
            throw new UsersRepositoryException("Failed to add " + username, e);
        }
    }
}
