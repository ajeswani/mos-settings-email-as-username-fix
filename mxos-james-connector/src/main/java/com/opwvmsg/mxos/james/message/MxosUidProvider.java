/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.message;

import org.apache.james.mailbox.MailboxException;
import org.apache.james.mailbox.MailboxSession;
import org.apache.james.mailbox.store.mail.UidProvider;
import org.apache.james.mailbox.store.mail.model.Mailbox;

import com.opwvmsg.mxos.james.pojos.JamesSession;

/**
 * Class to provide Uid.
 *
 * @author mxos-dev
 */
public class MxosUidProvider implements UidProvider<Integer> {
    @Override
    public long nextUid(MailboxSession session, Mailbox<Integer> mailbox)
        throws MailboxException {
        final JamesSession jamesSession = (JamesSession) session;
        final Integer folderId = jamesSession.getFolderId(mailbox.getName());
        return jamesSession.getFolderNextUID(folderId) - 1;
    }

    @Override
    public long lastUid(MailboxSession session, Mailbox<Integer> mailbox)
        throws MailboxException {
        return nextUid(session, mailbox);
    }
}
