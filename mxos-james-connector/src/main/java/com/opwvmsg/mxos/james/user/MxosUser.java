/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.user;

import javax.annotation.Resource;

import org.apache.james.user.api.model.User;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.james.pojos.MxosRequestMap;

/**
 * MxosUser is bridge bean between James and Mxos.
 *
 * @author mxos-dev
 */
public class MxosUser implements User {
    @Resource(name = "credentialService")
    protected ICredentialService credService;
    /** Key by user name. */
    private String email;

    /** Hashed password. */
    private String password;

    /** Password Type. */
    private String passwordType;

    /**
     * Default constructor.
     */
    public MxosUser() {
        super();
    }

    /**
     * Constructor with Mailbox as param.
     *
     * @param mailbox Mailbox
     */
    public MxosUser(final Base mailbox) {
        super();
        this.email = mailbox.getEmail();
        this.passwordType = "clear";
        this.password = "";
    }

    /**
     * Constructor with various params.
     *
     * @param email String
     * @param mailboxId Long
     * @param password String
     * @param passwordType String
     */
    public MxosUser(final String email, final Long mailboxId,
            final String password, final String passwordType) {
        super();
        this.email = email;
        this.passwordType = passwordType;
        this.password = password;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MxosUser other = (MxosUser) obj;
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[User " + email + "]";
    }
    /**
     * Method to get password.
     * @return password String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Method to set password type.
     *
     * @param passwordType String
     */
    public void setPasswordType(final String passwordType) {
        this.passwordType = passwordType;
    }

    /**
     * Method to get password type.
     *
     * @return String passwordType
     */
    public String getPasswordType() {
        return passwordType;
    }

    @Override
    public String getUserName() {
        // TODO Auto-generated method stub
        return this.email;
    }

    @Override
    public boolean verifyPassword(final String pass) {
        try {
            final MxosRequestMap map = new MxosRequestMap();
            map.add(MailboxProperty.email, getUserName());
            map.add(MailboxProperty.password, pass);
            if (credService != null) {
                System.out.println("Authentication (Verify) Request : " + map);
                credService.authenticate(map.getMultivaluedMap());
            } else {
                return false;
            }
            return true;
        } catch (MxOSException e) {
            return false;
        }
    }

    @Override
    public boolean setPassword(final String newPass) {
        // TODO MxosAPI Update Mailbox
        this.password = newPass;
        return true;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }
}
