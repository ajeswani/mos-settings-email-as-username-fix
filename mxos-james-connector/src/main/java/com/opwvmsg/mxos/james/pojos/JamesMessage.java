/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.pojos;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Flags;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.apache.james.mailbox.store.mail.model.AbstractMessage;
import org.apache.james.mailbox.store.mail.model.Property;
import org.apache.james.mailbox.store.mail.model.impl.PropertyBuilder;
import org.apache.james.mailbox.store.streaming.ConfigurableMimeTokenStream;
import org.apache.james.mailbox.store.streaming.CountingInputStream;
import org.apache.james.mailbox.store.streaming.LazySkippingInputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.descriptor.MaximalBodyDescriptor;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.parser.MimeEntityConfig;
import org.apache.james.mime4j.parser.MimeTokenStream;

import com.opwvmsg.mxos.data.pojos.Message;

public class JamesMessage extends AbstractMessage<Integer> {
    protected Integer folderId;
    protected String messageId;
    protected Map<String, String> headers;
    protected final String headerSummary;
    protected final String messageBody;
    protected int bodyStartOctet;
    protected final PropertyBuilder propertyBuilder = new PropertyBuilder();
    protected boolean parsed;
    protected boolean answered;
    protected boolean deleted;
    protected boolean draft;
    protected boolean flagged;
    protected boolean recent;
    protected boolean seen;
    protected long uid;
    protected boolean newMessage;
    protected long modSeq;
    protected long arrivalTime;
    protected long size;

    public JamesMessage(Integer folderId, long uid, String messageBody,
            String headerSummary, long arrivalTime, Flags flags, long size)
            throws IOException {
        this.folderId = folderId;
        this.headerSummary = headerSummary;
        this.messageBody = messageBody;

        this.arrivalTime = arrivalTime;
        this.size = size;
        setUid(uid);
        setModSeq(this.arrivalTime);
        setFlags(flags);
    }

    public JamesMessage(Integer folderId, Message message) throws IOException {
        this.folderId = folderId;
        this.headerSummary = message.getHeaderSummary();
        this.messageBody = message.getMessageBody();
        this.arrivalTime = message.getArrivalTime();        
        this.size = message.getSize();
        setUid(message.getUid());
        setModSeq(this.arrivalTime);
        setFlags(message);
    }
    
    public Integer getMailboxId() {
        return this.folderId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#setFlags(
     * javax.mail.Flags)
     */
    public void setFlags(Flags flags) {
        if (flags != null) {
            answered = flags.contains(Flags.Flag.ANSWERED);
            deleted = flags.contains(Flags.Flag.DELETED);
            draft = flags.contains(Flags.Flag.DRAFT);
            flagged = flags.contains(Flags.Flag.FLAGGED);
            recent = flags.contains(Flags.Flag.RECENT);
            seen = flags.contains(Flags.Flag.SEEN);
        }
    }/*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#setFlags(
     * javax.mail.Flags)
     */
    public void setFlags(Message message) {
        this.answered = message.getFlagAns();
        this.deleted = message.getFlagDel();
        this.draft = message.getFlagDraft();
        this.flagged = message.getFlagFlagged();
        this.recent = message.getFlagRecent();
        this.seen = message.getFlagSeen();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#isAnswered()
     */
    public boolean isAnswered() {
        return answered;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#isDeleted()
     */
    public boolean isDeleted() {
        return deleted;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#isDraft()
     */
    public boolean isDraft() {
        return draft;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#isFlagged()
     */
    public boolean isFlagged() {
        return flagged;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#isRecent()
     */
    public boolean isRecent() {
        return recent;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.james.mailbox.store.mail.model.MailboxMembership#isSeen()
     */
    public boolean isSeen() {
        return seen;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.james.mailbox.store.mail.model.MailboxMembership#unsetRecent()
     */
    public void unsetRecent() {
        recent = false;
    }

    
    
    /**
     * Indicates whether this MaildirMessage reflects a new message
     * or one that already exists in the file system.
     * @return true if it is new, false if it already exists
     */
    public boolean isNew() {
        return newMessage;
    }
    
    
    @Override
    public String toString() {
        StringBuffer theString = new StringBuffer("MaildirMessage ");
        theString.append(getUid());
        theString.append(" {");
        String FLAG_ANSWERD = "R";
        String FLAG_DELETED = "T";
        String FLAG_DRAFT = "D";
        String FLAG_FLAGGED = "F";
        String FLAG_SEEN = "S";

        if (answered)
            theString.append(FLAG_ANSWERD);
        if (deleted)
            theString.append(FLAG_DELETED);
        if (draft)
            theString.append(FLAG_DRAFT);
        if (flagged)
            theString.append(FLAG_FLAGGED);
        if (seen)
            theString.append(FLAG_SEEN);

        theString.append("} ");
        theString.append(getInternalDate());
        return theString.toString();
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.Message#getModSeq()
     */
    public long getModSeq() {
        return modSeq;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.Message#setModSeq(long)
     */
    public void setModSeq(long modSeq) {
        this.modSeq = modSeq;
    }
    /**
     * Parse message if needed
     */
    private synchronized void parseMessage() {

        if (parsed) {
            return;
        }
        // SharedFileInputStream tmpMsgIn = null;
        try {
            // tmpMsgIn = new SharedFileInputStream(messageName.getFile());
            // tmpMsgIn = new SharedFileInputStream(file)

            bodyStartOctet = bodyStartOctet(new ByteArrayInputStream(
                    messageBody.getBytes("UTF-8")));

            // Disable line length... This should be handled by the smtp server
            // component and not the parser itself
            // https://issues.apache.org/jira/browse/IMAP-122
            MimeEntityConfig config = new MimeEntityConfig();
            config.setMaximalBodyDescriptor(true);
            config.setMaxLineLen(-1);
            final ConfigurableMimeTokenStream parser =
                new ConfigurableMimeTokenStream(config);

            parser.setRecursionMode(MimeTokenStream.M_NO_RECURSE);
            // parser.parse(tmpMsgIn.newStream(0, -1));
            parser.parse(new ByteArrayInputStream(
                    messageBody.getBytes("UTF-8")));

            final Map<String, String> headers = new HashMap<String, String>();
            int next = parser.next();
            while (next != MimeTokenStream.T_BODY
                    && next != MimeTokenStream.T_END_OF_STREAM
                    && next != MimeTokenStream.T_START_MULTIPART) {
                if (next == MimeTokenStream.T_FIELD) {
                    Field field = parser.getField();
                    if (field != null) {
                        headers.put(field.getName(), field.getBody());
                    }
                }
                next = parser.next();
            }
            final MaximalBodyDescriptor descriptor =
                (MaximalBodyDescriptor) parser.getBodyDescriptor();
            final String mediaType;
            final String mediaTypeFromHeader = descriptor.getMediaType();
            final String subType;
            if (mediaTypeFromHeader == null) {
                mediaType = "text";
                subType = "plain";
            } else {
                mediaType = mediaTypeFromHeader;
                subType = descriptor.getSubType();
            }
            propertyBuilder.setMediaType(mediaType);
            propertyBuilder.setSubType(subType);
            propertyBuilder.setContentID(descriptor.getContentId());
            propertyBuilder.setContentDescription(descriptor
                    .getContentDescription());
            propertyBuilder.setContentLocation(descriptor.getContentLocation());
            propertyBuilder.setContentMD5(descriptor.getContentMD5Raw());
            propertyBuilder.setContentTransferEncoding(descriptor
                    .getTransferEncoding());
            propertyBuilder.setContentLanguage(descriptor.getContentLanguage());
            propertyBuilder.setContentDispositionType(descriptor
                    .getContentDispositionType());
            propertyBuilder.setContentDispositionParameters(descriptor
                    .getContentDispositionParameters());
            propertyBuilder.setContentTypeParameters(descriptor
                    .getContentTypeParameters());
            // Add missing types
            final String codeset = descriptor.getCharset();
            if (codeset == null) {
                if ("TEXT".equalsIgnoreCase(mediaType)) {
                    propertyBuilder.setCharset("us-ascii");
                }
            } else {
                propertyBuilder.setCharset(codeset);
            }

            final String boundary = descriptor.getBoundary();
            if (boundary != null) {
                propertyBuilder.setBoundary(boundary);
            }
            if ("text".equalsIgnoreCase(mediaType)) {
                long lines = -1;
                final CountingInputStream bodyStream = new CountingInputStream(
                        parser.getInputStream());
                try {
                    bodyStream.readAll();
                    lines = bodyStream.getLineCount();
                } finally {
                    IOUtils.closeQuietly(bodyStream);
                }

                next = parser.next();
                if (next == MimeTokenStream.T_EPILOGUE) {
                    final CountingInputStream epilogueStream =
                        new CountingInputStream(
                            parser.getInputStream());
                    try {
                        epilogueStream.readAll();
                        lines += epilogueStream.getLineCount();
                    } finally {
                        IOUtils.closeQuietly(epilogueStream);
                    }
                }
                propertyBuilder.setTextualLineCount(lines);
            }
        } catch (IOException e) {
            // has successfully been parsen when appending, shouldn't give any
            // problems
        } catch (MimeException e) {
            // has successfully been parsen when appending, shouldn't give any
            // problems
        } finally {
            // if (tmpMsgIn != null) {
            //    try {
            //        tmpMsgIn.close();
            //    } catch (IOException e) {
            //        ignore on close
            //    }
            // }
            parsed = true;
        }
    }

    /**
     * Return the position in the given {@link InputStream} at which the Body of
     * the Message starts
     * 
     * @param msgIn
     * @return bodyStartOctet
     * @throws IOException
     */
    private int bodyStartOctet(InputStream msgIn) throws IOException {
        // we need to pushback maximal 3 bytes
        PushbackInputStream in = new PushbackInputStream(msgIn, 3);
        int bodyStartOctet = in.available();
        int i = -1;
        int count = 0;
        while ((i = in.read()) != -1 && in.available() > 4) {
            if (i == 0x0D) {
                int a = in.read();
                if (a == 0x0A) {
                    int b = in.read();

                    if (b == 0x0D) {
                        int c = in.read();

                        if (c == 0x0A) {
                            bodyStartOctet = count + 4;
                            break;
                        }
                        in.unread(c);
                    }
                    in.unread(b);
                }
                in.unread(a);
            }
            count++;
        }
        return bodyStartOctet;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.Message#getMediaType()
     */
    public String getMediaType() {
        parseMessage();
        return propertyBuilder.getMediaType();
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.Message#getSubType()
     */
    public String getSubType() {
        parseMessage();
        return propertyBuilder.getSubType();
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.
     * Message#getFullContentOctets()
     */
    public long getFullContentOctets() {
        return this.size;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.
     * Message#getTextualLineCount()
     */
    public Long getTextualLineCount() {
        parseMessage();
        return propertyBuilder.getTextualLineCount();
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.Message#getProperties()
     */
    public List<Property> getProperties() {
        parseMessage();
        return propertyBuilder.toProperties();
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.
     * MailboxMembership#getInternalDate()
     */
    public Date getInternalDate() {
        return new Date(this.arrivalTime);
    }

    private InputStream getFullContent() throws IOException {
        return new ByteArrayInputStream(messageBody.getBytes("UTF-8"));
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.Message#getBodyContent()
     */
    public InputStream getBodyContent() throws IOException {
        parseMessage();
        return new LazySkippingInputStream(getFullContent(), bodyStartOctet);
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.model.
     * AbstractMessage#getBodyStartOctet()
     */
    protected int getBodyStartOctet() {
        parseMessage();
        return bodyStartOctet;
    }

    @Override
    public InputStream getHeaderContent() throws IOException {
        parseMessage();
        //long limit = getBodyStartOctet() -2;
        long limit = getBodyStartOctet();
        if (limit < 0) {
            limit = 0;
        }
        return new BoundedInputStream(getFullContent(), limit);
    }
    /**
     * @return the headers
     */
    public Map<String, String> getHeaders() {
        parseMessage();
        return headers;
    }
    /**
     * @param headers the headers to set
     */
    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
