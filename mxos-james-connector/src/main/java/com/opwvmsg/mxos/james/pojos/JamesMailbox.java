/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.pojos;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.james.mailbox.MailboxPath;
import org.apache.james.mailbox.store.mail.model.Mailbox;

import com.opwvmsg.mxos.data.pojos.Folder;

/**
 * MailBox object of mxos
 *
 * @author mxos-dev
 */
public class JamesMailbox implements Mailbox<Integer> {
    private Integer folderId;
    private String folderName;
    private String email;
    private String namespace;
    private Long nextUid;
    private long uidValidity;
    private Long lastUid;
    /**
     * Map of uid to messageId.
     */
    private final Map<Long, String> uidToMessageIdMap;
    private final List<Long> unseenMessageUids;
    private final List<Long> recentMessageUids;

    /**
     * Default constructor.
     *
     * @param path path
     * @param folderId folderId
     * @param uidValidity uidValidity
     */
    public JamesMailbox(final MailboxPath path, Folder folder) {
        super();
        this.folderName = path.getName();
        this.email = path.getUser();
        this.namespace = path.getNamespace();
        this.folderId = folder.getFolderId();
        this.nextUid = folder.getNextUID();
        this.uidValidity = folder.getNextUIDValidity();
        this.uidToMessageIdMap = new ConcurrentHashMap<Long, String>();
        this.recentMessageUids = new ArrayList<Long>();
        this.unseenMessageUids = new ArrayList<Long>();
    }

    public Integer getMailboxId() {
        // TODO Auto-generated method stub
        return this.folderId;
    }

    public String getNamespace() {
        // TODO Auto-generated method stub
        return this.namespace;
    }

    public void setNamespace(String namespace) {
        // TODO Auto-generated method stub
        this.namespace = namespace;
    }

    public String getUser() {
        // TODO Auto-generated method stub
        return this.email;
    }

    public void setUser(final String user) {
        // TODO Auto-generated method stub
        this.email = user;
    }

    public String getName() {
        // TODO Auto-generated method stub
        return this.folderName;
    }

    public void setName(final String name) {
        // TODO Auto-generated method stub
        this.folderName = name;
    }

    public long getUidValidity() {
        // TODO Auto-generated method stub
        return this.uidValidity;
    }

    /**
     * @return the folderId
     */
    public Integer getFolderId() {
        return folderId;
    }

    /**
     * @param folderId the folderId to set
     */
    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    /**
     * @return the folderName
     */
    public String getFolderName() {
        return folderName;
    }

    /**
     * @param folderName the folderName to set
     */
    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the nextUid
     */
    public Long getNextUid() {
        return nextUid;
    }

    /**
     * @param nextUid the nextUid to set
     */
    public void setNextUid(Long nextUid) {
        this.nextUid = nextUid;
    }

    /**
     * @return the lastUid
     */
    public Long getLastUid() {
        return lastUid;
    }

    /**
     * @param lastUid the lastUid to set
     */
    public void setLastUid(Long lastUid) {
        this.lastUid = lastUid;
    }

    /**
     * @return the uidToMessageIdMap
     */
    public Map<Long, String> getUidToMessageIdMap() {
        return uidToMessageIdMap;
    }

    /**
     * @param uidValidity the uidValidity to set
     */
    public void setUidValidity(long uidValidity) {
        this.uidValidity = uidValidity;
    }

    /**
     * @return the recentMessageUids
     */
    public List<Long> getRecentMessageUids() {
        return recentMessageUids;
    }

    /**
     * @return the unseenMessageUids
     */
    public List<Long> getUnseenMessageUids() {
        return unseenMessageUids;
    }

}
