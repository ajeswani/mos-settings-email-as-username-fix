/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.message;

import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.Flags;

import org.apache.james.mailbox.MailboxException;
import org.apache.james.mailbox.MailboxNotFoundException;
import org.apache.james.mailbox.MailboxSession;
import org.apache.james.mailbox.MessageMetaData;
import org.apache.james.mailbox.MessageRange;
import org.apache.james.mailbox.MessageRange.Type;
import org.apache.james.mailbox.UpdatedFlags;
import org.apache.james.mailbox.store.SimpleMessageMetaData;
import org.apache.james.mailbox.store.mail.AbstractMessageMapper;
import org.apache.james.mailbox.store.mail.model.Mailbox;
import org.apache.james.mailbox.store.mail.model.Message;
import org.apache.james.mailbox.store.mail.model.impl.SimpleMessage;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.HeaderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.interfaces.service.meta.IMessageService;
import com.opwvmsg.mxos.james.MxosJamesUtil;
import com.opwvmsg.mxos.james.folder.MxosFolderMapper;
import com.opwvmsg.mxos.james.pojos.JamesMailbox;
import com.opwvmsg.mxos.james.pojos.JamesMessage;
import com.opwvmsg.mxos.james.pojos.MxosRequestMap;
import com.opwvmsg.mxos.james.pojos.JamesSession;

/**
 * JamesMessage Mapper for Mxos Implementations.
 *
 * @author mxos-dev
 */
public class MxosMessageMapper extends AbstractMessageMapper<Integer> {
    protected IMessageService messageService;
    protected MxosFolderMapper mailboxMapper = null;

    /**
     * Default Constructor.
     * @param messageService messageService
     * @param session session
     * @param mailboxMapper mailboxMapper
     * @param uidProvider uidProvider
     * @param modSqlProvider mxosModSeqProvider
     */
    public MxosMessageMapper(final IMessageService messageService,
            final MailboxSession session, final MxosFolderMapper mailboxMapper,
            final MxosUidProvider uidProvider,
            final MxosModSeqProvider modSqlProvider) {
        super(session, uidProvider, modSqlProvider);
        this.mailboxMapper = mailboxMapper;
        this.messageService = messageService;
    }

    @Override
    public long countMessagesInMailbox(final Mailbox<Integer> mailbox)
        throws MailboxException {
        final JamesSession session = (JamesSession) mailboxSession;
        if (!session.isMessagesLoadded()) {
            findMessagesInMailboxBetweenUIDs(mailbox, null, 0, -1);
        }
        JamesMailbox folder = session.getFolder(mailbox.getName());
        return folder.getUidToMessageIdMap().size();
    }

    @Override
    public long countUnseenMessagesInMailbox(final Mailbox<Integer> mailbox)
        throws MailboxException {
        final JamesSession session = (JamesSession) mailboxSession;
        if (!session.isMessagesLoadded()) {
            findMessagesInMailboxBetweenUIDs(mailbox, null, 0, -1);
        }
        int folderId = session.getFolderId(mailbox.getName());
        JamesMailbox folder = session.getFolder(folderId);
        if (folder != null) {
            return folder.getUnseenMessageUids().size();
        } else {
            throw new MailboxNotFoundException("Mailbox [" + mailbox.getName()
                    + "] not found");
        }
    }

    @Override
    public void delete(final Mailbox<Integer> mailbox,
            final Message<Integer> message) throws MailboxException {
        if (messageService != null) {
            final JamesSession session = (JamesSession) mailboxSession;
            int folderId = session.getFolderId(mailbox.getName());
            String messageId = session.getMessageID(folderId, message.getUid());

            MxosRequestMap map = new MxosRequestMap();
            map.add(MailboxProperty.mailboxId, session.getMailboxId());
            map.add(FolderProperty.folderid, folderId);
            map.add(MessageProperty.messageId, messageId);
            try {
                messageService.delete(map.getMultivaluedMap());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new MailboxException("FolderService not found");
        }
    }

    @Override
    public void findInMailbox(final Mailbox<Integer> mbox,
            final MessageRange set, final FetchType fType,
            final MessageCallback<Integer> callback) throws MailboxException {
        final List<Message<Integer>> res;
        final long from = set.getUidFrom();
        final long to = set.getUidTo();
        final int batchSize = set.getBatchSize();
        final Type type = set.getType();

        switch (type) {
            default:
            case ALL:
                res = findMessagesInMailboxBetweenUIDs(mbox, null, 0, -1);
                break;
            case FROM:
                res = findMessagesInMailboxBetweenUIDs(mbox, null, from, -1);
                break;
            case ONE:
                res = findMessageInMailboxWithUID(mbox, from);
                break;
            case RANGE:
                res = findMessagesInMailboxBetweenUIDs(mbox, null, from, to);
                break;
        }
        Collections.sort(res);

        if (batchSize > 0) {
            int i = 0;
            while (i * batchSize < res.size()) {
                callback.onMessages(res.subList(i * batchSize, (i + 1)
                        * batchSize < res.size() ? (i + 1) * batchSize
                        : res.size()));
                i++;
            }
        } else {
            callback.onMessages(res);
        }
    }

    private List<Message<Integer>> findMessageInMailboxWithUID(
            final Mailbox<Integer> mailbox, long uid) throws MailboxException {

        if (messageService != null) {
            List<Message<Integer>> messages = new ArrayList<Message<Integer>>();
            final JamesSession session = (JamesSession) mailboxSession;
            Long userId = session.getMailboxId();
            int folderId = session.getFolderId(mailbox.getName());
            if (!session.isMessagesLoadded()) {
                findMessagesInMailboxBetweenUIDs(mailbox, null, 0, -1);
            }
            String messageId = session.getMessageID(folderId, uid);
            MxosRequestMap map = new MxosRequestMap();
            map.add(MailboxProperty.mailboxId, userId);
            map.add(FolderProperty.folderid, folderId);
            map.add(MessageProperty.messageId, messageId);
            try {
                com.opwvmsg.mxos.data.pojos.Message message = messageService
                        .readSummary(map.getMultivaluedMap());
                if (message != null) {
                    com.opwvmsg.mxos.data.pojos.Message msgBody = messageService
                            .readBody(map.getMultivaluedMap());
                    message.setMessageBody(msgBody.getMessageBody());
                    messages.add(new JamesMessage(folderId, message));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return messages;
        } else {
            throw new MailboxException("FolderService not found");
        }
    }

    private List<Message<Integer>> findMessagesInMailboxBetweenUIDs(
            final Mailbox<Integer> mailbox, final FilenameFilter filter123,
            long from, long to) throws MailboxException {

        if (messageService != null) {
            ArrayList<Message<Integer>> messages =
                new ArrayList<Message<Integer>>();

            final JamesSession session = (JamesSession) mailboxSession;
            Long userId = session.getMailboxId();
            int folderId = session.getFolderId(mailbox.getName());

            MxosRequestMap map = new MxosRequestMap();
            map.add(MailboxProperty.mailboxId, userId);
            map.add(FolderProperty.folderid, folderId);
            try {
                List<com.opwvmsg.mxos.data.pojos.Message> msgs = messageService
                        .readAllMessagesInFolder(map.getMultivaluedMap());
                // TODO: messages between given UIds calculation
                if (from < 0) {
                    from = 0;
                }
                if (to < 0) {
                    to = Integer.MAX_VALUE;
                }
                for (com.opwvmsg.mxos.data.pojos.Message msg : msgs) {
                    if (msg.getUid() >= from && msg.getUid() <= to) {
                        // TODO this might need to remove
                        map.add(MessageProperty.messageId, msg.getMessageId());
                        com.opwvmsg.mxos.data.pojos.Message msgBody =
                            messageService.readBody(map.getMultivaluedMap());

                        msg.setMessageBody(msgBody.getMessageBody());
                        messages.add(new JamesMessage(folderId, msg));

                        // Set all caching data
                        session.setMessageID(folderId, msg.getUid(),
                                msg.getMessageId());
                        JamesMailbox folder = session.getFolder(folderId);
                        if (msg.getFlagRecent()) {
                            List<Long> recentUids = folder
                                    .getRecentMessageUids();
                            recentUids.add(msg.getUid());
                        }
                        if (!msg.getFlagSeen()) {
                            List<Long> unseenUids = folder
                                    .getUnseenMessageUids();
                            unseenUids.add(msg.getUid());
                        }
                    }
                }
                session.setMessagesLoadded(true);
            } catch (Exception e) {
                e.printStackTrace();
                session.setMessagesLoadded(false);
            }
            return messages;
        } else {
            throw new MailboxException("FolderService not found");
        }
    }

    private List<Message<Integer>> findMessagesInMailbox(
            final Mailbox<Integer> mailbox, final FilenameFilter filter,
            int limit) throws MailboxException {

        ArrayList<Message<Integer>> filtered =
            new ArrayList<Message<Integer>>();
        final JamesSession session = (JamesSession) mailboxSession;
        Long userId = session.getMailboxId();
        int folderId = session.getFolderId(mailbox.getName());

        MxosRequestMap map = new MxosRequestMap();
        map.add(MailboxProperty.mailboxId, userId);
        map.add(FolderProperty.folderid, folderId);
        try {
            List<com.opwvmsg.mxos.data.pojos.Message> msgs = messageService
                    .readAllMessagesInFolder(map.getMultivaluedMap());
            if (msgs != null) {
                Iterator<com.opwvmsg.mxos.data.pojos.Message> itr = msgs
                        .iterator();
                for (int i = 0; i < limit && itr.hasNext(); i++) {
                    com.opwvmsg.mxos.data.pojos.Message msg = itr.next();
                    map.add(MessageProperty.messageId, msg.getMessageId());
                    com.opwvmsg.mxos.data.pojos.Message msgBody = messageService
                            .readBody(map.getMultivaluedMap());
                    msg.setMessageBody(msgBody.getMessageBody());
                    filtered.add(new JamesMessage(folderId, msg));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return filtered;

    }

    private List<Message<Integer>> findDeletedMessageInMailboxWithUID(
            final Mailbox<Integer> mailbox, long uid) throws MailboxException {
        return findMessageInMailboxWithUID(mailbox, uid);
    }

    @Override
    public List<Long> findRecentMessageUidsInMailbox(
            final Mailbox<Integer> mailbox) throws MailboxException {

        final JamesSession session = (JamesSession) mailboxSession;
        if (!session.isMessagesLoadded()) {
            findMessagesInMailboxBetweenUIDs(mailbox, null, 0, -1);
        }
        int folderId = session.getFolderId(mailbox.getName());
        JamesMailbox folder = session.getFolder(folderId);
        if (folder != null) {
            return folder.getRecentMessageUids();
        }
        return null;
    }

    @Override
    public Long findFirstUnseenMessageUid(final Mailbox<Integer> mailbox)
        throws MailboxException {
        final JamesSession session = (JamesSession) mailboxSession;

        if (!session.isMessagesLoadded()) {
            findMessagesInMailboxBetweenUIDs(mailbox, null, 0, -1);
        }
        int folderId = session.getFolderId(mailbox.getName());
        JamesMailbox folder = session.getFolder(folderId);
        if (folder != null) {
            if (folder.getUnseenMessageUids().size() > 0) {
                return folder.getUnseenMessageUids().get(0);
            }
        }
        return null;
    }

    @Override
    public void endRequest() {
        // not used

    }

    @Override
    protected MessageMetaData copy(final Mailbox<Integer> mailbox,
            long uid, long modSeq,
            final Message<Integer> original) throws MailboxException {
        SimpleMessage<Integer> theCopy = new SimpleMessage<Integer>(mailbox,
                original);
        try {
            final JamesSession session = (JamesSession) mailboxSession;
            Integer srcFolderId = original.getMailboxId();
            MxosRequestMap formData = new MxosRequestMap();
            Long userId = session.getMailboxId();
            int folderId = session.getFolderId(mailbox.getName());
            formData.add(MailboxProperty.mailboxId, userId);
            formData.add(FolderProperty.srcfolderid, srcFolderId);
            formData.add(FolderProperty.folderid, folderId);
            String messageId = session.getMessageID(srcFolderId,
                    original.getUid());
            formData.add(MessageProperty.messageId, messageId);
            System.out.println("Copy Message Request : " + formData);
            long newUid = messageService.copy(formData.getMultivaluedMap());
            session.setMessageID(folderId, newUid, messageId);
            theCopy.setUid(newUid);
        } catch (Exception e) {
            throw new MailboxException("Copy of message " + original
                    + " failed in mailbox " + mailbox, e);
        }
        return new SimpleMessageMetaData(theCopy);

    }
    /**
     * Method to save the Message.
     * @param mailbox - Mailbox
     * @param message - Message
     * @param messageId - messageId
     * @param copyMsg - true/false
     * @param copyMsgSize - size of the message
     * @return MessageMetaData MetaData
     * @throws MailboxException Exception
     */
    protected MessageMetaData saveInternal(final Mailbox<Integer> mailbox,
            final Message<Integer> message, String messageId, boolean copyMsg,
            long copyMsgSize) throws MailboxException {

        final JamesSession session = (JamesSession) mailboxSession;
        Long userId = session.getMailboxId();
        int folderId = session.getFolderId(mailbox.getName());
        MxosRequestMap formData = new MxosRequestMap();
        formData.add(MailboxProperty.mailboxId, userId);
        formData.add(FolderProperty.folderid, folderId);
        formData.add(MessageProperty.messageId, messageId);
        Map<String, String> headers = null;
        try {
            headers = MxosJamesUtil.getHeaders(message.getHeaderContent());
        } catch (Exception e) {
            System.out.print(e.getCause());
        }
        // TODO
        String from = null;
        String to = null;
        if (headers != null) {
            if (headers.containsKey(MxosJamesUtil.HeaderKey.FROM)) {
                from = MxosJamesUtil.getAsEmail(headers
                        .get(MxosJamesUtil.HeaderKey.FROM));
            } else {
                from = session.getEmail();
            }
            if (headers.containsKey(MxosJamesUtil.HeaderKey.TO)) {
                to = MxosJamesUtil.getAsEmail(headers
                        .get(MxosJamesUtil.HeaderKey.TO));
            } else {
                to = session.getEmail();
            }
            if (headers.containsKey(MxosJamesUtil.HeaderKey.SUBJECT)) {
                formData.add(HeaderProperty.subject,
                        headers.get(MxosJamesUtil.HeaderKey.SUBJECT));
            }
        } else {
            to = session.getEmail();
            from = session.getEmail();
        }
        formData.add(HeaderProperty.receivedfrom, from);
        formData.add(HeaderProperty.sentto, to);
        if (message.isRecent()) {
            formData.add(MessageProperty.flagrecent, "true");
        }
        if (message.isSeen()) {
            formData.add(MessageProperty.flagseen, "true");
        }
        if (message.isAnswered()) {
            formData.add(MessageProperty.flagans, "true");
        }
        if (message.isDeleted()) {
            formData.add(MessageProperty.flagdel, "true");
        }
        if (message.isDraft()) {
            formData.add(MessageProperty.flagdraft, "true");
        }
        if (message.isFlagged()) {
            formData.add(MessageProperty.flagflagged, "true");
        }
        try {
            final String headerSummary = MxosJamesUtil
                    .convertStreamToString(message.getHeaderContent());
            formData.add(HeaderProperty.headersummary, headerSummary);
            if (copyMsg) {
                formData.add("command", "copy");
                formData.add("size", "" + copyMsgSize);
            } else {
                StringBuffer sb = new StringBuffer(headerSummary)
                        .append("\n\r").append(
                                MxosJamesUtil.convertStreamToString(message
                                        .getBodyContent()));
                formData.add(MessageProperty.messagebody, sb.toString());
            }
            StringBuffer sb = new StringBuffer(headerSummary).append("\n\r")
                    .append(MxosJamesUtil.convertStreamToString(message
                            .getBodyContent()));
            formData.add(MessageProperty.messagebody, sb.toString());
            long uid = messageService.create(formData.getMultivaluedMap());
            session.setMessageID(folderId, uid, messageId);
            message.setUid(uid);
        } catch (Exception e) {
            throw new MailboxException("Save of message " + message
                    + " failed in mailbox " + mailbox, e);
        }
        return new SimpleMessageMetaData(message);
    }

    @Override
    protected MessageMetaData save(final Mailbox<Integer> mailbox,
            final Message<Integer> message) throws MailboxException {
        UUID messageId = UUID.randomUUID();
        return saveInternal(mailbox, message, messageId.toString(), false, 0);
    }

    @Override
    protected void begin() throws MailboxException {
        // nothing todo
    }

    @Override
    protected void commit() throws MailboxException {
        // nothing todo
    }

    @Override
    protected void rollback() throws MailboxException {
        // nothing todo
    }

    @Override
    public Iterator<UpdatedFlags> updateFlags(final Mailbox<Integer> mailbox,
            final Flags flags, final boolean value, final boolean replace,
            final MessageRange set) throws MailboxException {

        final List<UpdatedFlags> updatedFlags = new ArrayList<UpdatedFlags>();
        findInMailbox(mailbox, set, FetchType.Metadata,
                new MessageCallback<Integer>() {

                    @Override
                    public void onMessages(List<Message<Integer>> members)
                        throws MailboxException {

                        for (final Message<Integer> message : members) {
                            Flags oldFlags = message.createFlags();
                            if (replace) {
                                message.setFlags(flags);
                            } else {
                                Flags current = message.createFlags();
                                if (value) {
                                    current.add(flags);
                                } else {
                                    current.remove(flags);
                                }
                                message.setFlags(current);
                            }
                            Flags newFlags = message.createFlags();
                            if (UpdatedFlags.flagsChanged(oldFlags, newFlags)) {
                                // increase the mod-seq as we changed the flags
                                final JamesSession session =
                                    (JamesSession) mailboxSession;
                                MxosRequestMap map = new MxosRequestMap();
                                Long userId = session.getMailboxId();
                                int folderId = session.getFolderId(mailbox
                                        .getName());
                                String messageId = session.getMessageID(
                                        folderId, message.getUid());
                                map.add(MailboxProperty.mailboxId, userId);
                                map.add(FolderProperty.folderid, folderId);
                                map.add(MessageProperty.messageId, messageId);
                                setUpdateFlags(oldFlags, newFlags, map);
                                // update Message API
                                try {
                                    messageService.update(map
                                            .getMultivaluedMap());
                                } catch (Exception e) {
                                    System.out.println(e.getCause());
                                }
                            }
                            UpdatedFlags uFlags = new UpdatedFlags(message
                                    .getUid(), message.getModSeq(), oldFlags,
                                    newFlags);
                            updatedFlags.add(uFlags);
                        }
                    }
                });
        return updatedFlags.iterator();
    }

    @Override
    public Map<Long, MessageMetaData> expungeMarkedForDeletionInMailbox(
            final Mailbox<Integer> mbox,
            final MessageRange set) throws MailboxException {

        List<Message<Integer>> res = new ArrayList<Message<Integer>>();
        final long from = set.getUidFrom();
        final long to = set.getUidTo();
        final Type type = set.getType();

        switch (type) {
            default:
            case ALL:
                res = findMessagesInMailbox(mbox, null, -1);
                break;
            case FROM:
                res = findMessagesInMailboxBetweenUIDs(mbox, null, from, -1);
                break;
            case ONE:
                res = findDeletedMessageInMailboxWithUID(mbox, from);
                break;
            case RANGE:
                res = findMessagesInMailboxBetweenUIDs(mbox, null, from, to);
                break;
        }
        Map<Long, MessageMetaData> uids = new HashMap<Long, MessageMetaData>();
        for (int i = 0; i < res.size(); i++) {
            Message<Integer> m = res.get(i);
            if (m.isDeleted()) {
                long uid = m.getUid();
                uids.put(uid, new SimpleMessageMetaData(m));
                delete(mbox, m);
            }
        }
        return uids;
    }

    /*
     * @Override public long getLastUid(final Mailbox<Integer> mailbox) throws
     * MailboxException { final JamesSession session = (JamesSession)
     * this.mailboxSession; return session.getFolderLastUID(mailbox.getName());
     * }
     */
    private void setUpdateFlags(Flags oldFlags, Flags newFlags,
            MxosRequestMap map) {

        if ((oldFlags.contains(Flags.Flag.ANSWERED) ^ newFlags
                .contains(Flags.Flag.ANSWERED))) {
            map.add(MessageProperty.flagans.name(),
                    newFlags.contains(Flags.Flag.ANSWERED) ? "true"
                            : "false");
        }
        if ((oldFlags.contains(Flags.Flag.DELETED) ^ newFlags
                .contains(Flags.Flag.DELETED))) {
            map.add(MessageProperty.flagdel.name(),
                    newFlags.contains(Flags.Flag.DELETED) ? "true"
                            : "false");
        }
        if ((oldFlags.contains(Flags.Flag.DRAFT) ^ newFlags
                .contains(Flags.Flag.DRAFT))) {
            map.add(MessageProperty.flagdraft.name(),
                    newFlags.contains(Flags.Flag.DRAFT) ? "true"
                            : "false");
        }
        if ((oldFlags.contains(Flags.Flag.FLAGGED) ^ newFlags
                .contains(Flags.Flag.FLAGGED))) {
            map.add(MessageProperty.flagflagged.name(),
                    newFlags.contains(Flags.Flag.FLAGGED) ? "true"
                            : "false");
        }
        if ((oldFlags.contains(Flags.Flag.RECENT) ^ newFlags
                .contains(Flags.Flag.RECENT))) {
            map.add(MessageProperty.flagrecent.name(),
                    newFlags.contains(Flags.Flag.RECENT) ? "true"
                            : "false");
        }
        if ((oldFlags.contains(Flags.Flag.SEEN) ^ newFlags
                .contains(Flags.Flag.SEEN))) {
            map.add(MessageProperty.flagseen.name(),
                    newFlags.contains(Flags.Flag.SEEN) ? "true"
                            : "false");
        }
    }
}
