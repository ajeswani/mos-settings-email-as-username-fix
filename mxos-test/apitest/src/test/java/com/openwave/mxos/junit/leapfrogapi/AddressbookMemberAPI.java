package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsMembersService;

public class AddressbookMemberAPI {
	 static Logger alltestlog = Logger.getLogger(AddressbookMemberAPI.class);
	    static String service = "Addressbook GROUP MEMBER API";

	    public static int execute(final IMxOSContext context, ServiceEnum service,
	            Operation operation, String inputstring, String testCaseName,
	            String expectedOutput, String expResult) {

	        int testStatus = 0;
	        MxosRequestMap map;
	        map = createInputParams.create(inputstring, operation);
	       int checkExpOutput = 1;
	        if (null == expectedOutput || expectedOutput == "") {
	            checkExpOutput = 0;
	        }

	        System.out.println("checkExpOutput =" + checkExpOutput);
	        System.out.println("expHttpOut =" + expResult);
	        switch (operation) {
	        case GET: {
	             CommonUtil.printstartAPI("GET ADDRESSBOOK MEMBER");
	             testStatus = read(context, service, map, expResult,
	                            expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
	        case PUT: {
	             CommonUtil.printstartAPI("CREATE ADDRESSBOOK MEMBER");
	             testStatus = create(context, service, map, expResult,
	                                expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
	        case DELETE: {
	             CommonUtil.printstartAPI("DELETE ADDRESSBOOK MEMBER");
	             testStatus = delete(context, service, map, expResult,
	                                expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
	        case DELETEALL: {
	             CommonUtil.printstartAPI("DELETE ALL ADDRESSBOOK MEMBER");
	             testStatus = deleteall(context, service, map, expResult,
	                                expectedOutput, checkExpOutput);
	             return testStatus;
	                    }	                }
	        return 1;
	    }

	    public static int read(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        Member groupBaseMember = null;
	        int result;
	        try {
	            final IGroupsMembersService groupsMembersService = (IGroupsMembersService) 
	            		context.getService(ServiceEnum.GroupsMemberService.name());
	            CommonUtil.printOnBoth("read " + service + "service");
	            groupBaseMember = groupsMembersService.read(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("groupBaseMember = " + groupBaseMember);
	            CommonUtil.printOnBoth("read " + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("read " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        if (result == 1) {
	            if (checkExpOutput == 1 && groupBaseMember != null) {
	                result = CommonUtil.comparePayload(groupBaseMember.toString(),
	                        expectedOutput);
	            }
	        } else {
	            return 0;
	        }
	        return result;
	    }
	    public static int create (final IMxOSContext context,
	            ServiceEnum service, MxosRequestMap map, String expErrorCode,
	            String expectedOutput, int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        String method = "create " + service;
            long groupId;
        try {
	            CommonUtil.printOnBoth(method);
           final IGroupsMembersService groupsMembersService = (IGroupsMembersService) 
           context.getService(ServiceEnum.GroupsMemberService.name());
	        groupId =  groupsMembersService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth("groupId = " + groupId );
            CommonUtil.printOnBoth(method + "...Done");
	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode(method, e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }

	    public static int delete(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        try {
	            final IGroupsMembersService groupsMembersService = (IGroupsMembersService) 
	            context.getService(ServiceEnum.GroupsMemberService.name());
	            CommonUtil.printOnBoth("delete " + service + "service");
	            groupsMembersService.delete(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("delete " + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("delete " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }

	    public static int deleteall(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        try {
	            final IGroupsMembersService groupsMembersService = (IGroupsMembersService) 
	            context.getService(ServiceEnum.GroupsMemberService.name());
	            CommonUtil.printOnBoth("delete ALL  " + service + "service");
	            groupsMembersService.deleteAll(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("delete ALL" + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("delete " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }
	    
}

