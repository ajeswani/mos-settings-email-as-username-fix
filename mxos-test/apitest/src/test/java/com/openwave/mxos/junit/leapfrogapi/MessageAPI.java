package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;
//import com.opwvmsg.mxos.data.enums.HeaderProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
//import com.opwvmsg.mxos.data.pojos.DataMap;
//import com.opwvmsg.mxos.data.pojos.;
//import com.opwvmsg.mxos.data.pojos.MessageMetaData;
//import com.opwvmsg.mxos.data.pojos.SearchNode;
//import com.opwvmsg.mxos.data.pojos.SearchQuery;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
//import com.opwvmsg.mxos.interfaces.service.SearchOperation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.message.pojos.Message;

public class MessageAPI {

    static Logger alltestlog = Logger.getLogger(MessageAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult,
            Map<String, String> mapParams) {

        int testStatus = 0;
        MxosRequestMap map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case PUT: {
            CommonUtil.printstartAPI("Create message");
            testStatus = createMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput, mapParams);
            return testStatus;
        }
        case GET: {
            CommonUtil.printstartAPI("get Message");
            testStatus = readMessage(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        case COPY: {
            CommonUtil.printstartAPI("Copy message");
            testStatus = copyMessage(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
            /*
             * case GETALL: { CommonUtil.printstartAPI("get all Messages");
             * testStatus = readAll(context, service, map, expResult,
             * expectedOutput, checkExpOutput); return testStatus; } case
             * SEARCH: { CommonUtil.printstartAPI("Search all Messages");
             * testStatus = searchMessage(context, service, map, expResult,
             * expectedOutput, checkExpOutput); return testStatus; }
             */
        case COPYALL: {
            CommonUtil.printstartAPI("Copy all messages");
            testStatus = copyAllMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case COPYMULTI: {
            CommonUtil.printstartAPI("Copy multi messages");
            testStatus = copyMultiMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case MOVE: {
            CommonUtil.printstartAPI("Move message");
            testStatus = moveMessage(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        case MOVEALL: {
            CommonUtil.printstartAPI("Move all messages");
            testStatus = moveAllMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case MOVEMULTI: {
            CommonUtil.printstartAPI("Move Multi messages");
            testStatus = moveMultiMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case DELETE: {
            CommonUtil.printstartAPI("Delete message");
            testStatus = deleteMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case DELETEALL: {
            CommonUtil.printstartAPI("Delete all messages");
            testStatus = deleteAllMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case DELETEMULTI: {
            CommonUtil.printstartAPI("Delete Multi messages");
            testStatus = deleteMultiMessage(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        }
        return 1;
    }

    /*
     * This function will be user for creating a message
     */
    private static int createMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput, Map<String, String> mapParams) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Create Message";
        String msgId = null;

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            msgId = messageService.create(map.getMultivaluedMap());
            System.out.println("Message ID :: " + msgId);
            if (mapParams.containsKey(MessageProperty.messageId.name())) {
                LeapFrogTest.placeholderCache.put(
                        mapParams.get(MessageProperty.messageId.name()), msgId);
            }
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && msgId != null) {
                result = CommonUtil.comparePayload(msgId, expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    /*
     * This function will be user for reading single message from a folder using
     * messageId
     */
    private static int readMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Message msg = null;
        String method = "Read message";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            msg = messageService.read(multiMap);
            CommonUtil.printOnBoth(method + " ..done");
            CommonUtil.printOnBoth(msg + " ..message");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && msg != null) {
                result = CommonUtil.comparePayload(msg.toString(),
                        expectedOutput);

            }
        } else {
            return 0;
        }
        return result;
    }

    /*
     * This function will be user for copying single message from a folder using
     * messageId
     */
    private static int copyMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Copy Message";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.copy(multiMap);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    /*private static int readAll(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Map<String, MessageMetaData> msgAll = null;
        String method = "Read all message";
        
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            //msgAll = messageService.readAllMessageMetaDatasInFolder(map.getMultivaluedMap());
            msgAll = messageService.readAllMessageMetaDatasInFolder(multiMap);
            
            System.out.println("msgAll ============ "+msgAll);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && msgAll != null) {
                result = CommonUtil.comparePayload(msgAll.toString(),
                        expectedOutput);

            }
        } else {
            return 0;
        }
        return result;
    }*/
    
    /*private static int searchMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Map<String, Message> searchMsgs = null;
        String method = "Search message";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.parseQueryStringToReplacePlaceholder(multiMap,"query");

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            System.out.println("multiMap 1 ===========+++++ "+multiMap);
            //multiMap.remove("query");
            //String query = "msgId:==:<133-2013-0326-052432-3909@rwcvmx83c0099.openwave.com>&&msgId:==:<134-2013-0326-052432-3909@rwcvmx83c0099.openwave.com>";
          
            //String query = "msgId:==:<90-2013-0423-035847-5967@rwcvmx83c0099.openwave.com>||msgId:==:<84-2013-0423-034433-5967@rwcvmx83c0099.openwave.com>";
            //multiMap.get("query").clear();
            //multiMap.get("query").add(query);
            //System.out.println("context===="+context.toString());
            
           // System.out.println("++++++before SearchQueryUtils+++++++");
            //System.out.println("SearchQueryUtils.parse(multiMap) = "+SearchQueryUtils.parse(multiMap));
            searchMsgs = messageService.searchMessagesInFolder(multiMap,
                    SearchQueryUtils.parse(multiMap));
           // System.out.println("++++++After SearchQueryUtils+++++++");
          //  System.out.println("*** search output:: " + searchMsgs.size());
            System.out.println("searchMsg output:  "+searchMsgs);
            CommonUtil.printOnBoth(method + "...done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && searchMsgs != null) {
                result = CommonUtil.comparePayload(searchMsgs.toString(),
                        expectedOutput);

            }
        } else {
            return 0;
        }
        return result;
    }*/

    private static int copyAllMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Copy All Messages";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.copyAll(multiMap);

            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    /*
     * This function will be used for copying multiple messages from a folder using
     * messagedId
     */
    private static int copyMultiMessage(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Copy Multi Messages";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());
        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.copyMulti(multiMap);

            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    /*
     * This function will be used for moving single message from a folder using
     * messagedId
     */
    private static int moveMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Move Message";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.move(multiMap);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    /* This function will be used for moving 
     * all messages
     */
    private static int moveAllMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Move All Messages";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.moveAll(multiMap);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    /*
     * This function will be used for moving multiple messages from a folder
     * using messagedId
     */   
    private static int moveMultiMessage(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Move Multi Message";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.moveMulti(multiMap);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    /*
     * This function will be used for deleting single messages from a folder
     * using messagedId
     */
    private static int deleteMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Delete Message";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.delete(multiMap);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    /* This function will be used for deleting 
     * all the messages from a folder
     */
    private static int deleteAllMessage(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Delete All Messages";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.deleteAll(multiMap);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    /*
     * This Function will be used for deleting multiple messages from a folders
     * using messagedId
     */
    private static int deleteMultiMessage(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Delete Multi Messages";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMessageService messageService = (IMessageService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            messageService.deleteMulti(multiMap);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}


