package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosBaseService;

public class CosBaseAPI {
    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case GET: {
            CommonUtil.printstartAPI("GET COS BASESERVICE");
            testStatus = read(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        case POST: {
            CommonUtil.printstartAPI("UPDATE COS BASESERVICE");
            testStatus = update(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case GETALL: {
            CommonUtil.printstartAPI("SEARCH COS BASESERVICE");
            testStatus = search(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        }
        return 1;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read CosBaseService";
        Base basecos = null;
        try {
            ICosBaseService CosBaseService = (ICosBaseService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            basecos = CosBaseService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("basecos = " + basecos);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && basecos != null) {
                result = CommonUtil.comparePayload(basecos.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update CosBaseService";
        try {
            ICosBaseService CosBaseService = (ICosBaseService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            CosBaseService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int search(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "search CosBaseService";
        List<Base> basecos = null;
        try {
            ICosBaseService CosBaseService = (ICosBaseService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            basecos = CosBaseService.search(map.getMultivaluedMap());
            CommonUtil.printOnBoth("basecos = " + basecos.toString());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && basecos != null) {
                result = CommonUtil.comparePayload(basecos.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

}
