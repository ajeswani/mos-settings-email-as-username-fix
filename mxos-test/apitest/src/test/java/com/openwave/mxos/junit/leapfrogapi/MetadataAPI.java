package com.openwave.mxos.junit.leapfrogapi;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.message.search.SearchTermParser;

public class MetadataAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
        case MetadataService: {
            switch (operation) {
            case LIST: {
                CommonUtil.printstartAPI("List Metadata");
                testStatus = metadataList(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case LISTUID: {
                CommonUtil.printstartAPI("GET Metadata UUIDs and UIDs");
                testStatus = listMessageUIDs(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case SEARCH: {
                CommonUtil.printstartAPI("Search Metadata");
                testStatus = search(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        }
        return 1;
    }
   
    /*
     * This method will list all the message metadata in particular folder
     */
    private static int metadataList(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Map<String, Metadata> msg = null;
        String method = "Read Metadata list ";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMetadataService metadataService = (IMetadataService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            msg = metadataService.list(multiMap);
            CommonUtil.printOnBoth(method + " ..done");
            CommonUtil.printOnBoth(msg + " ..message");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && msg != null) {
                result = CommonUtil.comparePayload(msg.toString(),
                        expectedOutput);

            }
        } else {
            return 0;
        }
        return result;
    }
    
    /*
     * This function will list the UUID and UID in a particular folder
     */
    private static int listMessageUIDs(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Map<String, Metadata> msgUUIDsUIDs = null;
        String method = "Read message UUID and UID";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();

        try {
            IMetadataService metadataService = (IMetadataService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            msgUUIDsUIDs = metadataService.listUIDs(multiMap);
            CommonUtil.printOnBoth(method + " ..done");
            CommonUtil.printOnBoth(msgUUIDsUIDs + " ..msgUUIDsUIDs");
            Iterator entries = msgUUIDsUIDs.entrySet().iterator();
            while (entries.hasNext()) {
                Entry thisEntry = (Entry) entries.next();
                String key = (String) thisEntry.getKey();
                Metadata metadata = (Metadata) thisEntry.getValue();
                if (metadata.getUid() != null) {
                    if (metadata.getArrivalTime() != null
                            || metadata.getFlagAns() != null
                            || metadata.getFlagBounce() != null
                            || metadata.getFlagDel() != null
                            || metadata.getFlagDraft() != null
                            || metadata.getFlagFlagged() != null
                            || metadata.getFlagPriv() != null
                            || metadata.getFlagRecent() != null
                            || metadata.getFlagSeen() != null
                            || metadata.getFlagUnread() != null
                            || metadata.getHasAttachments() != null
                            || metadata.getBcc() != null
                            || metadata.getBlobMessageId() != null
                            || metadata.getCc() != null
                            || metadata.getDeliverNDR() != null
                            || metadata.getExpireOn() != null
                            || metadata.getFlagRichMail() != null
                            || metadata.getFolderId() != null
                            || metadata.getFrom() != null
                            || metadata.getInReplyTo() != null
                            || metadata.getKeywords() != null
                            || metadata.getPopDeletedFlag() != null
                            || metadata.getPriority() != null
                            || metadata.getReferences() != null
                            || metadata.getReplyTo() != null
                            || metadata.getSubject() != null
                            || metadata.getThreadId() != null
                            || metadata.getSize() != null
                            || metadata.getSentDate() != null
                            || metadata.getTo() != null
                            || metadata.getType() != null
                            || !metadata.getAdditionalProperties().isEmpty()) {
                        System.out.println("checking posos");
                        return 0;
                    }
                } else {
                    System.out.println("Not checking posos");
                    return 0;
                }
                CommonUtil.printOnBoth("uuid : " + key);
                CommonUtil.printOnBoth("uid : " + metadata.getUid());
            }

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && msgUUIDsUIDs != null) {
                result = CommonUtil.comparePayload(msgUUIDsUIDs.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }
    
    /* This method will search metadata of all messages */
    private static int search(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Map<String, Map<String, Metadata>> msg = null;
        String method = "Search Message Metadata";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            IMetadataService metadataService = (IMetadataService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            String query = multiMap.get("query").get(0);
            System.out.println("Query 1 : " + query);
            SearchTerm searchTerm = SearchTermParser.parse(query);
            System.out.println("After the search term creation");
            msg = metadataService.search(multiMap, searchTerm);
            CommonUtil.printOnBoth(method + " ..done");
            CommonUtil.printOnBoth(msg + " ..message");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && msg != null) {
                result = CommonUtil.comparePayload(msg.toString(),
                        expectedOutput);

            }
        } else {
            return 0;
        }
        return result;
    }
}
