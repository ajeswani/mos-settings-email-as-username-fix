package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoAddressService;

public class AddressbookContactsWorkInfoAddressAPI {

	    static Logger alltestlog = Logger.getLogger(AddressbookContactsWorkInfoAddressAPI.class);
	    static String service = "Addressbook Contacts WorkInfo Address API";

	    public static int execute(final IMxOSContext context, ServiceEnum service,
	            Operation operation, String inputstring, String testCaseName,
	            String expectedOutput, String expResult) {

	        int testStatus = 0;
	        MxosRequestMap map;
	        map = createInputParams.create(inputstring, operation);
	        
	        int checkExpOutput = 1;
	        if (null == expectedOutput || expectedOutput == "") {
	            checkExpOutput = 0;
	        }

	        System.out.println("checkExpOutput =" + checkExpOutput);
	        System.out.println("expHttpOut =" + expResult);
	        switch (operation) {
	        case GET: {
	             CommonUtil.printstartAPI("GET ADDRESSBOOK CONTACTS WORKINFO Address");
	             testStatus = read(context, service, map, expResult,
	                            expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
	        case POST: {
	             CommonUtil.printstartAPI("UPDATE ADDRESSBOOK CONTACTS WORKINFO Address");
	             testStatus = update(context, service, map, expResult,
	                                expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
	                }
	        return 1;
	    }

	    public static int read(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        Address workInfoAddress = null;
	        int result;
	        try {
	            final IContactsWorkInfoAddressService contactsWorkInfoAddressService =
	                (IContactsWorkInfoAddressService) context.getService(service.name());
	            CommonUtil.printOnBoth("read " + service + "service");
	            workInfoAddress = contactsWorkInfoAddressService.read(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("contact workInfo Address= " + workInfoAddress);
	            CommonUtil.printOnBoth("read " + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("read " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        if (result == 1) {
	            if (checkExpOutput == 1 && workInfoAddress != null) {
	                result = CommonUtil.comparePayload(workInfoAddress.toString(),
	                        expectedOutput);
	            }
	        } else {
	            return 0;
	        }
	        return result;
	    }

	    public static int update(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        try {
	            final IContactsWorkInfoAddressService contactsWorkInfoAddressService =
	                (IContactsWorkInfoAddressService) context.getService(service.name());
	            CommonUtil.printOnBoth("update " + service + "service");
	            contactsWorkInfoAddressService.update(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("update " + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("update " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }	
}
