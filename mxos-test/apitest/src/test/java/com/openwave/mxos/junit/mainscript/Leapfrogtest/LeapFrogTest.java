package com.openwave.mxos.junit.mainscript.Leapfrogtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Test;

import com.openwave.mxos.junit.helper.ApiFactory;
import com.openwave.mxos.junit.helper.ErrorCodes;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.addressbook.pojos.Cookies;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

/**
 * This is main Class and it would Initialize variables, create services for
 * mailbox,message and folder reads xls sheet execute the test cases
 * 
 * @author mxos-test
 */

public class LeapFrogTest {

    private static String testcaseFilePath;
    private IMxOSContext context = null;

    private static String messageId1 = getTimeUUID();
    static int totalNoTestcase = 0;
    static int totalTestcasePass = 0;
    static int totalTestcaseFail = 0;
    static int errorCount = 0;
    static int flag = 1;
    public static List groupidarray;
    public static List msgidarray;
    public static List sessionidarray;
    public static List cookiesarray;
    public static List contactIdArray;
	public static final Map<String, String> placeholderCache =
		new HashMap<String, String>();
	public static Cookies cookies;
    private static String priority = null;
    public static String confHost = null;
    public static String confUser = null;
    public static String confPass = null;
    public static String mxosBaseUrl = null;
    public static String mxosMaxConnections = null;
    public static String noUserPass = "yes";
    static String contextClassName = null;
    public static String customVal = "false";

    HashMap errorCodeHash;
    public static int amar = 0;
    static Logger alltestlog = Logger.getLogger(LeapFrogTest.class);
    static Logger summary = Logger.getLogger("summarylogs");

    /**
     * This constructor creates mailboxSrevice creates folderService creates
     * messageService
     */

    public LeapFrogTest() {
        alltestlog.info("****************** Start ********************");

        String mycwd1 = System.getProperty("user.dir");
        System.out.println("mycwd ==  " + mycwd1);
        String mycwd = mycwd1
                + "/config/mxos-api-test-log4j.xml";
        DOMConfigurator.configure(mycwd);

        String mxosconfigfile = mycwd1
                + "/config/mxos-api-test.properties";
        alltestlog.info("Config file path == " + mxosconfigfile);
        Properties propfile = new Properties();

        try {
            propfile.load(new FileInputStream(mxosconfigfile));
        } catch (FileNotFoundException e) {
            alltestlog.error("Unable to find test properties file " + propfile);
            System.err.println("Unable to find test properties file "
                    + propfile);
        } catch (IOException e) {
            System.err.println("Unable to open test properties file "
                    + propfile);
            alltestlog.error("Unable to open test properties file " + propfile);
        }

        testcaseFilePath = propfile.getProperty("testcaseFiles");
        priority = propfile.getProperty("TcExecOfPriority");
        confHost = propfile.getProperty("configHost");
        alltestlog.info("confHost " + confHost);
        confUser = propfile.getProperty("configUser");
        alltestlog.info("confUser " + confUser);
        confPass = propfile.getProperty("configPass");
        alltestlog.info("confPass " + confPass);
        contextClassName = propfile.getProperty("contextClassName");
        alltestlog.info("contextClassName ==  " + contextClassName);
        mxosBaseUrl = propfile.getProperty("mxosBaseUrl");
        alltestlog.info("mxosBaseUrl ==  " + mxosBaseUrl);
        mxosMaxConnections = propfile.getProperty("mxosMaxConnections");
        alltestlog.info("mxosMaxConnections ==  " + mxosMaxConnections);
        customVal = propfile.getProperty("loggingApiflag");
        if (customVal == null) {
            customVal = "false";
        }
        alltestlog.info("customVal ==  " + customVal);

        if (null == contextClassName || contextClassName == "") {
            alltestlog
                    .info("Please enter valid contextClassName in mxos.properties");
            System.out
                    .println("Please enter valid contextClassName in mxos.properties");
            System.exit(0);
        }
        contextClassName = contextClassName.replaceAll("\\s", "");
        System.out.println("contextClassName ==  " + contextClassName);

        // /*/mxos/contextClassName

        String testcasePath = System.getProperty("testcasePath", "progression");
        System.out.println("testcasePath ==  " + testcasePath);

        testcaseFilePath = mycwd1 + "/" + testcasePath;
        System.out.println("testcaseFilePath ==  " + testcaseFilePath);
        alltestlog.info("testcaseFilePath ==  " + testcaseFilePath);
        alltestlog.info("priority ==  " + priority);
        if (priority == null) {
            priority = "p1,p2,p3";
        }
        if (confUser == null || confUser == "" || confPass == null
                || confPass == "") {
            noUserPass = "no";
        }
        alltestlog.info("priority ===  " + priority);

        alltestlog.info("Creating Hashmap for available Error codes");
        System.out.println("mycwd1 ***** ==  " + mycwd1);
        mycwd1 = mycwd1.replaceAll("apitest", "");
        System.out.println("mycwd1 ***** ==  " + mycwd1);
        mycwd1 = mycwd1.replaceAll("mxos-test", "");
        System.out.println("mycwd1 ***** ==  " + mycwd1);

        String xmlpath = mycwd1;
        String PathForXml = xmlpath + "config/mxos";
        System.out.println("PathForXml ***** ==  " + PathForXml);

        // String PathForXml = "D:\\Workspace\\mxos11\\config\\mxos";
        errorCodeHash = ErrorCodes.geterrorcode(PathForXml);

        Properties p = new Properties();
        /*
         * if (contextClassName.equals("STUB")) {
         * p.setProperty(MxOSContextFactory.MXOS_CONTEXT_ID,
         * ServiceEnum.Context.STUB.name()); } else if
         * (contextClassName.equals("BACKEND")) {
         * p.setProperty(MxOSContextFactory.MXOS_CONTEXT_ID,
         * ServiceEnum.Context.BACKEND.name()); }
         */
        if (contextClassName.equals("STUB")) {
            p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE, "STUB");
        } else if (contextClassName.equals("BACKEND")) {
            p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE, "BACKEND");
        } else if (contextClassName.equals("REST")) {

            if (null == mxosBaseUrl || mxosBaseUrl == "") {
                alltestlog
                        .info("Please enter valid mxosBaseUrl in mxos.properties");
                System.out
                        .println("Please enter valid mxosBaseUrl in mxos.properties");
                System.exit(0);
            }

            if (null == mxosMaxConnections || mxosMaxConnections == "") {
                alltestlog
                        .info("mxosMaxConnections is not defined in mxos.properties default value would be 10");
                System.out
                        .println("mxosMaxConnections is not defined in mxos.properties default value would be 10");
            }
            p.setProperty("contextType", "REST");
            p.setProperty("mxosBaseUrl", mxosBaseUrl);
            p.setProperty("custom", customVal);
            p.setProperty("mxosMaxConnections", mxosMaxConnections);
            p.setProperty("custom", "false");
        } else {
            alltestlog.info("contextClassName should be STUB/REST/BACKEND ");
            System.out.println("contextClassName should be STUB/REST/BACKEND ");
            System.exit(0);
        }

        try {
            String contextId = "TEST-MXOS-2.0";
            context = MxOSContextFactory.getInstance().getContext(contextId, p);

            System.out.println("MXOS_CONTEXT_TYPE = "
                    + p.getProperty(ContextProperty.MXOS_CONTEXT_TYPE));

        } catch (Exception e) {
            System.err.println("Unable to get required services");
            System.err.println("Error = " + e.getMessage());
            e.printStackTrace();
            System.err.println("Error stack trace = " + e.getStackTrace());
            alltestlog.error("Unable to get required services");
            alltestlog.error("Error = " + e.getMessage());
            System.exit(0);
        }
        summary.info("***************  START TEST CASES ***************");
        alltestlog.error("***************  START TEST CASES ***************");
        // sshtemp.sshexec();

    }

    /**
     * This function would return the UUID which would be used in test cases as
     * message id
     */
    // public static String getTimeUUID() {
    // return new com.eaio.uuid.UUID().toString();
    // }

    public static String getTimeUUID() {
        // java.util.UUID msguuid =
        // java.util.UUID.fromString(new com.eaio.uuid.UUID().toString());
        // return msguuid.toString();
        return null;
    }

    /**
     * This method would get the input parameters from .xls sheet execute the
     * test case print the result
     */

    @Test
    public void test() throws Exception {

        Sheet readSheet = null;
        String sheetName = "";
        int numberOfReadSheets;

        File testcaseFolder = new File(testcaseFilePath);
        File[] listOfFiles = testcaseFolder.listFiles();
        System.out.println("Total Number of files:: " + listOfFiles.length);

        for (File file : listOfFiles) {

            Pattern pattern = Pattern.compile("\\~\\$");
            Matcher matcher = pattern.matcher(file.toString());

            if (matcher.find()) {
                alltestlog.info("SKIP: Reading file :: " + file);
                continue;
            }

            Pattern pattern1 = Pattern.compile("[xls,xlsx]$");
            Matcher matcher1 = pattern1.matcher(file.toString());

            if (!matcher1.find()) {
                alltestlog.info("SKIP: Reading file :: " + file);
                continue;
            }

            System.out.println("Reading file :: " + file);
            alltestlog.info("Reading file :: " + file);

            summary.info("----------------------------------------------------------------------------------------");
            summary.info("Executing File " + file);
            summary.info("----------------------------------------------------------------------------------------");

            InputStream read_InputStream = new FileInputStream(file);
            Workbook read_WB = WorkbookFactory.create(read_InputStream);
            numberOfReadSheets = read_WB.getNumberOfSheets();

            alltestlog.info("Total number of Tabs =" + numberOfReadSheets);
            System.out.println("Total number of Tabs =" + numberOfReadSheets);

            for (int sheetno = 1; sheetno < numberOfReadSheets; sheetno++) {

                readSheet = read_WB.getSheetAt(sheetno);
                // sheetName = readSheet.getSheetName();
                sheetName = "Executing test case present at sheet no = "
                        + sheetno;
                String testCaseName = null;
                String test_end = null;
                ServiceEnum service = null;
                Operation operation = null;
                String Priority = null;
                String inputParams = null;
                String expResult = null;
                String expectedOutput = null;
                String messagidarray[];
                messagidarray = null;
                System.out.println("Executing Tab(Sheet) number = " + sheetno);
                alltestlog.info("Executing Tab(Sheet) number = " + sheetno);
                int rowCount = 0;
                int testEnd = 0;
                int stepskip = 0;
                int donotexec = 0;
                String tcNum = null;
                sessionidarray = new ArrayList<Long>();
                cookiesarray = new ArrayList<Long>();
                contactIdArray = new ArrayList<Long>();
                placeholderCache.clear();
               
                Outer: for (Row inputRow : readSheet) {
                	// We need this map per row. After each row, this should be reset.
                    Map<String, String> mapParams = null;

                    // alltestlog.info("=====================================");
                    System.out.println("=====================================");
                    int count = 0;
                    int notc = 0;
                    for (int cellCount = 0; cellCount < 10; cellCount++) {
                        // System.out.println("cellCount ===  " + cellCount);

                        String token = "";
                        if (rowCount == 0) {
                            continue;
                        }
                        if (donotexec == 1) {
                            String token2 = "";
                            Cell inputCell2 = inputRow.getCell(2);
                            if (null != inputCell2
                                    && inputCell2.toString() != "") {
                                token2 = inputCell2.toString().trim();
                            }
                            if (token2.contains("END")
                                    || token2.contains("end")) {
                                donotexec = 0;
                            }
                            continue Outer;
                        }
                        Cell inputCell = inputRow.getCell(cellCount);
                        if (null != inputCell && inputCell.toString() != "") {
                            token = inputCell.toString().trim();
                        }
                        if (count == 0) {
                            tcNum = token;
                        }
                        if (count == 1) {
                            if (null == inputCell || inputCell.toString() == "") {
                                notc = 1;
                                count++;
                                continue;
                            }
                        }
                        if (count == 2 && notc == 1) {
                            if (null == inputCell || inputCell.toString() == "") {
                                count++;
                                continue;
                            }
                        }
                        if (count == 1) {
                            Cell inputCell1 = inputRow.getCell(3);
                            String token1 = "";
                            if (null != inputCell1
                                    && inputCell1.toString() != "") {
                                token1 = inputCell1.toString().trim();
                            }
                            if (priority.toUpperCase().contains(
                                    token1.toUpperCase())) {
                                alltestlog.info(" ****Prority contains token1 "
                                        + token1);
                            } else {
                                alltestlog
                                        .info(" ****Prority not contains token1 "
                                                + token1);
                                Cell inputCell11 = inputRow.getCell(2);
                                String token11 = "";
                                if (null != inputCell11
                                        && inputCell11.toString() != "") {
                                    token11 = inputCell11.toString().trim();
                                }
                                alltestlog
                                        .info(" ****Prority not contains token11 "
                                                + token11);
                                if (token11.contains("END")
                                        || token11.contains("end")) {
                                    donotexec = 0;
                                    continue Outer;
                                }

                                donotexec = 1;
                                continue Outer;
                            }

                            testCaseName = token;
                            testEnd = 0;
                            stepskip = 0;
                            if (flag == 1) {
                                alltestlog
                                        .info("===================================="
                                                + "=============================");
                                System.out.println("=====  Start: " + tcNum
                                        + ") " + testCaseName + " ======");
                                alltestlog.info("=====  Start: " + tcNum + ") "
                                        + testCaseName + " ======");
                                alltestlog
                                        .info("===================================="
                                                + "=============================");
                                summary.info("=====  Start: " + tcNum + ") "
                                        + testCaseName + " ======");
                                msgidarray = new ArrayList<Long>();
                                groupidarray = new ArrayList<Long>();
                                cookies = null;
                                amar = 0;
                                System.out.println(" amar == " + amar);
                            }
                            flag = 0;
                        } else if (count == 2) {
                            test_end = token;
                            if (test_end.equalsIgnoreCase(LfConstants.TESTEND)) {
                                testEnd = 1;
                            }
                            if (test_end.equalsIgnoreCase(LfConstants.STEPSKIP)) {
                                stepskip = 1;
                                System.out.println("Skipping teststep ");
                                alltestlog.info("Skipping teststep ");
                                break;
                            }
                            if (test_end
                                    .equalsIgnoreCase(LfConstants.TESTENDSKIP)) {
                                stepskip = 1;
                                testEnd = 1;
                                System.out.println("Skipping teststep ");
                                alltestlog.info("Skipping teststep ");
                                printEnd(testCaseName, 0, errorCount);
                                break;
                            }
                        } else if (count == 3) {
                            if (null == inputCell || inputCell.toString() == "") {
                                count++;
                                continue;
                            }
                            Priority = token;
                        } else if (count == 4) {

                            if (null == inputCell || inputCell.toString() == "") {
                                continue Outer;
                            }
                            service = ServiceEnum.valueOf(token);
                        } else if (count == 5) {

                            if (null == inputCell || inputCell.toString() == "") {
                                continue Outer;
                            }
                            operation = Operation.valueOf(token);
                        } else if (count == 6) {
                            inputParams = token.replaceAll("\n", "\r\n");
                        } else if (count == 7) {
                            alltestlog.info("***************************");
                            alltestlog.info("expResult string = " + token);

                            if (null != token || token != "") {
                                expResult = (String) errorCodeHash.get(token);
                                expResult = token + "," + expResult;

                            }
                            if (token.contains("api_success")) {
                                expResult = "200";
                            }
                            if (null == expResult || expResult == "") {
                                expResult = "";
                                // stepskip = 1;
                                System.out.println("Skipping teststep ");
                            }
                            alltestlog.info("expResult value = " + expResult);
                        } else if (count == 8) {
                            expectedOutput = token;
                        } else if (count == 9) {
                        	if (null != token && !"".equals(token)) {
                        		mapParams = createInputParams.createParamsMap(token);
                        	}
                        }
                        count++;
                    }
                    rowCount++;
                    if (rowCount != 1 && stepskip == 0) {

                        alltestlog.info("---------------------------");
                        // alltestlog.info("....Excel data ... ");
                        // alltestlog.info("rowCount" + rowCount);
                        // alltestlog.info("testCaseName = " + testCaseName);
                        // alltestlog.info("testEnd = " + testEnd);
                        alltestlog.info("Service = " + service.name());
                        alltestlog.info("Operation = " + operation.name());
                        alltestlog.info("Priority = " + Priority);
                        // alltestlog.info("inputParams  = " + inputParams);
                        // alltestlog.info("expResult = " + expResult);
                        // alltestlog.info("expected out =" + expectedOutput);
                        // alltestlog.info("***************************");

                        int testStatus1 = ApiFactory.execute(
                                context, service, operation, inputParams, testCaseName,
                                expectedOutput, expResult, mapParams);
                        if (testStatus1 == 0) {
                            errorCount++;
                        }
                        if (testEnd == 1) {
                            printEnd(testCaseName, testStatus1, errorCount);
                        }

                    }
                }
            }
        }

        System.out.println("********************************************");
        System.out.println("Total No. of Test cases executed = "
                + totalNoTestcase);
        System.out.println("Total No. of PASS test cases     = "
                + totalTestcasePass);
        System.out.println("Total No. of FAIL test cases     = "
                + totalTestcaseFail);
        System.out.println("********************************************");

        summary.info("********************************************");
        summary.info("Total No. of Test cases executed = " + totalNoTestcase);
        summary.info("Total No. of PASS test cases     = " + totalTestcasePass);
        summary.info("Total No. of FAIL test cases     = " + totalTestcaseFail);
        summary.info("********************************************");

        alltestlog
                .info("Total No. of Test cases executed = " + totalNoTestcase);
        alltestlog.info("Total No. of PASS test cases     = "
                + totalTestcasePass);
        alltestlog.info("Total No. of FAIL test cases     = "
                + totalTestcaseFail);
        alltestlog.info("********************************************");

    }

    /**
     * This function would print End message for test case
     */
    public static void printEnd(String testcaseName, int testStatus, int errCnt) {

        if (errCnt != 0) {
            totalNoTestcase++;
            totalTestcaseFail++;
            System.err.println("----- Found " + errCnt + " Errors ---- ");
            System.err.println("----- Test case Fail ---- ");
            alltestlog.error("----- Found " + errCnt + " Errors ---- ");
            alltestlog.error("----- Test case Fail ---- ");
            summary.info("TEST : FAIL");
        } else {
            totalNoTestcase++;
            totalTestcasePass++;
            System.out.println("----- Test case Pass ---- ");
            alltestlog.info("----- Test case Pass ---- ");
            summary.info("TEST : PASS");
        }

        System.out.println("=====  End: " + testcaseName + " ======");
        System.out
                .println("==================================================");
        alltestlog.info("=====  End: " + testcaseName + " ======");
        // alltestlog.info("==================================================");
        errorCount = 0;
        flag = 1;
    }

}
