package com.openwave.mxos.junit.leapfrogapi;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.McAfeeFilters;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
//import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendBMIFiltersService;
//import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendCommtouchFiltersService;
//import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendFiltersService;
//import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendMcAfeeFiltersService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendBMIFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendCommtouchFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendMcAfeeFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendService;
import com.opwvmsg.mxos.interfaces.service.mailbox.INumDelayedDeliveryMessagesPendingService;

public class SendMailAPI {

    static Logger alltestlog = Logger.getLogger(MailReceiptAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case MailSendService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET MAILSEND");
                        testStatus = readMailSendService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE MAILSEND");
                        testStatus = updateMailSendService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MailSendFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET MAILSEND FILTERS");
                        testStatus = readMailSendFiltersService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MailSendBMIFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET MAILSEND BMIFILTERS");
                        testStatus = readMailSendBMIFiltersService(context, service,
                                map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE MAILSEND BMIFILTERS");
                        testStatus = updateMailSendBMIFiltersService(context, service,
                                map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MailSendCommtouchFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET MAILSEND COMMTOUCHFILTERS");
                        testStatus = readMailSendCommtouchFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE MAILSEND COMMTOUCHFILTERS");
                        testStatus = updateMailSendCommtouchFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MailSendMcAfeeFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET MAILSEND MCAFEEFILTERS");
                        testStatus = readMailSendMcAfeeFiltersService(context, service,
                                map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE MAILSEND MCAFEEFILTERS");
                        testStatus = updateMailSendMcAfeeFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosMailSendService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS MAILSEND");
                        testStatus = readCosMailSendService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS MAILSEND");
                        testStatus = updateCosMailSendService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MailSendDeliveryMessagesPendingService: {
                switch (operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("CREATE SEND MAIL DELIVERY MESSAGE PENDING");
                        testStatus = createDeliveryMessagesPendingService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case GET: {
                        CommonUtil.printstartAPI("GET SEND MAIL DELIVERY MESSAGE PENDING");
                        testStatus = readDeliveryMessagesPendingService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE SEND MAIL DELIVERY MESSAGE PENDING");
                        testStatus = updateDeliveryMessagesPendingService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETE: {
                        CommonUtil.printstartAPI("DELETE SEND MAIL DELIVERY MESSAGE PENDING");
                        testStatus = deleteDeliveryMessagesPendingService(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
/*           case CosMailSendFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS MAILSEND FILTERS");
                        testStatus = readCosMailSendFiltersService(context, service,
                                map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosMailSendBMIFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS MAILSEND BMIFILTERS");
                        testStatus = readCosMailSendBMIFiltersService(context, service,
                                map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS MAILSEND BMIFILTERS");
                        testStatus = updateCosMailSendBMIFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosMailSendCommtouchFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS MAILSEND COMMTOUCHFILTERS");
                        testStatus = readCosMailSendCommtouchFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil
                                .printstartAPI("UPDATE COS MAILSEND COMMTOUCHFILTERS");
                        testStatus = updateCosMailSendCommtouchFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosMailSendMcAfeeFiltersService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS MAILSEND MCAFEEFILTERS");
                        testStatus = readCosMailSendMcAfeeFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS MAILSEND MCAFEEFILTERS");
                        testStatus = updateCosMailSendMcAfeeFiltersService(context,
                                service, map, expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    } 
                }
            }
*/        }
        return 1;
    }

    public static int readMailSendService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        MailSend mailsend = null;
        String method = "read MailSendService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendService MailSendService = (IMailSendService) context
                    .getService(service.name());

            mailsend = MailSendService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("mailsend = " + mailsend);

            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailsend != null) {
                result = CommonUtil.comparePayload(mailsend.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateMailSendService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update MailSendService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendService MailSendService = (IMailSendService) context
                    .getService(service.name());

            MailSendService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readMailSendFiltersService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Filters mailsendfilter = null;
        String method = "read MailSendFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendFiltersService MailSendFiltersService = (IMailSendFiltersService) context
                    .getService(service.name());

            mailsendfilter = MailSendFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("mailsendfilter = " + mailsendfilter);

            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailsendfilter != null) {
                result = CommonUtil.comparePayload(mailsendfilter.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int readMailSendBMIFiltersService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        BmiFilters bmifilter = null;
        String method = "read MailSendBMIFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendBMIFiltersService MailSendBMIFiltersService = (IMailSendBMIFiltersService) context
                    .getService(service.name());

            bmifilter = MailSendBMIFiltersService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("bmifilter = " + bmifilter);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && bmifilter != null) {
                result = CommonUtil.comparePayload(bmifilter.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateMailSendBMIFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update MailSendBMIFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendBMIFiltersService MailSendBMIFiltersService = (IMailSendBMIFiltersService) context
                    .getService(service.name());

            MailSendBMIFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readMailSendCommtouchFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        CommtouchFilters commtouchFilters = null;
        String method = "read MailSendCommtouchFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendCommtouchFiltersService MailSendCommtouchFiltersService = (IMailSendCommtouchFiltersService) context
                    .getService(service.name());
            commtouchFilters = MailSendCommtouchFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("commtouchFilters = " + commtouchFilters);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && commtouchFilters != null) {
                result = CommonUtil.comparePayload(commtouchFilters.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateMailSendCommtouchFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update MailSendCommtouchFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendCommtouchFiltersService MailSendCommtouchFiltersService = (IMailSendCommtouchFiltersService) context
                    .getService(service.name());

            MailSendCommtouchFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readMailSendMcAfeeFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        McAfeeFilters mcAfeeFilters = null;
        String method = "read MailSendMcAfeeFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendMcAfeeFiltersService MailSendMcAfeeFiltersService = (IMailSendMcAfeeFiltersService) context
                    .getService(service.name());

            mcAfeeFilters = MailSendMcAfeeFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("mcAfeeFilters = " + mcAfeeFilters);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mcAfeeFilters != null) {
                result = CommonUtil.comparePayload(mcAfeeFilters.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateMailSendMcAfeeFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update MailSendMcAfeeFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final IMailSendMcAfeeFiltersService MailSendMcAfeeFiltersService = (IMailSendMcAfeeFiltersService) context
                    .getService(service.name());

            MailSendMcAfeeFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    // #33333333333333333

    public static int readCosMailSendService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        MailSend Cosmailsend = null;
        String method = "read COS MailSendService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendService CosMailSendService = (ICosMailSendService) context
                    .getService(service.name());

            Cosmailsend = CosMailSendService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Cosmailsend = " + Cosmailsend);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && Cosmailsend != null) {
                result = CommonUtil.comparePayload(Cosmailsend.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCosMailSendService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update MailSendService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendService CosMailSendService = (ICosMailSendService) context
                    .getService(service.name());

            CosMailSendService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int createDeliveryMessagesPendingService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Create send mail delivery message pending";
        
        try {
            INumDelayedDeliveryMessagesPendingService numDelayedDeliveryMessagePendingService = (INumDelayedDeliveryMessagesPendingService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            numDelayedDeliveryMessagePendingService.create(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readDeliveryMessagesPendingService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<String> numDelayedDeliveryMessagePending = null;
        List<String> modifiedNumDelayedDeliveryMessagePending = null;;
        String method = "Read send mail delivery message pending";
        try {
            CommonUtil.printOnBoth(method);
            final INumDelayedDeliveryMessagesPendingService numDelayedDeliveryMessagePendingService = (INumDelayedDeliveryMessagesPendingService) context
                    .getService(service.name());

            numDelayedDeliveryMessagePending = numDelayedDeliveryMessagePendingService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("numDelayedDeliveryMessagePending = " + numDelayedDeliveryMessagePending);
            modifiedNumDelayedDeliveryMessagePending = new ArrayList<String>();
            for(String numDelayedDeliveryMessagePendingValue : numDelayedDeliveryMessagePending) {
                modifiedNumDelayedDeliveryMessagePending.add(numDelayedDeliveryMessagePendingValue.replace(",", "&&"));
            }

            CommonUtil.printOnBoth("modifiedNumDelayedDeliveryMessagePending = " + modifiedNumDelayedDeliveryMessagePending);
            CommonUtil.printOnBoth(method + " ..done");
            
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && numDelayedDeliveryMessagePending != null) {
                result = CommonUtil.comparePayload(modifiedNumDelayedDeliveryMessagePending.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    
    public static int updateDeliveryMessagesPendingService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update send mail delivery message pending";
        try {
            final INumDelayedDeliveryMessagesPendingService numDelayedDeliveryMessagePendingService = (INumDelayedDeliveryMessagesPendingService) context
                    .getService(service.name());

            numDelayedDeliveryMessagePendingService.update(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
     public static int deleteDeliveryMessagesPendingService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        int result = 0;
        String errorCode = LfConstants.HTTPSUCCESS;

        try {
            
            INumDelayedDeliveryMessagesPendingService numDelayedDeliveryMessagePendingService = (INumDelayedDeliveryMessagesPendingService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Delete send mail delivery message pending");
            numDelayedDeliveryMessagePendingService.delete(map
                    .getMultivaluedMap());
            CommonUtil
                    .printOnBoth("Delete send mail delivery message pending ...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Delete send mail delivery message pending", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }



/*    public static int readCosMailSendFiltersService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Filters Cosmailsendfilter = null;
        String method = "read Cos MailSendFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendFiltersService CosMailSendFiltersService = (ICosMailSendFiltersService) context
                    .getService(service.name());

            Cosmailsendfilter = CosMailSendFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("Cosmailsendfilter = " + Cosmailsendfilter);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && Cosmailsendfilter != null) {
                result = CommonUtil.comparePayload(
                        Cosmailsendfilter.toString(), expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }
*/

/*    public static int readCosMailSendBMIFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        BmiFilters Cosbmifilter = null;
        String method = "read CosMailSendBMIFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendBMIFiltersService CosMailSendBMIFiltersService = (ICosMailSendBMIFiltersService) context
                    .getService(service.name());

            Cosbmifilter = CosMailSendBMIFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("Cosbmifilter = " + Cosbmifilter);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && Cosbmifilter != null) {
                result = CommonUtil.comparePayload(Cosbmifilter.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }
*/

/*    public static int updateCosMailSendBMIFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update CosMailSendBMIFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendBMIFiltersService CosMailSendBMIFiltersService = (ICosMailSendBMIFiltersService) context
                    .getService(service.name());

            CosMailSendBMIFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
*/

/*    public static int readCosMailSendCommtouchFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        CommtouchFilters CoscommtouchFilters = null;
        String method = "read CosMailSendCommtouchFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendCommtouchFiltersService CosMailSendCommtouchFiltersService = (ICosMailSendCommtouchFiltersService) context
                    .getService(service.name());

            CoscommtouchFilters = CosMailSendCommtouchFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("CoscommtouchFilters = "
                    + CoscommtouchFilters);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && CoscommtouchFilters != null) {
                result = CommonUtil.comparePayload(
                        CoscommtouchFilters.toString(), expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }
*/
/*    public static int updateCosMailSendCommtouchFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update CosMailSendCommtouchFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendCommtouchFiltersService CosMailSendCommtouchFiltersService = (ICosMailSendCommtouchFiltersService) context
                    .getService(service.name());

            CosMailSendCommtouchFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
*/
/*    public static int readCosMailSendMcAfeeFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        McAfeeFilters CosmcAfeeFilters = null;
        String method = "read CosMailSendMcAfeeFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendMcAfeeFiltersService CosMailSendMcAfeeFiltersService = (ICosMailSendMcAfeeFiltersService) context
                    .getService(service.name());

            CosmcAfeeFilters = CosMailSendMcAfeeFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("CosmcAfeeFilters = " + CosmcAfeeFilters);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && CosmcAfeeFilters != null) {
                result = CommonUtil.comparePayload(CosmcAfeeFilters.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }
*/
/*    public static int updateCosMailSendMcAfeeFiltersService(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update CosMailSendMcAfeeFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosMailSendMcAfeeFiltersService CosMailSendMcAfeeFiltersService = (ICosMailSendMcAfeeFiltersService) context
                    .getService(service.name());

            CosMailSendMcAfeeFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
*/
}
