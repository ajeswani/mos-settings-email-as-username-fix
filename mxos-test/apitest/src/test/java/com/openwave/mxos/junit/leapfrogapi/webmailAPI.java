package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosWebMailFeaturesService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesService;

public class webmailAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");
    static String service = "webmail";

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);

        switch (service) {
            case WebMailFeaturesService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET WEB MAIL FEATURES");
                        testStatus = read(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE WEB MAIL FEATURES");
                        testStatus = update(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosWebMailFeaturesService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS WEB MAIL FEATURES");
                        testStatus = readCos(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS WEB MAIL FEATURES");
                        testStatus = updateCos(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
        }
        return 1;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        WebMailFeatures webmailreturn = null;
        int result;
        try {
            CommonUtil.printOnBoth("read " + service + "service");
            IWebMailFeaturesService wemailservice = (IWebMailFeaturesService) context
                    .getService(service.name());
            webmailreturn = wemailservice.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("webmailreturn = " + webmailreturn);
            CommonUtil.printOnBoth("read " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && webmailreturn != null) {
                result = CommonUtil.comparePayload(webmailreturn.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            CommonUtil.printOnBoth("update " + service + "service");
            IWebMailFeaturesService wemailservice = (IWebMailFeaturesService) context
                    .getService(service.name());
            wemailservice.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCos(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        WebMailFeatures webmailreturn = null;
        String method = "Read CosWebMailFeatures";
        int result;
        try {
            CommonUtil.printOnBoth(method);
            ICosWebMailFeaturesService CosWebMailFeaturesService = (ICosWebMailFeaturesService) context
                    .getService(service.name());
            webmailreturn = CosWebMailFeaturesService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("coswebmailreturn = " + webmailreturn);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && webmailreturn != null) {
                result = CommonUtil.comparePayload(webmailreturn.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCos(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        String method = "Update CosWebMailFeatures";
        int result;
        try {
            CommonUtil.printOnBoth(method);
            ICosWebMailFeaturesService CosWebMailFeaturesService = (ICosWebMailFeaturesService) context
                    .getService(service.name());
            CosWebMailFeaturesService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
