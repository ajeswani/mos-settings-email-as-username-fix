 package com.openwave.mxos.junit.leapfrogapi;

import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.addressbook.ILoginUserService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMessageEventRecordsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedSendersListService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMessageEventRecordsService;

public class AddressbookContactsAPI {

    static Logger alltestlog = Logger.getLogger(AddressbookContactsAPI.class);
    static String service = "AddressbookContactsAPI API";

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case ContactsService: {
                switch (operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("Create Addressbook Contacts MICRO");
                        testStatus = create(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETE: {
                        CommonUtil.printstartAPI("Delete Addressbook Contacts MICRO");
                        testStatus = delete(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
          }
        return 1;
    }
    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "CREATE ADDRESSBOOK CONTACTS";
        try {
            CommonUtil.printOnBoth(method);
            IContactsService contactsService = (IContactsService) context
                    .getService(service.name());
            long contactID = contactsService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth("contactID id = " + contactID);
            LeapFrogTest.contactIdArray.add(contactID);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "DELETE ADDRESSBOOK CONTACTS";
        try {
            CommonUtil.printOnBoth(method);
            IContactsService contactsService = (IContactsService) context
                    .getService(service.name());
            contactsService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

} 
