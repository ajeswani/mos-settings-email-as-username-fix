package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminApprovedSendersListService;

public class AdminApprovedSendersListServiceAPI {

    static Logger alltestlog = Logger
            .getLogger(AdminApprovedSendersListServiceAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case PUT: {
            CommonUtil.printstartAPI("ADD ADMIN APPROVED SENDERS LIST");
            testStatus = create(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case GET: {
            CommonUtil.printstartAPI("GET ADMIN APPROVED SENDERS LIST");
            testStatus = read(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        case POST: {
            CommonUtil.printstartAPI("UPDATE ADMIN APPROVED SENDERS LIST");
            testStatus = update(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case DELETE: {
            CommonUtil.printstartAPI("DELETE ADMIN APPROVED SENDERS LIST");
            testStatus = delete(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        }
        return 1;
    }

    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "CREATE APPROVED ADMIN SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method);
            IAdminApprovedSendersListService adminApprovedSendersListService = (IAdminApprovedSendersListService) context
                    .getService(service.name());
            adminApprovedSendersListService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
            } catch (MxOSException e) {
                errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "GET APPROVED ADMIN SENDERS LIST";
        List<String> adminApprovedSendersList = null;
        try {
            CommonUtil.printOnBoth(method);
            IAdminApprovedSendersListService adminApprovedSendersListService = (IAdminApprovedSendersListService) context
            .getService(service.name());
            adminApprovedSendersList = adminApprovedSendersListService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("adminApprovedSendersList="+adminApprovedSendersList);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && adminApprovedSendersList != null) {
                result = CommonUtil.comparePayload(
                        adminApprovedSendersList.toString(), expectedOutput);
            }
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "UPDATE APPROVED ADMIN SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method);
            IAdminApprovedSendersListService adminApprovedSendersListService = (IAdminApprovedSendersListService) context
            .getService(service.name());
            adminApprovedSendersListService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "DELETE APPROVED ADMIN SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method+"Starts");
            IAdminApprovedSendersListService adminApprovedSendersListService = (IAdminApprovedSendersListService) context
            .getService(service.name());
            adminApprovedSendersListService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
