package com.openwave.mxos.junit.helper;

import static com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest.placeholderCache;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.sun.org.apache.xalan.internal.xsltc.runtime.Operators;
//import com.opwvmsg.mxos.interfaces.service.IFolderService;
//import com.opwvmsg.mxos.interfaces.service.IMessageService;

/**
 * This Class has common utilities which has been used by other methods
 * 
 * @author Amar
 */

public class CommonUtil {

//    static Logger alltestlog = Logger.getLogger(CommonUtil.class);
	static Logger alltestlog = Logger.getLogger(CommonUtil.class);
/*
    public static int createFolder(IFolderService folderService,
            FolderParams fld) throws Exception {
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        if (fld.getFolderProperty(LfConstants.MAILBOXID) != null) {
            List<String> mailbox = new ArrayList<String>(); 
            mailbox.add(fld.getFolderProperty(LfConstants.MAILBOXID));
            inputParams.put(MailboxProperty.mailboxId.name(),mailbox);
        }
        
        if (fld.getFolderProperty(LfConstants.PERFOLDERID) != null) {
            List<String> ParentFolderId = new ArrayList<String>(); 
            ParentFolderId.add(fld.getFolderProperty(LfConstants.PERFOLDERID));
            inputParams.put(FolderProperty.parentfolderid.name(),ParentFolderId);
        }

        if (fld.getFolderProperty(LfConstants.FOLDERNAME) != null) {
            List<String> foldername = new ArrayList<String>(); 
            foldername.add(fld.getFolderProperty(LfConstants.FOLDERNAME));
            inputParams.put(FolderProperty.name.name(),foldername);
        }

        int folderId = folderService.create(inputParams);

        System.out.println("Folder ID: " + folderId);
        return folderId;
    }


    public static Folder getFolder(IFolderService folderService,
            FolderParams fld) throws Exception {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        if (fld.getFolderProperty(LfConstants.MAILBOXID) != null) {
            List<String> mailbox = new ArrayList<String>(); 
            mailbox.add(fld.getFolderProperty(LfConstants.MAILBOXID));
            inputParams.put(MailboxProperty.mailboxId.name(),mailbox);
        }
        
        if (fld.getFolderProperty(LfConstants.FOLDERID) != null) {
            List<String> FolderId = new ArrayList<String>(); 
            FolderId.add(fld.getFolderProperty(LfConstants.FOLDERID));
            inputParams.put(FolderProperty.folderid.name(),FolderId);
        }

      //  Folder folder = folderService.read(inputParams);
      //  System.out.println("Folder: " + folder);
        
        Folder folder = null;  
        return folder;
    }

   

    public static List<Folder> readAllFolder(IFolderService folderService,
            FolderParams fld) throws Exception {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        if (fld.getFolderProperty(LfConstants.MAILBOXID) != null) {
            List<String> mailbox = new ArrayList<String>(); 
            mailbox.add(fld.getFolderProperty(LfConstants.MAILBOXID));
            inputParams.put(MailboxProperty.mailboxId.name(),mailbox);
        }

        System.out.println("mailboxId...: "
                + fld.getFolderProperty(LfConstants.MAILBOXID));
        //List<Folder> folders = folderService.readAll(inputParams);

        //System.out.println("All Folders: " + folders);
        List<Folder> folders = null;
        return folders;
    }

 

    public static void updateFolder(IFolderService folderService,
            FolderParams fld) throws Exception {
        
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        if (fld.getFolderProperty(LfConstants.MAILBOXID) != null) {
            List<String> mailbox = new ArrayList<String>(); 
            mailbox.add(fld.getFolderProperty(LfConstants.MAILBOXID));
            inputParams.put(MailboxProperty.mailboxId.name(),mailbox);
        }
        
        if (fld.getFolderProperty(LfConstants.FOLDERID) != null) {
            List<String> FolderId = new ArrayList<String>(); 
            FolderId.add(fld.getFolderProperty(LfConstants.FOLDERID));
            inputParams.put(FolderProperty.folderid.name(),FolderId);
        }

        if (fld.getFolderProperty(LfConstants.FOLDERNAME) != null) {
            List<String> foldername = new ArrayList<String>(); 
            foldername.add(fld.getFolderProperty(LfConstants.FOLDERNAME));
            inputParams.put(FolderProperty.name.name(),foldername);
        }

         folderService.update(inputParams);
    }


    public static void deleteFolder(IFolderService folderService,
            FolderParams fld) throws Exception {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        if (fld.getFolderProperty(LfConstants.MAILBOXID) != null) {
            List<String> mailbox = new ArrayList<String>(); 
            mailbox.add(fld.getFolderProperty(LfConstants.MAILBOXID));
            inputParams.put(MailboxProperty.mailboxId.name(),mailbox);
        }
        
        if (fld.getFolderProperty(LfConstants.FOLDERID) != null) {
            List<String> FolderId = new ArrayList<String>(); 
            FolderId.add(fld.getFolderProperty(LfConstants.FOLDERID));
            inputParams.put(FolderProperty.folderid.name(),FolderId);
        }

        Thread.sleep(100);
        folderService.delete(inputParams);
    }

 
    public static String getfolderid(IFolderService folderService,
            String mailboxId, String Foldername) {
        FolderParams fld1 = new FolderParams();
        
	    Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
       if (mailboxId != null) {
           List<String> mailboxIdVal = new ArrayList<String>(); 
           mailboxIdVal.add(mailboxId);
           inputParams.put(MailboxProperty.mailboxId.name(),mailboxIdVal);
       }

        String folderid = null;
        List<Folder> folders;
        try {
            folders = readAllFolder(folderService, fld1);
        } catch (Exception e) {
            e.printStackTrace();
            return LfConstants.FOLDERERROR;
        }

        Pattern pattern = Pattern
                .compile("folderId=\\d+\\s*,\\s*folderName=\\s*[a-zA-Z_0-9./]+");

        Matcher matcher = pattern.matcher(folders.toString());
        String[] paramNameValPairs;
        String[] paramNameVal;
        String[] paramNamekey;

        while (matcher.find()) {
            String delimiter = ",";
            paramNameValPairs = matcher.group().split(delimiter);
            delimiter = "=";
            paramNameVal = paramNameValPairs[0].split(delimiter);
            paramNamekey = paramNameValPairs[1].split(delimiter);

            if (Foldername.equals(paramNamekey[1])) {
                folderid = paramNameVal[1];
                System.out.println("Foldername = " + Foldername);
                System.out.println("FolderId = " + folderid);
                return folderid;
            }
        }
        return LfConstants.FOLDERERROR;
    }


    public static String getfoldername(IFolderService folderService,
            String mailboxid, String FoldeId) {
        FolderParams fld1 = new FolderParams();
        fld1.setFolderProperty("mailboxId", mailboxid);
        String foldername = null;
        List<Folder> folders;
        try {
            folders = readAllFolder(folderService, fld1);
        } catch (Exception e) {
            e.printStackTrace();
            return LfConstants.FOLDERERROR;
        }

        Pattern pattern = Pattern
                .compile("folderId=\\d+\\s*,\\s*folderName=\\s*[a-zA-Z_0-9./]+");
        Matcher matcher = pattern.matcher(folders.toString());
        String[] paramNameValPairs;
        String[] paramNameVal;
        String[] paramNamekey;
        while (matcher.find()) {
            String delimiter = ",";
            paramNameValPairs = matcher.group().split(delimiter);
            delimiter = "=";
            paramNameVal = paramNameValPairs[0].split(delimiter);
            paramNamekey = paramNameValPairs[1].split(delimiter);

            if (FoldeId.equals(paramNameVal[1])) {
                foldername = paramNamekey[1];
                System.out.println("Foldername **= " + foldername);
                return foldername;
            }

        }
        return LfConstants.FOLDERERROR;
    }

    
    public static void createMessage(IMessageService messageService,
            Messageparms msg) throws Exception {

        System.out.println("Creating message.... ");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> MailboxId = new ArrayList<String>(); 
        MailboxId.add(msg.getMessageProperty(LfConstants.MAILBOXID));
        inputParams.put(MailboxProperty.mailboxId.name(),MailboxId);
        
        List<String> MessageId = new ArrayList<String>(); 
        MessageId.add(msg.getMessageProperty(LfConstants.MESSAGEID));
        inputParams.put(MessageProperty.messageid.name(),MessageId);

        List<String> FolderId = new ArrayList<String>(); 
        FolderId.add(msg.getMessageProperty(LfConstants.FOLDERID));
        inputParams.put(FolderProperty.folderid.name(),FolderId);
        
        List<String> Frm = new ArrayList<String>(); 
        Frm.add(msg.getMessageProperty(LfConstants.FROM));
        inputParams.put(HeaderProperty.receivedfrom.name(),Frm);
        
        List<String> Sentto = new ArrayList<String>(); 
        Sentto.add(msg.getMessageProperty(LfConstants.SENTTO));
        inputParams.put(HeaderProperty.sentto.name(),Sentto);
        
        List<String> HeaderSummary = new ArrayList<String>(); 
        HeaderSummary.add(msg.getMessageProperty(LfConstants.HEADERSUMMARY));
        inputParams.put(HeaderProperty.headersummary.name(),HeaderSummary);

        List<String> FlagRecent = new ArrayList<String>(); 
        FlagRecent.add(msg.getMessageProperty(LfConstants.FLAGRECENT));
        inputParams.put(MessageProperty.flagrecent.name(),FlagRecent);

        List<String> MessageBody = new ArrayList<String>(); 
        MessageBody.add(msg.getMessageProperty(LfConstants.MESSAGEBODY));
        inputParams.put(MessageProperty.messagebody.name(),MessageBody);

        List<String> Type = new ArrayList<String>(); 
        Type.add(msg.getMessageProperty(LfConstants.TYPE));
        inputParams.put(MessageProperty.type.name(),Type);

        List<String> Priority = new ArrayList<String>(); 
        Priority.add(msg.getMessageProperty(LfConstants.PRIORITY));
        inputParams.put(MessageProperty.priority.name(),Priority);

        List<String> DeliverNDR = new ArrayList<String>(); 
        DeliverNDR.add(msg.getMessageProperty(LfConstants.DELIVERNDR));
        inputParams.put(MessageProperty.deliverndr.name(),DeliverNDR);
        
        List<String> ExpTime = new ArrayList<String>(); 
        ExpTime.add(msg.getMessageProperty(LfConstants.EXPTIME));
        inputParams.put(MessageProperty.exptime.name(),ExpTime);

        List<String> Size = new ArrayList<String>(); 
        Size.add(msg.getMessageProperty(LfConstants.SIZE));
        inputParams.put(MessageProperty.size.name(),Size);

        List<String> Subject = new ArrayList<String>(); 
        Subject.add(msg.getMessageProperty(LfConstants.SUBJECT));
        inputParams.put(HeaderProperty.subject.name(),Subject);

        System.out.println(" ******* 1");

        System.out.println("sending mailboxid "
                + msg.getMessageProperty(LfConstants.MAILBOXID));
        System.out.println("sending message body "
                + msg.getMessageProperty(LfConstants.MESSAGEBODY));
        System.out.println("sending message id "
                + msg.getMessageProperty(LfConstants.MESSAGEID));

        long msgUID = messageService.create(inputParams);
        System.out.println("IMAP UID : " + msgUID);

    }


    public static Message readMessage(IMessageService messageService,
            Messageparms msg) throws Exception {


        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> MailboxId = new ArrayList<String>(); 
        MailboxId.add(msg.getMessageProperty(LfConstants.MAILBOXID));
        inputParams.put(MailboxProperty.mailboxId.name(),MailboxId);
        
        List<String> MessageId = new ArrayList<String>(); 
        MessageId.add(msg.getMessageProperty(LfConstants.MESSAGEID));
        inputParams.put(MessageProperty.messageid.name(),MessageId);

        List<String> FolderId = new ArrayList<String>(); 
        FolderId.add(msg.getMessageProperty(LfConstants.FOLDERID));
        inputParams.put(FolderProperty.folderid.name(),FolderId);

     //   Message message = messageService.read(inputParams);
        Message message = null;
        System.out.println(message);
        return message;
    }

    public static void updateMessage(IMessageService messageService,
            Messageparms msg) throws Exception {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> MailboxId = new ArrayList<String>(); 
        MailboxId.add(msg.getMessageProperty(LfConstants.MAILBOXID));
        inputParams.put(MailboxProperty.mailboxId.name(),MailboxId);
        
        List<String> MessageId = new ArrayList<String>(); 
        MessageId.add(msg.getMessageProperty(LfConstants.MESSAGEID));
        inputParams.put(MessageProperty.messageid.name(),MessageId);

        List<String> FolderId = new ArrayList<String>(); 
        FolderId.add(msg.getMessageProperty(LfConstants.FOLDERID));
        inputParams.put(FolderProperty.folderid.name(),FolderId);
        
        List<String> ExpTime = new ArrayList<String>(); 
        ExpTime.add(msg.getMessageProperty(LfConstants.EXPTIME));
        inputParams.put(MessageProperty.exptime.name(),ExpTime);

        List<String> FlagFlagged = new ArrayList<String>(); 
        FlagFlagged.add(msg.getMessageProperty(LfConstants.FLAGFLAGGED));
        inputParams.put(MessageProperty.flagflagged.name(),FlagFlagged);

        List<String> FlagDraft = new ArrayList<String>(); 
        FlagDraft.add(msg.getMessageProperty(LfConstants.FLAGDRAFT));
        inputParams.put(MessageProperty.flagdraft.name(),FlagDraft);

        List<String> FlagRecent = new ArrayList<String>(); 
        FlagRecent.add(msg.getMessageProperty(LfConstants.FLAGRECENT));
        inputParams.put(MessageProperty.flagrecent.name(),FlagRecent);

        messageService.update(inputParams);
    }

    public static void deleteMessage(IMessageService messageService,
            Messageparms msg) throws Exception {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> MailboxId = new ArrayList<String>(); 
        MailboxId.add(msg.getMessageProperty(LfConstants.MAILBOXID));
        inputParams.put(MailboxProperty.mailboxId.name(),MailboxId);
        
        List<String> MessageId = new ArrayList<String>(); 
        MessageId.add(msg.getMessageProperty(LfConstants.MESSAGEID));
        inputParams.put(MessageProperty.messageid.name(),MessageId);

        List<String> FolderId = new ArrayList<String>(); 
        FolderId.add(msg.getMessageProperty(LfConstants.FOLDERID));
        inputParams.put(FolderProperty.folderid.name(),FolderId);

        messageService.delete(inputParams);
    }
   */
   
    /**
     * This function would return error code
     * 
     * @param errorCode
     *            string
     * 
     * @return error Code
     * 
     */

    public static String getErrorCode(String errorCode) {
    	//Error: 3000, Message:
       // Pattern pattern = Pattern.compile("HTTP Code:\\s*\\d+");
        alltestlog.info("----------------------------------");
        alltestlog.info("----- retriving Error Code from error message -------");
        alltestlog.info("----------------------------------");
        
        if(errorCode != null)
        {
	        Pattern pattern = Pattern.compile("Error\\s*:\\s*\\d+");
	
	        Matcher matcher = pattern.matcher(errorCode);
	        String[] paramNameValPairs;
	        String errorReturnCode = null;
	        while (matcher.find()) {
	            String delimiter = ":";
	            paramNameValPairs = matcher.group().split(delimiter);
	            paramNameValPairs[0] = paramNameValPairs[0].replaceAll("\\s+", "");
	            errorReturnCode = paramNameValPairs[1].replaceAll("\\s+", "");
	        }
	
	        alltestlog.info("Error code = " + errorReturnCode);
	        System.out.println("Error code = " + errorReturnCode);
	        if (errorReturnCode == null)
	        {
	            alltestlog.info("No Error code found returning 0");
	            alltestlog.info("----------------------------------");
	            return "0";
	        }	
	        alltestlog.info("----------------------------------");
	        return errorReturnCode;
        } 
        return "0";
    }

	public static String getPrintErrorCode(String apiname, MxOSException e) {
		e.printStackTrace(System.err);
		String errorCode = null;
		if (e.getShortMessage() != null) {
			errorCode = e.getShortMessage();
			CommonUtil.printOnBoth(apiname + " .. Fail");
			alltestlog.info("Error code ==" + e.getCode());
			alltestlog.info("Error Message ==" + e.getMessage());
			alltestlog.info("Error Details == " + e.getShortMessage());
			alltestlog.info("Error ShortMessage == " + errorCode);
		} else {
			CommonUtil.printErrorOnBoth(apiname + " .. Fail");
			alltestlog.info("Error code ==" + e.getCode());
			alltestlog.error("Error Message ==" + e.getMessage());
			alltestlog.error("Error Details == " + e.getShortMessage());
			alltestlog.info("Error ShortMessage == " + errorCode);
		}
		return e.getCode() + "," + errorCode;

		//return errorCode;
	}
    
    /**
     * This function would compare actual errorcode with expErrorCode Print
     * Errors return 1 on success and 0 in failure
     * 
     * @param expErrorCode
     *            - Expected error code 
     * 
     * @return on success 1 on failure 0
     */

	public static int compareErrorCode(String errorCode, String expErrorCode) {

		alltestlog.info("----------------------------------");
		alltestlog.info("----- Comparing Error Code -------");
		alltestlog.info("Expected Error code = " + expErrorCode);
		alltestlog.info("Actual Error code = " + errorCode);

		if (errorCode == null || !expErrorCode.equals(errorCode)) {
			System.err.println("ERROR: Error code is not matching ");
			System.err.println("ERROR: Actual Error code is = " + errorCode);
			System.err.println("ERROR: Expected Error code is " + expErrorCode);
			alltestlog.error("ERROR: Error code is not matching ");
			alltestlog.info("--------------------------");

			return 0;
		}
		System.out.println("Error code matched.");
		alltestlog.info("Error code matched.");
		alltestlog.info("--------------------------");
		// alltestlog.info("Actual Error code is = " + errorCode);
		// alltestlog.info("Expected Error code is " + expErrorCode);

		return 1;
	}
    
	public static int
	   compOuputVal(String actualVal, String [] expectinterKeyValPairs) {
     if (expectinterKeyValPairs[1] != null)
     {	  
	  //     alltestlog.info("Inside compOuputVal if ..");
  	    if (expectinterKeyValPairs[1].equals(actualVal)) 
	            {   
             printSuccess(actualVal,expectinterKeyValPairs);
             return 0;
          } else {
             printFail(actualVal,expectinterKeyValPairs);
         }
     }
     else
     {
  	 //  alltestlog.info("Inside compOuputVal else ..");
  	   if (actualVal != null)
  	   {
  		   printFail(actualVal,expectinterKeyValPairs);
            return 1;
  	   }
  	   else
  	   {
  		   printSuccess(actualVal,expectinterKeyValPairs);
  	   }	   
      }	    
     return 1;
	}	
	
	public static void
	   printSuccess(String actvalue, String [] expectinterKeyValPairs) {
	//	alltestlog.info("inside printSucess");
		alltestlog.info("Actual " + expectinterKeyValPairs[0]
    + " = " + actvalue + " Expected "
    + expectinterKeyValPairs[0] + " = "
    + expectinterKeyValPairs[1]);
		
	}	
	public static void
	   printFail(String actvalue, String [] expectinterKeyValPairs) {
	//	alltestlog.info("inside printFail");
		alltestlog.error("Actual " + expectinterKeyValPairs[0]
	 + " = " + actvalue + " Expected "
	 + expectinterKeyValPairs[0] + " = "
	 + expectinterKeyValPairs[1]);
	}	
  
 
	public static int
	   compOuputValArray(List <String> actualVal, String [] expectinterKeyValPairs) {
  int errcnt = 0;	
  if (expectinterKeyValPairs[1] != null)
  {	  
	    //   alltestlog.info("Inside compOuputValArray if ..");
           String [] expectedlist = expectinterKeyValPairs[1].split("&");
           for(int count1 = 0 ; count1 < expectedlist.length ; count1++)
           {
               boolean yesorno = actualVal.contains(expectedlist[count1]);
               if(yesorno)
               {
            	  alltestlog.info("Expected " + expectinterKeyValPairs[0]
                              + " = " + expectedlist[count1] + " is found in " 
                              + " Actual " + expectinterKeyValPairs[0] + " = "
                              + actualVal);
               }   
               else
               {
             	  alltestlog.error("ERROR :Expected " + expectinterKeyValPairs[0]
                              + " = " + expectedlist[count1] + " is NOT found in " 
                              + " Actual " + expectinterKeyValPairs[0] + " = "
                              + actualVal);
             	 errcnt++;
               }	   
          }   
  }
  else
  {
	 //  alltestlog.info("Inside compOuputValArray else ..");
	   if (actualVal != null)
	   {
		   alltestlog.error("Expected value = null");
		   alltestlog.error("Actual value = " + actualVal);
       	   errcnt++;

	   }
	   else
	   {
		   alltestlog.info("Expected and Actual value = null");
	   }	   
   }
  return errcnt; 
 }	
	public static void
	   printExcepError(Exception e , String errorCode ) {
		alltestlog.error("Error code is = " + errorCode);
		System.err.println("Error Message = " + e.getMessage());
		alltestlog.error("Error Message = " + e.getMessage());
		alltestlog.error("Stack Trace = " + e.getStackTrace());
	}	

	public static void
	   printstartAPI(String apiName ) {
		alltestlog.info("---------------------------");
		alltestlog.info("**** " + apiName + " *****");
//		alltestlog.info("---------------------------");
		System.out.println("---------------------------");
		System.out.println("**** " + apiName + " *****");
//		System.out.println("---------------------------");

	}	
	public static void printOnBoth(String message ) {
		alltestlog.info(message);
		System.out.println(message);
	}	
	public static void
	printErrorOnBoth(String message ) {
		alltestlog.error(message);
		System.err.println(message);
	}	
	
	/*
	 * 	        Pattern pattern = Pattern.compile("Error\\s*:\\s*\\d+");
	
	        Matcher matcher = pattern.matcher(errorCode);
	        String[] paramNameValPairs;
	        String errorReturnCode = null;
	        while (matcher.find()) {
	            String delimiter = ":";
	            paramNameValPairs = matcher.group().split(delimiter);
	            paramNameValPairs[0] = paramNameValPairs[0].replaceAll("\\s+", "");
	            errorReturnCode = paramNameValPairs[1].replaceAll("\\s+", "");
	        }
	

	 */
	
    public static int comparePayload(String actualOutput, String expectedOputput
            ) {
        int result = 1; 
        List<String> mismatchList = new ArrayList<String>();
        List<String> nonExistParameters = new ArrayList<String>();
        String[] expectedRespParamValues;
        int errorcnt = 0;
        String delimiter = ",";
        String commaDelimiter = ",";
        String expectedRespParamValue = "";
        expectedOputput = expectedOputput.replaceAll("\n", "\r\n");
        expectedRespParamValues = expectedOputput.split(delimiter);
        String[] actualMxosResult = actualOutput.split(commaDelimiter);
        for (int i = 0; i < expectedRespParamValues.length; i++) {
            expectedRespParamValue = expectedRespParamValues[i];
            alltestlog.info("Expected result [" + (i + 1)
                    + "] = " + expectedRespParamValue);
            if (!actualOutput.contains(expectedRespParamValue)) {
            	alltestlog.error("Expected result = "
                        + expectedRespParamValue + " is not matching");
            	errorcnt++;
                if (actualMxosResult.length > 0) {
                    for (String str : actualMxosResult) {
                        if (expectedRespParamValue.contains("=")) {
                            String[] exptectedArray = expectedRespParamValue
                                    .split("=");
                            if (exptectedArray.length > 0) {
                                if (str.contains(exptectedArray[0])) {
                                    if (str.contains("code")) {
                                        mismatchList.add(str.substring(str
                                                .indexOf('[')));
                                    } else if ((!str.contains("["))) {
                                        mismatchList.add(str);
                                    }
                                }
                            }
                        } else if (expectedRespParamValue.contains(":")) {
                            String[] exptectedArray = expectedRespParamValue
                                    .split(":");
                            if (exptectedArray.length > 0) {
                                if (str.contains(exptectedArray[0])) {
                                    mismatchList.add(str);
                                }
                            }
                        }
                    }
                }
                nonExistParameters.add(expectedRespParamValue);
                result = 0;
                
                // return payloadMatch;
            } else {
            	alltestlog.info("Expected result  = "
                        + expectedRespParamValue + " is matching");
            }
        }
        
        alltestlog.info("exactMismachtString:::::: " + mismatchList);
        alltestlog.info("nonExistParameters:::::: " + nonExistParameters);
	
	       if (errorcnt == 0) {
	           return 1;
	       } else {
	           System.err.println("ERROR: " + errorcnt + " errors found");
	           alltestlog.error("ERROR: " + errorcnt + " errors found");
	           return 0;
	       }
    }
    
    
    

    public static int comparePayload1(String actualOutput,
            String expectedOputput) {

        actualOutput = "com.opwvmsg.mxos.data.pojos.Mailbox@1eb67cb[userName=1231_23123,domain=openwave.com,"
                + "mailboxId=122610004,status=active,metaStatus=0,type=Normal,email=1231_23123@openwave."
                + "com,allowedDomains=[],emailAliases=[amar@openwave.com,amar1@openwave.com],maxNumAliases=20,lastAccessDate=1342000423639,"
                + "customFields=<null>,cosId=default,credentials=com.opwvmsg.mxos.data.pojos.Credentials@"
                + "120bb37[passwordStoreType=clear,password=leapfrog1005,additionalProperties={}],generalP"
                + "references=com.opwvmsg.mxos.data.pojos.GeneralPreferences@903e28[charset=<null>,locale="
                + "<null>,timezone=<null>,additionalProperties={}],mailSend=com.opwvmsg.mxos.data.pojos.Mail"
                + "Send@139fb49[maxSendMessageSizeKB=0,additionalProperties={}],mailReceipt=com.opwvmsg.mxos."
                + "data.pojos.MailReceipt@18a0814[receiveMessages=yes,forwardingEnabled=<null>,copyOnForward="
                + "<null>,forwardingAddress=[],blockedSenders=[],blockedSenderAction=<null>,blockedSenderMessage"
                + "=<null>,filteringEnabled=no,rejectBouncedMessage=no,blockSendersPABActive=<null>,"
                + "blockSendersPABAccess=<null>,deliveryScope=<null>,addressesForLocalDelivery=[],"
                + "autoReplyMode=none,autoReplyMessage=<null>,maxReceiveMessageSizeKB=100,additionalProperties"
                + "={}],mailAccess=com.opwvmsg.mxos.data.pojos.MailAccess@1bef480[popAccessEnabled=yes,"
                + "imapAccessEnabled=yes,smtpAccessEnabled=yes,webmailAccessEnabled=yes,additionalProperties={}]"
                + ",mailStore=com.opwvmsg.mxos.data.pojos.MailStore@f80ca5[maxMessages=1000,maxStorageSizeKB="
                + "10000,numMsgRead=0,numMsg=0,sizeMsgRead=0,sizeMsg=0,storageSizeKB=<null>,quotaWarningThreshold=70"
                + ",quotaBounceNotify=yes,folders=[],additionalProperties={}],internalInfo=com.opwvmsg.mxos."
                + "data.pojos.InternalInfo@198151e[version=4,additionalProperties={}],additionalProperties={}]";

        expectedOputput = "userName=1231_23123,Domain=openwave.com,email=1231_23123@openwave,"
                + "mailboxId=122610004,status=active,cosId=default,passwordStoreType=clear1,"
                + "password=leapfrog1005,emailAliases=[amar@openwave.com,amar1@openwave.com],timezone=";

        boolean payloadMatch = true;
        int result = 0;
        List<String> mismatchList = new ArrayList<String>();
        List<String> nonExistParameters = new ArrayList<String>();
        String[] expectedRespParamValues;
        String delimiter = ",";
        String commaDelimiter = ",";
        String expectedRespParamValue = "";
        expectedRespParamValues = expectedOputput.split(delimiter);
        String[] actualMxosResult = actualOutput.split(commaDelimiter);
        for (int i = 0; i < expectedRespParamValues.length; i++) {
            expectedRespParamValue = expectedRespParamValues[i];
            alltestlog.info("expected response parameterName_Value_Pair["
                    + (i + 1) + "]=" + expectedRespParamValue);
            if (!actualOutput.contains(expectedRespParamValue)) {
                alltestlog.info("expected Response Param Name_Value_Pair="
                        + expectedRespParamValue + " is not matching");
                /*
                 * List<String> misMatchString = new ArrayList<String>();
                 * misMatchString.add(expectedRespParamValue);
                 * log.info("misMatchString::::::::::: " + misMatchString);
                 */
                if (actualMxosResult.length > 0) {
                    for (String str : actualMxosResult) {
                        if (expectedRespParamValue.contains("=")) {
                            String[] exptectedArray = expectedRespParamValue
                                    .split("=");
                            if (exptectedArray.length > 0) {
                                if (str.contains(exptectedArray[0])) {
                                    if (str.contains("code")) {
                                        mismatchList.add(str.substring(str
                                                .indexOf('[')));
                                    } else if ((!str.contains("["))) {
                                        mismatchList.add(str);
                                    }
                                }
                            }
                        } else if (expectedRespParamValue.contains(":")) {
                            String[] exptectedArray = expectedRespParamValue
                                    .split(":");
                            if (exptectedArray.length > 0) {
                                if (str.contains(exptectedArray[0])) {
                                    mismatchList.add(str);
                                }
                            }
                        }
                    }
                }
                nonExistParameters.add(expectedRespParamValue);
                payloadMatch = false;
                result = 1;

                // return payloadMatch;
            } else {
                alltestlog.info("expected Response Param Name_Value_Pair="
                        + expectedRespParamValue + " is matching");
            }
        }
        alltestlog.info("exactMismachtString:::::: " + mismatchList);
        alltestlog.info("nonExistParameters:::::: " + nonExistParameters);

        return result;
    }

    public static void replcaePlaceholder(Map<String, List<String>> multiMap,
            String key) {
        System.out.println("multimap ==== Before " + multiMap);
        System.out.println("key:: " + key);
        if (multiMap.containsKey(key) && null != multiMap.get(key)
                && !multiMap.get(key).isEmpty()) {
            StringBuilder messageId = null;
            List<String> list = multiMap.get(key);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) != null) {
                    messageId = new StringBuilder(list.get(i));
                    int lastIndex = messageId.length() - 1;
                    if (LfConstants.SYMBOL_PERCENTAGE == messageId.charAt(0)
                            && LfConstants.SYMBOL_PERCENTAGE == messageId
                                    .charAt(lastIndex)) {
                        messageId.replace(0, 1, "").replace(lastIndex - 1,
                                lastIndex, "");
                        if (placeholderCache.containsKey(messageId.toString())) {
                            multiMap.get(key).set(i,
                                    placeholderCache.get(messageId.toString()));
                        }
                    }
                }
            }
        }
        System.out.println("multimap ==== After " + multiMap);
    }

    public static void parseQueryStringToReplacePlaceholder(
            Map<String, List<String>> multiMap, String key) {
        if (multiMap.containsKey(key) && !multiMap.get(key).isEmpty()) {
            System.out.println("Key = " + key);
            List<String> queryList = multiMap.get(key);
            Operator operator = null;
            String[] splitQueryString = null;
            Iterator<String> itr = multiMap.keySet().iterator();

            while (itr.hasNext()) {
                String s = itr.next();
                System.out.println("Key : " + s + ", value " + multiMap.get(s));
            }

            /*
             * System.out.println("multiMap = " +
             * multiMap.get(key).get(0).contains("||"));
             * System.out.println("multiMap = " +
             * multiMap.get(key).get(0).contains("&&"));
             */
            if (queryList.get(0).contains("||")) {
                operator = Operator.OR;
            } else if (queryList.get(0).contains("&&")) {
                operator = Operator.AND;
            }
            // System.out.println("operator.OR="+Operator.OR);
            // System.out.println("operator.AND="+Operator.AND);
            // System.out.println("operator= "+operator);
            // System.out.println("operator val= "+operator.toString());

            switch (operator) {
            case OR: {
                splitQueryString = queryList.get(0).split("\\|\\|");
                // System.out.println("splitQueryString 0= "+splitQueryString[0]);
                // System.out.println("splitQueryString 1= "+splitQueryString[1]);
                // msgId:==:%msgId2%||msgId:==:%msgId3%

                StringBuilder messageId = null;
                for (int i = 0; i < queryList.size(); i++) {
                    messageId = new StringBuilder(queryList.get(i));
                    // System.out.println("messageId="+messageId);
                    int lastIndex = messageId.length() - 1;
                    if (LfConstants.SYMBOL_PERCENTAGE == messageId.charAt(0)
                            && LfConstants.SYMBOL_PERCENTAGE == messageId
                                    .charAt(lastIndex)) {
                        messageId.replace(0, 1, "").replace(lastIndex - 1,
                                lastIndex, "");
                        if (placeholderCache.containsKey(messageId.toString())) {
                            multiMap.get(key).set(i,
                                    placeholderCache.get(messageId.toString()));

                        }
                    }
                }

                // for (int i = 0; i < list.size(); i ++) {
                // messageId = new splitQueryString(list.get(i));
            }
            case AND: {
                splitQueryString = queryList.get(0).split("\\&\\&");

            }
            }
        }
    }    

    /* public static void parseQueryString(Map<String, List<String>> multiMap,
             String key) {
         if (multiMap.containsKey(key) && !multiMap.get(key).isEmpty()) {
             List<String> queryList = multiMap.get(key); 
             String[] splitArray = queryList.get(0).split(":==:");
             String operator = splitArray[1].substring(8, 10);
             String[] msgIdsArray = splitArray[1].split("\\|\\|");
             String finalQueryString = splitArray[0]+":==:";
             //String finalQueryString = splitArray[0]+"==";
             for (int i = 0; i < msgIdsArray.length; i++) {
                 int lastIndex = msgIdsArray[i].length() - 1;
                 if (LfConstants.SYMBOL_PERCENTAGE == msgIdsArray[i].charAt(0)
                         && LfConstants.SYMBOL_PERCENTAGE == msgIdsArray[i]
                                 .charAt(lastIndex)) {
                     StringBuilder msgIdBuilder = new StringBuilder(msgIdsArray[i]);
                     msgIdBuilder.replace(0, 1, "").replace(lastIndex - 1, lastIndex,"");
                     finalQueryString = finalQueryString+placeholderCache.get(msgIdBuilder.toString());
                         if(i<msgIdsArray.length-1){
                                 finalQueryString = finalQueryString+operator+splitArray[0]+":==:";
                                 //finalQueryString = finalQueryString+operator+splitArray[0]+"==";
                         }
                  } 
                     
              }
               multiMap.get(key).set(0,finalQueryString);
               System.out.println("parseQueryString multimap After=========="+multiMap); 
             }
             
         }*/

    public static void replaceExpectOutputMsgId(Map<String, List<String>> multiMap,
            String key) {
    	printOnBoth("multimap ==== Before "+multiMap);
    	
    	
        String msgid1 = "";

		if (multiMap.containsKey(key) && !multiMap.get(key).isEmpty()) {
            StringBuilder messageId = null;
            List<String> list = multiMap.get(key);
            for (int i = 0; i < list.size(); i ++) {
                messageId = new StringBuilder(list.get(i));
                int lastIndex = messageId.length() - 1;
                if (LfConstants.SYMBOL_PERCENTAGE == messageId.charAt(0)
                        && LfConstants.SYMBOL_PERCENTAGE == messageId
                                .charAt(lastIndex)) {
                    messageId.replace(0, 1, "").replace(lastIndex - 1, lastIndex,
                            "");
                    if (placeholderCache.containsKey(messageId.toString())) {
                        multiMap.get(key).set(i,
                                placeholderCache.get(messageId.toString()));
                    }
                }
                printOnBoth("multimap value **** "+ list.get(i));
                String msgid2 = "messageId=" + "[" + list.get(i) +"]," ;
                msgid1 = msgid1 + msgid2;
            }
        }
		printOnBoth("msgid1 =====***  " + msgid1);

		String temp11 = msgid1;
    	Iterator iter = multiMap.entrySet().iterator();

    	while (iter.hasNext()) {
        	Map.Entry mEntry = (Map.Entry) iter.next();
	  		printOnBoth( ""+ mEntry.getKey() + "==" +  multiMap.get(mEntry.getKey()+ ""));
	  		
	  		if (!mEntry.getKey().toString().contains("messageId"))
	  		{
	  		temp11 =  temp11 + mEntry.getKey() + "=" +  multiMap.get(mEntry.getKey()) + ",";
	  		}
		}
		
        printOnBoth("temp11 ==== After "+temp11);

        temp11 = temp11.replaceAll("[\\[\\]]","");
        temp11.substring(0,temp11.length()-1);
        
        printOnBoth("finalstring ==== After "+temp11);

        printOnBoth("multimap ==== After "+multiMap);
    }

}
