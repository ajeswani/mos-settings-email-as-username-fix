package com.openwave.mxos.junit.leapfrogapi;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendSMSService;

public class SendSmsAPI {

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {
        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);
        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);

        if (operation == Operation.POST) {
            CommonUtil.printstartAPI("process SENDSMS Service");
            testStatus = sendsms(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        return 1;
    }

    public static int sendsms(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String Service = "sendsms";
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final ISendSMSService SendSMSService = (ISendSMSService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Process " + Service);
            SendSMSService.process(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Process " + Service + "..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Process " + Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

}
