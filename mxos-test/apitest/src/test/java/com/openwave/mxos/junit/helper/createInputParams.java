package com.openwave.mxos.junit.helper;

import static com.openwave.mxos.junit.helper.CommonUtil.printOnBoth;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.interfaces.service.Operation;

public class createInputParams {

    static Logger alltestlog = Logger.getLogger(createInputParams.class);

    public static MxosRequestMap create(String input, Operation operation) {
        alltestlog.info("----------------------------");
        alltestlog.info("-----* Input Params *-------");
        System.out.println(operation);
        String delimiter = ",";
        String[] inputKeyValPairs = input.split(delimiter);
        String[] paramsKeyValPairs = { null, null };
        String[] paramsKeyValPairs_temp;

        final MxosRequestMap map = new MxosRequestMap();

        for (int inputCount = 0; inputCount < inputKeyValPairs.length; inputCount++) {
            delimiter = "=";
            paramsKeyValPairs_temp = inputKeyValPairs[inputCount]
                    .split(delimiter);
            if (paramsKeyValPairs_temp.length == 1) {
                paramsKeyValPairs[0] = paramsKeyValPairs_temp[0];
                paramsKeyValPairs[1] = null;
                alltestlog.info("Value for key | " + paramsKeyValPairs[0]
						+ " |,after parsing is : " + paramsKeyValPairs[1]);
            } else {
                paramsKeyValPairs[0] = paramsKeyValPairs_temp[0];
				paramsKeyValPairs[1] = inputKeyValPairs[inputCount]
						.substring(inputKeyValPairs[inputCount].indexOf("=") + 1);
                paramsKeyValPairs[1] = paramsKeyValPairs[1].replaceAll("&&",
                        ",");
                alltestlog.info("Value for key | " + paramsKeyValPairs[0]
						+ " |,after parsing is : " + paramsKeyValPairs[1]);
            }
			try {
				paramsKeyValPairs[1] = new String(
						paramsKeyValPairs[1].getBytes());
				alltestlog.info("UTF-8 encoded string : " + paramsKeyValPairs[1]);
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
        
            if (paramsKeyValPairs[0].equalsIgnoreCase(LfConstants.DOMAIN)) {
                map.add(DomainProperty.domain, paramsKeyValPairs[1]);
                printOnBoth("domain = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.DOMAINTYPE)
                    && operation.name().contains("domain")) {
                map.add(DomainProperty.type, paramsKeyValPairs[1]);
                printOnBoth("domainType = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.MAILRELAYHOST)) {
                map.add(DomainProperty.relayHost, paramsKeyValPairs[1]);
                printOnBoth("relayHost = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.MAILREWRITEDOMAIN)) {
                map.add(DomainProperty.alternateDomain, paramsKeyValPairs[1]);
                printOnBoth("alternateDomain = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.DEFAULTMAILBOX)) {
                map.add(DomainProperty.defaultMailbox, paramsKeyValPairs[1]);
                printOnBoth("defaultMailbox = " + paramsKeyValPairs[1]);
            }
            // else if (paramsKeyValPairs[0]
            // .equalsIgnoreCase(LfConstants.CUSTUMFIELD)) {
            // customFields = paramsKeyValPairs[1];
            // List<String> customFieldsVal = new ArrayList<String>();
            // customFieldsVal.add(customFields);
            // inputParams.put(.name(), customFieldsVal);
            // }
            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.USERNAME)) {
                map.add(MailboxProperty.userName, paramsKeyValPairs[1]);
                System.out.println("userName =" + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.EMAIL)) {
                map.add(MailboxProperty.email, paramsKeyValPairs[1]);
                printOnBoth("email = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.STATUS)) {
                map.add(MailboxProperty.status, paramsKeyValPairs[1]);
                printOnBoth("status = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.MAILBOXID)) {
                map.add(MailboxProperty.mailboxId, paramsKeyValPairs[1]);
                printOnBoth("mailboxId = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.COSID)) {
                map.add(MailboxProperty.cosId, paramsKeyValPairs[1]);
                printOnBoth("cosId = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.PASSWORD)) {
                map.add(MailboxProperty.password, paramsKeyValPairs[1]);
                printOnBoth("password = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.PASSSTORETYPE)) {
                map.add(MailboxProperty.passwordStoreType, paramsKeyValPairs[1]);
                printOnBoth("passwordStoreType = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.MSGSTOREHOST)) {
                // map.add(MailboxProperty.messageStoreHosts,
                // paramsKeyValPairs[1]);
                // printOnBoth("messageStoreHosts = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.MAILAUTOREPLYHOST)) {
                // map.add(MailboxProperty.autoreplyhost, paramsKeyValPairs[1]);
                // printOnBoth("MailAutoReplyHost = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.MAXNUMALIASES)) {
                map.add(MailboxProperty.maxNumAliases, paramsKeyValPairs[1]);
                printOnBoth("maxNumAliases = " + paramsKeyValPairs[1]);
            }

            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.EMAILALIAS)) {
                map.add(MailboxProperty.emailAlias, paramsKeyValPairs[1]);
                printOnBoth("emailAlias = " + paramsKeyValPairs[1]);
            }

            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.NEWEMAILALIAS)) {
                map.add(MailboxProperty.newEmailAlias, paramsKeyValPairs[1]);
                printOnBoth("newEmailAlias = " + paramsKeyValPairs[1]);
            }

            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.OLDEMAILALIAS)) {
                map.add(MailboxProperty.oldEmailAlias, paramsKeyValPairs[1]);
                printOnBoth("oldEmailAlias = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.DELEMAILALIAS)) {
                // map.add(MailboxProperty.deleteEmdeailAlias,
                // paramsKeyValPairs[1]);
                // printOnBoth("deleteEmailAlias = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.LOCALE)) {
                map.add(MailboxProperty.locale, paramsKeyValPairs[1]);
                printOnBoth("locale = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.TIMEZONE)) {
                map.add(MailboxProperty.timezone, paramsKeyValPairs[1]);
                System.out.println("timeZone =" + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.AUTOREPLYMODE)) {
                map.add(MailboxProperty.autoReplyMode, paramsKeyValPairs[1]);
                printOnBoth("autoReplyMode = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.AUTOREPLYMSG)) {
                map.add(MailboxProperty.autoReplyMessage, paramsKeyValPairs[1]);
                printOnBoth("autoReplyMessage = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.MAXSTOARGESIZEKB)) {
                map.add(MailboxProperty.maxStorageSizeKB, paramsKeyValPairs[1]);
                printOnBoth("maxStorageSizeKB = " + paramsKeyValPairs[1]);
            }

            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.MAXRECMSGSIZEKB)) {
                map.add(MailboxProperty.maxReceiveMessageSizeKB,
                        paramsKeyValPairs[1]);
                printOnBoth("maxReceiveMessageSizeKB = " + paramsKeyValPairs[1]);
            }

            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.FWDENABLE)) {
                map.add(MailboxProperty.forwardingEnabled, paramsKeyValPairs[1]);
                printOnBoth("forwardingEnabled = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.FWDADDRESS)) {
                map.add(MailboxProperty.forwardingAddress, paramsKeyValPairs[1]);
                printOnBoth("ForwardingAddress = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.NEWFWDADDRESS)) {
                map.add(MailboxProperty.newForwardingAddress,
                        paramsKeyValPairs[1]);
                printOnBoth("newForwardingAddress = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.OLDFWDADDRESS)) {
                map.add(MailboxProperty.oldForwardingAddress,
                        paramsKeyValPairs[1]);
                printOnBoth("oldForwardingAddress = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.DELFWDADDRESS)) {
                // map.add(MailboxProperty.deleteForwardingAddress,
                // paramsKeyValPairs[1]);
                // printOnBoth("deleteForwardingAddress = " +
                // paramsKeyValPairs[1]);
            }

            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.ALLOWEDDOMAIN)) {
                map.add(MailboxProperty.allowedDomain, paramsKeyValPairs[1]);
                printOnBoth("allowedDomains = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.OLDALLOWEDDOMAIN)) {
                map.add(MailboxProperty.oldAllowedDomain, paramsKeyValPairs[1]);
                System.out
                        .println("oldallowedDomains =" + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    LfConstants.NEWALLOWEDDOMAIN)) {
                map.add(MailboxProperty.newAllowedDomain, paramsKeyValPairs[1]);
                System.out
                        .println("newallowedDomains =" + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    MailboxProperty.businessFeaturesEnabled.name())) {
                map.add(MailboxProperty.businessFeaturesEnabled,
                        paramsKeyValPairs[1]);
                printOnBoth("businessFeaturesEnabled = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    MailboxProperty.fullFeaturesEnabled.name())) {
                map.add(MailboxProperty.fullFeaturesEnabled,
                        paramsKeyValPairs[1]);
                printOnBoth("fullFeaturesEnabled = " + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    MailboxProperty.lastFullFeaturesConnectionDate.name())) {
                map.add(MailboxProperty.lastFullFeaturesConnectionDate,
                        paramsKeyValPairs[1]);
                printOnBoth("lastFullFeaturesConnectionDate = "
                        + paramsKeyValPairs[1]);
            } else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
            		AddressBookProperty.sessionId.name())) {
            	map.add(AddressBookProperty.sessionId.name(), LeapFrogTest.sessionidarray.get(0).toString());
          //  	map.add(AddressBookProperty.session.name(), LeapFrogTest.sessionidarray.get(0).toString());
                printOnBoth("" + AddressBookProperty.sessionId.name() + 
                		" == " + LeapFrogTest.sessionidarray.get(0).toString());
        //      printOnBoth("" + AddressBookProperty.session.name() + 
         //       		" == " + LeapFrogTest.sessionidarray.get(0).toString());
            } 
            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
            		AddressBookProperty.cookieString.name())) {
            	map.add(AddressBookProperty.cookieString.name(), LeapFrogTest.cookiesarray.get(0).toString());
                printOnBoth("" + AddressBookProperty.cookieString.name() + " == " + LeapFrogTest.cookiesarray.get(0).toString());
            } 
            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
                    MessageProperty.messageId.name())) {
                map.add(MessageProperty.messageId, paramsKeyValPairs[1]);
            } 
            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
            		AddressBookProperty.groupId.name())) {
            	if (paramsKeyValPairs[1].startsWith(":"))
            	{
            		paramsKeyValPairs[1] = paramsKeyValPairs[1].substring(1);
            		map.add(AddressBookProperty.groupId.name(),paramsKeyValPairs[1]);
            	}
            	else
            	{	
             	map.add(AddressBookProperty.groupId.name(),
            	    LeapFrogTest.groupidarray.get(Integer.parseInt(paramsKeyValPairs[1])).toString());
                printOnBoth("" + AddressBookProperty.groupId.name() + " == " + LeapFrogTest.groupidarray.get(Integer.parseInt(paramsKeyValPairs[1])).toString());
            	}
            } 
            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
            		AddressBookProperty.contactId.name())) {
            	if (paramsKeyValPairs[0].toString().equalsIgnoreCase("placeholder"))
            	{
            	map.add(AddressBookProperty.contactId.name(), LeapFrogTest.contactIdArray.get(0).toString());
                printOnBoth("" + AddressBookProperty.contactId.name() + " == " + LeapFrogTest.contactIdArray.get(0).toString());
            	}
            	else 
            	{
                 	map.add(AddressBookProperty.contactId.name(),
                    	    LeapFrogTest.contactIdArray.get(Integer.parseInt(paramsKeyValPairs[1])).toString());
                        printOnBoth("" + AddressBookProperty.contactId.name() + " == " + LeapFrogTest.contactIdArray.get(Integer.parseInt(paramsKeyValPairs[1])).toString());
            	}
            } 
            else if (paramsKeyValPairs[0].toString().equalsIgnoreCase(
            		AddressBookProperty.memberId.name())) {
            	if (paramsKeyValPairs[0].toString().equalsIgnoreCase("placeholder"))
            	{
            	map.add(AddressBookProperty.memberId.name(), LeapFrogTest.contactIdArray.get(0).toString());
                printOnBoth("" + AddressBookProperty.contactId.name() + " == " + LeapFrogTest.contactIdArray.get(0).toString());
            	}
            	else 
            	{
                 	map.add(AddressBookProperty.memberId.name(),
                    	    LeapFrogTest.contactIdArray.get(Integer.parseInt(paramsKeyValPairs[1])).toString());
                        printOnBoth("" + AddressBookProperty.memberId.name() + " == " + LeapFrogTest.contactIdArray.get(Integer.parseInt(paramsKeyValPairs[1])).toString());
            	}
            }
            else {
                map.add(paramsKeyValPairs[0], paramsKeyValPairs[1]);
                printOnBoth(paramsKeyValPairs[0] + " = " + paramsKeyValPairs[1]);
            }
        }
        return map;
    }
    public static MxosRequestMap expout (String input) {
        alltestlog.info("----------------------------");
        alltestlog.info("-----* Input Params *-------");
        String delimiter = ",";
        String[] inputKeyValPairs = input.split(delimiter);
        String[] paramsKeyValPairs = { null, null };
        String[] paramsKeyValPairs_temp;

        final MxosRequestMap mapexpout = new MxosRequestMap();

        for (int inputCount = 0; inputCount < inputKeyValPairs.length; inputCount++) {
            delimiter = "=";
            paramsKeyValPairs_temp = inputKeyValPairs[inputCount]
                    .split(delimiter);
            if (paramsKeyValPairs_temp.length == 1) {
                paramsKeyValPairs[0] = paramsKeyValPairs_temp[0];
                paramsKeyValPairs[1] = null;
                alltestlog.info("Value for key | " + paramsKeyValPairs[0]
						+ " |,after parsing is : " + paramsKeyValPairs[1]);
            } else {
                paramsKeyValPairs[0] = paramsKeyValPairs_temp[0];
				paramsKeyValPairs[1] = inputKeyValPairs[inputCount]
						.substring(inputKeyValPairs[inputCount].indexOf("=") + 1);
                paramsKeyValPairs[1] = paramsKeyValPairs[1].replaceAll("&&",
                        ",");
                alltestlog.info("Value for key | " + paramsKeyValPairs[0]
						+ " |,after parsing is : " + paramsKeyValPairs[1]);
            }
			try {
				paramsKeyValPairs[1] = new String(
						paramsKeyValPairs[1].getBytes());
				alltestlog.info("UTF-8 encoded string : " + paramsKeyValPairs[1]);
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
                 mapexpout.add(paramsKeyValPairs[0], paramsKeyValPairs[1]);
                printOnBoth(paramsKeyValPairs[0] + " = " + paramsKeyValPairs[1]);
           }
        return mapexpout;
    }

    public static Map<String, String> createParamsMap(String input) {
        String delimiter = ",";
        String[] inputKeyValPairs = input.split(delimiter);

        final Map<String, String> map = new HashMap<String, String>();

        for (String pair: inputKeyValPairs) {
            delimiter = ":";
            String[] params = pair.split(delimiter);
            if (MessageProperty.messageId.name().equals(params[0])) {
                map.put(MessageProperty.messageId.name(), params[1]);
            } else if (MailboxProperty.accountId.name().equals(params[0])) {
                map.put(MailboxProperty.accountId.name(), params[1]);
            }
        }
        return map;
    }
}
