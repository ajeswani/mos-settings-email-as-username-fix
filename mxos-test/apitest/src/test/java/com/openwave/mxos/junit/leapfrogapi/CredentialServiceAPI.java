package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosCredentialsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;

public class CredentialServiceAPI {
    static Logger alltestlog = Logger.getLogger(CredentialServiceAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);

        switch (service) {
            case CredentialService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET CREDENTIAL");
                        testStatus = readCredentials(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE CREDENTIAL");
                        testStatus = updateCredentials(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosCredentialsService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS CREDENTIAL");
                        testStatus = readCosCredentials(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS CREDENTIAL");
                        testStatus = updateCosCredentials(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case AuthenticateService: {
                switch (operation) {
                      case POST: {
                        CommonUtil.printstartAPI("authenticate");
                        testStatus = auth(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
        }
        return 1;
    }

    public static int readCredentials(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Credentials credentials = null;
        int result;
        String method = "read credentials";

        try {
            CommonUtil.printOnBoth(method);
            final ICredentialService CredentialService = (ICredentialService) context
                    .getService(service.name());

            credentials = CredentialService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "..Done");
            CommonUtil.printOnBoth("credentials = " + credentials);
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && credentials != null) {
                result = CommonUtil.comparePayload(credentials.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCredentials(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update credentials";
        try {
            CommonUtil.printOnBoth(method);
            final ICredentialService CredentialService = (ICredentialService) context
                    .getService(service.name());
            CredentialService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosCredentials(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Credentials credentials = null;
        int result;
        String method = "read credentials";

        try {
            CommonUtil.printOnBoth(method);
            final ICosCredentialsService CosCredentialsService = (ICosCredentialsService) context
                    .getService(service.name());
            credentials = CosCredentialsService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "..Done");
            CommonUtil.printOnBoth("credentials = " + credentials);
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && credentials != null) {
                result = CommonUtil.comparePayload(credentials.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCosCredentials(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update credentials";
        try {
            CommonUtil.printOnBoth(method);
            final ICosCredentialsService CosCredentialsService = (ICosCredentialsService) context
                    .getService(service.name());
            CosCredentialsService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int auth(final IMxOSContext context, 
    		ServiceEnum service, MxosRequestMap map,String expErrorCode, 
    		String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Authenticate";
        try {
            CommonUtil.printOnBoth(method);
            final ICredentialService CredentialService = (ICredentialService) context
                    .getService(ServiceEnum.CredentialService.name());
            CredentialService.authenticate(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int verifyCredentialServiceOutput(Credentials credentials,
            String expectedOutput) {
        System.out.println("Verifying CredentialService Output ....");

        String delimiter = ",";
        String[] expectKeyValPairs = expectedOutput.split(delimiter);
        String[] expectinterKeyValPairs;
        int errorcount = 0;
        int paramexist = 0;
        alltestlog.info("paramKeyValPairs  ...  = " + expectKeyValPairs.length);

        // need to handle the case when expected output has non standard pattern
        // Would work it on at the time of tool enhancement phase

        if (expectKeyValPairs.length == 0) {
            return 1;
        }
        for (int count = 0; count < expectKeyValPairs.length; count++) {
            paramexist = 0;
            delimiter = "=";
            expectinterKeyValPairs = expectKeyValPairs[count].split(delimiter);
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.PASSWORD)) {
                paramexist = 1;
                if (credentials.getPassword().toString()
                        .equals(expectinterKeyValPairs[1].toString())) {
                    alltestlog.info("Actual " + expectinterKeyValPairs[0]
                            + " = " + credentials.getPassword() + " Expected "
                            + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                } else {
                    alltestlog.error("ERROR: Actual "
                            + expectinterKeyValPairs[0] + " = "
                            + credentials.getPassword() + " Expected "
                            + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                    errorcount++;
                }
            }
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.PASSSTORETYPE)) {
                paramexist = 1;
                if (credentials.getPasswordStoreType().toString()
                        .equals(expectinterKeyValPairs[1].toString())) {
                    alltestlog.info("Actual " + expectinterKeyValPairs[0]
                            + " = " + credentials.getPasswordStoreType()
                            + " Expected " + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                } else {
                    alltestlog.error("ERROR: Actual "
                            + expectinterKeyValPairs[0] + " = "
                            + credentials.getPasswordStoreType() + " Expected "
                            + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                    errorcount++;
                }
            }
            if (paramexist == 0) {
                alltestlog.error("ERROR: paramer = "
                        + expectinterKeyValPairs[1]
                        + " is not present in actual output");
                errorcount++;
            }
        }
        if (errorcount == 0) {
            return 1;
        } else {
            System.err.println("ERROR: " + errorcount + " errors found");
            alltestlog.error("ERROR: " + errorcount + " errors found");
            return 0;
        }
    }

}
