package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;

public class MailboxAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case MailboxService: {
                switch (operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("createMailbox");
                        testStatus = create(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETE: {
                        CommonUtil.printstartAPI("deleteMailbox");
                        testStatus = delete(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case GET: {
                        CommonUtil.printstartAPI("getMailbox");
                        testStatus = readMailbox(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MailboxBaseService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("getMailboxBase");
                        testStatus = read(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("updateMailboxBase");
                        testStatus = update(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
        
                    }
                    case GETALL: {
                        CommonUtil.printstartAPI("searchMailboxBase");
                        testStatus = search(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
        
                    }
                }
            }
        }
        return 1;
    }

    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            IMailboxService mailboxService = (IMailboxService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("create mailbox");
            mailboxService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth("create mailbox ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("create mailbox", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    /**
     * This function would read mailbox compare for http output with
     * expErrorCode check expected output if checkExpOutput = 1 return 1 on
     * success and 0 in failure
     * 
     * @param mailboxSerivce Mailbox service.
     * @param userName mailboxId domain email OptType expErrorCode
     *            expectedOutput -- e.g userName=xyz,mailboxId=123....
     *            checkExpOutput -- 1 to check expected output -- 0 to ignore
     *            expected output
     * @return on success 1 on failure 0
     */

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        Base basemailbox = null;
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            IMailboxBaseService mailboxbaservice = (IMailboxBaseService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Read base mailbox");
            basemailbox = mailboxbaservice.read(map.getMultivaluedMap());
            alltestlog.info("base mailbox = " + basemailbox);
            CommonUtil.printOnBoth("Read base mailbox ..Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Read base mailbox", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && basemailbox != null) {
                result = CommonUtil.comparePayload(basemailbox.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    /**
     * This function would delete mailbox compare for http output with
     * expErrorCode check expected output if checkExpOutput = 1 return 1 on
     * success and 0 in failure
     * 
     * @param mailboxSerivce Mailbox service.
     * @param userName mailboxId domain email expErrorCode expectedOutput -- e.g
     *            userName=xyz,mailboxId=123.... checkExpOutput -- 1 to check
     *            expected output -- 0 to ignore expected output
     * @return on success 1 on failure 0
     */

    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        int result = 0;
        String errorCode = LfConstants.HTTPSUCCESS;

        try {
            IMailboxService mailboxService = (IMailboxService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Delete mailbox");
            mailboxService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Delete mailbox ...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Delete mailbox", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    /**
     * This function would update mailbox compare for http output with
     * expErrorCode check expected output if checkExpOutput = 1 return 1 on
     * success and 0 in failure
     * 
     * @param mailboxSerivce Mailbox service.
     * @param userName mailboxId domain email expErrorCode expectedOutput -- e.g
     *            userName=xyz,mailboxId=123.... checkExpOutput -- 1 to check
     *            expected output -- 0 to ignore expected output
     * @return on success 1 on failure 0
     */

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        int result;
        String errorCode = LfConstants.HTTPSUCCESS;
        try {
            IMailboxBaseService mailboxbaservice = (IMailboxBaseService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Update mailbox");
            mailboxbaservice.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Update mailbox...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Update mailbox", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int search(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        int result;
        String errorCode = LfConstants.HTTPSUCCESS;
        try {
            IMailboxBaseService mailboxbaservice = (IMailboxBaseService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Search mailbox");
            mailboxbaservice.search(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Search mailbox...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Search mailbox", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    
    public static int readMailbox(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        Mailbox mailbox = null;
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            IMailboxService mailboxService = (IMailboxService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Read mailbox");
            mailbox = mailboxService.read(map.getMultivaluedMap());
            alltestlog.info("mailbox = " + mailbox);
            CommonUtil.printOnBoth("Read mailbox ..Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Read mailbox", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailbox != null) {
                result = CommonUtil.comparePayload(mailbox.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    /**
     * This function would compare actual mailbox output with expectedOutput
     * Print Errors return 1 on success and 0 in failure
     * 
     * @param expectedOutput - Expected output folder -- folder object
     * @return on success 1 on failure 0 At present we have only considered
     *         username,domain,email,mailboxid and password
     */

    public static int verifyMailboxOutput(Mailbox mailbox, String expectedOutput) {

        System.out.println("Verifying Mailbox Output ....");
        alltestlog.info("------------------------------");
        alltestlog.info("----- Verifying Output -------");
        alltestlog.info("------------------------------");

        /*
         * String delimiter = ","; String[] expectKeyValPairs =
         * expectedOutput.split(delimiter); String[] expectinterKeyValPairs_temp
         * = {null,null}; String[] expectinterKeyValPairs = {null,null}; int
         * errorcount = 0; int paramexist = 0; int resultval = 0; //
         * alltestlog.info("paramKeyValPairs  ...  = " +
         * expectKeyValPairs.length); if (expectKeyValPairs.length == 0) {
         * return 1; } for (int count = 0; count < expectKeyValPairs.length;
         * count++) { paramexist = 0; delimiter = "=";
         * expectinterKeyValPairs_temp =
         * expectKeyValPairs[count].split(delimiter); if
         * (expectinterKeyValPairs_temp.length == 1) { expectinterKeyValPairs[0]
         * = expectinterKeyValPairs_temp[0]; expectinterKeyValPairs[1] = null; }
         * else { expectinterKeyValPairs[0] = expectinterKeyValPairs_temp[0];
         * expectinterKeyValPairs[1] = expectinterKeyValPairs_temp[1]; } //
         * alltestlog.info("expectinterKeyValPairs[0] " +
         * expectinterKeyValPairs[0] ); //
         * alltestlog.info("expectinterKeyValPairs[1] " +
         * expectinterKeyValPairs[1] ); if (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.USERNAME)) { paramexist = 1; String
         * actualVal = mailbox.getUserName(); resultval =
         * CommonUtil.compOuputVal(actualVal,expectinterKeyValPairs); errorcount
         * = errorcount + resultval; } else if (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.DOMAIN)) { paramexist = 1; String
         * actualVal = mailbox.getDomain(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.COSID)) {
         * paramexist = 1; String actualVal = mailbox.getCosId(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.EMAIL)) {
         * paramexist = 1; String actualVal = mailbox.getEmail(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.STATUS)) {
         * paramexist = 1; String actualVal = mailbox.getStatus(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.TYPE)) {
         * paramexist = 1; String actualVal = mailbox.getType(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.MAILBOXID))
         * { paramexist = 1; String actualVal =
         * mailbox.getMailboxId().toString(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.PASSWORD)) {
         * paramexist = 1; String actualVal =
         * mailbox.getCredentials().getPassword(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.PASSSTORETYPE)) { paramexist = 1;
         * String actualVal = mailbox.getCredentials().getPasswordStoreType();
         * resultval = CommonUtil.compOuputVal(actualVal,
         * expectinterKeyValPairs); errorcount = errorcount + resultval; } else
         * if (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.LOCALE))
         * { paramexist = 1; String actualVal =
         * mailbox.getGeneralPreferences().getLocale(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.AUTOREPLYMODE)) { paramexist = 1;
         * String actualVal = mailbox.getMailReceipt().getAutoReplyMode();
         * resultval = CommonUtil.compOuputVal(actualVal,
         * expectinterKeyValPairs); errorcount = errorcount + resultval; } else
         * if (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.MAXSTOARGESIZEKB)) { paramexist = 1;
         * String actualVal =
         * mailbox.getMailStore().getMaxStorageSizeKB().toString(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.MAXRECMSGSIZEKB)) { paramexist = 1;
         * String actualVal =
         * mailbox.getMailReceipt().getMaxReceiveMessageSizeKB().toString();
         * resultval = CommonUtil.compOuputVal(actualVal,
         * expectinterKeyValPairs); errorcount = errorcount + resultval; } else
         * if (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.AUTOREPLYMSG)) { paramexist = 1; String
         * actualVal = mailbox.getMailReceipt().getAutoReplyMessage(); resultval
         * = CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.FWDENABLE))
         * { paramexist = 1; String actualVal =
         * mailbox.getMailReceipt().getForwardingEnabled(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.MAXNUMALIASES)) { paramexist = 1;
         * String actualVal = mailbox.getMaxNumAliases().toString(); resultval =
         * CommonUtil.compOuputVal(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.MAXNUMALIASES)) { paramexist = 1; List
         * <String> actualVal = mailbox.getEmailAliases(); resultval =
         * CommonUtil.compOuputValArray(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.MAXNUMALIASES)) { paramexist = 1; List
         * <String> actualVal = mailbox.getEmailAliases(); resultval =
         * CommonUtil.compOuputValArray(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0]
         * .equalsIgnoreCase(LfConstants.ALLOWEDDOMAIN)) { paramexist = 1; List
         * <String> actualVal = mailbox.getAllowedDomains(); resultval =
         * CommonUtil.compOuputValArray(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } else if
         * (expectinterKeyValPairs[0] .equalsIgnoreCase(LfConstants.FWDADDRESS))
         * { paramexist = 1; List <String> actualVal =
         * mailbox.getMailReceipt().getForwardingAddress(); resultval =
         * CommonUtil.compOuputValArray(actualVal, expectinterKeyValPairs);
         * errorcount = errorcount + resultval; } if (paramexist == 0) {
         * alltestlog.error("ERROR: paramer = " + expectinterKeyValPairs[0] +
         * " is not present in actual output"); errorcount++; } } if (errorcount
         * == 0) { alltestlog.info("------------------------------"); return 1;
         * } else { System.err.println("ERROR: " + errorcount +
         * " errors found"); alltestlog.error("ERROR: " + errorcount +
         * " errors found"); alltestlog.info("------------------------------");
         * return 0; }
         */
        return 0;
    }

}
