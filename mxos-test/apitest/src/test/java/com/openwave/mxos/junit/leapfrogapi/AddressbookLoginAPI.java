package com.openwave.mxos.junit.leapfrogapi;



import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;
import com.opwvmsg.mxos.addressbook.pojos.Cookies;
import com.opwvmsg.mxos.addressbook.pojos.Session;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.ILoginUserService;

public class AddressbookLoginAPI {

    static Logger alltestlog = Logger.getLogger(AddressbookLoginAPI.class);
    static String service = "AddressbookLogin API";

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case LoginUserService: {
                switch (operation) {
                    case POST: {
                        CommonUtil.printstartAPI("ADDRESSBOOK LOGIN");
                        testStatus = update(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
          }
        return 1;
    }


    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Session session;
        try {
            final ILoginUserService LoginUserService =
                (ILoginUserService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("update " + service + "service");
            Thread.currentThread().sleep(5000);                        
            session= LoginUserService.loginUser(map.getMultivaluedMap());
            

            CommonUtil.printOnBoth("update " + service + "service ..done");

            CommonUtil.printOnBoth("session id = " + session.getSessionId());
            LeapFrogTest.sessionidarray.add(session.getSessionId());
 
            CommonUtil.printOnBoth("session cookies = " + session.getCookieString());
            String cookies = session.getCookieString();
            CommonUtil.printOnBoth("cookiesAll = " + cookies);
            LeapFrogTest.cookiesarray.add(cookies);
   
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + service
                    + "service", e);
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
