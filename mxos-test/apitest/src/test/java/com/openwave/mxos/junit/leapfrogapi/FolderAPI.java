package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;

public class FolderAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);

        switch (service) {
        case FolderService: {
            switch (operation) {
            case PUT: {
                CommonUtil.printstartAPI("CREATE FOLDER");
                testStatus = createFolder(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case GET: {
                CommonUtil.printstartAPI("GET FOLDER");
                testStatus = readFolder(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE FOLDER");
                testStatus = updateFolder(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case DELETE: {
                CommonUtil.printstartAPI("DELETE FOLDER");
                testStatus = deleteFolder(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case LIST: {
                CommonUtil.printstartAPI("GET FOLDER");
                testStatus = listFolder(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        }
        return 1;
    }

    public static int createFolder(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "create folder";

        try {
            CommonUtil.printOnBoth(method);
            final IFolderService folderService = (IFolderService) context
                    .getService(service.name());
            folderService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readFolder(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Folder folder = null;
        int result;
        String method = "read folder";

        try {
            CommonUtil.printOnBoth(method);
            final IFolderService folderService = (IFolderService) context
                    .getService(service.name());
            folder = folderService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...genpref == " + folder);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && folder != null) {
                result = CommonUtil.comparePayload(folder.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateFolder(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update folder";

        try {
            CommonUtil.printOnBoth(method);
            final IFolderService folderService = (IFolderService) context
                    .getService(service.name());
            folderService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int deleteFolder(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "delete folder";

        try {
            CommonUtil.printOnBoth(method);
            final IFolderService folderService = (IFolderService) context
                    .getService(service.name());
            folderService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    /*
     * This method will list all the folders for a particular mailbox
     */
    public static int listFolder(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<Folder> folderList = null;
        String method = "list folders";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        try {
            CommonUtil.printOnBoth(method);
            final IFolderService folderService = (IFolderService) context
                    .getService(service.name());
            folderList = folderService.list(multiMap);
            CommonUtil.printOnBoth("...genpref == " + folderList);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && folderList != null) {
                result = CommonUtil.comparePayload(folderList.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
}
