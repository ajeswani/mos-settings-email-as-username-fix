package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosGeneralPreferencesService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IGeneralPreferenceService;

public class GeneralPreferenceAPI {

    static Logger alltestlog = Logger.getLogger(GeneralPreferenceAPI.class);

    /**
     * Function to decides read/get/update/delete operation.
     * 
     * @return API call execution status for read/get/update/delete
     */

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);

        switch (service) {
        case GeneralPreferenceService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET GENERAL PRFRENCE");
                testStatus = readGenPref(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE GENERAL PRFRENCE");
                testStatus = updateGenPref(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        case CosGeneralPreferencesService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET COS GENERAL PRFRENCE");
                testStatus = readCosGenPref(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE COS GENERAL PRFRENCE");
                testStatus = updateCosGenPref(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        }
        return 1;
    }

    /**
     * Function for read/get GeneralPreferenceService.
     * 
     * @return 1 for success, 0 for fail
     */

    public static int readGenPref(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        GeneralPreferences genpref = null;
        int result;
        String method = "read GeneralPreference";

        try {
            CommonUtil.printOnBoth(method);
            final IGeneralPreferenceService gpService = (IGeneralPreferenceService) context
                    .getService(service.name());
            genpref = gpService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...genpref == " + genpref);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && genpref != null) {
                result = CommonUtil.comparePayload(genpref.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    /**
     * Function for update GeneralPreferenceService.
     * 
     * @return 1 for success, 0 for fail
     */

    public static int updateGenPref(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update GeneralPreference";
        try {
            CommonUtil.printOnBoth(method);
            final IGeneralPreferenceService gpService = (IGeneralPreferenceService) context
                    .getService(service.name());
            gpService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosGenPref(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        GeneralPreferences genpref = null;
        int result;
        String method = "read Cos GeneralPreference";

        try {
            CommonUtil.printOnBoth(method);
            final ICosGeneralPreferencesService cgpService = (ICosGeneralPreferencesService) context
                    .getService(service.name());
            genpref = cgpService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...genpref == " + genpref);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && genpref != null) {
                result = CommonUtil.comparePayload(genpref.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCosGenPref(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update Cos GeneralPreference";
        try {
            CommonUtil.printOnBoth(method);

            final ICosGeneralPreferencesService cgpService = (ICosGeneralPreferencesService) context
                    .getService(service.name());
            cgpService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

}
