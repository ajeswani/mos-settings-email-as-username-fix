package com.openwave.mxos.junit.leapfrogapi;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedDomainService;

public class AllowedDomainAPI {

    static Logger alltestlog = Logger.getLogger(AllowedDomainAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);

        switch (operation) {
            case PUT: {
                CommonUtil.printstartAPI("ADD ALLOWDDOMAIN");
                testStatus = create(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case GET: {
                CommonUtil.printstartAPI("GET ALLOWD-DOMAIN");
                testStatus = read(context, service, map, expResult, expectedOutput,
                        checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE ALLOWD-DOMAIN");
                testStatus = update(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case DELETE: {
                CommonUtil.printstartAPI("DELETE ALLOWD-DOMAIN");
                testStatus = delete(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
        }
        return 1;
    }

    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            IAllowedDomainService allowedDomainService = (IAllowedDomainService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Add allowedDomains");
            allowedDomainService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Add allowedDomains ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Add allowedDomains", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        List<String> adomain = new ArrayList<String>();
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            IAllowedDomainService allowedDomainService = (IAllowedDomainService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("read allowedDomains");
            adomain = allowedDomainService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("read allowedDomains .. Done ");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read allowedDomains", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && adomain != null) {
                result = CommonUtil.comparePayload(adomain.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            IAllowedDomainService allowedDomainService = (IAllowedDomainService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("update allowedDomains");
            allowedDomainService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update allowedDomains ...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil
                    .getPrintErrorCode("update allowedDomains", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            IAllowedDomainService allowedDomainService = (IAllowedDomainService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("delete allowedDomains");
            allowedDomainService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth("delete allowedDomains .. Done ");
        } catch (MxOSException e) {
            errorCode = CommonUtil
                    .getPrintErrorCode("delete allowedDomains", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int verifyalloweddomainOutput(List<String> adomain,
            String expectedOutput) {

        System.out.println("Verifying allowdDomain Output ....");
        alltestlog.info("------------------------------");
        alltestlog.info("----- Verifying Output -------");
        alltestlog.info("------------------------------");

        String delimiter = ",";
        String[] expectKeyValPairs = expectedOutput.split(delimiter);
        String[] expectinterKeyValPairs;
        int errorcount = 0;
        int paramexist = 0;
        alltestlog.info("paramKeyValPairs  ...  = " + expectKeyValPairs.length);

        // need to handle the case when expected output has non standard pattern
        // Would work it on at the time of tool enhancement phase

        if (expectKeyValPairs.length == 0) {
            return 1;
        }
        for (int count = 0; count < expectKeyValPairs.length; count++) {
            paramexist = 0;
            delimiter = "=";
            expectinterKeyValPairs = expectKeyValPairs[count].split(delimiter);
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.ALLOWEDDOMAIN)) {
                paramexist = 1;
                String[] expectedlist = expectinterKeyValPairs[1].split("&");
                for (int count1 = 0; count1 < expectedlist.length; count1++) {
                    boolean yesorno = adomain.contains(expectedlist[count]);
                    if (yesorno) {
                        alltestlog.info("Expected " + expectinterKeyValPairs[0]
                                + " = " + expectedlist[count] + " is found in "
                                + " Actual " + expectinterKeyValPairs[0]
                                + " = " + adomain.toString());
                    } else {
                        alltestlog.info("ERROR :Expected "
                                + expectinterKeyValPairs[0] + " = "
                                + expectedlist[count] + " is NOT found in "
                                + " Actual " + expectinterKeyValPairs[0]
                                + " = " + adomain.toString());
                        errorcount++;
                    }
                }
            }

            if (paramexist == 0) {
                alltestlog.error("ERROR: paramer = "
                        + expectinterKeyValPairs[1]
                        + " is not present in actual output");
                errorcount++;
            }

        }
        if (errorcount == 0) {
            return 1;
        } else {
            System.err.println("ERROR: " + errorcount + " errors found");
            alltestlog.error("ERROR: " + errorcount + " errors found");
            return 0;
        }
    }

}
