package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
//import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsNotificationService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsOnlineService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsServicesService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsNotificationsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsOnlineService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;

public class smsAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);
        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch(service) {
            case SmsServicesService: {
                switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET SMS SERVICE");
                    testStatus = readSmsService(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE SMS SERVICE");
                    testStatus = updateSmsService(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                }
            }
            case SmsOnlineService: {
                switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET SMS ONLINE");
                    testStatus = readSmsOnline(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE SMS ONLINE");
                    testStatus = updateSmsOnline(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                }
            }
            case SmsNotificationsService: {
                switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET SMS NOTIFICATIONS");
                    testStatus = readSmsNotification(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE SMS NOTIFICATIONS");
                    testStatus = updateSmsNotification(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                }
            }
            case CosSmsServicesService: {
                switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET COS SMS SERVICE");
                    testStatus = readCosSmsServices(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE COS SMS SERVICE");
                    testStatus = updateCosSmsServices(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                }
            }
            case CosSmsOnlineService: {
                switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET COS SMS ONLINE");
                    testStatus = readCosSmsOnlineService(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE COS SMS ONLINE");
                    testStatus = updateCosSmsOnlineService(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                }
            }
/*            case CosSmsNotificationsService: {
                switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET COS SMS NOTIFICATIONS");
                    testStatus = readCosSmsNotificationService(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE COS SMS NOTIFICATIONS");
                    testStatus = updateCosSmsNotificationService(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                }
            }
*/        }
        return 1;
    }

    public static int readSmsService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        SmsServices smsreturn = null;
        String Service = "smsservice";
        int result;
        try {
            CommonUtil.printOnBoth("read " + Service);
            final ISmsServicesService SmsServicesService = (ISmsServicesService) context
                    .getService(service.name());
            smsreturn = SmsServicesService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...smsservice == " + smsreturn);
            CommonUtil.printOnBoth("read " + Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && smsreturn != null) {
                result = CommonUtil.comparePayload(smsreturn.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateSmsService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String Service = "smsservice";
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final ISmsServicesService SmsServicesService = (ISmsServicesService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("update " + Service);
            SmsServicesService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readSmsOnline(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        SmsOnline smsonlinereturn = null;
        String Service = "smsonline";
        int result;
        try {
            CommonUtil.printOnBoth("read " + Service);
            final ISmsOnlineService SmsOnlineService = (ISmsOnlineService) context
                    .getService(service.name());

            smsonlinereturn = SmsOnlineService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...smsonline == " + smsonlinereturn);
            CommonUtil.printOnBoth("read " + Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && smsonlinereturn != null) {
                result = CommonUtil.comparePayload(smsonlinereturn.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateSmsOnline(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String Service = "smsonline";
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final ISmsOnlineService SmsOnlineService = (ISmsOnlineService) context
                    .getService(service.name());

            CommonUtil.printOnBoth("update " + Service);
            SmsOnlineService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readSmsNotification(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        SmsNotifications smsnotificationreturn = null;
        String Service = "smsnotification";
        int result;
        try {

            CommonUtil.printOnBoth("read " + Service);
            final ISmsNotificationsService SmsNotificationsService = (ISmsNotificationsService) context
                    .getService(service.name());
            smsnotificationreturn = SmsNotificationsService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("...smsnotification == "
                    + smsnotificationreturn);
            CommonUtil.printOnBoth("read " + Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && smsnotificationreturn != null) {
                result = CommonUtil.comparePayload(
                        smsnotificationreturn.toString(), expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateSmsNotification(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String Service = "smsnotification";
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            CommonUtil.printOnBoth("update " + Service);
            final ISmsNotificationsService SmsNotificationsService = (ISmsNotificationsService) context
                    .getService(service.name());
            SmsNotificationsService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosSmsServices(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        SmsServices smsreturn = null;
        String Service = " read CosSmsServicesService";
        int result;
        try {
            CommonUtil.printOnBoth(Service);
            final ICosSmsServicesService CosSmsServicesService = (ICosSmsServicesService) context
                    .getService(service.name());
            smsreturn = CosSmsServicesService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...CosSmsServicesService == " + smsreturn);
            CommonUtil.printOnBoth(Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && smsreturn != null) {
                result = CommonUtil.comparePayload(smsreturn.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCosSmsServices(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String Service = "Update CosSmsServicesService";
        try {
            CommonUtil.printOnBoth(Service);
            final ICosSmsServicesService CosSmsServicesService = (ICosSmsServicesService) context
                    .getService(service.name());
            CosSmsServicesService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosSmsOnlineService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        SmsOnline smsonlinereturn = null;
        String Service = "Read CosSmsOnlineService";
        int result;
        try {
            CommonUtil.printOnBoth(Service);
            final ICosSmsOnlineService CosSmsOnlineService = (ICosSmsOnlineService) context
                    .getService(service.name());
            smsonlinereturn = CosSmsOnlineService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...cossmsonline == " + smsonlinereturn);
            CommonUtil.printOnBoth(Service + "..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && smsonlinereturn != null) {
                result = CommonUtil.comparePayload(smsonlinereturn.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCosSmsOnlineService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String Service = "Update CosSmsOnlineService";
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            CommonUtil.printOnBoth(Service);
            final ICosSmsOnlineService CosSmsOnlineService = (ICosSmsOnlineService) context
                    .getService(service.name());

            CosSmsOnlineService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

/*    public static int readCosSmsNotificationService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        SmsNotifications smsnotificationreturn = null;
        String Service = "Read CosSmsNotificationService";
        int result;
        try {
            CommonUtil.printOnBoth(Service);
            final ICosSmsNotificationService CosSmsNotificationService = (ICosSmsNotificationService) context
                    .getService(service.name());
            smsnotificationreturn = CosSmsNotificationService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("Cossmsnotification == "
                    + smsnotificationreturn);
            CommonUtil.printOnBoth(Service + "..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && smsnotificationreturn != null) {
                result = CommonUtil.comparePayload(
                        smsnotificationreturn.toString(), expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }
*/
/*    public static int updateCosSmsNotificationService(
            final IMxOSContext context, ServiceEnum service, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        String Service = "Update CosSmsNotificationService";
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            CommonUtil.printOnBoth(Service);
            final ICosSmsNotificationService CosSmsNotificationService = (ICosSmsNotificationService) context
                    .getService(service.name());
            CosSmsNotificationService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(Service + "..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(Service, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
*/
}
