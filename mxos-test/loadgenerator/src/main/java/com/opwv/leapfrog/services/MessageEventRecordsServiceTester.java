package com.opwv.leapfrog.services;

import java.util.Random;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMessageEventRecordsService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on MessageEventRecordsService.
 * 
 * @author mxos-dev
 */
public class MessageEventRecordsServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(MessageEventRecordsServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public MessageEventRecordsServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * MailSendBMIFiltersService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isUpdateEnabled()) {
                updateMessageEventRecords();
            }

            if (properties.isReadEnabled()) {
                readMessageEventRecords();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for read operation for MessageEventRecordsService.
     */
    private MessageEventRecords readMessageEventRecords() {

        MessageEventRecords messageEventRecords = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MessageEventRecordsService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IMessageEventRecordsService testService = (IMessageEventRecordsService) mxoscontext
                    .getService(serviceName.name());
            messageEventRecords = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            if (properties.isDebug()) {
                logger.debug("For user " + emailId
                        + " MessageEventRecords are "
                        + messageEventRecords.toString());
            }

            logger.info("Successfully read MessageEventRecords for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return messageEventRecords;
    }

    /**
     * Method for update operation for MessageEventRecordsService.
     */

    private void updateMessageEventRecords() {

        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MessageEventRecordsService;
        Random rTmp = new Random();
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.eventRecordingEnabled.name(),
                HelperFunctions.getConfirmation());
        mxosRequestMap.add(MailboxProperty.maxHeaderLength.name(),
                rTmp.nextInt(256));
        mxosRequestMap.add(MailboxProperty.maxFilesSizeKB.name(),
                rTmp.nextInt(256));
        time = Stats.startTimer();
        try {
            IMessageEventRecordsService testService = (IMessageEventRecordsService) mxoscontext
                    .getService(serviceName.name());
            testService.update(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully updated MessageEventRecords for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }
}
