/*
test * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/TestProperties.java#1 $
 */

/**
 * Class for storing test properties used by Load generator
 *
 * @author mxos-dev
 */

package com.opwv.leapfrog.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

/**
 * Class for storing test properties.
 * 
 * @author mxos-dev
 **/
public class TestProperties {

    private Vector<String> blobData;
    private Vector<String> headerSummary;
    private int numstartThreads;
    private int numendThreads;
    private long testInterval = 0L;
    private boolean flag = true;
    private boolean read;
    private boolean delete = false;
    private boolean create = false;
    private boolean update = false;
    private boolean search = false;
    private boolean process = false;
    private boolean authenticate = false;
    private long mboxIdStart;
    private long numMboxes;
    private String hdrSummaryFile;
    private boolean customEnabled;
    private int statsInterval;
    private boolean debug = false;
    private String serviceToTest;
    private String oxHttpURL;
    private String runToolAs;
    private String defaultCos;
    private String usernamePrefix;
    private String defaultDomain;
    private long testCount = 0L;
    private long testDuration;
    private boolean durationRun;
    private long eachThreadOperationsCount;
    private long testRampUpInterval;
    private boolean usersRandom = true;
    private long eachthreadMboxes;
    private String bgcMxosBaseUrl;
    private String mxosBaseUrl;

    public String getBgcMxosBaseUrl() {
        return bgcMxosBaseUrl;
    }

    public void setBgcMxosBaseUrl(String bgcMxosBaseUrl)
            throws InvalidDataException {
        if (bgcMxosBaseUrl != null) {
            this.bgcMxosBaseUrl = bgcMxosBaseUrl;
        } else {
            throw new InvalidDataException("Please specify valid "
                    + LoadGeneratorConstants.BGCMXOSBASEURL);
        }
    }

    public String getMxosBaseUrl() {
        return mxosBaseUrl;
    }

    public void setMxosBaseUrl(String mxosBaseUrl) throws InvalidDataException {

        if (mxosBaseUrl != null) {
            this.mxosBaseUrl = mxosBaseUrl;
        } else {
            throw new InvalidDataException("Please specify valid "
                    + LoadGeneratorConstants.MXOSBASEURL);
        }
    }

    /**
     * Default Constructor.
     */
    public TestProperties() {
    }

    /**
     * Method for getting NumstartThreads for the test.
     * 
     * @return numstartThreads Number of start threads.
     */
    public int getNumstartThreads() {
        return numstartThreads;
    }

    /**
     * Method for setting NumstartThreads for the test.
     * 
     * @param numstartThreads Number of start threads.
     */
    public void setNumstartThreads(String numstartThreads)
            throws InvalidDataException {
        try {
            this.numstartThreads = HelperFunctions
                    .validateInteger(numstartThreads);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.NUMSTARTTHREADS);
        }
    }

    /**
     * Method for getting NumendThreads for the test.
     * 
     * @return numendThreads Number of end threads.
     */
    public int getNumendThreads() {
        return numendThreads;
    }

    /**
     * Method for setting NumendThreads for the test.
     * 
     * @param numendThreads Number of end threads.
     */
    public void setNumendThreads(String numendThreads)
            throws InvalidDataException {
        try {
            this.numendThreads = HelperFunctions.validateInteger(numendThreads);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.NUMENDTHREADS);
        }
        if (this.numendThreads < this.numstartThreads) {
            throw new InvalidDataException(LoadGeneratorConstants.NUMENDTHREADS
                    + " < " + LoadGeneratorConstants.NUMSTARTTHREADS);
        }
    }

    /**
     * Method for getting test Interval.
     * 
     * @return testInterval Test interval.
     */
    public long getTestInterval() {
        return testInterval;
    }

    /**
     * Method for setting Interval for the test.
     * 
     * @param testInterval Test Interval.
     */
    public void setTestInterval(String testInterval)
            throws InvalidDataException {

        setDurationRun(true);
        String[] testSecs = testInterval
                .split(LoadGeneratorConstants.DEFAULTSEPERATOR);
        try {
            if (testSecs.length == 2) {
                setTestRampUpInterval(HelperFunctions.validateLong(testSecs[0]));
                setTestDuration(HelperFunctions.validateLong(testSecs[1]));
            } else if (testSecs.length == 1) {
                setTestRampUpInterval(0L);
                setTestDuration(HelperFunctions.validateLong(testSecs[0]));
            } else {
                throw new InvalidDataException(
                        "Invalid value for test parameter "
                                + LoadGeneratorConstants.TESTINTERVAL);
            }
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.TESTINTERVAL);
        }

    }

    public void setTestRampUpInterval(long testRampUpInterval) {
        this.testRampUpInterval = testRampUpInterval;

    }

    public long getTestRampUpInterval() {
        return testRampUpInterval;
    }

    /**
     * Method for checking whether flag for the test is set or not.
     * 
     * @return flag Flag for the test.
     */
    public boolean isflagSet() {
        return flag;
    }

    /**
     * Method for setting flag for the test.
     * 
     * @param flag Flag for the test.
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * Method for checking whether read CRUD operation is enabled for the test
     * is or not.
     * 
     * @return read Flag for the test.
     */
    public boolean isReadEnabled() {
        return read;
    }

    /**
     * Method for setting read CRUD operation for the test.
     * 
     * @param read Flag for the test.
     * @throws InvalidDataException
     */
    public void setReadEnabled(String read) throws InvalidDataException {
        try {
            this.read = HelperFunctions.validateBoolean(read);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.OPRREAD);
        }
    }

    /**
     * Method for checking whether read CRUD operation is enabled for the test
     * is or not.
     * 
     * @return read Flag for the test.
     */
    public boolean isUpdateEnabled() {
        return update;
    }

    /**
     * Method for setting read CRUD operation for the test.
     * 
     * @param read Flag for the test.
     * @throws InvalidDataException
     */
    public void setUpdateEnabled(String update) throws InvalidDataException {
        try {
            this.update = HelperFunctions.validateBoolean(update);
            System.out.println("Shantanu" + this.update);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.OPRUPDATE);
        }
    }

    /**
     * Method for checking whether read CRUD operation is enabled for the test
     * is or not.
     * 
     * @return read Flag for the test.
     */
    public boolean isSearchEnabled() {
        return search;
    }

    /**
     * Method for setting read CRUD operation for the test.
     * 
     * @param read Flag for the test.
     * @throws InvalidDataException
     */
    public void setSearchEnabled(String search) throws InvalidDataException {
        try {
            this.search = HelperFunctions.validateBoolean(search);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.OPRSEARCH);
        }
    }

    /**
     * Method for checking whether delete CRUD operation is enabled for the test
     * is or not.
     * 
     * @return delete Flag for the test.
     */
    public boolean isDeleteEnabled() {
        return delete;
    }

    /**
     * Method for setting delete CRUD operation for the test.
     * 
     * @param delete Flag for the test.
     * @throws InvalidDataException
     */
    public void setDeleteEnabled(String delete) throws InvalidDataException {
        try {
            this.delete = HelperFunctions.validateBoolean(delete);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.OPRDELE);
        }
    }

    /**
     * Method for checking whether create CRUD operation is enabled for the test
     * is or not.
     * 
     * @return create Flag for the test.
     */
    public boolean isCreateEnabled() {
        return create;
    }

    /**
     * Method for setting create CRUD operation for the test.
     * 
     * @param create Flag for the test.
     * @throws InvalidDataException
     */
    public void setCreateEnabled(String create) throws InvalidDataException {

        try {
            this.create = HelperFunctions.validateBoolean(create);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.OPRCREATE);
        }
    }

    /**
     * Method for getting mailbox id start for the test.
     * 
     * @return mboxIdStart mailbox id start for the test.
     */
    public long getMboxIdStart() {
        return mboxIdStart;
    }

    /**
     * Method for setting Starting mailboxId for the test.
     * 
     * @param mboxIdStart Starting mailboxId for the test.
     */
    public void setMboxIdStart(String mboxIdStart) throws InvalidDataException {
        try {
            this.mboxIdStart = HelperFunctions.validateLong(mboxIdStart);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.MBOXSTART);
        }
    }

    /**
     * Method for getting Total number of Mailboxes for the test.
     * 
     * @return numMboxes Total mailboxes for the test.
     */
    public long getNumMboxes() {
        return numMboxes;
    }

    /**
     * Method for setting Total number of Mailboxes for the test.
     * 
     * @param numMboxes Total mailboxes for the test.
     */
    public void setNumMboxes(String numMboxes) throws InvalidDataException {

        try {
            this.numMboxes = HelperFunctions.validateLong(numMboxes);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.NUMMBOXES);
        }
    }

    /**
     * Method for getting Blob file for the test.
     * 
     * @return blobFile Blob file for the test.
     */
    public Vector<String> getMessageFiles() {
        return blobData;
    }

    public Vector<String> getHeaderSummary() {
        return headerSummary;
    }

    public static StringBuffer readFile(String file) throws IOException {
        StringBuffer sb = new StringBuffer();
        FileReader fin = new FileReader(file);
        BufferedReader br = new BufferedReader(fin);
        String s1;
        while ((s1 = br.readLine()) != null) {
            sb.append(s1);
        }
        br.close();
        return sb;
    }

    /**
     * Method for setting Blob file for the test.
     * 
     * @param blobFile Blob file for the test.
     * @throws InvalidDataException, IOException
     */
    public void setMessageFiles(String messageFiles)
            throws InvalidDataException, IOException {
        blobData = new Vector<String>();
        headerSummary = new Vector<String>();

        int count = 0;
        String[] blobFiles = messageFiles
                .split(LoadGeneratorConstants.DEFAULTSEPERATOR);
        if (blobFiles.length >= 1) {
            String[] filenameAndCount;
            int numFiles = 0;
            for (String blobFile : blobFiles) {
                try {
                    filenameAndCount = blobFile
                            .split(LoadGeneratorConstants.COUNTSEPERATOR);
                    numFiles = HelperFunctions
                            .validateInteger(filenameAndCount[1]);
                    count += numFiles;
                } catch (Exception e) {
                    throw new InvalidDataException(
                            "Invalid count in test parameter "
                                    + LoadGeneratorConstants.MESSAGEFILES);
                }
                StringBuffer sbblob = readFile(filenameAndCount[0]);
                for (int i = 0; i < numFiles; i++) {
                    blobData.add(sbblob.toString());
                    String newheaderSummary = sbblob.toString();
                    while (newheaderSummary.length() < LoadGeneratorConstants.DEFAULTHSSIZE) {
                        newheaderSummary = newheaderSummary
                                .concat(newheaderSummary);
                    }
                    headerSummary.add(newheaderSummary.substring(0,
                            LoadGeneratorConstants.DEFAULTHSSIZE));
                }
            }
        } else {
            throw new InvalidDataException("Invalid data for test parameter "
                    + LoadGeneratorConstants.MESSAGEFILES);
        }
        if (count != 100) {
            throw new InvalidDataException(
                    "The sum of messageFiles should be 100. Verify test parameter "
                            + LoadGeneratorConstants.MESSAGEFILES);
        }
    }

    /**
     * Method for getting Header summary file for the test.
     * 
     * @return hdrSummaryFile Header summary file for the test.
     */
    public String getHdrSummaryFile() {
        return hdrSummaryFile;
    }

    /**
     * Method for setting Header summary file for the test.
     * 
     * @param hdrSummaryFile Header summary file for the test.
     */
    public void setHdrSummaryFile(String hdrSummaryFile) {
        this.hdrSummaryFile = hdrSummaryFile;
    }

    /**
     * Method for getting statsInterval parameter for the test.
     * 
     * @return statsInterval statsInterval parameter for the test.
     */
    public int getStatsInterval() {
        return statsInterval;
    }

    /**
     * Method for setting statsInterval parameter for the test.
     * 
     * @param statsInterval statsInterval parameter for the test.
     */
    public void setStatsInterval(String statsInterval)
            throws InvalidDataException {
        try {
            this.statsInterval = HelperFunctions.validateInteger(statsInterval);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.STATSINTERVAL);
        }
        if (this.statsInterval < 0) {
            throw new InvalidDataException(
                    "LoadGeneratorConstants.STATSINTERVAL can not be negative");
        }
    }

    /**
     * Method for setting debug parameter for the test.
     * 
     * @return debug debug parameter for the test.
     */
    public boolean isDebug() {
        return debug;
    }

    /**
     * Method for setting debug parameter for the test.
     * 
     * @param debug debug parameter for the test.
     * @throws InvalidDataException
     */
    public void setDebugEnabled(String debug) throws InvalidDataException {
        try {
            this.debug = HelperFunctions.validateBoolean(debug);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.DEBUG);
        }
    }

    /**
     * Method for getting componentToTest parameter for the test.
     * 
     * @return componentToTest componentToTest parameter for the test.
     */
    public String getServiceToTest() {
        return serviceToTest;
    }

    /**
     * Method for setting componentToTest parameter for the test.
     * 
     * @param componentToTest componentToTest parameter for the test.
     * @throws InvalidDataException
     */
    public void setServiceToTest(String serviceToTest)
            throws InvalidDataException {
        validateServiceToTest(serviceToTest);
        this.serviceToTest = serviceToTest;
    }

    /**
     * Method for getting runToolAs parameter for the test.
     * 
     * @return runToolAs RunToolAs parameter for the test.
     */
    public String getRunToolAs() {
        return runToolAs;
    }

    /**
     * Method for setting runToolAs parameter for the test.
     * 
     * @param runToolAs RunToolAs parameter for the test.
     * @throws InvalidDataException
     */
    public void setRunToolAs(String runToolAs) throws InvalidDataException {
        validateRunToolAs(runToolAs);
        this.runToolAs = runToolAs;
    }

    /**
     * Method for setting defaultCos parameter for the test.
     * 
     * @param defaultCos DefaultCos parameter for the test.
     * 
     */
    public void setDefaultCos(String defaultCos) {
        this.defaultCos = defaultCos;
    }

    /**
     * Method for getting defaultCos parameter for the test.
     * 
     * @return defaultCos DefaultCos parameter for the test.
     * 
     */
    public String getDefaultCos() {
        return defaultCos;
    }

    /**
     * Method for setting usernamePrefix parameter for the test.
     * 
     * @param usernamePrefix UsernamePrefix parameter for the test.
     * 
     */
    public void setUsernamePrefix(String usernamePrefix) {
        this.usernamePrefix = usernamePrefix;
    }

    /**
     * Method for getting usernamePrefix parameter for the test.
     * 
     * @return usernamePrefix UsernamePrefix parameter for the test.
     * 
     */
    public String getUsernamePrefix() {
        return usernamePrefix;
    }

    /**
     * Method for setting defaultDomain parameter for the test.
     * 
     * @param defaultDomain defaultDomain parameter for the test.
     * 
     */
    public void setDefaultDomain(String defaultDomain) {
        this.defaultDomain = "@" + defaultDomain;
    }

    /**
     * Method for getting defaultDomain parameter for the test.
     * 
     * @return defaultDomain defaultDomain parameter for the test.
     * 
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Method for setting testCount for the test.
     * 
     * @param testCount testCount parameter for the test.
     * @throws InvalidDataException
     */
    public void setTestCount(String testCount) throws InvalidDataException {
        try {
            this.testCount = HelperFunctions.validateLong(testCount);
            setDurationRun(false);
            this.setTestDuration(0L);
            this.setTestRampUpInterval(0L);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.TESTCOUNT);
        }
    }

    /**
     * Method for getting testCount for the test.
     * 
     * @param testCount testCount parameter for the test.
     * @throws InvalidDataException
     */
    public long getTestCount() {
        return testCount;
    }

    /**
     * Method for enabling or disabling mailbox duration run mode for the test.
     * 
     * @param value true for enabling and false for disabling the duration run
     *            mode.
     * @throws InvalidDataException if Any.
     */
    public void setDurationRun(boolean durationRun) {
        this.durationRun = durationRun;
    }

    /**
     * Method to verify if duration run mode is enabled or disabled for the
     * test.
     * 
     * @return durationRun true for enabling and false for disabling the
     *         duration run mode.
     * 
     */
    public boolean isDurationRun() {
        return durationRun;
    }

    /**
     * Method to set the iteration count for each thread.
     * 
     * @param eachThreadOperationsCount the iteration count for each thread
     * 
     */
    public void setEachThreadOperationsCount(long eachThreadOperationsCount) {
        this.eachThreadOperationsCount = eachThreadOperationsCount;
    }

    /**
     * Method to get the iteration count for each thread.
     * 
     * @return eachThreadOperationsCount the iteration count for each thread
     * 
     */
    public long getEachThreadOperationsCount() {
        return eachThreadOperationsCount;
    }

    /**
     * Method to validate runToolAs parameter for the test.
     * 
     * @param propertyValue tunToolAs parameter either BACKEND or REST.
     * @throws InvalidDataException if Any
     */
    private void validateRunToolAs(String propertyValue)
            throws InvalidDataException {
        boolean isInvalid;
        if (propertyValue.equals(LoadGeneratorConstants.BACKEND)) {
            isInvalid = false;
        } else if (propertyValue.equals(LoadGeneratorConstants.REST)) {
            isInvalid = false;
        } else {
            isInvalid = true;
        }
        if (isInvalid) {
            throw new InvalidDataException(
                    "Invalid value for test parameter runToolAs. It should be either "
                            + LoadGeneratorConstants.BACKEND + " or "
                            + LoadGeneratorConstants.REST);
        }
    }

    /**
     * Method to validate componentToTest parameter for the test.
     * 
     * @param propertyValue componentToTest parameter Mailbox, Folder or
     *            Message.
     * @throws InvalidDataException if Any
     */
    private void validateServiceToTest(String propertyValue)
            throws InvalidDataException {
        {
            boolean isInValid = true;
            List<String> validServices = DefaultParams.getAvailableServices();

            for (String serviceName : validServices) {
                if (propertyValue.equals(serviceName)) {
                    isInValid = false;
                    break;
                }
            }

            if (isInValid) {
                throw new InvalidDataException(
                        "Invalid value for test parameter serviceToTest. ");
            }
        }
    }

    /**
     * Method to set duration in seconds for the test.
     * 
     * @param testDuration duration in seconds for the test.
     */
    public void setTestDuration(long testDuration) {
        this.testDuration = testDuration;
    }

    /**
     * Method to get duration in seconds for the test.
     * 
     * @return testDuration duration in seconds for the test.
     */
    public long getTestDuration() {
        return testDuration;
    }

    public void setUsersRandom(String usersRandom) throws InvalidDataException {
        try {
            this.usersRandom = HelperFunctions.validateBoolean(usersRandom);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.USERSRANDOM);
        }
    }

    /**
     * Method to check whether the tool should select users randomly for the
     * test or not.
     * 
     * @return usersRandom true/false.
     */
    public boolean isUsersRandom() {
        return usersRandom;
    }

    /**
     * Method to set the number of mailboxes to be processed by each thread by
     * the tool during the test.
     */
    public void setEachthreadMboxes(long eachthreadMboxes) {
        this.eachthreadMboxes = eachthreadMboxes;
    }

    /**
     * Method to get the number of mailboxes to be processed by each thread by
     * the tool during the test.
     * 
     * @return eachthreadMboxes Number of mailboxes to be processed by each
     *         thread
     */
    public long getEachthreadMboxes() {
        return eachthreadMboxes;
    }

    /**
     * Method to set the mandatory property for the services.
     * 
     * @throws InvalidDataException if any.
     * @throws IOException
     */
    public void setMandatoryProperty(final String propertyName,
            final String propertyValue) throws InvalidDataException,
            IOException {

        if (propertyValue != null) {

            if (propertyName.equals(LoadGeneratorConstants.USRNAMEPREFIX)) {
                setUsernamePrefix(propertyValue);
            } else if (propertyName
                    .equals(LoadGeneratorConstants.DEFAULTDOMAIN)) {
                setDefaultDomain(propertyValue);
            } else if (propertyName.equals(LoadGeneratorConstants.MBOXSTART)) {
                setMboxIdStart(propertyValue);
            } else if (propertyName.equals(LoadGeneratorConstants.NUMMBOXES)) {
                setNumMboxes(propertyValue);
            } else if (propertyName.equals(LoadGeneratorConstants.MESSAGEFILES)) {
                setMessageFiles(propertyValue);
            } else if (propertyName.equals(LoadGeneratorConstants.OXHTTPURL)) {
                setOxHttpURL(propertyValue);
            }
        } else {
            throw new InvalidDataException("Invalid value for test parameter "
                    + propertyName + " for service " + getServiceToTest());
        }
    }

    /**
     * Method to set the optional property for the services.
     * 
     * @throws InvalidDataException if any.
     */
    public void setOptionalProperty(final String propertyName,
            final String propertyValue) throws InvalidDataException {
        if (propertyValue != null) {

        }
    }

    /**
     * Method to enable the CRUD operations for the services.
     * 
     * @throws InvalidDataException if any.
     */
    public void setServiceOperations(final String[] allowdOpr, Properties pFile)
            throws InvalidDataException {
        for (final String oprName : allowdOpr) {
            final String oprValue = pFile.getProperty(oprName);
            if (oprValue == null) {
                throw new InvalidDataException("Operation " + oprName
                        + " for service " + getServiceToTest()
                        + " is undefined in test properties file");
            } else {
                if (oprName.equals(LoadGeneratorConstants.OPRCREATE)) {
                    setCreateEnabled(oprValue);
                } else if (oprName.equals(LoadGeneratorConstants.OPRREAD)) {
                    setReadEnabled(oprValue);
                } else if (oprName.equals(LoadGeneratorConstants.OPRDELE)) {
                    setDeleteEnabled(oprValue);
                } else if (oprName.equals(LoadGeneratorConstants.OPRUPDATE)) {
                    setUpdateEnabled(oprValue);
                } else if (oprName.equals(LoadGeneratorConstants.OPRSEARCH)) {
                    setSearchEnabled(oprValue);
                } else if (oprName.equals(LoadGeneratorConstants.OPRPROCESS)) {
                    setProcessEnabled(oprValue);
                } else if (oprName
                        .equals(LoadGeneratorConstants.OPRAUTHENTICATE)) {
                    setAuthenticateEnabled(oprValue);
                }
            }
        }
    }

    public boolean isProcessEnabled() {
        return process;
    }

    /**
     * Method for setting create CRUD operation for the test.
     * 
     * @param create Flag for the test.
     * @throws InvalidDataException
     */
    public void setProcessEnabled(String process) throws InvalidDataException {

        try {
            this.process = HelperFunctions.validateBoolean(process);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.OPRPROCESS);
        }
    }

    public void setAuthenticateEnabled(String authenticate)
            throws InvalidDataException {
        try {
            this.authenticate = HelperFunctions.validateBoolean(authenticate);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.OPRAUTHENTICATE);
        }
    }

    public boolean isAuthenticateEnabled() {
        return authenticate;
    }

    public void setOxHttpURL(String oxHttpURL) {
        this.oxHttpURL = oxHttpURL;
    }

    public String getOxHttpURL() {
        return oxHttpURL;
    }

    public void setCustomEnabled(String customEnabled)
            throws InvalidDataException {
        try {
            this.customEnabled = HelperFunctions.validateBoolean(customEnabled);
        } catch (Exception e) {
            throw new InvalidDataException("Invalid value for test parameter "
                    + LoadGeneratorConstants.CUSTOM);
        }
    }

    public String isCustomEnabled() {
        return Boolean.toString(customEnabled);
    }
}
