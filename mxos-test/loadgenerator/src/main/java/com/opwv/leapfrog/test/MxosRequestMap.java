/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/InvalidDataException.java#1 $
 */

/**
 * InvalidDataException used by Load generator in case of exception.
 *
 * @author mxos-dev
 */

package com.opwv.leapfrog.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * Helper class for creating a Map of arguments required by Mxos for processing
 * the requests.
 * 
 * @author mxos-dev
 * 
 */
public class MxosRequestMap {
    Map<String, List<String>> multivaluedMap;

    /**
     * Constructor.
     */
    public MxosRequestMap() {
        multivaluedMap = new HashMap<String, List<String>>();
    }

    public void add(DataMap.Property property, Object value) {
        add(property.name(), value.toString());
    }

    public void add(String property, Object value) {
        add(property, value.toString());
    }

    public void add(DataMap.Property property, String value) {
        add(property.name(), value);
    }

    public void add(String key, String value) {
        if (multivaluedMap.containsKey(key)) {
            multivaluedMap.get(key).add(value);
        } else {
            List<String> list = new ArrayList<String>();
            list.add(value);
            multivaluedMap.put(key, list);
        }
    }

    public void remove(String key) {
        if (multivaluedMap.containsKey(key)) {
            multivaluedMap.get(key).remove(key);
        }
    }

    public Map<String, List<String>> getMultivaluedMap() {
        return multivaluedMap;
    }

    public void setMultivaluedMap(Map<String, List<String>> multivaluedMap) {
        this.multivaluedMap = multivaluedMap;
    }
}
