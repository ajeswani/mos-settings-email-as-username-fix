/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/InvalidDataException.java#1 $
 */

/**
 * InvalidDataException used by Load generator in case of exception.
 *
 * @author mxos-dev
 */
package com.opwv.leapfrog.test;

/**
 * InvalidDataException used by Load generator in case of exception.
 * 
 * @author mxos-dev
 */
public class InvalidDataException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * 
     * @param message Message representing exception details.
     */
    public InvalidDataException(final String message) {
        super(message);
    }
}
