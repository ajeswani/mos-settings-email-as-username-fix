package com.opwv.leapfrog.services;

import java.util.List;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IBlockedSendersListService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on BlockedSendersListService.
 * 
 * @author mxos-dev
 */
public class BlockedSendersListServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(BlockedSendersListServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public BlockedSendersListServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * BlockedSendersListService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isCreateEnabled()) {
                createBlockedSendersList();
            }

            if (properties.isReadEnabled()) {
                readBlockedSendersList();
            }

            if (properties.isUpdateEnabled()) {
                updateBlockedSendersList();
            }

            if (properties.isDeleteEnabled()) {
                deleteBlockedSendersList();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for BlockedSendersListService.
     */
    private void createBlockedSendersList() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.BlockedSendersListService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.blockedSender.name(),
                HelperFunctions.getRandomUser(properties));
        time = Stats.startTimer();
        try {
            IBlockedSendersListService testService = (IBlockedSendersListService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created Blocked Senders List for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for BlockedSendersListService.
     */
    private List<String> readBlockedSendersList() {

        List<String> blockedSendersList = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.BlockedSendersListService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IBlockedSendersListService testService = (IBlockedSendersListService) mxoscontext
                    .getService(serviceName.name());
            blockedSendersList = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            if (properties.isDebug()) {
                if (blockedSendersList != null && blockedSendersList.size() > 0) {
                    logger.debug("For user " + emailId
                            + " AllowedSenders address are "
                            + blockedSendersList.toString());
                } else {
                    logger.debug("For user " + emailId
                            + " BlockedSendersList not set.");
                }
            }
            logger.info("Successfully read BlockedSendersList for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return blockedSendersList;
    }

    /**
     * Method for update operation for BlockedSendersListService.
     */

    private void updateBlockedSendersList() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.BlockedSendersListService;
        List<String> blockedSendersList = readBlockedSendersList();
        if (blockedSendersList != null && blockedSendersList.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.oldBlockedSender.name(),
                    blockedSendersList.get(0));
            mxosRequestMap.add(MailboxProperty.newBlockedSender.name(),
                    HelperFunctions.getRandomUser(properties));
            time = Stats.startTimer();
            try {
                IBlockedSendersListService testService = (IBlockedSendersListService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated BlockedSendersList for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("BlockedSendersList is not set for user " + emailId);
        }
    }

    /**
     * Method for delete operation for BlockedSendersListService.
     */

    private void deleteBlockedSendersList() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.BlockedSendersListService;
        List<String> blockedSendersList = readBlockedSendersList();
        if (blockedSendersList != null && blockedSendersList.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.blockedSender.name(),
                    blockedSendersList.get(0));
            time = Stats.startTimer();
            try {
                IBlockedSendersListService testService = (IBlockedSendersListService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted address "
                        + blockedSendersList.get(0)
                        + " from BlockedSendersList for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("BlockedSendersList is not set for user " + emailId);
        }
    }
}
