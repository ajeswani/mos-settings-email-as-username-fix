package com.opwv.leapfrog.services;

import java.util.List;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on EmailAliasService.
 * 
 * @author mxos-dev
 */
public class EmailAliasServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(EmailAliasServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public EmailAliasServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for EmailAliasService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }
            if (properties.isCreateEnabled()) {
                createEmailAlias();
            }

            if (properties.isUpdateEnabled()) {
                updateEmailAlias();
            }

            if (properties.isDeleteEnabled()) {
                deletEmailAlias();
            }

            if (properties.isReadEnabled()) {
                readEmailAlias();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for EmailAliasService.
     */
    private void createEmailAlias() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.MailAliasService;
        final String emailAlias = Long.toString(System.currentTimeMillis())
                + "@" + properties.getDefaultDomain();
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.emailAlias.name(), emailAlias);
        time = Stats.startTimer();
        try {
            IEmailAliasService testService = (IEmailAliasService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created alias " + emailAlias
                    + " for user " + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for EmailAliasService.
     */
    private List<String> readEmailAlias() {
        List<String> listOfAlias = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MailAliasService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IEmailAliasService testService = (IEmailAliasService) mxoscontext
                    .getService(serviceName.name());
            listOfAlias = testService.read(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            if (properties.isDebug()) {
                for (final String emailAlias : listOfAlias) {
                    logger.debug("Email Alias for user " + emailId + " "
                            + emailAlias);
                }
            }
            logger.info("Successfully aliases for user " + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return listOfAlias;
    }

    /**
     * Method for update operation for EmailAliasService.
     */

    private void updateEmailAlias() {

        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MailAliasService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        List<String> listOfAlias = readEmailAlias();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        if (listOfAlias != null && listOfAlias.size() > 0) {
            final String oldEmailAlias = listOfAlias.get(0);
            final String newEmailAlias = Long.toString(System
                    .currentTimeMillis()) + "@" + properties.getDefaultDomain();
            mxosRequestMap.add(MailboxProperty.oldEmailAlias.name(),
                    oldEmailAlias);
            mxosRequestMap.add(MailboxProperty.newEmailAlias.name(),
                    newEmailAlias);
            time = Stats.startTimer();
            try {
                IEmailAliasService testService = (IEmailAliasService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated email alias for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("No aliases to update for user " + emailId);
        }
    }

    /**
     * Method for delete operation for EmailAliasService.
     */

    private void deletEmailAlias() {

        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.MailAliasService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        List<String> listOfAlias = readEmailAlias();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        if (listOfAlias != null && listOfAlias.size() > 0) {
            final String emailAlias = listOfAlias.get(0);
            mxosRequestMap.add(serviceName.name(), emailAlias);
            time = Stats.startTimer();
            try {
                IEmailAliasService testService = (IEmailAliasService) mxoscontext
                        .getService(ServiceEnum.MailAliasService.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted email alias " + emailAlias
                        + " for user " + emailId);

            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("No aliases to delete for user " + emailId);
        }
    }
}
