package com.opwv.leapfrog.services;

import java.util.List;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworkSiteService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on SocialNetworkSiteService.
 * 
 * @author mxos-dev
 */
public class SocialNetworkSiteServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(SocialNetworkSiteServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public SocialNetworkSiteServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * SocialNetworkSiteService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isCreateEnabled()) {
                createSocialNetworkSite();
            }

            if (properties.isReadEnabled()) {
                readSocialNetworkSite();
            }

            if (properties.isUpdateEnabled()) {
                updateSocialNetworkSite();
            }

            if (properties.isDeleteEnabled()) {
                deleteSocialNetworkSite();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for SocialNetworkSiteService.
     */
    private void createSocialNetworkSite() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.SocialNetworkSiteService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.socialNetworkSite.name(),
                HelperFunctions.getSocialNetworkSite());
        mxosRequestMap.add(
                MailboxProperty.socialNetworkSiteAccessEnabled.name(),
                HelperFunctions.generateRandomBoolean());
        time = Stats.startTimer();
        try {
            ISocialNetworkSiteService testService = (ISocialNetworkSiteService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            logger.info("Successfully created Social Network Site for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for SocialNetworkSiteService.
     */
    private List<SocialNetworkSite> readSocialNetworkSite() {

        List<SocialNetworkSite> socialNetworkSites = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.SocialNetworkSiteService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            ISocialNetworkSiteService testService = (ISocialNetworkSiteService) mxoscontext
                    .getService(serviceName.name());
            socialNetworkSites = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            if (properties.isDebug()) {
                if (socialNetworkSites != null && socialNetworkSites.size() > 0) {
                    for (SocialNetworkSite networkSite : socialNetworkSites) {
                        logger.debug("For user " + emailId
                                + " SocialNetworkSite is "
                                + networkSite.toString());
                    }
                } else {
                    logger.debug("For user " + emailId
                            + " SocialNetworkSites are not set.");
                }
            }
            logger.info("Successfully read SocialNetworkSites for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return socialNetworkSites;
    }

    /**
     * Method for update operation for SocialNetworkSiteService.
     */

    private void updateSocialNetworkSite() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.SocialNetworkSiteService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(
                MailboxProperty.socialNetworkIntegrationAllowed.name(),
                HelperFunctions.getConfirmation());
        mxosRequestMap.add(
                MailboxProperty.socialNetworkSiteAccessEnabled.name(),
                HelperFunctions.getConfirmation());
        mxosRequestMap.add(MailboxProperty.socialNetworkSite.name(),
                HelperFunctions.getSocialNetworkSite());
        time = Stats.startTimer();
        try {
            ISocialNetworkSiteService testService = (ISocialNetworkSiteService) mxoscontext
                    .getService(serviceName.name());
            testService.update(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully updated SocialNetworkSite for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for delete operation for SocialNetworkSiteService.
     */

    private void deleteSocialNetworkSite() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.SocialNetworkSiteService;
        List<SocialNetworkSite> socialNetworkSites = readSocialNetworkSite();
        if (socialNetworkSites != null && socialNetworkSites.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.socialNetworkSite.name(),
                    socialNetworkSites.get(0).getSocialNetworkSite());
            time = Stats.startTimer();
            try {
                ISocialNetworkSiteService testService = (ISocialNetworkSiteService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted "
                        + socialNetworkSites.get(0).getSocialNetworkSite()
                        + " from SocialNetworkSite for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("SocialNetworkSites are not set for user " + emailId);
        }
    }
}
