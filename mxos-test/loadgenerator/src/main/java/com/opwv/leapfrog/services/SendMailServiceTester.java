package com.opwv.leapfrog.services;

import org.apache.log4j.Logger;
import com.opwv.leapfrog.test.DefaultParams;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

public class SendMailServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(SendMailServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long time;
    private int threadId;
    private Operation currentOpr;
    private ServiceEnum serviceName;
    private int messagefileNum;

    public SendMailServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        messagefileNum = 0;
    }

    @Override
    public void run() {
        while (myFlag) {
            if (properties.isProcessEnabled()) {
                if (messagefileNum == properties.getMessageFiles().size()) {
                    messagefileNum = 0;
                }
                sendMail();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    private void sendMail() {
        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.SendMailService;
        final String fromAddr = HelperFunctions.getRandomUser(properties);
        final String toAddr = HelperFunctions.getRandomUser(properties);
        final String messageToSend = properties.getMessageFiles().get(
                messagefileNum);
        messagefileNum++;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(SmsProperty.fromAddress.name(), fromAddr);
        mxosRequestMap.add(SmsProperty.toAddress.name(), toAddr);
        mxosRequestMap.add(SmsProperty.message.name(), messageToSend);
        time = Stats.startTimer();
        try {
            ISendMailService sednMailService = (ISendMailService) mxoscontext
                    .getService(serviceName.name());
            sednMailService.process(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully sent message from " + fromAddr + " to "
                    + toAddr);
        } catch (Exception e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage(), e);
        }
    }
}
