package com.opwv.leapfrog.services;

import java.util.List;
import java.util.Random;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.LoadGeneratorConstants;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * * Class for doing CRUD operations on MailboxBaseService. * * @author mxos-dev
 * */
public class MailboxBaseServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(MailboxBaseServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * * Constructor. * * @param tId Thread id * @param properties
     * TestProperties used by LoadGenerator * @param mxoscontext IMxOSContext
     * class object *
     * */
    public MailboxBaseServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * * Thread run method for running CRUD operations for MailboxBaseService.
     * */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }
            if (properties.isReadEnabled()) {
                readMailbox();
            }

            if (properties.isUpdateEnabled()) {
                updateMailbox();
            }

            if (properties.isSearchEnabled()) {
                searchMailbox();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * * Method for read operation for MailboxBaseService.
     * */
    private void readMailbox() {

        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MailboxBaseService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IMailboxBaseService testService = (IMailboxBaseService) mxoscontext
                    .getService(serviceName.name());
            Base mailboxBase = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully read mailbox for user " + emailId);
            if (properties.isDebug()) {
                logger.debug("For User " + emailId
                        + " mailbox Base properties are "
                        + mailboxBase.toString());
            }

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * * Method for search operation for MailboxBaseService.
     * */
    private void searchMailbox() {
        currentOpr = Operation.GETALL;
        serviceName = ServiceEnum.MailboxBaseService;
        List<Base> searchResults = null;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        final String searchQuery = "mail=" + emailId;
        mxosRequestMap.add(MailboxProperty.query.name(), searchQuery);
        time = Stats.startTimer();
        try {
            IMailboxBaseService testService = (IMailboxBaseService) mxoscontext
                    .getService(serviceName.name());
            searchResults = testService.search(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            if (properties.isDebug()) {
                if (searchResults != null && searchResults.size() > 0) {
                    for (Base searchResult : searchResults) {
                        logger.debug("MailboxBase search results for user "
                                + emailId + " " + searchResult.toString());
                    }
                } else {
                    logger.warn("Mailbox for user " + emailId
                            + " does not exists");
                }
            }
            logger.info("Successfully searched mailbox for user " + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * * Method for update operation for MailboxBaseService.
     * */

    private void updateMailbox() {

        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MailboxBaseService;
        String customFields = "";
        Random rTmp = new Random();
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.maxNumAllowedDomains.name(),
                rTmp.nextInt(50) + 1);
        for (int i = 0; i < LoadGeneratorConstants.CUSTOMFIELDS.length; i++) {
            customFields += LoadGeneratorConstants.CUSTOMFIELDS[i]
                    + Integer.toString((rTmp.nextInt(50)) % 2);
        }
        mxosRequestMap.add(MailboxProperty.customFields.name(), customFields);
        mxosRequestMap.add(MailboxProperty.status.name(),
                HelperFunctions.generateRandomStatusForMailbox());
        time = Stats.startTimer();
        try {
            IMailboxBaseService testService = (IMailboxBaseService) mxoscontext
                    .getService(serviceName.name());
            testService.update(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully updated mailbox for user " + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }
}
