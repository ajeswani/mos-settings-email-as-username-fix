/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/DefaultParams.java#1 $
 */

/**
 * Default Parameters for Load generator
 *
 * @author mxos-dev
 */

package com.opwv.leapfrog.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 * Default parameters used by Load Generator.
 * 
 * @author mxos-dev
 * 
 */
public final class DefaultParams {

    private static Document xmlDoc;

    /**
     * Constructor.
     **/

    private DefaultParams() {

    }

    /**
     * Method for initializing the default parameters used by Load Generator.
     * 
     * @param properties TestProperties used by LoadGenerator
     * @throws InvalidDataException ,IOException DocumentException If any
     */

    public static void createDefaultParams(TestProperties properties)
            throws IOException, DocumentException {

        SAXReader reader = new SAXReader();
        xmlDoc = reader.read(LoadGeneratorConstants.XMLFILE);
    }

    /**
     * Method for getting the class which will be used to test the service. The
     * method will use the config/services.xml file for getting the class name.
     * 
     * @return class name which will be used to test the service.
     */
    public static String getClass(String serviceName) {
        final String nodeName = "//services/" + serviceName + "/class";
        Node node = xmlDoc.selectSingleNode(nodeName);
        return node.getStringValue();
    }

    /**
     * Method for getting the list of available services to test. The method
     * will use the config/services.xml file for getting the class name.
     * 
     * @return availableServices list of available services to test.
     */
    public static List<String> getAvailableServices() {
        List<String> availableServices = new ArrayList<String>();
        Element root = xmlDoc.getRootElement();
        for (Iterator<?> i = root.elementIterator(); i.hasNext();) {
            Element element = (Element) i.next();
            availableServices.add(element.getName());
        }
        return availableServices;
    }

    /**
     * Method for getting the list of operations available services to test. The
     * method will use the config/services.xml file for getting the class name.
     * 
     * @return allowdOperations list of available operations for the specified
     *         services to test.
     */
    public static String[] getOperationsAllowdInService(String serviceName) {
        final String nodeName = "//services/" + serviceName + "/operations";
        Node node = xmlDoc.selectSingleNode(nodeName);
        final String[] allowdOperations = node.getStringValue().split(
                LoadGeneratorConstants.DEFAULTSEPERATOR);
        return allowdOperations;
    }

    public static String getServiceDescription(String serviceName) {
        final String nodeName = "//services/" + serviceName + "/description";
        Node node = xmlDoc.selectSingleNode(nodeName);
        final String serviceDescription = node.getStringValue();
        return serviceDescription;
    }

    /**
     * Method for getting the list of mandatory and optional parameters required
     * for the services to test. The method will use the config/services.xml
     * file for getting the class name.
     * 
     * @return requiredParams Map contating list of mandatory and optional
     *         parameters.
     */
    public static Map<String, String> getRequiredParamsForService(
            String serviceName) {
        Map<String, String> requiredParams = new HashMap<String, String>();
        final String optionalParams = "//services/" + serviceName
                + "/optionalparams";
        final String mandatoryParams = "//services/" + serviceName
                + "/mandatoryparams";
        Node node = xmlDoc.selectSingleNode(optionalParams);
        requiredParams.put(LoadGeneratorConstants.OPTIONALPROPERTY,
                node.getStringValue());
        node = xmlDoc.selectSingleNode(mandatoryParams);
        requiredParams.put(LoadGeneratorConstants.MANDATORYPROPERTY,
                node.getStringValue());
        return requiredParams;
    }

    /**
     * Method for reading the config/services.xml file. Which will later be used
     * by diffrent methods for processing the inputs provided by the user.
     * 
     * @throws DocumentException if any.
     */
    public static void readXML() throws DocumentException {

        SAXReader reader = new SAXReader();
        xmlDoc = reader.read(LoadGeneratorConstants.XMLFILE);
    }
}
