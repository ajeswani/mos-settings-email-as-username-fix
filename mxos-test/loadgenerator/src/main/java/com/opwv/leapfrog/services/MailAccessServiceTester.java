package com.opwv.leapfrog.services;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on MailAccessService.
 * 
 * @author mxos-dev
 */
public class MailAccessServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(MailAccessServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public MailAccessServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * MailSendBMIFiltersService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isUpdateEnabled()) {
                updateMailAccess();
            }

            if (properties.isReadEnabled()) {
                readMailAccess();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for read operation for MailAccessService.
     */
    private MailAccess readMailAccess() {

        MailAccess mailAccess = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MailAccessService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IMailAccessService testService = (IMailAccessService) mxoscontext
                    .getService(serviceName.name());
            mailAccess = testService.read(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            if (properties.isDebug()) {
                logger.debug("For user " + emailId + " MailAccess are "
                        + mailAccess.toString());
            }

            logger.info("Successfully read MailAccess for user " + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return mailAccess;
    }

    /**
     * Method for update operation for MailAccessService.
     */

    private void updateMailAccess() {

        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MailAccessService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.popAccessType.name(),
                HelperFunctions.getAccessType());
        mxosRequestMap.add(MailboxProperty.imapAccessType.name(),
                HelperFunctions.getAccessType());
        mxosRequestMap.add(MailboxProperty.popSSLAccessType.name(),
                HelperFunctions.getAccessType());
        mxosRequestMap.add(MailboxProperty.imapSSLAccessType.name(),
                HelperFunctions.getAccessType());
        mxosRequestMap.add(MailboxProperty.smtpAccessEnabled.name(),
                HelperFunctions.getConfirmation());
        mxosRequestMap.add(MailboxProperty.smtpSSLAccessEnabled.name(),
                HelperFunctions.getConfirmation());
        mxosRequestMap.add(MailboxProperty.smtpAuthenticationEnabled.name(),
                HelperFunctions.getConfirmation());
        mxosRequestMap.add(MailboxProperty.webmailAccessType.name(),
                HelperFunctions.getAccessType());
        mxosRequestMap.add(MailboxProperty.webmailSSLAccessType.name(),
                HelperFunctions.getAccessType());
        time = Stats.startTimer();
        try {
            IMailAccessService testService = (IMailAccessService) mxoscontext
                    .getService(serviceName.name());
            testService.update(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully updated MailAccess for user " + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }
}
