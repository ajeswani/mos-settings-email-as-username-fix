package com.opwv.leapfrog.services;

import org.apache.log4j.Logger;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendSMSService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

public class SendSMSServiceTester implements Runnable {

    private static Logger logger = Logger.getLogger(SendSMSServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long time;
    private int threadId;
    private Operation currentOpr;
    private ServiceEnum serviceName;

    public SendSMSServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
    }

    @Override
    public void run() {
        while (myFlag) {
            if (properties.isProcessEnabled()) {
                sendSMS();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    private void sendSMS() {
        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.SendSmsService;
        final String fromAddr = HelperFunctions.getPhoneNumber();
        final String toAddr = HelperFunctions.getPhoneNumber();
        final String messageToSend = "This is test message for " + toAddr
                + " from " + fromAddr;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(SmsProperty.smsType.name(),
                HelperFunctions.getSmsType());
        mxosRequestMap.add(SmsProperty.fromAddress.name(), fromAddr);
        mxosRequestMap.add(SmsProperty.toAddress.name(), toAddr);
        mxosRequestMap.add(SmsProperty.message.name(), messageToSend);
        time = Stats.startTimer();
        try {
            ISendSMSService sednSmsService = (ISendSMSService) mxoscontext
                    .getService(serviceName.name());
            sednSmsService.process(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
        } catch (Exception e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage(), e);
        }
    }
}
