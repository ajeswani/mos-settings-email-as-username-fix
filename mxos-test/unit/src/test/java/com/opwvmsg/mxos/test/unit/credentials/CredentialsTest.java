package com.opwvmsg.mxos.test.unit.credentials;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class CredentialsTest {
    private static ICredentialService credentials;
    private static IMailboxService mailboxService;
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PWD_KEY = MailboxProperty.password.name();
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String HOST_KEY = MailboxProperty.messageStoreHost.name();

    private static final String EMAIL = "test1@owmessaging.com";
    private static final String EMAIL1 = "test2@owmessaging.com";
    private static final String EMAIL2 = "test3@owmessaging.com";
    private static final String PWD = "test";
    private static final String COS_ID = "default";
    private static final String PRE_ENCYRPT_PWD_KEY = MailboxProperty.preEncryptedPasswordAllowed.name();
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CredentialsTest....setUpBeforeClass...");
          credentials = (ICredentialService) ContextUtils.loadContext()
                .getService(ServiceEnum.CredentialService.name());
          mailboxService = (IMailboxService) ContextUtils.loadContext()
          .getService(ServiceEnum.MailboxService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        assertNotNull("CredentialsTest object is null.", credentials);
        CosHelper.createCos(COS_ID);
        MailboxHelper.createMailbox(EMAIL, PWD, COS_ID );
        MailboxHelper.createMailbox(EMAIL1, PWD, COS_ID );
        createMailboxOnlyLdap();
        /*long id = MailboxHelper.createMailbox(EMAIL, PWD, COS_ID );
        assertTrue("Mailbox was not created.", id != -1);*/
   
    }

    /**
     * @throws java.lang.Exception
     */

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CredentialsTest...tearDownAfterClass");
        inputParams.clear();
        CosHelper.deleteCos(COS_ID);
        MailboxHelper.deleteMailbox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL1);
        MailboxHelper.deleteMailbox(EMAIL2);
        credentials= null;
    }
    
    public static void createMailboxOnlyLdap() {
        System.out.println("testCreateMailbox...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COS_ID);
        inputParams.put(HOST_KEY, new ArrayList<String>());
        inputParams.get(HOST_KEY).add("dummy");
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            System.out.println("Exception message: "  +e.fillInStackTrace());

        }
    }

    @Test
    public void testGetCrdentials() {
        System.out.println("testGetCrdentials....");

        try {
            List<String> email = new ArrayList<String>();
            email.add(EMAIL);
            inputParams.clear();
            inputParams.put("email", email);
            Credentials result = credentials.read(inputParams);
            assertNotNull(result);
            System.out.println(result);
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testUpdatePassword() {
       
    	System.out.println("testUpdatePassword....");
    	
    	inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String password = "Yahoo";
        p.add(password);
        inputParams.put(MailboxProperty.password.name(), p);
        p = new ArrayList<String>();
        String passtype = "clear";
        p.add(passtype);
        inputParams.put(MailboxProperty.passwordStoreType.name(), p);
      
        try {
            credentials.update(inputParams);
            
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           
            Credentials creds = credentials.read(inputParams);
            System.out.println("testUpdatePassword: "+creds.getPassword());
    
            assertEquals("Password:  ", password, creds.getPassword());
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }
    @Test
    public void testUpdateBcryptedPassword() {

        System.out.println("testUpdateBcryptedPassword....");

        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL1);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String password = "$2a$10$Ro0CUfOqk6cXEKf3dyaM7Ox3VKMOUI7atskEN0jUadLjfwdMfJU7S";
        p.add(password);
        inputParams.put(MailboxProperty.password.name(), p);
        p = new ArrayList<String>();
        String passtype = "bcrypt";
        p.add(passtype);
        inputParams.put(MailboxProperty.passwordStoreType.name(), p);
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add("true");
        try {
            credentials.update(inputParams);

            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL1);
            inputParams.put(MailboxProperty.email.name(), p1);

            Credentials creds = credentials.read(inputParams);
            System.out.println("testUpdatePassword: " + creds.getPassword());

            assertEquals("Password:  ", password, creds.getPassword());
        } catch (Exception e) {
            // e.printStackTrace();
            fail();
        }
    }
    @Test
    public void testUpdateBcryptedPasswordNeg() {

        System.out.println("testUpdateBcryptedPasswordNeg....");

        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL1);
        inputParams.put(MailboxProperty.email.name(), p);

        p = new ArrayList<String>();
        String password = "$2a$10$Ro0CUfOqk6cXEKf3dyaM7Ox3VKMOUI7atskEN0jUadLjfwdMfJU7S";
        p.add(password);
        inputParams.put(MailboxProperty.password.name(), p);
        p = new ArrayList<String>();
        String passtype = "bcrypt";
        p.add(passtype);
        inputParams.put(MailboxProperty.passwordStoreType.name(), p);
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add("false");

        try {
            credentials.update(inputParams);
            fail();
        } catch (MxOSException e) {
            assertEquals("MBX_ENCRYPTION_NOT_SUPPORTED",
                    MailboxError.MBX_BCRYPT_ENCRYPTION_NOT_SUPPORTED.name());
        }
    }
    @Test
    public void testUpdatePasswordNeg() {
    	System.out.println("testUpdatePasswordNeg....");
        
    	String expectedErrorCode = MailboxError.MBX_INVALID_PASSWORD.name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add(null);
        inputParams.put(MailboxProperty.password.name(), p);
        try {
            credentials.update(inputParams);
            Credentials creds = credentials.read(inputParams);
            System.out.println("testUpdatePasswordNeg: "+creds.getPassword());
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testUpdatePasswordStoreType() {
    	System.out.println("testUpdatePasswordStoreType....");

    	inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String passtype = "sha1";
        p.add(passtype);
        inputParams.put(MailboxProperty.passwordStoreType.name(), p);
        p = new ArrayList<String>();
        p.add("password");
        inputParams.put(MailboxProperty.password.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);           
            Credentials creds = credentials.read(inputParams);
            
            assertEquals("PasswordStoreType: ", passtype,
                    creds.getPasswordStoreType().toString());
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testUpdatePasswordStoreTypeNeg() {
    	System.out.println("testUpdatePasswordStoreTypeNeg....");

    	String expectedErrorCode = MailboxError
        .MBX_INVALID_PASSWORD_STORETYPE.name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("password");
        inputParams.put(MailboxProperty.password.name(), p);
        p = new ArrayList<String>();
        p.add("JunkValue");
        inputParams.put(MailboxProperty.passwordStoreType.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }
    
    @Test
    public void testPasswordRecoveryAllowed() {
     	System.out.println("testPasswordRecoveryAllowed....");

     	inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String passRecAllowed = "yes";
        p.add(passRecAllowed);
        inputParams.put(MailboxProperty.passwordRecoveryAllowed.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           
            Credentials creds = credentials.read(inputParams);
            
            MxosEnums.BooleanType flag = creds.getPasswordRecoveryAllowed();
            
            System.out.println("testPasswordRecoveryAllowed: "+ flag.name());
            
            assertNotNull("Is null.", flag);
           
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPasswordRecoveryAllowedNeg() {
     	System.out.println("testPasswordRecoveryAllowedNeg....");

     	String expectedErrorCode = MailboxError.MBX_INVALID_PASSWORD_RECOVERY_ALLOWED
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("unknown");
        inputParams.put(MailboxProperty.passwordRecoveryAllowed.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testPasswordRecoveryPreference() {
    	System.out.println("testPasswordRecoveryPreference....");
    	inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String passRecPref = "ALL";
        p.add(passRecPref);
        inputParams.put(MailboxProperty.passwordRecoveryPreference.name(), p);
        try {
            credentials.update(inputParams);
           
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           
            Credentials creds = credentials.read(inputParams);
            MxosEnums.PasswordRecoveryPreference pref = creds.getPasswordRecoveryPreference();
            
            System.out.println("testPassRecoveryMsisdn: "+ pref.name());
            
            assertNotNull("Is null.", pref);
     
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPasswordRecoveryPreferenceNeg() {
    	System.out.println("testPasswordRecoveryPreferenceNeg....");

    	String expectedErrorCode = MailboxError.MBX_INVALID_PASSWORD_RECOVERY_PREFERENCE
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("I_dont_know");
        inputParams.put(MailboxProperty.passwordRecoveryPreference.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testPassRecoveryMsisdn() {
    	System.out.println("testPassRecoveryMsisdn....");

    	inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String passRecMSISDN = "+919999999999";
        p.add(passRecMSISDN);
        inputParams.put(MailboxProperty.passwordRecoveryMsisdn.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           
            Credentials creds = credentials.read(inputParams);
            String msisdn = creds.getPasswordRecoveryMsisdn();
            
            System.out.println("testPassRecoveryMsisdn: "+creds.getPasswordRecoveryMsisdn());
            
            assertNotNull("Is null.", msisdn);
            assertTrue("Has a wrong value.",
            		msisdn.equals("+919999999999"));
         
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPassRecoveryMsisdnNeg() {
       	System.out.println("testPassRecoveryMsisdnNeg....");

       	String expectedErrorCode = MailboxError.MBX_INVALID_PASSWORD_RECOVERY_MSISDN
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("94837588957698743859795387344545");
        inputParams.put(MailboxProperty.passwordRecoveryMsisdn.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            System.out.println("testPassRecoveryMsisdnNeg: "+me.getCode());
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testPassRecoveryMsisdnStatus() {
     	System.out.println("testPassRecoveryMsisdnStatus....");

     	inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String passRecMsisdnStatus = "activated";
        p.add(passRecMsisdnStatus);
        inputParams.put(MailboxProperty.passwordRecoveryMsisdnStatus.name(), p);
        try {
            credentials.update(inputParams);
        
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           Credentials creds = credentials.read(inputParams);
           String status = creds.getPasswordRecoveryMsisdnStatus().name();
           
           System.out.println("testPassRecoveryMsisdnStatus: "+ status);
           
           assertNotNull("Is null.", status);
         
       
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPassRecoveryMsisdnStatusNeg() {
    	System.out.println("testPassRecoveryMsisdnStatusNeg....");

    	String expectedErrorCode = MailboxError.MBX_INVALID_PASSWORD_RECOVERY_MSISDN_STATUS
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("Active_Deactive");
        inputParams.put(MailboxProperty.passwordRecoveryMsisdnStatus.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            System.out.println("testPassRecoveryMsisdnStatusNeg: "+me.getCode());
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testLastPassRecChangeStatusDate() {
    	System.out.println("testLastPassRecChangeStatusDate....");

    	inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String date = "2012-08-23T23:50:45Z";
        p.add(date);
        inputParams.put(
                MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate
                        .name(), p);
        try {
            credentials.update(inputParams);
            
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           
            Credentials creds = credentials.read(inputParams);
            System.out.println("Expected Date: "+date +"Actual Date: "
                    +creds.getLastPasswordRecoveryMsisdnStatusChangeDate());
            assertEquals("lastPasswordRecoveryMsisdnStatusChangeDate: ", date,
                    creds.getLastPasswordRecoveryMsisdnStatusChangeDate());
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testLastPassRecChangeStatusDateNeg() {
    	System.out.println("testLastPassRecChangeStatusDateNeg....");
 
    	String expectedErrorCode = MailboxError.MBX_INVALID_PASS_REC_MSISDN_STATUS_CHANGE_DATE
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("what is date");
        inputParams.put(
                MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate
                        .name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testPassRecEmail() {
    	System.out.println("testPassRecEmail....");
    	   inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String passRecEmail = "shoaib@aricent.com";
        p.add(passRecEmail);
        inputParams.put(MailboxProperty.passwordRecoveryEmail.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           Credentials creds = credentials.read(inputParams);
            assertEquals("passwordRecoveryEmail: ", passRecEmail,
                    creds.getPasswordRecoveryEmail());
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPassRecEmailNeg() {
    	System.out.println("testPassRecEmailNeg....");
    	
    	String expectedErrorCode = MailboxError.MBX_INVALID_PASSWORD_RECOVERY_EMAIL
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("hell**fire@gimme_more");
        inputParams.put(MailboxProperty.passwordRecoveryEmail.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testPassRecEmailStatus() {
    	System.out.println("testPassRecEmailStatus....");
           inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String passRecEmailStatus = "activated";
        p.add(passRecEmailStatus);
        inputParams.put(MailboxProperty.passwordRecoveryEmailStatus.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(),p1);
           Credentials creds = credentials.read(inputParams);
         
           String status = creds.getPasswordRecoveryEmailStatus().name();
           
           System.out.println("testPassRecEmailStatus: "+status);
           
           assertNotNull("Is null.", status);
               
        } catch (Exception e) {
            //e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPassRecEmailStatusNeg() {
    	System.out.println("testPassRecEmailStatusNeg....");
        
    	String expectedErrorCode = MailboxError.MBX_INVALID_PASSWORD_RECOVERY_EMAIL_STATUS
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("active-all");
        inputParams.put(MailboxProperty.passwordRecoveryEmailStatus.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            System.out.println("testPassRecEmailStatusNeg: "+me.getCode());
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }
    
    @Test
    public void testLastLoginAttemptDateNeg() {
        System.out.println("testLastLoginAttemptDateNeg....");

        String expectedErrorCode = MailboxError.MBX_INVALID_LAST_LOGIN_ATTEMPTS_DATE
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("active-all");
        inputParams.put(MailboxProperty.lastLoginAttemptDate.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            System.out.println("testLastLoginAttemptDateNeg: " + me.getCode());
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testLastLoginAttemptDate() {
        System.out.println("testLastLoginAttemptDate....");

        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String date = "2012-08-23T23:50:45Z";
        p.add(date);
        inputParams.put(MailboxProperty.lastLoginAttemptDate.name(), p);
        try {
            credentials.update(inputParams);

            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);

            Credentials creds = credentials.read(inputParams);
            System.out.println("Expected Date: " + date + "Actual Date: "
                    + creds.getLastLoginAttemptDate());
            assertEquals("testLastLoginAttemptDate: ", date,
                    creds.getLastLoginAttemptDate());
        } catch (Exception e) {
            // e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testLastLoginSuccessDateUpdate() {
        System.out.println("testLastLoginSuccessDateUpdate....");
        
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("2013-02-28T07:32:33Z");
        inputParams.put(MailboxProperty.lastSuccessfulLoginDate.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            System.out.println("testLastLoginSuccessDateUpdate: " + me.getCode());
        } catch (Exception e) {
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }
    
    @Test
    public void testLastLoginSuccessDateUpdateRead() {
        System.out.println("testLastLoginSuccessDateRead....");

        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String date = "2013-02-26T07:32:33Z";
        p.add(date);
        inputParams.put(MailboxProperty.lastSuccessfulLoginDate.name(), p);
        try {
            credentials.update(inputParams);

            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);

            Credentials creds = credentials.read(inputParams);
            System.out.println("Expected Date: " + date + "Actual Date: "
                    + creds.getLastLoginAttemptDate());
            assertEquals("lastSuccessfulLoginDate: ", date,
                    creds.getLastLoginAttemptDate());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMaxFailedLoginAttemptsNeg() {
        System.out.println("testMaxFailedLoginAttemptsNeg....");
        String expectedErrorCode = MailboxError.MBX_INVALID_MAX_FAILED_LOGIN_ATTEMPTS
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("hell**fire@gimme_more");
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testMaxFailedLoginAttempts() {
        System.out.println("testMaxFailedLoginAttempts....");
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFLAttempts = "6";
        p.add(maxFLAttempts);
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            Credentials creds = credentials.read(inputParams);

            Integer maxFailedLoginAttempts = creds.getMaxFailedLoginAttempts();

            System.out.println("testMaxFailedLoginAttempts: "
                    + maxFailedLoginAttempts);
            assertNotNull("Is null.", maxFailedLoginAttempts);

        } catch (Exception e) {
            // e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testLastFailedLoginDateNeg() {
        System.out.println("testFailedLoginAttemptsNeg....");

        String expectedErrorCode = MailboxError.MBX_INVALID_FAILED_LOGIN_ATTEMPTS
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("hell**fire@gimme_more");
        inputParams.put(MailboxProperty.lastFailedLoginDate.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testLastFailedLoginDate() {
        System.out.println("testLastFailedLoginTime....");
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String lastFailedLoginDate = "2013-02-26T07:32:33Z";
        p.add(lastFailedLoginDate);
        inputParams.put(MailboxProperty.lastFailedLoginDate.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            Credentials creds = credentials.read(inputParams);
            assertEquals("lastFailedLoginDate: ", lastFailedLoginDate,
                    creds.getLastFailedLoginDate());       
        } catch (Exception e) {
            fail();
        }
    }
    
    @Test
    public void testFailedLoginAttemptsNeg() {
        System.out.println("testFailedLoginAttemptsNeg....");

        String expectedErrorCode = MailboxError.MBX_INVALID_FAILED_LOGIN_ATTEMPTS
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("hell**fire@gimme_more");
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testUpdateFailedLoginAttempts() {
        System.out.println("testFailedLoginAttempts....");
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "6";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            Credentials creds = credentials.read(inputParams);

            Integer fLoginAttempts = creds.getMaxFailedLoginAttempts();
            System.out.println("testFailedLoginAttempts: " + fLoginAttempts);

            assertNotNull("Is null.", fLoginAttempts);

        } catch (Exception e) {
            // e.printStackTrace();
            fail();
        }
    }


    @Test
    public void testMaxFailedCaptchaLoginAttemptsNeg() {
        System.out.println("testMaxFailedCaptchaLoginAttemptsNeg....");
        String expectedErrorCode = MailboxError.MBX_INVALID_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("hell**fire@gimme_more");
        inputParams.put(MailboxProperty.maxFailedCaptchaLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testMaxFailedCaptchaLoginAttempts() {
        System.out.println("testMaxFailedCaptchaLoginAttempts....");
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFLAttempts = "6";
        p.add(maxFLAttempts);
        inputParams.put(MailboxProperty.maxFailedCaptchaLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            Credentials creds = credentials.read(inputParams);

            Integer maxFailedCaptchaLoginAttempts = creds.getMaxFailedCaptchaLoginAttempts();

            System.out.println("testMaxFailedCaptchaLoginAttempts: "
                    + maxFailedCaptchaLoginAttempts);
            assertNotNull("Is null.", maxFailedCaptchaLoginAttempts);

        } catch (Exception e) {
            // e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testFailedCaptchaLoginAttemptsNeg() {
        System.out.println("testFailedCaptchaLoginAttemptsNeg....");

        String expectedErrorCode = MailboxError.MBX_INVALID_FAILED_CAPTCHA_LOGIN_ATTEMPTS
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("hell**fire@gimme_more");
        inputParams.put(MailboxProperty.failedCaptchaLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testUpdateFailedCaptchaLoginAttempts() {
        System.out.println("testUpdateFailedCaptchaLoginAttempts....");
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "6";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedCaptchaLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            Credentials creds = credentials.read(inputParams);

            Integer fCLoginAttempts = creds.getMaxFailedCaptchaLoginAttempts();
            System.out.println("testFailedLoginAttempts: " + fCLoginAttempts);

            assertNotNull("Is null.", fCLoginAttempts);

        } catch (Exception e) {
            // e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testAuthenticaionSuccessWithoutMailboxonMSS() {
        try {
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL2);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "test";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

        } catch (Exception e) {
            fail();
        }
    }
    
    @Test
    public void testAuthenticaionSuccess() {
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFailedLoginAttempts = "4";
        p.add(maxFailedLoginAttempts);
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "0";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String lastFailedLoginDate = "2013-02-26T07:32:33Z";
        p.add(lastFailedLoginDate);
        inputParams.put(MailboxProperty.lastFailedLoginDate.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "test";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = credentials.read(inputParams);
            System.out.println("currentLDAPDatetESTSU: " + creds.getLastLoginAttemptDate());
            long fLAttempts = creds.getFailedLoginAttempts();
         //   long lflt = creds.getLastFailedLoginTime();
            assertEquals("failedLoginAttempts :  ", fLAttempts, 0);
          //  assertEquals("lastFailedLogintime :  ", lflt, lastFailedLogintime);

        } catch (Exception e) {
            fail();
        }
    }
    
    @Test
    public void testAuthenticaionWithoutPasswordValidation() {
        String expectedErrorCode = MailboxError.MBX_AUTHENTICATION_FAILED
                .name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        String password = "Yahoo";
        p.add(password);
        inputParams.put(MailboxProperty.password.name(), p);
        p = new ArrayList<String>();
        String passtype = "clear";
        p.add(passtype);
        inputParams.put(MailboxProperty.passwordStoreType.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            password = "*test"; // validation error will be thrown if password is validated
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        }
    }


    @Test
    public void testUpdateLDAPOnAuthSuccess() {
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFailedLoginAttempts = "4";
        p.add(maxFailedLoginAttempts);
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "2";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        String updateLdapAfterAuthenticationEnabled = System
        .getProperty(SystemProperty.updateMSSAfterAuthenticationEnabled
                .name());
        boolean isUpdateEnabled = Boolean.valueOf(updateLdapAfterAuthenticationEnabled);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "test";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = credentials.read(inputParams);
            System.out.println("currentLDAPDatetESTSU: " + creds.getLastLoginAttemptDate());
            long fLAttempts = creds.getFailedLoginAttempts();
            if(isUpdateEnabled){ 
            assertEquals("failedLoginAttempts :  ", fLAttempts, 0);
            }
            else{
            assertEquals("failedLoginAttempts :  ", fLAttempts, 2);
            }
        } catch (Exception e) {
            fail();
        }
    } 

    @Test
    public void testAuthenticaionFailure() {
        String expectedErrorCode = MailboxError.MBX_AUTHENTICATION_FAILED.name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFailedLoginAttempts = "4";
        p.add(maxFailedLoginAttempts);
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "0";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);

            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "testing";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = null;
            try {
                creds = credentials.read(inputParams);
            } catch (MxOSException e) {
                e.printStackTrace();
            }
            System.out.println("currentLDAPDatetESTfA: " + creds.getLastLoginAttemptDate());

            long fLAttempts = creds.getFailedLoginAttempts();
            assertEquals("failedLoginAttempts :  ", fLAttempts, 1);

        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }
    
    @Test
    public void testAuthenticaionBadPasswordDelaySuccess() {
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFailedLoginAttempts = "2";
        p.add(maxFailedLoginAttempts);
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "0";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String password = "test";
        p.add(password);
        inputParams.put(MailboxProperty.password.name(), p);
        p = new ArrayList<String>();
        String passtype = "clear";
        p.add(passtype);
        inputParams.put(MailboxProperty.passwordStoreType.name(), p);
        try {
            credentials.update(inputParams);
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            password = "test";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = credentials.read(inputParams);
            System.out.println("currentLDAPDatetESTSU: " + creds.getLastLoginAttemptDate());
            long fLAttempts = creds.getFailedLoginAttempts();
            assertEquals("failedLoginAttempts :  ", fLAttempts, 0);

        } catch (Exception e) {
            fail();
        }
    }
            
    @Test
    public void testAuthenticaionBadPasswordDelayFailure() {
        String expectedErrorCode = MailboxError.MBX_ACCOUNT_ACCESS_DELAYED.name();
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFailedLoginAttempts = "4";
        p.add(maxFailedLoginAttempts);
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "2";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        p.add(null);
        inputParams.put(MailboxProperty.lastLoginAttemptDate.name(), p);
        try {
            credentials.update(inputParams);

            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "testing";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = null;
            try {
                creds = credentials.read(inputParams);
            } catch (MxOSException e) {
                e.printStackTrace();
            }
            System.out.println("currentLDAPDatetESTfA: " + creds.getLastLoginAttemptDate());

            long fLAttempts = creds.getFailedLoginAttempts();
            assertEquals("failedLoginAttempts :  ", fLAttempts, 2);

        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }
    
    @Test
    public void testResetFailedLoginAttemptsOnAuthSuccessOfLockedAccount() {
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String maxFailedLoginAttempts = "2";
        p.add(maxFailedLoginAttempts);
        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
        p = new ArrayList<String>();
        String failedLoginAttempts = "5";
        p.add(failedLoginAttempts);
        inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
        try {
            credentials.update(inputParams);

            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "test";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);
            
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = credentials.read(inputParams);
            System.out.println("currentLDAPDatetESTSU: " + creds.getLastLoginAttemptDate());
            long fLAttempts = creds.getFailedLoginAttempts();
            assertEquals("failedLoginAttempts :  ", fLAttempts, 0);

        } catch (Exception me) {
            fail();
        } 
    }
    
    @Test
    public void testAuthenticaionWithUsernameSuccess() {
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);

        try {
            p = new ArrayList<String>();
            String maxFailedLoginAttempts = "2";
            p.add(maxFailedLoginAttempts);
            inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
            p = new ArrayList<String>();
            String failedLoginAttempts = "0";
            p.add(failedLoginAttempts);
            inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
            credentials.update(inputParams);
            inputParams.clear();

            p = new ArrayList<String>();
            p.add("prateep.thadi3@openwave.com");
            inputParams.put(MailboxProperty.userName.name(), p);
            String password = PWD;
            p = new ArrayList<String>();
            p.add(password);
            inputParams.put(MailboxProperty.password.name(), p);
            p = new ArrayList<String>();
            String passtype = "clear";
            p.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p);
            credentials.authenticate(inputParams);
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = credentials.read(inputParams);

            System.out.println("currentLDAPDatetESTSU: "
                     +creds.getLastLoginAttemptDate());
            long fLAttempts = creds.getFailedLoginAttempts();
            assertEquals("failedLoginAttempts :  ", fLAttempts, 0);

        } catch (Exception e) {
            fail();
        }
    }
  
    @Test
    public void testAuthenticaionWithUsernameFailure() {
        inputParams.clear();
        String expectedErrorCode = MailboxError.MBX_AUTHENTICATION_FAILED.name();
        List<String> p = new ArrayList<String>();
        try {
            p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            String maxFailedLoginAttempts = "4";
            p.add(maxFailedLoginAttempts);
            inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(), p);
            p = new ArrayList<String>();
            String failedLoginAttempts = "0";
            p.add(failedLoginAttempts);
            inputParams.put(MailboxProperty.failedLoginAttempts.name(), p);
            credentials.update(inputParams);
            inputParams.clear();

            p = new ArrayList<String>();
            p.add("prateep.thadi3@openwave.com");
            inputParams.put(MailboxProperty.userName.name(), p);
            String password = "testing";
            p = new ArrayList<String>();
            p.add(password);
            inputParams.put(MailboxProperty.password.name(), p);
            p = new ArrayList<String>();
            String passtype = "clear";
            p.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p);
            credentials.authenticate(inputParams);

        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            p2 = new ArrayList<String>();
            Credentials creds = null;
            try {
                creds = credentials.read(inputParams);
            } catch (MxOSException e) {
                e.printStackTrace();
            }
            System.out.println("currentLDAPDatetESTfA: "  +creds.getLastLoginAttemptDate());

            long fLAttempts = creds.getFailedLoginAttempts();
            assertEquals("failedLoginAttempts :  ", fLAttempts, 1);

        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: "  +e.fillInStackTrace());
        }
    }
    
    @Test
    public void testUpdateInvalidDate() {
       
        System.out.println("testUpdateInvalidDate....");
        
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        String date = "2013-02-30T17:54:01Z";
        p.add(date);
        inputParams.put(MailboxProperty.lastSuccessfulLoginDate.name(), p);
              
        try {
            credentials.update(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
    
    @Test
    public void testAuthenticaionFailureWithNonExistingAccount() {
        String expectedErrorCode = MailboxError.MBX_NOT_FOUND.name();
        try {
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add("nonexisting@openwave.com");
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "testing";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        }
    }
    
    @Test
    public void testAuthenticaionFailureWithNonExistingUsername() {
        String expectedErrorCode = MailboxError.MBX_NOT_FOUND.name();
        try {
            inputParams.clear();
            List<String> p1 = new ArrayList<String>();
            p1.add("nonexisting");
            inputParams.put(MailboxProperty.email.name(), p1);
            p1 = new ArrayList<String>();
            String password = "testing";
            p1.add(password);
            inputParams.put(MailboxProperty.password.name(), p1);
            p1 = new ArrayList<String>();
            String passtype = "clear";
            p1.add(passtype);
            inputParams.put(MailboxProperty.passwordStoreType.name(), p1);
            credentials.authenticate(inputParams);

        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        }
    }

}
