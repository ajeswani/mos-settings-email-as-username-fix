package com.opwvmsg.mxos.test.unit.cos.mailstore;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to MailStore
@SuiteClasses({ CosMailStoreGET.class, CosMailStorePOST.class,
        CosExternalStorePOST.class })
public class CosMailStoreTestSuite {
}
