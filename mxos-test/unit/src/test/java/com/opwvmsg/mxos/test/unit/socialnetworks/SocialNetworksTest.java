package com.opwvmsg.mxos.test.unit.socialnetworks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworksService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class SocialNetworksTest {
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "sn123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static ISocialNetworksService isns = null;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("SocialNetworksTest.setUpBeforeClass...");
        isns = (ISocialNetworksService) ContextUtils.loadContext().getService(
                ServiceEnum.SocialNetworksService.name());
        assertNotNull(isns);
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("SocialNetworksTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        isns = null;
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("SocialNetworksTest.setUp...");
        assertNotNull("SocialNetworksService object is null.", isns);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("SocialNetworksTest.tearDown...");
    }

    private static SocialNetworks getSocialNetworksParams() {
        SocialNetworks sn = null;
        try {
            sn = isns.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("SocialNetworks object is null.", sn);
        return sn;
    }

    private static void updateSocialNetworksParams(
            Map<String, List<String>> updateParams) {
        updateSocialNetworksParams(updateParams, null);
    }

    private static void updateSocialNetworksParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            isns.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                updateParams.put(EMAIL_KEY, new ArrayList<String>());
                updateParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testUpdateSocialNetworksWithoutEmail() throws Exception {
        updateParams.clear();
        System.out
                .println("SocialNetworksTest.testUpdateSocialNetworksWithoutEmail...");
        updateSocialNetworksParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateSocialNetworksWithEmptyEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add("");
        System.out
                .println("SocialNetworksTest.testUpdateSocialNetworksWithEmptyEmail...");
        updateSocialNetworksParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateSocialNetworksWithNullEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(null);
        System.out
                .println("SocialNetworksTest.testUpdateSocialNetworksWithNullEmail...");
        updateSocialNetworksParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateSocialNetworksWithoutAnyParam() throws Exception {
        System.out
                .println("SocialNetworksTest.testUpdateSocialNetworksWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateSocialNetworksParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testSocialNetworkIntegrationAllowedNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSocialNetworkIntegrationAllowedNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkIntegrationAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateSocialNetworksParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateSocialNetworksParams(updateParams);
        BooleanType pa = getSocialNetworksParams().getSocialNetworkIntegrationAllowed();
        assertNotNull("SocialNetworkIntegrationAllowed is null.", pa);
        assertNotNull("SocialNetworkIntegrationAllowed is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSocialNetworkIntegrationAllowedInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSocialNetworkIntegrationAllowedInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkIntegrationAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateSocialNetworksParams(updateParams,
                MailboxError.MBX_INVALID_SNINTEGRATIONALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSocialNetworkIntegrationAllowedSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSocialNetworkIntegrationAllowedSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkIntegrationAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateSocialNetworksParams(updateParams,
                MailboxError.MBX_INVALID_SNINTEGRATIONALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSocialNetworkIntegrationAllowedEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSocialNetworkIntegrationAllowedEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkIntegrationAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateSocialNetworksParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateSocialNetworksParams(updateParams);
        BooleanType pa = getSocialNetworksParams().getSocialNetworkIntegrationAllowed();
        assertNotNull("SocialNetworkIntegrationAllowed is null.", pa);
        assertNotNull("SocialNetworkIntegrationAllowed is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSocialNetworkIntegrationAllowedSuccess() throws Exception {
        System.out.println("UpdateMailAccessTest.testSocialNetworkIntegrationAllowedSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkIntegrationAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateSocialNetworksParams(updateParams);
        BooleanType pa = getSocialNetworksParams().getSocialNetworkIntegrationAllowed();
        assertNotNull("SocialNetworkIntegrationAllowed is null.", pa);
        assertTrue("SocialNetworkIntegrationAllowed has a wrong value.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

}
