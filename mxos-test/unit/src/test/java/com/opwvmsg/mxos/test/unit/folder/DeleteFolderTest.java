package com.opwvmsg.mxos.test.unit.folder;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class DeleteFolderTest {

    private static final String TEST_NAME = "DeleteFolderTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "folderDelete@openwave.com";
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test5";
    private static final String ISADMIN_KEY = MessageProperty.isAdmin.name();
    private static final String ISADMMIN_TRUE = "true";
    private static final String ISADMMIN_FALSE = "false";

    private static final String FORCE_KEY = FolderProperty.force.name();
    private static final String FORCE_TRUE = "true";
    private static final String FORCE_FALSE = "false";

    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();

    private static final String FOLDERNAME_VALUE_INBOX = "INBOX";
    private static final String FOLDERNAME_VALUE_1 = "1";
    private static final String FOLDERNAME_VALUE_1_2 = "1%2f2";
    private static final String FOLDERNAME_VALUE_3 = "3";
    private static final String FOLDERNAME_VALUE_3_1 = "3%2f1";
    private static final String FOLDERNAME_VALUE_3_2 = "3%2f2";
    private static final String FOLDERNAME_VALUE_4 = "4";
    private static final String FOLDERNAME_VALUE_5 = "5";
    private static final String FOLDERNAME_VALUE_6 = "6";
    private static final String FOLDERNAME_VALUE_7 = "7";
    private static final String FOLDERNAME_VALUE_8 = "8";
    private static final String FOLDERNAME_VALUE_1_22 = "1111111111111111111111111111111111111111111111%2f22222222222222222222222222222222222222222222222" +
    "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222" +
    "2222222222222222222222222222222222222222222";
    private static final String FOLDERNAME_VALUE_11_22 = "1111111111111111111111111111111111111111111111%2f22222222222222222222222222222222222222222222222" +
    "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222" +
    "222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222";
    private static final String FOLDERNAME_VALUE_9 = "fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2ft";
    private static final String FOLDERNAME_VALUE_10 = "a%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2ft";

    private static final String FOLDERNAME_VALUE_test = "test";
    private static final String FOLDERNAME_VALUE_Test = "Test";

    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static IFolderService folderService;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        folderService = (IFolderService) context
                .getService(ServiceEnum.FolderService.name());
        MailboxHelper.createMailbox(EMAIL,PASSWORD,true);
        //createMailBox(EMAIL, PASSWORD);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        mailboxService = null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, EMAIL_KEY, email);
        addParams(inputParams, PASSWORD_KEY, password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Unable to create Mailbox...", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.delete(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            folderService.delete(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNullEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.delete(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
            		ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.delete(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }

    @Test
    public void isAdminInvalid() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSuccess_isAdminInvalid");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_5);
            addParams(inputParams, ISADMIN_KEY, "junk");
            folderService.delete(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_IS_ADMIN.name(), e.getCode());
        }
    }

    @Test
    public void forceInvalid() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSuccess_forceInvalid");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_5);
            addParams(inputParams, FORCE_KEY, "junk");
            folderService.delete(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FORCE.name(), e.getCode());
        }
    }

    @Test
    public void testDeleteSuccess_main() {
        System.out.println(TEST_NAME + ARROW_SEP + "testDeleteSuccess_main");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_1);
            folderService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_1);
            folderService.delete(inputParams2);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testDeleteSuccess_secondLevel() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSuccess_secondLevel");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_6);
            folderService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_6);
            folderService.delete(inputParams2);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testDeleteSuccess_secondLevelFolderName() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_secondLevelFolderName");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_1_22);
            folderService.create(inputParams);
            
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_1_22);
            folderService.delete(inputParams2);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }
    
    @Test
    public void testDeleteFailure_secondLevelFolderName() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateFailure_secondLevelFolderName");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_11_22);
            folderService.create(inputParams);
            
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_11_22);
            folderService.delete(inputParams2);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_FOLDER_NAME.name(), e.getCode());
        }
    }
    
    @Test
    public void testDeleteSuccess_FolderDepth() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_FolderDepth");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_9);
            folderService.create(inputParams);
            
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_9);
            folderService.delete(inputParams2);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }
    
    @Test
    public void testDeleteFailure_FolderDepth() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateFailure_FolderDepth");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_10);
            folderService.create(inputParams);
            
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_10);
            folderService.delete(inputParams2);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_FOLDER_NAME.name(), e.getCode());
        }
    }

    
    @Test
    public void testDeleteSuccess_secondLevel_isAdminTrue() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSuccess_secondLevel_isAdminTrue");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_7);
            folderService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_7);
            addParams(inputParams2, ISADMIN_KEY, ISADMMIN_TRUE);
            folderService.delete(inputParams2);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testDeleteSuccess_secondLevel_isAdminFalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSuccess_secondLevel_isAdminFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_8);
            folderService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_8);
            addParams(inputParams2, ISADMIN_KEY, ISADMMIN_FALSE);
            folderService.delete(inputParams2);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testDelete_secondLevel_forcetrue() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSuccess_secondLevel_forcetrue");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_3_1);
            folderService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_3);
            addParams(inputParams2, ISADMIN_KEY, ISADMMIN_TRUE);
            addParams(inputParams2, FORCE_KEY, FORCE_TRUE);
            folderService.delete(inputParams2);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            e.printStackTrace();
          assertEquals("Some unexpected error code.",
        		  FolderError.FLD_NOT_FOUND.name(),
                  e.getCode());
        }
    }

    @Test
    public void testDelete_secondLevel_forcefalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDelete_secondLevel_forcefalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_3_2);
            folderService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_3);
            addParams(inputParams2, ISADMIN_KEY, ISADMMIN_FALSE);
            addParams(inputParams2, FORCE_KEY, FORCE_FALSE);
            folderService.delete(inputParams2);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            e.printStackTrace();
            assertEquals("Some unexpected error code.",
            		FolderError.FLD_NOT_FOUND.name(),
                    e.getCode());
        }
    }

    @Test
    public void testDeleteSuccess_folderNotPresent() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSuccess_folderNotPresent");
        try {
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_4);
            folderService.delete(inputParams2);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
            		FolderError.FLD_NOT_FOUND.name(),
                    e.getCode());
        }
    }
    
    @Test
    public void testDeleteAllFolderWithNoEmail() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testDeleteAllFolderSuccess");
        try {
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, null);
            folderService.deleteAll(inputParams2);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }
    
    @Test
    public void testDeleteAllFolderSuccess() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testDeleteAllFolderSuccess");
        try {
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            folderService.deleteAll(inputParams2);
        } catch (MxOSException e) {
            fail("This should not have been come!!!");
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }
}
