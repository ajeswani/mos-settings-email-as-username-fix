package com.opwvmsg.mxos.test.unit.addressbook.contacts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsNameService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsNamePOST {

    private static final String TEST_NAME = "ContactNamePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsNameService contactsNameService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Name getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static Name getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        Name name = null;
        try {
            name = contactsNameService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("Contacts name object is null.", name);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
        return name;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            AddressBookException expectedError) {

        try {
            contactsNameService.update(params);

            params.clear();
            params.put(AddressBookProperty.userId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.userId.name()).add(USERID);

            params.put(AddressBookProperty.contactId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.contactId.name()).add(
                    String.valueOf(CONTACTID));

            if (session != null) {
                params.put(AddressBookProperty.sessionId.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.sessionId.name()).add(
                        session.getSessionId());

                params.put(AddressBookProperty.cookieString.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.cookieString.name()).add(
                        session.getCookieString());
            }
            Name name = getParams(params);

            String firstName = name.getFirstName();
            assertNotNull("FirstName is null.", firstName);
            assertEquals("updated firstName value is not equal.", "firstName2",
                    firstName);

            String middleName = name.getMiddleName();
            assertNotNull("MiddleName is null.", middleName);
            assertEquals("updated middleName value is not equal.",
                    "middleName2", middleName);

            String lastName = name.getLastName();
            assertNotNull("LastName is null.", lastName);
            assertEquals("updated lastName value is not equal.", "lastName2",
                    lastName);

            String displayName = name.getDisplayName();
            assertNotNull("DisplayName is null.", displayName);
            assertEquals("updated displayName value is not equal.",
                    "displayName2", displayName);

            String nickName = name.getNickName();
            assertNotNull("NickName is null.", nickName);
            assertEquals("updated nickName value is not equal.", "nickName2",
                    nickName);

        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsNameService = (IContactsNameService) ContextUtils.loadContext()
                .getService(ServiceEnum.ContactsNameService.name());
        login(USERID, PASSWORD);
        
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsNameService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    // base post
    @Test
    public void testContactsName() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsName");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        params.put("firstName", new ArrayList<String>());
        params.get("firstName").add("firstName2");

        params.put("middleName", new ArrayList<String>());
        params.get("middleName").add("middleName2");

        params.put("lastName", new ArrayList<String>());
        params.get("lastName").add("lastName2");

        params.put("displayName", new ArrayList<String>());
        params.get("displayName").add("displayName2");

        params.put("nickName", new ArrayList<String>());
        params.get("nickName").add("nickName2");

        setParams(params);
    }
}
