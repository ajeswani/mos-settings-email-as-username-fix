package com.opwvmsg.mxos.test.unit.smsservices;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsOnlineService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SmsOnlineGET {

    private static final String TEST_NAME = "SmsOnlineGET";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String EMAIL = "test.smsonline@openwave.com";
    private static final String PASSWORD = "Password1";
    private static ISmsOnlineService smsOnlineService;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        smsOnlineService = (ISmsOnlineService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsOnlineService.name());
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("SmsServicesService object is null.", smsOnlineService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        smsOnlineService = null;
        mailboxService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }
    
    private static SmsOnline getParams() {
        return getParams(null);
    }

    private static SmsOnline getParams(String expectedError) {
        SmsOnline smsOnline = null;
        try {
            smsOnline = smsOnlineService.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                addToParams(params, EMAIL_KEY, EMAIL);
            }
        }
        return smsOnline;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            smsOnlineService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("MailBox was not created.", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testGetWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithoutEmail");
        // Clear the params.
        params.remove(EMAIL_KEY);
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithNullEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, null);
        getParams(ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithEmptyEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "");
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "something.junk@foobar.com");
        getParams(MailboxError.MBX_NOT_FOUND.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInEmail() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testGetWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "!#$%^&*()+-=.junk@foobar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "junk@$&*^-bar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSmsOnlineEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSmsOnlineEnabled");
        MxosEnums.BooleanType smsOnlineEnabled = getParams().getSmsOnlineEnabled();
        assertNull("smsOnlineEnabled is not null.", smsOnlineEnabled);
    }

    @Test
    public void testInternationalSMSAllowed() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSAllowed");
        MxosEnums.BooleanType internationalSMSAllowed = getParams()
                .getInternationalSMSAllowed();
        assertNotNull("internationalSMSAllowed is not null.",
                internationalSMSAllowed);
    }

    @Test
    public void testInternationalSMSEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSEnabled");
        MxosEnums.BooleanType internationalSMSEnabled = getParams()
                .getInternationalSMSEnabled();
        assertNull("internationalSMSEnabled is not null.",
                internationalSMSEnabled);
    }

    @Test
    public void testMaxSMSPerDay() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxSMSPerDay");
        Integer maxSMSPerDay = getParams().getMaxSMSPerDay();
        assertNotNull("maxSMSPerDay is not null.", maxSMSPerDay);
    }

    @Test
    public void testConcatenatedSMSAllowed() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testConcatenatedSMSAllowed");
        MxosEnums.BooleanType concatenatedSMSAllowed = getParams()
                .getConcatenatedSMSAllowed();
        assertNotNull("concatenatedSMSAllowed is not null.", concatenatedSMSAllowed);
    }

    @Test
    public void testConcatenatedSMSEnabled() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testConcatenatedSMSEnabled");
        MxosEnums.BooleanType concatenatedSMSEnabled = getParams()
                .getConcatenatedSMSEnabled();
        assertNull("concatenatedSMSEnabled is not null.", concatenatedSMSEnabled);
    }

    @Test
    public void testMaxConcatenatedSMSSegments() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegments");
        Integer maxConcatenatedSMSSegments = getParams()
                .getMaxConcatenatedSMSSegments();
        assertNotNull("maxConcatenatedSMSSegments is not null.",
                maxConcatenatedSMSSegments);
    }

    @Test
    public void testMaxPerCaptchaSMS() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxPerCaptchaSMS");
        Integer maxPerCaptchaSMS = getParams().getMaxPerCaptchaSMS();
        assertNotNull("maxPerCaptchaSMS is not null.", maxPerCaptchaSMS);
    }

    @Test
    public void testMaxPerCaptchaDurationMins() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMins");
        Integer maxPerCaptchaDurationMins = getParams()
                .getMaxPerCaptchaDurationMins();
        assertNotNull("maxPerCaptchaDurationMins is not null.",
                maxPerCaptchaDurationMins);
    }

}
