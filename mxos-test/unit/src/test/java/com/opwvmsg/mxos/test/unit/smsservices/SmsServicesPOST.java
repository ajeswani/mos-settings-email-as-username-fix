package com.opwvmsg.mxos.test.unit.smsservices;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SmsServicesPOST {

    private static final String TEST_NAME = "SmsServicesPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String EMAIL = "test.smsservices@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static ISmsServicesService smsServicesService;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        smsServicesService = (ISmsServicesService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsServicesService.name());
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("Object is null.", smsServicesService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        smsServicesService = null;
        mailboxService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static SmsServices getParams() {
        SmsServices smsServices = null;
        try {
            smsServices = smsServicesService.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return smsServices;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            smsServicesService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("MailBox was not created.", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSmsServicesAllowedNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedNullParam");
        String key = MailboxProperty.smsServicesAllowed.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesAllowedEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedEmptyParam");
        String key = MailboxProperty.smsServicesAllowed.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesAllowedInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedInvalidParam");
        String key = MailboxProperty.smsServicesAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_SERVICES_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesAllowedSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedSplCharsInParam");
        String key = MailboxProperty.smsServicesAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_SERVICES_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesAllowedSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedSuccess");
        String key = MailboxProperty.smsServicesAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String smsServicesAllowed = getParams().getSmsServicesAllowed().name();
        assertNotNull("Is null.", smsServicesAllowed);
        assertTrue("Has a wrong value.",
                smsServicesAllowed
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }
    
    @Test
    public void testSmsServicesAllowedSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedSuccess1");
        String key = MailboxProperty.smsServicesAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String smsServicesAllowed = getParams().getSmsServicesAllowed().name();
        assertNotNull("Is null.", smsServicesAllowed);
        assertTrue("Has a wrong value.",
                smsServicesAllowed
                        .equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnNullParam");
        String key = MailboxProperty.smsServicesMsisdn.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnEmptyParam");
        String key = MailboxProperty.smsServicesMsisdn.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnInvalidParam");
        String key = MailboxProperty.smsServicesMsisdn.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams, MailboxError.MBX_INVALID_SMS_SERVICES_MSISDN.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnSplCharsInParam");
        String key = MailboxProperty.smsServicesMsisdn.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams, MailboxError.MBX_INVALID_SMS_SERVICES_MSISDN.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnSuccess");
        String key = MailboxProperty.smsServicesMsisdn.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("+919999999999");
        updateParams(updateParams);
        String smsServicesMsisdn = getParams().getSmsServicesMsisdn();
        assertNotNull("Is null.", smsServicesMsisdn);
        assertTrue("Has a wrong value.",
                smsServicesMsisdn.equals("+919999999999"));
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnStatusNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnStatusNullParam");
        String key = MailboxProperty.smsServicesMsisdnStatus.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnStatusEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnStatusEmptyParam");
        String key = MailboxProperty.smsServicesMsisdnStatus.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnStatusInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnStatusInvalidParam");
        String key = MailboxProperty.smsServicesMsisdnStatus.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_SERVICES_MSISDN_STATUS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnStatusSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnStatusSplCharsInParam");
        String key = MailboxProperty.smsServicesMsisdnStatus.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_SERVICES_MSISDN_STATUS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsServicesMsisdnStatusSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnStatusSuccess");
        String key = MailboxProperty.smsServicesMsisdnStatus.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("activated");
        updateParams(updateParams);
        MxosEnums.SmsServicesMsisdnStatusType smsServicesMsisdn = getParams()
                .getSmsServicesMsisdnStatus();
        assertNotNull("Is null.", smsServicesMsisdn);
        assertTrue("Has a wrong value.",
                smsServicesMsisdn
                        .equals(MxosEnums.SmsServicesMsisdnStatusType
                                .ACTIVATED));
        updateParams.remove(key);
    }
    
    @Test
    public void testSmsServicesMsisdnStatusSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnStatusSuccess1");
        String key = MailboxProperty.smsServicesMsisdnStatus.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("deactivated");
        updateParams(updateParams);
        MxosEnums.SmsServicesMsisdnStatusType smsServicesMsisdn = getParams()
                .getSmsServicesMsisdnStatus();
        assertNotNull("Is null.", smsServicesMsisdn);
        assertTrue("Has a wrong value.",
                smsServicesMsisdn
                        .equals(MxosEnums.SmsServicesMsisdnStatusType
                                .DEACTIVATED));
        updateParams.remove(key);
    }

    @Test
    public void testLastSmsServicesMsisdnStatusChangeDateNullParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLastSmsServicesMsisdnStatusChangeDateNullParam");
        String key = MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                .name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(
                updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testLastSmsServicesMsisdnStatusChangeDateEmptyParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLastSmsServicesMsisdnStatusChangeDateEmptyParam");
        String key = MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                .name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(
                updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testLastSmsServicesMsisdnStatusChangeDateInvalidParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLastSmsServicesMsisdnStatusChangeDateInvalidParam");
        String key = MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                .name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(
                updateParams,
                MailboxError
                .MBX_INVALID_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE.name());
        updateParams.remove(key);
    }

    @Test
    public void testLastSmsServicesMsisdnStatusChangeDateSplCharsInParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLastSmsServicesMsisdnStatusChangeDateSplCharsInParam");
        String key = MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                .name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(
                updateParams,
                MailboxError
                .MBX_INVALID_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testLastSmsServicesMsisdnStatusChangeDateSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLastSmsServicesMsisdnStatusChangeDateSuccess");
        String key = MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                .name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("2012-08-28T00:00:00Z");
        updateParams(
                updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testLastSmsServicesMsisdnStatusChangeDateNeg() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLastSmsServicesMsisdnStatusChangeDateNeg");
        String key = MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                .name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("what is date");
        updateParams(
                updateParams,
                MailboxError.MBX_INVALID_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE.name());
        updateParams.remove(key);
    }
}
