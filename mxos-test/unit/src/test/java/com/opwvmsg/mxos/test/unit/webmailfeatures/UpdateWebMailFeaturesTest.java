package com.opwvmsg.mxos.test.unit.webmailfeatures;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateWebMailFeaturesTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IWebMailFeaturesService wmfs;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateWebMailFeaturesTest.setUpBeforeClass...");
        wmfs = (IWebMailFeaturesService) ContextUtils.loadContext()
                .getService(ServiceEnum.WebMailFeaturesService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateWebMailFeaturesTest.setUp...");
        assertNotNull("WebMailFeaturesService object is null.", wmfs);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateWebMailFeaturesTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateWebMailFeaturesTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        wmfs = null;
    }

    private static WebMailFeatures getWebMailFeaturesParams() {
        WebMailFeatures wmf = null;
        try {
            wmf = wmfs.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("WebMailFeatures object is null.", wmf);
        return wmf;
    }

    private static void updateWebMailFeaturesParams(
            Map<String, List<String>> updateParams) {
        updateWebMailFeaturesParams(updateParams, null);
    }

    private static void updateWebMailFeaturesParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            wmfs.update(updateParams);
            if (null != expectedError) {
                fail("1. This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("2. This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                updateParams.put(EMAIL_KEY, new ArrayList<String>());
                updateParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWebMailFeaturesWithoutEmail() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateWebMailFeaturesTest.testUpdateWebMailFeaturesWithoutEmail...");
        updateWebMailFeaturesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateWebMailFeaturesWithEmptyEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add("");
        System.out
                .println("UpdateWebMailFeaturesTest.testUpdateWebMailFeaturesWithEmptyEmail...");
        updateWebMailFeaturesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateWebMailFeaturesWithNullEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(null);
        System.out
                .println("UpdateWebMailFeaturesTest.testUpdateWebMailFeaturesWithNullEmail...");
        updateWebMailFeaturesParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateWebMailFeaturesWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testUpdateMailAccessWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateWebMailFeaturesParams(updateParams,
                ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testBusinessFeaturesEnabledNullParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testBusinessFeaturesEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateWebMailFeaturesParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateWebMailFeaturesParams(updateParams);
        BooleanType pa = getWebMailFeaturesParams().getBusinessFeaturesEnabled();
        assertNotNull("BusinessFeaturesEnabled is null.", pa);
        assertNotNull("BusinessFeaturesEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testBusinessFeaturesEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testBusinessFeaturesEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateWebMailFeaturesParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateWebMailFeaturesParams(updateParams);
        BooleanType pa = getWebMailFeaturesParams().getBusinessFeaturesEnabled();
        assertNotNull("BusinessFeaturesEnabled is null.", pa);
        assertNotNull("BusinessFeaturesEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testBusinessFeaturesEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testBusinessFeaturesEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testBusinessFeaturesEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testBusinessFeaturesEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testBusinessFeaturesEnabledSuccess() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testBusinessFeaturesEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateWebMailFeaturesParams(updateParams);
        BooleanType pa = getWebMailFeaturesParams()
                .getBusinessFeaturesEnabled();
        assertNotNull("BusinessFeaturesEnabled is null.", pa);
        assertTrue("BusinessFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledNullParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testFullFeaturesEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateWebMailFeaturesParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateWebMailFeaturesParams(updateParams);
        BooleanType pa = getWebMailFeaturesParams().getFullFeaturesEnabled();
        assertNotNull("FullFeaturesEnabled is null.", pa);
        assertNotNull("FullFeaturesEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testFullFeaturesEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateWebMailFeaturesParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateWebMailFeaturesParams(updateParams);
        BooleanType pa = getWebMailFeaturesParams().getFullFeaturesEnabled();
        assertNotNull("FullFeaturesEnabled is null.", pa);
        assertNotNull("FullFeaturesEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testFullFeaturesEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testFullFeaturesEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledSuccess() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testFullFeaturesEnabledSuccess...");
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateWebMailFeaturesParams(updateParams);
        BooleanType pa = getWebMailFeaturesParams().getFullFeaturesEnabled();
        assertNotNull("FullFeaturesEnabled is null.", pa);
        assertTrue("FullFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testLastFullFeaturesConnectionDateNullParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testLastFullFeaturesConnectionDateNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.lastFullFeaturesConnectionDate.name();
        updateParams.put(key, new ArrayList<String>());
        String date = formatDate(new Date(System.currentTimeMillis()));
        updateParams.get(key).add(date);
        updateWebMailFeaturesParams(updateParams);
        assertNotNull("LastFullFeaturesConnectionDate is not null.",
                getWebMailFeaturesParams().getLastFullFeaturesConnectionDate());
        updateParams.get(key).clear();

        updateParams.get(key).add(null);
        updateWebMailFeaturesParams(updateParams);
        Object ldapDate = getWebMailFeaturesParams()
                .getLastFullFeaturesConnectionDate();
        assertNull("LastFullFeaturesConnectionDate is not null.", ldapDate);
        updateParams.remove(key);
    }

    @Test
    public void testLastFullFeaturesConnectionDateEmptyParam() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testLastFullFeaturesConnectionDateEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.lastFullFeaturesConnectionDate.name();
        updateParams.put(key, new ArrayList<String>());
        String date = formatDate(new Date(System.currentTimeMillis()));
        updateParams.get(key).add(date);
        updateWebMailFeaturesParams(updateParams);
        assertNotNull("LastFullFeaturesConnectionDate is not null.",
                getWebMailFeaturesParams().getLastFullFeaturesConnectionDate());
        updateParams.get(key).clear();

        updateParams.get(key).add("");
        updateWebMailFeaturesParams(updateParams);
        Object ldapDate = getWebMailFeaturesParams()
                .getLastFullFeaturesConnectionDate();
        assertNull("LastFullFeaturesConnectionDate is not null.", ldapDate);
        updateParams.remove(key);
    }

    @Test
    public void testLastFullFeaturesConnectionDateInvalidParam()
            throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testLastFullFeaturesConnectionDateInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.lastFullFeaturesConnectionDate.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_LAST_FULL_FEATURES_CONN_DATE
                        .name());
        updateParams.remove(key);
    }

    @Test
    public void testLastFullFeaturesConnectionDateSplCharsInParam()
            throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testLastFullFeaturesConnectionDateSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.lastFullFeaturesConnectionDate.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_LAST_FULL_FEATURES_CONN_DATE
                        .name());
        updateParams.remove(key);
    }

    @Test
    public void testLastFullFeaturesConnectionDateInvalidDate()
            throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testLastFullFeaturesConnectionDateInvalidDate...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.lastFullFeaturesConnectionDate.name();
        updateParams.put(key, new ArrayList<String>());
        String date = formatDate(new Date(System.currentTimeMillis()));
        updateParams.get(key).add("1234-" + date);
        updateWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_LAST_FULL_FEATURES_CONN_DATE
                .name());
        updateParams.remove(key);
    }

    @Test
    public void testLastFullFeaturesConnectionDateSuccess() throws Exception {
        System.out
                .println("UpdateWebMailFeaturesTest.testLastFullFeaturesConnectionDateSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.lastFullFeaturesConnectionDate.name();
        updateParams.put(key, new ArrayList<String>());
        String date = formatDate(new Date(System.currentTimeMillis()));
        updateParams.get(key).add(date);
        updateWebMailFeaturesParams(updateParams);
        Object ldapDate = getWebMailFeaturesParams()
                .getLastFullFeaturesConnectionDate();
        assertNotNull("LastFullFeaturesConnectionDate is null.", ldapDate);
        assertNotNull("actualDate is null.", date);
        assertTrue("LastFullFeaturesConnectionDate has a wrong value.",
                date.equals(ldapDate));
        updateParams.remove(key);
    }

    private static String formatDate(Date d) {
        String date = null;
        String format = System
                .getProperty(SystemProperty.userDateFormat.name());
        try {
            date = new SimpleDateFormat(format).format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

}
