package com.opwvmsg.mxos.test.unit.captcha;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to captcha 
@SuiteClasses({
    CaptchaValidateTest.class
})
public class CaptchaSuite {
}
