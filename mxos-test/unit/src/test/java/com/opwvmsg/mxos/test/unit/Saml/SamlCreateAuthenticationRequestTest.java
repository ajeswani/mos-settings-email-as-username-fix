package com.opwvmsg.mxos.test.unit.Saml;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.saml.ISamlService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 *
 * @author mxos-dev
 */
public class SamlCreateAuthenticationRequestTest {
    private static ISamlService samlService;
    private static IMxOSContext context;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CaptchaValidateTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        samlService = (ISamlService) context
                .getService(ServiceEnum.SamlService.name());
        assertNotNull("SamlService object is null.", samlService);
        
    }
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("SamlCreateAuthenticationRequestTest.setUp...");
    }
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("SamlCreateAuthenticationRequestTest.tearDown...");
    }
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("SamlCreateAuthenticationRequestTest.tearDownAfterClass...");
        // Delete the mailbox if already exists
        samlService=null;
    }
    
    @Test
    public void testNullTargetURLInCreateSamlAuthenticationRequest() {
        System.out.println("\n\ntestNullTargetURLInCreateSamlAuthenticationRequest...");
        List<String> acsURL= new ArrayList<String>();
        acsURL.add(null);
        params.put("acsURL", acsURL); 
        try {
            String authRequest= samlService.getSAMLRequest(params);
            assertNull("AuthRequest is null", authRequest);
            params.clear();
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }

    }
    
  
    @Test
    public void testSuccessCreateSamlAuthenticationRequest() {
        System.out.println("\n\ntestSuccessCreateSamlAuthenticationRequest...");
        List<String> acsURL= new ArrayList<String>();
        acsURL.add("http%3A%2F%2Fmsg-buildlnx01.openwave.com%3A8080%2Fview%2FmOS%2Fjob%2FmOS-2.0%2F");
        params.put("acsURL", acsURL); 
        try {
            String authRequest= samlService.getSAMLRequest(params);
            assertNotNull("AuthRequest is null", authRequest);
            params.clear();
        } catch (MxOSException e) {
            fail();
            System.out.println(e.getCode());
        }

    }    
}
