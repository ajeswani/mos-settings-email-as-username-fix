package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 *
 * @author mxos-dev
 * 
 */
public class MessageInFolderMOVE {

    private static final String TEST_NAME = "MessageInFolderMOVE";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "message.rest666@openwave.com";
    private static final String PASSWORD = "test5";

    private static final String RECEIVED_FROM_KEY = MxOSPOJOs.receivedFrom
            .name();
    private static final String RECEIVED_FROM_VALUE = "message.rest777@openwave.com";
    private static final String MESSAGE_KEY = MxOSPOJOs.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDER_KEY = FolderProperty.folderName.name();
    private static final String SRC_FOLDER_KEY = FolderProperty.srcFolderName
            .name();
    private static final String TO_FOLDER_KEY = FolderProperty.toFolderName
            .name();
    private static final String MESSAGE_ID_KEY = MessageProperty.messageId.name();
    private static final String FOLDER_INBOX = "INBOX";
    private static final String FOLDER_TO = "SentMail";
    private static final String FOLDER_TO_TRASH = "Trash";
    private static final String OPTION_FOLDER_IS_HINT_KEY = MessageProperty.optionFolderIsHint
            .name();
    private static final String OPTION_FOLDER_IS_HINT_VALUE = "true";
    private static final String OPTION_MULTIPLE_OK_KEY = MessageProperty.optionMultipleOk
            .name();
    private static final String OPTION_MULTIPLE_OK_VALUE = "true";
    private static final String COS_ID = "default";
    private static final String FOLDERNAME_VALUE_11_22 = "1111111111111111111111111111111111111111111111%2f22222222222222222222222222222222222222222222222" +
    "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222" +
    "222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222";
    private static final String FOLDERNAME_VALUE_10 = "a%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2ft";

    private static IMxOSContext context;
    private static IMessageService messageService;
    private static IMetadataService metadataService;
    private static IMailboxService mailboxService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        createMailBox(EMAIL, PASSWORD);
        createMailBox(RECEIVED_FROM_VALUE, PASSWORD);
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", messageService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox(EMAIL);
        deleteMailBox(RECEIVED_FROM_VALUE);
        params.clear();
        params = null;
        messageService = null;
        mailboxService = null;
    }

    private static void createMailBox(String email, String password) {
        Long id = MailboxHelper.createMailbox(email, password, COS_ID);
        if (id == -1L)
            fail("Failed to create Mailbox");
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static String createMessage() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, RECEIVED_FROM_KEY, RECEIVED_FROM_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            return messageService.create(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
        return null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testMoveMessageInFolderNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageInFolderNoEmail");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            messageService.move(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testMoveMessageInFolderNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageInFolderNoFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            messageService.move(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMoveMessageInFolderNonExistingFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageInFolderNonExistingFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, "asdfadsf");
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            messageService.move(inputParams);
        } catch (MxOSException e) {
            String expectedError = FolderError.FLD_NOT_FOUND.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testMoveMessageInFolderInvalidFolderName() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageInFolderInvalidFolderName");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDERNAME_VALUE_11_22);
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            messageService.move(inputParams);
        } catch (MxOSException e) {
            String expectedError = FolderError.FLD_INVALID_TO_FOLDERNAME.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }
    
    @Test
    public void testMoveMessageInFolderInvalidFolderDepth() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageInFolderInvalidFolderDepth");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDERNAME_VALUE_10);
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            messageService.move(inputParams);
        } catch (MxOSException e) {
            String expectedError = FolderError.FLD_INVALID_TO_FOLDERNAME.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }
    
    @Test
    public void testMoveMessageSourceDestFolderSame() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageSourceDestFolderSame");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, "' '");
            addParams(inputParams, TO_FOLDER_KEY, "' '");
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            
            messageService.move(inputParams);
        } catch (MxOSException e) {
            String expectedError = MessageError
                .MSG_ERROR_MOVE_SOURCE_DEST_CANNOT_BE_SAME
                    .name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testMoveAllMessageInFolderSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageInFolderSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            createMessage();
            createMessage();
            createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.moveAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            // Map<String, MessageMetaData> messages =
            // messageService.readAllMessageMetaDatasInFolder(inputParams);
            assertTrue("Message not moved", messages.size() == 0);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testMoveMessageInFolderWithMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageInFolderWithMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            String messageId = null;
            for (String msg : messages.keySet()) {
                messageId = msg;
            }

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, MESSAGE_ID_KEY, messageId);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.move(inputParams);
            System.out.println("move successfull");

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messages = metadataService.list(inputParams);
            for (String msg : messages.keySet()) {
                String msgIdAfterMove = msg;
                if (msgIdAfterMove != null && msgIdAfterMove.equals(messageId)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testMoveMessageWithMultipleSameMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageWithMultipleSameMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.moveAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO_TRASH);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.moveAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            //Map<String, MessageMetaData> messages = messageService
              //      .readAllMessageMetaDatasInFolder(inputParams);
            if (messages.size() > 0) {
                fail();
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testMoveMultipleMessagesInFolderWithMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMultipleMessagesInFolderWithMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();

            Map<String, Metadata> messages = metadataService
                    .list(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            inputParams.put(MESSAGE_ID_KEY, new ArrayList<String>());
            for (String msg : messages.keySet()) {
                inputParams.get(MESSAGE_ID_KEY).add(msg);
            }
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.move(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);

            Map<String, Metadata> messagesInFromFolder = metadataService
                    .list(inputParams);
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);

            Map<String, Metadata> messagesInToFolder = metadataService
                    .list(inputParams);
            if (messagesInFromFolder.keySet().size() > 0) {
                fail();
            }
            if (messagesInToFolder.keySet().size() < 3) {
                fail();
            }

            for (String msg : messages.keySet()) {
                String msgIdInFromFolder = msg;
                if (msgIdInFromFolder != null
                        && !messagesInToFolder.containsKey(msgIdInFromFolder)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testMoveMultipleMessages_Success() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMultipleMessages_Success");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            String messageId = null;
            for (String msg : messages.keySet()) {
                messageId = msg;
            }

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, MESSAGE_ID_KEY, messageId);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.moveMulti(inputParams);
            System.out.println("move successfull");

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messages = metadataService.list(inputParams);
            for (String msg : messages.keySet()) {
                String msgIdAfterMove = msg;
                if (msgIdAfterMove != null && msgIdAfterMove.equals(messageId)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }
}
