package com.opwvmsg.mxos.test.unit.addressbook.contacts.personalInfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoCommunicationService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsPersonalInfoCommunicationPOST {

    private static final String TEST_NAME = "ContactsPersonalInfoCommunicationPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsPersonalInfoCommunicationService contactsPersonalInfoCommunicationService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Map<String, List<String>> getBasicParams(
            Map<String, List<String>> params) {

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        return params;
    }

    private static Communication getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static Communication getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        Communication communication = null;
        try {
            communication = contactsPersonalInfoCommunicationService
                    .read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("Personal Info address object is null.",
                        communication);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
        return communication;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    private static void setEmptyParams(Map<String, List<String>> params) {

        try {
            contactsPersonalInfoCommunicationService.update(params);

        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
        }
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            AddressBookException expectedError) {

        try {
            contactsPersonalInfoCommunicationService.update(params);

            if (expectedError != null)
                fail("This should not have been come!!!");

            params.clear();
            Map<String, List<String>> inputParam = getBasicParams(params);

            Communication communication = getParams(inputParam);

            assertNotNull("Phone1 is null.", communication.getPhone1());
            assertNotNull("Phone2 is null.", communication.getPhone2());
            assertNotNull("Mobile is null.", communication.getMobile());
            assertNotNull("Fax is null.", communication.getFax());
            assertNotNull("Email is null.", communication.getEmail());
            assertNotNull("ImAddress is null.", communication.getImAddress());
            assertEquals("Updated Phone1 value is not equal.", "+1223444",
                    communication.getPhone1());
            assertEquals("Updated Phone2 value is not equal.", "+1223455",
                    communication.getPhone2());
            assertEquals("Updated Mobile value is not equal.", "1223444",
                    communication.getMobile());
            assertEquals("Updated Fax value is not equal.", "+19620300590",
                    communication.getFax());
            assertEquals("Updated Email value is not equal.",
                    "alice.bob@owmessaging.com", communication.getEmail());
            assertEquals("Updated ImAddress value is not equal.",
                    "alice.bob@owmessaging.com", communication.getImAddress());

        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
        }

    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsPersonalInfoCommunicationService = (IContactsPersonalInfoCommunicationService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ContactsPersonalInfoCommunicationService
                                .name());
        login(USERID, PASSWORD);
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsPersonalInfoCommunicationService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    // base post
    @Test
    public void testContactsPersonalInfoCommunication() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoCommunication");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("phone1", new ArrayList<String>());
        inputParam.get("phone1").add("+1223444");

        inputParam.put("phone2", new ArrayList<String>());
        inputParam.get("phone2").add("+1223455");

        inputParam.put("mobile", new ArrayList<String>());
        inputParam.get("mobile").add("1223444");

        inputParam.put("fax", new ArrayList<String>());
        inputParam.get("fax").add("+19620300590");

        inputParam.put("email", new ArrayList<String>());
        inputParam.get("email").add("alice.bob@owmessaging.com");

        inputParam.put("imAddress", new ArrayList<String>());
        inputParam.get("imAddress").add("alice.bob@owmessaging.com");

        setParams(params);
    }

    @Test
    public void testContactsPersonalInfoCommunicationEmptyParam()
            throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoCommunicationEmptyParam");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("phone1", new ArrayList<String>());
        inputParam.get("phone1").add("");

        inputParam.put("phone2", new ArrayList<String>());
        inputParam.get("phone2").add("");

        inputParam.put("mobile", new ArrayList<String>());
        inputParam.get("mobile").add("");

        inputParam.put("fax", new ArrayList<String>());
        inputParam.get("fax").add("");

        inputParam.put("email", new ArrayList<String>());
        inputParam.get("email").add("");

        inputParam.put("voip", new ArrayList<String>());
        inputParam.get("voip").add("");

        inputParam.put("imAddress", new ArrayList<String>());
        inputParam.get("imAddress").add("");

        setEmptyParams(inputParam);
    }

    @Test
    public void testContactsPersonalInfoCommunicationNegTest() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoCommunicationNegTest");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("email", new ArrayList<String>());
        inputParam.get("email").add("121");

        setParams(
                params,
                new AddressBookException(
                        AddressBookError.ABS_PERSONALINFO_INVALID_IM_ADDRESS
                                .toString()));
    }
}
