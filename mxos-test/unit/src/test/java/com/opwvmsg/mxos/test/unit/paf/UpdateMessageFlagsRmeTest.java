package com.opwvmsg.mxos.test.unit.paf;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.backend.crud.mss.MssSlMetaCRUD;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.Mailbox;
import com.opwvmsg.utils.paf.intermail.mail.MsgFlagChangeInfo;
import com.opwvmsg.utils.paf.intermail.mail.MsgFlags;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.mail.MsgFlags.MsgFlagIndex;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMsg;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.StoreMsgFlags;

public class UpdateMessageFlagsRmeTest {

    private static final String TEST_NAME = "UpdateMessageFlagsRmeTest";
    private static final String ARROW_SEP = " --> ";

    public static String CONFIG_HOST = null;
    public static String CONFIG_PORT = null;
    public static String MSS_HOST = null;
    public static final String DEFAULT_APP = "mss";
    public static final String DEFAULT_HOST = "*";
    public static String MSS_RME_VERSION = "175";
    // public static final String MAILBOX_ID = "1366645701422";
    public static String MAILBOX_ID = "7126905549041528032";
    public static final String MESSAGE = "Test message from junit client";
    public static final String FROM_ADDRESS = "<abc@123.com>";
    public static final String FLAGS_STRING = "TFFFFF-";
    public static final String SRC_FOLDER_NAME = "INBOX";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        try {
            String mxosPropertyFile = System.getProperty("MXOS_HOME");
            ;
            String mxosconfigfile = mxosPropertyFile
                    + "/config/mxos.properties";
            Properties propfile = new Properties();
            try {
                propfile.load(new FileInputStream(mxosconfigfile));
            } catch (FileNotFoundException e) {
                System.err.println("Unable to find test properties file "
                        + propfile);
            } catch (IOException e) {
                System.err.println("Unable to open test properties file "
                        + propfile);
            }
            CONFIG_HOST = propfile.getProperty("configHost");
            CONFIG_PORT = propfile.getProperty("configPort");
            MSS_RME_VERSION = String.valueOf(MxOSConstants.RME_MX9_VER);
            MSS_HOST = CONFIG_HOST.substring(0, CONFIG_HOST.indexOf("."));
            System.out.println("CONFIG_HOST = " + CONFIG_HOST
                    + " CONFIG_PORT = " + CONFIG_PORT + " MSS_HOST = "
                    + MSS_HOST + " MSS_RME_VERSION = " + MSS_RME_VERSION);
            Config.setConfig("intermail", getConfigdbMap());
            Config config = Config.getInstance();
            createMailBox();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox();
        getConfigdbMap().clear();
    }

    private static Map<String, Object> getConfigdbMap() {
        HashMap<String, Object> configdbMap = new HashMap<String, Object>();
        System.setProperty("mssRmeVersion", MSS_RME_VERSION);
        configdbMap.put("confServHost", CONFIG_HOST);
        configdbMap.put("confServPort", CONFIG_PORT);
        configdbMap.put("defaultApp", DEFAULT_APP);
        configdbMap.put("defaultHost", DEFAULT_HOST);
        return configdbMap;
    }

    @Test
    public void testUpdateMessageFlagsSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageFlagsSuccess");
        try {

            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            if (srcFolderUUID == null) {
                fail();
            }
            UUID msgUUID = createMessage(srcFolderUUID);

            MsgFlagChangeInfo[] msgFlagChangeInfo = new MsgFlagChangeInfo[1];

            msgFlagChangeInfo[0] = new MsgFlagChangeInfo();
            msgFlagChangeInfo[0].setMsgUUID(msgUUID);

            MsgFlags modFlags = new MsgFlags();
            modFlags.SetFlag(MsgFlagIndex.flagdraft, true);
            modFlags.SetFlag(MsgFlagIndex.flagans, true);
            modFlags.SetFlag(MsgFlagIndex.flagrecent, false);
            int modified = modFlags.getFlagInt();

            MsgFlags newFlags = new MsgFlags();
            newFlags.SetFlag(MsgFlagIndex.flagdraft, true);
            newFlags.SetFlag(MsgFlagIndex.flagans, true);
            newFlags.SetFlag(MsgFlagIndex.flagrecent, false);
            int newFlag = newFlags.getFlagInt();

            msgFlagChangeInfo[0].setModifiedFlags(modified);
            msgFlagChangeInfo[0].setNewFlagValues(newFlag);
            msgFlagChangeInfo[0].setKeywords("");

/*            StoreMsgFlags op5 = new StoreMsgFlags(MSS_HOST, MAILBOX_ID, null,
                    srcFolderUUID, msgFlagChangeInfo, false,
                    Mailbox.ACCESS_GENERIC_END_USER);
            op5.execute();*/
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testUpdateMessageWithFolderUUIDNull() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageWithFolderUUIDNull");
        try {

            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            UUID msgUUID = createMessage(srcFolderUUID);

            srcFolderUUID = null;
            MsgFlagChangeInfo[] msgFlagChangeInfo = new MsgFlagChangeInfo[1];

            msgFlagChangeInfo[0] = new MsgFlagChangeInfo();
            msgFlagChangeInfo[0].setMsgUUID(msgUUID);

            MsgFlags modFlags = new MsgFlags();
            modFlags.SetFlag(MsgFlagIndex.flagdraft, true);
            modFlags.SetFlag(MsgFlagIndex.flagans, true);
            modFlags.SetFlag(MsgFlagIndex.flagrecent, false);
            int modified = modFlags.getFlagInt();

            MsgFlags newFlags = new MsgFlags();
            newFlags.SetFlag(MsgFlagIndex.flagdraft, true);
            newFlags.SetFlag(MsgFlagIndex.flagans, true);
            newFlags.SetFlag(MsgFlagIndex.flagrecent, false);
            int newFlag = newFlags.getFlagInt();

            msgFlagChangeInfo[0].setModifiedFlags(modified);
            msgFlagChangeInfo[0].setNewFlagValues(newFlag);
            msgFlagChangeInfo[0].setKeywords("");

/*            StoreMsgFlags op5 = new StoreMsgFlags(MSS_HOST, MAILBOX_ID, null,
                    srcFolderUUID, msgFlagChangeInfo, false,
                    Mailbox.ACCESS_GENERIC_END_USER);
            op5.execute();*/
            fail("This should not have been come!!!");
/*        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);*/
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testUpdateMessageWithFlagChangeInfoNull() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageWithFlagChangeInfoNull");
        try {

            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            if (srcFolderUUID == null) {
                fail();
            }
            MsgFlagChangeInfo[] msgFlagChangeInfo = null;
/*            StoreMsgFlags op5 = new StoreMsgFlags(MSS_HOST, MAILBOX_ID, null,
                    srcFolderUUID, msgFlagChangeInfo, false,
                    Mailbox.ACCESS_GENERIC_END_USER);
            op5.execute();*/
            fail("This should not have been come!!!");
/*        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);*/
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    private static UUID createMessage(UUID folderUUID) {
        try {
            int options = 0x121C;
            CreateMsg cm = new CreateMsg(MSS_HOST, MAILBOX_ID, null,
                    folderUUID, MESSAGE.getBytes(), FROM_ADDRESS, FLAGS_STRING, null,
                    false, 0, 0, options, null, null);
            cm.execute();
            return cm.getMsgUUID();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
        return null;
    }

    private static void createMailBox() {
        try {
            CreateMailbox createMailbox = new CreateMailbox(MSS_HOST,
                    MAILBOX_ID, null, null, 0, 0);
            createMailbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while creating mailbox.", true);
        }
    }

    private static void deleteMailBox() {
        try {
            DeleteMailbox deleteMAilbox = new DeleteMailbox(MSS_HOST,
                    MAILBOX_ID, null, false, false, false, RmeDataModel.Leopard);
            deleteMAilbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while deleting mailbox.", true);
        }
    }
}
