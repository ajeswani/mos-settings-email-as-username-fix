/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminApprovedSendersListService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class AdminApprovedSendersGET {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String ADMIN_EMAIL = "head2@openwave.com";
    private static final String CHILD_EMAIL = "child005@openwave.com";
    private static final String INVALID_EMAIL = "alphabeta@openwave.com";
    private static final String PASSWORD = "test";
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "AdminApprovedSendersGET";
    private static final String ARROW_SEP = " --> ";
    private static IAdminApprovedSendersListService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IAdminApprovedSendersListService) ContextUtils.loadContext()
                .getService(ServiceEnum.AdminApprovedSendersListService.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        if (params.containsKey(EMAIL_KEY))
            params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        service = null;
    }

    private static void addToParams(String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static List<String> getParams(String key, String value) {
        addToParams(key, value);
        return getParams(null);
    }

    private static List<String> getParams(String key, String value,
            String expectedError) {
        addToParams(key, value);
        return getParams(expectedError);
    }

    private static List<String> getParams(String expectedError) {
        List<String> adminApprovedSendersList = null;
        try {
            adminApprovedSendersList = service.read(params);
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
            }
        }
        return adminApprovedSendersList;
    }

    @Test
    public void testAdminApprovedSendersForAdminMail() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersForAdminMail");
        List<String> value = getParams(EMAIL_KEY, ADMIN_EMAIL);
        System.out.println(value.toString());
    }

    @Test
    public void testAdminApprovedSendersAllowedForChildMail()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersAllowedForChildMail");
        List<String> value = getParams(EMAIL_KEY, CHILD_EMAIL);
        System.out.println(value.toString());
    }

    @Test
    public void testAdminApprovedSendersAllowedForInvalidMail()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersAllowedForInvalidMail");
        List<String> value = getParams(EMAIL_KEY, INVALID_EMAIL,
                "invalid email can not retrieve family details");
    }

}
