package com.opwvmsg.mxos.test.unit.cos.webmailfeatures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosWebMailFeaturesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetCosWebMailFeaturesTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosWebMailFeaturesService wmfs;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.setUpBeforeClass...");
        wmfs = (ICosWebMailFeaturesService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosWebMailFeaturesService.name());
        boolean flag = CosHelper.createCos(COS_ID);
        assertTrue("Cos was not created.", flag);
        params.put(COSID_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.setUp...");
        assertNotNull("CosWebMailFeaturesService object is null.", wmfs);
        assertNotNull("Input Param:cosId is null.", params.get(COSID_KEY));
        params.get(COSID_KEY).add(COS_ID);
        assertTrue("Input Param:cosId is empty.", !params.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.tearDown...");
        if (null != params.get(COSID_KEY)) {
            params.get(COSID_KEY).clear();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.tearDownAfterClass...");
        CosHelper.deleteCos(COS_ID);
        params.clear();
        params = null;
        wmfs = null;
    }

    private static WebMailFeatures getCosWebMailFeaturesParams(
            Map<String, List<String>> params) {
        return getCosWebMailFeaturesParams(params, null);
    }

    private static WebMailFeatures getCosWebMailFeaturesParams(
            Map<String, List<String>> params, String expectedError) {
        WebMailFeatures wmf = null;
        try {
            wmf = wmfs.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("WebMailFeatures object is null.", wmf);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(COSID_KEY, new ArrayList<String>());
            }
        }
        return wmf;
    }

    @Test
    public void testGetWebMailFeaturesParamsWithoutCosId() {
        System.out
                .println("GetCosWebMailFeaturesTest.testGetWebMailFeaturesParamsWithoutCosId...");
        // Clear the params.
        params.clear();
        getCosWebMailFeaturesParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(COSID_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetWebMailFeaturesWithNullCosId() {
        System.out
                .println("GetCosWebMailFeaturesTest.testGetWebMailFeaturesWithNullCosId...");
        // Remove the cosId from the map to have a null cosId.
        params.get(COSID_KEY).clear();
        getCosWebMailFeaturesParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetWebMailFeaturesWithNonExistingCosId() {
        System.out
                .println("GetCosWebMailFeaturesTest.testGetWebMailFeaturesWithNonExistingCosId...");
        // Replace openwave with a junk value to set an invalid cosId.
        params.get(COSID_KEY).clear();
        params.get(COSID_KEY).add("something_junk_cos_1234");
        getCosWebMailFeaturesParams(params, CosError.COS_NOT_FOUND.name());
    }

    @Test
    public void testBusinessFeaturesEnabledSuccess() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.testBusinessFeaturesEnabledSuccess...");
        BooleanType pa = getCosWebMailFeaturesParams(params).getBusinessFeaturesEnabled();
        assertNotNull("BusinessFeaturesEnabled is null.", pa);
        assertTrue("BusinessFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testFullFeaturesEnabledSuccess() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.testFullFeaturesEnabledSuccess...");
        BooleanType pa = getCosWebMailFeaturesParams(params).getFullFeaturesEnabled();
        assertNotNull("FullFeaturesEnabled is null.", pa);
        assertTrue("FullFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }
}
