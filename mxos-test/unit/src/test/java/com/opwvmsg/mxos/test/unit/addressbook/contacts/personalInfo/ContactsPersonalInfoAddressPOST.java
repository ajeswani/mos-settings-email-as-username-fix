package com.opwvmsg.mxos.test.unit.addressbook.contacts.personalInfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoAddressService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsPersonalInfoAddressPOST {

    private static final String TEST_NAME = "ContactsPersonalInfoAddressPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsPersonalInfoAddressService contactsPersonalInfoAddressService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Map<String, List<String>> getBasicParams(
            Map<String, List<String>> params) {

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        return params;
    }

    private static Address getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static Address getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        Address address = null;
        try {
            address = contactsPersonalInfoAddressService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("Personal Info address object is null.", address);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
            }
        }
        return address;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    private static void setEmptyParams(Map<String, List<String>> params) {

        try {

            contactsPersonalInfoAddressService.update(params);

            params.clear();
            Map<String, List<String>> inputParam = getBasicParams(params);

            Address address = getParams(inputParam);

            assertEquals("Updated Street value is not equal.", null,
                    address.getStreet());
            assertEquals("Updated City value is not equal.", null,
                    address.getCity());
            assertEquals("Updated StateOrProvince value is not equal.", null,
                    address.getStateOrProvince());
            assertEquals("Updated PostalCode value is not equal.", null,
                    address.getPostalCode());
            assertEquals("Updated Country value is not equal.", null,
                    address.getCountry());
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);

        }
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            AddressBookException expectedError) {

        try {
            contactsPersonalInfoAddressService.update(params);

            params.clear();
            Map<String, List<String>> inputParam = getBasicParams(params);

            Address address = getParams(inputParam);

            assertNotNull("Street is null.", address.getStreet());
            assertEquals("Updated Street value is not equal.",
                    "2101, Seaport Blvd", address.getStreet());
            assertNotNull("City is null.", address.getCity());
            assertEquals("Updated City value is not equal.", "Redwood city",
                    address.getCity());
            assertNotNull("StateOrProvince is null.",
                    address.getStateOrProvince());
            assertEquals("Updated StateOrProvince value is not equal.",
                    "California", address.getStateOrProvince());
            assertNotNull("PostalCode is null.", address.getPostalCode());
            assertEquals("Updated PostalCode value is not equal.", "45-765-24",
                    address.getPostalCode());
            assertNotNull("Country is null.", address.getCountry());
            assertEquals("Updated Country value is not equal.",
                    "United States", address.getCountry());

        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsPersonalInfoAddressService = (IContactsPersonalInfoAddressService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ContactsPersonalInfoAddressService.name());
        login(USERID, PASSWORD);
        
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsPersonalInfoAddressService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    // base post
    @Test
    public void testContactsPersonalInfoAddress() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoAddress");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("street", new ArrayList<String>());
        inputParam.get("street").add("2101, Seaport Blvd");

        inputParam.put("city", new ArrayList<String>());
        inputParam.get("city").add("Redwood city");

        inputParam.put("stateOrProvince", new ArrayList<String>());
        inputParam.get("stateOrProvince").add("California");

        inputParam.put("postalCode", new ArrayList<String>());
        inputParam.get("postalCode").add("45-765-24");

        inputParam.put("country", new ArrayList<String>());
        inputParam.get("country").add("United States");

        setParams(inputParam);

    }

    @Test
    public void testContactsPersonalInfoAddressEmptyParams() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoAddressEmptyParams");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("street", new ArrayList<String>());
        inputParam.get("street").add("");

        inputParam.put("city", new ArrayList<String>());
        inputParam.get("city").add("");

        inputParam.put("stateOrProvince", new ArrayList<String>());
        inputParam.get("stateOrProvince").add("");

        inputParam.put("postalCode", new ArrayList<String>());
        inputParam.get("postalCode").add("");

        inputParam.put("country", new ArrayList<String>());
        inputParam.get("country").add("");

        setEmptyParams(inputParam);

    }

}
