package com.opwvmsg.mxos.test.unit.internalinfo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Test Suite for test cases related to InternalInfo and
 * its micro api MessageEventRecords.
 * 
 * @author mxos-dev
 *
 */

@RunWith(Suite.class)
//Include all the test case classes related to InternalInfo and its micro api MessageEventRecords
@SuiteClasses({
    GetInternalInfoTest.class,
    UpdateInternalInfoTest.class,
    GetMessageEventRecordsTest.class,
    UpdateMessageEventRecordsTest.class
})
public class InternalInfoSuite {
}
