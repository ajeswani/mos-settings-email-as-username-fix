package com.opwvmsg.mxos.test.unit.captcha;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.captcha.ICaptchaService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 *
 * @author mxos-dev
 */
public class CaptchaValidateTest {
    private static final String COSID = "cos_2.0_captcha";
    private static final String DOMAIN_NAME = "openwave.com";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "cap100005@" + DOMAIN_NAME;
    private static final String PWD = "test";
    private static final String ISVALID_KEY = MailboxProperty.isValid.name();
    private static final String ISVALID_TRUE = "true";
    private static final String ISVALID_FALSE = "false";

    private static ICredentialService credentialService;
    private static ICaptchaService captchaService;
    private static IMxOSContext context;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CaptchaValidateTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        credentialService = (ICredentialService) context
                .getService(ServiceEnum.CredentialService.name());
        captchaService = (ICaptchaService) context
        .getService(ServiceEnum.CaptchaService.name());
        
        assertNotNull("MailboxBaseService object is null.", credentialService);
        assertNotNull("Captcha object is null.", captchaService);
        CosHelper.createCos(COSID);
        try{
            MailboxHelper.createMailbox(EMAIL, PWD, COSID);
            updateCredentials("0");
        }catch(Exception nEx){
            CosHelper.deleteCos(COSID);
        }
    }
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("CaptchaValidateTest.setUp...");
    }
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("CaptchaValidateTest.tearDown...");
    }
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CaptchaValidateTest.tearDownAfterClass...");
        // Delete the mailbox if already exists
        try {
            MailboxHelper.deleteMailbox(EMAIL);
            CosHelper.deleteCos(COSID);
        } catch (Exception e) {
            try{
                CosHelper.deleteCos(COSID);
            }finally{}
        }
    }
    
    public static void updateCredentials(String counters) {
        System.out.println("updateCredentials...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);

        inputParams.put(MailboxProperty.lastSuccessfulLoginDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastSuccessfulLoginDate.name()).add(
                "2013-02-27T07:32:33Z");
        
        inputParams.put(MailboxProperty.lastLoginAttemptDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastLoginAttemptDate.name()).add(
                "2013-02-27T07:32:33Z");
        
        inputParams.put(MailboxProperty.failedLoginAttempts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.failedLoginAttempts.name()).add(counters);
        
        inputParams.put(MailboxProperty.failedCaptchaLoginAttempts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.failedCaptchaLoginAttempts.name()).add(counters);
        
        inputParams.put(MailboxProperty.maxFailedCaptchaLoginAttempts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxFailedCaptchaLoginAttempts.name()).add("3");
        
        try {
            credentialService.update(inputParams);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    
    @Test
    public void testInValidCaptchaExceededAttempts() {
        System.out.println("\n\ntestInValidCaptchaExceededAttempts...");
        updateCredentials("3");
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("Before Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(ISVALID_KEY, new ArrayList<String>());
        inputParams.get(ISVALID_KEY).add(ISVALID_FALSE);

        try {
            System.out.println("Validating Captcha with false...");
            captchaService.validateCaptcha(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("After Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
    

    
    @Test
    public void testInValidCaptchaAllowedAttempts() {
        System.out.println("\n\ntestInValidCaptchaAllowedAttempts...");
        updateCredentials("0");
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("Before Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(ISVALID_KEY, new ArrayList<String>());
        inputParams.get(ISVALID_KEY).add(ISVALID_FALSE);

        try {
            System.out.println("Validating Captcha with false...");
            captchaService.validateCaptcha(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("After Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
    
    @Test
    public void testInValidCaptchaAllowedAttemptsWithNull() {
        System.out.println("\n\ntestInValidCaptchaAllowedAttemptsWithNull...");
        updateCredentials("");
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("Before Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(ISVALID_KEY, new ArrayList<String>());
        inputParams.get(ISVALID_KEY).add(ISVALID_FALSE);

        try {
            System.out.println("Validating Captcha with false...");
            captchaService.validateCaptcha(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("After Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
    
    @Test
    public void testValidCaptcha() {
        System.out.println("\n\ntestValidCaptcha...");
        updateCredentials("2");
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("Before Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(ISVALID_KEY, new ArrayList<String>());
        inputParams.get(ISVALID_KEY).add(ISVALID_TRUE);

        try {
            System.out.println("Validating Captcha with true...");
            captchaService.validateCaptcha(inputParams);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("After Validating Captcha...");
        try {
            Credentials cred = credentialService.read(inputParams);
            System.out.println("LastLoginAttemptDate "+cred.getLastLoginAttemptDate());
            System.out.println("LastSuccessfulLoginDate "+cred.getLastSuccessfulLoginDate());
            System.out.println("FailedLoginAttempts "+cred.getFailedLoginAttempts());
            System.out.println("FailedCaptchaLoginAttempts "+cred.getFailedCaptchaLoginAttempts());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
    
}
