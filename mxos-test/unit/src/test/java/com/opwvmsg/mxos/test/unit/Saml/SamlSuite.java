package com.opwvmsg.mxos.test.unit.Saml;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to sank 
@SuiteClasses({
    SamlCreateAuthenticationRequestTest.class,
    SamlDecodeAssertionTest.class
})
public class SamlSuite {
}
