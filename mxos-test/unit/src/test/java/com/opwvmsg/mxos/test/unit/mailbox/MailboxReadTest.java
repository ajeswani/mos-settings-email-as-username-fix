package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailboxReadTest {
    private static long MAILBOX_ID;
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String ALIAS_KEY = MailboxProperty.emailAlias.name();
    private static final String EMAIL = "mx100005@openwave.com";
    private static final String EMAIL1 = "mx100006@openwave.com";
    private static final String EMAIL2 = "mx100007@openwave.com";
    private static final String ALIAS = "mx100007alias@openwave.com";
    private static final String PWD = "test1";
    private static final String COSID = "ut_cos1_2.0";
    private static final String COSID1 = "ut_cos2_2.0";
    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static IMailboxService mailboxesService;
    private static IEmailAliasService emailAliasService;
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
    String maxAllowedDomain = "20";
    String firstName="Foooo";
    String lastName="Barrrrrr";   
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MailboxReadTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        mailboxesService = (IMailboxService) context
        .getService(ServiceEnum.MailboxesService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        emailAliasService = (IEmailAliasService) context
                .getService(ServiceEnum.MailAliasService.name());
        assertNotNull("EmailAliasService object is null.", emailAliasService);
        CosHelper.createCos(COSID);
        MAILBOX_ID = MailboxHelper.createMailbox(EMAIL, PWD, COSID);
        MailboxHelper.createMailbox(EMAIL1, PWD, COSID);
        CosHelper.createCos(COSID1);
        MailboxHelper.createMailbox(EMAIL2, PWD, COSID1);
        assertTrue("Mailbox NOT Created", (MAILBOX_ID > 0));
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("MailboxReadTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("MailboxReadTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MailboxReadTest.tearDownAfterClass...");
        //inputParams.clear();
        // Delete the mailbox if already exists
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            fail();
        }
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL1);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            fail();
        }
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            fail();
        }
        CosHelper.deleteCos(COSID);
        CosHelper.deleteCos(COSID1);
        inputParams = null;
        mailboxService = null;
    }

    @Test
    public void testGetMailbox() throws Exception {
        System.out.println("MailboxReadTest.testGetMailboxBase...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        Mailbox mailbox = null;
        try {
            mailbox = mailboxService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Mailbox object is null.", mailbox);
        System.out.println("Mailbox = " + mailbox);
    }

    @Test
    public void testGetMailboxWithAlias() throws Exception {
        System.out.println("MailboxReadTest.testGetMailboxWithAlias...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(ALIAS_KEY, new ArrayList<String>());
        inputParams.get(ALIAS_KEY).add(ALIAS);
        try {
            emailAliasService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }

        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(ALIAS);
        Mailbox mailbox = null;
        try {
            mailbox = mailboxService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Mailbox object is null.", mailbox);
        System.out.println("Mailbox = " + mailbox);
    }
    
    @Test
    public void testGetMailboxes() throws Exception {
        System.out.println("MailboxReadTest.testGetMailboxes...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        System.out.println("MailBox ID 1 : " +EMAIL);
        inputParams.get(EMAIL_KEY).add(EMAIL);
        System.out.println("MailBox ID 2 : " +EMAIL);
        inputParams.get(EMAIL_KEY).add(EMAIL1);
        System.out.println("MailBox ID 3 : " +EMAIL);
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        List<Mailbox> mailboxes = null;
        try {
            mailboxes = mailboxesService.list(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Mailbox object is null.", mailboxes);
        if(mailboxes.size()>0){
            System.out.println("MailBox ID 1 : " + mailboxes.get(0).getBase().getEmail());
        }
        if(mailboxes.size()>1){
            System.out.println("MailBox ID 2 : " + mailboxes.get(1).getBase().getEmail());
        }
        if(mailboxes.size()>2){
            System.out.println("MailBox ID 3 : " + mailboxes.get(2).getBase().getEmail());
        }
        System.out.println("No. of Mailboxes = " + mailboxes.size());
    }
    

    @Test
    public void testGetMailboxWithoutEmail() throws Exception {
        System.out.println("MailboxReadTest.testGetMailboxBaseWithoutEmail...");
        inputParams.clear();
        Mailbox mailbox = null;
        try {
            mailbox = mailboxService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_GET_MISSING_PARAMS",
                    e.getCode().equalsIgnoreCase(
                            ErrorCode.GEN_BAD_REQUEST.name()));
        }
        assertNull(mailbox);
    }

    @Test
    public void testGetMailboxInvalidEmail() throws Exception {
        System.out.println("MailboxReadTest.testGetMailboxeInvalidEmail...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add("temp.comskdjf");
        Mailbox mailbox = null;
        try {
            mailbox = mailboxService.read(inputParams);
            fail();
        } catch (MxOSException e) {
            e.printStackTrace();
        }
        assertNull(mailbox);
    }
    
}