package com.opwvmsg.mxos.test.unit.gp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IGeneralPreferenceService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class GeneralPreferenceAPITest {
    private static IGeneralPreferenceService gp;
    private static long MAILBOX_ID;
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "generalpreference@test.com";
    private static final String PWD_KEY = MailboxProperty.password.name();
    private static final String PWD = "test";
    private static final String COSID = "ut_default";
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GeneralPreferenceAPITest....setUpBeforeClass...");
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        gp = (IGeneralPreferenceService) ContextUtils.loadContext()
                .getService(ServiceEnum.GeneralPreferenceService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        assertNotNull("GeneralPreferenceService object is null.", gp);

        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PWD_KEY, new ArrayList<String>());
        assertNotNull("inputParams map is empty.", inputParams.get(EMAIL_KEY));
        assertNotNull("inputParams map is empty.", inputParams.get(PWD_KEY));
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.get(PWD_KEY).add(PWD);
        try {
            CosHelper.createCos(COSID);
            MailboxHelper.createMailbox(EMAIL,PWD,COSID);
            //MAILBOX_ID = mailboxService.create(inputParams);
            //assertTrue("Mailbox Created", (MAILBOX_ID > 0));
        } catch (Exception e) {
            System.out.println("Mailbox Already created");
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GeneralPreferenceAPITest...tearDownAfterClass");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            mailboxService.delete(inputParams);
            MailboxHelper.deleteMailbox(EMAIL);
            CosHelper.deleteCos(COSID);
        } catch (MxOSException e) {
            System.out.println("Deletion Failed Ignoring");
            assertFalse("Deleting Failed", true);
        } catch (Exception e) {
            fail();
        }
        inputParams = null;
    }

    @Test
    public void testGetGeneralPreferences() {
        System.out.println("Starting testGetGeneralPreferences");
        try {
            inputParams.clear();
            List<String> email = new ArrayList<String>();
            email.add(EMAIL);
            inputParams.put("email", email);
            GeneralPreferences result = gp.read(inputParams);
            assertNotNull(result);
            System.out.println("GET API: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testUpdateLocale() {
        System.out.println("Starting testUpdateLocale");
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add(Locale.CHINESE.toString());
            inputParams.put(MailboxProperty.locale.name(), p);
            gp.update(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testUpdateLocaleNeg() {
        System.out.println("Starting testUpdateLocaleNeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_LOCALE.name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("foreign");
            inputParams.put(MailboxProperty.locale.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Unknown Expected Occured", false);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testUpdateTimeZone() {
        System.out.println("Starting testUpdateTimeZone");
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("PST8PDT");
            inputParams.put(MailboxProperty.timezone.name(), p);
            gp.update(inputParams);
            inputParams = new HashMap<String, List<String>>();
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            GeneralPreferences result = gp.read(inputParams);
            System.out.println("testUpdateTimeZone: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
            fail();
        }
    }

    @Test
    public void testUpdateTimeZoneNeg() {
        System.out.println("Starting testUpdateTimeZoneNeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_TIMEZONE.name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("Unknown");
            inputParams.put(MailboxProperty.timezone.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testUpdateCharset() {
        System.out.println("Starting testUpdateCharset");
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("UTF-8");
            inputParams.put(MailboxProperty.charset.name(), p);
            gp.update(inputParams);
            inputParams = new HashMap<String, List<String>>();
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            GeneralPreferences result = gp.read(inputParams);
            System.out.println("testUpdateCharset: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testUpdateCharsetNeg() {
        System.out.println("Starting testUpdateCharsetNeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_CHARSET.name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("xyz");
            inputParams.put(MailboxProperty.charset.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testUpdateParentalControl() {
        System.out.println("Starting testUpdateParentalControl");
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("yes");
            inputParams.put(MailboxProperty.parentalControlEnabled.name(), p);
            gp.update(inputParams);
            inputParams = new HashMap<String, List<String>>();
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            GeneralPreferences result = gp.read(inputParams);
            System.out.println("testUpdateParentalControl: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testUpdateParentalControlNeg() {
        System.out.println("Starting testUpdateParentalControlNeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_PARENTALCONTROL
                .name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("ParentalContol-No");
            inputParams.put(MailboxProperty.parentalControlEnabled.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testUpdateParentalControlNull() {
        System.out.println("Starting testUpdateParentalControlNeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_PARENTALCONTROL
                .name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add(null);
            inputParams.put(MailboxProperty.parentalControlEnabled.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testRecycleBinEnabled() {
        System.out.println("Starting testRecycleBinEnabled");
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("no");
            inputParams.put(MailboxProperty.recycleBinEnabled.name(), p);
            gp.update(inputParams);
            inputParams = new HashMap<String, List<String>>();
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            GeneralPreferences result = gp.read(inputParams);
            System.out.println("testRecycleBinEnabled: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testRecycleBinEnabledNeg() {
        System.out.println("Starting testRecycleBinEnabledNeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_RECYCLEBIN_ENABLED
                .name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("RecycleBin-No");
            inputParams.put(MailboxProperty.recycleBinEnabled.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }

    @Test
    public void testPreferredTheme() {
        System.out.println("Starting testPreferredTheme");
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("DummyTheme");
            inputParams.put(MailboxProperty.preferredTheme.name(), p);
            gp.update(inputParams);
            inputParams = new HashMap<String, List<String>>();
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            GeneralPreferences result = gp.read(inputParams);
            System.out.println("testPreferredTheme: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    // As per mailbox mxos rules for Preferred theme empty/null is allowed too so commenting this UT.
    /* @Test
    public void testPreferredThemeNeg() {
        System.out.println("Starting testPreferredThemeNeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_PREFERRED_THEME
                .name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add(null);
            inputParams.put(MailboxProperty.preferredTheme.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    } */

    
    @Test
    public void testWebMailMTA() {
        System.out.println("Starting testWebMailMTA");
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add("rwcvmx83c0105");
            inputParams.put(MailboxProperty.webmailMTA.name(), p);
            gp.update(inputParams);
            inputParams = new HashMap<String, List<String>>();
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            GeneralPreferences result = gp.read(inputParams);
            System.out.println("testWebMailMTA: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    } 

    // As per mailbox mxos rules for WebMailMTA empty/null is allowed too so commenting this UT.
    /*@Test
    public void testWebMailMTANeg() {
        System.out.println("Starting testWebMailMTANeg");
        String expectedErrorCode = MailboxError.MBX_INVALID_WEBMAILMTA.name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add(null);
            inputParams.put(MailboxProperty.webmailMTA.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    } */

    @Test
    public void testPreferredUserExperience() {
        System.out.println("Starting testPreferredUserExperience");
        inputParams.clear();
        List<String> p = new ArrayList<String>();
        p.add(EMAIL);
        inputParams.put(MailboxProperty.email.name(), p);
        p = new ArrayList<String>();
        p.add("basic");
        inputParams.put(MailboxProperty.preferredUserExperience.name(), p);
        try {
            gp.update(inputParams);
            inputParams.clear();
            List<String> p2 = new ArrayList<String>();
            p2.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p2);
            GeneralPreferences result = gp.read(inputParams);
            System.out.println("preferredUserExperience: " + result);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testPreferredUserExperienceNeg() {
        System.out.println("Starting testPreferredUserExperienceNeg");
        String expectedErrorCode = MailboxError.
        MBX_INVALID_PREFERRED_USER_EXPERIENCE.name();
        try {
            inputParams.clear();
            List<String> p = new ArrayList<String>();
            p.add(EMAIL);
            inputParams.put(MailboxProperty.email.name(), p);
            p = new ArrayList<String>();
            p.add(null);
            inputParams.put(MailboxProperty.preferredUserExperience.name(), p);
            gp.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Got Expected Error code", expectedErrorCode,
                    me.getCode());
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Exception message: " + e.fillInStackTrace());
        }
    }
}
