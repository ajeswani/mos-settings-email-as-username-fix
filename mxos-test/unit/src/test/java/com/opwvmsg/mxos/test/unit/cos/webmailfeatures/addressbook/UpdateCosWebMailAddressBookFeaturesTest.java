package com.opwvmsg.mxos.test.unit.cos.webmailfeatures.addressbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosWebMailFeaturesAddressBookService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateCosWebMailAddressBookFeaturesTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosWebMailFeaturesAddressBookService wmfsab;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateCosWebMailAddressBookFeaturesTest.setUpBeforeClass...");
        wmfsab = (ICosWebMailFeaturesAddressBookService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosWebMailFeaturesAddressBookService.name());
        boolean flag = CosHelper.createCos(COS_ID);
        assertTrue("Cos was not created.", flag);
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.setUp...");
        assertNotNull("CosWebMailFeaturesAddressBookService object is null.", wmfsab);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateCosWebMailAddressBookFeaturesTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateCosWebMailAddressBookFeaturesTest.tearDownAfterClass...");
        CosHelper.deleteCos(COS_ID);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        wmfsab = null;
    }

    private static AddressBookFeatures getCosWebMailFeaturesParams() {
        AddressBookFeatures ab = null;
        try {
            ab = wmfsab.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(COSID_KEY)
                    || getParams.get(COSID_KEY).isEmpty()) {
                getParams.put(COSID_KEY, new ArrayList<String>());
                getParams.get(COSID_KEY).add(COS_ID);
            }
        }
        assertNotNull("WebMailFeaturesAddressBook object is null.", ab);
        return ab;
    }

    private static void updateCosWebMailFeaturesAddressBookParams(
            Map<String, List<String>> updateParams) {
        updateCosWebMailFeaturesAddressBookParams(updateParams, null);
    }

    private static void updateCosWebMailFeaturesAddressBookParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            wmfsab.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(COSID_KEY)
                    || updateParams.get(COSID_KEY).isEmpty()) {
                updateParams.put(COSID_KEY, new ArrayList<String>());
                updateParams.get(COSID_KEY).add(COS_ID);
            }
        }
    }

    @Test
    public void testSetMaxContactsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxContactsSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxContacts.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("201");
        updateCosWebMailFeaturesAddressBookParams(updateParams);
        int maxContacts = getCosWebMailFeaturesParams()
                .getMaxContacts();
        System.out.println("Max Contacts " +maxContacts);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetMaxGroupsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxGroupsSuccess..");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxGroups.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("51");
        updateCosWebMailFeaturesAddressBookParams(updateParams);
        int maxGroups = getCosWebMailFeaturesParams()
                .getMaxGroups();
        System.out.println("Max Groups " +maxGroups);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetMaxContactsPerGroupsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetMaxContactsPerGroupsSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxContactsPerGroup.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("21");
        updateCosWebMailFeaturesAddressBookParams(updateParams);
        int maxContactsPG = getCosWebMailFeaturesParams()
                .getMaxContactsPerGroup();
        System.out.println("Max ContactsPerGroups " +maxContactsPG);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetMaxContactsPerPageSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetMaxContactsPerPageSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxContactsPerPage.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("16");
        updateCosWebMailFeaturesAddressBookParams(updateParams);
        int maxContactsPP = getCosWebMailFeaturesParams()
                .getMaxContactsPerPage();
        System.out.println("Max ContactsPerPage " +maxContactsPP);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetCreateContactsFromOutgoingEmailsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetCreateContactsFromOutgoingEmailsSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.createContactsFromOutgoingEmails.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("disabled");
        updateCosWebMailFeaturesAddressBookParams(updateParams);
        String contFromOutgoingEmails = getCosWebMailFeaturesParams()
                .getCreateContactsFromOutgoingEmails().toString();
        System.out.println("CreateContactsFromOutgoingEmails " +contFromOutgoingEmails);
        updateParams.remove(key);
    }
    
    
    @Test
    public void testSetMaxContactsFailure() throws Exception {
        String wrong_value = "null";
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxContactsFailure...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxContacts.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(wrong_value);
        try {
            wmfsab.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
        updateParams.remove(key);
    }
    
    @Test
    public void testSetMaxGroupsFailure() throws Exception {
        String wrong_value = "null";
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxGroupsFailure..");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxGroups.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(wrong_value);
        try {
            wmfsab.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
        updateParams.remove(key);
    }
    
    @Test
    public void testSetMaxContactsPerGroupsFailure() throws Exception {
        String wrong_value = "null";
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxContactsPerGroupsFailure...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxContactsPerGroup.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(wrong_value);
        try {
            wmfsab.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
        updateParams.remove(key);
    }
    
    @Test
    public void testSetMaxContactsPerPageFailure() throws Exception {
        String wrong_value = "null";
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxContactsPerPageFailure...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxContactsPerPage.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(wrong_value);
        try {
            wmfsab.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
        updateParams.remove(key);
    }
    
    @Test
    public void testSetCreateContactsFromOutgoingEmailsFailure() throws Exception {
        String wrong_value = "wrong_value";
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetCreateContactsFromOutgoingEmailsFailure...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.createContactsFromOutgoingEmails.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(wrong_value);
        try {
            wmfsab.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
        updateParams.remove(key);
    }
    
}
