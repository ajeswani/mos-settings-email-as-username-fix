package com.opwvmsg.mxos.test.unit.addressbook.groups.members;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsMembersService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GroupsMembersGET {

    private static final String TEST_NAME = "GroupsMembersGET";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long GROUPID;
    private static long MEMBERID;

    private static IGroupsMembersService groupsMembersService;
    private static IExternalLoginService externalLoginService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static ExternalSession session = null;

    private static List<Member> getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static List<Member> getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        List<Member> memberList = null;
        try {
            memberList = groupsMembersService.readAll(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("groupsMembersService object is null.",
                        groupsMembersService);
            }
        } catch (MxOSException e) {
            assertNotNull("MxOSError is null", e);
        }
        return memberList;
    }

    private static Member getParamsSingle(Map<String, List<String>> params) {
        return getParamsSingle(params, null);
    }

    private static Member getParamsSingle(Map<String, List<String>> params,
            AddressBookException expectedError) {
        Member member = null;
        try {
            member = groupsMembersService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("groupsMembersService object is null.",
                        groupsMembersService);
            }
        } catch (MxOSException e) {
            assertNotNull("MxOSError is null", e);
        }
        return member;
    }

    private static void createMember() {
        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));

        params.put(AddressBookProperty.memberId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.memberId.name()).add(
                String.valueOf(MEMBERID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        try {
            groupsMembersService.create(params);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        groupsMembersService = (IGroupsMembersService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.GroupsMemberService.name());
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        login(USERID, PASSWORD);
        
        GROUPID = AddressBookHelper.createGroup(USERID, session);
        MEMBERID = AddressBookHelper.createContact(USERID, session);
        createMember();
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        groupsMembersService = null;
        externalLoginService = null;
        AddressBookHelper.deleteGroup(USERID, GROUPID, session);
        AddressBookHelper.deleteContact(USERID, MEMBERID, session);
    }

    // base get
    @Test
    public void testGroupsMembers() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsMembers");

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        List<Member> memberList = getParams(params);
        assertNotNull("memberList is null", memberList);

        for (Member mem : memberList) {
            assertNotNull("MemberId Is null", mem.getMemberId());
        }

    }

    // base get
    @Test
    public void testGroupsMembersSingle() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsMembersSingle");

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));

        params.put(AddressBookProperty.memberId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.memberId.name()).add(
                String.valueOf(MEMBERID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        Member member = getParamsSingle(params);
        assertNotNull("member is null", member);
        assertNotNull("MemberId Is null", member.getMemberId());

    }
}
