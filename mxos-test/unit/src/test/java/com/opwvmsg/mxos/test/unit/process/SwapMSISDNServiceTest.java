package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnSwapService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SwapMSISDNServiceTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "PassW0rD";
    private static final String CURRENT_MSISDN = "+71262383494";
    private static final String NEW_MSISDN = "+49438326217";
    private static IMsisdnSwapService service;
    private static IMailboxBaseService base;
    private static ICredentialService credentials;
    private static ISmsServicesService smsService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("SwapMSISDNServiceTest.setUpBeforeClass...");
        service = (IMsisdnSwapService) ContextUtils.loadContext()
                .getService(ServiceEnum.MsisdnSwapService.name());
        base = (IMailboxBaseService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxBaseService.name());
        credentials = (ICredentialService) ContextUtils.loadContext()
                .getService(ServiceEnum.CredentialService.name());
        smsService = (ISmsServicesService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsServicesService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("SwapMSISDNServiceTest.setUp...");
        assertNotNull("MailBoxBaseService object is null.", base);
        assertNotNull("CredentialsService object is null.", credentials);
        assertNotNull("SMSServicesService object is null.", smsService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("SwapMSISDNServiceTest.tearDown...");
        params.clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("SwapMSISDNServiceTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        base = null;
        credentials = null;
        smsService = null;
    }

    private static void swapMSISDN(Map<String, List<String>> updateParams) {
        swapMSISDN(updateParams, null);
    }

    private static void swapMSISDN(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.process(updateParams);
            if (null != expectedError) {
                fail("1. This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("2. This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        }
    }

    private static void setupBase(String msisdn, String status) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add(status);
        inputParams.put(MailboxProperty.msisdn.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.msisdn.name()).add(msisdn);
        inputParams.put(MailboxProperty.msisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.msisdnStatus.name()).add(
                MsisdnStatus.ACTIVATED.name());
        try {
            base.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static Base getBase() {
        Base baseObject = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            baseObject = base.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Base object is null.", baseObject);
        return baseObject;
    }

    private static void setupCredentials(String msisdn, String pwdPref,
            String pwdRecoveryEmail) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.passwordRecoveryPreference.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryPreference.name()).add(
                pwdPref);
        if (null != pwdRecoveryEmail) {
            inputParams.put(MailboxProperty.passwordRecoveryEmail.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.passwordRecoveryEmail.name()).add(
                    pwdRecoveryEmail);
            inputParams.put(MailboxProperty.passwordRecoveryEmailStatus.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.passwordRecoveryEmailStatus.name())
                    .add(MxosEnums.MsisdnStatus.ACTIVATED.name());
        }
        inputParams.put(MailboxProperty.passwordRecoveryMsisdn.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryMsisdn.name()).add(
                msisdn);
        inputParams.put(MailboxProperty.passwordRecoveryMsisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryMsisdnStatus.name())
                .add(MsisdnStatus.ACTIVATED.name());
        try {
            credentials.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static Credentials getCredentials() {
        Credentials creds = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            creds = credentials.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Credentials object is null.", creds);
        return creds;
    }

    private static void setupSMSFeatures(String msisdn) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);

        inputParams.put(MailboxProperty.smsServicesMsisdn.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsServicesMsisdn.name()).add(msisdn);
        inputParams.put(MailboxProperty.smsServicesMsisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsServicesMsisdnStatus.name()).add(
                MsisdnStatus.ACTIVATED.name());
        try {
            smsService.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        inputParams.remove(MailboxProperty.smsServicesMsisdnStatus.name());
    }

    private static SmsServices getSmsServices() {
        SmsServices smsServiceObject = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            smsServiceObject = smsService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("SmsServices object is null.", smsServiceObject);
        return smsServiceObject;
    }

    @Test
    public void testSwapMSISDNWithoutAnyMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithoutAnyMSISDN...");
        swapMSISDN(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testSwapMSISDNWithoutNewMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithoutNewMSISDN...");
        String key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        System.out.println("################# " + params);
        swapMSISDN(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(key);
    }

    @Test
    public void testSwapMSISDNWithoutCurrentMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithoutCurrentMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        swapMSISDN(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(key);
    }

    @Test
    public void testSwapMSISDNWithNullCurrentMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithNullCurrentMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(null);
        swapMSISDN(params, ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithNullNewMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithNullNewMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(null);
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        swapMSISDN(params, ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithEmptyCurrentMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithEmptyCurrentMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        swapMSISDN(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithEmptyNewMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithEmptyNewMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        swapMSISDN(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithJunkCurrentMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithJunkCurrentMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("SomethingJunk");
        swapMSISDN(params, "301");
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithJunkNewMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithJunkNewMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("SomethingJunk");
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        swapMSISDN(params, "302");
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithSplCharsInCurrentMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithSplCharsInCurrentMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*()_+-=");
        swapMSISDN(params, "301");
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithSplCharsInNewMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithSplCharsInNewMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*()_+-=");
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        swapMSISDN(params, "302");
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithInvalidCurrentMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithInvalidCurrentMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("+9198800123450");
        swapMSISDN(params, "301");
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithInvalidNewMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithInvalidNewMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("+9198800123450");
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        swapMSISDN(params, "302");
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNWithNonExistingCurrentMSISDN() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNWithNonExistingCurrentMSISDN...");
        String key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("+91999999999");
        swapMSISDN(params, MailboxError.MBX_UNABLE_TO_SEARCH.name());
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNForProximusAccount() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNForProximusAccount...");
        setupBase(CURRENT_MSISDN, MxosEnums.Status.OPEN.name());
        String key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        swapMSISDN(params);
        Base base = getBase();
        String msisdn = base.getMsisdn();
        assertNotNull("Msisdn is null.", msisdn);
        assertTrue("Msisdn was not swapped.", NEW_MSISDN.equals(msisdn));
        setupBase(NEW_MSISDN, MxosEnums.Status.OPEN.name());
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNForPwdRecovery() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNForPwdRecovery...");
        setupCredentials(CURRENT_MSISDN,
                MxosEnums.PasswordRecoveryPreference.EMAIL.name(),
                "xx123456@openwave.com");
        String key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        swapMSISDN(params);
        Credentials creds = getCredentials();
        String msisdn = creds.getPasswordRecoveryMsisdn();
        assertNotNull("Msisdn is null.", msisdn);
        assertTrue("Msisdn was not swapped.", NEW_MSISDN.equals(msisdn));
        setupCredentials(NEW_MSISDN,
                MxosEnums.PasswordRecoveryPreference.EMAIL.name(),
                "xx123456@openwave.com");
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }

    @Test
    public void testSwapMSISDNForSMSServices() throws Exception {
        System.out
                .println("SwapMSISDNServiceTest.testSwapMSISDNForSMSServices...");
        setupSMSFeatures(CURRENT_MSISDN);
        String key = MailboxProperty.currentMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(CURRENT_MSISDN);
        key = MailboxProperty.newMSISDN.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(NEW_MSISDN);
        swapMSISDN(params);
        SmsServices smsServices = getSmsServices();
        String msisdn = smsServices.getSmsServicesMsisdn();
        assertNotNull("Msisdn is null.", msisdn);
        assertTrue("Msisdn was not swapped.", NEW_MSISDN.equals(msisdn));
        setupSMSFeatures(NEW_MSISDN);
        params.remove(MailboxProperty.newMSISDN.name());
        params.remove(MailboxProperty.currentMSISDN.name());
    }
}
