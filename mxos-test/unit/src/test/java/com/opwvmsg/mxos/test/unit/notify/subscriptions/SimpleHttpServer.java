package com.opwvmsg.mxos.test.unit.notify.subscriptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class SimpleHttpServer {

    private static final String OK = "/200";
    private static final String NOT_FOUND = "/404";
    private static final String GONE =  "/410";
    public static final int PORT = 8071;
    
    private static final Map<String, Integer> SC = new HashMap<String, Integer>();
    static{
        SC.put(OK, 200);
        SC.put(GONE, 410);
        SC.put(NOT_FOUND, 404);
    }
    
    public static void start() throws IOException{
        HttpServer server = HttpServer.create(new InetSocketAddress(8071), 0);
        server.createContext(OK, new Handler());
        server.createContext(NOT_FOUND, new Handler());
        server.createContext(GONE, new Handler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }
    
    private static class Handler implements HttpHandler{

        @Override
        public void handle(HttpExchange arg) throws IOException {

            System.out.println("SimpleHttpServer - Reveived: Header="
                    + arg.getRequestHeaders().entrySet());

            BufferedReader brb = new BufferedReader(new InputStreamReader(
                    arg.getRequestBody()));
            System.out.println("SimpleHttpServer - Reveived: Body=" + brb.readLine());

            String response = "SimpleHttpServer response.";
            arg.sendResponseHeaders(SC.get(arg.getRequestURI().toString()),
                    response.length());
            OutputStream os = arg.getResponseBody();
            os.write(response.getBytes());
            os.close();
            os.flush();
        }
        
    }
}
