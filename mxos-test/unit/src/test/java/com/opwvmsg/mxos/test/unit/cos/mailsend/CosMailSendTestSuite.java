package com.opwvmsg.mxos.test.unit.cos.mailsend;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailSend Cos 
@SuiteClasses({
    CosMailSendPOST.class,
    CosMailSendGET.class
})
public class CosMailSendTestSuite {
}
