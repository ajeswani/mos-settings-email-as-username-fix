package com.opwvmsg.mxos.test.unit.internalinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.EventRecordingType;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMessageEventRecordsService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetMessageEventRecordsTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "test.user.1234@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IMessageEventRecordsService info;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetMessageEventRecordsTest.setUpBeforeClass...");
        info = (IMessageEventRecordsService) ContextUtils.loadContext()
                .getService(ServiceEnum.MessageEventRecordsService.name());
        long id = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("Mailbox was not created.", id != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetMessageEventRecordsTest.setUp...");
        assertNotNull("InternalInfoService object is null.", info);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetMessageEventRecordsTest.tearDown...");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetMessageEventRecordsTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        info = null;
    }

    private static MessageEventRecords getMsgEventRecordsParams(
            Map<String, List<String>> params) {
        return getMsgEventRecordsParams(params, null);
    }

    private static MessageEventRecords getMsgEventRecordsParams(
            Map<String, List<String>> params, String expectedError) {
        MessageEventRecords mer = null;
        try {
            mer = info.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MessageEventRecords object is null.", mer);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
            }
        }
        return mer;
    }

    @Test
    public void testGetInternalInfoWithoutEmail() {
        System.out
                .println("GetMessageEventRecordsTest.testGetInternalInfoWithoutEmail...");
        // Clear the params.
        params.clear();
        getMsgEventRecordsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetInternalInfoWithNullEmail() {
        System.out
                .println("GetMessageEventRecordsTest.testGetInternalInfoWithNullEmail...");
        // Remove the email from the map to have a null email.
        params.get(EMAIL_KEY).clear();
        getMsgEventRecordsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetInternalInfoWithNonExistingEmail() {
        System.out
                .println("GetMessageEventRecordsTest.testGetInternalInfoWithNonExistingEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("something.junk@foobar.com");
        getMsgEventRecordsParams(params, MailboxError.MBX_NOT_FOUND.name());
    }

    @Test
    public void testGetInternalInfoWithSplCharsInEmail() {
        System.out
                .println("GetMessageEventRecordsTest.testGetInternalInfoWithSplCharsInEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("!#$%^&*()+-=.junk@foobar.com");
        getMsgEventRecordsParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testGetInternalInfoWithSplCharsInDomain() {
        System.out
                .println("GetMessageEventRecordsTest.testGetInternalInfoWithSplCharsInDomain...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("junk@foo!#+()$&*^-bar.com");
        getMsgEventRecordsParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testEventRecordingEnabledSuccess() throws Exception {
        System.out.println("GetMessageEventRecordsTest.testEventRecordingEnabledSuccess...");
        EventRecordingType ert = getMsgEventRecordsParams(params).getEventRecordingEnabled();
        assertNotNull("EventRecordingEnabled is null.", ert);
        assertTrue("EventRecordingEnabled has a wrong value.",
                ert.equals(EventRecordingType.NEVER));
    }

    @Test
    public void testMaxHeaderLengthSuccess() throws Exception {
        System.out.println("GetMessageEventRecordsTest.testMaxHeaderLengthSuccess...");
        Integer headerLength = getMsgEventRecordsParams(params).getMaxHeaderLength();
        assertNotNull("MaxHeaderLength is null.", headerLength);
        assertTrue("MaxHeaderLength has a wrong value.", headerLength == 0);
    }

    @Test
    public void testMaxFilesSizeKBSuccess() throws Exception {
        System.out.println("GetMessageEventRecordsTest.testMaxFilesSizeKBSuccess...");
        Integer filesSize = getMsgEventRecordsParams(params).getMaxFilesSizeKB();
        assertNotNull("MaxFilesSizeKB is null.", filesSize);
        assertTrue("MaxFilesSizeKB has a wrong value.", filesSize == 0);
    }

}
