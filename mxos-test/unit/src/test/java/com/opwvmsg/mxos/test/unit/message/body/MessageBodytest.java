package com.opwvmsg.mxos.test.unit.message.body;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IBodyService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MessageBodytest {

    private static final String TEST_NAME = "MessageCreateTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "message_createto@openwave.com";
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test5";
    private static final String COSID = "ut_default1";
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "message_createfrom@openwave.com";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, test mail from MessageCreateTest Program";

    private static final String MESSAGE_ID_KEY = MessageProperty.messageId.name();
    
    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static final String ISADMIN_KEY = MessageProperty.isAdmin.name();
    private static final String MAILMESSAGESTORE_KEY = MailboxProperty.messageStoreHost
            .name();
    private static final String MAILBOXID_KEY = LDAPMailboxProperty.mailboxid
            .name();
    private static final String EXPIRETIME_KEY = MessageProperty.expireTime.name();
    private static final String KEYWORDS_KEY = MessageProperty.keywords.name();
    private static final String FLAGPRIV_KEY = MessageProperty.flagPriv.name();
    private static final String FLAGSEEN_KEY = MessageProperty.flagSeen.name();
    private static final String FLAGANS_KEY = MessageProperty.flagAns.name();
    private static final String FLAGFLAGGED_KEY = MessageProperty.flagFlagged
            .name();
    private static final String FLAGDELETED_KEY = MessageProperty.flagDel
            .name();
    private static final String FLAGRES_KEY = MessageProperty.flagRes.name();
    private static final String COS_ID = "default";
    
    private static final String MESSAGE_UID = MessageProperty.uid.name();

    private static IMxOSContext context;
    private static IBodyService bodyService;
    private static IMessageService messageService;
    private static IMailboxService mailboxService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        //createMailBox(FROMADDRESS, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(FROMADDRESS, PASSWORD, COSID);
        bodyService = (IBodyService) context
                .getService(ServiceEnum.BodyService.name());
        messageService = (IMessageService) context
        .getService(ServiceEnum.MessageService.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", messageService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        //deleteMailBox(FROMADDRESS);
        MailboxHelper.deleteMailbox(EMAIL,true);
        MailboxHelper.deleteMailbox(FROMADDRESS);
        CosHelper.deleteCos(COSID);
        params.clear();
        params = null;
        messageService = null;
        mailboxService = null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static void createMailBox(String email, String passwd) {
        Long id = MailboxHelper.createMailbox(email, passwd, COS_ID);
        if (id == -1L)
            fail("Failed to create Mailbox");
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testSuccesswithFlagResFalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithFlagResFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGRES_KEY, "false");
            String uid = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, "messageId", uid);
            Body msg = bodyService.read(inputParams1);
            assertFalse(false);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
}
