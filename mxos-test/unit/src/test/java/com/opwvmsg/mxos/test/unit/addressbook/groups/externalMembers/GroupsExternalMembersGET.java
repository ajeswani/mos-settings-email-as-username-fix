package com.opwvmsg.mxos.test.unit.addressbook.groups.externalMembers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsExternalMembersService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GroupsExternalMembersGET {

    private static final String TEST_NAME = "GroupsExternalMembersGET";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long GROUPID;

    private static IGroupsExternalMembersService groupsExternalMembersService;
    private static IExternalLoginService externalLoginService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static ExternalSession session = null;

    private static void createExternalMembers() {
        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        params.put(AddressBookProperty.memberName.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.memberName.name()).add("DUMMY");
        params.put(AddressBookProperty.memberEmail.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.memberEmail.name()).add(
                "DUMMY@EMAIL.com");

        try {
            groupsExternalMembersService.create(params);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }

    private static List<ExternalMember> getParams(
            Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static List<ExternalMember> getParams(
            Map<String, List<String>> params, AddressBookException expectedError) {
        List<ExternalMember> memberList = null;
        try {
            memberList = groupsExternalMembersService.readAll(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("groupsMembersService object is null.",
                        groupsExternalMembersService);
            }
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
        }
        return memberList;
    }

    private static ExternalMember getParamsSingle(
            Map<String, List<String>> params) {
        return getParamsSingle(params, null);
    }

    private static ExternalMember getParamsSingle(
            Map<String, List<String>> params, AddressBookException expectedError) {
        ExternalMember externalMember = null;
        try {
            externalMember = groupsExternalMembersService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("groupsMembersService object is null.",
                        groupsExternalMembersService);
            }
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
        }
        return externalMember;
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        groupsExternalMembersService = (IGroupsExternalMembersService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.GroupsExternalMemberService.name());
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        login(USERID, PASSWORD);
        
        GROUPID = AddressBookHelper.createGroup(USERID, session);
        createExternalMembers();
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        groupsExternalMembersService = null;
        externalLoginService = null;
        AddressBookHelper.deleteGroup(USERID, GROUPID, session);
    }

    // base get
    @Test
    public void testGroupsMembers() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsMembers");

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        List<ExternalMember> memberList = getParams(params);
        assertNotNull("memberList is null", memberList);

        for (ExternalMember mem : memberList) {
            assertNotNull("MemberName Is null", mem.getMemberName());
        }

    }

    // base get
    @Test
    public void testGroupsMembersSingle() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsMembersSingle");

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        params.put(AddressBookProperty.memberName.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.memberName.name()).add("DUMMY");

        ExternalMember externalMember = getParamsSingle(params);
        assertNotNull("externalMember is null", externalMember);
        assertNotNull("MemberName Is null", externalMember.getMemberName());

    }
}
