package com.opwvmsg.mxos.test.unit.webmailfeatures;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to WebMailFeatures 
@SuiteClasses({
    GetWebMailFeaturesTest.class,
    UpdateWebMailFeaturesTest.class
})
public class WebMailFeaturesSuite {
}
