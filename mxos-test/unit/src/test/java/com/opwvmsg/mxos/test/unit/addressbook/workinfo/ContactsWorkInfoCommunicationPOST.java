package com.opwvmsg.mxos.test.unit.addressbook.workinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoCommunicationService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsWorkInfoCommunicationPOST {

    private static final String TEST_NAME = "ContactWorkInfoCommunicationPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsWorkInfoCommunicationService contactsWorkInfoCommunicationService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Map<String, List<String>> getBasicParams(
            Map<String, List<String>> params) {

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        return params;

    }

    private static Communication getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static Communication getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        Communication info = null;
        try {
            info = contactsWorkInfoCommunicationService.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("Work Info communication  object is null.", info);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
        return info;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            MxOSException expectedError) {

        try {
            contactsWorkInfoCommunicationService.update(params);

            Map<String, List<String>> inputParam = getBasicParams(params);

            Communication communication = getParams(inputParam);

            assertNotNull("Email Is null.", communication.getEmail());
            assertNotNull("Fax Is null.", communication.getFax());
            assertNotNull("Phone1 Is null.", communication.getPhone1());
            assertNotNull("Phone2 Is null.", communication.getPhone2());
            assertNotNull("Mobile Is null.", communication.getMobile());
            assertNotNull("ImAddress Is null.", communication.getImAddress());

            assertEquals("Updated Email value is not equal.", "test@tpd.com",
                    communication.getEmail());
            assertEquals("Updated Fax value is not equal.", "12555",
                    communication.getFax());
            assertEquals("Updated Phone1 value is not equal.", "123434",
                    communication.getPhone1());
            assertEquals("Updated Phone2 value is not equal.", "12323",
                    communication.getPhone2());
            assertEquals("Updated Mobile value is not equal.", "12343425",
                    communication.getMobile());
            assertEquals("Updated ImAddress value is not equal.",
                    "test@tpd.com", communication.getImAddress());

        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
                System.out.println(e.getCode());
            }
        }

    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsWorkInfoCommunicationService = (IContactsWorkInfoCommunicationService) ContextUtils
                .loadContext()
                .getService(
                        ServiceEnum.ContactsWorkInfoCommunicationService.name());
        login(USERID, PASSWORD);
        
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsWorkInfoCommunicationService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    @Test
    public void testContactsWorkInfoCommNegTest() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsWorkInfoCommNegTest");
        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("email", new ArrayList<String>());
        inputParam.get("email").add("junkemailid");

        setParams(params, new InvalidRequestException(
                AddressBookError.ABS_WORKINFO_INVALID_EMAIL.toString()));

    }

    @Test
    public void testContactsWorkInfoCommunication() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsWorkInfoCommunication");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        params.put("phone1", new ArrayList<String>());
        params.get("phone1").add("123434");

        params.put("phone2", new ArrayList<String>());
        params.get("phone2").add("12323");

        params.put("mobile", new ArrayList<String>());
        params.get("mobile").add("12343425");

        params.put("fax", new ArrayList<String>());
        params.get("fax").add("12555");

        params.put("email", new ArrayList<String>());
        params.get("email").add("test@tpd.com");

        params.put("imAddress", new ArrayList<String>());
        params.get("imAddress").add("test@tpd.com");

        setParams(params);

    }
}
