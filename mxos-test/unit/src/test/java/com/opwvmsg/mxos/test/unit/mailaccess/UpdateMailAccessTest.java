package com.opwvmsg.mxos.test.unit.mailaccess;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.IntAccessType;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateMailAccessTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IMailAccessService ma;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateMailAccessTest.setUpBeforeClass...");
        ma = (IMailAccessService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailAccessService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateMailAccessTest.setUp...");
        assertNotNull("MailAccessService object is null.", ma);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateMailAccessTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateMailAccessTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        ma = null;
    }

    private static MailAccess getMailAccessParams() {
        MailAccess mao = null;
        try {
            mao = ma.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("MailAccess object is null.", mao);
        return mao;
    }

    private static void updateMailAccessParams(
            Map<String, List<String>> updateParams) {
        updateMailAccessParams(updateParams, null);
    }

    private static void updateMailAccessParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            ma.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                updateParams.put(EMAIL_KEY, new ArrayList<String>());
                updateParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testUpdateMailAccessWithoutEmail() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateMailAccessTest.testUpdateMailAccessWithoutEmail...");
        updateMailAccessParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMailAccessWithEmptyEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add("");
        System.out
                .println("UpdateMailAccessTest.testUpdateMailAccessWithEmptyEmail...");
        updateMailAccessParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMailAccessWithNullEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(null);
        System.out
                .println("UpdateMailAccessTest.testUpdateMailAccessWithNullEmail...");
        updateMailAccessParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateMailAccessWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testUpdateMailAccessWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateMailAccessParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testPopAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.TRUSTED.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getPopAccessType();
        assertNotNull("PopAccessType is null.", pa);
        assertNotNull("PopAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testPopAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_POP_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testPopAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_POP_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testPopAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.TRUSTED.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getPopAccessType();
        assertNotNull("PopAccessType is null.", pa);
        assertTrue("PopAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testPopAccessTypeSuccess() throws Exception {
        System.out.println("UpdateMailAccessTest.testPopAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.ALL.toString());
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getPopAccessType();
        assertNotNull("PopAccessType is null.", pa);
        assertTrue("PopAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testPopSSLAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopSSLAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getPopSSLAccessType();
        assertNotNull("PopSSLAccessType is null.", pa);
        assertNotNull("PopSSLAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testPopSSLAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopSSLAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_POP_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testPopSSLAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopSSLAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_POP_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testPopSSLAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopSSLAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getPopSSLAccessType();
        assertNotNull("PopSSLAccessType is null.", pa);
        assertNotNull("PopSSLAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testPopSSLAccessTypeSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopSSLAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getPopSSLAccessType();
        assertNotNull("PopSSLAccessType is null.", pa);
        assertTrue("PopSSLAccessType has a wrong value.",
                pa.equals(AccessType.NONE));
        updateParams.remove(key);
    }

    @Test
    public void testPopAuthenticationTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAuthenticationTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAuthenticationType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(IntAccessType._1.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        IntAccessType pa = getMailAccessParams().getPopAuthenticationType();
        assertNotNull("PopAuthenticationType is null.", pa);
        assertNotNull("PopAuthenticationType is not from default COS.",
                pa.equals(IntAccessType._0));
        updateParams.remove(key);
    }

    @Test
    public void testPopAuthenticationTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAuthenticationTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAuthenticationType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("5");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_POP_AUTH_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testPopAuthenticationTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAuthenticationTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAuthenticationType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_POP_AUTH_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testPopAuthenticationTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAuthenticationTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAuthenticationType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(IntAccessType._1.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        IntAccessType pa = getMailAccessParams().getPopAuthenticationType();
        assertNotNull("PopAuthenticationType is null.", pa);
        assertNotNull("PopAuthenticationType is not from default COS.",
                pa.equals(IntAccessType._0));
        updateParams.remove(key);
    }

    @Test
    public void testPopAuthenticationTypeSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testPopAuthenticationTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popAuthenticationType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(IntAccessType._0.toString());
        updateMailAccessParams(updateParams);
        IntAccessType pa = getMailAccessParams().getPopAuthenticationType();
        assertNotNull("PopAuthenticationType is null.", pa);
        assertTrue("PopAuthenticationType has a wrong value.",
                pa.equals(IntAccessType._0));
        updateParams.remove(key);
    }

    @Test
    public void testImapAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getImapAccessType();
        assertNotNull("ImapAccessType is null.", pa);
        assertNotNull("ImapAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testImapAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getImapAccessType();
        assertNotNull("ImapAccessType is null.", pa);
        assertNotNull("ImapAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testImapAccessTypeSuccess() throws Exception {
        System.out.println("UpdateMailAccessTest.testImapAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getImapAccessType();
        assertNotNull("ImapAccessType is null.", pa);
        assertTrue("ImapAccessType has a wrong value.",
                pa.equals(AccessType.NONE));
        updateParams.remove(key);
    }

    @Test
    public void testImapSSLAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapSSLAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getImapSSLAccessType();
        assertNotNull("ImapSSLAccessType is null.", pa);
        assertNotNull("ImapSSLAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testImapSSLAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapSSLAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapSSLAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapSSLAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapSSLAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapSSLAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getImapSSLAccessType();
        assertNotNull("ImapSSLAccessType is null.", pa);
        assertNotNull("ImapSSLAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testImapSSLAccessTypeSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testImapSSLAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getImapSSLAccessType();
        assertNotNull("ImapSSLAccessType is null.", pa);
        assertTrue("ImapSSLAccessType has a wrong value.",
                pa.equals(AccessType.NONE));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpAccessEnabled();
        assertNotNull("SmtpAccessEnabled is null.", pa);
        assertNotNull("SmtpAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_SMTP_ACCESS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_SMTP_ACCESS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpAccessEnabled();
        assertNotNull("SmtpAccessEnabled is null.", pa);
        assertNotNull("SmtpAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpAccessEnabled();
        assertNotNull("SmtpAuthenticationEnabled is null.", pa);
        assertTrue("SmtpAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpSSLAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpSSLAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpSSLAccessEnabled();
        assertNotNull("SmtpSSLAccessEnabled is null.", pa);
        assertNotNull("SmtpSSLAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpSSLAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpSSLAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_SMTP_SSL_ACCESS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmtpSSLAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpSSLAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_SMTP_SSL_ACCESS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmtpSSLAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpSSLAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpSSLAccessEnabled();
        assertNotNull("SmtpSSLAccessEnabled is null.", pa);
        assertNotNull("SmtpSSLAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpSSLAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpSSLAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpSSLAccessEnabled();
        assertNotNull("SmtpSSLAccessEnabled is null.", pa);
        assertTrue("SmtpSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAuthenticationEnabledNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAuthenticationEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpAuthenticationEnabled();
        assertNotNull("SmtpAuthenticationEnabled is null.", pa);
        assertNotNull("SmtpAuthenticationEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAuthenticationEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAuthenticationEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_SMTP_AUTH_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAuthenticationEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAuthenticationEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_SMTP_AUTH_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAuthenticationEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAuthenticationEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpAuthenticationEnabled();
        assertNotNull("SmtpAuthenticationEnabled is null.", pa);
        assertNotNull("SmtpAuthenticationEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testSmtpAuthenticationEnabledSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testSmtpAuthenticationEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getSmtpAuthenticationEnabled();
        assertNotNull("SmtpAuthenticationEnabled is null.", pa);
        assertTrue("SmtpAuthenticationEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getWebmailAccessType();
        assertNotNull("WebmailAccessType is null.", pa);
        assertNotNull("WebmailAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getWebmailAccessType();
        assertNotNull("WebmailAccessType is null.", pa);
        assertNotNull("WebmailAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailAccessTypeSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getWebmailAccessType();
        assertNotNull("WebmailAccessType is null.", pa);
        assertTrue("WebmailAccessType has a wrong value.",
                pa.equals(AccessType.NONE));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailSSLAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailSSLAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.TRUSTED.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getWebmailSSLAccessType();
        assertNotNull("WebmailSSLAccessType is null.", pa);
        assertNotNull("WebmailSSLAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailSSLAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailSSLAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailSSLAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailSSLAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailSSLAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailSSLAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.TRUSTED.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getWebmailSSLAccessType();
        assertNotNull("WebmailSSLAccessType is null.", pa);
        assertNotNull("WebmailSSLAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailSSLAccessTypeSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testWebmailSSLAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailSSLAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getWebmailSSLAccessType();
        assertNotNull("WebmailSSLAccessType is null.", pa);
        assertTrue("WebmailSSLAccessType has a wrong value.",
                pa.equals(AccessType.NONE));
        updateParams.remove(key);
    }

    @Test
    public void testMobileMailAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileMailAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileMailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getMobileMailAccessType();
        assertNotNull("MobileMailAccessType is null.", pa);
        assertNotNull("MobileMailAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testMobileMailAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileMailAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileMailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_MOBILE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMobileMailAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileMailAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileMailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_MOBILE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMobileMailAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileMailAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileMailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getMobileMailAccessType();
        assertNotNull("MobileMailAccessType is null.", pa);
        assertNotNull("MobileMailAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testMobileMailAccessTypeSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileMailAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileMailAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());
        updateMailAccessParams(updateParams);
        AccessType pa = getMailAccessParams().getMobileMailAccessType();
        assertNotNull("MobileMailAccessType is null.", pa);
        assertTrue("MobileMailAccessType has a wrong value.",
                pa.equals(AccessType.NONE));
        updateParams.remove(key);
    }

    @Test
    public void testMobileActiveSyncAllowedNullParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileActiveSyncAllowedNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileActiveSyncAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getMobileActiveSyncAllowed();
        assertNotNull("MobileActiveSyncAllowed is null.", pa);
        assertNotNull("MobileActiveSyncAllowed is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testMobileActiveSyncAllowedInvalidParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileActiveSyncAllowedInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileActiveSyncAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_MOBILE_ACTIVE_SYNC_ALLOWED
                        .name());
        updateParams.remove(key);
    }

    @Test
    public void testMobileActiveSyncAllowedSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileActiveSyncAllowedSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileActiveSyncAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMailAccessParams(updateParams,
                MailboxError.MBX_INVALID_MOBILE_ACTIVE_SYNC_ALLOWED
                        .name());
        updateParams.remove(key);
    }

    @Test
    public void testMobileActiveSyncAllowedEmptyParam() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileActiveSyncAllowedEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileActiveSyncAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateMailAccessParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getMobileActiveSyncAllowed();
        assertNotNull("MobileActiveSyncAllowed is null.", pa);
        assertNotNull("MobileActiveSyncAllowed is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testMobileActiveSyncAllowedSuccess() throws Exception {
        System.out
                .println("UpdateMailAccessTest.testMobileActiveSyncAllowedSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.mobileActiveSyncAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateMailAccessParams(updateParams);
        BooleanType pa = getMailAccessParams().getMobileActiveSyncAllowed();
        assertNotNull("MobileActiveSyncAllowed is null.", pa);
        assertTrue("MobileActiveSyncAllowed has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

}
