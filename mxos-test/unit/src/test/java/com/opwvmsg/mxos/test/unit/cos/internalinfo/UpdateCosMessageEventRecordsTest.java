package com.opwvmsg.mxos.test.unit.cos.internalinfo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.EventRecordingType;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMessageEventRecordsService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateCosMessageEventRecordsTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosMessageEventRecordsService mer;
    private static ICosService cs;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateCosMessageEventRecordsTest.setUpBeforeClass...");
        mer = (ICosMessageEventRecordsService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosMessageEventRecordsService.name());
        cs = (ICosService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosService.name());
        createCos(COS_ID);
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateCosMessageEventRecordsTest.setUp...");
        assertNotNull("MessageEventRecordsService object is null.", mer);
        assertNotNull("CosService object is null.", cs);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateCosMessageEventRecordsTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateCosMessageEventRecordsTest.tearDownAfterClass...");
        deleteCos(COS_ID);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        mer = null;
        cs = null;
    }

    private static MessageEventRecords getCosMessageEventRecordsParams() {
        MessageEventRecords obj = null;
        try {
            obj = mer.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(COSID_KEY)
                    || getParams.get(COSID_KEY).isEmpty()) {
                getParams.put(COSID_KEY, new ArrayList<String>());
                getParams.get(COSID_KEY).add(COS_ID);
            }
        }
        assertNotNull("MessageEventRecords object is null.", obj);
        return obj;
    }

    private static void updateCosMessageEventRecordsParams(
            Map<String, List<String>> updateParams) {
        updateCosMessageEventRecordsParams(updateParams, null);
    }

    private static void updateCosMessageEventRecordsParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            mer.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(COSID_KEY)
                    || updateParams.get(COSID_KEY).isEmpty()) {
                updateParams.put(COSID_KEY, new ArrayList<String>());
                updateParams.get(COSID_KEY).add(COS_ID);
            }
        }
    }

    private static void createCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cs.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cs.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testUpdateMessageEventRecordsWithoutCosId() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateMessageEventRecordsTest.testUpdateMessageEventRecordsWithoutCosId...");
        updateCosMessageEventRecordsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMessageEventRecordsWithEmptyCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add("");
        System.out
                .println("UpdateCosInternalInfoTest.testUpdateMessageEventRecordsWithEmptyCosId...");
        updateCosMessageEventRecordsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMessageEventRecordsWithNullCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(null);
        System.out
                .println("UpdateMessageEventRecordsTest.testUpdateMessageEventRecordsWithNullCosId...");
        updateCosMessageEventRecordsParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateMessageEventRecordsWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testUpdateMessageEventRecordsWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        updateCosMessageEventRecordsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testEventRecordingEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testEventRecordingEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_EVENT_RECORDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testEventRecordingEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_EVENT_RECORDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testEventRecordingEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_EVENT_RECORDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testEventRecordingEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_EVENT_RECORDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledSuccess() throws Exception {
        System.out.println("UpdateCosMessageEventRecordsTest.testEventRecordingEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(EventRecordingType.ALWAYS.name());
        updateCosMessageEventRecordsParams(updateParams);
        EventRecordingType ert = getCosMessageEventRecordsParams().getEventRecordingEnabled();
        assertNotNull("EventRecordingEnabled is null.", ert);
        assertTrue("EventRecordingEnabled has a wrong value.",
                ert.equals(EventRecordingType.ALWAYS));
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthNullParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxHeaderLengthNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_HEADER_LENGTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxHeaderLengthSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_HEADER_LENGTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthEmptyParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxHeaderLengthEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_HEADER_LENGTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthSuccess() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxHeaderLengthSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateCosMessageEventRecordsParams(updateParams);
        Integer headerLength = getCosMessageEventRecordsParams().getMaxHeaderLength();
        assertNotNull("MaxHeaderLength is null.", headerLength);
        assertTrue("MaxHeaderLength has a wrong value.",
                headerLength.equals(new Integer(100)));
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBNullParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxFilesSizeKBNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_FILES_SIZE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxFilesSizeKBSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_FILES_SIZE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBEmptyParam() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxFilesSizeKBEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_FILES_SIZE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBSuccess() throws Exception {
        System.out
                .println("UpdateCosMessageEventRecordsTest.testMaxFilesSizeKBSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateCosMessageEventRecordsParams(updateParams);
        Integer headerLength = getCosMessageEventRecordsParams().getMaxFilesSizeKB();
        assertNotNull("MaxFilesSizeKB is null.", headerLength);
        assertTrue("MaxFilesSizeKB has a wrong value.",
                headerLength.equals(new Integer(100)));
        updateParams.remove(key);
    }

}
