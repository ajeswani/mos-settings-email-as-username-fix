package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums.AutoReplyMode;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.DeliveryScope;
import com.opwvmsg.mxos.data.enums.MxosEnums.DisplayHeadersType;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailReceipt.AckReturnReceiptReq;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailReceiptPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailReceiptPOST";
    private static final String ARROW_SEP = " --> ";
    private static IMailReceiptService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IMailReceiptService) ContextUtils.loadContext().getService(
                ServiceEnum.MailReceiptService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static MailReceipt getParams() {
        MailReceipt mailReceipt = null;
        try {
            mailReceipt = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return mailReceipt;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    // receiveMessages
    @Test
    public void testReceiveMessagesNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testReceiveMessagesNullParam");
        String key = MailboxProperty.receiveMessages.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testReceiveMessagesEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testReceiveMessagesEmptyParam");
        String key = MailboxProperty.receiveMessages.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testReceiveMessagesInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testReceiveMessagesInvalidParam");
        String key = MailboxProperty.receiveMessages.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_RECEIVE_MESSAGES.name());
        updateParams.remove(key);
    }

    @Test
    public void testReceiveMessagesSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testReceiveMessagesSplCharsInParam");
        String key = MailboxProperty.receiveMessages.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_RECEIVE_MESSAGES.name());
        updateParams.remove(key);
    }

    @Test
    public void testReceiveMessagesSuccess() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testReceiveMessagesSuccess");
        String key = MailboxProperty.receiveMessages.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getReceiveMessages();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testReceiveMessagesSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testReceiveMessagesSuccess1");
        String key = MailboxProperty.receiveMessages.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getReceiveMessages();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // forwardingEnabled
    @Test
    public void testForwardingEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testForwardingEnabledNullParam");
        String key = MailboxProperty.forwardingEnabled.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testForwardingEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testForwardingEnabledEmptyParam");
        String key = MailboxProperty.forwardingEnabled.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testForwardingEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testForwardingEnabledInvalidParam");
        String key = MailboxProperty.forwardingEnabled.name();
        addToParams(updateParams, key, "asdfsda");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FORWARDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testForwardingEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testForwardingEnabledSplCharsInParam");
        String key = MailboxProperty.forwardingEnabled.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FORWARDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testForwardingEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testForwardingEnabledSuccess");
        String key = MailboxProperty.forwardingEnabled.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getForwardingEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testForwardingEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testForwardingEnabledSuccess1");
        String key = MailboxProperty.forwardingEnabled.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getForwardingEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    @Test
    public void testCopyOnForwardNullParam() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testCopyOnForwardNullParam");
        String key = MailboxProperty.copyOnForward.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testCopyOnForwardEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyOnForwardEmptyParam");
        String key = MailboxProperty.copyOnForward.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testCopyOnForwardInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyOnForwardInvalidParam");
        String key = MailboxProperty.copyOnForward.name();
        addToParams(updateParams, key, "asdfsda");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_COPY_ON_FORWARD.name());
        updateParams.remove(key);
    }

    @Test
    public void testCopyOnForwardSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyOnForwardSplCharsInParam");
        String key = MailboxProperty.copyOnForward.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_COPY_ON_FORWARD.name());
        updateParams.remove(key);
    }

    @Test
    public void testCopyOnForwardSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testCopyOnForwardSuccess");
        String key = MailboxProperty.copyOnForward.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getCopyOnForward();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    @Test
    public void testCopyOnForwardSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testCopyOnForwardSuccess1");
        String key = MailboxProperty.copyOnForward.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getCopyOnForward();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // filterHTMLContent
    @Test
    public void testFilterHTMLContentNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFilterHTMLContentNullParam");
        String key = MailboxProperty.filterHTMLContent.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testFilterHTMLContentEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFilterHTMLContentEmptyParam");
        String key = MailboxProperty.filterHTMLContent.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testFilterHTMLContentInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFilterHTMLContentInvalidParam");
        String key = MailboxProperty.filterHTMLContent.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FILTER_HTML_CONTENT.name());
        updateParams.remove(key);
    }

    @Test
    public void testFilterHTMLContentSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFilterHTMLContentSplCharsInParam");
        String key = MailboxProperty.filterHTMLContent.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FILTER_HTML_CONTENT.name());
        updateParams.remove(key);
    }

    @Test
    public void testFilterHTMLContentSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFilterHTMLContentSuccess");
        String key = MailboxProperty.filterHTMLContent.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getFilterHTMLContent();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testFilterHTMLContentSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFilterHTMLContentSuccess1");
        String key = MailboxProperty.filterHTMLContent.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getFilterHTMLContent();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    @Test
    public void testDisplayHeadersNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDisplayHeadersNullParam");
        String key = MailboxProperty.displayHeaders.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testDisplayHeadersEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDisplayHeadersEmptyParam");
        String key = MailboxProperty.displayHeaders.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testDisplayHeadersInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDisplayHeadersInvalidParam");
        String key = MailboxProperty.displayHeaders.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_DISPLAY_HEADERS.name());
        updateParams.remove(key);
    }

    @Test
    public void testDisplayHeadersSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDisplayHeadersSplCharsInParam");
        String key = MailboxProperty.displayHeaders.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_DISPLAY_HEADERS.name());
        updateParams.remove(key);
    }

    @Test
    public void testDisplayHeadersSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testDisplayHeadersSuccess");
        String key = MailboxProperty.displayHeaders.name();
        addToParams(updateParams, key, DisplayHeadersType.HTML.name());
        updateParams(updateParams);
        DisplayHeadersType value = getParams().getDisplayHeaders();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, DisplayHeadersType.HTML);
        updateParams.remove(key);
    }

    @Test
    public void testDisplayHeadersSuccess1() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testDisplayHeadersSuccess1");
        String key = MailboxProperty.displayHeaders.name();
        addToParams(updateParams, key, DisplayHeadersType.TEXT.name());
        updateParams(updateParams);
        DisplayHeadersType value = getParams().getDisplayHeaders();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, DisplayHeadersType.TEXT);
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayWidthNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayWidthNullParam");
        String key = MailboxProperty.webmailDisplayWidth.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayWidthEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayWidthEmptyParam");
        String key = MailboxProperty.webmailDisplayWidth.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayWidthInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayWidthInvalidParam");
        String key = MailboxProperty.webmailDisplayWidth.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_DISPLAY_WIDTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayWidthSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayWidthSplCharsInParam");
        String key = MailboxProperty.webmailDisplayWidth.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_DISPLAY_WIDTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayWidthSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayWidthSuccess");
        String key = MailboxProperty.webmailDisplayWidth.name();
        String val = "1234";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getWebmailDisplayWidth().toString();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayFieldsNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayFieldsNullParam");
        String key = MailboxProperty.webmailDisplayFields.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayFieldsEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayFieldsEmptyParam");
        String key = MailboxProperty.webmailDisplayFields.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testWebmailDisplayFieldsSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWebmailDisplayFieldsSuccess");
        String key = MailboxProperty.webmailDisplayFields.name();
        String val = "1234";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getWebmailDisplayFields();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

    @Test
    public void testMaxMailsPerPageNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMailsPerPageNullParam");
        String key = MailboxProperty.maxMailsPerPage.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxMailsPerPageEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMailsPerPageEmptyParam");
        String key = MailboxProperty.maxMailsPerPage.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxMailsPerPageInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMailsPerPageInvalidParam");
        String key = MailboxProperty.maxMailsPerPage.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_MAILS_PER_PAGE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxMailsPerPageSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMailsPerPageSplCharsInParam");
        String key = MailboxProperty.maxMailsPerPage.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_MAILS_PER_PAGE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxMailsPerPageNegativeValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMailsPerPageNegativeValue");
        String key = MailboxProperty.maxMailsPerPage.name();
        addToParams(updateParams, key, "-1");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_MAILS_PER_PAGE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxMailsPerPageBigValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMailsPerPageBigValue");
        String key = MailboxProperty.maxMailsPerPage.name();
        addToParams(updateParams, key, "21474836471");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_MAILS_PER_PAGE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxMailsPerPageSuccess() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testMaxMailsPerPageSuccess");
        String key = MailboxProperty.maxMailsPerPage.name();
        Integer val = 50;
        addToParams(updateParams, key, val.toString());
        updateParams(updateParams);
        Integer value = getParams().getMaxMailsPerPage();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

    // previewPaneEnabled
    /*@Test
    public void testPreviewPaneEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testPreviewPaneEnabledNullParam");
        String key = MailboxProperty.previewPaneEnabled.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testPreviewPaneEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testPreviewPaneEnabledEmptyParam");
        String key = MailboxProperty.previewPaneEnabled.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testPreviewPaneEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testPreviewPaneEnabledInvalidParam");
        String key = MailboxProperty.previewPaneEnabled.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_PREVIEW_PANE_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testPreviewPaneEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testPreviewPaneEnabledSplCharsInParam");
        String key = MailboxProperty.previewPaneEnabled.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_PREVIEW_PANE_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testPreviewPaneEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testPreviewPaneEnabledSuccess");
        String key = MailboxProperty.previewPaneEnabled.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getPreviewPaneEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testPreviewPaneEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testPreviewPaneEnabledSuccess1");
        String key = MailboxProperty.previewPaneEnabled.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getPreviewPaneEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }*/

    @Test
    public void testAckReturnReceiptReqNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAckReturnReceiptReqNullParam");
        String key = MailboxProperty.ackReturnReceiptReq.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAckReturnReceiptReqEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAckReturnReceiptReqEmptyParam");
        String key = MailboxProperty.ackReturnReceiptReq.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAckReturnReceiptReqInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAckReturnReceiptReqInvalidParam");
        String key = MailboxProperty.ackReturnReceiptReq.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ACK_RETURN_RECEIPT_REQ.name());
        updateParams.remove(key);
    }

    @Test
    public void testAckReturnReceiptReqSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAckReturnReceiptReqSplCharsInParam");
        String key = MailboxProperty.ackReturnReceiptReq.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ACK_RETURN_RECEIPT_REQ.name());
        updateParams.remove(key);
    }

    @Test
    public void testAckReturnReceiptReqSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAckReturnReceiptReqSuccess");
        String key = MailboxProperty.ackReturnReceiptReq.name();
        addToParams(updateParams, key, AckReturnReceiptReq.ALWAYS.name());
        updateParams(updateParams);
        AckReturnReceiptReq value = getParams().getAckReturnReceiptReq();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AckReturnReceiptReq.ALWAYS);
        updateParams.remove(key);
    }

    @Test
    public void testAckReturnReceiptReqSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAckReturnReceiptReqSuccess1");
        String key = MailboxProperty.ackReturnReceiptReq.name();
        addToParams(updateParams, key, AckReturnReceiptReq.ASK.name());
        updateParams(updateParams);
        AckReturnReceiptReq value = getParams().getAckReturnReceiptReq();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AckReturnReceiptReq.ASK);
        updateParams.remove(key);
    }

    @Test
    public void testAckReturnReceiptReqSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAckReturnReceiptReqSuccess2");
        String key = MailboxProperty.ackReturnReceiptReq.name();
        addToParams(updateParams, key, AckReturnReceiptReq.NEVER.name());
        updateParams(updateParams);
        AckReturnReceiptReq value = getParams().getAckReturnReceiptReq();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AckReturnReceiptReq.NEVER);
        updateParams.remove(key);
    }

    @Test
    public void testDeliveryScopeNullParam() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testDeliveryScopeNullParam");
        String key = MailboxProperty.deliveryScope.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testDeliveryScopeEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeliveryScopeEmptyParam");
        String key = MailboxProperty.deliveryScope.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testDeliveryScopeInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeliveryScopeInvalidParam");
        String key = MailboxProperty.deliveryScope.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_DELIVERY_SCOPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testDeliveryScopeSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeliveryScopeSplCharsInParam");
        String key = MailboxProperty.deliveryScope.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_DELIVERY_SCOPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testDeliveryScopeSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testDeliveryScopeSuccess");
        String key = MailboxProperty.deliveryScope.name();
        addToParams(updateParams, key, DeliveryScope.GLOBAL.name());
        updateParams(updateParams);
        DeliveryScope value = getParams().getDeliveryScope();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, DeliveryScope.GLOBAL);
        updateParams.remove(key);
    }

    @Test
    public void testDeliveryScopeSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testDeliveryScopeSuccess1");
        String key = MailboxProperty.deliveryScope.name();
        addToParams(updateParams, key, DeliveryScope.LOCAL.name());
        updateParams(updateParams);
        DeliveryScope value = getParams().getDeliveryScope();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, DeliveryScope.LOCAL);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeNullParam() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testAutoReplyModeNullParam");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoReplyModeEmptyParam");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoReplyModeInvalidParam");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_REPLY_MODE.name());
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoReplyModeSplCharsInParam");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_REPLY_MODE.name());
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoReplyModeSuccess");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, AutoReplyMode.NONE.name());
        updateParams(updateParams);
        AutoReplyMode value = getParams().getAutoReplyMode();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AutoReplyMode.NONE);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoReplyModeSuccess1");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, AutoReplyMode.VACATION.name());
        updateParams(updateParams);
        AutoReplyMode value = getParams().getAutoReplyMode();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AutoReplyMode.VACATION);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoReplyModeSuccess2");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, AutoReplyMode.REPLY.name());
        updateParams(updateParams);
        AutoReplyMode value = getParams().getAutoReplyMode();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AutoReplyMode.REPLY);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyModeSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoReplyModeSuccess3");
        String key = MailboxProperty.autoReplyMode.name();
        addToParams(updateParams, key, AutoReplyMode.NONE.name());
        updateParams(updateParams);
        AutoReplyMode value = getParams().getAutoReplyMode();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AutoReplyMode.NONE);
        updateParams.remove(key);
    }

    @Test
    public void testMaxReceiveMessageSizeKBNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKBNullParam");
        String key = MailboxProperty.maxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxReceiveMessageSizeKBEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKBEmptyParam");
        String key = MailboxProperty.maxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxReceiveMessageSizeKBInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKBInvalidParam");
        String key = MailboxProperty.maxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_RECEIVE_MESSAGE_SIZE_KB.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxReceiveMessageSizeKBSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKBSplCharsInParam");
        String key = MailboxProperty.maxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_RECEIVE_MESSAGE_SIZE_KB.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxReceiveMessageSizeKBNegativeValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKBNegativeValue");
        String key = MailboxProperty.maxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, "-1");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_RECEIVE_MESSAGE_SIZE_KB.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxReceiveMessageSizeKBBigValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKBBigValue");
        String key = MailboxProperty.maxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, "21474836471");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_RECEIVE_MESSAGE_SIZE_KB.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxReceiveMessageSizeKBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKBSuccess");
        String key = MailboxProperty.maxReceiveMessageSizeKB.name();
        Integer val = 50;
        addToParams(updateParams, key, val.toString());
        updateParams(updateParams);
        Integer value = getParams().getMaxReceiveMessageSizeKB();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }
    
    @Test
    public void testMaxForwardingAddressesNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddressesNullParam");
        String key = MailboxProperty.maxNumForwardingAddresses .name();
        addToParams(updateParams, key, null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxForwardingAddressesEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddressesEmptyParam");
        String key = MailboxProperty.maxNumForwardingAddresses .name();
        addToParams(updateParams, key, "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxForwardingAddressesInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddressesInvalidParam");
        String key = MailboxProperty.maxNumForwardingAddresses .name();
        addToParams(updateParams, key, "13a");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxForwardingAddressesSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddressesSplCharsInParam");
        String key = MailboxProperty.maxNumForwardingAddresses .name();
        addToParams(updateParams, key, "3&*");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxForwardingAddressesMinValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddressesMinValue");
        String key = MailboxProperty.maxNumForwardingAddresses .name();
        addToParams(updateParams, key, "-1");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxForwardingAddressesMaxValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddressesMaxValue");
        String key = MailboxProperty.maxNumForwardingAddresses .name();
        addToParams(updateParams, key, "2147483648");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxForwardingAddressesSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddressesSuccess");
        String key = MailboxProperty.maxNumForwardingAddresses .name();
        Integer val = 18;
        addToParams(updateParams, key, val.toString());
        updateParams(updateParams);
        Integer value = getParams().getMaxNumForwardingAddresses();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }
    
    @Test
    public void testMobileMaxReceiveMessageSizeKBNegativeValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxReceiveMessageSizeKBNegativeValue");
        String key = MailboxProperty.mobileMaxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, "-1");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MOBILE_MAX_RECEIVE_MESSAGE_SIZE_KB.name());
        updateParams.remove(key);
    }

    @Test
    public void testMobileMaxReceiveMessageSizeKBBigValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxReceiveMessageSizeKBBigValue");
        String key = MailboxProperty.mobileMaxReceiveMessageSizeKB.name();
        addToParams(updateParams, key, "21474836471");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MOBILE_MAX_RECEIVE_MESSAGE_SIZE_KB.name());
        updateParams.remove(key);
    }

    @Test
    public void testMobileMaxReceiveMessageSizeKBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxReceiveMessageSizeKBSuccess");
        String key = MailboxProperty.mobileMaxReceiveMessageSizeKB.name();
        Integer val = 50;
        addToParams(updateParams, key, val.toString());
        updateParams(updateParams);
        Integer value = getParams().getMobileMaxReceiveMessageSizeKB();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }
}
