package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MessageCreateTest {

    private static final String TEST_NAME = "MessageCreateTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "message_createto@openwave.com";
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test5";
    private static final String COSID = "default";
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "message_createfrom@openwave.com";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, test mail from MessageCreateTest Program";
    
    private static final String MESSAGE_ENCODED_VALUE = "SGksIHRlc3QgbWFpbCBmcm9tIE1lc3NhZ2VDcmVhdGVUZXN0IFByb2dyYW0=";

    private static final String MESSAGE_ID_KEY = MessageProperty.messageId.name();
    
    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static final String ISADMIN_KEY = MessageProperty.isAdmin.name();
    private static final String MAILMESSAGESTORE_KEY = MailboxProperty.messageStoreHost
            .name();
    private static final String MAILBOXID_KEY = LDAPMailboxProperty.mailboxid
            .name();
    private static final String ARRIVALTIME_KEY = MessageProperty.arrivalTime.name();
    private static final String EXPIRETIME_KEY = MessageProperty.expireTime.name();
    private static final String KEYWORDS_KEY = MessageProperty.keywords.name();
    private static final String FLAGPRIV_KEY = MessageProperty.flagPriv.name();
    private static final String FLAGSEEN_KEY = MessageProperty.flagSeen.name();
    private static final String FLAGANS_KEY = MessageProperty.flagAns.name();
    private static final String FLAGFLAGGED_KEY = MessageProperty.flagFlagged
            .name();
    private static final String FLAGDELETED_KEY = MessageProperty.flagDel
            .name();
    private static final String FLAGRES_KEY = MessageProperty.flagRes.name();
    private static final String MSG_BASE64ENCODED_KEY = MessageProperty.msgBase64Encoded.name();
    private static final String COS_ID = "default";
    
    private static final String MESSAGE_UID = MessageProperty.uid.name();
    private static final String FOLDERNAME_VALUE_11_22 = "1111111111111111111111111111111111111111111111%2f22222222222222222222222222222222222222222222222" +
    "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222" +
    "222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222";
    private static final String FOLDERNAME_VALUE_10 = "a%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2ft";


    private static IMxOSContext context;
    private static IMessageService messageService;
    private static IMetadataService metadataService;
    private static IMailboxService mailboxService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        //createMailBox(FROMADDRESS, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(FROMADDRESS, PASSWORD, COSID);
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", messageService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        //deleteMailBox(FROMADDRESS);
        MailboxHelper.deleteMailbox(EMAIL,true);
        MailboxHelper.deleteMailbox(FROMADDRESS);
        CosHelper.deleteCos(COSID);
        params.clear();
        params = null;
        messageService = null;
        mailboxService = null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static void createMailBox(String email, String passwd) {
        Long id = MailboxHelper.createMailbox(email, passwd, COS_ID);
        if (id == -1L)
            fail("Failed to create Mailbox");
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testWithInvalidFolderName() {
        System.out.println(TEST_NAME + ARROW_SEP + "testWithInvalidFolderName");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_11_22);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    FolderError.FLD_INVALID_FOLDERNAME.name(), e.getCode());
        }
    }
    
    @Test
    public void testWithInvalidFolderDepth() {
        System.out.println(TEST_NAME + ARROW_SEP + "testWithInvalidFolderDepth");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_10);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    FolderError.FLD_INVALID_FOLDERNAME.name(), e.getCode());
        }
    }
    @Test
    public void testNoReceivedFrom() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNoMessage() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoMessage");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNullEmail() {

        System.out.println(TEST_NAME + ARROW_SEP + "testNullEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidReceivedFromAddress() {
        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_MESSAGE_FROM.name(), e.getCode());
        }
    }

    @Test
    public void testNullReceivedFromAddress() {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testNullReceivedFromAddress");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_MESSAGE_FROM.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidIsAdmin() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidIsAdmin");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, ISADMIN_KEY, "junk");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_IS_ADMIN.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidIsPrivate() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidIsPrivate");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, ISADMIN_KEY, "junk");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_IS_ADMIN.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidFlag() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGSEEN_KEY, "junk");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FLAG_SEEN.name(), e.getCode());
        }
    }
    @Test
    public void testEmptyUserFlags() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, KEYWORDS_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_USER_FLAGS.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyFlagSeen() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGSEEN_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FLAG_SEEN.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyArrivalTime() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidArrivalTime");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, ARRIVALTIME_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_ARRIVAL_TIME.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyExpireTime() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidExpireTime");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, EXPIRETIME_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_EXPIRY_TIME.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyFlagDraft() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, MessageProperty.flagDraft.name(), "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FLAG_DRAFT.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyFlagRes() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGRES_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FLAG_RES.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyFlagAns() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGANS_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FLAG_ANS.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyFlagFlagged() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGFLAGGED_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FLAG_FLAGGED.name(), e.getCode());
        }
    }
    
    @Test
    public void testEmptyFlagDelete() {

        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidFlag");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGDELETED_KEY, "");
            String uid = messageService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_FLAG_DEL.name(), e.getCode());
        }
    }
    @Test
    public void testWithInvalidMailMessageStoreAndInvalidMailboxId() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testWithInvalidMailMessageStoreAndInvalidMailboxId");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, MAILMESSAGESTORE_KEY, "junk");
            addParams(inputParams, MAILBOXID_KEY, "123");
            String uid = messageService.create(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(),
                    e.getCode());
        }
    }

    @Test
    public void testSuccesswithMandatoryParams() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithMandatoryParams");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGE_ID_KEY, uid);
            Message msg = messageService.read(inputParams1);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testSuccesswithFlagSeenFalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithFlagSeenFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGSEEN_KEY, "false");
            String uid = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, "messageId", uid);
            Message msg = messageService.read(inputParams1);
            assertFalse(msg.getMetadata().getFlagSeen());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testSuccesswithFlagAnsFalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithFlagAnsFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGANS_KEY, "false");
            String uid = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, "messageId", uid);
            Message msg = messageService.read(inputParams1);
            assertFalse(msg.getMetadata().getFlagAns());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testSuccesswithFlagFlaggedFalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithFlagFlaggedFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGFLAGGED_KEY, "false");
            String uid = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, "messageId", uid);
            Message msg = messageService.read(inputParams1);
            assertFalse(msg.getMetadata().getFlagFlagged());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testSuccesswithFlagDelFalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithFlagDelFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGDELETED_KEY, "false");
            String uid = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, "messageId", uid);
            Message msg = messageService.read(inputParams1);
            assertFalse(msg.getMetadata().getFlagDel());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testSuccesswithUID() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithFlagResFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FLAGRES_KEY, "false");
            addParams(inputParams, MESSAGE_UID, "2015");
            String uid = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, "messageId", uid);
            Message msg = messageService.read(inputParams1);
            long uid1 = msg.getMetadata().getUid();
            assertEquals(2015, uid1);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testSuccesswithMsgBase64EncodedTrue() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithMsgBase64EncodedTrue");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_ENCODED_VALUE);
            addParams(inputParams, MSG_BASE64ENCODED_KEY, "true");
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGE_ID_KEY, messageId);
            Message msg = messageService.read(inputParams1);
            if (MxOSConfig.isMsgBodyBlobBase64Encoded()) {
                assertEquals(msg.getBody().getMessageBlob(), MESSAGE_ENCODED_VALUE);
            } else {
                assertEquals(msg.getBody().getMessageBlob(), MESSAGE_VALUE);
            }           
        } catch (MxOSException e) {
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testSuccesswithMsgBase64EncodedFalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuccesswithMsgBase64EncodedFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, MSG_BASE64ENCODED_KEY, "false");
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGE_ID_KEY, messageId);
            Message msg = messageService.read(inputParams1);
            if (MxOSConfig.isMsgBodyBlobBase64Encoded()) {
                assertEquals(msg.getBody().getMessageBlob(), MESSAGE_ENCODED_VALUE);
            } else {
                assertEquals(msg.getBody().getMessageBlob(), MESSAGE_VALUE);
            }
        } catch (MxOSException e) {
            assertFalse("Exception Occured...", true);
        }
    }
}
