package com.opwvmsg.mxos.test.unit.cos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosExternalAccountsService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * @author mxos-dev
 */
public class CosExternalAccountTest {
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String cosId = "cos_2.0_ut104";
    private static IMxOSContext context;
    private static ICosService cosService;
    private static ICosExternalAccountsService iceas = null;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();

    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp starting......");
        context = ContextUtils.loadContext();
        cosService = (ICosService) context.getService(ServiceEnum.CosService
                .name());
        iceas = (ICosExternalAccountsService) context
                .getService(ServiceEnum.CosExternalAccountService.name());
        assertNotNull("ICosExternalAccountsService object is null.", iceas);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("setUp...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.create(inputParams);
        } catch (MxOSException e) {
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CosBaseTest.tearDownAfterClass...");
        inputParams = null;
        cosService = null;
        iceas = null;
    }

    @Test
    public void testExternalAccountsRead() {
        System.out
                .println("CosExternalAccountTest.testExternalAccountsRead starting ...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            ExternalAccounts ea = iceas.read(inputParams);
            assertNotNull("ExternalAccount object is null", ea);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            fail();
        }

    }

    @Test
    public void testExternalAccountsUpdate() {
        System.out.println("testExternalAccountsUpdate starting");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        inputParams.put("externalMailAccountsAllowed", new ArrayList<String>());
        inputParams.get("externalMailAccountsAllowed").add("yes");
        inputParams
                .put("promptForExternalAccountSync", new ArrayList<String>());
        inputParams.get("promptForExternalAccountSync").add("no");

        try {
            iceas.update(inputParams);
            inputParams.clear();
            inputParams.put(COSID_KEY, new ArrayList<String>());
            inputParams.get(COSID_KEY).add(cosId);
            ExternalAccounts ea = iceas.read(inputParams);
            assertEquals("They are equals parameter1: ",
                    ea.getExternalMailAccountsAllowed(),
                    MxosEnums.BooleanType.YES);
            assertEquals("They are equals parameter2: ",
                    ea.getPromptForExternalAccountSync(),
                    MxosEnums.BooleanType.NO);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            fail();
        }
    }

    @Test
    public void testExternalAccounts2() {
        System.out.println("testExternalAccounts2 starting ...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        inputParams.put("externalMailAccountsAllowed", new ArrayList<String>());
        inputParams.get("externalMailAccountsAllowed").add("");
        inputParams
                .put("promptForExternalAccountSync", new ArrayList<String>());
        inputParams.get("promptForExternalAccountSync").add("");
        try {
            iceas.update(inputParams);
            inputParams.clear();
            inputParams.put(COSID_KEY, new ArrayList<String>());
            inputParams.get(COSID_KEY).add(cosId);
            ExternalAccounts ea = iceas.read(inputParams);
            assertNull("externalMailAccountsAllowed is null",
                    ea.getExternalMailAccountsAllowed());
            assertNull("promptForExternalAccountSync is null",
                    ea.getPromptForExternalAccountSync());
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            fail();
        }
    }

    @Test
    public void testExternalUpdateNeg() {
        System.out.println("testExternalUpdateNeg starting");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        inputParams.put("externalMailAccountsAllowed", new ArrayList<String>());
        inputParams.get("externalMailAccountsAllowed").add("INVALID");
        try {
            iceas.update(inputParams);
            inputParams.clear();
            inputParams.put(COSID_KEY, new ArrayList<String>());
            inputParams.get(COSID_KEY).add(cosId);
            iceas.read(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_INVALID_EXTERNAL_ACCOUNTS_ALLOWED
                            .toString());
        }
    }
}
