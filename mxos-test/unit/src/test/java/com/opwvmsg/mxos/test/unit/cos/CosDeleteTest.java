package com.opwvmsg.mxos.test.unit.cos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosBaseService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * @author mxos-dev
 */
public class CosDeleteTest {
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String cosId = "cos_2.0_ut102";
    private static IMxOSContext context;
    private static ICosService cosService;
    private static ICosBaseService cosBaseService;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();

    /**
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosService = (ICosService) context.getService(ServiceEnum.CosService
                .name());
        cosBaseService = (ICosBaseService) context
                .getService(ServiceEnum.CosBaseService.name());
        assertNotNull("CosService object is null.", cosService);
        assertNotNull("CosBaseService object is null.", cosBaseService);

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("setUp...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.create(inputParams);
        } catch (MxOSException e) {
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("tearDownAfterClass...");
        inputParams = null;
        cosService = null;
        cosBaseService = null;
    }

    @Test
    public void testDeleteCos() {
        System.out.println("testDeleteCos...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
            fail();
            return;
        }
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosBaseService.read(inputParams);
            fail();
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not COS_NOT_FOUND", e.getCode()
                    .equalsIgnoreCase(CosError.COS_NOT_FOUND.name()));
            return;
        }
    }
}
