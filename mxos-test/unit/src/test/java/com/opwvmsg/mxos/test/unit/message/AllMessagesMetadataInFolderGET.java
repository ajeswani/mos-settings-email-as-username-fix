package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class AllMessagesMetadataInFolderGET {

    private static final String TEST_NAME = "AllMessagesMetadataInFolderGET";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "message.rest1@openwave.com";
    private static final String PASSWORD = "test5";
    
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "message.rest2@openwave.com";
    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDER_KEY = FolderProperty.folderName.name();
    private static final String FOLDER_INBOX = "INBOX";
    private static final String COS_ID = "default";

    private static IMxOSContext context;
    private static IMessageService messageService;
    private static IMetadataService metadataService;
    private static IMailboxService mailboxService;
    private static ISendMailService sendMailService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        createMailBox(EMAIL, PASSWORD);
        createMailBox(FROMADDRESS, PASSWORD);
        messageService = (IMessageService) context
        .getService(ServiceEnum.MessageService.name());
        metadataService = (IMetadataService) context
        .getService(ServiceEnum.MetadataService.name());
        sendMailService = (ISendMailService) context
        .getService(ServiceEnum.SendMailService.name());
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", messageService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox(EMAIL);
        deleteMailBox(FROMADDRESS);
        params.clear();
        params = null;
        messageService = null;
        mailboxService = null;
        sendMailService = null;
    }
    
    private static void createMailBox(String email, String password) {
        Long id = MailboxHelper.createMailbox(email, password, COS_ID);
        if (id == -1L)
            fail("Failed to create Mailbox");
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }
    
    private static void sendMail() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            //sendMailService.process(inputParams);
            messageService.create(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    public static void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testMetadataInFolderSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP + "testMetadataInFolderSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            assertTrue("Unable to read messages", messages.size() == 6);
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testMetadataInFolderNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testMetadataInFolderNoEmail");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            metadataService.list(inputParams);
            
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        }  catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testMetadataInFolderNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP + "testMetadataInFolderNoEmail");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            metadataService.list(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMetadataInFolderNonExistingFolder() {
        System.out.println(TEST_NAME + ARROW_SEP + "testMetadataInFolderNonExistingFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, "zzzz");
            metadataService.list(inputParams);
        } catch (MxOSException e) {
            String expectedError = FolderError.FLD_NOT_FOUND.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }
}
