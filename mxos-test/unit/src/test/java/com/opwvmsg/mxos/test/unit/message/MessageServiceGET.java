package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 *
 * @author mxos-dev
 *
 */
public class MessageServiceGET {

    private static final String TEST_NAME = "MessageServiceGET";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "zz123456@test.com";

    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static final String MESSAGEID_KEY = MessageProperty.messageId
            .name();
    private static final String MESSAGEID_VALUE = "1003";
    private static final String MESSAGEID_VALUE_NOMSG = "1005";

    private static IMxOSContext context;
    private static IMessageService messageService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", messageService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        messageService = null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

//    @Test
//    public void testGetMessageSuccess() {
//        System.out.println(TEST_NAME + ARROW_SEP + "testGetMessageSuccess");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, EMAIL_KEY, EMAIL);
//            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
//            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE);
//            Message message = messageService.readBody(inputParams);
//        } catch (Exception e) {
//            assertFalse("Exception Occured...", true);
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testGetMessageNoMsg() {
//        System.out.println(TEST_NAME + ARROW_SEP + "testGetMessageNoMsg");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, EMAIL_KEY, EMAIL);
//            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
//            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
//            Message message = messageService.readBody(inputParams);
//            fail("This should not have been come!!!");
//        } catch (MxOSException e) {
//            assertEquals("Some unexpected error code.",
//                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e.getCode());
//        }
//    }

    @Test
    public void testGetMessageNoEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Message message = messageService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(),
                    e.getCode());
        }
    }

    @Test
    public void testGetMessageNoMessageId() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Message message = messageService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(),
                    e.getCode());
        }
    }

    @Test
    public void testGetMessageNoFolder() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Message message = messageService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(),
                    e.getCode());
        }
    }

    @Test
    public void testGetMessageNullEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Message message = messageService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageInvalidEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Message message = messageService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            e.printStackTrace();
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }

}
