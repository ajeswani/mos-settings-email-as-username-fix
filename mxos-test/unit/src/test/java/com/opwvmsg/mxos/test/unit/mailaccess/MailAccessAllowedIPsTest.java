package com.opwvmsg.mxos.test.unit.mailaccess;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessAllowedIpService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailAccessAllowedIPsTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IMailAccessAllowedIpService maais;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MailAccessAllowedIPsTest.setUpBeforeClass...");
        maais = (IMailAccessAllowedIpService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailAccessAllowedIpService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("MailAccessAllowedIPsTest.setUp...");
        assertNotNull("MailAccessallowedIPService object is null.", maais);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("MailAccessAllowedIPsTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MailAccessAllowedIPsTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        params.clear();
        params = null;
        maais = null;
    }

    private static List<String> getMailAccessallowedIP() {
        List<String> allowedIP = null;
        try {
            allowedIP = maais.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("allowedIP object is null.", allowedIP);
        return allowedIP;
    }

    private static void createMailAccessAllowedIP(
            Map<String, List<String>> params) {
        createMailAccessAllowedIP(params, null);
    }

    private static void createMailAccessAllowedIP(
            Map<String, List<String>> params, String expectedError) {
        try {
            maais.create(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    private static void updateMailAccessAllowedIP(
            Map<String, List<String>> params) {
        updateMailAccessAllowedIP(params, null);
    }

    private static void updateMailAccessAllowedIP(
            Map<String, List<String>> params, String expectedError) {
        try {
            maais.update(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    private static void deleteMailAccessAllowedIP(
            Map<String, List<String>> params) {
        deleteMailAccessAllowedIP(params, null);
    }

    private static void deleteMailAccessAllowedIP(
            Map<String, List<String>> params, String expectedError) {
        try {
            maais.delete(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testCreateMailAccessAllowedIPWithoutEmail() throws Exception {
        params.clear();
        System.out
                .println("MailAccessAllowedIPsTest.testCreateMailAccessAllowedIPWithoutEmail...");
        createMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testCreateMailAccessAllowedIPWithEmptyEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add("");
        System.out
                .println("MailAccessAllowedIPsTest.testCreateMailAccessAllowedIPWithEmptyEmail...");
        createMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testCreateMailAccessAllowedIPWithNullEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(null);
        System.out
                .println("MailAccessAllowedIPsTest.testCreateMailAccessAllowedIPWithNullEmail...");
        createMailAccessAllowedIP(params, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testCreateMailAccessAllowedIPWithoutAnyParam() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testCreateMailAccessAllowedIPWithoutAnyParam...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        createMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testAddAllowedIPWithNoIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddAllowedIPWithNoIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        // Create IP
        createMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP still non-empty.", allowedIP.isEmpty());
    }

    @Test
    public void testAddAllowedIPWithNullIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddAllowedIPWithNullIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(null);
        // Create IP
        createMailAccessAllowedIP(params,
                ErrorCode.MXS_INPUT_ERROR.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP still non-empty.", allowedIP.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddAllowedIPWithEmptyIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddAllowedIPWithEmptyIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        // Create IP
        createMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP still non-empty.", allowedIP.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddAllowedIPWithSplCharsInIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddAllowedIPWithSplCharsInIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*()");
        // Create IP
        createMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP still non-empty.", allowedIP.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddallowedIPBigIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddallowedIPBigIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.23.190.231.216";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testAddallowedIPBigIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddallowedIPBigIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "fd00:1:11:5a:221:5aff:fea7:65e0:120";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testAddallowedIPInvalidIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddallowedIPInvalidIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.623.190.231";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testAddallowedIPInvalidIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddallowedIPInvalidIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "fd400:1:11:5a:221:5aff:fea7:65e0";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testAddallowedIPWithIPMaxLimit() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddallowedIPWithIPMaxLimit...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String[] ips = new String[] { "10.123.10.23", "10.203.234.51",
                "ac34:5aff:151:8a49:221:a6bf:16ea:1b05", "10.98.102.219",
                "10.102.90.100", "10.36.89.231",
                "ac34:841:151:84c9:221:abf8:16ea:b095", "10.39.193.153",
                "10.162.181.107", "fd00:1:11:5a:221:5aff:fea7:65e0", };
        String ip = "10.223.199.241";
        // Add all 10 ips first
        for (String value : ips) {
            params.remove(key);
            params.put(key, new ArrayList<String>());
            params.get(key).add(value);
            // Create IP
            createMailAccessAllowedIP(params);
        }
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP once more.
        createMailAccessAllowedIP(params,
                MailboxError.MBX_MAILACCESS_ALLOWED_IPS_REACHED_MAX_LIMIT
                        .name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertEquals("allowedIP length does not match with the actual.",
                ips.length, allowedIP.size());
        for (String value : ips) {
            assertTrue("allowedIP does not contain " + value + ".",
                    allowedIP.contains(value));
        }
        assertFalse("allowedIP still contains " + ip + ".",
                allowedIP.contains(ip));
        // Delete all 10 ips
        for (String value : ips) {
            // Delete IP
            params.remove(key);
            params.put(key, new ArrayList<String>());
            params.get(key).add(value);
            deleteMailAccessAllowedIP(params);
        }
        params.remove(key);
    }

    @Test
    public void testAddallowedIPSuccessWithIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddallowedIPSuccessWithIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        // Delete IP
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testAddallowedIPSuccessWithIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testAddallowedIPSuccessWithIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "fd00:1:11:5a:221:5aff:fea7:65e0";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        // Delete IP
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testGetallowedIPSuccessWithIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testGetallowedIPSuccessWithIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        String ip2 = "10.2.23.141";
        assertFalse("allowedIP contains " + ip2 + ".", allowedIP.contains(ip2));
        // Delete IP
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testGetallowedIPSuccessWithIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testGetallowedIPSuccessWithIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "fd00:1:11:5a:221:5aff:fea7:65e0";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        String ip2 = "10.2.23.141";
        assertFalse("allowedIP contains " + ip2 + ".", allowedIP.contains(ip2));
        // Delete IP
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateMailAccessAllowedIPWithoutEmail() throws Exception {
        params.clear();
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateMailAccessAllowedIPWithoutEmail...");
        updateMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMailAccessAllowedIPWithEmptyEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add("");
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateMailAccessAllowedIPWithEmptyEmail...");
        updateMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMailAccessAllowedIPWithNullEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(null);
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateMailAccessAllowedIPWithNullEmail...");
        updateMailAccessAllowedIP(params, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateMailAccessAllowedIPWithoutAnyParam() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateMailAccessAllowedIPWithoutAnyParam...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        updateMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateAllowedIPWithNoOldIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithNoOldIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithNoNewIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithNoOldIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        // Update IP
        updateMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithNullOldIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithNullOldIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(null);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params,
                ErrorCode.MXS_INPUT_ERROR.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithNullNewIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithNullNewIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(null);
        // Update IP
        updateMailAccessAllowedIP(params,
                ErrorCode.MXS_INPUT_ERROR.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithEmptyOldIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithEmptyOldIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add("");
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithEmptyNewIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithEmptyNewIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add("");
        // Update IP
        updateMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithSplCharsInOldIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithSplCharsInNewIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add("!@#$%^&*()");
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_OLD_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithSplCharsInNewIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithSplCharsInNewIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add("!@#$%^&*()");
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_NEW_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithBigOldIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithBigOldIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add("10.2.23.141.123");
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_OLD_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithBigOldIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithBigOldIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "fd00:1:11:5a:221:5aff:fea7:65e0";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add("fd00:1:11:5a:221:5aff:fea7:65e0:120F");
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_OLD_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithBigNewIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithBigNewIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add("10.2.23.141.231");
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_NEW_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithBigNewIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithBigNewIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "fd00:1:11:5a:221:5aff:fea7:65e0";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add("fd00:1:11:5a:221:5aff:fea7:65e0:12FF");
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_NEW_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithInvalidOldIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithInvalidOldIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add("10.2.523.141");
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_OLD_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithInvalidOldIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithInvalidOldIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "fd00:1:11:5a:221:5aff:fea7:65e0";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add("fd00:1VV:11:5a:221:5aff:fea7:65e0:120F");
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_OLD_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithInvalidNewIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithInvalidNewIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add("10.452.23.141.231");
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_NEW_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateAllowedIPWithInvalidNewIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateAllowedIPWithInvalidNewIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "fd00:1:11:5a:221:5aff:fea7:65e0";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add("fd00:1:1GH1:5a:221:5aff:fea7:65e0:12FF");
        // Update IP
        updateMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_NEW_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP updated with " + newIP + ".",
                allowedIP.contains(oldIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateallowedIPSuccessWithIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateallowedIPSuccessWithIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "10.2.23.141";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + oldIP + ".",
                allowedIP.contains(oldIP));
        assertTrue("allowedIP does not contain " + newIP + ".",
                allowedIP.contains(newIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(newIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testUpdateallowedIPSuccessWithIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateallowedIPSuccessWithIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.2.23.140";
        String newIP = "fd00:1:11:5a:221:5aff:fea7:65e0";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + oldIP + ".",
                allowedIP.contains(oldIP));
        assertTrue("allowedIP does not contain " + newIP + ".",
                allowedIP.contains(newIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(newIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }
    
    @Test
    public void testUpdateallowedIPSuccessWithSameNewIPAndOldIP()
            throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testUpdateallowedIPSuccessWithSameNewIPAndOldIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String oldIP = "10.10.23.204";
        String newIP = "10.10.23.204";
        String key = MailboxProperty.allowedIP.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(oldIP);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);

        String oldKey = MailboxProperty.oldAllowedIP.name();
        String newKey = MailboxProperty.newAllowedIP.name();
        params.put(oldKey, new ArrayList<String>());
        params.get(oldKey).add(oldIP);
        params.put(newKey, new ArrayList<String>());
        params.get(newKey).add(newIP);
        // Update IP
        updateMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + newIP + ".",
                allowedIP.contains(newIP));
        params.remove(oldKey);
        params.remove(newKey);
        params.put(key, new ArrayList<String>());
        params.get(key).add(newIP);
        // Delete IP
        deleteMailAccessAllowedIP(params);
        params.remove(key);
    }

    @Test
    public void testDeleteMailAccessAllowedIPWithoutEmail() throws Exception {
        params.clear();
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteMailAccessAllowedIPWithoutEmail...");
        deleteMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testDeleteMailAccessAllowedIPWithEmptyEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add("");
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteMailAccessAllowedIPWithEmptyEmail...");
        deleteMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testDeleteMailAccessAllowedIPWithNullEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(null);
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteMailAccessAllowedIPWithNullEmail...");
        deleteMailAccessAllowedIP(params, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testDeleteMailAccessAllowedIPWithoutAnyParam() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteMailAccessAllowedIPWithoutAnyParam...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        deleteMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testDeleteAllowedIPWithNoIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithNoIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        // Delete IP
        deleteMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
    }

    @Test
    public void testDeleteAllowedIPWithNullIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithNullIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(null);
        // Delete IP
        deleteMailAccessAllowedIP(params,
                ErrorCode.MXS_INPUT_ERROR.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPWithEmptyIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithEmptyIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        // Delete IP
        deleteMailAccessAllowedIP(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPWithSplCharsInIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithSplCharsInIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*()");
        // Delete IP
        deleteMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPWithInvalidIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithInvalidIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("10.2.23.256");
        // Delete IP
        deleteMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPWithBigIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithBigIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("10.2.23.140.2.43");
        // Delete IP
        deleteMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPWithInvalidIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithInvalidIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("afd00:1:11:5g:221:5aff:fea7:65e0");
        // Delete IP
        deleteMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPWithBigIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPWithBigIPV6...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("fd00:1:11:5a:221:5aff:fea7:65e0:af10");
        // Delete IP
        deleteMailAccessAllowedIP(params,
                MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertTrue("allowedIP does not contain " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPFromEmptyMailAccessAllowedIP() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPFromEmptyMailAccessAllowedIP...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);        
        //createMailAccessAllowedIP(params);
        // Delete IP
        deleteMailAccessAllowedIP(params,
                MailboxError.MBX_MAILACCESS_ALLOWED_IP_NOT_EXIST.name());
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP list is not empty ",
                allowedIP.size()>0);
        params.remove(key);
    }
    
    @Test
    public void testDeleteAllowedIPSuccessWithIPV4() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPSuccessWithIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "10.2.23.140";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        // Delete IP
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        deleteMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

    @Test
    public void testDeleteAllowedIPSuccessWithIPV6() throws Exception {
        System.out
                .println("MailAccessAllowedIPsTest.testDeleteAllowedIPSuccessWithIPV4...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.allowedIP.name();
        String ip = "fd00:1:11:5a:221:5aff:fea7:65e0";
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        // Create IP
        createMailAccessAllowedIP(params);
        // Delete IP
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(ip);
        deleteMailAccessAllowedIP(params);
        // Get the list and compare
        List<String> allowedIP = getMailAccessallowedIP();
        assertNotNull("allowedIP is null.", allowedIP);
        assertFalse("allowedIP still contains " + ip + ".",
                allowedIP.contains(ip));
        params.remove(key);
    }

}
