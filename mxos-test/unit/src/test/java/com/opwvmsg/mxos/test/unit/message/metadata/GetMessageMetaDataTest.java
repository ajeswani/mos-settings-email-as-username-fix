package com.opwvmsg.mxos.test.unit.message.metadata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetMessageMetaDataTest {

    private static final String TEST_NAME = "GetMessageMetaDataTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "messageMetaDataTo@openwave.com";
    private static final String RECEIVED_FROM_VALUE = "messageMetaDataFrom@openwave.com";

    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "messageMetaDataFrom@openwave.com";
    private static final String TOADDRESS_KEY = SmsProperty.toAddress.name();
    
    private static final String PASSWORD = "test5";

    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static final String MESSAGEID_KEY = MessageProperty.messageId
            .name();
    private static final String MESSAGEID_VALUE = "1062d0c4-512c-11e2-a0d7-108a47a179ce";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";

    private static final String MESSAGEID_VALUE_NOMSG = "1005";

    private static final String COS_ID = "default";
    
    private static IMxOSContext context;
    private static IMetadataService metadataService;    
    private static ISendMailService sendMailService;
    private static IMessageService messageService;
    
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        MailboxHelper.createMailbox(EMAIL, PASSWORD, COS_ID);
        MailboxHelper.createMailbox(RECEIVED_FROM_VALUE, PASSWORD, COS_ID);
        sendMailService = (ISendMailService) context
                .getService(ServiceEnum.SendMailService.name());
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageMetaDataService object is null.",
                metadataService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        metadataService = null;
        messageService=null;
        MailboxHelper.deleteMailbox(EMAIL);
        MailboxHelper.deleteMailbox(RECEIVED_FROM_VALUE);
        CosHelper.deleteCos(COS_ID);        
        
    }

    private static void sendMail() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            //sendMailService.process(inputParams);
            messageService.create(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testGetMessageMetaDataSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetMessageMetaDataSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, "INBOX");
            addParams(inputParams, MESSAGEID_KEY,
                    "1062d0c4-512c-11e2-a0d7-108a47a179ce");
            Metadata messageMetaData = metadataService
                    .read(inputParams);
            assertNotNull(messageMetaData);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_NOT_FOUND.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageMetaDataNoEmail() {
        System.out
                .println("GetMessageMetaDataTest.testGetMessageMetaDataNoEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE);
            Metadata message = metadataService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageMetaDataNoMessageId() {
        System.out
                .println("GetMessageMetaDataTest.testGetMessageMetaDataNoMessageId...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Metadata message = metadataService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageMetaDataNoFolder() {
        System.out
                .println("GetMessageMetaDataTest.testGetMessageMetaDataNoFolder...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE);
            Metadata message = metadataService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageMetDataNullEmail() {
        System.out
                .println("GetMessageMetaDataTest.testGetMessageMetDataNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Metadata message = metadataService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageMetaDataInvalidEmail() {
        System.out
                .println("GetMessageMetaDataTest.testGetMessageMetaDataInvalidEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Metadata message = metadataService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }

}
