package com.opwvmsg.mxos.test.unit.mailsend;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailSendGET {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    //private static final String EMAIL = "mx100006@domain2ut203.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailSendGET";
    private static final String ARROW_SEP = " --> ";
    private static IMailSendService mailSendService;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        mailSendService = (IMailSendService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailSendService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("mailSendService object is null.", mailSendService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        mailSendService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static MailSend getParams() {
        MailSend mailSend = null;
        try {
            mailSend = mailSendService.read(params);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                addToParams(params, EMAIL_KEY, EMAIL);
            }
        }
        return mailSend;
    }

    // futureDeliveryEnabled
    @Test
    public void testFutureDeliveryEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testFutureDeliveryEnabled");
        String futureDeliveryEnabled = getParams().getFutureDeliveryEnabled()
                .name();
        assertNotNull("Is null.", futureDeliveryEnabled);
    }

    // maxFutureDeliveryDaysAllowed
    @Test
    public void testMaxFutureDeliveryDaysAllowed() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxFutureDeliveryDaysAllowed");
        Integer maxFutureDeliveryDaysAllowed = getParams()
                .getMaxFutureDeliveryDaysAllowed();
        assertNotNull("Is null.", maxFutureDeliveryDaysAllowed);
    }

    // maxFutureDeliveryMessagesAllowed
    @Test
    public void testMaxFutureDeliveryMessagesAllowed() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxFutureDeliveryMessagesAllowed");
        Integer maxFutureDeliveryMessagesAllowed = getParams()
                .getMaxFutureDeliveryMessagesAllowed();
        assertNotNull("Is null.", maxFutureDeliveryMessagesAllowed);
    }

    // useRichTextEditor
    @Test
    public void testUseRichTextEditor() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testUseRichTextEditor");
        BooleanType useRichTextEditor = getParams().getUseRichTextEditor();
        assertNotNull("Is null.", useRichTextEditor);
    }

    // includeOrginalMailInReply
    @Test
    public void testIncludeOrginalMailInReply() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReply");
        BooleanType includeOrginalMailInReply = getParams()
                .getIncludeOrginalMailInReply();
        assertNotNull("Is null.", includeOrginalMailInReply);
    }

    // originalMailSeperatorCharacter
    @Test
    public void testOriginalMailSeperatorCharacter() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testOriginalMailSeperatorCharacter");
        String originalMailSeperatorCharacter = getParams()
                .getOriginalMailSeperatorCharacter();
        assertNotNull("Is null.", originalMailSeperatorCharacter);
    }

    // autoSaveSentMessages
    @Test
    public void testAutoSaveSentMessages() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoSaveSentMessages");
        BooleanType autoSaveSentMessages = getParams()
                .getAutoSaveSentMessages();
        assertNotNull("Is null.", autoSaveSentMessages);
    }

    // addSignatureForNewMails
    @Test
    public void testAddSignatureForNewMails() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMails");
        BooleanType addSignatureForNewMails = getParams()
                .getAddSignatureForNewMails();
        assertNotNull("Is null.", addSignatureForNewMails);
    }

    // addSignatureInReplyType
    @Test
    public void testAddSignatureInReplyType() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureInReplyType");
        SignatureInReplyType addSignatureInReplyType = getParams()
                .getAddSignatureInReplyType();
        assertNotNull("Is null.", addSignatureInReplyType);
    }
    
    // Signature
    @Test
    public void testSignature() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "Signature");
        String signature = getParams().getSignature();
        System.out.println("signature: "+signature);
        //assertNotNull("Is null.", signature);
    }

    // autoSpellCheckEnabled
    @Test
    public void testAutoSpellCheckEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoSpellCheckEnabled");
        BooleanType autoSpellCheckEnabled = getParams()
                .getAutoSpellCheckEnabled();
        assertNotNull("Is null.", autoSpellCheckEnabled);
    }

    // confirmPromptOnDelete
    @Test
    public void testConfirmPromptOnDelete() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testConfirmPromptOnDelete");
        BooleanType confirmPromptOnDelete = getParams()
                .getConfirmPromptOnDelete();
        assertNotNull("Is null.", confirmPromptOnDelete);
    }

    // includeReturnReceiptReq
    @Test
    public void testIncludeReturnReceiptReq() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReq");
        BooleanType includeReturnReceiptReq = getParams()
                .getIncludeReturnReceiptReq();
        assertNotNull("Is null.", includeReturnReceiptReq);
    }

    // maxSendMessageSizeKB
    @Test
    public void testMaxSendMessageSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxSendMessageSizeKB");
        Integer maxSendMessageSizeKB = getParams().getMaxSendMessageSizeKB();
        assertNotNull("Is null.", maxSendMessageSizeKB);
    }

    // maxAttachmentSizeKB
    @Test
    public void testMaxAttachmentSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxAttachmentSizeKB");
        Integer maxAttachmentSizeKB = getParams().getMaxAttachmentSizeKB();
        assertNotNull("Is null.", maxAttachmentSizeKB);
    }

    // maxAttachments
    @Test
    public void testMaxAttachmentsInSession() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxAttachmentsInSession");
        Integer maxAttachmentsInSession = getParams().getMaxAttachmentsInSession();
        assertNotNull("Is null.", maxAttachmentsInSession);
    }

    // maxCharactersPerPage
    @Test
    public void testMaxCharactersPerPage() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxCharactersPerPage");
        Integer maxCharactersPerPage = getParams().getMaxCharactersPerPage();
        assertNotNull("Is null.", maxCharactersPerPage);
    }
}
