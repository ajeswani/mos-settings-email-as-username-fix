package com.opwvmsg.mxos.test.unit.logging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailForwardService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class LoggingMailboxTest {
    private static IMailboxService loggingMailboxService = null;
    private static IMailboxBaseService loggingMailboxBaseService = null;
    private static ICredentialService loggingCredentialsService = null;
    private static IEmailAliasService loggingEmailAliasService = null;
    private static IMailForwardService loggingForwaringService = null;
    private static IMailReceiptService loggingMailReceiptService = null;
    private static IMailboxBaseService imbs = null;
    private static ICredentialService ics = null;
    private static IEmailAliasService ies = null;
    private static IMailReceiptService imr = null;
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
    private static final String email = "lo000000@openwave.com";
    private static final String emai11 = "lo0000001@openwave.com";
    private static final String password = "pass";
    private static final String cosId="ut_default1";
    
    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp:cos starting......");
        inputParams.clear();
        MailboxHelper.createMailbox(email, password, true);
        CosHelper.createCos(cosId);
        loggingMailboxService = (IMailboxService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.MailboxService.name());
        loggingMailboxBaseService = (IMailboxBaseService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.MailboxBaseService.name());
        loggingCredentialsService = (ICredentialService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.CredentialService.name());
        loggingEmailAliasService = (IEmailAliasService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.MailAliasService.name());
        loggingForwaringService = (IMailForwardService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.MailForwardService.name());
        loggingMailReceiptService = (IMailReceiptService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.MailReceiptService.name());
        
        imbs = (IMailboxBaseService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxBaseService.name());
        ics = (ICredentialService) ContextUtils.loadContext()
                .getService(ServiceEnum.CredentialService.name());
        ies = (IEmailAliasService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailAliasService.name());
        imr = (IMailReceiptService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailReceiptService.name());
        assertNotNull(loggingMailboxService);
        assertNotNull(imbs);
        assertNotNull(ics);
        assertNotNull(imr);
    }

    @AfterClass
    public static void TearDown() throws Exception {
        System.out.println("TearDown:logging starting......");
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        try {
            loggingMailboxService.delete(inputParams);
            MailboxHelper.deleteMailbox(email, true);
            MailboxHelper.deleteMailbox(emai11);
            CosHelper.deleteCos(cosId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParams.clear();
    }

    @Test
    public void testMaaLoggingCreateMailbox() {
        System.out.println("testMaaLoggingCreateMailbox starting ...");
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(emai11);
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(cosId);
        inputParams.put("maxNumAliases", new ArrayList<String>());
        inputParams.get("maxNumAliases").add("5");
        inputParams.put("password", new ArrayList<String>());
        inputParams.get("password").add(password);
        inputParams.put("maxStorageSizeKB", new ArrayList<String>());
        inputParams.get("maxStorageSizeKB").add("1000");
        inputParams.put("maxMsgSize", new ArrayList<String>());
        inputParams.get("maxMsgSize").add("100");
        inputParams.put("locale", new ArrayList<String>());
        inputParams.get("locale").add("de_DE");
        inputParams.put("customFields", new ArrayList<String>());
        inputParams
                .get("customFields")
                .add("bgcFKey:2|bgcPSource:23|bgcNewsPref:4");
        try {
            loggingMailboxService.create(inputParams);
            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(email);
            Base base = imbs.read(inputParams);
            assertNotNull(base);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingStatusUpdate() {
        System.out.println("testMaaLoggingStatusUpdate starting ...");
        String status = MxosEnums.Status.PROXY.name();
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("status", new ArrayList<String>());
        inputParams.get("status").add(status);
        try {
            loggingMailboxBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(email);
            Base base = imbs.read(inputParams);
            assertEquals("Status are updated", base.getStatus(),
                    MxosEnums.Status.PROXY);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingPasswordUpdate() {
        System.out.println("testMaaLoggingPasswordUpdate starting ...");
        String password = "test";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("password", new ArrayList<String>());
        inputParams.get("password").add(password);
        try {
            loggingCredentialsService.update(inputParams);
            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(email);
            Credentials cred = ics.read(inputParams);
            assertEquals("Password is updated", cred.getPassword(), password);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingAliasAdd() {
        System.out.println("testMaaLoggingAliasAdd starting ...");
        boolean test = false;
        String emailAlias = "old@openwave.com";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("emailAlias", new ArrayList<String>());
        inputParams.get("emailAlias").add(emailAlias);
        try {
            loggingEmailAliasService.create(inputParams);
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(email);
            List<String> aliases = ies.read(inputParams);
            for (String alias : aliases) {
                if (alias.equalsIgnoreCase(emailAlias)) {
                    test = true;
                }
            }
            assertTrue(test);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingAliasUpdate() {
        System.out.println("testMaaLoggingAliasUpdate starting ...");
        boolean test = false;
        String newEmailAlias = "newalias@openwave.com";
        String oldEmailAlias = "old@openwave.com";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("newEmailAlias", new ArrayList<String>());
        inputParams.get("newEmailAlias").add(newEmailAlias);
        inputParams.put("oldEmailAlias", new ArrayList<String>());
        inputParams.get("oldEmailAlias").add(oldEmailAlias);
        try {
            loggingEmailAliasService.update(inputParams);
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(email);
            List<String> aliases = ies.read(inputParams);
            for (String alias : aliases) {
                if (alias.equalsIgnoreCase(newEmailAlias)) {
                    test = true;
                }
            }
            assertTrue(test);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingAliasDelete() {
        System.out.println("testMaaLoggingAliasDelete starting ...");
        boolean test = true;
        String deleteEmailAlias = "newalias@openwave.com";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("emailAlias", new ArrayList<String>());
        inputParams.get("emailAlias").add(deleteEmailAlias);
        try {
            loggingEmailAliasService.delete(inputParams);
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(email);
            List<String> aliases = ies.read(inputParams);
            if (aliases != null) {
                for (String alias : aliases) {
                    if (alias.equalsIgnoreCase(deleteEmailAlias)) {
                        test = false;
                    }
                }
            }
            assertTrue(test);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingOutOfOfficeUpdate() {
        System.out.println("testMaaLoggingOutOfOfficeUpdate starting ...");
        String autoReplyMessage = "OOOM";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("autoReplyMessage", new ArrayList<String>());
        inputParams.get("autoReplyMessage").add(autoReplyMessage);
        try {
            loggingMailReceiptService.update(inputParams);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingForwardingUpdate() {
        System.out.println("testMaaLoggingForwardingUpdate starting ...");
        boolean test = false;
        String forwardingAddress = "zz123456fwd@test.com";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("forwardingAddress", new ArrayList<String>());
        inputParams.get("forwardingAddress").add(forwardingAddress);
        try {
            loggingForwaringService.create(inputParams);
            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(email);
            MailReceipt mr = imr.read(inputParams);
            for (String forwarding : mr.getForwardingAddress()) {
                if (forwarding != null
                        && forwarding.equalsIgnoreCase(forwardingAddress)) {
                    test = true;
                }
            }
            assertTrue(test);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

   /* @Test
    public void testMaaLoggingOptionPrefUpdate() {
        System.out.println("testMaaLoggingOptionPrefUpdate starting ...");
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("customFields", new ArrayList<String>());
        inputParams.get("customFields").add("bgcOptionsPref:1|bgcFKey:test");
        try {
            loggingMailboxBaseService.update(inputParams);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    } */

    /*
    public static void createMailboxNeg() {
        System.out.println("createMailboxNeg starting ...");
        inputParams.clear();
        // Mandatory Parameter maxStorageSizeKB missing
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("maxNumAliases", new ArrayList<String>());
        inputParams.get("maxNumAliases").add("5");
        inputParams.put("password", new ArrayList<String>());
        inputParams.get("password").add(password);
        inputParams.put("maxMsgSize", new ArrayList<String>());
        inputParams.get("maxMsgSize").add("100");
        inputParams.put("locale", new ArrayList<String>());
        inputParams.get("locale").add("UNK");
        inputParams.put("customFields", new ArrayList<String>());
        inputParams
                .get("customFields")
                .add("bgcFKey:2|bgcPSource:23|bgcOptionsPriv:10|bgcOptionsPref:0|bgcNewsPref:4|bgcNewsPriv:4|bgcExpireDaysPriv:5|bgcExpireDaysPref:4");
        try {
            long MailboxId = loggingMailboxService.create(inputParams);
            fail();
            System.out.println("Created mailbox with Id: " + MailboxId);
            assertNotNull(MailboxId);
        } catch (MxOSException me) {
            assertEquals("Exception is expected ", me.getCode(),
                    ErrorCode.GEN_BAD_REQUEST.name());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingStatusUpdateNeg() {
        System.out.println("testMaaLoggingStatusUpdateNeg starting ...");
        String status = "Unknown";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("status", new ArrayList<String>());
        inputParams.get("status").add(status);
        try {
            loggingMailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Exception is expected ",
                    MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                    me.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingPasswordUpdateNeg() {
        System.out.println("testMaaLoggingPasswordUpdateNeg starting ...");
        // Invalid Password
        String password = "*##!!";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("password", new ArrayList<String>());
        inputParams.get("password").add(password);
        try {
            loggingMailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Exception is expected",
                    MailboxError.MBX_INVALID_PASSWORD.name(), me.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingAliasAddNeg() {
        System.out.println("testMaaLoggingAliasAddNeg starting ...");
        // Invalid alias, adding primary account
        String emailAlias = "zz123456@test.com";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("emailAlias", new ArrayList<String>());
        inputParams.get("emailAlias").add(emailAlias);
        try {
            loggingEmailAliasService.create(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Exception is expected",
                    MailboxError.MBX_INVALID_ALIAS.name(), me.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingAliasUpdateNeg() {
        System.out.println("testMaaLoggingAliasUpdateNeg starting ...");
        // Updating an nonexisting alias
        String newEmailAlias = "newalias@test.com";
        String oldEmailAlias = "nonExit@test.com";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("newEmailAlias", new ArrayList<String>());
        inputParams.get("newEmailAlias").add(newEmailAlias);
        inputParams.put("oldEmailAlias", new ArrayList<String>());
        inputParams.get("oldEmailAlias").add(oldEmailAlias);
        try {
            loggingEmailAliasService.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Exception is expected",
                    MailboxError.MBX_INVALID_OLD_ALIAS.name(), me.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingAliasDeleteNeg() {
        System.out.println("testMaaLoggingAliasDeleteNeg starting ...");
        // Deleting NonExiting alias
        String deleteEmailAlias = "nonexisting@test.com";
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("deleteEmailAlias", new ArrayList<String>());
        inputParams.get("deleteEmailAlias").add(deleteEmailAlias);
        try {
            loggingEmailAliasService.delete(inputParams);
            fail();
        } catch (MxOSException me) {
            System.out.println(me.getShortMessage());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingOutOfOfficeUpdateNeg() {
        System.out.println("testMaaLoggingOutOfOfficeUpdateNeg starting ...");
        // Adding null autoReplyMessage
        String autoReplyMessage = null;
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("autoReplyMessage", new ArrayList<String>());
        inputParams.get("autoReplyMessage").add(autoReplyMessage);
        try {
            loggingMailReceiptService.update(inputParams);
        } catch (MxOSException me) {
            assertEquals("Exception expected ",
                    MailboxError.MBX_INVALID_AUTO_REPLY_MESSAGE.name(),
                    me.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingForwardingUpdateNeg() {
        System.out.println("testMaaLoggingForwardingUpdateNeg starting ...");
        // INVALID forwarding address
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("forwardingAddress", new ArrayList<String>());
        inputParams.get("forwardingAddress").add(forwardingEmail);
        try {
            loggingForwaringService.create(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Exception expected ",
                    MailboxError.MBX_INVALID_FORWARDING_ADDRESS.name(),
                    me.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testMaaLoggingOptionPrefUpdateNeg() {
        System.out.println("testMaaLoggingOptionPrefUpdateNeg starting ...");
        inputParams.clear();
        // Invalid value
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(email);
        inputParams.put("customFields", new ArrayList<String>());
        inputParams.get("customFields").add("bgcOptionsPref:89");
        try {
            loggingMailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException me) {
            assertEquals("Exception expected ",
                    MailboxError.MBX_INVALID_CUSTOMFIELD.name(), me.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    } */
}
