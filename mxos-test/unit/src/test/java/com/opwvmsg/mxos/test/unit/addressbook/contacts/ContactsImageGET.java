package com.opwvmsg.mxos.test.unit.addressbook.contacts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsImageService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsImageGET {

    private static final String TEST_NAME = "ContactImageGET";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;
    private static long OTHER_CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsImageService contactsImageService;
    private static ExternalSession session;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static void updateContactsActualImage(
            Map<String, List<String>> params) {
        String base64encodeString = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBggGBQkIBwgKCQkKDSIODQwMDRoTFBAWHxwhIB8cHh4jJzItIyUvJR4eK0csLzM1ODI4LS0xQTA2QTI0NTABCQoKDQsNGQ4OGTUkHiQ1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1NTU1Nf/AABEIADAAMAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAFBgEDBAcCAP/EAC0QAAIBAwIEBAUFAAAAAAAAAAECAwAEESExBQYSURMiQfAyYXGBwRQjQpGx/8QAGgEAAgIDAAAAAAAAAAAAAAAABAYCBQABA//EACgRAAEDAwMDAwUAAAAAAAAAAAECAxEABCESMVEFE3FB0fAUImGBwf/aAAwDAQACEQMRAD8A7jUZGcZ1HpU0N4haiOSW5hmFtO4XMuMlgpz0699qitQQNR2qSQknJiiJONTXysGUFSCDsRQG/wCa4baQJHbtMrD4uoAVnh5nS4tjaxRyWspTpjkADhT6HGlbTK2u+nKOaFF0xr7ZVn90xQTidSRoV0YZ2NW1ks41mso3L9bkZMg0OT72rRGzaq+rL69x3rAQRIonByK90lca5ge4vXWEYSJulTnfG/8AdOp2rmXgSNdmHH7nVg570Sxb270m42TnfGPU+PziqfqbzrYSlv1rQZJbwxhiFjHy009itdrwxYCZBqzHfGDjTerEtZbaACAp1Y8w7n61cniEt4mQdwoIxSV1DqutGiyUlLQnAid+InM7gwRXNlspMuSVGinCbgxXQjJ8kmmPnRje4GPRdfudP8NKlrfA8QjjiUtIsoGD9abIkKKSxyzHLGrDoTVw1baH0xnE8EA/2rVhxKwdJ2qXYIhY7AZpN5jspbHiS3scfSkhz3Absac2AZSCMg6EVTJAXiaJwk0bDBWT3rTA2pKVSoSIII5B3FRubcXDemYO4NJ1lxL9VKY3UKcZGDvV17KIYQ3SWbOFxRYcrWnj+LAZ7Zgfh6gR+a0Ly9aGUSTeJMRsrt5R9h+aX3+hW316XmPtajIzM8eD55oNDNz2yhcaufntQTlLhTyXX66VSI00jz/Ju9N9QqhFCqAqgYAGwqaZH3i8vUaLtbdNu3oFf//Z";
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        params.put(AddressBookProperty.sessionId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.sessionId.name()).add(
                session.getSessionId());

        params.put(AddressBookProperty.cookieString.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.cookieString.name()).add(
                session.getCookieString());

        params.put("actualImage", new ArrayList<String>());
        params.get("actualImage").add(base64encodeString);

        setParams(params);
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            AddressBookException expectedError) {

        try {
            contactsImageService.update(params);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    AddressBookError.ABS_INVALID_SIZE_OF_CONTACTS_IMAGE.toString());
        }

    }

    private static Image getImageParams(Map<String, List<String>> params) {
        return getImageParams(params, null);
    }

    private static void getNegImageParams(Map<String, List<String>> params) {
        getImageParams(params, new AddressBookException(
                AddressBookError.ABS_CONTACTS_IMAGE_NOT_FOUND.name()));
    }

    private static Image getImageParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        Image contactImage = null;
        try {
            contactImage = contactsImageService.read(params);
            if (null != expectedError) {
                fail();
            } else {
                assertNotNull("contactsImageService object is null.",
                        contactsImageService);
            }
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    AddressBookError.ABS_CONTACTS_IMAGE_NOT_FOUND.toString());
        }
        return contactImage;
    }

    private static byte[] getActualImageParams(Map<String, List<String>> params) {
        return getActualImageParams(params, null);
    }

    private static void getNegActualImageParams(Map<String, List<String>> params) {
        getActualImageParams(params, new AddressBookException(
                AddressBookError.ABS_CONTACTS_IMAGE_NOT_FOUND.toString()));
    }

    private static byte[] getActualImageParams(
            Map<String, List<String>> params, AddressBookException expectedError) {
        byte[] actualImage = null;
        try {
            actualImage = contactsImageService.readImage(params);
            if (null != expectedError) {
                fail();
            } else {
                assertNotNull("actualImage object is null.", actualImage);
            }
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    AddressBookError.ABS_CONTACTS_IMAGE_NOT_FOUND.toString());
        }
        return actualImage;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsImageService = (IContactsImageService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ContactsImageService.name());
        login(USERID, PASSWORD);
        
        CONTACTID = AddressBookHelper.createContact(USERID, session);
        OTHER_CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;

        externalLoginService = null;
        contactsImageService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
        AddressBookHelper.deleteContact(USERID, OTHER_CONTACTID, session);
    }

    // Image get
    @Test
    public void testContactsImage() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsImage");

        params.clear();
        updateContactsActualImage(params);
        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        params.put(AddressBookProperty.sessionId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.sessionId.name()).add(
                session.getSessionId());

        params.put(AddressBookProperty.cookieString.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.cookieString.name()).add(
                session.getCookieString());

        Image contactImage = getImageParams(params);

        String imageUrl = contactImage.getImageUrl();
        assertNotNull("Is null.", imageUrl);

    }

    // Image get
    @Test
    public void testNegContactsImage() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testNegContactsImage");

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(OTHER_CONTACTID));

        params.put(AddressBookProperty.sessionId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.sessionId.name()).add(
                session.getSessionId());

        params.put(AddressBookProperty.cookieString.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.cookieString.name()).add(
                session.getCookieString());

        getNegImageParams(params);

    }

    // Image get
    @Test
    public void testContactsActualImage() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsActualImage");

        params.clear();
        updateContactsActualImage(params);

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        params.put(AddressBookProperty.sessionId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.sessionId.name()).add(
                session.getSessionId());

        params.put(AddressBookProperty.cookieString.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.cookieString.name()).add(
                session.getCookieString());

        byte[] contactImage = getActualImageParams(params);
        assertNotNull("Is null.", contactImage);

    }

    // Image get
    @Test
    public void testNegContactsActualImage() throws Exception {

        System.out
                .println(TEST_NAME + ARROW_SEP + "testNegContactsActualImage");

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(OTHER_CONTACTID));

        params.put(AddressBookProperty.sessionId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.sessionId.name()).add(
                session.getSessionId());

        params.put(AddressBookProperty.cookieString.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.cookieString.name()).add(
                session.getCookieString());

        getNegActualImageParams(params);

    }

}
