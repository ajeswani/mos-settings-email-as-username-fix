package com.opwvmsg.mxos.test.unit.addressbook.contacts;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsCreateAndMoveMultiple {

    private static final String TEST_NAME = "ContactsCreateAndMoveMultiple";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static List<Long> CONTACTID_LIST = null;

    private static IExternalLoginService externalLoginService;
    private static IContactsService contactsService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsService = (IContactsService) ContextUtils.loadContext()
                .getService(ServiceEnum.ContactsService.name());
        login(USERID, PASSWORD);

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsService = null;
    }

    // create post
    @Test
    public void testContactsCreateMultiple() throws Exception {

        System.out
                .println(TEST_NAME + ARROW_SEP + "testContactsCreateMultiple");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());
            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }

        FileInputStream inputStream = new FileInputStream(home
                + "/config/UnitTest/contacts.txt");
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
        params.put(AddressBookProperty.contactsList.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactsList.name()).add(
                writer.toString());

        CONTACTID_LIST = contactsService.createMultiple(params);
        assertTrue(CONTACTID_LIST.size() > 0);

    }

    // move post
    @Test
    public void testContactsMoveMultiple() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsMoveMultiple");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());
            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }

        String contactIds = "";
        for (int i = 0; i < CONTACTID_LIST.size(); i++) {
            contactIds = contactIds + "contactId=" + CONTACTID_LIST.get(i)
                    + "\r\n";
        }

        try {
            File file = new File(home + "/config/UnitTest/contacts_move.txt");
            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            Writer w = new BufferedWriter(osw);
            w.write(contactIds);
            w.close();
        } catch (IOException e) {
            System.err.println("Problem while writing to contacts_move.txt");
        }

        FileInputStream inputStream = new FileInputStream(home
                + "/config/UnitTest/contacts_move.txt");
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
        params.put(AddressBookProperty.contactsList.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactsList.name()).add(
                writer.toString());

        params.put(AddressBookProperty.folderId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.folderId.name()).add("22");

        params.put(AddressBookProperty.moveToFolderId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.moveToFolderId.name()).add("25");

        contactsService.moveMultiple(params);
    }
}
