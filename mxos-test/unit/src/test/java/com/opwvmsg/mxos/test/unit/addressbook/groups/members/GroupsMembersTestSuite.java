package com.opwvmsg.mxos.test.unit.addressbook.groups.members;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to MailAccess
@SuiteClasses({ GroupsMembersCreateAndDelete.class, GroupsMembersGET.class,
        GroupsMembersDeleteAll.class

})
public class GroupsMembersTestSuite {

}
