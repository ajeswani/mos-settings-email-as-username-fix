package com.opwvmsg.mxos.test.unit.smsservices;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsOnlineService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SmsOnlinePOST {

    private static final String TEST_NAME = "SmsOnlinePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String EMAIL = "test.smsonline@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static ISmsOnlineService smsOnlineService;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        smsOnlineService = (ISmsOnlineService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsOnlineService.name());
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("Object is null.", smsOnlineService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        smsOnlineService = null;
        mailboxService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static SmsOnline getParams() {
        SmsOnline smsOnline = null;
        try {
            smsOnline = smsOnlineService.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return smsOnline;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            smsOnlineService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("MailBox was not created.", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }


    @Test
    public void testSmsOnlineEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledNullParam");
        String key = MailboxProperty.smsOnlineEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsOnlineEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledEmptyParam");
        String key = MailboxProperty.smsOnlineEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsOnlineEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledInvalidParam");
        String key = MailboxProperty.smsOnlineEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams, MailboxError.MBX_INVALID_SMS_ONLINE_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsOnlineEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledSplCharsInParam");
        String key = MailboxProperty.smsOnlineEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams, MailboxError.MBX_INVALID_SMS_ONLINE_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsOnlineEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledSuccess");
        String key = MailboxProperty.smsOnlineEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(
                com.opwvmsg.mxos.backend.crud.ldap.Boolean.Yes.toString());
        updateParams(updateParams);
        String smsServicesEnabled = getParams().getSmsOnlineEnabled().name();
        assertNotNull("Is null.", smsServicesEnabled);
        assertTrue("Has a wrong value.",
                smsServicesEnabled
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSAllowedNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSAllowedNullParam");
        String key = MailboxProperty.internationalSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSAllowedEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSAllowedEmptyParam");
        String key = MailboxProperty.internationalSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSAllowedInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSAllowedInvalidParam");
        String key = MailboxProperty.internationalSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INTERNATIONAL_SMS_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSAllowedSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSAllowedSplCharsInParam");
        String key = MailboxProperty.internationalSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INTERNATIONAL_SMS_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSAllowedSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSAllowedSuccess");
        String key = MailboxProperty.internationalSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(
                com.opwvmsg.mxos.backend.crud.ldap.Boolean.Yes.toString());
        updateParams(updateParams);
        String booleanValue = getParams().getInternationalSMSAllowed().name();
        assertNotNull("Is null.", booleanValue);
        assertTrue("Has a wrong value.",
                booleanValue
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSEnabledNullParam");
        String key = MailboxProperty.internationalSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSEnabledEmptyParam");
        String key = MailboxProperty.internationalSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSEnabledInvalidParam");
        String key = MailboxProperty.internationalSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INTERNALTONAL_SMS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSEnabledSplCharsInParam");
        String key = MailboxProperty.internationalSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INTERNALTONAL_SMS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testInternationalSMSEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationalSMSEnabledSuccess");
        String key = MailboxProperty.internationalSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(
                com.opwvmsg.mxos.backend.crud.ldap.Boolean.Yes.toString());
        updateParams(updateParams);
        String booleanValue = getParams().getInternationalSMSEnabled().name();
        assertNotNull("Is null.", booleanValue);
        assertTrue("Has a wrong value.",
                booleanValue
                .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }

    @Test
    public void testMaxSMSPerDayNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxSMSPerDayNullParam");
        String key = MailboxProperty.maxSMSPerDay.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxSMSPerDayEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxSMSPerDayEmptyParam");
        String key = MailboxProperty.maxSMSPerDay.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxSMSPerDayInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxSMSPerDayInvalidParam");
        String key = MailboxProperty.maxSMSPerDay.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams, MailboxError.MBX_INVALID_MAX_SMS_PER_DAY.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxSMSPerDaySplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxSMSPerDaySplCharsInParam");
        String key = MailboxProperty.maxSMSPerDay.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams, MailboxError.MBX_INVALID_MAX_SMS_PER_DAY.name());
        updateParams.remove(key);
    }

   @Test
    public void testMaxSMSPerDaySuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxSMSPerDaySuccess");
        String key = MailboxProperty.maxSMSPerDay.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("123");
        updateParams(updateParams);
        Integer integerValue = getParams().getMaxSMSPerDay();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 123));
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSAllowedNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSAllowedNullParam");
        String key = MailboxProperty.concatenatedSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSAllowedEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSAllowedEmptyParam");
        String key = MailboxProperty.concatenatedSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSAllowedInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSAllowedInvalidParam");
        String key = MailboxProperty.concatenatedSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSAllowedSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSAllowedSplCharsInParam");
        String key = MailboxProperty.concatenatedSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSAllowedSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSAllowedSuccess");
        String key = MailboxProperty.concatenatedSMSAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(
                com.opwvmsg.mxos.backend.crud.ldap.Boolean.Yes.toString());
        updateParams(updateParams);
        String booleanValue = getParams().getConcatenatedSMSAllowed().name();
        assertNotNull("Is null.", booleanValue);
        assertTrue("Has a wrong value.",
                booleanValue
                .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSEnabledNullParam");
        String key = MailboxProperty.concatenatedSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSEnabledEmptyParam");
        String key = MailboxProperty.concatenatedSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSEnabledInvalidParam");
        String key = MailboxProperty.concatenatedSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSEnabledSplCharsInParam");
        String key = MailboxProperty.concatenatedSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testConcatenatedSMSEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenatedSMSEnabledSuccess");
        String key = MailboxProperty.concatenatedSMSEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(
                com.opwvmsg.mxos.backend.crud.ldap.Boolean.Yes.toString());
        updateParams(updateParams);
        String booleanValue = getParams().getConcatenatedSMSEnabled().name();
        System.out.println("Boolean value : " +booleanValue);
        assertNotNull("Is null.", booleanValue);
        assertTrue("Has a wrong value.",
                booleanValue
                .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsNullParam");
        String key = MailboxProperty.maxConcatenatedSMSSegments.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsEmptyParam");
        String key = MailboxProperty.maxConcatenatedSMSSegments.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsInvalidParam");
        String key = MailboxProperty.maxConcatenatedSMSSegments.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testMaxConcatenatedSMSSegmentsMaxBoundary()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSplCharsInParam");
        String key = MailboxProperty.maxConcatenatedSMSSegments.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("256");
        updateParams(updateParams);
        updateParams.remove(key);
    }
    
    @Test
    public void testMaxConcatenatedSMSSegmentsNegValue()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSplCharsInParam");
        String key = MailboxProperty.maxConcatenatedSMSSegments.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("-1");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testMaxConcatenatedSMSSegmentsOutOfRange()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSplCharsInParam");
        String key = MailboxProperty.maxConcatenatedSMSSegments.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("257");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS.name());
        updateParams.remove(key);
    }
   
   @Test
   public void testMaxConcatenatedSMSSegmentsZero() throws Exception {
       System.out.println(TEST_NAME + ARROW_SEP
               + "testMaxConcatenatedSMSSegmentsSuccess");
       String key = MailboxProperty.maxConcatenatedSMSSegments.name();
       updateParams.put(key, new ArrayList<String>());
       updateParams.get(key).add("0");
       updateParams(updateParams);
       Integer integerValue = getParams().getMaxConcatenatedSMSSegments();
       assertNotNull("Is null.", integerValue);
       assertTrue("Has a wrong value.", (integerValue == 0));
       updateParams.remove(key);
   }
   
   @Test
   public void testMaxConcatenatedSMSSegmentsSuccess() throws Exception {
       System.out.println(TEST_NAME + ARROW_SEP
               + "testMaxConcatenatedSMSSegmentsSuccess");
       String key = MailboxProperty.maxConcatenatedSMSSegments.name();
       updateParams.put(key, new ArrayList<String>());
       updateParams.get(key).add("256");
       updateParams(updateParams);
       Integer integerValue = getParams().getMaxConcatenatedSMSSegments();
       assertNotNull("Is null.", integerValue);
       assertTrue("Has a wrong value.", (integerValue == 256));
       updateParams.remove(key);
   }

    @Test
    public void testMaxPerCaptchaSMSNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSNullParam");
        String key = MailboxProperty.maxPerCaptchaSMS.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaSMSEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSEmptyParam");
        String key = MailboxProperty.maxPerCaptchaSMS.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaSMSInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSInvalidParam");
        String key = MailboxProperty.maxPerCaptchaSMS.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams, MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_SMS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaSMSSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSSplCharsInParam");
        String key = MailboxProperty.maxPerCaptchaSMS.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams, MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_SMS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaSMSSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSSuccess");
        String key = MailboxProperty.maxPerCaptchaSMS.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("12345");
        updateParams(updateParams);
        Integer integerValue = getParams().getMaxPerCaptchaSMS();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 12345));
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsNullParam");
        String key = MailboxProperty.maxPerCaptchaDurationMins.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsEmptyParam");
        String key = MailboxProperty.maxPerCaptchaDurationMins.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsInvalidParam");
        String key = MailboxProperty.maxPerCaptchaDurationMins.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsSplCharsInParam");
        String key = MailboxProperty.maxPerCaptchaDurationMins.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsSuccess");
        String key = MailboxProperty.maxPerCaptchaDurationMins.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("12345");
        updateParams(updateParams);
        Integer integerValue = getParams().getMaxPerCaptchaDurationMins();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 12345));
        updateParams.remove(key);
    }
}
