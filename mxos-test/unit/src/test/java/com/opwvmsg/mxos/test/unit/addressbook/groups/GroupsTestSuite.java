package com.opwvmsg.mxos.test.unit.addressbook.groups;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to MailStore
@SuiteClasses({ GroupsBaseGET.class, GroupsBasePOST.class,
        GroupsCreateAndDelete.class, GroupsCreateAndMoveMultiple.class,
        GroupsDeleteAll.class })
public class GroupsTestSuite {
}
