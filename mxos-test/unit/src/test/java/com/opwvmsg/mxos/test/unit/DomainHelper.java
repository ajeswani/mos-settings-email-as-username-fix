package com.opwvmsg.mxos.test.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.domain.IDomainService;

public class DomainHelper {

    private static final String DOMAIN_KEY = DomainProperty.domain.name();
    private static final String TYPE_KEY = DomainProperty.type.name();

    private static IDomainService domainService;

    static {
        try {
            domainService = (IDomainService) ContextUtils.loadContext()
                    .getService(ServiceEnum.DomainService.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createDomain(String domainName) {
        createDomain(domainName, "local");
    }

    public static void createDomain(String domainName, String type) {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put(DOMAIN_KEY, new ArrayList<String>());
        params.put(TYPE_KEY, new ArrayList<String>());
        params.get(DOMAIN_KEY).add(domainName);
        params.get(TYPE_KEY).add(type);
        try {
            domainService.create(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static void deleteDomain(String domainName) {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.clear();
        params.put(DOMAIN_KEY, new ArrayList<String>());
        params.get(DOMAIN_KEY).add(domainName);
        try {
            domainService.delete(params);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
    }
}
