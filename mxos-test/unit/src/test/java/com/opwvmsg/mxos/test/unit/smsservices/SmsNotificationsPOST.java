package com.opwvmsg.mxos.test.unit.smsservices;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsNotificationsService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SmsNotificationsPOST {

    private static final String TEST_NAME = "SmsOnlinePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String EMAIL = "test.smsnotifications@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static ISmsNotificationsService smsNotificationsService;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        smsNotificationsService = (ISmsNotificationsService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.SmsNotificationsService.name());
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("Object is null.", smsNotificationsService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        smsNotificationsService = null;
        mailboxService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static SmsNotifications getParams() {
        SmsNotifications smsNotifications = null;
        try {
            smsNotifications = smsNotificationsService.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return smsNotifications;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            smsNotificationsService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("MailBox was not created.", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSmsBasicNotificationsEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsBasicNotificationsEnabledNullParam");
        String key = MailboxProperty.smsBasicNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsBasicNotificationsEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsBasicNotificationsEnabledEmptyParam");
        String key = MailboxProperty.smsBasicNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsBasicNotificationsEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsBasicNotificationsEnabledInvalidParam");
        String key = MailboxProperty.smsBasicNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_BASIC_NOTICATIONS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsBasicNotificationsEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsBasicNotificationsEnabledSplCharsInParam");
        String key = MailboxProperty.smsBasicNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_BASIC_NOTICATIONS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsBasicNotificationsEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsBasicNotificationsEnabledSuccess");
        String key = MailboxProperty.smsBasicNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(
                com.opwvmsg.mxos.backend.crud.ldap.Boolean.Yes.toString());
        updateParams(updateParams);
        String booleanValue = getParams().getSmsBasicNotificationsEnabled().name();
        assertNotNull("Is null.", booleanValue);
        assertTrue("Has a wrong value.",
                booleanValue
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }
    
    @Test
    public void testSmsAdvancedNotificationsEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsAdvancedNotificationsEnabledNullParam");
        String key = MailboxProperty.smsAdvancedNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsAdvancedNotificationsEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsAdvancedNotificationsEnabledEmptyParam");
        String key = MailboxProperty.smsAdvancedNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSmsAdvancedNotificationsEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsAdvancedNotificationsEnabledInvalidParam");
        String key = MailboxProperty.smsAdvancedNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_ADVANCED_NOTIFICATIONS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsAdvancedNotificationsEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsAdvancedNotificationsEnabledSplCharsInParam");
        String key = MailboxProperty.smsAdvancedNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SMS_ADVANCED_NOTIFICATIONS_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSmsAdvancedNotificationsEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsAdvancedNotificationsEnabledSuccess");
        String key = MailboxProperty.smsAdvancedNotificationsEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(
                com.opwvmsg.mxos.backend.crud.ldap.Boolean.Yes.toString());
        updateParams(updateParams);
        String booleanValue = getParams().getSmsAdvancedNotificationsEnabled().name();
        assertNotNull("Is null.", booleanValue);
        assertTrue("Has a wrong value.",
                booleanValue
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        updateParams.remove(key);
    }
}
