package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SendMailTest {

    private static final String FROMADDRESS_VALUE = "zz123456@openwave.com";

    private static final String TOADDRESS_VALUE = "raghu1@openwave.com";
    private static final String TOADDRESS_VALUE_USER_NOT_PRESENT = "test@openwave.com";
    
    private static final String SMTP_AUTH_USERNAME = "zz123456@openwave.com";
    private static final String SMTP_AUTH_USER_PASS = "abc123";

    private static final String MESSAGE_VALUE = "Hi";

    private static ISendMailService sendMailService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("SendMailTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null
                || System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        sendMailService = (ISendMailService) ContextUtils.loadContext()
                .getService(ServiceEnum.SendMailService.name());
        assertNotNull("MailboxService object is null.", sendMailService);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("SendMailTest.tearDownAfterClass...");
        params.clear();
        params = null;
        sendMailService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void SendMailTest_fromAddressNotPresent() {
        System.out.println("SendMailTest.fromAddressNotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendMailService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void SendMailTest_toAddressNotPresent() {
        System.out.println("SendMailTest.toAddressNotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendMailService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void SendMailTest_messageNotPresent() {
        System.out.println("SendMailTest.messageNotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            sendMailService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void SendMailTest_fromAddressNotInCorrectFormat() {
        System.out.println("SendMailTest.fromAddressNotInCorrectFormat...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(), "Openwave");
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendMailService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SEND_MAIL_INVALID_FROM_ADDRESS.name(),
                    e.getCode());
        }
    }

    @Test
    public void SendMailTest_toAddressNotInCorrectFormat() {
        System.out.println("SendMailTest.toAddressNotInCorrectFormat...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(), "Openwave");
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendMailService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SEND_MAIL_INVALID_TO_ADDRESS.name(),
                    e.getCode());
        }
    }

    @Test
    public void SendMailTest_Success() {
        System.out.println("SendMailTest.Success...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendMailService.process(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Happened", true);
            e.printStackTrace();
        }
    }
    
    @Test
    public void SendMailTestWithToAddressInHeader_Success() {
        System.out.println("SendMailTestWithToAddressInHeader_Success...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(
                    inputParams,
                    SmsProperty.message.name(),
                    "message=Content-type: text/plain; charset=iso-8859-1\r\nTo: test1@test.com,test2@test.com@test.com\r\nCc: test3@test.com\r\nSubject: Test cc and bcc\r\n\r\n Test Message");
            sendMailService.process(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Happened", true);
            e.printStackTrace();
        }
    }

    @Test
    public void SendMailTest_EmptyMessage() {
        System.out.println("SendMailTest.EmptyMessage...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), "");
            sendMailService.process(inputParams);
        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void SendMailTest_UserNotPresent() {
        System.out.println("SendMailTest.UserNotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE_USER_NOT_PRESENT);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendMailService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SEND_MAIL_ERROR.name(), e.getCode());
        }
    }
    
    @Test
    public void SendMailTest_SampleMultiLineTest() {
        System.out.println("SendMailTest.SampleMultiLineTest...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), "from:raghu\n\nHi");
            sendMailService.process(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Happened", true);
        }
    }
    
    @Test
    public void SendMailTest_UserNameAndPassword() {
        System.out.println("SendMailTest.UserNameAndPassword...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, MailboxProperty.password.name(),
                    SMTP_AUTH_USER_PASS);
            addParams(inputParams, MailboxProperty.smtpAuthUser.name(),
                    SMTP_AUTH_USERNAME);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendMailService.process(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Happened", true);
            e.printStackTrace();
        }
    }

}
