package com.opwvmsg.mxos.test.unit.folder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    CreateFolderTest.class,
    DeleteFolderTest.class,
    UpdateFolderTest.class,
    ReadFolderTest.class
})
public class FolderTestSuite {
}
