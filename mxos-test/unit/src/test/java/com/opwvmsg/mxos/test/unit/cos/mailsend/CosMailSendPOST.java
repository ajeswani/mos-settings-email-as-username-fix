package com.opwvmsg.mxos.test.unit.cos.mailsend;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailSendPOST {

    private static final String TEST_NAME = "CosMailSendRestPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String COS_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "mail_send_cos";

    private static IMxOSContext context;
    private static ICosMailSendService cosMailSendService;
    private static ICosService cosService;
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        cosMailSendService = (ICosMailSendService) context
                .getService(ServiceEnum.CosMailSendService.name());
        cosService = (ICosService) context
        .getService(ServiceEnum.CosService.name());
        createCos(COS_ID);
        updateParams.put(COS_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("cosMailSendService object is null.", cosMailSendService);
        assertNotNull("cosService object is null.", cosService);
        assertNotNull("Input Param:cosId is null.", updateParams.get(COS_KEY));
        updateParams.get(COS_KEY).add(COS_ID);
        assertTrue("Input Param:cosId is empty.", !updateParams.get(COS_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        //updateParams.get(COS_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteCos(COS_ID);
        updateParams.clear();
        updateParams = null;
        cosMailSendService = null;
        cosService = null;
    }


    private static MailSend getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static MailSend getParams(Map<String, List<String>> params,
            String expectedError) {
        MailSend mailSend = null;
        try {
            mailSend = cosMailSendService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("SenderBlocking object is null.", mailSend);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        }
        return mailSend;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
        addParams(updateParams, COS_KEY, COS_ID);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            MailboxError expectedError) {
        try {
            cosMailSendService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private static void createCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COS_KEY, new ArrayList<String>());
        inputParams.get(COS_KEY).add(cosId);
        try {
            cosService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COS_KEY, new ArrayList<String>());
        inputParams.get(COS_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    //    futureDeliveryEnabled
    @Test
    public void testFutureDeliveryEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.futureDeliveryEnabled.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FUTURE_DELIVERY_ENABLED);
    }

    @Test
    public void testFutureDeliveryEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.futureDeliveryEnabled.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FUTURE_DELIVERY_ENABLED);
    }

    @Test
    public void testFutureDeliveryEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.futureDeliveryEnabled.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FUTURE_DELIVERY_ENABLED);
    }

    @Test
    public void testFutureDeliveryEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.futureDeliveryEnabled.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FUTURE_DELIVERY_ENABLED);
    }

    @Test
    public void testFutureDeliveryEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.futureDeliveryEnabled.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getFutureDeliveryEnabled()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testFutureDeliveryEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.futureDeliveryEnabled.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getFutureDeliveryEnabled()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    maxFutureDeliveryDaysAllowed
    @Test
    public void testMaxFutureDeliveryDaysAllowedSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxFutureDeliveryDaysAllowedSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams,
                MailboxProperty.maxFutureDeliveryDaysAllowed.name(), "10");
        updateParams(updateParams);
        int value = getParams(updateParams).getMaxFutureDeliveryDaysAllowed();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", (value == 10));
    }
    //    maxFutureDeliveryMessagesAllowed
    //    useRichTextEditor
    @Test
    public void testUseRichTextEditorNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.useRichTextEditor.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_USE_RICH_TEXT_EDITOR);
    }

    @Test
    public void testUseRichTextEditorEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.useRichTextEditor.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_USE_RICH_TEXT_EDITOR);
    }

    @Test
    public void testUseRichTextEditorInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.useRichTextEditor.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_USE_RICH_TEXT_EDITOR);
    }

    @Test
    public void testUseRichTextEditorSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.useRichTextEditor.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_USE_RICH_TEXT_EDITOR);
    }

    @Test
    public void testUseRichTextEditorSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.useRichTextEditor.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getUseRichTextEditor()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testUseRichTextEditorSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.useRichTextEditor.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getUseRichTextEditor()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    includeOrginalMailInReply
    @Test
    public void testIncludeOrginalMailInReplyNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplyNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeOrginalMailInReply.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY);
    }

    @Test
    public void testIncludeOrginalMailInReplyEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplyEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeOrginalMailInReply.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY);
    }

    @Test
    public void testIncludeOrginalMailInReplyInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplyInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeOrginalMailInReply.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY);
    }

    @Test
    public void testIncludeOrginalMailInReplySplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplySplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeOrginalMailInReply.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY);
    }

    @Test
    public void testIncludeOrginalMailInReplySuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplySuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.includeOrginalMailInReply.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getIncludeOrginalMailInReply()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testIncludeOrginalMailInReplySuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplySuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.includeOrginalMailInReply.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getIncludeOrginalMailInReply()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    originalMailSeperatorCharacter
    //    autoSaveSentMessages
    @Test
    public void testAutoSaveSentMessagesNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSaveSentMessages.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SAVE_MESSAGES);
    }

    @Test
    public void testAutoSaveSentMessagesEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSaveSentMessages.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SAVE_MESSAGES);
    }

    @Test
    public void testAutoSaveSentMessagesInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSaveSentMessages.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SAVE_MESSAGES);
    }

    @Test
    public void testAutoSaveSentMessagesSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSaveSentMessages.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SAVE_MESSAGES);
    }

    @Test
    public void testAutoSaveSentMessagesSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.autoSaveSentMessages.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getAutoSaveSentMessages()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testAutoSaveSentMessagesSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.autoSaveSentMessages.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getAutoSaveSentMessages()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    addSignatureForNewMails
    @Test
    public void testAddSignatureForNewMailsNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.addSignatureForNewMails.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS);
    }

    @Test
    public void testAddSignatureForNewMailsEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.addSignatureForNewMails.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS);
    }

    @Test
    public void testAddSignatureForNewMailsInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.addSignatureForNewMails.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS);
    }

    @Test
    public void testAddSignatureForNewMailsSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.addSignatureForNewMails.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS);
    }

    @Test
    public void testAddSignatureForNewMailsSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.addSignatureForNewMails.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getAddSignatureForNewMails()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testAddSignatureForNewMailsSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.addSignatureForNewMails.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getAddSignatureForNewMails()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    addSignatureInReplyType
    @Test
    public void testAddSignatureInReplyTypeSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureInReplyTypeSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.addSignatureInReplyType.name(),
                MxosEnums.SignatureInReplyType.BEFORE_ORIGINAL_MESSAGE.toString());
        updateParams(updateParams);
        String value = getParams(updateParams).getAddSignatureInReplyType()
                .name();
        assertNotNull("Is null.", value);
        assertTrue(
                "Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.SignatureInReplyType.BEFORE_ORIGINAL_MESSAGE
                        .name()));
    }

    //    autoSpellCheckEnabled
    @Test
    public void testAutoSpellCheckEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSpellCheckEnabled.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SPELL_CHECK_ENABLED);
    }

    @Test
    public void testAutoSpellCheckEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSpellCheckEnabled.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SPELL_CHECK_ENABLED);
    }

    @Test
    public void testAutoSpellCheckEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSpellCheckEnabled.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SPELL_CHECK_ENABLED);
    }

    @Test
    public void testAutoSpellCheckEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.autoSpellCheckEnabled.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SPELL_CHECK_ENABLED);
    }

    @Test
    public void testAutoSpellCheckEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.autoSpellCheckEnabled.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getAutoSpellCheckEnabled()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testAutoSpellCheckEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.autoSpellCheckEnabled.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getAutoSpellCheckEnabled()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    confirmPromptOnDelete
    @Test
    public void testConfirmPromptOnDeleteNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.confirmPromptOnDelete.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONFIRM_PROMT_ON_DELETE);
    }

    @Test
    public void testConfirmPromptOnDeleteEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.confirmPromptOnDelete.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONFIRM_PROMT_ON_DELETE);
    }

    @Test
    public void testConfirmPromptOnDeleteInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.confirmPromptOnDelete.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONFIRM_PROMT_ON_DELETE);
    }

    @Test
    public void testConfirmPromptOnDeleteSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.confirmPromptOnDelete.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONFIRM_PROMT_ON_DELETE);
    }

    @Test
    public void testConfirmPromptOnDeleteSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.confirmPromptOnDelete.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getConfirmPromptOnDelete()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testConfirmPromptOnDeleteSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.confirmPromptOnDelete.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getConfirmPromptOnDelete()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    includeReturnReceiptReq
    @Test
    public void testIncludeReturnReceiptReqNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeReturnReceiptReq.name(),
                null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ);
    }

    @Test
    public void testIncludeReturnReceiptReqEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeReturnReceiptReq.name(),
                "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ);
    }

    @Test
    public void testIncludeReturnReceiptReqInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeReturnReceiptReq.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ);
    }

    @Test
    public void testIncludeReturnReceiptReqSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);
        addParams(updateParams, MailboxProperty.includeReturnReceiptReq.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ);
    }

    @Test
    public void testIncludeReturnReceiptReqSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.includeReturnReceiptReq.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getIncludeReturnReceiptReq()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testIncludeReturnReceiptReqSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.includeReturnReceiptReq.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        String value = getParams(updateParams).getIncludeReturnReceiptReq()
                .name();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
    //    maxSendMessageSizeKB
    @Test
    public void testMaxAttachmentSizeKBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAttachmentSizeKBSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.maxAttachmentSizeKB.name(), "10");
        updateParams(updateParams);
        Integer value = getParams(updateParams).getMaxAttachmentSizeKB();
        assertNotNull("Is null.", value);
    }
    //    maxAttachments
    @Test
    public void testMaxAttachmentsSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAttachmentsSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.maxAttachmentsInSession.name(),
                "10");
        updateParams(updateParams);
        Integer value = getParams(updateParams).getMaxAttachmentsInSession();
        assertNotNull("Is null.", value);
    }
    @Test
    public void testMaxAttachmentsToMessageSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAttachmentsToMessageSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.maxAttachmentsToMessage.name(),
                "5");
        updateParams(updateParams);
        Integer value = getParams(updateParams).getMaxAttachmentsToMessage();
        assertNotNull("Is null.", value);
    }
    //    maxCharactersPerPage
    @Test
    public void testMaxCharactersPerPageSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxCharactersPerPageSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COS_KEY, COS_ID);

        addParams(updateParams, MailboxProperty.maxCharactersPerPage.name(), "10");
        updateParams(updateParams);
        Integer value = getParams(updateParams).getMaxCharactersPerPage();
        assertNotNull("Is null.", value);
    }

    
}
