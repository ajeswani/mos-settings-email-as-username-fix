package com.opwvmsg.mxos.test.unit.addressbook.contacts;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsCreateAndDelete {

    private static final String TEST_NAME = "ContactsCreateAndDelete";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsService contactsService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsService = (IContactsService) ContextUtils.loadContext()
                .getService(ServiceEnum.ContactsService.name());
        login(USERID, PASSWORD);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsService = null;
    }

    // create post
    @Test
    public void testContactsCreate() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsCreate");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());
            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        params.put("firstName", new ArrayList<String>());
        params.get("firstName").add("firstName1");

        params.put("middleName", new ArrayList<String>());
        params.get("middleName").add("middleName1");

        params.put("lastName", new ArrayList<String>());
        params.get("lastName").add("lastName1");

        params.put("displayName", new ArrayList<String>());
        params.get("displayName").add("displayName1");

        params.put("nickName", new ArrayList<String>());
        params.get("nickName").add("nickName1");

		params.put(AddressBookProperty.otherPhone.name(),
				new ArrayList<String>());
		params.get(AddressBookProperty.otherPhone.name()).add("1234567890");

        params.put("suffix", new ArrayList<String>());
        params.get("suffix").add("suffix1");

        params.put("maritalStatus", new ArrayList<String>());
        params.get("maritalStatus").add("Married");

        params.put("spouseName", new ArrayList<String>());
        params.get("spouseName").add("spouseName1");

        params.put("companyName", new ArrayList<String>());
        params.get("companyName").add("companyName1");

        params.put("department", new ArrayList<String>());
        params.get("department").add("department1");

        params.put("manager", new ArrayList<String>());
        params.get("manager").add("manager1");

        params.put("assistant", new ArrayList<String>());
        params.get("assistant").add("assistant1");

        params.put("assistantPhone", new ArrayList<String>());
        params.get("assistantPhone").add("assistantPhone1");

        params.put("employeeId", new ArrayList<String>());
        params.get("employeeId").add("employeeId1");

        params.put("personalInfoCity", new ArrayList<String>());
        params.get("personalInfoCity").add("personalInfoCity1");

        params.put("personalInfoStateOrProvince", new ArrayList<String>());
        params.get("personalInfoStateOrProvince").add(
                "personalInfoStateOrProvince1");

        params.put("personalInfoPostalCode", new ArrayList<String>());
        params.get("personalInfoPostalCode").add("personalInfoPostalCode1");

        params.put("personalInfoCountry", new ArrayList<String>());
        params.get("personalInfoCountry").add("personalInfoCountry1");

        params.put("personalInfoPhone1", new ArrayList<String>());
        params.get("personalInfoPhone1").add("personalInfoPhone11");

        params.put("personalInfoPhone2", new ArrayList<String>());
        params.get("personalInfoPhone2").add("personalInfoPhone2");

        params.put("personalInfoMobile", new ArrayList<String>());
        params.get("personalInfoMobile").add("personalInfoMobile");

        params.put("personalInfoFax", new ArrayList<String>());
        params.get("personalInfoFax").add("personalInfoFax");

        params.put("personalInfoEmail", new ArrayList<String>());
        params.get("personalInfoEmail").add("personalInfoEmail1@test.com");

        params.put("personalInfoImAddress", new ArrayList<String>());
        params.get("personalInfoImAddress").add(
                "personalInfoImAddress1@test.com");

        params.put("workInfoCity", new ArrayList<String>());
        params.get("workInfoCity").add("workInfoCity1");

        params.put("workInfoStateOrProvince", new ArrayList<String>());
        params.get("workInfoStateOrProvince").add("workInfoStateOrProvince1");

        params.put("workInfoPostalCode", new ArrayList<String>());
        params.get("workInfoPostalCode").add("workInfoPostalCode1");

        params.put("workInfoCountry", new ArrayList<String>());
        params.get("workInfoCountry").add("workInfoCountry1");

        params.put("workInfoPhone1", new ArrayList<String>());
        params.get("workInfoPhone1").add("workInfoPhone11");

        params.put("workInfoPhone2", new ArrayList<String>());
        params.get("workInfoPhone2").add("workInfoPhone2");

        params.put("workInfoMobile", new ArrayList<String>());
        params.get("workInfoMobile").add("workInfoMobile");

        params.put("workInfoFax", new ArrayList<String>());
        params.get("workInfoFax").add("workInfoFax");

        params.put("workInfoEmail", new ArrayList<String>());
        params.get("workInfoEmail").add("workInfoEmail1@test.com");

        params.put("workInfoImAddress", new ArrayList<String>());
        params.get("workInfoImAddress").add("workInfoImAddress1@test.com");

        CONTACTID = contactsService.create(params);

        assertTrue(CONTACTID > 0);

    }

    // create post
    @Test
    public void testContactsCreateNegativeCase() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsCreateNegativeCase");

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        params.put("firstName", new ArrayList<String>());
        params.get("firstName")
                .add("firstName1firstName1firstName1firstName1firstName1firstName1firstName1firstName1firstName1firstName1firstName1firstName1firstName11");

        try {
            CONTACTID = contactsService.create(params);
        } catch (Exception e) {
            if (e instanceof MxOSException) {
                // this is expected
            } else {
                fail();
            }
        }
    }

    // delete post
    @Test
    public void testContactsDelete() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsDelete");

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        contactsService.delete(params);
    }
}
