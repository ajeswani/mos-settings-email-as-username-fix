package com.opwvmsg.mxos.test.unit.message.metadata;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.AndTerm;
import com.opwvmsg.mxos.message.search.CcTerm;
import com.opwvmsg.mxos.message.search.FromTerm;
import com.opwvmsg.mxos.message.search.OrTerm;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.message.search.SubjectTerm;
import com.opwvmsg.mxos.message.search.ToTerm;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SearchMessageMetaDataTest {

    private static final String TEST_NAME = "SearchMessageMetaDataTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "messageMetaDataSearchTo@openwave.com";
    private static final String PASSWORD = "test5";
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "messageMetaDataSearchFrom@openwave.com";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static IMxOSContext context;
    private static IMetadataService metadataService;
    private static IMessageService messageService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static final String COS_ID = "default";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        MailboxHelper.createMailbox(EMAIL, PASSWORD, COS_ID);
        MailboxHelper.createMailbox(FROMADDRESS, PASSWORD, COS_ID);
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        // params.put(EMAIL_KEY, new ArrayList<String>());
    }

    private static void sendMail() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            //sendMailService.process(inputParams);
            messageService.create(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("message metadata object is null.",
                metadataService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL);
        MailboxHelper.deleteMailbox(FROMADDRESS);
        params.clear();
        params = null;
        metadataService = null;
        messageService = null; 
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testSearchMessageMetaDataSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            SearchTerm st = new SubjectTerm("test");            
            Map<String, Map<String, Metadata>> message = metadataService
                    .search(inputParams, st);
            System.out.println("message.size() : " + message.size());
            assertTrue("Unable to read messages", (message.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testSearchMessageMetaDataWithNullSearchTerm() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);          
            Map<String, Map<String, Metadata>> message = metadataService
                    .search(inputParams, null);
            System.out.println("message.size() : " + message.size());
            assertTrue("Unable to read messages", (message.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testSearchMessageMetaDataWithOrSearchTerm() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            SearchTerm from = new FromTerm("test@openwave.com");
            SearchTerm sub = new SubjectTerm("test");
            SearchTerm st = new OrTerm(from, sub);
            Map<String, Map<String, Metadata>> message = metadataService
                    .search(inputParams, st);
            System.out.println("message.size() : " + message.size());
            assertTrue("Unable to read messages", (message.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testSearchMessageMetaDataWithComplexSearchTerm() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            String searchFor = "foo@openwave.com";
            SearchTerm from = new FromTerm(searchFor);
            SearchTerm to = new ToTerm(searchFor);
            SearchTerm cc = new CcTerm(searchFor);
            SearchTerm sub = new SubjectTerm("test");
            SearchTerm[] starray = new SearchTerm[3];
            starray[0] = from;
            starray[1] = to;
            starray[2] = cc;
            SearchTerm st = new OrTerm(starray);
            SearchTerm at = new AndTerm(st, sub);
            Map<String, Map<String, Metadata>> message = metadataService
                    .search(inputParams, at);
            System.out.println("message.size() : " + message.size());
            assertTrue("Unable to read messages", (message.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }


    @Test
    public void testSearchMessageMetaDataWithAndSearchTerm() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            SearchTerm from = new FromTerm("test@openwave.com");
            SearchTerm sub = new SubjectTerm("test");
            SearchTerm st = new AndTerm(from, sub);
            Map<String, Map<String, Metadata>> message = metadataService
                    .search(inputParams, st);
            System.out.println("message.size() : " + message.size());
            assertTrue("Unable to read messages", (message.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testListMessageMetaDataSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);

            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            System.out.println("message.size() : " + message.size());
            assertTrue("Unable to read messages", (message.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    @Test
    public void testSearchMessageMetaDataNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSearchMessageMetaDataNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSearchMessageMetaDataNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataNullEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSearchMessageMetaDataInvalidEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSearchMessageMetaDataInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }

}
