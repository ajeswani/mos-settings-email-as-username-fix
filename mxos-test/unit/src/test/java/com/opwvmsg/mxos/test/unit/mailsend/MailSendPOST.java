package com.opwvmsg.mxos.test.unit.mailsend;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailSendPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    //private static final String EMAIL = "mx100006@domain2ut203.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailSendPOST";
    private static final String ARROW_SEP = " --> ";
    private static IMailSendService mailSendService;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        mailSendService = (IMailSendService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailSendService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("mailSendService object is null.", mailSendService);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        mailSendService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static MailSend getParams() {
        MailSend mailSend = null;
        try {
            mailSend = mailSendService.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return mailSend;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            MailboxError expectedError) {
        try {
            mailSendService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    // fromAddress
    // futureDeliveryEnabled
    @Test
    public void testFutureDeliveryEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledNullParam");
        String key = MailboxProperty.futureDeliveryEnabled.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testFutureDeliveryEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledEmptyParam");

        String key = MailboxProperty.futureDeliveryEnabled.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testFutureDeliveryEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledInvalidParam");
        String key = MailboxProperty.futureDeliveryEnabled.name();
        addToParams(updateParams, key, "fasdfasef");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FUTURE_DELIVERY_ENABLED);
        updateParams.remove(key);
    }

    @Test
    public void testFutureDeliveryEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledSplCharsInParam");
        String key = MailboxProperty.futureDeliveryEnabled.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_FUTURE_DELIVERY_ENABLED);
        updateParams.remove(key);
    }

    @Test
    public void testFutureDeliveryEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledSuccess");
        String key = MailboxProperty.futureDeliveryEnabled.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getFutureDeliveryEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    @Test
    public void testFutureDeliveryEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testFutureDeliveryEnabledSuccess1");
        String key = MailboxProperty.futureDeliveryEnabled.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getFutureDeliveryEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    // maxFutureDeliveryDaysAllowed
    // maxFutureDeliveryMessagesAllowed
    // alternateFromAddess
    // replyToAddress
    // useRichTextEditor
    @Test
    public void testUseRichTextEditorNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorNullParam");
        String key = MailboxProperty.useRichTextEditor.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testUseRichTextEditorEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorEmptyParam");
        String key = MailboxProperty.useRichTextEditor.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testUseRichTextEditorInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorInvalidParam");
        String key = MailboxProperty.useRichTextEditor.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_USE_RICH_TEXT_EDITOR);
        updateParams.remove(key);
    }

    @Test
    public void testUseRichTextEditorSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorSplCharsInParam");
        String key = MailboxProperty.useRichTextEditor.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_USE_RICH_TEXT_EDITOR);
        updateParams.remove(key);
    }

    @Test
    public void testUseRichTextEditorSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorSuccess");
        String key = MailboxProperty.useRichTextEditor.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getUseRichTextEditor();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    @Test
    public void testUseRichTextEditorSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUseRichTextEditorSuccess1");
        String key = MailboxProperty.useRichTextEditor.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getUseRichTextEditor();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    // includeOrginalMailInReply
    @Test
    public void testIncludeOrginalMailInReplyNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplyNullParam");
        String key = MailboxProperty.includeOrginalMailInReply.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeOrginalMailInReplyEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplyEmptyParam");
        String key = MailboxProperty.includeOrginalMailInReply.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeOrginalMailInReplyInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplyInvalidParam");
        String key = MailboxProperty.includeOrginalMailInReply.name();
        addToParams(updateParams, key, "fasdfefsfd");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeOrginalMailInReplySplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplySplCharsInParam");
        String key = MailboxProperty.includeOrginalMailInReply.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeOrginalMailInReplySuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplySuccess");
        String key = MailboxProperty.includeOrginalMailInReply.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getIncludeOrginalMailInReply();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeOrginalMailInReplySuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOrginalMailInReplySuccess1");
        String key = MailboxProperty.includeOrginalMailInReply.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getIncludeOrginalMailInReply();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // originalMailSeperatorCharacter
    // autoSaveSentMessages
    @Test
    public void testAutoSaveSentMessagesNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesNullParam");
        String key = MailboxProperty.autoSaveSentMessages.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSaveSentMessagesEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesEmptyParam");
        String key = MailboxProperty.autoSaveSentMessages.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSaveSentMessagesInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesInvalidParam");
        String key = MailboxProperty.autoSaveSentMessages.name();
        addToParams(updateParams, key, "dfasdfss");
        updateParams(updateParams, MailboxError.MBX_INVALID_AUTO_SAVE_MESSAGES);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSaveSentMessagesSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesSplCharsInParam");
        String key = MailboxProperty.autoSaveSentMessages.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams, MailboxError.MBX_INVALID_AUTO_SAVE_MESSAGES);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSaveSentMessagesSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesSuccess");
        String key = MailboxProperty.autoSaveSentMessages.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getAutoSaveSentMessages();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSaveSentMessagesSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSaveSentMessagesSuccess1");
        String key = MailboxProperty.autoSaveSentMessages.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getAutoSaveSentMessages();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    // addSignatureForNewMails
    @Test
    public void testAddSignatureForNewMailsNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsNullParam");
        String key = MailboxProperty.addSignatureForNewMails.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAddSignatureForNewMailsEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsEmptyParam");
        String key = MailboxProperty.addSignatureForNewMails.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAddSignatureForNewMailsInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsInvalidParam");
        String key = MailboxProperty.addSignatureForNewMails.name();
        addToParams(updateParams, key, "fasdfas");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS);
        updateParams.remove(key);
    }

    @Test
    public void testAddSignatureForNewMailsSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsSplCharsInParam");
        String key = MailboxProperty.addSignatureForNewMails.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS);
        updateParams.remove(key);
    }

    @Test
    public void testAddSignatureForNewMailsSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsSuccess");
        String key = MailboxProperty.addSignatureForNewMails.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getAddSignatureForNewMails();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testAddSignatureForNewMailsSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMailsSuccess1");
        String key = MailboxProperty.addSignatureForNewMails.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getAddSignatureForNewMails();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // addSignatureInReplyType
    
    // autoSpellCheckEnabled
    @Test
    public void testAutoSpellCheckEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledNullParam");
        String key = MailboxProperty.autoSpellCheckEnabled.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSpellCheckEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledEmptyParam");
        String key = MailboxProperty.autoSpellCheckEnabled.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSpellCheckEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledInvalidParam");
        String key = MailboxProperty.autoSpellCheckEnabled.name();
        addToParams(updateParams, key, "sadfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SPELL_CHECK_ENABLED);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSpellCheckEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledSplCharsInParam");
        String key = MailboxProperty.autoSpellCheckEnabled.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_AUTO_SPELL_CHECK_ENABLED);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSpellCheckEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledSuccess");
        String key = MailboxProperty.autoSpellCheckEnabled.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getAutoSpellCheckEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testAutoSpellCheckEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAutoSpellCheckEnabledSuccess1");
        String key = MailboxProperty.autoSpellCheckEnabled.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getAutoSpellCheckEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // confirmPromptOnDelete
    @Test
    public void testConfirmPromptOnDeleteNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteNullParam");
        String key = MailboxProperty.confirmPromptOnDelete.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testConfirmPromptOnDeleteEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteEmptyParam");
        String key = MailboxProperty.confirmPromptOnDelete.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testConfirmPromptOnDeleteInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteInvalidParam");
        String key = MailboxProperty.confirmPromptOnDelete.name();
        addToParams(updateParams, key, "sadfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONFIRM_PROMT_ON_DELETE);
        updateParams.remove(key);
    }

    @Test
    public void testConfirmPromptOnDeleteSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteSplCharsInParam");
        String key = MailboxProperty.confirmPromptOnDelete.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CONFIRM_PROMT_ON_DELETE);
        updateParams.remove(key);
    }

    @Test
    public void testConfirmPromptOnDeleteSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteSuccess");
        String key = MailboxProperty.confirmPromptOnDelete.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getConfirmPromptOnDelete();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testConfirmPromptOnDeleteSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConfirmPromptOnDeleteSuccess1");
        String key = MailboxProperty.confirmPromptOnDelete.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getConfirmPromptOnDelete();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // includeReturnReceiptReq
    @Test
    public void testIncludeReturnReceiptReqNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqNullParam");
        String key = MailboxProperty.includeReturnReceiptReq.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeReturnReceiptReqEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqEmptyParam");
        String key = MailboxProperty.includeReturnReceiptReq.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeReturnReceiptReqInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqInvalidParam");
        String key = MailboxProperty.includeReturnReceiptReq.name();
        addToParams(updateParams, key, "fgasdASD");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeReturnReceiptReqSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqSplCharsInParam");
        String key = MailboxProperty.includeReturnReceiptReq.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeReturnReceiptReqSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqSuccess");
        String key = MailboxProperty.includeReturnReceiptReq.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getIncludeReturnReceiptReq();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testIncludeReturnReceiptReqSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReqSuccess1");
        String key = MailboxProperty.includeReturnReceiptReq.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getIncludeReturnReceiptReq();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }
    
    // signature
    @Test
    public void testSignature() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSignature");
        String key = MailboxProperty.signature.name();
        String signature = "Signature 1";
        addToParams(updateParams, key, signature);
        updateParams(updateParams);
        String sign = getParams().getSignature();
        assertNotNull("Is null.", sign);
        assertEquals("Has a wrong value.", sign, signature);
        System.out.println("signature: "+sign);
        updateParams.remove(key);
    }
    
    // DELETE signature
    @Test
    public void testDeleteSignature() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteSignature");
        String key = MailboxProperty.signature.name();
        //String signature = "";
        String signature = null;
        addToParams(updateParams, key, signature);
        updateParams(updateParams);
        String sign = getParams().getSignature();
        System.out.println("signature: "+sign);
        assertEquals("Has a wrong value.", sign, signature);
        updateParams.remove(key);
    }
    // maxSendMessageSizeKB
    // maxAttachmentSizeKB
    // maxAttachmentsInSession
    @Test
    public void testMaxAttachmentsInSession() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAttachmentsInSession");
        String key = MailboxProperty.maxAttachmentsInSession.name();
        String maxAttachmentsInSession = "5";
        addToParams(updateParams, key, maxAttachmentsInSession);
        updateParams(updateParams);
        Integer maxAttachmentsInSession1 = getParams().getMaxAttachmentsInSession();
        assertNotNull("Is null.", maxAttachmentsInSession1);
        assertEquals("Has a wrong value.", maxAttachmentsInSession1.toString(), maxAttachmentsInSession);
        updateParams.remove(key);
    }
    // maxAttachmentsToMessage
    @Test
    public void testMaxAttachmentsToMessage() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAttachmentsToMessage");
        String key = MailboxProperty.maxAttachmentsToMessage.name();
        String maxAttachmentsToMessage = "5";
        addToParams(updateParams, key, maxAttachmentsToMessage);
        updateParams(updateParams);
        Integer maxAttachmentsToMessage1 = getParams().getMaxAttachmentsToMessage();
        assertNotNull("Is null.", maxAttachmentsToMessage1);
        assertEquals("Has a wrong value.", maxAttachmentsToMessage1.toString(), maxAttachmentsToMessage);
        updateParams.remove(key);
    }
    // maxCharactersPerPage
}
