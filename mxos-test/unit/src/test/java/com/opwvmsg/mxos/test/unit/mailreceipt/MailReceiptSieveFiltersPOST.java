package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BlockedSenderAction;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailReceiptSieveFiltersPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailReceiptSieveFiltersGET";
    private static final String ARROW_SEP = " --> ";
    private static IMailReceiptSieveFiltersService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IMailReceiptSieveFiltersService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailReceiptSieveFiltersService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static SieveFilters getParams() {
        SieveFilters object = null;
        try {
            object = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return object;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSieveFilteringEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSieveFilteringEnabledNullParam");
        String key = MailboxProperty.sieveFilteringEnabled.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSieveFilteringEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSieveFilteringEnabledEmptyParam");
        String key = MailboxProperty.sieveFilteringEnabled.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSieveFilteringEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSieveFilteringEnabledInvalidParam");
        String key = MailboxProperty.sieveFilteringEnabled.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SIEVE_FILTERING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSieveFilteringEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSieveFilteringEnabledSplCharsInParam");
        String key = MailboxProperty.sieveFilteringEnabled.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SIEVE_FILTERING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSieveFilteringEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSieveFilteringEnabledSuccess");
        String key = MailboxProperty.sieveFilteringEnabled.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSieveFilteringEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testSieveFilteringEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSieveFilteringEnabledSuccess1");
        String key = MailboxProperty.sieveFilteringEnabled.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSieveFilteringEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    @Test
    public void testMtaFilterNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMtaFilterNullParam");
        String key = MailboxProperty.mtaFilter.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMtaFilterEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMtaFilterEmptyParam");
        String key = MailboxProperty.mtaFilter.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMtaFilterInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMtaFilterInvalidParam");
        String key = MailboxProperty.mtaFilter.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMtaFilterSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMtaFilterSplCharsInParam");
        String key = MailboxProperty.mtaFilter.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMtaFilterSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMtaFilterSuccess");
        String key = MailboxProperty.mtaFilter.name();
        String val = "testMTAFilter";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getMtaFilter();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

    @Test
    public void testRmFilterNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRmFilterNullParam");
        String key = MailboxProperty.rmFilter.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRmFilterEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRmFilterEmptyParam");
        String key = MailboxProperty.rmFilter.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRmFilterInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRmFilterInvalidParam");
        String key = MailboxProperty.rmFilter.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRmFilterSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRmFilterSplCharsInParam");
        String key = MailboxProperty.rmFilter.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRmFilterSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRmFilterSuccess");
        String key = MailboxProperty.rmFilter.name();
        String val = "testRmFilter";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getRmFilter();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionNullParam");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionEmptyParam");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionInvalidParam");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SIEVE_FILTERS_BLOCKED_SENDER_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionSplCharsInParam");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SIEVE_FILTERS_BLOCKED_SENDER_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionSuccess");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, BlockedSenderAction.BOUNCE.name());
        updateParams(updateParams);
        BlockedSenderAction value = getParams().getBlockedSenderAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BlockedSenderAction.BOUNCE);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionSuccess1");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, BlockedSenderAction.REJECT.name());
        updateParams(updateParams);
        BlockedSenderAction value = getParams().getBlockedSenderAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BlockedSenderAction.REJECT);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionSuccess2");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, BlockedSenderAction.SIDELINE.name());
        updateParams(updateParams);
        BlockedSenderAction value = getParams().getBlockedSenderAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BlockedSenderAction.SIDELINE);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionSuccess3");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, BlockedSenderAction.TOSS.name());
        updateParams(updateParams);
        BlockedSenderAction value = getParams().getBlockedSenderAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BlockedSenderAction.TOSS);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderActionSuccess4() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderActionSuccess4");
        String key = MailboxProperty.blockedSenderAction.name();
        addToParams(updateParams, key, BlockedSenderAction.NONE.name());
        updateParams(updateParams);
        BlockedSenderAction value = getParams().getBlockedSenderAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BlockedSenderAction.NONE);
        updateParams.remove(key);
    }

    // blockedSenderMessage
    @Test
    public void testBlockedSenderMessageNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderMessageNullParam");
        String key = MailboxProperty.blockedSenderMessage.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderMessageEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderMessageEmptyParam");
        String key = MailboxProperty.blockedSenderMessage.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderMessageInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderMessageInvalidParam");
        String key = MailboxProperty.blockedSenderMessage.name();
        addToParams(updateParams, key, "asdfasdf");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderMessageSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderMessageSplCharsInParam");
        String key = MailboxProperty.blockedSenderMessage.name();
        addToParams(updateParams, key, "!#@$^%&");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSenderMessageSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSenderMessageSuccess");
        String key = MailboxProperty.blockedSenderMessage.name();
        String val = "testMessage";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getBlockedSenderMessage();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

    // rejectBouncedMessage
    @Test
    public void testRejectBouncedMessageNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectBouncedMessageNullParam");
        String key = MailboxProperty.rejectBouncedMessage.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectBouncedMessageEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectBouncedMessageEmptyParam");
        String key = MailboxProperty.rejectBouncedMessage.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectBouncedMessageInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectBouncedMessageInvalidParam");
        String key = MailboxProperty.rejectBouncedMessage.name();
        addToParams(updateParams, key, "asdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SIEVE_FILTERS_REJECT_BOUNCED_MESSAGE.name());
        updateParams.remove(key);
    }

    @Test
    public void testRejectBouncedMessageSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectBouncedMessageSplCharsInParam");
        String key = MailboxProperty.rejectBouncedMessage.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SIEVE_FILTERS_REJECT_BOUNCED_MESSAGE.name());
        updateParams.remove(key);
    }

    @Test
    public void testRejectBouncedMessageSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectBouncedMessageSuccess");
        String key = MailboxProperty.rejectBouncedMessage.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getRejectBouncedMessage();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testRejectBouncedMessageSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectBouncedMessageSuccess1");
        String key = MailboxProperty.rejectBouncedMessage.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getRejectBouncedMessage();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

}
