package com.opwvmsg.mxos.test.unit.webmailfeatures.addressbook;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesAddressBookService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateWebMailAddressBookFeaturesTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100013@openwave.com";
    private static final String PASSWORD = "test1";
    private static IWebMailFeaturesAddressBookService wmfabs;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateWebMailAddressBookFeaturesTest.setUpBeforeClass...");
        wmfabs = (IWebMailFeaturesAddressBookService) ContextUtils.loadContext()
                .getService(ServiceEnum.WebMailFeaturesAddressBookService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateWebMailAddressBookFeaturesTest.setUp...");
        assertNotNull("WebMailFeaturesServiceAB object is null.", wmfabs);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateWebMailAddressBookFeaturesTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateWebMailAddressBookFeaturesTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        wmfabs = null;
    }

    private static AddressBookFeatures getWebMailFeaturesParams() {
        AddressBookFeatures ab = null;
        try {
            ab = wmfabs.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("WebMailFeaturesAB object is null.", ab);
        return ab;
    }

    private static void updateWebMailFeaturesParams(
            Map<String, List<String>> updateParams) {
        updateWebMailFeaturesParams(updateParams, null);
    }

    private static void updateWebMailFeaturesParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            wmfabs.update(updateParams);
            if (null != expectedError) {
                fail("1. This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("2. This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                updateParams.put(EMAIL_KEY, new ArrayList<String>());
                updateParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWebMailFeaturesAddressBookWithoutEmail() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testUpdateWebMailFeaturesWithoutEmail...");
        updateWebMailFeaturesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateWebMailFeaturesAddressBookWithEmptyEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add("");
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testUpdateWebMailFeaturesWithEmptyEmail...");
        updateWebMailFeaturesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateWebMailFeaturesAddressBookWithNullEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(null);
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testUpdateWebMailFeaturesWithNullEmail...");
        updateWebMailFeaturesParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateWebMailFeaturesAddressBookWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testUpdateMailAccessWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateWebMailFeaturesParams(updateParams,
                ErrorCode.GEN_BAD_REQUEST.name());
    }

    

    @Test
    public void testSetMaxContactsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxContactsSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxContacts.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("201");
        updateWebMailFeaturesParams(updateParams);
        int maxContacts = getWebMailFeaturesParams()
                .getMaxContacts();
        System.out.println("Max Contacts " +maxContacts);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetMaxGroupsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxGroupsSuccess..");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxGroups.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("51");
        updateWebMailFeaturesParams(updateParams);
        int maxGroups = getWebMailFeaturesParams()
                .getMaxGroups();
        System.out.println("Max Groups " +maxGroups);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetSetMaxContactsPerGroupsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetMaxContactsPerGroupsSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxContactsPerGroup.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("21");
        updateWebMailFeaturesParams(updateParams);
        int maxContactsPG = getWebMailFeaturesParams()
                .getMaxContactsPerGroup();
        System.out.println("Max ContactsPerGroups " +maxContactsPG);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetSetMaxContactsPerPageSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetMaxContactsPerPageSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxContactsPerPage.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("16");
        updateWebMailFeaturesParams(updateParams);
        int maxContactsPP = getWebMailFeaturesParams()
                .getMaxContactsPerPage();
        System.out.println("Max ContactsPerPage " +maxContactsPP);
        updateParams.remove(key);
    }
    
    @Test
    public void testSetCreateContactsFromOutgoingEmailsSuccess() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetCreateContactsFromOutgoingEmailsSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.createContactsFromOutgoingEmails.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("disabled");
        updateWebMailFeaturesParams(updateParams);
        String contFromOutgoingEmails = getWebMailFeaturesParams()
                .getCreateContactsFromOutgoingEmails().toString();
        System.out.println("CreateContactsFromOutgoingEmails " +contFromOutgoingEmails);
        updateParams.remove(key);
    }
    
    
    @Test
    public void testSetMaxContactsFailure() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxContactsFailure...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxContacts.name();
        updateParams.put(key, new ArrayList<String>());
        String wrong_value = "null";
        updateParams.get(key).add(wrong_value);
        try {
            wmfabs.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
    }
    
    @Test
    public void testSetMaxGroupsFailure() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetMaxGroupsFailure..");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxGroups.name();
        updateParams.put(key, new ArrayList<String>());
        String wrong_value = "null";
        updateParams.get(key).add(wrong_value);
        try {
            wmfabs.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
    }
    
    @Test
    public void testSetSetMaxContactsPerGroupsFailure() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetMaxContactsPerGroupsFailure...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxContactsPerGroup.name();
        updateParams.put(key, new ArrayList<String>());
        String wrong_value = "null";
        updateParams.get(key).add(wrong_value);
        try {
            wmfabs.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
    }
    
    @Test
    public void testSetSetMaxContactsPerPageFailure() throws Exception {
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetSetMaxContactsPerPageFailure...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxContactsPerPage.name();
        updateParams.put(key, new ArrayList<String>());
        String wrong_value = "null";
        updateParams.get(key).add(wrong_value);
        try {
            wmfabs.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
        updateParams.remove(key);
    }
    
    @Test
    public void testSetCreateContactsFromOutgoingEmailsFailure() throws Exception {
        String wrong_value = "wrong_value";
        System.out
                .println("UpdateWebMailAddressBookFeaturesTest.testSetCreateContactsFromOutgoingEmailsFailure...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.createContactsFromOutgoingEmails.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(wrong_value);
        try {
            wmfabs.update(updateParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            System.out.println("Message : "+e.getCode());
        }
        updateParams.remove(key);
    }

    

}
