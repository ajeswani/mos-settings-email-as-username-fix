package com.opwvmsg.mxos.test.unit.message.metadata;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateMessageMetaDataTest {

    private static final String TEST_NAME = "UpdateMessageMetaDataTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "messageMetaDataUpdateTo@openwave.com";
    private static final String PASSWORD = "test5";
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "messageMetaDataUpdateFrom@openwave.com";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static IMxOSContext context;
    private static IMetadataService metadataService;
    private static IMessageService messageService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static final String COS_ID = "default";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        MailboxHelper.createMailbox(EMAIL, PASSWORD, COS_ID);
        MailboxHelper.createMailbox(FROMADDRESS, PASSWORD, COS_ID);
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        sendMail();
        // params.put(EMAIL_KEY, new ArrayList<String>());
    }

    private static void sendMail() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            //sendMailService.process(inputParams);
            messageService.create(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("message metadata object is null.",
                metadataService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL);
        MailboxHelper.deleteMailbox(FROMADDRESS);
        params.clear();
        params = null;
        metadataService = null;
        messageService = null; 
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testUpdateMessageMetaDataSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);

            // Need to get the message UID's ans need to send as a paramter. 
            //TODO Not completed.
            
            
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testUpdateMessageMetaDataFailure() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageMetaDataFailure");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            
            
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
}
