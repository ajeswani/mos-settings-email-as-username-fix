package com.opwvmsg.mxos.test.unit.cos.webmailfeatures;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to WebMailFeatures for Cos service. 
@SuiteClasses({
    GetCosWebMailFeaturesTest.class,
    UpdateCosWebMailFeaturesTest.class
})
public class CosWebMailFeaturesSuite {
}
