package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedDomainService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class AllowedDomainsTest {
    private static long MAILBOX_ID;
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "bstestuser100@test.com";
    private static final String PWD = "test";
    private static final String COSID = "cos_2.0_ut201";
    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static IAllowedDomainService allowedDomainService;
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("AllowedDomainsTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        allowedDomainService = (IAllowedDomainService) context
                .getService(ServiceEnum.AllowedDomainService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        assertNotNull("allowedDomainService object is null.", allowedDomainService);

        CosHelper.createCos(COSID);
        MAILBOX_ID = MailboxHelper.createMailbox(EMAIL, PWD, COSID);
        //assertTrue("Mailbox NOT Created", (MAILBOX_ID > 0));
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("AllowedDomainsTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("AllowedDomainsTest.tearDown...");
        inputParams.clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("AllowedDomainsTest.tearDownAfterClass...");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            //mailboxService.delete(inputParams);
            MailboxHelper.deleteMailbox(EMAIL);
            CosHelper.deleteCos(COSID);
        } catch (Exception e) {
            fail();
        }
        inputParams = null;
        mailboxService = null;
        allowedDomainService = null;
    }

    @Test
    public void testAddAllowedDomain() throws Exception {
        System.out.println("AllowedDomainsTest.testAddAllowedDomain...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.allowedDomain.name(),
                new ArrayList<String>());
        String domain = "test.com";
        inputParams.get(MailboxProperty.allowedDomain.name()).add(domain);
        try {
            allowedDomainService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            List<String> ads = allowedDomainService.read(inputParams);
            assertNotNull(ads);
            assertTrue(ads.contains(domain));
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }
    @Test
    public void testAddMultipleAllowedDomain() throws Exception {
        System.out.println("AllowedDomainsTest.testAddAllowedDomain...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.allowedDomain.name(),
                new ArrayList<String>());
        String domain = "test.com";
        String domain2="openwave.com";
        inputParams.get(MailboxProperty.allowedDomain.name()).add(domain);
        inputParams.get(MailboxProperty.allowedDomain.name()).add(domain2);
        try {
            allowedDomainService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            List<String> ads = allowedDomainService.read(inputParams);
            if (ads.size() < 2) {
                fail();
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

}
