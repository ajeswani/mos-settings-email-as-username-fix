package com.opwvmsg.mxos.test.unit.cos.internalinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosInternalInfoService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetCosInternalInfoTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosInternalInfoService info;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetCosInternalInfoTest.setUpBeforeClass...");
        info = (ICosInternalInfoService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosInternalInfoService.name());
        boolean flag = CosHelper.createCos(COS_ID);
        assertTrue("Cos was not created.", flag);
        params.put(COSID_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetCosInternalInfoTest.setUp...");
        assertNotNull("InternalInfoService object is null.", info);
        assertNotNull("Input Param:cosId is null.", params.get(COSID_KEY));
        params.get(COSID_KEY).add(COS_ID);
        assertTrue("Input Param:cosId is empty.", !params.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetCosInternalInfoTest.tearDown...");
        params.get(COSID_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetCosInternalInfoTest.tearDownAfterClass...");
        CosHelper.deleteCos(COS_ID);
        params.clear();
        params = null;
        info = null;
    }

    private static InternalInfo getCosInternalInfoParams(
            Map<String, List<String>> params) {
        return getCosInternalInfoParams(params, null);
    }

    private static InternalInfo getCosInternalInfoParams(
            Map<String, List<String>> params, String expectedError) {
        InternalInfo obj = null;
        try {
            obj = info.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("InternalInfo object is null.", obj);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(COSID_KEY, new ArrayList<String>());
            }
        }
        return obj;
    }

    @Test
    public void testGetCosInternalInfoWithoutCosId() {
        System.out
                .println("GetCosInternalInfoTest.testGetCosInternalInfoWithoutCosId...");
        // Clear the params.
        params.clear();
        getCosInternalInfoParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(COSID_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetCosInternalInfoWithNullCosId() {
        System.out
                .println("GetCosInternalInfoTest.testGetCosInternalInfoWithNullCosId...");
        params.get(COSID_KEY).clear();
        getCosInternalInfoParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetCosInternalInfoWithNonExistingCosId() {
        System.out
                .println("GetCosInternalInfoTest.testGetCosInternalInfoWithNonExistingCosId...");
        params.get(COSID_KEY).clear();
        params.get(COSID_KEY).add("something_junk_cos_1234");
        getCosInternalInfoParams(params, CosError.COS_NOT_FOUND.name());
    }

    @Test
    public void testWebmailVersionSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testWebmailVersionSuccess...");
        String version = getCosInternalInfoParams(params).getWebmailVersion();
        assertNotNull("WebmailVersion is null.", version);
    }

    @Test
    public void testSelfCareAccessEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testSelfCareAccessEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getSelfCareAccessEnabled();
        assertNotNull("SelfCareAccessEnabled is null.", pa);
        assertTrue("SelfCareAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testSelfCareSSLAccessEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testSelfCareSSLAccessEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getSelfCareSSLAccessEnabled();
        assertNotNull("SelfCareSSLAccessEnabled is null.", pa);
        assertTrue("SelfCareSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testImapProxyAuthenticationEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testImapProxyAuthenticationEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getImapProxyAuthenticationEnabled();
        assertNotNull("ImapProxyAuthenticationEnabled is null.", pa);
        assertTrue("ImapProxyAuthenticationEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testRemoteCallTracingEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testRemoteCallTracingEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getRemoteCallTracingEnabled();
        assertNotNull("RemoteCallTracingEnabled is null.", pa);
        assertTrue("RemoteCallTracingEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testInterManagerAccessEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testInterManagerAccessEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getInterManagerAccessEnabled();
        assertNotNull("InterManagerAccessEnabled is null.", pa);
        assertTrue("InterManagerAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testInterManagerSSLAccessEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testInterManagerSSLAccessEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getInterManagerSSLAccessEnabled();
        assertNotNull("InterManagerSSLAccessEnabled is null.", pa);
        assertTrue("InterManagerSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testVoiceMailAccessEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testVoiceMailAccessEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getVoiceMailAccessEnabled();
        assertNotNull("VoiceMailAccessEnabled is null.", pa);
        assertTrue("VoiceMailAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testFaxAccessEnabledSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testFaxAccessEnabledSuccess...");
        BooleanType pa = getCosInternalInfoParams(params).getFaxAccessEnabled();
        assertNotNull("FaxAccessEnabled is null.", pa);
        assertTrue("FaxAccessEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testLdapUtilitiesAccessTypeSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testLdapUtilitiesAccessTypeSuccess...");
        AccessType pa = getCosInternalInfoParams(params).getLdapUtilitiesAccessType();
        assertNotNull("LdapUtilitiesAccessType is null.", pa);
        assertTrue("LdapUtilitiesAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testAddressBookProviderSuccess() throws Exception {
        System.out.println("GetCosInternalInfoTest.testAddressBookProviderSuccess...");
        AddressBookProviderType pa = getCosInternalInfoParams(params).getAddressBookProvider();
        assertNotNull("AddressBookProvider is null.", pa);
        assertTrue("AddressBookProvider has a wrong value.",
                pa.equals(AddressBookProviderType.MSS));
    }

}
