package com.opwvmsg.mxos.test.unit.internalinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateInternalInfoTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IInternalInfoService info;
    private static IMessageService msg;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> msgParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateInternalInfoTest.setUpBeforeClass...");
        info = (IInternalInfoService) ContextUtils.loadContext().getService(
                ServiceEnum.InternalInfoService.name());
        msg = (IMessageService) ContextUtils.loadContext().getService(
                ServiceEnum.MessageService.name());
        long id = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("Mailbox was not created.", id != -1);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);

        msgParams.put(EMAIL_KEY, new ArrayList<String>());
        msgParams.get(EMAIL_KEY).add(EMAIL);
        msgParams.put(MxOSPOJOs.receivedFrom.name(), new ArrayList<String>());
        msgParams.get(MxOSPOJOs.receivedFrom.name()).add(EMAIL);
        msgParams
                .put(FolderProperty.folderName.name(), new ArrayList<String>());
        msgParams.get(FolderProperty.folderName.name()).add("INBOX");
        msgParams.put(MxOSPOJOs.message.name(), new ArrayList<String>());
        msgParams.get(MxOSPOJOs.message.name()).add("Hi");
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateInternalInfoTest.setUp...");
        assertNotNull("InternalInfoService object is null.", info);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateInternalInfoTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateInternalInfoTest.tearDownAfterClass...");
        try {
            msg.create(msgParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        msgParams.clear();
        msgParams = null;
        info = null;
        msg = null;
    }

    private static InternalInfo getInternalInfoParams() {
        InternalInfo mao = null;
        try {
            mao = info.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("InternalInfo object is null.", mao);
        return mao;
    }

    private static void updateInternalInfoParams(
            Map<String, List<String>> updateParams) {
        updateInternalInfoParams(updateParams, null);
    }

    private static void updateInternalInfoParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            info.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
            	updateParams.put(EMAIL_KEY, new ArrayList<String>());
            	updateParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testUpdateInternalInfoWithoutEmail() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateInternalInfoTest.testUpdateInternalInfoWithoutEmail...");
        updateInternalInfoParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateInternalInfoWithEmptyEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add("");
        System.out
                .println("UpdateInternalInfoTest.testUpdateInternalInfoWithEmptyEmail...");
        updateInternalInfoParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateInternalInfoWithNullEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(null);
        System.out
                .println("UpdateInternalInfoTest.testUpdateInternalInfoWithNullEmail...");
        updateInternalInfoParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateInternalInfoWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testUpdateInternalInfoWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateInternalInfoParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testWebmailVersionNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testWebmailVersionNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailVersion.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("1.2");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        String version = getInternalInfoParams().getWebmailVersion();
        assertNotNull("WebmailVersion is null.", version);
        assertTrue("WebmailVersion is not from default COS.",
                version.equals("0"));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailVersionSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testWebmailVersionSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailVersion.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_VERSION.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailVersionEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testWebmailVersionEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.webmailVersion.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("1.2");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        String version = getInternalInfoParams().getWebmailVersion();
        assertNotNull("WebmailVersion is null.", version);
        assertTrue("WebmailVersion is not from default COS.",
                version.equals("0"));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailVersionSuccess() throws Exception {
        System.out.println("UpdateInternalInfoTest.testWebmailVersionSuccess...");
        String key = MailboxProperty.webmailVersion.name();
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("1.5");
        updateInternalInfoParams(updateParams);
        String version = getInternalInfoParams().getWebmailVersion();
        assertNotNull("WebmailVersion is null.", version);
        assertTrue("WebmailVersion has a wrong value.",
                version.equals("1.5"));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getSelfCareAccessEnabled();
        assertNotNull("SelfCareAccessEnabled is null.", pa);
        assertNotNull("SelfCareAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getSelfCareAccessEnabled();
        assertNotNull("SelfCareAccessEnabled is null.", pa);
        assertNotNull("SelfCareAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getSelfCareAccessEnabled();
        assertNotNull("SelfCareAccessEnabled is null.", pa);
        assertTrue("SelfCareAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareSSLAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getSelfCareSSLAccessEnabled();
        assertNotNull("SelfCareSSLAccessEnabled is null.", pa);
        assertNotNull("SelfCareSSLAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareSSLAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareSSLAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareSSLAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getSelfCareSSLAccessEnabled();
        assertNotNull("SelfCareSSLAccessEnabled is null.", pa);
        assertNotNull("SelfCareSSLAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSelfCareSSLAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getSelfCareSSLAccessEnabled();
        assertNotNull("SelfCareSSLAccessEnabled is null.", pa);
        assertTrue("SelfCareSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    /* Delete of this attribute not supported right now.
    @Test
    public void testMessageStoreHostNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testMessageStoreHostNullParam...");
        String key = MailboxProperty.messageStoreHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_MESSAGE_STORE_HOST.name());
        updateParams.remove(key);
    }

    @Test
    public void testMessageStoreHostEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testMessageStoreHostEmptyParam...");
        String key = MailboxProperty.messageStoreHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("rwcvmx83c0100");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        List<String> hosts = getInternalInfoParams().getMessageStoreHosts();
        assertNotNull("MessageStoreHosts is null.", hosts);
        assertTrue("MessageStoreHosts is not empty.", hosts.isEmpty());
        updateParams.remove(key);
    }*/

    @Test
    public void testMessageStoreHostSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testMessageStoreHostSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.messageStoreHost.name();
        String host = "http://RWCVMX83C0100:80";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("http://rwcvmx83c0100:80");
        updateInternalInfoParams(updateParams);
        List<String> hosts = getInternalInfoParams().getMessageStoreHosts();
        assertNotNull("MessageStoreHosts is null.", hosts);
        assertTrue("MessageStoreHosts is empty.",
                hosts.contains(host.toLowerCase()));
        updateParams.remove(key);
    }
    
    @Test
    public void testMessageStoreHostFailure() {
        System.out
                .println("UpdateInternalInfoTest.testMessageStoreHostFailure...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.messageStoreHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("rwcvmx83c0100");
        try{
            updateInternalInfoParams(updateParams);
            fail();
        }catch(Exception nEx){
            System.out.println(nEx.getMessage());
        }
        
    }

    @Test
    public void testSmtpProxyHostNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSmtpProxyHostNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpProxyHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("testSmtpProxyHost");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        String host = getInternalInfoParams().getSmtpProxyHost();
        assertNull("SmtpProxyHost is not null.", host);
        updateParams.remove(key);
    }

    @Test
    public void testSmtpProxyHostEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSmtpProxyHostEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpProxyHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("testSmtpProxyHost");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        String host = getInternalInfoParams().getSmtpProxyHost();
        assertNull("SmtpProxyHost is not null.", host);
        updateParams.remove(key);
    }

    @Test
    public void testSmtpProxyHostBigValue() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSmtpProxyHostEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpProxyHost.name();
        String host = "testSmtpProxyHost";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(host);
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomeBigValueSomeBigValue" +
        		"SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
        		"SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
        		"SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
                        "SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
                        "SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
                        "SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
        		"SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
        		"SomeBigValueSomeBigValueSomeBigValueSomeBigValue" +
        		"SomeBigValueSomeBigValueSomeBigValueSomeBigValue");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SMTP_PROXY_HOST.name());
        String value = getInternalInfoParams().getSmtpProxyHost();
        assertNotNull("SmtpProxyHost is null.", value);
        updateParams.remove(key);
    }

    @Test
    public void testSmtpProxyHostSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testSmtpProxyHostSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.smtpProxyHost.name();
        String host = "testSmtpProxyHost";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(host);
        updateInternalInfoParams(updateParams);
        String value = getInternalInfoParams().getSmtpProxyHost();
        assertNotNull("SmtpProxyHost is null.", value);
        assertTrue("SmtpProxyHost has a wrong value.",
                value.equals(host.toLowerCase()));
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyHostNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyHostNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyHost.name();
        String host = "RWCVMX83C0101";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(host);
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        String value = getInternalInfoParams().getImapProxyHost();
        assertNotNull("ImapProxyHost is not null.", value);
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyHostEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyHostEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyHost.name();
        String host = "RWCVMX83C0101";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(host);
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        String value = getInternalInfoParams().getImapProxyHost();
        assertNotNull("ImapProxyHost is not null.", value);
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyHostSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyHostSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyHost.name();
        String host = "RWCVMX83C0101";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(host);
        updateInternalInfoParams(updateParams);
        String value = getInternalInfoParams().getImapProxyHost();
        assertNotNull("ImapProxyHost is null.", value);
        assertTrue("ImapProxyHost has a wrong value.",
                value.equals(host.toLowerCase()));
        updateParams.remove(key);
    }

    @Test
    public void testPopProxyHostNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testPopProxyHostNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popProxyHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("rwcvmx83c0100");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        String host = getInternalInfoParams().getPopProxyHost();
        assertNull("PopProxyHost is not null.", host);
        updateParams.remove(key);
    }

    @Test
    public void testPopProxyHostEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testPopProxyHostEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popProxyHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("rwcvmx83c0100");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        String host = getInternalInfoParams().getPopProxyHost();
        assertNull("PopProxyHost is not null.", host);
        updateParams.remove(key);
    }

    @Test
    public void testPopProxyHostSuccess() throws Exception {
        System.out.println("UpdateInternalInfoTest.testPopProxyHostSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.popProxyHost.name();
        String host = "RWCVMX83C0100";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(host);
        updateInternalInfoParams(updateParams);
        String value = getInternalInfoParams().getPopProxyHost();
        assertNotNull("PopProxyHost is null.", value);
        assertTrue("PopProxyHost has a wrong value.",
                value.equals(host.toLowerCase()));
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyHostNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAutoReplyHostNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.autoReplyHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("rwcvmx83c0100");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        String host = getInternalInfoParams().getAutoReplyHost();
        assertNull("AutoReplyHost is null.", host);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyHostEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAutoReplyHostEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.autoReplyHost.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("rwcvmx83c0100");
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        String host = getInternalInfoParams().getAutoReplyHost();
        assertNull("AutoReplyHost is null.", host);
        updateParams.remove(key);
    }

    @Test
    public void testAutoReplyHostSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAutoReplyHostSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.autoReplyHost.name();
        String host = "RWCVMX83C0100";
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(host);
        updateInternalInfoParams(updateParams);
        String value = getInternalInfoParams().getAutoReplyHost();
        assertNotNull("AutoReplyHost is null.", value);
        assertTrue("AutoReplyHost has a wrong value.",
                value.equals(host.toLowerCase()));
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyAuthEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getImapProxyAuthenticationEnabled();
        assertNotNull("ImapProxyAuthEnabled is null.", pa);
        assertNotNull("ImapProxyAuthEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyAuthEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_PROXY_AUTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyAuthEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_PROXY_AUTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyAuthEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getImapProxyAuthenticationEnabled();
        assertNotNull("ImapProxyAuthEnabled is null.", pa);
        assertNotNull("ImapProxyAuthEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testImapProxyAuthEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getImapProxyAuthenticationEnabled();
        assertNotNull("ImapProxyAuthEnabled is null.", pa);
        assertTrue("ImapProxyAuthEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testRemoteCallTracingEnabledNullParam...");
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getRemoteCallTracingEnabled();
        assertNotNull("RemoteCallTracingEnabled is null.", pa);
        assertNotNull("RemoteCallTracingEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testRemoteCallTracingEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_REMOTE_CALL_TRACING.name());
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testRemoteCallTracingEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_REMOTE_CALL_TRACING.name());
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testRemoteCallTracingEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getRemoteCallTracingEnabled();
        assertNotNull("RemoteCallTracingEnabled is null.", pa);
        assertNotNull("RemoteCallTracingEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testRemoteCallTracingEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getRemoteCallTracingEnabled();
        assertNotNull("RemoteCallTracingEnabled is null.", pa);
        assertTrue("RemoteCallTracingEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getInterManagerAccessEnabled();
        assertNotNull("InterManagerAccessEnabled is null.", pa);
        assertNotNull("InterManagerAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getInterManagerAccessEnabled();
        assertNotNull("InterManagerAccessEnabled is null.", pa);
        assertNotNull("InterManagerAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getInterManagerAccessEnabled();
        assertNotNull("InterManagerAccessEnabled is null.", pa);
        assertTrue("InterManagerAccessEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerSSLAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getInterManagerSSLAccessEnabled();
        assertNotNull("InterManagerSSLAccessEnabled is null.", pa);
        assertNotNull("InterManagerSSLAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerSSLAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerSSLAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerSSLAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getInterManagerSSLAccessEnabled();
        assertNotNull("InterManagerSSLAccessEnabled is null.", pa);
        assertNotNull("InterManagerSSLAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testInterManagerSSLAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getInterManagerSSLAccessEnabled();
        assertNotNull("InterManagerSSLAccessEnabled is null.", pa);
        assertTrue("InterManagerSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testVoiceMailAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getVoiceMailAccessEnabled();
        assertNotNull("VoiceMailAccessEnabled is null.", pa);
        assertNotNull("VoiceMailAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testVoiceMailAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_VOICE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testVoiceMailAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_VOICE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testVoiceMailAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getVoiceMailAccessEnabled();
        assertNotNull("VoiceMailAccessEnabled is null.", pa);
        assertNotNull("VoiceMailAccessEnabled is not from default COS.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testVoiceMailAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getVoiceMailAccessEnabled();
        assertNotNull("VoiceMailAccessEnabled is null.", pa);
        assertTrue("VoiceMailAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testFaxAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getFaxAccessEnabled();
        assertNotNull("FaxAccessEnabled is null.", pa);
        assertNotNull("FaxAccessEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testFaxAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_FAX_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testFaxAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_FAX_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testFaxAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getFaxAccessEnabled();
        assertNotNull("FaxAccessEnabled is null.", pa);
        assertNotNull("FaxAccessEnabled is not from default COS.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testFaxAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateInternalInfoParams(updateParams);
        BooleanType pa = getInternalInfoParams().getFaxAccessEnabled();
        assertNotNull("FaxAccessEnabled is null.", pa);
        assertTrue("FaxAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testLdapUtilitiesAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        AccessType pa = getInternalInfoParams().getLdapUtilitiesAccessType();
        assertNotNull("LdapUtilitiesAccessType is null.", pa);
        assertNotNull("LdapUtilitiesAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testLdapUtilitiesAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testLdapUtilitiesAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testLdapUtilitiesAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.NONE.toString());

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        AccessType pa = getInternalInfoParams().getLdapUtilitiesAccessType();
        assertNotNull("LdapUtilitiesAccessType is null.", pa);
        assertNotNull("LdapUtilitiesAccessType is not from default COS.",
                pa.equals(AccessType.ALL));
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testLdapUtilitiesAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.TRUSTED.toString());
        updateInternalInfoParams(updateParams);
        AccessType pa = getInternalInfoParams().getLdapUtilitiesAccessType();
        assertNotNull("LdapUtilitiesAccessType is null.", pa);
        assertTrue("LdapUtilitiesAccessType has a wrong value.",
                pa.equals(AccessType.TRUSTED));
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderNullParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAddressBookProviderNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AddressBookProviderType.VOX.toString());

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateInternalInfoParams(updateParams);
        AddressBookProviderType pa = getInternalInfoParams().getAddressBookProvider();
        assertNotNull("AddressBookProvider is null.", pa);
        assertNotNull("AddressBookProvider is not from default COS.",
                pa.equals(AddressBookProviderType.MSS));
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderInvalidParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAddressBookProviderInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_ADDRESS_BOOK_PROVIDER.name());
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderSplCharsInParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAddressBookProviderSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_ADDRESS_BOOK_PROVIDER.name());
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderEmptyParam() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAddressBookProviderEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AddressBookProviderType.VOX.toString());

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        AddressBookProviderType pa = getInternalInfoParams().getAddressBookProvider();
        assertNotNull("AddressBookProvider is null.", pa);
        assertNotNull("AddressBookProvider is not from default COS.",
                pa.equals(AddressBookProviderType.MSS));
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testAddressBookProviderSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AddressBookProviderType.PLAXO.toString());
        updateInternalInfoParams(updateParams);
        AddressBookProviderType pa = getInternalInfoParams().getAddressBookProvider();
        assertNotNull("AddressBookProvider is null.", pa);
        assertTrue("AddressBookProvider has a wrong value.",
                pa.equals(AddressBookProviderType.PLAXO));
        updateParams.remove(key);
    }
    @Test
    public void testMailRealmSuccess() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testMailRealmSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.realm.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("mailRealm");
        updateInternalInfoParams(updateParams);
        String pa = getInternalInfoParams().getRealm();
        assertNotNull("mailRealm is null.", pa);
        updateParams.remove(key);
    }
    @Test
    public void testMailRealmNeg() throws Exception {
        System.out
                .println("UpdateInternalInfoTest.testMailRealmNeg...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.realm.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateInternalInfoParams(updateParams);
        String pa = getInternalInfoParams().getRealm();
        assertNull("mailRealm is not null.", pa);
        updateParams.remove(key);
    }
}
