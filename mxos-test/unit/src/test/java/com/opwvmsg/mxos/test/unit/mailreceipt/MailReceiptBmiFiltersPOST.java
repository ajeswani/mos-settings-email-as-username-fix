package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BmiSpamType;
import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptBMIFiltersService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 *
 * @author mxos-dev
 *
 */
public class MailReceiptBmiFiltersPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailReceiptBmiFiltersPOST";
    private static final String ARROW_SEP = " --> ";
    private static IMailReceiptBMIFiltersService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IMailReceiptBMIFiltersService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailReceiptBMIFiltersService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static BmiFilters getParams() {
        BmiFilters object = null;
        try {
            object = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return object;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSpamActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionNullParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    // spamAction
    @Test
    public void testSpamActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionEmptyParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionInvalidParam() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testSpamActionInvalidParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "asdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_BMI_FILTERS_SPAM_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSplCharsInParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_BMI_FILTERS_SPAM_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, BmiSpamType.DISCARD.toString());
        updateParams(updateParams);
        BmiSpamType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BmiSpamType.DISCARD);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess1");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, BmiSpamType.NONE.toString());
        updateParams(updateParams);
        BmiSpamType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BmiSpamType.NONE);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess2");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, BmiSpamType.FOLDER_DELIVER.toString());
        updateParams(updateParams);
        BmiSpamType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BmiSpamType.FOLDER_DELIVER);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess3");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, BmiSpamType.ADD_HEADER.toString());
        updateParams(updateParams);
        BmiSpamType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BmiSpamType.ADD_HEADER);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess4() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess4");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, BmiSpamType.SUBJECT_PREFIX.toString());
        updateParams(updateParams);
        BmiSpamType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BmiSpamType.SUBJECT_PREFIX);
        updateParams.remove(key);
    }

    // spamMessageIndicator
    @Test
    public void testSpamMessageIndicatorNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamMessageIndicatorNullParam");
        String key = MailboxProperty.spamMessageIndicator.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamMessageIndicatorEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamMessageIndicatorEmptyParam");
        String key = MailboxProperty.spamMessageIndicator.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamMessageIndicatorInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamMessageIndicatorInvalidParam");
        String key = MailboxProperty.spamMessageIndicator.name();
        addToParams(updateParams, key, "asdfsdf");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamMessageIndicatorSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamMessageIndicatorSplCharsInParam");
        String key = MailboxProperty.spamMessageIndicator.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamMessageIndicatorSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamMessageIndicatorSuccess");
        String key = MailboxProperty.spamMessageIndicator.name();
        String val = "testSpamMessageIndicator";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getSpamMessageIndicator();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

}
