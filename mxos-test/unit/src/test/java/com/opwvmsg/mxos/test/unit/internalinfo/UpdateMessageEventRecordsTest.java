package com.opwvmsg.mxos.test.unit.internalinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.EventRecordingType;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMessageEventRecordsService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateMessageEventRecordsTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IMessageEventRecordsService mer;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateMessageEventRecordsTest.setUpBeforeClass...");
        mer = (IMessageEventRecordsService) ContextUtils.loadContext()
                .getService(ServiceEnum.MessageEventRecordsService.name());
        long id = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("Mailbox was not created.", id != -1);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateMessageEventRecordsTest.setUp...");
        assertNotNull("MessageEventRecordsService object is null.", mer);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateMessageEventRecordsTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateMessageEventRecordsTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        mer = null;
    }

    private static MessageEventRecords getMessageEventRecordsParams() {
        MessageEventRecords obj = null;
        try {
            obj = mer.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("MessageEventRecords object is null.", obj);
        return obj;
    }

    private static void updateMessageEventRecordsParams(
            Map<String, List<String>> updateParams) {
        updateMessageEventRecordsParams(updateParams, null);
    }

    private static void updateMessageEventRecordsParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            mer.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
            	updateParams.put(EMAIL_KEY, new ArrayList<String>());
            	updateParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testUpdateMessageEventRecordsWithoutEmail() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateMessageEventRecordsTest.testUpdateMessageEventRecordsWithoutEmail...");
        updateMessageEventRecordsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMessageEventRecordsWithEmptyEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add("");
        System.out
                .println("UpdateMessageEventRecordsTest.testUpdateMessageEventRecordsWithEmptyEmail...");
        updateMessageEventRecordsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateMessageEventRecordsWithNullEmail() throws Exception {
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(null);
        System.out
                .println("UpdateMessageEventRecordsTest.testUpdateMessageEventRecordsWithNullEmail...");
        updateMessageEventRecordsParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateMessageEventRecordsWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testUpdateMessageEventRecordsWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateMessageEventRecordsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testEventRecordingEnabledNullParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testEventRecordingEnabledNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(EventRecordingType.ALWAYS.name());
        updateMessageEventRecordsParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        EventRecordingType ert = getMessageEventRecordsParams().getEventRecordingEnabled();
        assertNotNull("EventRecordingEnabled is null.", ert);
        assertNotNull("EventRecordingEnabled is not from default COS.",
                ert.equals(EventRecordingType.NEVER));
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testEventRecordingEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_EVENT_RECORDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testEventRecordingEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_EVENT_RECORDING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testEventRecordingEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(EventRecordingType.ALWAYS.name());
        updateMessageEventRecordsParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        EventRecordingType ert = getMessageEventRecordsParams().getEventRecordingEnabled();
        assertNotNull("EventRecordingEnabled is null.", ert);
        assertNotNull("EventRecordingEnabled is not from default COS.",
                ert.equals(EventRecordingType.NEVER));
        updateParams.remove(key);
    }

    @Test
    public void testEventRecordingEnabledSuccess() throws Exception {
        System.out.println("UpdateMessageEventRecordsTest.testEventRecordingEnabledSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.eventRecordingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(EventRecordingType.ALWAYS.name());
        updateMessageEventRecordsParams(updateParams);
        EventRecordingType ert = getMessageEventRecordsParams().getEventRecordingEnabled();
        assertNotNull("EventRecordingEnabled is null.", ert);
        assertTrue("EventRecordingEnabled has a wrong value.",
                ert.equals(EventRecordingType.ALWAYS));
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthNullParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxHeaderLengthNullParam...");
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateMessageEventRecordsParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageEventRecordsParams(updateParams);
        Integer headerLength = getMessageEventRecordsParams()
                .getMaxHeaderLength();
        assertNotNull("MaxHeaderLength is null.", headerLength);
        assertNotNull("MaxHeaderLength is not from default COS.",
                headerLength.equals(new Integer(0)));
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxHeaderLengthEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateMessageEventRecordsParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageEventRecordsParams(updateParams);
        Integer headerLength = getMessageEventRecordsParams().getMaxHeaderLength();
        assertNotNull("MaxHeaderLength is null.", headerLength);
        assertNotNull("MaxHeaderLength is not from default COS.",
                headerLength.equals(new Integer(0)));
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxHeaderLengthSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_HEADER_LENGTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxHeaderLengthInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("-120");
        updateMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_HEADER_LENGTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxHeaderLengthSuccess() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxHeaderLengthSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxHeaderLength.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateMessageEventRecordsParams(updateParams);
        Integer headerLength = getMessageEventRecordsParams().getMaxHeaderLength();
        assertNotNull("MaxHeaderLength is null.", headerLength);
        assertTrue("MaxHeaderLength has a wrong value.",
                headerLength.equals(new Integer(100)));
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBNullParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxFilesSizeKBNullParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateMessageEventRecordsParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageEventRecordsParams(updateParams);
        Integer headerLength = getMessageEventRecordsParams().getMaxFilesSizeKB();
        assertNotNull("MaxFilesSizeKB is null.", headerLength);
        assertNotNull("MaxFilesSizeKB is not from default COS.",
                headerLength.equals(new Integer(0)));
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxFilesSizeKBEmptyParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateMessageEventRecordsParams(updateParams);

        updateParams.remove(key);
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageEventRecordsParams(updateParams);
        Integer headerLength = getMessageEventRecordsParams().getMaxFilesSizeKB();
        assertNotNull("MaxFilesSizeKB is null.", headerLength);
        assertNotNull("MaxFilesSizeKB is not from default COS.",
                headerLength.equals(new Integer(0)));
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxFilesSizeKBSplCharsInParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_FILES_SIZE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxFilesSizeKBInvalidParam...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("-100");
        updateMessageEventRecordsParams(updateParams,
                MailboxError.MBX_INVALID_MAX_FILES_SIZE.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFilesSizeKBSuccess() throws Exception {
        System.out
                .println("UpdateMessageEventRecordsTest.testMaxFilesSizeKBSuccess...");
        updateParams.clear();
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.maxFilesSizeKB.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("100");
        updateMessageEventRecordsParams(updateParams);
        Integer headerLength = getMessageEventRecordsParams().getMaxFilesSizeKB();
        assertNotNull("MaxFilesSizeKB is null.", headerLength);
        assertTrue("MaxFilesSizeKB has a wrong value.",
                headerLength.equals(new Integer(100)));
        updateParams.remove(key);
    }

}
