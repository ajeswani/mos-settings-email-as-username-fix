package com.opwvmsg.mxos.test.unit.Saml;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.saml.ISamlService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 */
public class SamlDecodeAssertionTest {
    private static ISamlService samlService;
    private static IMxOSContext context;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("SamlDecodeAssertionTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        samlService = (ISamlService) context.getService(ServiceEnum.SamlService
                .name());
        assertNotNull("SamlService object is null.", samlService);

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("SamlDecodeAssertionTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("SamlDecodeAssertionTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("SamlDecodeAssertionTest.tearDownAfterClass...");
        // Delete the mailbox if already exists
        samlService = null;
    }

    @Test
    public void testNullSamlAssertionInSamlDecodeAssertionRequest() {
        System.out.println("\n\ntestNullTargetURLInSamlDecodeAssertionTest...");
        List<String> samlAssertion = new ArrayList<String>();
        samlAssertion.add(null);
        params.put("samlAssertion", samlAssertion);
        try {
            Map<String, String> metaAttributes = samlService
                    .decodeSAMLAssertion(params);
            assertNull("Saml Assertion is null", metaAttributes);
            params.clear();
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }

    }

    @Test
    public void testSuccessSamlDecodeAssertionRequest() {
        System.out.println("\n\testSuccessSamlDecodeAssertionTest...");
        List<String> samlAssertion = new ArrayList<String>();
        String assertion = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<samlp:Response xmlns:samlp=\"urn:oasis:names:tc:SAML:2.0:protocol\""
                + " xmlns=\"urn:oasis:names:tc:SAML:2.0:assertion\""
                + " xmlns:xenc=\"http://www.w3.org/2001/04/xmlenc#\""
                + " ID=\"dlmjimlchjkbhjpiphmapbpeagcclhaepmmononc\""
                + " IssueInstant=\"2013-07-24T07:35:56Z\" Version=\"2.0\">"
                + "<Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\">"
                + "<SignedInfo>"
                + "<CanonicalizationMethod"
                + " Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments\" />"
                + "<SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#dsa-sha1\" />"
                + "<Reference URI=\"\"><Transforms>"
                + "<Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\" />"
                + "</Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" />"
                + "<DigestValue>r5ey5K8tttaEibUo5nva4aLk5/A=</DigestValue></Reference>"
                + "</SignedInfo><SignatureValue>PjPko1Ub3aePhf7Gct5Tx9OykqBNbPsyf7wnSO5n83A/LginASlaqA==</SignatureValue>"
                + "<KeyInfo><KeyValue><DSAKeyValue><P>/KaCzo4Syrom78z3EQ5SbbB4sF7ey80etKII864WF64B81uRpH5t9jQTxeEu0ImbzRMqzVDZkVG9xD7nN1kuFw==</P><Q>li7dzDacuo67Jg7mtqEm2TRuOMU=</Q><G>Z4Rxsnqc9E7pGknFFH2xqaryRPBaQ01khpMdLRQnG541Awtx/XPaF5Bpsy4pNWMOHCBiNU0NogpsQW5QvnlMpA==</G><Y>VMoV//Oh7VytBbZVySNmVZevV1bw7vmJwx5hHszeR25bforBFA19nk+3ehg6SgUjWiXn7HsybemjRFs5x4+XFg==</Y></DSAKeyValue>"
                + "</KeyValue></KeyInfo></Signature><samlp:Status><samlp:StatusCode Value=\"urn:oasis:names:tc:SAML:2.0:status:Success\" />"
                + "</samlp:Status><saml:Assertion xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\" "
                + "ID=\"onggflghgggfgneijedgdppbajcbdamehgmgbpco\" IssueInstant=\"2003-04-17T00:46:02Z\" "
                + "Version=\"2.0\"><saml:Issuer>virginmedia.com</saml:Issuer><saml:Subject><saml:NameID "
                + "Format=\"urn:oasis:names:tc:SAML:2.0:nameid-format:emailAddress\">dave14thmay@ntlworld.com</saml:NameID>"
                + "<saml:SubjectConfirmation Method=\"urn:oasis:names:tc:SAML:2.0:cm:bearer\">"
                + "<saml:SubjectConfirmationData "
                + "Recipient=\"http://localhost:8080/mxos/v2/saml/acs\" /></saml:SubjectConfirmation></saml:Subject>"
                + "<saml:Conditions NotBefore=\"2013-07-24T08:34:56Z\" NotOnOrAfter=\"2013-07-24T10:36:56Z\" />"
                + "<saml:AuthnStatement AuthnInstant=\"2013-07-24T09:35:56Z\"><saml:AuthnContext>"
                + "<saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml:AuthnContextClassRef>"
                + "</saml:AuthnContext></saml:AuthnStatement><saml:AttributeStatement>"
                + "<saml:Attribute Name=\"existTVSubServiceHighestTier\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\" /></saml:Attribute>"
                + "<saml:Attribute Name=\"productList\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">10102,10201</saml:AttributeValue>"
                + "</saml:Attribute><saml:Attribute Name=\"tvProductCodes\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\" /></saml:Attribute><saml:Attribute Name=\"vmEbillUPI\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">20102:10308699:7</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"LastName\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">TEST</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmPlayerLearn\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue"
                + " xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\" /></saml:Attribute><saml:Attribute Name=\"twAccountStatus\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">EN</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmLoginID\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue"
                + " xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">dave14thmay@ntlworld.com</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmUserURL\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue"
                + " xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">homepage.ntlworld.com/dave14thmay</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmAccountID\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">26159345401</saml:AttributeValue></saml:Attribute>"
                + "<saml:Attribute Name=\"vmAccountStatus\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue"
                + " xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">EN</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmUnverifiedSSOLoginID\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue"
                + " xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\" /></saml:Attribute><saml:Attribute Name=\"vmOIMProfileStatus\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">1</saml:AttributeValue></saml:Attribute>"
                + "<saml:Attribute Name=\"FirstName\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">INTERNAL</saml:AttributeValue></saml:Attribute>"
                + "<saml:Attribute Name=\"existTelSubServiceHighestTier\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\" /></saml:Attribute><saml:Attribute Name=\"twSiteID\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">26</saml:AttributeValue>"
                + "</saml:Attribute><saml:Attribute Name=\"twAccountID\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">26159345401</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmSSOLoginID\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">dave14thmay@ntlworld.com</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"tierOfService\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">T5</saml:AttributeValue></saml:Attribute>"
                + "<saml:Attribute Name=\"tvTierOfService\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">0</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmPlayerStatus\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\" /></saml:Attribute><saml:Attribute Name=\"vmPlayerUPI\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\" /></saml:Attribute><saml:Attribute Name=\"vmUserStatus\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">EN</saml:AttributeValue></saml:Attribute>"
                + "<saml:Attribute Name=\"vmInternetCustomer\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">1</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"vmUserType\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">Admin</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"bbStatus\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue"
                + " xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">ACTIVE</saml:AttributeValue></saml:Attribute><saml:Attribute"
                + " Name=\"vmUnverifiedContactEmailAddress\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\"><saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\" /></saml:Attribute><saml:Attribute"
                + " Name=\"vmUserID\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                + " xsi:type=\"xs:string\">12082541</saml:AttributeValue></saml:Attribute><saml:Attribute Name=\"internetProductCodes\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">P100332</saml:AttributeValue></saml:Attribute>"
                + "<saml:Attribute Name=\"vmPVRUPI\" NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\" />"
                + "</saml:Attribute><saml:Attribute Name=\"vmParentPlayerSetting\""
                + " NameFormat=\"urn:oasis:names:tc:SAML:2.0:attrname-format:basic\">"
                + "<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\""
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\" />"
                + "</saml:Attribute></saml:AttributeStatement></saml:Assertion></samlp:Response>";
        samlAssertion.add(assertion);
        params.put("samlAssertion", samlAssertion);
        try {
            Map<String, String> metaAttributes = samlService
                    .decodeSAMLAssertion(params);
            assertNotNull("Saml Assertion is null", metaAttributes);
            params.clear();
        } catch (MxOSException e) {
            fail();
            System.out.println(e.getCode());
        }

    }
}
