package com.opwvmsg.mxos.test.unit.folder;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CreateFolderTest {

    private static final String TEST_NAME = "CreateFolderTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "folderCreate@openwave.com";
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test5";
    private static final String ISADMIN_KEY = MessageProperty.isAdmin.name();
    private static final String ISADMMIN_TRUE = "true";
    private static final String ISADMMIN_FALSE = "false";

    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();

    private static final String FOLDERNAME_VALUE_INBOX = "INBOX";
    private static final String FOLDERNAME_VALUE_1 = "1";
    private static final String FOLDERNAME_VALUE_1_2 = "1%2f2";
    private static final String FOLDERNAME_VALUE_1_22 = "1111111111111111111111111111111111111111111111%2f22222222222222222222222222222222222222222222222" +
                "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222" +
                "2222222222222222222222222222222222222222222";
    private static final String FOLDERNAME_VALUE_11_22 = "1111111111111111111111111111111111111111111111%2f22222222222222222222222222222222222222222222222" +
    "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222" +
    "222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222";
    private static final String FOLDERNAME_VALUE_6 = "fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2ft";
    private static final String FOLDERNAME_VALUE_7 = "a%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2ft";
    private static final String FOLDERNAME_VALUE_3_1 = "3%2f1";
    private static final String FOLDERNAME_VALUE_4 = "4";
    private static final String FOLDERNAME_VALUE_5 = "5";

    private static final String FOLDERNAME_VALUE_test = "test";
    private static final String FOLDERNAME_VALUE_Test = "Test";

    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static IFolderService folderService;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        folderService = (IFolderService) context
                .getService(ServiceEnum.FolderService.name());
        //createMailBox(EMAIL, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

    }
    

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        mailboxService = null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, EMAIL_KEY, email);
        addParams(inputParams, PASSWORD_KEY, password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Unable to create Mailbox...", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            folderService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNullEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
            		ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }

    @Test
    public void testCreateSuccess_root() {
        System.out.println(TEST_NAME + ARROW_SEP + "testCreateSuccess_root");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_1);
            folderService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testCreateSuccess_secondLevel() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_secondLevel");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_1_2);
            folderService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }
    
    @Test
    public void testCreateSuccess_secondLevelFolderName() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_secondLevelFolderName");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_1_22);
            folderService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }
    
    @Test
    public void testCreateFailure_secondLevelFolderName() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateFailure_secondLevelFolderName");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_11_22);
            folderService.create(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_FOLDER_NAME.name(), e.getCode());
        }
    }
    
    @Test
    public void testCreateSuccess_FolderDepth() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_FolderDepth");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_6);
            folderService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }
    
    @Test
    public void testCreateFailure_FolderDepth() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateFailure_FolderDepth");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_7);
            folderService.create(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_FOLDER_NAME.name(), e.getCode());
        }
    }
    
    @Test
    public void testCreateSuccess_secondLevel_firstLevelNotPresent() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_secondLevel_firstLevelNotPresent");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_3_1);
            folderService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testCreateSuccess_isAdmintrue() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_isAdmintrue");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_4);
            addParams(inputParams, ISADMIN_KEY, ISADMMIN_TRUE);
            folderService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testCreateSuccess_isAdminfalse() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_isAdminfalse");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_5);
            addParams(inputParams, ISADMIN_KEY, ISADMMIN_FALSE);
            folderService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testCreateSuccess_isAdminInvalid() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_isAdminInvalid");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_5);
            addParams(inputParams, ISADMIN_KEY, "junk");
            folderService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_IS_ADMIN.name(), e.getCode());
        }
    }

    @Test
    public void testCreateSuccess_duplicateFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_duplicateFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_5);
            folderService.create(inputParams);
            folderService.create(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
            		FolderError.FLD_ALREADY_EXISTS.name(),
                    e.getCode());
        }
    }

    @Test
    public void testCreateSuccess_duplicateFolderButDifferentCase() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateSuccess_duplicateFolderButDifferentCase");
        
        try {
            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE_Test);
            folderService.create(inputParams1);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_test);
            folderService.create(inputParams2);
            assertTrue("folder created",true);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

}
