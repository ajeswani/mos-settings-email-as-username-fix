package com.opwvmsg.mxos.test.unit.cos.credentials;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.PasswordRecoveryPreference;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosCredentialsService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateCosCredentialsTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static final String MFLA = "6";
    private static ICosCredentialsService cosCredentialsService;
    private static ICosService cosService;
    private static IMxOSContext context;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateCosCredentialsTest....setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosCredentialsService = (ICosCredentialsService)context
                .getService(ServiceEnum.CosCredentialsService.name());
        cosService = (ICosService) context
                .getService(ServiceEnum.CosService.name());
        createCos(COS_ID);
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
    
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CredentialsTest...tearDownAfterClass");
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        deleteCos(COS_ID);
        cosCredentialsService = null;
        cosService = null;
    }

    private static void createCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.create(inputParams);

        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void UpdateCosCredentialsParams(
            Map<String, List<String>> updateParams) {

        UpdateCosCredentialsParams(updateParams, null);
    }

    private static void UpdateCosCredentialsParams(
            Map<String, List<String>> updateParams, String expectedError) {

        try {
            cosCredentialsService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!");
            }

        } catch (MxOSException me) {
            if (null == expectedError) {
                fail("This should not have come!");
            } else {
                assertNotNull("MxOSException is not null.", me);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, me.getCode());
            }
        } finally {

            if (updateParams.isEmpty()) {
                updateParams.put(COSID_KEY, new ArrayList<String>());
                updateParams.get(COSID_KEY).add(COS_ID);
            }
        }
    }

    private static Credentials GetCosCredentialsParams() {
        Credentials credentials = null;
        try {
            credentials = cosCredentialsService.read(getParams);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();

        } finally {
            if (getParams.isEmpty()|| null == getParams.get(COSID_KEY)
                    || getParams.get(COSID_KEY).isEmpty()) {
                getParams.put(COSID_KEY, new ArrayList<String>());
                getParams.get(COSID_KEY).add(COS_ID);
            }
        }
        assertNotNull("Credentials object is null.", credentials);
        return credentials;
    }

    @Test
    public void testGetCosCrdentialsWithCosId() {
        Credentials result = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            inputParams.put(COSID_KEY, new ArrayList<String>());
            inputParams.get(COSID_KEY).add(COS_ID);

            result = cosCredentialsService.read(inputParams);
            assertNotNull(result);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull(result);
        System.out.println("GET API: " + result);

    }

    @Test
    public void testPasswordRecoveryAllowedEmptyParam() {
        System.out
                .println("UpdateCosCredentialsTest....testPasswordRecoveryAllowedEmptyParam...");
        String key = MailboxProperty.passwordRecoveryAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        UpdateCosCredentialsParams(updateParams,
                ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(key);
    }

    @Test
    public void testPasswordRecoveryAllowedNullParam() {
        System.out
                .println("UpdateCosCredentialsTest....testPasswordRecoveryAllowedEmptyParam...");
        String key = MailboxProperty.passwordRecoveryAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        UpdateCosCredentialsParams(updateParams,
        		ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(key);
    }

    @Test
    public void testPasswordRecoveryAllowedInvalidParam() {
        System.out
                .println("UpdateCosCredentialsTest....testPasswordRecoveryAllowedInvalidParam...");
        String key = MailboxProperty.passwordRecoveryAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        UpdateCosCredentialsParams(updateParams,
        		ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(key);
    }

    @Test
    public void testPasswordRecoveryAllowedSuccess() {
        System.out
                .println("UpdateCosCredentialsTest....testPasswordRecoveryAllowedSuccess...");
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.passwordRecoveryAllowed.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        UpdateCosCredentialsParams(updateParams);
        BooleanType pra = GetCosCredentialsParams()
                .getPasswordRecoveryAllowed();
        assertNotNull("passwordRecoveryAllowed object is null.",
        		pra);
        assertTrue("passwordRecoveryAllowed has a wrong value.",
                pra.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testMaxFailedLoginAttemptsNullParam() {
        System.out
                .println("UpdateCosCredentialsTest....testMaxFailedLoginAttemptsNullParam...");
        String key = MailboxProperty.maxFailedLoginAttempts.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        UpdateCosCredentialsParams(updateParams,
        		ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFailedLoginAttemptsInvalidParam() {
        System.out
                .println("UpdateCosCredentialsTest....testMaxFailedLoginAttemptsInvalidParam...");
        String key = MailboxProperty.maxFailedLoginAttempts.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        UpdateCosCredentialsParams(updateParams,
        		MailboxError.MBX_INVALID_MAX_FAILED_LOGIN_ATTEMPTS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFailedLoginAttemptsSuccess() {
        System.out
                .println("UpdateCosCredentialsTest....testMaxFailedLoginAttemptsSuccess...");
        String key = MailboxProperty.maxFailedLoginAttempts.name();
        
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(MFLA);
        UpdateCosCredentialsParams(updateParams);
        
        getParams.clear();
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);

        assertNotNull("MaxFailedLoginAttempts object is null.", GetCosCredentialsParams()
                        .getMaxFailedLoginAttempts());
        updateParams.remove(key);
    }
    
    @Test
    public void testMaxFailedCaptchaLoginAttemptsSuccess() {
        System.out
                .println("UpdateCosCredentialsTest....testMaxFailedCaptchaLoginAttemptsSuccess...");
        String key = MailboxProperty.maxFailedCaptchaLoginAttempts.name();
        
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(MFLA);
        UpdateCosCredentialsParams(updateParams);
        
        getParams.clear();
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);

        assertNotNull("MaxFailedCaptchaLoginAttempts object is null.", GetCosCredentialsParams()
                        .getMaxFailedCaptchaLoginAttempts());
        updateParams.remove(key);
    }
    
    @Test
    public void testMaxFailedCaptchaLoginAttemptsNullParam() {
        System.out
                .println("UpdateCosCredentialsTest....testMaxFailedCaptchaLoginAttemptsNullParam...");
        String key = MailboxProperty.maxFailedCaptchaLoginAttempts.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        UpdateCosCredentialsParams(updateParams,
                        ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxFailedCaptchaLoginAttemptsInvalidParam() {
        System.out
                .println("UpdateCosCredentialsTest....testMaxFailedCaptchaLoginAttemptsInvalidParam...");
        String key = MailboxProperty.maxFailedCaptchaLoginAttempts.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        UpdateCosCredentialsParams(updateParams,
                        ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(key);
    }

    
    
    @Test
    public void testCredentialsWithoutAnyParam() {
        System.out
                .println("UpdateCosCredentialsTest....testCredentialsWithoutAnyParam...");
        UpdateCosCredentialsParams(updateParams,
                ErrorCode.GEN_BAD_REQUEST.name());

    }

}
