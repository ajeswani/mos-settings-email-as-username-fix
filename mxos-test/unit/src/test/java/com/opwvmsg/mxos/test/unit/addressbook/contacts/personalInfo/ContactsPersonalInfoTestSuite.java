package com.opwvmsg.mxos.test.unit.addressbook.contacts.personalInfo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to PersonalInfo
@SuiteClasses({ ContactsPersonalInfoGET.class, ContactsPersonalInfoPOST.class,
ContactsPersonalInfoAddressGET.class, ContactsPersonalInfoAddressPOST.class,
ContactsPersonalInfoCommunicationGET.class, ContactsPersonalInfoCommunicationPOST.class,
ContactsPersonalInfoEventsTestSuite.class })
public class ContactsPersonalInfoTestSuite {

}
