package com.opwvmsg.mxos.test.unit.paf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.backend.crud.mss.MssSlMetaCRUD;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.FolderInfo;
import com.opwvmsg.utils.paf.intermail.mail.MessageMetadata;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMsg;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.MoveMsgs;

public class MoveMessagesRmeTest {

    private static final String TEST_NAME = "MoveMessagesRmeTest";
    private static final String ARROW_SEP = " --> ";

    public static String CONFIG_HOST = null;
    public static String CONFIG_PORT = null;
    public static String MSS_HOST = null;
    public static final String DEFAULT_APP = "mss";
    public static final String DEFAULT_HOST = "*";
    public static String MSS_RME_VERSION = "175";
    public static String MAILBOX_ID = "7126905549041528032";
    // public static final String MAILBOX_ID = "1366645701422";
    public static final String MESSAGE = "Test message from junit client";
    public static final String FROM_ADDRESS = "<abc@123.com>";
    public static final String FLAGS_STRING = "TFFFFF-";
    private static final String SRC_FOLDER_NAME = "INBOX";
    private static final String DEST_FOLDER_NAME = "SentMail";
    public static final String FORWARD_SLASH = "/";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        try {
            String mxosPropertyFile = System.getProperty("MXOS_HOME");
            ;
            String mxosconfigfile = mxosPropertyFile
                    + "/config/mxos.properties";
            Properties propfile = new Properties();
            try {
                propfile.load(new FileInputStream(mxosconfigfile));
            } catch (FileNotFoundException e) {
                System.err.println("Unable to find test properties file "
                        + propfile);
            } catch (IOException e) {
                System.err.println("Unable to open test properties file "
                        + propfile);
            }
            CONFIG_HOST = propfile.getProperty("configHost");
            CONFIG_PORT = propfile.getProperty("configPort");
            MSS_RME_VERSION = String.valueOf(MxOSConstants.RME_MX9_VER);
            MSS_HOST = CONFIG_HOST.substring(0, CONFIG_HOST.indexOf("."));
            System.out.println("CONFIG_HOST = " + CONFIG_HOST
                    + " CONFIG_PORT = " + CONFIG_PORT + " MSS_HOST = "
                    + MSS_HOST + " MSS_RME_VERSION = " + MSS_RME_VERSION);
            Config.setConfig("intermail", getConfigdbMap());
            Config config = Config.getInstance();
            createMailBox();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox();
        getConfigdbMap().clear();
    }

    private static Map<String, Object> getConfigdbMap() {
        HashMap<String, Object> configdbMap = new HashMap<String, Object>();
        System.setProperty("mssRmeVersion", MSS_RME_VERSION);
        configdbMap.put("confServHost", CONFIG_HOST);
        configdbMap.put("confServPort", CONFIG_PORT);
        configdbMap.put("defaultApp", DEFAULT_APP);
        configdbMap.put("defaultHost", DEFAULT_HOST);
        return configdbMap;
    }

    @Test
    public void testMoveMessageSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP + "testMoveMessageSuccess");
        try {
            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            UUID destFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, DEST_FOLDER_NAME);
            if (srcFolderUUID == null || destFolderUUID == null) {
                fail();
            }
            UUID[] msgUUID = new UUID[1];
            msgUUID[0] = createMessage(srcFolderUUID);
            MessageMetadata[] cpMessageMetadatas = new MessageMetadata[1];
            cpMessageMetadatas[0] = new MessageMetadata(1);
            cpMessageMetadatas[0].setMessageUUID(msgUUID[0]);
            int options = MoveMsgs.MOVE_MSGS_DISABLE_NOTIFICATION;
            MoveMsgs moveMessage = new MoveMsgs(MSS_HOST, MAILBOX_ID, null,
                    cpMessageMetadatas, srcFolderUUID, destFolderUUID, options);
            moveMessage.execute();
            FolderInfo destFolderInfo = readFolderInfo(DEST_FOLDER_NAME);
            assertEquals("Unxpected number of messages: ", 1,
                    destFolderInfo.getFolderTotMsgs());
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testMoveMessageWithNoSourceFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageWithNoSourceFolder");
        try {
            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            UUID destFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, DEST_FOLDER_NAME);
            UUID[] msgUUID = new UUID[1];
            msgUUID[0] = createMessage(srcFolderUUID);
            srcFolderUUID = null;
            MessageMetadata[] cpMessageMetadatas = new MessageMetadata[1];
            cpMessageMetadatas[0] = new MessageMetadata(1);
            cpMessageMetadatas[0].setMessageUUID(msgUUID[0]);
            int options = MoveMsgs.MOVE_MSGS_DISABLE_NOTIFICATION;
            MoveMsgs moveMessage = new MoveMsgs(MSS_HOST, MAILBOX_ID, null,
                    cpMessageMetadatas, srcFolderUUID, destFolderUUID, options);
            moveMessage.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testMoveMessageWithNoDestinationFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageWithNoDestinationFolder");
        try {
            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            UUID destFolderUUID = null;

            UUID[] msgUUID = new UUID[1];
            msgUUID[0] = createMessage(srcFolderUUID);
            MessageMetadata[] cpMessageMetadatas = new MessageMetadata[1];
            cpMessageMetadatas[0] = new MessageMetadata(1);
            cpMessageMetadatas[0].setMessageUUID(msgUUID[0]);
            int options = MoveMsgs.MOVE_MSGS_DISABLE_NOTIFICATION;
            MoveMsgs moveMessage = new MoveMsgs(MSS_HOST, MAILBOX_ID, null,
                    cpMessageMetadatas, srcFolderUUID, destFolderUUID, options);
            moveMessage.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testMoveMessageWithInvalidMessageID() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageWithInvalidMessageID");
        try {
            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            UUID destFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, DEST_FOLDER_NAME);
            if (srcFolderUUID == null || destFolderUUID == null) {
                fail();
            }
            UUID[] msgUUID = new UUID[1];
            msgUUID[0] = UUID
                    .fromString("69f6c173-13b7-3e87-b4a6-df8b70a3cd17");
            MessageMetadata[] cpMessageMetadatas = new MessageMetadata[1];
            cpMessageMetadatas[0] = new MessageMetadata(1);
            cpMessageMetadatas[0].setMessageUUID(msgUUID[0]);
            int options = MoveMsgs.MOVE_MSGS_DISABLE_NOTIFICATION;
            MoveMsgs moveMessage = new MoveMsgs(MSS_HOST, MAILBOX_ID, null,
                    cpMessageMetadatas, srcFolderUUID, destFolderUUID, options);
            moveMessage.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testMoveMessageWithNullMessageID() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMoveMessageWithNullMessageID");
        try {
            UUID srcFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, SRC_FOLDER_NAME);
            UUID destFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, DEST_FOLDER_NAME);
            if (srcFolderUUID == null || destFolderUUID == null) {
                fail();
            }
            UUID[] msgUUID = null;
            MessageMetadata[] cpMessageMetadatas = new MessageMetadata[1];
            cpMessageMetadatas[0] = new MessageMetadata(1);
            cpMessageMetadatas[0].setMessageUUID(null);
            int options = MoveMsgs.MOVE_MSGS_DISABLE_NOTIFICATION;
            MoveMsgs moveMessage = new MoveMsgs(MSS_HOST, MAILBOX_ID, null,
                    cpMessageMetadatas, srcFolderUUID, destFolderUUID, options);
            moveMessage.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    private static UUID createMessage(UUID folderUUID) {
        try {
            int options = 0x121C;
            CreateMsg cm = new CreateMsg(MSS_HOST, MAILBOX_ID, null,
                    folderUUID, MESSAGE.getBytes(), FROM_ADDRESS, FLAGS_STRING, null,
                    false, 0, 0, options, null, null);
            cm.execute();
            return cm.getMsgUUID();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
        return null;
    }

    private FolderInfo readFolderInfo(String folderName) {
        FolderInfo folderInfo = null;
        try {
            FolderInfo[] folderInfoArray = MssSlMetaCRUD.getFolderInfo(
                    MSS_HOST, MAILBOX_ID, null);
            if (folderInfoArray != null) {
                for (FolderInfo f : folderInfoArray) {
                    if (f.getPathName().equals(FORWARD_SLASH + folderName)) {
                        folderInfo = f;
                        return f;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while reading folder data.", true);
        }
        return folderInfo;
    }

    private static void createMailBox() {
        try {
            CreateMailbox createMailbox = new CreateMailbox(MSS_HOST,
                    MAILBOX_ID, null, null, 0, 0);
            createMailbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while creating mailbox.", true);
        }
    }

    private static void deleteMailBox() {
        try {
            DeleteMailbox deleteMAilbox = new DeleteMailbox(MSS_HOST,
                    MAILBOX_ID, null, false, false, false, RmeDataModel.Leopard);
            deleteMAilbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while deleting mailbox.", true);
        }
    }
}
