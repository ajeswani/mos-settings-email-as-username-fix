package com.opwvmsg.mxos.test.unit.mailsend;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.INumDelayedDeliveryMessagesPendingService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailSendNumDelayedDeliveryMessagesPendingTest {
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "zz123456@openwave.com";
    private static final String PASSWORD = "test1";
    private static INumDelayedDeliveryMessagesPendingService deliveryMessagesPendingService;
    private static final String TEST_NAME = "MailSendDeliveryMessagesPending";
    private static final String ARROW_SEP = " --> ";
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        deliveryMessagesPendingService = (INumDelayedDeliveryMessagesPendingService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.MailSendDeliveryMessagesPendingService
                                .name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        inputParams.clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        inputParams.clear();
        inputParams = null;
        deliveryMessagesPendingService = null;
    }

    @Test
    public void testReadDeliveryMessagesPendingNullEmail() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testReadDeliveryMessagesPendingNullEmail");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, null);
            deliveryMessagesPendingService.read(inputParams);
            fail();
        } catch (MxOSException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testAddDeliveryMessagesPendingWithOutTime() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddDeliveryMessagesPendingWithOutTime");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name(),
                    new ArrayList<String>());
            String numDelayedDeliveryMessagesPending = " ,messageId";
            inputParams.get(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name())
                    .add(numDelayedDeliveryMessagesPending);
            deliveryMessagesPendingService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue(
                    "Error is MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING",
                    e.getCode()
                            .equalsIgnoreCase(
                                    MailboxError.MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                                            .name()));
        }
    }

    @Test
    public void testAddDeliveryMessagesPendingWithOutMessageId()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddDeliveryMessagesPendingWithOutMessageId");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name(),
                    new ArrayList<String>());
            String numDelayedDeliveryMessagesPending = "2011-06-28T17:26:54Z,";
            inputParams.get(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name())
                    .add(numDelayedDeliveryMessagesPending);
            deliveryMessagesPendingService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue(
                    "Error is MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING",
                    e.getCode()
                            .equalsIgnoreCase(
                                    MailboxError.MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                                            .name()));
        }
    }

    @Test
    public void testAddDeliveryMessagesPendingWithTimeMessageID()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddDeliveryMessagesPendingWithTimeMessageID");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name(),
                    new ArrayList<String>());
            String numDelayedDeliveryMessagesPending = "2011-06-28T17:26:54Z,MessageId";
            inputParams.get(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name())
                    .add(numDelayedDeliveryMessagesPending);
            deliveryMessagesPendingService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            List<String> deliveryMessagesPendingList = deliveryMessagesPendingService
                    .read(inputParams);
            System.out.println(deliveryMessagesPendingList);
            assertNotNull(deliveryMessagesPendingList);
            assertTrue(deliveryMessagesPendingList
                    .contains(numDelayedDeliveryMessagesPending));
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testAddDeliveryMessagesPendingWithInvalidTime()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddDeliveryMessagesPendingWithInvalidTime");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name(),
                    new ArrayList<String>());
            String numDelayedDeliveryMessagesPending = "2011-06-28,MessageId";
            inputParams.get(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name())
                    .add(numDelayedDeliveryMessagesPending);
            deliveryMessagesPendingService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }
}