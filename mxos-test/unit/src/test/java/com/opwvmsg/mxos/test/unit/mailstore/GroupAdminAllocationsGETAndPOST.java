package com.opwvmsg.mxos.test.unit.mailstore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IGroupAdminAllocationsService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GroupAdminAllocationsGETAndPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "head1@openwave.com";
    private static final String PASSWORD = "password";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "GroupAdminAllocationsGETAndPOST";
    private static final String ARROW_SEP = " --> ";
    private static IGroupAdminAllocationsService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IGroupAdminAllocationsService) ContextUtils.loadContext()
                .getService(ServiceEnum.GroupAdminAllocationsService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static GroupAdminAllocations getParams() {
        GroupAdminAllocations object = null;
        try {
            object = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return object;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    // maxAdminStorageSizeKB
    @Test
    public void testMaxAdminStorageSizeKBWithMinusOne() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAdminStorageSizeKBWithMinusOne");
        String key = MailboxProperty.maxAdminStorageSizeKB.name();
        addToParams(updateParams, key, "-1");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxAdminStorageSizeKBInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAdminStorageSizeKBInvalidParam");
        String key = MailboxProperty.maxAdminStorageSizeKB.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_ADMIN_STORAGE_SIZEKB.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxAdminStorageSizeKBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAdminStorageSizeKBSuccess");
        String key = MailboxProperty.maxAdminStorageSizeKB.name();
        addToParams(updateParams, key, "150000");
        updateParams(updateParams);
        long value = getParams().getMaxAdminStorageSizeKB();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, 150000);
        updateParams.remove(key);
    }

    @Test
    public void testMaxAdminStorageSizeKBEmptyParamSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAdminStorageSizeKBEmptyParam");
        String key = MailboxProperty.maxAdminStorageSizeKB.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    // maxAdminUsers
    @Test
    public void testMaxAdminUsersWithMinusOne() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testMaxAdminUsersWithMinusOne");
        String key = MailboxProperty.maxAdminUsers.name();
        addToParams(updateParams, key, "-1");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testMaxAdminUsersInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAdminUsersInvalidParam");
        String key = MailboxProperty.maxAdminUsers.name();
        addToParams(updateParams, key, "sadfasf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_ADMIN_USERS.name());
        updateParams.remove(key);
    }

    @Test
    public void testMaxAdminUsersSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxAdminUsersSuccess");
        String key = MailboxProperty.maxAdminUsers.name();
        addToParams(updateParams, key, "200000");
        updateParams(updateParams);
        long maxAdminUsers = getParams().getMaxAdminUsers();
        long adminNumUsers = getParams().getAdminNumUsers();
        long adminUsedStorate = getParams().getAdminUsedStorageSizeKB();
        assertNotNull("Is null. " + adminNumUsers);
        assertNotNull("Is null. " + adminUsedStorate);
        assertNotNull("Is null. ", maxAdminUsers);
        assertEquals("Has a wrong value.", maxAdminUsers, 200000);
        updateParams.remove(key);
    }

    @Test
    public void testMaxAdminUsersEmptyParamSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxAdminUsersEmptyParamSuccess");
        String key = MailboxProperty.maxAdminUsers.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }
}
