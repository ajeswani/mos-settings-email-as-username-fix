package com.opwvmsg.mxos.test.unit.mailaccess;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.IntAccessType;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetMailAccessTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IMailAccessService ma;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetMailAccessTest.setUpBeforeClass...");
        ma = (IMailAccessService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailAccessService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetMailAccessTest.setUp...");
        assertNotNull("MailAccessService object is null.", ma);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetMailAccessTest.tearDown...");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetMailAccessTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        ma = null;
    }

    private static MailAccess getMailAccessParams(
            Map<String, List<String>> params) {
        return getMailAccessParams(params, null);
    }

    private static MailAccess getMailAccessParams(
            Map<String, List<String>> params, String expectedError) {
        MailAccess mao = null;
        try {
            mao = ma.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MailAccess object is null.", mao);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
            }
        }
        return mao;
    }

    @Test
    public void testGetMailAccessWithoutEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithoutEmail...");
        // Clear the params.
        params.clear();
        getMailAccessParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetMailAccessWithNullEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNullEmail...");
        // Remove the email from the map to have a null email.
        params.get(EMAIL_KEY).clear();
        getMailAccessParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetMailAccessWithNonExistingEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNonExistingEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("something.junk@foobar.com");
        getMailAccessParams(params, MailboxError.MBX_NOT_FOUND.name());
    }

    @Test
    public void testGetMailAccessWithSplCharsInEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithSplCharsInEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("!#$%^&*()+-=.junk@foobar.com");
        getMailAccessParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testGetMailAccessWithSplCharsInDomain() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithSplCharsInDomain...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("junk@foo!#+()$&*^-bar.com");
        getMailAccessParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testPopAccessTypeSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testPopAccessType...");
        AccessType pa = getMailAccessParams(params).getPopAccessType();
        assertNotNull("PopAccessType is null.", pa);
        assertTrue("PopAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testPopSSLAccessTypeSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testPopSSLAccessTypeSuccess...");
        AccessType pa = getMailAccessParams(params).getPopSSLAccessType();
        assertNotNull("PopSSLAccessType is null.", pa);
        assertTrue("PopSSLAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testImapAccessTypeSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testImapAccessTypeSuccess...");
        AccessType pa = getMailAccessParams(params).getImapAccessType();
        assertNotNull("ImapAccessType is null.", pa);
        assertTrue("ImapAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testImapSSLAccessTypeSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testImapSSLAccessTypeSuccess...");
        AccessType pa = getMailAccessParams(params).getImapSSLAccessType();
        assertNotNull("ImapSSLAccessType is null.", pa);
        assertTrue("ImapSSLAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testSmtpAccessEnabledSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testSmtpAccessEnabledSuccess...");
        BooleanType pa = getMailAccessParams(params).getSmtpAccessEnabled();
        assertNotNull("SmtpAccessEnabled is null.", pa);
        assertTrue("SmtpAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testSmtpSSLAccessEnabledSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testSmtpSSLAccessEnabledSuccess...");
        BooleanType pa = getMailAccessParams(params).getSmtpSSLAccessEnabled();
        assertNotNull("SmtpSSLAccessEnabled is null.", pa);
        assertTrue("SmtpSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testSmtpAuthenticationEnabledSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testSmtpAuthenticationEnabledSuccess...");
        BooleanType pa = getMailAccessParams(params).getSmtpAuthenticationEnabled();
        assertNotNull("SmtpAuthenticationEnabled is null.", pa);
        assertTrue("SmtpAuthenticationEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testPopAuthenticationTypeSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testPopAuthenticationTypeSuccess...");
        IntAccessType at = getMailAccessParams(params).getPopAuthenticationType();
        assertNotNull("PopAuthenticationType is null.", at);
        assertTrue("PopAuthenticationType has a wrong value.",
                at.equals(IntAccessType._0));
    }

    @Test
    public void testWebmailAccessTypeSuccess() throws Exception {
        System.out.println("GetMailAccessTest.testWebmailAccessTypeSuccess...");
        AccessType pa = getMailAccessParams(params).getWebmailAccessType();
        assertNotNull("WebmailAccessType is null.", pa);
        assertTrue("WebmailAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testWebmailSSLAccessTypeSuccess() throws Exception {
        System.out
                .println("GetMailAccessTest.testWebmailSSLAccessTypeSuccess...");
        AccessType pa = getMailAccessParams(params).getWebmailSSLAccessType();
        assertNotNull("WebmailSSLAccessType is null.", pa);
        assertTrue("WebmailSSLAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testMobileMailAccessTypeSuccess() throws Exception {
        System.out
                .println("GetMailAccessTest.testMobileMailAccessTypeSuccess...");
        AccessType pa = getMailAccessParams(params).getMobileMailAccessType();
        assertNotNull("MobileMailAccessType is null.", pa);
        assertTrue("MobileMailAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testMobileActiveSyncAllowedSuccess() throws Exception {
        System.out
                .println("GetMailAccessTest.testMobileActiveSyncAllowedSuccess...");
        BooleanType pa = getMailAccessParams(params).getMobileActiveSyncAllowed();
        assertNotNull("MobileActiveSyncAllowed is null.", pa);
        assertTrue("MobileActiveSyncAllowed has a wrong value.",
                pa.equals(BooleanType.YES));
    }

}
