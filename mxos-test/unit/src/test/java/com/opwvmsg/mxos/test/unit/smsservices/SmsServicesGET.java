package com.opwvmsg.mxos.test.unit.smsservices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class SmsServicesGET {

    private static final String TEST_NAME = "SmsServicesGET";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String EMAIL = "test.smsservices@openwave.com";
    private static final String PASSWORD = "Password1";
    private static ISmsServicesService smsServicesService;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        smsServicesService = (ISmsServicesService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsServicesService.name());
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("SmsServicesService object is null.", smsServicesService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        smsServicesService = null;
        mailboxService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }
    
    private static SmsServices getParams() {
        return getParams(null);
    }

    private static SmsServices getParams(String expectedError) {
        SmsServices smsServices = null;
        try {
            smsServices = smsServicesService.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                addToParams(params, EMAIL_KEY, EMAIL);
            }
        }
        return smsServices;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            smsServicesService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }
    
    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("MailBox was not created.", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testGetWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithoutEmail");
        // Clear the params.
        params.remove(EMAIL_KEY);
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithNullEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, null);
        getParams(ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithEmptyEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "");
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "something.junk@foobar.com");
        getParams(MailboxError.MBX_NOT_FOUND.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInEmail() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testGetWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "!#$%^&*()+-=.junk@foobar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "junk@$&*^-bar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSmsServicesAllowed() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSmsServicesAllowed");
        String key = MailboxProperty.smsServicesAllowed.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(MxosEnums.BooleanType.YES.name());
        updateParams(params);
        String smsServicesAllowed = getParams().getSmsServicesAllowed().name();
        assertNotNull("smsServicesAllowed is null.", smsServicesAllowed);
    }

    @Test
    public void testSmsServicesMsisdn() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSmsServicesMsisdn");
        String key = MailboxProperty.smsServicesMsisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("+919999999999");
        updateParams(params);
        params.remove(key);
        String smsServicesMsisdn = getParams().getSmsServicesMsisdn();
        assertNotNull("smsServicesMsisdn is null.", smsServicesMsisdn);
    }

    @Test
    public void testSmsServicesMsisdnStatus() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesMsisdnStatus");
        String key = MailboxProperty.smsServicesMsisdnStatus.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("activated");
        updateParams(params);
        String smsServicesMsisdnStatus = (getParams()
                .getSmsServicesMsisdnStatus()).name();
        assertNotNull("smsServicesMsisdnStatus is null.",
                smsServicesMsisdnStatus);
    }

    /* @Test
    public void testLastSmsServicesMsisdnStatusChangeDate() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLastSmsServicesMsisdnStatusChangeDate");
        String lastSmsServicesMsisdnStatusChangeDate = getParams()
                .getLastSmsServicesMsisdnStatusChangeDate().toString();
        assertNotNull("lastSmsServicesMsisdnStatusChangeDate is null.",
                lastSmsServicesMsisdnStatusChangeDate);
    }*/

}
