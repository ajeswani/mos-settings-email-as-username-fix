package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateMessageFlagsTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String FOLDER_KEY = FolderProperty.folderName.name();
    private static final String MSG_ID_KEY = MessageProperty.messageId.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static final String FOLDER = "INBOX";
    private static final String MSG_ID = "<29-2013-0114-122517-22263@rwcvmx83c0106.openwave.com>";
    private static IMetadataService service;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateMessageFlagsTest.setUpBeforeClass...");
        service = (IMetadataService) ContextUtils.loadContext().getService(
                ServiceEnum.MetadataService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
        updateParams.get(EMAIL_KEY).add(EMAIL);
        updateParams.put(FOLDER_KEY, new ArrayList<String>());
        updateParams.get(FOLDER_KEY).add(FOLDER);
        updateParams.put(MSG_ID_KEY, new ArrayList<String>());
        updateParams.get(MSG_ID_KEY).add(MSG_ID);

        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
        getParams.put(FOLDER_KEY, new ArrayList<String>());
        getParams.get(FOLDER_KEY).add(FOLDER);
        getParams.put(MSG_ID_KEY, new ArrayList<String>());
        getParams.get(MSG_ID_KEY).add(MSG_ID);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateMessageFlagsTest.setUp...");
        assertNotNull("Service object is null.", service);

        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());

        assertNotNull("Input Param:folderName is null.", updateParams.get(FOLDER_KEY));
        assertTrue("Input Param:folderName is empty.", !updateParams.get(FOLDER_KEY)
                .isEmpty());

        assertNotNull("Input Param:messageId is null.", updateParams.get(MSG_ID_KEY));
        assertTrue("Input Param:messageId is empty.", !updateParams.get(MSG_ID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateMessageFlagsTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateMessageFlagsTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static Flags getMessageFlagsParams() {
        Flags flags = null;
        try {
            // flags =
            service.read(getParams);
            // TODO - flags to be mapped
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
            if (getParams.isEmpty() || null == getParams.get(FOLDER_KEY)
                    || getParams.get(FOLDER_KEY).isEmpty()) {
                getParams.put(FOLDER_KEY, new ArrayList<String>());
                getParams.get(FOLDER_KEY).add(FOLDER);
            }
            if (getParams.isEmpty() || null == getParams.get(MSG_ID_KEY)
                    || getParams.get(MSG_ID_KEY).isEmpty()) {
                getParams.put(MSG_ID_KEY, new ArrayList<String>());
                getParams.get(MSG_ID_KEY).add(MSG_ID);
            }
        }
        assertNotNull("Flags object is null.", flags);
        return flags;
    }

    private static void updateMessageFlagsParams(
            Map<String, List<String>> updateParams) {
        updateMessageFlagsParams(updateParams, null);
    }

    private static void updateMessageFlagsParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                updateParams.put(EMAIL_KEY, new ArrayList<String>());
                updateParams.get(EMAIL_KEY).add(EMAIL);
            }
            if (updateParams.isEmpty() || null == updateParams.get(FOLDER_KEY)
                    || updateParams.get(FOLDER_KEY).isEmpty()) {
                updateParams.put(FOLDER_KEY, new ArrayList<String>());
                updateParams.get(FOLDER_KEY).add(FOLDER);
            }
            if (updateParams.isEmpty() || null == updateParams.get(MSG_ID_KEY)
                    || updateParams.get(MSG_ID_KEY).isEmpty()) {
                updateParams.put(MSG_ID_KEY, new ArrayList<String>());
                updateParams.get(MSG_ID_KEY).add(MSG_ID);
            }
        }
    }

    @Test
    public void testUpdateFlagsWithEmptyEmail() throws Exception {
        updateParams.get(EMAIL_KEY).clear();
        updateParams.get(EMAIL_KEY).add("");
        System.out
                .println("UpdateMessageFlagsTest.testUpdateFlagsWithEmptyEmail...");
        updateMessageFlagsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateFlagsWithNullEmail() throws Exception {
        updateParams.get(EMAIL_KEY).clear();
        System.out
                .println("UpdateMessageFlagsTest.testUpdateFlagsWithNullEmail...");
        updateMessageFlagsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateFlagsWithEmptyFolder() throws Exception {
        updateParams.get(FOLDER_KEY).clear();
        updateParams.get(FOLDER_KEY).add("");
        System.out
                .println("UpdateMessageFlagsTest.testUpdateFlagsWithEmptyFolder...");
        updateMessageFlagsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateFlagsWithNullFolder() throws Exception {
        updateParams.get(FOLDER_KEY).clear();
        System.out
                .println("UpdateMessageFlagsTest.testUpdateFlagsWithNullFolder...");
        updateMessageFlagsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }


    @Test
    public void testUpdateFlagsWithEmptyMessageId() throws Exception {
        updateParams.get(MSG_ID_KEY).clear();
        updateParams.get(MSG_ID_KEY).add("");
        System.out
                .println("UpdateMessageFlagsTest.testUpdateFlagsWithEmptyMessageId...");
        updateMessageFlagsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateFlagsWithNullMessageId() throws Exception {
        updateParams.get(MSG_ID_KEY).clear();
        System.out
                .println("UpdateMessageFlagsTest.testUpdateFlagsWithNullMessageId...");
        updateMessageFlagsParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testFlagSeenNullParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagSeenNullParam...");
        String key = MessageProperty.flagSeen.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_SEEN.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagSeenEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagSeenEmptyParam...");
        String key = MessageProperty.flagSeen.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_SEEN.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagSeenInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagSeenInvalidParam...");
        String key = MessageProperty.flagSeen.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_SEEN.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagSeenSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagSeenSplCharsInParam...");
        String key = MessageProperty.flagSeen.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_SEEN.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagSeenSuccess1() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagSeenSuccess1...");
        String key = MessageProperty.flagSeen.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.TRUE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagSeen();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.TRUE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagSeenSuccess2() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagSeenSuccess2...");
        String key = MessageProperty.flagSeen.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.FALSE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagSeen();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.FALSE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagAnsNullParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagAnsNullParam...");
        String key = MessageProperty.flagAns.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_ANS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagAnsEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagAnsEmptyParam...");
        String key = MessageProperty.flagAns.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_ANS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagAnsInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagAnsInvalidParam...");
        String key = MessageProperty.flagAns.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_ANS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagAnsSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagAnsSplCharsInParam...");
        String key = MessageProperty.flagAns.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_ANS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagAnsSuccess1() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagAnsSuccess1...");
        String key = MessageProperty.flagAns.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.TRUE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagAns();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.TRUE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagAnsSuccess2() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagAnsSuccess2...");
        String key = MessageProperty.flagAns.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.FALSE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagAns();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.FALSE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagFlaggedNullParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagFlaggedNullParam...");
        String key = MessageProperty.flagFlagged.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_FLAGGED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagFlaggedEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagFlaggedEmptyParam...");
        String key = MessageProperty.flagFlagged.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_FLAGGED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagFlaggedInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagFlaggedInvalidParam...");
        String key = MessageProperty.flagFlagged.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_FLAGGED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagFlaggedSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagFlaggedSplCharsInParam...");
        String key = MessageProperty.flagFlagged.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_FLAGGED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagFlaggedSuccess1() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagFlaggedSuccess1...");
        String key = MessageProperty.flagFlagged.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.TRUE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagFlagged();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.TRUE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagFlaggedSuccess2() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagFlaggedSuccess2...");
        String key = MessageProperty.flagFlagged.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.FALSE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagFlagged();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.FALSE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagDelNullParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDelNullParam...");
        String key = MessageProperty.flagDel.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DEL.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDelEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDelEmptyParam...");
        String key = MessageProperty.flagDel.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DEL.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDelInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDelInvalidParam...");
        String key = MessageProperty.flagDel.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DEL.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDelSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDelSplCharsInParam...");
        String key = MessageProperty.flagDel.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DEL.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDelSuccess1() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagDelSuccess1...");
        String key = MessageProperty.flagDel.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.TRUE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagDel();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.TRUE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagDelSuccess2() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagDelSuccess2...");
        String key = MessageProperty.flagDel.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.FALSE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagDel();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.FALSE));
        updateParams.remove(key);
    }

    /*@Test
    public void testFlagResNullParam() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagResNullParam...");
        String key = MessageProperty.flagRes.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_RES.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagResEmptyParam() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagResEmptyParam...");
        String key = MessageProperty.flagRes.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_RES.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagResInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagResInvalidParam...");
        String key = MessageProperty.flagRes.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_RES.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagResSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagResSplCharsInParam...");
        String key = MessageProperty.flagRes.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_RES.name());
        updateParams.remove(key);
    }*/

    @Test
    public void testFlagDraftNullParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDraftNullParam...");
        String key = MessageProperty.flagDraft.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DRAFT.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDraftEmptyParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDraftEmptyParam...");
        String key = MessageProperty.flagDraft.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DRAFT.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDraftInvalidParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDraftInvalidParam...");
        String key = MessageProperty.flagDraft.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DRAFT.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDraftSplCharsInParam() throws Exception {
        System.out
                .println("UpdateMessageFlagsTest.testFlagDraftSplCharsInParam...");
        String key = MessageProperty.flagDraft.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateMessageFlagsParams(updateParams,
                MessageError.MSG_INVALID_FLAG_DRAFT.name());
        updateParams.remove(key);
    }

    @Test
    public void testFlagDraftSuccess1() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagDraftSuccess1...");
        String key = MessageProperty.flagDraft.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.TRUE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagDraft();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.TRUE));
        updateParams.remove(key);
    }

    @Test
    public void testFlagDraftSuccess2() throws Exception {
        System.out.println("UpdateMessageFlagsTest.testFlagDraftSuccess2...");
        String key = MessageProperty.flagDraft.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(TrueOrFalse.FALSE.toString());
        updateMessageFlagsParams(updateParams);
        Boolean value = getMessageFlagsParams().getFlagDraft();
        assertNotNull("Flag is null.", value);
        assertTrue("Flag has wrong value.", value.equals(Boolean.FALSE));
        updateParams.remove(key);
    }

}
