package com.opwvmsg.mxos.test.unit.mailstore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalStoreService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ExternalStorePOST {

    private static final String TEST_NAME = "ExternalStorePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String EMAIL = "test.ms@openwave.com";
    private static final String PASSWORD = "Password1";
    private static final String COSID = "default_mxos.2.0";
    private static IExternalStoreService externalStoreService;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        inputParams.put(MailboxProperty.cosId.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.cosId.name()).add(COSID);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("MailBox was not created.", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static ExternalStore getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static ExternalStore getParams(Map<String, List<String>> params,
            String expectedError) {
        ExternalStore externalStore = null;
        try {
            externalStore = externalStoreService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("SenderBlocking object is null.", externalStore);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        }
        return externalStore;
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalStoreService = (IExternalStoreService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ExternalStoreService.name());
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, COSID);
        updateParams.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL);
        CosHelper.deleteCos(COSID);
        updateParams.clear();
        updateParams = null;
        externalStoreService = null;
        mailboxService = null;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            MailboxError expectedError) {
        try {
            externalStoreService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("mailReceiptService object is null.",
                externalStoreService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        updateParams.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        updateParams.get(EMAIL_KEY).clear();
    }

    @Test
    public void testExternalStoreAccessAllowedEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(), "");
        updateParams(updateParams);
    }

    @Test
    public void testExternalStoreAccessAllowedInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(), "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED);
    }

    // externalStoreAccessAllowed
    @Test
    public void testExternalStoreAccessAllowedNullParam() throws Exception {
        if (System.getProperty(ContextProperty.MXOS_CONTEXT_ID) == ContextEnum.BACKEND
                .name()) {
            System.out.println(TEST_NAME + ARROW_SEP
                    + "testExternalStoreAccessAllowedNullParam");

            Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
            updateParams.clear();
            addParams(updateParams, EMAIL_KEY, EMAIL);
            addParams(updateParams,
                    MailboxProperty.externalStoreAccessAllowed.name(), null);
            updateParams(updateParams,
                    MailboxError.MBX_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED);
        }
    }

    @Test
    public void testExternalStoreAccessAllowedSplCharsInParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(), "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED);
    }

    @Test
    public void testMaxExternalStoreSizeMBEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "");
        updateParams(updateParams);
    }

    @Test
    public void testMaxExternalStoreSizeMBInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_EXTERNAL_STORE_SIZEMB);
    }

    // maxExternalStoreSizeMB
    @Test
    public void testMaxExternalStoreSizeMBNullParam() throws Exception {
        // Test is only valid for backend context, so ignore for rest context
        if (System.getProperty(ContextProperty.MXOS_CONTEXT_ID) == ContextEnum.BACKEND
                .name()) {
            System.out.println(TEST_NAME + ARROW_SEP
                    + "testMaxExternalStoreSizeMBNullParam");

            Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
            updateParams.clear();
            addParams(updateParams, EMAIL_KEY, EMAIL);
            addParams(updateParams,
                    MailboxProperty.maxExternalStoreSizeMB.name(), null);
            updateParams(updateParams,
                    MailboxError.MBX_INVALID_MAX_EXTERNAL_STORE_SIZEMB);
        }
    }

    @Test
    public void testMaxExternalStoreSizeMBSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaWarningThresholdSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MAX_EXTERNAL_STORE_SIZEMB);
    }

    @Test
    public void testMaxExternalStoreSizeMBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "1");

        updateParams(updateParams);

        addParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testStoreAccessAllowedSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(),
                MxosEnums.BooleanType.YES.name());
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "1");

        updateParams(updateParams);

        addParams(updateParams, EMAIL_KEY, EMAIL);
        ExternalStore es = getParams(updateParams);
        if (es != null) {
            Integer maxExternalStoreSizeMB = es.getMaxExternalStoreSizeMB();
            assertNotNull("Is null.", maxExternalStoreSizeMB);
            assertTrue("Has a wrong value.", maxExternalStoreSizeMB == 1);

            String externalStoreAccessAllowed = es
                    .getExternalStoreAccessAllowed();
            assertNotNull("Is null.", externalStoreAccessAllowed);
            assertTrue("Has a wrong value.",
                    externalStoreAccessAllowed
                            .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        }
    }

    @Test
    public void testStoreAccessAllowedSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedSuccess1");
        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, EMAIL_KEY, EMAIL);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(),
                MxosEnums.BooleanType.NO.name());

        updateParams(updateParams);

        addParams(updateParams, EMAIL_KEY, EMAIL);
        ExternalStore es = getParams(updateParams);
        if (es != null) {
            String externalStoreAccessAllowed = es
                    .getExternalStoreAccessAllowed();
            assertNotNull("Is null.", externalStoreAccessAllowed);
            assertTrue("Has a wrong value.",
                    externalStoreAccessAllowed
                            .equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
        }
    }
}
