package com.opwvmsg.mxos.test.unit.cos.generalpreferences;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Test Suite for test cases related to GeneralPreferences for Cos service.
 * 
 * @author mxos-dev
 *
 */

@RunWith(Suite.class)
// Include all the test case classes related to COS GeneralPreferences
@SuiteClasses({
    GetCosGeneralPreferencesTest.class,
    UpdateCosGeneralPreferencesTest.class
})
public class CosGeneralPreferencesSuite {
}