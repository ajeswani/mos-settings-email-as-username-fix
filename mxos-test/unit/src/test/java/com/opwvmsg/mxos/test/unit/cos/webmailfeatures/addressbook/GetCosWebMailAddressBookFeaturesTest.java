package com.opwvmsg.mxos.test.unit.cos.webmailfeatures.addressbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosWebMailFeaturesAddressBookService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetCosWebMailAddressBookFeaturesTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosWebMailFeaturesAddressBookService wmfsab;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTest.setUpBeforeClass...");
        wmfsab = (ICosWebMailFeaturesAddressBookService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosWebMailFeaturesAddressBookService.name());
        boolean flag = CosHelper.createCos(COS_ID);
        assertTrue("Cos was not created.", flag);
        params.put(COSID_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTest.setUp...");
        assertNotNull("CosWebMailFeaturesService object is null.", wmfsab);
        assertNotNull("Input Param:cosId is null.", params.get(COSID_KEY));
        params.get(COSID_KEY).add(COS_ID);
        assertTrue("Input Param:cosId is empty.", !params.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTest.tearDown...");
        if (null != params.get(COSID_KEY)) {
            params.get(COSID_KEY).clear();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTest.tearDownAfterClass...");
        CosHelper.deleteCos(COS_ID);
        params.clear();
        params = null;
        wmfsab = null;
    }

    private static AddressBookFeatures getCosWebMailFeaturesAddressBookParams(
            Map<String, List<String>> params) {
        return getCosWebMailFeaturesAddressBookParams(params, null);
    }

    private static AddressBookFeatures getCosWebMailFeaturesAddressBookParams(
            Map<String, List<String>> params, String expectedError) {
        AddressBookFeatures ab = null;
        try {
            ab = wmfsab.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("WebMailFeatures object is null.", ab);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(COSID_KEY, new ArrayList<String>());
            }
        }
        return ab;
    }

    @Test
    public void testSetMaxContacts() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTesttestSetMaxContacts...");
        int maxContacts = getCosWebMailFeaturesAddressBookParams(params).getMaxContacts();
        System.out.println("MaxContacts : "+maxContacts);
    }

    @Test
    public void testSetMaxGroups() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTesttestSetMaxContacts...");
        int maxContacts = getCosWebMailFeaturesAddressBookParams(params).getMaxGroups();
        System.out.println("MaxContacts : "+maxContacts);
    }
    
    @Test
    public void testSetMaxContactsPerGroup() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTesttestSetMaxContacts...");
        int maxContacts = getCosWebMailFeaturesAddressBookParams(params).getMaxContactsPerGroup();
        System.out.println("MaxContacts : "+maxContacts);
    }
    
    @Test
    public void testSetMaxContactsPerPage() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTesttestSetMaxContacts...");
        int maxContacts = getCosWebMailFeaturesAddressBookParams(params).getMaxContactsPerPage();
        System.out.println("MaxContacts : "+maxContacts);
    }
    
    @Test
    public void testSetCreateContactsFromOutgoingEmails() throws Exception {
        System.out.println("GetCosWebMailAddressBookFeaturesTestcreateContactsFromOutgoingEmails...");
        String foe = getCosWebMailFeaturesAddressBookParams(params).getCreateContactsFromOutgoingEmails().toString();
        assertNotNull("CreateContactsFromOutgoingEmails is null.", foe);
        System.out.println("CreateContactsFromOutgoingEmails : "+foe);
    }
}
