package com.opwvmsg.mxos.test.unit.mailstore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailStoreService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailStoreGET {

    private static final String TEST_NAME = "MailStoreGET";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String EMAIL = "test.ms@openwave.com";
    private static final String PASSWORD = "Password1";
    private static IMailStoreService mailStoreService;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("MailBox was not created.", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static MailStore getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static MailStore getParams(Map<String, List<String>> params,
            MailboxError expectedError) {
        MailStore mailStore = null;
        try {
            mailStore = mailStoreService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("SmsServices object is null.", mailStore);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
        return mailStore;
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        mailStoreService = (IMailStoreService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailStoreService.name());
        mailboxService = (IMailboxService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxService.name());
        //createMailBox(EMAIL, PASSWORD);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        //deleteMailBox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        mailStoreService = null;
        mailboxService = null;
    }

    // externalStoreAccessAllowed
    @Test
    public void testExternalStoreAccessAllowed() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowed");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        ExternalStore es = null;
        MailStore ms = getParams(params);
        if (ms.getExternalStore() instanceof ExternalStore) {
            es = (ExternalStore) ms.getExternalStore();
        }
        if (es != null) {
            String externalStoreAccessAllowed = es
                    .getExternalStoreAccessAllowed();
            assertNull("Is null.", externalStoreAccessAllowed);
        }
    }

    // folderQuota
    @Test
    public void testFolderQuota() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testFolderQuota");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String folderQuota = getParams(params).getFolderQuota();
        assertNull("Is null.", folderQuota);
    }

    // maxExternalStoreSizeMB
    @Test
    public void testMaxExternalStoreSizeMB() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testMaxExternalStoreSizeMB");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        ExternalStore es = null;
        MailStore ms = getParams(params);
        if (ms.getExternalStore() instanceof ExternalStore) {
            es = (ExternalStore) ms.getExternalStore();
        }
        if (es != null) {
            Integer maxExternalStoreSizeMB = es.getMaxExternalStoreSizeMB();
            assertNull("Is null.", maxExternalStoreSizeMB);
        }
    }

    // maxMessages
    @Test
    public void testMaxMessages() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxMessages");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Integer maxMessages = getParams(params).getMaxMessages();
        assertNotNull("Is null.", maxMessages);
    }

    // maxStorageSizeKB
    @Test
    public void testMaxStorageSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxStorageSizeKB");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Long maxStorageSizeKB = getParams(params).getMaxStorageSizeKB();
        assertNotNull("Is null.", maxStorageSizeKB);
    }

    // mobileMaxMessages
    @Test
    public void testMobileMaxMessages() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMobileMaxMessages");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Integer mobileMaxMessages = getParams(params).getMobileMaxMessages();
        assertNotNull("Is null.", mobileMaxMessages);
    }

    // mobileMaxStorageSizeKB
    @Test
    public void testMobileStorageSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMobileStorageSizeKB");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Long mobileMaxStorageSizeKB = getParams(params).getMobileMaxStorageSizeKB();
        assertNotNull("Is null.", mobileMaxStorageSizeKB);
    }
    
    // maxMessagesSoftLimit
    @Test
    public void testMaxMessagesSoftLimit() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxMessagesSoftLimit");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Integer maxMessagesSoftLimit = getParams(params).getMaxMessagesSoftLimit();
        assertNotNull("Is null.", maxMessagesSoftLimit);
    }

    // maxStorageSizeKBSoftLimit
    @Test
    public void testMaxStorageSizeKBSoftLimit() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxStorageSizeKBSoftLimit");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Long maxStorageSizeKBSoftLimit = getParams(params).getMaxStorageSizeKBSoftLimit();
        assertNotNull("Is null.", maxStorageSizeKBSoftLimit);
    }
        
    // mobileMaxMessagesSoftLimit
    @Test
    public void testMobileMaxMessagesSoftLimit() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMobileMaxMessagesSoftLimit");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Integer mobileMaxMessagesSoftLimit = getParams(params).getMobileMaxMessagesSoftLimit();
        assertNotNull("Is null.", mobileMaxMessagesSoftLimit);
    }

    // mobileMaxStorageSizeKBSoftLimit
    @Test
    public void testMobileMaxStorageSizeKBSoftLimit() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMobileMaxStorageSizeKBSoftLimit");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Long mobileMaxStorageSizeKBSoftLimit = getParams(params).getMobileMaxStorageSizeKBSoftLimit();
        assertNotNull("Is null.", mobileMaxStorageSizeKBSoftLimit);
    }
    // quotaBounceNotify
    @Test
    public void testQuotaBounceNotify() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testQuotaBounceNotify");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String quotaBounceNotify = getParams(params).getQuotaBounceNotify();
        assertNotNull("Is null.", quotaBounceNotify);
    }

    // quotaWarningThreshold
    @Test
    public void testQuotaWarningThreshold() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testQuotaWarningThreshold");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        Integer quotaWarningThreshold = getParams(params)
                .getQuotaWarningThreshold();
        assertNotNull("Is null.", quotaWarningThreshold);
    }
    
    // largeMailboxPlatformEnabled
    @Test
    public void testLargeMailboxPlatformEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testLargeMailboxPlatformEnabled");
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String largeMailboxPlatformEnabled = getParams(params).getLargeMailboxPlatformEnabled();
        assertNotNull("Is null.", largeMailboxPlatformEnabled);
    }
}
