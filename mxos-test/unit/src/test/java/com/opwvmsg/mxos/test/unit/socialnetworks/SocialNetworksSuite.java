package com.opwvmsg.mxos.test.unit.socialnetworks;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    SocialNetworksTest.class,
    SocialNetworkSitesTest.class
})
public class SocialNetworksSuite {
}
