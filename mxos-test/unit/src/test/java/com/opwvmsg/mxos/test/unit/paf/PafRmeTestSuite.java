package com.opwvmsg.mxos.test.unit.paf;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
    CreateMailboxRmeTest.class,
    ReadMailboxInfoRmeTest.class,
    MailboxMaintenanceRmeTest.class,
    ReadMailboxAccessInfoRmeTest.class,
    UpdateMailboxAccessInfoTest.class,
    CreateFolderRmeTest.class,
    CreateMessageRmeTest.class,
    ReadMessagesRmeTest.class,
    CopyMessagesRmeTest.class,
    MoveMessagesRmeTest.class,
    ReadSortedMessagesRmeTest.class,
    UpdateMessageFlagsRmeTest.class,
    DeleteMultiMessagesRmeTest.class,
    SetAutoReplyRmeTest.class,
    GetAutoReplyRmeTest.class,
    RenameFolderRmeTest.class,
    SubscribeFolderRmeTest.class,
    GetMessageAttributesRmeTest.class,
    DeleteFolderRmeTest.class,
    DeleteMailboxRmeTest.class,
    PopListRmeTest.class
})

public class PafRmeTestSuite {
}
