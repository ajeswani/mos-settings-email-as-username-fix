package com.opwvmsg.mxos.test.unit.cos.socialnetworks;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    CosSocialNetworksTest.class
})
public class CosSocialNetworksSuite {
}
