package com.opwvmsg.mxos.test.unit.paf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMailbox;
import com.opwvmsg.utils.paf.intermail.replystore.Reply;
import com.opwvmsg.utils.paf.intermail.replystore.ops.ReadReply;
import com.opwvmsg.utils.paf.intermail.replystore.ops.UpdateReply;

public class GetAutoReplyRmeTest {

    private static final String TEST_NAME = "GetAutoReplyRmeTest";
    private static final String ARROW_SEP = " --> ";

    public static String CONFIG_HOST = null;
    public static String CONFIG_PORT = null;
    public static String MSS_HOST = null;
    public static final String DEFAULT_APP = "mss";
    public static final String DEFAULT_HOST = "*";
    public static String MSS_RME_VERSION = "175";
    // public static final String MAILBOX_ID = "1366645701422";
    public static final String MAILBOX_ID = "7126905549041528032";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        try {
            String mxosPropertyFile = System.getProperty("MXOS_HOME");
            String mxosconfigfile = mxosPropertyFile
                    + "/config/mxos.properties";
            Properties propfile = new Properties();
            try {
                propfile.load(new FileInputStream(mxosconfigfile));
            } catch (FileNotFoundException e) {
                System.err.println("Unable to find test properties file "
                        + propfile);
            } catch (IOException e) {
                System.err.println("Unable to open test properties file "
                        + propfile);
            }
            CONFIG_HOST = propfile.getProperty("configHost");
            CONFIG_PORT = propfile.getProperty("configPort");
            MSS_RME_VERSION = String.valueOf(MxOSConstants.RME_MX9_VER);
            MSS_HOST = CONFIG_HOST.substring(0, CONFIG_HOST.indexOf("."));
            System.out.println("CONFIG_HOST = " + CONFIG_HOST
                    + " CONFIG_PORT = " + CONFIG_PORT + " MSS_HOST = "
                    + MSS_HOST + " MSS_RME_VERSION = " + MSS_RME_VERSION);
            Config.setConfig("intermail", getConfigdbMap());
            Config config = Config.getInstance();
            createMailBox();
            setAutoReply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox();
        getConfigdbMap().clear();
    }

    private static Map<String, Object> getConfigdbMap() {
        HashMap<String, Object> configdbMap = new HashMap<String, Object>();
        System.setProperty("mssRmeVersion", MSS_RME_VERSION);
        configdbMap.put("confServHost", CONFIG_HOST);
        configdbMap.put("confServPort", CONFIG_PORT);
        configdbMap.put("defaultApp", DEFAULT_APP);
        configdbMap.put("defaultHost", DEFAULT_HOST);
        return configdbMap;
    }

    @Test
    public void testGetAutoReplySuccess() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetAutoReplySuccess");
        try {
            String autoReplyMessage = "";

            ReadReply readReply = new ReadReply(MSS_HOST, MAILBOX_ID, null,
                    Reply.AUTOREPLY, RmeDataModel.Leopard);
            readReply.execute();
            Reply reply = readReply.getReply();
            autoReplyMessage = reply.getText();
            assertEquals("Unxpected Errors: ", 0, readReply.getNumLogEvents());
            assertNotNull("Auto reply is not null", autoReplyMessage);
            System.out.println("autoReplyMessage == " + autoReplyMessage);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    private static void createMailBox() {
        try {
            CreateMailbox createMailbox = new CreateMailbox(MSS_HOST,
                    MAILBOX_ID, null, null, 0, 0);
            createMailbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while creating mailbox.", true);
        }
    }

    private static void deleteMailBox() {
        try {
            DeleteMailbox deleteMAilbox = new DeleteMailbox(MSS_HOST,
                    MAILBOX_ID, null, false, false, false, RmeDataModel.Leopard);
            deleteMAilbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while deleting mailbox.", true);
        }
    }

    private static void setAutoReply() {
        try {
            String autoReplyMessage = "Test message for auto reply";
            Reply reply = new Reply();
            reply.setType(Reply.AUTOREPLY);
            reply.setText(autoReplyMessage);
            int operation = UpdateReply.SET_ENTRY;
            if (null == autoReplyMessage || "".equals(autoReplyMessage)) {
                operation = UpdateReply.CLEAR_ENTRY;
            }
            UpdateReply updateOpr = new UpdateReply(MSS_HOST, MAILBOX_ID, null,
                    operation, reply, RmeDataModel.Leopard);
            updateOpr.execute();
            assertEquals("Unxpected Errors: ", 0, updateOpr.getNumLogEvents());

        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }
}
