package com.opwvmsg.mxos.test.unit.webmailfeatures.addressbook;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to WebMailFeatures 
@SuiteClasses({
    GetWebMailAddressBookFeaturesTest.class,
    UpdateWebMailAddressBookFeaturesTest.class
})
public class WebMailAddressBookFeaturesSuite {
}
