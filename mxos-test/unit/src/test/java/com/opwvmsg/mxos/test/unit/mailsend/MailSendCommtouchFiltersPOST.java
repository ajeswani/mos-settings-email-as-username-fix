package com.opwvmsg.mxos.test.unit.mailsend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.CommtouchActionType;
import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendCommtouchFiltersService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailSendCommtouchFiltersPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailSendCommtouchFiltersPOST";
    private static final String ARROW_SEP = " --> ";
    private static IMailSendCommtouchFiltersService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IMailSendCommtouchFiltersService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailSendCommtouchFiltersService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static CommtouchFilters getParams() {
        CommtouchFilters filters = null;
        try {
            filters = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return filters;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CommtouchActionType.DISCARD.toString());
        updateParams(updateParams, MailboxError.MBX_NOT_FOUND.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CommtouchActionType.DISCARD.toString());
        updateParams(updateParams, MailboxError.MBX_INVALID_EMAIL.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CommtouchActionType.DISCARD.toString());
        updateParams(updateParams, MailboxError.MBX_INVALID_EMAIL.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSpamActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionNullParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionEmptyParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionInvalidParam() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testSpamActionInvalidParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MS_COMMTOUCH_FILTERS_SPAM_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSplCharsInParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MS_COMMTOUCH_FILTERS_SPAM_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CommtouchActionType.DISCARD.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.DISCARD);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess1");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CommtouchActionType.ALLOW.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.ALLOW);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess2");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CommtouchActionType.REJECT.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.REJECT);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamActionSuccess3");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CommtouchActionType.TAG.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.TAG);
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testVirusActionNullParam");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testVirusActionEmptyParam");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionInvalidParam() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testVirusActionInvalidParam");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, "sadfsadf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MS_COMMTOUCH_FILTERS_VIRUS_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testVirusActionSplCharsInParam");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_MS_COMMTOUCH_FILTERS_VIRUS_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testVirusActionSuccess");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, CommtouchActionType.DISCARD.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getVirusAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.DISCARD);
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testVirusActionSuccess1");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, CommtouchActionType.ALLOW.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getVirusAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.ALLOW);
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testVirusActionSuccess2");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, CommtouchActionType.REJECT.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getVirusAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.REJECT);
        updateParams.remove(key);
    }

    @Test
    public void testVirusActionSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testVirusActionSuccess3");
        String key = MailboxProperty.virusAction.name();
        addToParams(updateParams, key, CommtouchActionType.TAG.toString());
        updateParams(updateParams);
        CommtouchActionType value = getParams().getVirusAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, CommtouchActionType.TAG);
        updateParams.remove(key);
    }

}
