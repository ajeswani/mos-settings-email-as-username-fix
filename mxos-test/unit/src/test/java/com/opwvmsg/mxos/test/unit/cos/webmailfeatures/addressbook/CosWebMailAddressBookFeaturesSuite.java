package com.opwvmsg.mxos.test.unit.cos.webmailfeatures.addressbook;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to WebMailFeatures for Cos service. 
@SuiteClasses({
    GetCosWebMailAddressBookFeaturesTest.class,
    UpdateCosWebMailAddressBookFeaturesTest.class
})
public class CosWebMailAddressBookFeaturesSuite {
}
