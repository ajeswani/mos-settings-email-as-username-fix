package com.opwvmsg.mxos.test.unit.gp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    GeneralPreferenceAPITest.class
})
public class GeneralPrefSuite {
}
