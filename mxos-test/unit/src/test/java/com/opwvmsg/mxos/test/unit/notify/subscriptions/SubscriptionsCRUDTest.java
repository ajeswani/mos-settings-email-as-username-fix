package com.opwvmsg.mxos.test.unit.notify.subscriptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.opwvmsg.mxos.backend.service.notify.BackendSubscriptionsService;
import com.opwvmsg.mxos.error.NotifyError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.notify.ISubscriptionsService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

import static com.opwvmsg.mxos.interfaces.service.ServiceEnum.SubscriptionsService; 
import static com.opwvmsg.mxos.data.enums.MxOSConstants.MXOS_HOME;
import static com.opwvmsg.mxos.data.enums.MxOSConstants.NOTIFY_MULTIMAP_ID;
import static com.opwvmsg.mxos.data.enums.NotificationProperty.*;
import static com.opwvmsg.mxos.error.NotifyError.NTF_SUBSCRIPTION_NOT_FOUND;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

/**
 * Test for {@link ISubscriptionsService} implemented by
 * {@link BackendSubscriptionsService}
 * 
 * @author
 */
public class SubscriptionsCRUDTest {

    private static final String TEST_NAME = SubscriptionsCRUDTest.class.getSimpleName();
    private static final String ARROW_SEP = " ------------------------------> ";
    
    private static final String TOPIC = "sTopic1";
    private static final String SUBS = "http://192.168.010.111:6888/imapserv1";
    
    private static ISubscriptionsService subsService;
    private static HazelcastInstance hzInst;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        /* empty Hz if already running before executing this test */
        String hzCfgPath = System.getProperty(MXOS_HOME) + "/config/hazelcast/hazelcast-mxos.xml";
        System.setProperty("hazelcast.config", hzCfgPath);
        hzInst = Hazelcast.newHazelcastInstance();
        
        subsService = (ISubscriptionsService) ContextUtils.loadContext()
                .getService(SubscriptionsService.name());
    }
    
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).clear();
        fillData();
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        subsService = null;
    }
    
    /**
     * Positive case for storing Topic and Subs into Datastore.
     */
    @Test
    public void testCreate() {
        System.out.println(TEST_NAME + ARROW_SEP + "testCreate");
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        inputParams.put(subscription.name(), new ArrayList<String>());
        inputParams.get(subscription.name()).add(SUBS);
        
        try {
            subsService.create(inputParams);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    /**
     * Positive case for read of Topic and Subs stored into Datastore.
     */
    @Test
    public void testRead(){
        System.out.println(TEST_NAME + ARROW_SEP + "testRead");
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        
        try {
            List<String> result = subsService.read(inputParams);
            assertEquals(SUBS, result.get(0));
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    /**
     * Error case: Reading unknown Topic.
     */
    @Test
    public void testRead_1(){
        System.out.println(TEST_NAME + ARROW_SEP + "testRead_1");
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC+"_UNK");
        
        try {
            subsService.read(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(NotifyError.NTF_SUBSCRIPTION_NOT_FOUND.name(), e.getCode());
        }
    }
    
    /**
     * Update not supported UnsupportedOperationException should be received.
     */
    @Test(expected=UnsupportedOperationException.class)
    public void testUpdate(){
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdate");
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        inputParams.put(subscription.name(), new ArrayList<String>());
        inputParams.get(subscription.name()).add(SUBS);
        
        try {
            subsService.update(inputParams);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    /**
     * Positive case for Topic and Subs are deletion of given subscription from
     * Datastore.
     */
    @Test
    public void testDelete(){
        System.out.println(TEST_NAME + ARROW_SEP + "testDelete");
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        inputParams.put(subscription.name(), new ArrayList<String>());
        inputParams.get(subscription.name()).add(SUBS);

        try {
            subsService.delete(inputParams);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    /**
     * Error case: Subscription sent for deletion does not exist in Datastore.
     */
    @Test
    public void testDelete_1(){
        System.out.println(TEST_NAME + ARROW_SEP + "testDelete_1");
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        inputParams.put(subscription.name(), new ArrayList<String>());
        inputParams.get(subscription.name()).add(SUBS+"_UNK");

        try {
            subsService.delete(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(NotifyError.NTF_SUBSCRIPTION_NOT_FOUND.name(), e.getCode());
        }
    }
    
    private void fillData(){
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        inputParams.put(subscription.name(), new ArrayList<String>());
        inputParams.get(subscription.name()).add(SUBS);
        
        try {
            subsService.create(inputParams);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
}
