package com.opwvmsg.mxos.test.unit.notify.subscriptions;

import static com.opwvmsg.mxos.data.enums.MxOSConstants.*;
import static com.opwvmsg.mxos.data.enums.NotificationProperty.*;
import static com.opwvmsg.mxos.interfaces.service.ServiceEnum.NotifyService;
import static com.opwvmsg.mxos.interfaces.service.ServiceEnum.SubscriptionsService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.opwvmsg.mxos.backend.service.notify.BackendNotifyService;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.NotifyError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.notify.INotifyService;
import com.opwvmsg.mxos.interfaces.service.notify.ISubscriptionsService;
import com.opwvmsg.mxos.notify.pojos.Notify;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * Test for {@link INotifyService} implemented by
 * {@link BackendNotifyService}
 * 
 * @author
 */
public class NotifyCRUDTest {
    
    private static final String TEST_NAME = NotifyCRUDTest.class.getSimpleName();
    private static final String ARROW_SEP = " ----------> ";
    
    private static final String TOPIC = "nTopic1-UT";
    private static final String SUBS = "http://192.168.010.111:6888/imapserv_n";
    
    private static INotifyService notifyService;
    
    private static ISubscriptionsService subsService;
    
    private static List<String> inputSubs = new ArrayList<String>();
    
    private static HazelcastInstance hzInst;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        /* empty Hz if already running before executing this test */
        String hzCfgPath = System.getProperty(MXOS_HOME) + "/config/hazelcast/hazelcast-mxos.xml";
        System.setProperty("hazelcast.config", hzCfgPath);
        hzInst = Hazelcast.newHazelcastInstance();
        
        notifyService = (INotifyService) ContextUtils.loadContext().getService(
                NotifyService.name());
        
        subsService = (ISubscriptionsService) ContextUtils.loadContext()
        .getService(SubscriptionsService.name());
        
        /* start server and wait till server is running */
        SimpleHttpServer.start();
        System.setProperty(SystemProperty.notifyPublishHttpPoolMaxSize.name(), "360");
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        notifyService = null;
    }
    
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).clear();
        
        inputSubs.clear();
        inputSubs.add(SUBS+"1");
        inputSubs.add(SUBS+"2");
        inputSubs.add(SUBS+"3");
        
        for(String sub : inputSubs){
            setTestMappingToDataStore(sub);
        }
    }
    
    /**
     * Positive case for read of Topic stored into IMDB.
     */
    @Test
    public void testRead(){
        System.out.println(TEST_NAME + ARROW_SEP + "testRead");
        
        try {
            Notify result = notifyService.read(getInputParam());
            assertEquals(TOPIC, result.getTopic());
            System.out.println(inputSubs);
            System.out.println(result.getSubscriptions());
            assertEquals(inputSubs.size(), result.getSubscriptions().size());

        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    /**
     * Negative case for read, Topic not found.
     */
    @Test
    public void testRead_1(){
        System.out.println(TEST_NAME + ARROW_SEP + "testRead_1");
        
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            inputParams.put(topic.name(), new ArrayList<String>());
            inputParams.get(topic.name()).add(TOPIC+1);
            notifyService.read(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(NotifyError.NTF_TOPIC_NOT_FOUND.name(), e.getCode());
        }
    }
    
    /**
     * Positive case for deleting of a Topic stored into IMDB.
     */
    @Test
    public void testDelete(){
        System.out.println(TEST_NAME + ARROW_SEP + "testDelete");
        
        try {
            notifyService.delete(getInputParam());
            
            assertEquals(0, hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).get(TOPIC).size());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    /**
     * Error case: TOPIC sent for deletion does not exist in Datastore.
     */
    @Test
    public void testDelete_1(){
        System.out.println(TEST_NAME + ARROW_SEP + "testDelete_1");
        
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC+"NOK");

        try {
            notifyService.delete(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(NotifyError.NTF_TOPIC_NOT_FOUND.name(), e.getCode());
        }
    }
    
    @Test(expected=UnsupportedOperationException.class)
    public void testCreate(){
        System.out.println(TEST_NAME + ARROW_SEP + "testCreate");
        try {
            notifyService.create(getInputParam());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    @Test(expected=UnsupportedOperationException.class)
    public void testUpdate(){
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdate");
        try {
            notifyService.update(getInputParam());
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }

    /**
     * Positive case, Publish request for a topic.
     */
    @Test
    public void testPublish() throws Exception{
        System.out.println(TEST_NAME + ARROW_SEP + "testPublish");

        /* empty hz before staring this test */
        hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).clear();
        assertEquals(0, hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).size());
        
        try {
            String loop = System.getProperty("testPublishLoop", "1");
            int loopCtr = Integer.parseInt(loop);
            while(loopCtr-- > 0){
                setTestMappingForPublish();
                String addMsg = "{\"Type\":\"MessageAdded\",\"Message-Id\":\"37477552-7c63-11e2-b18f-55432c0edab3\",\"Imap-UID\":1002,\"Message-Flags\":\"TFFFFFF\",\"Keywords\":[\"call_received\",\"migrated\"]}";
                notifyService.publish(getPublishInputParam(addMsg));
                Thread.currentThread().sleep(1000);
            }
            /* sleep for 15 sec for application Threads to finish before verification */
            Thread.currentThread().sleep(15000);
            /* 200OK case */
            assertTrue(hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).containsEntry(TOPIC, "http://localhost:8071/200"));
            /* 404OK case */
            assertTrue(hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).containsEntry(TOPIC, "http://localhost:8071/404"));
            /* 410OK case - entry removed */
            assertFalse(hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).containsEntry(TOPIC, "http://localhost:8071/410"));
            /* TimeOut case - entry removed */
            assertFalse(hzInst.getMultiMap(NOTIFY_MULTIMAP_ID).containsEntry(TOPIC, "http://192.168.010.111:6888/imapserv"));    

        } catch (MxOSException e) {
            System.out.println(e.getCode());
            fail();
        }
    }
    
    /************ support methods **********************/
    private static Map<String, List<String>> getInputParam(){
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        return inputParams;
    }
    
    private static void setTestMappingToDataStore(String sub) throws MxOSException{
        Map<String, List<String>> inParams = new HashMap<String, List<String>>();
        inParams.put(topic.name(), new ArrayList<String>());
        inParams.get(topic.name()).add(TOPIC);
        inParams.put(subscription.name(), new ArrayList<String>());
        inParams.get(subscription.name()).add(sub);
        subsService.create(inParams);
    }
    
    private static void setTestMappingForPublish()throws MxOSException{
        String SHSPath = "http://localhost:" + SimpleHttpServer.PORT + "/";
        List<String> subs = new ArrayList<String>();
        subs.add("http://192.168.010.111:6888/imapserv");
        subs.add(SHSPath+"200");
        subs.add(SHSPath+"404");
        subs.add(SHSPath+"410");
        System.out.println(subs);
        for(String sub : subs){
            setTestMappingToDataStore(sub);
        }
    }
    
    private static Map<String, List<String>> getPublishInputParam(String msg){
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(topic.name(), new ArrayList<String>());
        inputParams.get(topic.name()).add(TOPIC);
        inputParams.put(notifyMessage.name(), new ArrayList<String>());
        inputParams.get(notifyMessage.name()).add(msg);
        return inputParams;
    }
    
}
