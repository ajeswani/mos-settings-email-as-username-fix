package com.opwvmsg.mxos.test.unit.notify.subscriptions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({SubscriptionsCRUDTest.class, NotifyCRUDTest.class})
public class NotifySubsTestSuite {

}
