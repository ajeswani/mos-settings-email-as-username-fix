package com.opwvmsg.mxos.test.unit.message.body;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IBodyService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MessageBodyGETTest {

    private static final String TEST_NAME = "MessageBodyGETTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "messageBodyTo@openwave.com";

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID = "ut_default_787";

    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "messageBodyFrom@openwave.com";

    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test";

    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static final String MESSAGEID_KEY = MessageProperty.messageId
            .name();
    private static String MESSAGEID_ID = "1062d0c4-512c-11e2-a0d7-108a47a179ce";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test message from rest context";
    private static final String MESSAGEID_VALUE_NOMSG = "1005";

    private static IMxOSContext context;
    private static IBodyService bodyService;
    private static IMailboxService mailboxService;
    private static IMessageService messageService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        createMailBox(EMAIL, PASSWORD);
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        bodyService = (IBodyService) context.getService(ServiceEnum.BodyService
                .name());
        createMessage();
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageBodyService object is null.", bodyService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox(EMAIL);
        params.clear();
        params = null;
        bodyService = null;
        mailboxService = null;
        messageService = null;
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        CosHelper.createCos(COSID);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Unable to create Mailbox...", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
            CosHelper.deleteCos(COSID);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void createMessage() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            MESSAGEID_ID = messageService.create(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testGetMessageBodySuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetMessageBodySuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_ID);
            Body body = bodyService.read(inputParams);
            assertNotNull("Message body is null.", body.getMessageBlob());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testGetMessageBodyNoEmail() {
        System.out.println("MessageBodyGETTest.testGetMessageBodyNoEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_ID);
            Body messageBody = bodyService.read(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageBodyNoMessageId() {
        System.out
                .println("MessageBodyGETTest.testGetMessageBodyNoMessageId...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Body message = bodyService.read(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageBodyNoFolder() {
        System.out.println("MessageBodyGETTest.testGetMessageBodyNoFolder...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_ID);
            Body message = bodyService.read(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageBodyNullEmail() {
        System.out.println("MessageBodyGETTest.testGetMessageBodyNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Body message = bodyService.read(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageBodyInvalidEmail() {
        System.out
                .println("MessageBodyGETTest.testGetMessageBodyInvalidEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Body message = bodyService.read(inputParams);
            fail("This should not have come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_NOT_FOUND.name(), e.getCode());
        }
    }
}
