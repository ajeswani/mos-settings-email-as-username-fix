package com.opwvmsg.mxos.test.unit.message.metadata;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    GetMessageMetaDataTest.class,
    SearchMessageMetaDataTest.class,
    UpdateMessageMetaDataTest.class,
    ListMessageMetaDataUidsTest.class,
    ListMessageMetaDataTest.class
})
public class MessageMetaDataTestSuite {
}
