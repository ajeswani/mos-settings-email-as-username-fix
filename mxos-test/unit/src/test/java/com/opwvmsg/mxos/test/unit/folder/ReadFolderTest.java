package com.opwvmsg.mxos.test.unit.folder;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ReadFolderTest {

    private static final String TEST_NAME = "ReadFolderTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "folderReadTo@openwave.com";

    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test";
    private static final String COSID = "ut_default1";

    private static final String ISADMIN_KEY = MessageProperty.isAdmin.name();
    private static final String ISADMMIN_TRUE = "true";
    private static final String ISADMMIN_FALSE = "false";

    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String TO_FOLDERNAME_KEY = FolderProperty.toFolderName
            .name();
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String TOADDRESS_KEY = SmsProperty.toAddress.name();
    private static final String FROMADDRESS = "folderReadFrom@openwave.com";
    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";

    private static final String FOLDERNAME_VALUE_INBOX = "INBOX";
    private static final String FOLDERNAME_VALUE_Test = "test";

    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static IFolderService folderService;
    private static ISendMailService sendMailService;
    private static IMessageService messageService;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        folderService = (IFolderService) context
                .getService(ServiceEnum.FolderService.name());
        sendMailService = (ISendMailService) context
                .getService(ServiceEnum.SendMailService.name());
        messageService = (IMessageService) context
        .getService(ServiceEnum.MessageService.name());
        MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(FROMADDRESS, PASSWORD, COSID);
        sendMail();
        sendMail();
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        // deleteMailBox(EMAIL);
        // deleteMailBox(FROMADDRESS);
        MailboxHelper.deleteMailbox(EMAIL, true);
        MailboxHelper.deleteMailbox(FROMADDRESS);
        CosHelper.deleteCos(COSID);
        mailboxService = null;
        sendMailService = null;
        folderService = null;
        messageService = null;
    }

    private static void sendMail() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            messageService.create(inputParams);
            //sendMailService.process(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, EMAIL_KEY, email);
        addParams(inputParams, PASSWORD_KEY, password);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Unable to create Mailbox...", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            folderService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testNullEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testInvalidEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            folderService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }

    @Test
    public void isAdminInvalid() {
        System.out.println(TEST_NAME + ARROW_SEP + "isAdminInvalid");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            addParams(inputParams, ISADMIN_KEY, "junk");
            folderService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_IS_ADMIN.name(), e.getCode());
        }
    }

    @Test
    public void testSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP + "testSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            Folder folder = folderService.read(inputParams);
            System.out.println("Ouptut : " + folder.getNumMessages());
            assertEquals("Expected Exception: ", "2", folder.getNumMessages().toString());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSuccess_isAdminTrue() {
        System.out.println(TEST_NAME + ARROW_SEP + "testSuccess_isAdminTrue");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            addParams(inputParams, ISADMIN_KEY, ISADMMIN_TRUE);
            Folder folder = folderService.read(inputParams);
            assertEquals("Expected Exception: ", "2", folder.getNumMessages().toString());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSuccess_isAdminFalse() {
        System.out.println(TEST_NAME + ARROW_SEP + "testSuccess_isAdminFalse");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INBOX);
            addParams(inputParams, ISADMIN_KEY, ISADMMIN_FALSE);
            Folder folder = folderService.read(inputParams);
            assertEquals("Expected Exception: ", "2", folder.getNumMessages().toString());
        } catch (MxOSException e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void folderNotPresent() {
        System.out.println(TEST_NAME + ARROW_SEP + "folderNotPresent");
        try {
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE_Test);
            folderService.read(inputParams2);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
            		FolderError.FLD_NOT_FOUND.name(), e.getCode());
        }
    }
}
