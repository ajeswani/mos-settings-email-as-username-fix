package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AutoReplyMode;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.DeliveryScope;
import com.opwvmsg.mxos.data.enums.MxosEnums.DisplayHeadersType;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailReceipt.AckReturnReceiptReq;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailReceiptGET {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailReceiptGET";
    private static final String ARROW_SEP = " --> ";
    private static IMailReceiptService mailReceiptService;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        mailReceiptService = (IMailReceiptService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailReceiptService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("mailReceiptService object is null.", mailReceiptService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        mailReceiptService = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static MailReceipt getParams() {
        return getParams(null);
    }

    private static MailReceipt getParams(String expectedError) {
        MailReceipt mailReceipt = null;
        try {
            mailReceipt = mailReceiptService.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                addToParams(params, EMAIL_KEY, EMAIL);
            }
        }
        return mailReceipt;
    }

    @Test
    public void testGetWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithoutEmail");
        // Clear the params.
        params.remove(EMAIL_KEY);
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithNullEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, null);
        getParams(ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithEmptyEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "");
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "something.junk@foobar.com");
        getParams(MailboxError.MBX_NOT_FOUND.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInEmail() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testGetWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "!#$%^&*()+-=.junk@foobar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "junk@$&*^-bar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testReceiveMessages() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testReceiveMessages");
        BooleanType receiveMessages = getParams().getReceiveMessages();
        assertNotNull("Is null.", receiveMessages);
    }

    @Test
    public void testForwardingEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testForwardingEnabled");
        BooleanType forwardingEnabled = getParams().getForwardingEnabled();
        assertNotNull("Is null.", forwardingEnabled);
    }

    @Test
    public void testCopyOnForward() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testCopyOnForward");
        BooleanType copyOnForward = getParams().getCopyOnForward();
        assertNotNull("Is null.", copyOnForward);
    }

    @Test
    public void testFilterHTMLContent() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testFilterHTMLContent");
        BooleanType filterHTMLContent = getParams().getFilterHTMLContent();
        assertNotNull("Is null.", filterHTMLContent);
    }

    @Test
    public void testDisplayHeaders() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testDisplayHeaders");
        DisplayHeadersType displayHeaders = getParams().getDisplayHeaders();
        assertNotNull("Is null.", displayHeaders);
    }

    @Test
    public void testWebmailDisplayWidth() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testWebmailDisplayWidth");
        String webmailDisplayWidth = getParams().getWebmailDisplayWidth().toString();
        assertNotNull("Is null.", webmailDisplayWidth);
    }

    @Test
    public void testWebmailDisplayFields() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testWebmailDisplayFields");
        String webmailDisplayFields = getParams().getWebmailDisplayFields();
        assertNotNull("Is null.", webmailDisplayFields);
    }

    @Test
    public void testMaxMailsPerPage() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxMailsPerPage");
        Integer maxMailsPerPage = getParams().getMaxMailsPerPage();
        assertNotNull("Is null.", maxMailsPerPage);
    }

    /*@Test
    public void testPreviewPaneEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testPreviewPaneEnabled");
        BooleanType previewPaneEnabled = getParams().getPreviewPaneEnabled();
        assertNotNull("Is null.", previewPaneEnabled);
    }*/

    @Test
    public void testAckReturnReceiptReq() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAckReturnReceiptReq");
        AckReturnReceiptReq ackReturnReceiptReq = getParams()
                .getAckReturnReceiptReq();
        assertNotNull("Is null.", ackReturnReceiptReq);
    }

    @Test
    public void testDeliveryScope() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testDeliveryScope");
        DeliveryScope deliveryScope = getParams().getDeliveryScope();
        assertNotNull("Is null.", deliveryScope);
    }

    @Test
    public void testAutoReplyMode() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoReplyMode");
        AutoReplyMode autoReplyMode = getParams().getAutoReplyMode();
        assertNotNull("Is null.", autoReplyMode);
    }

    @Test
    public void testAutoReplyMessage() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoReplyMessage");
        String autoReplyMessage = getParams().getAutoReplyMessage();
        assertNull("Is no null.", autoReplyMessage);
    }

    @Test
    public void testMaxReceiveMessageSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxReceiveMessageSizeKB");
        Integer maxReceiveMessageSizeKB = getParams()
                .getMaxReceiveMessageSizeKB();
        assertNotNull("Is null.", maxReceiveMessageSizeKB);
    }
    
    @Test
    public void testMobileMaxReceiveMessageSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxReceiveMessageSizeKB");
        Integer mobileMaxReceiveMessageSizeKB = getParams()
                .getMobileMaxReceiveMessageSizeKB();
        assertNotNull("Is null.", mobileMaxReceiveMessageSizeKB);
    }
    
    @Test
    public void testMaxForwardingAddresses() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxForwardingAddresses");
        Integer maxForwardingAddresses = getParams()
                .getMaxNumForwardingAddresses();
        assertNotNull("Is null.", maxForwardingAddresses);
    }

}
