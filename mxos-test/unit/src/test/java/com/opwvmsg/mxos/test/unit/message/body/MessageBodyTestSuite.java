package com.opwvmsg.mxos.test.unit.message.body;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
    MessageBodytest.class,
    MessageBodyUPATETest.class,
    MessageBodyGETTest.class
    })


public class MessageBodyTestSuite {

}
