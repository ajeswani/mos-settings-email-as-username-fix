package com.opwvmsg.mxos.test.unit.addressbook.groups;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsBaseService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GroupsBasePOST {

    private static final String TEST_NAME = "GroupsBasePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long GROUPID;

    private static ExternalSession session = null;
    private static IGroupsBaseService groupsBaseService;
    private static IExternalLoginService externalLoginService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static GroupBase getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static GroupBase getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        GroupBase base = null;
        try {
            base = groupsBaseService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("Contacts name object is null.", base);
            }
        } catch (MxOSException e) {
            assertNotNull("MxOSError is null", e);
        }
        return base;
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        groupsBaseService = (IGroupsBaseService) ContextUtils.loadContext()
                .getService(ServiceEnum.GroupsBaseService.name());
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        login(USERID, PASSWORD);
        
        GROUPID = AddressBookHelper.createGroup(USERID, session);
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        groupsBaseService = null;
        externalLoginService = null;
        AddressBookHelper.deleteGroup(USERID, GROUPID, session);
    }

    // base post
    @Test
    public void testGroupsBase() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsBase");
        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));

        params.put("groupName", new ArrayList<String>());
        params.get("groupName").add("newGroupName");
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        groupsBaseService.update(params);

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        GroupBase base = getParams(params);
        assertNotNull("GroupBase Is null.", base);

        String name = base.getGroupName();
        assertNotNull("GroupName Is null.", name);
        assertEquals("Updated GroupName value is not equal.", "newGroupName",
                name);
    }

    // base post
    @Test
    public void testNegGroupsBase() throws Exception {
        try {
            System.out.println(TEST_NAME + ARROW_SEP + "testNegGroupsBase");
            params.clear();
            params.put(AddressBookProperty.userId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.userId.name()).add(USERID);

            params.put(AddressBookProperty.groupId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.groupId.name()).add(
                    String.valueOf(GROUPID));

            params.put("groupName", new ArrayList<String>());
            params.get("groupName")
                    .add("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            if (session != null) {
                params.put(AddressBookProperty.sessionId.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.sessionId.name()).add(
                        session.getSessionId());

                params.put(AddressBookProperty.cookieString.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.cookieString.name()).add(
                        session.getCookieString());
            }
            groupsBaseService.update(params);

        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(AddressBookError.ABS_INVALID_GROUP_NAME.name(),
                    e.getCode());
        }
    }

    // base post
    @Test
    public void testEmptyGroupsBase() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testEmptyGroupsBase");
        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));

        params.put("groupName", new ArrayList<String>());
        params.get("groupName").add("");
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        groupsBaseService.update(params);

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        GroupBase base = getParams(params);
        assertNotNull("GroupBase Is null.", base);
        assertNotNull("Groupname Is null.", base.getGroupName());

    }
}
