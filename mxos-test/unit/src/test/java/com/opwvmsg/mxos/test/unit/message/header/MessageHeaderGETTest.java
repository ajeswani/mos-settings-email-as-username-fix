package com.opwvmsg.mxos.test.unit.message.header;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.HeaderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IHeaderService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MessageHeaderGETTest {

    private static final String TEST_NAME = "MessageHeaderGETTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "messageHeadetTo@openwave.com";

    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "messageHeaderFrom@openwave.com";

    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test5";
    
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID = "ut_default_786";

    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";

    private static final String MESSAGEID_KEY = MessageProperty.messageId
            .name();
    private static String MESSAGEID_ID = "1062d0c4-512c-11e2-a0d7-108a47a179ce";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Test message from rest context";

    private static final String MESSAGEID_VALUE_NOMSG = "1005";

    private static IMxOSContext context;
    private static IHeaderService headerService;
    private static IMailboxService mailboxService;
    private static IMessageService messageService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        createMailBox(EMAIL, PASSWORD);
        headerService = (IHeaderService) context
                .getService(ServiceEnum.HeaderService.name());
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        createMessage();
        //createMessage();
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", headerService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox(EMAIL);
        params.clear();
        params = null;
        headerService = null;
        mailboxService = null;
        messageService = null;
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        CosHelper.createCos(COSID);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        long mailBoxId = -1L;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Unable to create Mailbox...", mailBoxId > 0);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
            CosHelper.deleteCos(COSID);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void createMessage() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            MESSAGEID_ID = messageService.create(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

     @Test
     public void testGetMessageHeaderSuccess() {
     System.out.println(TEST_NAME + ARROW_SEP + "testGetMessageHeaderSuccess");
     Map<String, List<String>> inputParams = new HashMap<String,
     List<String>>();
     try {
     addParams(inputParams, EMAIL_KEY, EMAIL);
     addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
     addParams(inputParams, MESSAGEID_KEY, MESSAGEID_ID);
     Header header = headerService.read(inputParams);
     } catch (MxOSException e) {
         e.printStackTrace();
         assertFalse("Exception Occured...", true);
     }
     }

    @Test
    public void testGetMessageHeaderNoEmail() {
        System.out
                .println("GetMailAccessTest.testGetMessageHeaderNoEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_ID);
            Header header = headerService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageHeaderNoMessageId() {
        System.out
                .println("GetMailAccessTest.testGetMessageHeaderNoMessageId...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Header header = headerService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageHeaderNoFolder() {
        System.out
                .println("GetMailAccessTest.testGetMessageHeaderNoFolder...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_ID);
            Header header = headerService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageHeaderNullEmail() {
        System.out
                .println("GetMailAccessTest.testGetMailAccessWithNullEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, null);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Header header = headerService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.MXS_INPUT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testGetMessageHeaderInvalidEmail() {
        System.out
                .println("GetMailAccessTest.testGetMessageHeaderInvalidEmail...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "junk");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGEID_KEY, MESSAGEID_VALUE_NOMSG);
            Header header = headerService.read(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_NOT_FOUND.name(), e.getCode());
        }
    }
}
