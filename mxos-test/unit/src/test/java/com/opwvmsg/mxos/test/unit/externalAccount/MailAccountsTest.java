package com.opwvmsg.mxos.test.unit.externalAccount;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalMailAccountService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class MailAccountsTest {
    private static IExternalMailAccountService emas = null;
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

    private static final String EMAIL = "testExtAcct@openwave.com";
    private static final String PASSWORD = "Password1";
    private static final String COSID = "cos_2.0_ut200";

    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp starting......");
        emas = (IExternalMailAccountService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalMailAccountService.name());
        assertNotNull(emas);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, COSID);
        
    }

    @AfterClass
    public static void AfterClassSetUp() throws Exception {
        System.out.println("AfterClassSetUp starting......");
        String accountId[] = { "5097", "5098" };

        try {
            for (int i = 0; i < accountId.length; i++) {
                inputParams.clear();
                inputParams.put("email", new ArrayList<String>());
                inputParams.get("email").add(EMAIL);
                inputParams.put("accountId", new ArrayList<String>());
                inputParams.get("accountId").add(accountId[i]);
                emas.delete(inputParams);
            }
        } catch (Exception e) {
            assertTrue("Exception happened...", true);
            fail();
        }

        MailboxHelper.deleteMailbox(EMAIL);
        CosHelper.deleteCos(COSID);        
        emas = null;

    }

    @Test
    public void testMailAccount() {

        System.out.println("MailAccount Create starting......");
        // 1) CREATE
        testMailAccountCreate();
        System.out.println("MailAccount Create done......");

        // 2) READ
        System.out.println("MailAccount Read starting......");
        testMailAccountRead();
        System.out.println("MailAccount Read done......");

        // 3) UPDATE
        System.out.println("MailAccount Update starting......");
        testMailAccountUpdate();
        System.out.println("MailAccount Update done......");

        // 4) DELETE
        System.out.println("MailAccount Delete starting......");
        testMailAccountDeleteNonExistingAcct();
        System.out.println("MailAccount Delete done......");

    }

    public void testMailAccountCreate() {
        System.out.println("testMailAccountCreate starting......");
        // 1.Create with all parameters
        testMailAccountCreateWithAllParam();

        // 2.Create with minimum parameters
        testMailAccountCreateWithMinParam();

        // 3.Create with invalid parameters
        testMailAccountCreateWithInvalidAccountId();
        testMailAccountCreateWithInvalidServerAddress();
        testMailAccountCreateWithInvalidEmailAddress();
        testMailAccountCreateWithInvalidBoolean();
        testMailAccountCreateWithInvalidAccountType();

        // 4.Create already existing account
        testMailAccountCreateAlreadyExistingAccount();
    }

    public void testMailAccountRead() {
        System.out.println("testMailAccountRead starting......");
        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        try {
            List<MailAccount> sns = emas.read(inputParams);
            assertNotNull(sns);
            for (MailAccount ma : sns) {
                System.out.println("Mail Account: " + ma.getAccountId());
            }
        } catch (Exception e) {
            assertTrue("Exception happened...", true);
            fail();
        }
    }

    public void testMailAccountUpdate() {
        System.out.println("testMailAccountUpdate starting......");

        // 1.Update with all parameters
        testMailAccountUpdateWithAllParam();

        // 2.Update non existing account
        testMailAccountUpdateNonExistingAccount();

    }

    public void testMailAccountDeleteNonExistingAcct() {
        System.out
                .println("testMailAccountDeleteNonExistingAcct starting......");
        String accountId = "5099";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);

        try {
            emas.delete(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_MAIL_ACCOUNT_NOT_FOUND.toString());
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateWithAllParam() {
        System.out.println("testMailAccountCreateWithAllParam starting......");
        String accountId = "5097";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        inputParams.put("accountName", new ArrayList<String>());
        inputParams.get("accountName").add("test17");
        inputParams.put("accountType", new ArrayList<String>());
        inputParams.get("accountType").add("pop3");
        inputParams.put("serverAddress", new ArrayList<String>());
        inputParams.get("serverAddress").add("rwc-paclnx11.openwave.com");
        inputParams.put("serverPort", new ArrayList<String>());
        inputParams.get("serverPort").add("30110");
        inputParams.put("timeoutSecs", new ArrayList<String>());
        inputParams.get("timeoutSecs").add("60");
        inputParams.put("emailAddress", new ArrayList<String>());
        inputParams.get("emailAddress").add("test17@openwave.com");
        inputParams.put("accountUserName", new ArrayList<String>());
        inputParams.get("accountUserName").add("test17");
        inputParams.put("accountPassword", new ArrayList<String>());
        inputParams.get("accountPassword").add("password");
        inputParams.put("displayImage", new ArrayList<String>());
        inputParams.get("displayImage").add("ob_email1");
        inputParams.put("fetchMailsEnabled", new ArrayList<String>());
        inputParams.get("fetchMailsEnabled").add("yes");
        inputParams.put("autoFetchMails", new ArrayList<String>());
        inputParams.get("autoFetchMails").add("yes");
        inputParams.put("fetchedMailsFolderName", new ArrayList<String>());
        inputParams.get("fetchedMailsFolderName").add("INBOX");
        inputParams.put("leaveEmailsOnServer", new ArrayList<String>());
        inputParams.get("leaveEmailsOnServer").add("no");
        inputParams.put("fromAddressInReply", new ArrayList<String>());
        inputParams.get("fromAddressInReply").add("test17@openwave.com");
        inputParams.put("mailSendEnabled", new ArrayList<String>());
        inputParams.get("mailSendEnabled").add("yes");
        inputParams.put("mailSendSMTPAddress", new ArrayList<String>());
        inputParams.get("mailSendSMTPAddress").add("abc.openwave.com");
        inputParams.put("mailSendSMTPPort", new ArrayList<String>());
        inputParams.get("mailSendSMTPPort").add("10");
        inputParams.put("smtpSSLEnabled", new ArrayList<String>());
        inputParams.get("smtpSSLEnabled").add("yes");

        try {
            emas.create(inputParams);
            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            List<MailAccount> mailAcctList = emas.read(inputParams);
            assertNotNull(mailAcctList);
            for (MailAccount ma : mailAcctList) {
                if (ma.getAccountId().equals(Integer.valueOf(accountId)))
                    System.out.println("Mail Account: " + ma.getAccountId());
            }
        } catch (MxOSException e) {
            System.out.println("testMailAccountCreate Exception: "
                    + e.getCode());
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateWithMinParam() {
        System.out.println("testMailAccountCreateWithMinParam starting......");
        String accountId = "5098";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        try {
            emas.create(inputParams);
            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            List<MailAccount> mailAcctList = emas.read(inputParams);
            assertNotNull(mailAcctList);
            for (MailAccount ma : mailAcctList) {
                if (ma.getAccountId().equals(Integer.valueOf(accountId)))
                    System.out.println("Mail Account: " + ma.getAccountId());
            }
        } catch (MxOSException e) {
            System.out.println("testMailAccountCreate Exception: "
                    + e.getCode());
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateWithInvalidAccountId() {
        System.out
                .println("testMailAccountCreateWithInvalidAccountId starting......");
        String accountId = "5099s";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        try {
            emas.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_INVALID_ACCOUNT_ID.toString());
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateWithInvalidServerAddress() {
        System.out
                .println("testMailAccountCreateWithInvalidServerAddress starting......");
        String accountId = "5099";
        String serverAddress = "serverAddress########openwave#com#";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        inputParams.put("serverAddress", new ArrayList<String>());
        inputParams.get("serverAddress").add(serverAddress);
        try {
            emas.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_INVALID_SERVER_ADDRESS.toString());
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateWithInvalidEmailAddress() {
        System.out
                .println("testMailAccountCreateWithInvalidEmailAddress starting......");
        String accountId = "5099";
        String emailAddress = "username";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        inputParams.put("emailAddress", new ArrayList<String>());
        inputParams.get("emailAddress").add(emailAddress);
        try {
            emas.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_INVALID_EMAIL_ADDRESS.toString());
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateWithInvalidBoolean() {
        System.out
                .println("testMailAccountCreateWithInvalidBoolean starting......");
        String accountId = "5099";
        String mailSendEnabled = "invalid";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        inputParams.put("mailSendEnabled", new ArrayList<String>());
        inputParams.get("mailSendEnabled").add(mailSendEnabled);
        try {
            emas.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_INVALID_MAIL_SEND_ENABLED.toString());
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateWithInvalidAccountType() {
        System.out
                .println("testMailAccountCreateWithInvalidAccountType starting......");
        String accountId = "5099";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        inputParams.put("accountType", new ArrayList<String>());
        inputParams.get("accountType").add("invalid");

        try {
            emas.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_INVALID_ACCOUNT_TYPE.toString());
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountCreateAlreadyExistingAccount() {
        System.out
                .println("testMailAccountCreateAlreadyExistingAccount starting......");
        String accountId = "5097";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);

        try {
            emas.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_MAIL_ACCOUNT_ALREADY_EXISTS.toString());
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountUpdateWithAllParam() {
        System.out.println("testMailAccountUpdateWithAllParam starting......");
        String accountId = "5098";

        try {

            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            List<MailAccount> mailAcctList = emas.read(inputParams);
            assertNotNull(mailAcctList);

            for (MailAccount ma : mailAcctList) {
                if (ma.getAccountId() == Integer.valueOf(accountId)) {
                    System.out.println("Before Update");
                    System.out.println("Mail Account: " + ma);
                }
            }

            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            inputParams.put("accountId", new ArrayList<String>());
            inputParams.get("accountId").add(accountId);
            inputParams.put("accountName", new ArrayList<String>());
            inputParams.get("accountName").add("AccountName");
            inputParams.put("accountType", new ArrayList<String>());
            inputParams.get("accountType").add("pop3");
            inputParams.put("serverAddress", new ArrayList<String>());
            inputParams.get("serverAddress").add("rwcvmx78.openwave.com");
            inputParams.put("serverPort", new ArrayList<String>());
            inputParams.get("serverPort").add("1010");
            inputParams.put("timeoutSecs", new ArrayList<String>());
            inputParams.get("timeoutSecs").add("10");
            inputParams.put("emailAddress", new ArrayList<String>());
            inputParams.get("emailAddress").add("xyz@openwave.com");
            inputParams.put("accountUserName", new ArrayList<String>());
            inputParams.get("accountUserName").add("username@openwave.com");
            inputParams.put("accountPassword", new ArrayList<String>());
            inputParams.get("accountPassword").add("password");
            inputParams.put("displayImage", new ArrayList<String>());
            inputParams.get("displayImage").add("displayimage");
            inputParams.put("fetchMailsEnabled", new ArrayList<String>());
            inputParams.get("fetchMailsEnabled").add("yes");
            inputParams.put("autoFetchMails", new ArrayList<String>());
            inputParams.get("autoFetchMails").add("yes");
            inputParams.put("fetchedMailsFolderName", new ArrayList<String>());
            inputParams.get("fetchedMailsFolderName").add("FolderName");
            inputParams.put("leaveEmailsOnServer", new ArrayList<String>());
            inputParams.get("leaveEmailsOnServer").add("no");
            inputParams.put("fromAddressInReply", new ArrayList<String>());
            inputParams.get("fromAddressInReply").add(
                    "customerCare@openwave.com");
            inputParams.put("mailSendEnabled", new ArrayList<String>());
            inputParams.get("mailSendEnabled").add("yes");
            inputParams.put("mailSendSMTPAddress", new ArrayList<String>());
            inputParams.get("mailSendSMTPAddress").add("abc.openwave.com");
            inputParams.put("mailSendSMTPPort", new ArrayList<String>());
            inputParams.get("mailSendSMTPPort").add("9090");
            inputParams.put("smtpSSLEnabled", new ArrayList<String>());
            inputParams.get("smtpSSLEnabled").add("yes");
            emas.update(inputParams);

            inputParams.clear();
            inputParams.put("email", new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            mailAcctList = emas.read(inputParams);
            assertNotNull(mailAcctList);
            for (MailAccount ma : mailAcctList) {
                if (ma.getAccountId().equals(accountId)) {
                    System.out.println("After Update");
                    System.out.println("Mail Account: " + ma);
                }
            }
        } catch (MxOSException e) {
            System.out.println("testMailAccountUpdateWithAllParam Exception: "
                    + e.getCode());
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    public static void testMailAccountUpdateNonExistingAccount() {
        System.out
                .println("testMailAccountUpdateNonExistingAccount starting......");
        String accountId = "5099";

        inputParams.clear();
        inputParams.put("email", new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("accountId", new ArrayList<String>());
        inputParams.get("accountId").add(accountId);
        inputParams.put("accountName", new ArrayList<String>());
        inputParams.get("accountName").add("accountName");

        try {
            emas.update(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),
                    MailboxError.MBX_MAIL_ACCOUNT_NOT_FOUND.toString());
        } catch (Exception e) {
            fail();
        }
    }

}
