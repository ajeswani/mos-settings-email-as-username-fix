package com.opwvmsg.mxos.test.unit.message;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
    MessageInFolderDELETE.class,
    MessageInFolderCOPY.class,
    MessageInFolderMOVE.class,
    AllMessagesMetadataInFolderGET.class,
    MessageServiceGET.class,
    MessageCreateTest.class
})
public class MessageTestSuite {
}
