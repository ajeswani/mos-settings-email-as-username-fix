package com.opwvmsg.mxos.test.unit.webmailfeatures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetWebMailFeaturesTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IWebMailFeaturesService wmfs;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetWebMailFeaturesTest.setUpBeforeClass...");
        wmfs = (IWebMailFeaturesService) ContextUtils.loadContext().getService(
                ServiceEnum.WebMailFeaturesService.name());
        long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetWebMailFeaturesTest.setUp...");
        assertNotNull("WebMailFeaturesService object is null.", wmfs);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetWebMailFeaturesTest.tearDown...");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetWebMailFeaturesTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        wmfs = null;
    }

    private static WebMailFeatures getWebMailFeaturesParams(
            Map<String, List<String>> params) {
        return getWebMailFeaturesParams(params, null);
    }

    private static WebMailFeatures getWebMailFeaturesParams(
            Map<String, List<String>> params, String expectedError) {
        WebMailFeatures wmf = null;
        try {
            wmf = wmfs.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("WebMailFeatures object is null.", wmf);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
            }
        }
        return wmf;
    }

    @Test
    public void testGetWebMailFeaturesParamsWithoutEmail() {
        System.out
                .println("GetWebMailFeaturesTest.testGetWebMailFeaturesParamsWithoutEmail...");
        // Clear the params.
        params.clear();
        getWebMailFeaturesParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetWebMailFeaturesWithNullEmail() {
        System.out
                .println("GetWebMailFeaturesTest.testGetWebMailFeaturesWithNullEmail...");
        // Remove the email from the map to have a null email.
        params.get(EMAIL_KEY).clear();
        getWebMailFeaturesParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetWebMailFeaturesWithNonExistingEmail() {
        System.out
                .println("GetWebMailFeaturesTest.testGetWebMailFeaturesWithNonExistingEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("something.junk@foobar.com");
        getWebMailFeaturesParams(params, MailboxError.MBX_NOT_FOUND.name());
    }

    @Test
    public void testGetWebMailFeaturesWithSplCharsInEmail() {
        System.out
                .println("GetWebMailFeaturesTest.testGetWebMailFeaturesWithSplCharsInEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("!#$%^&*()+-=.junk@foobar.com");
        getWebMailFeaturesParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testGetWebMailFeaturesWithSplCharsInDomain() {
        System.out
                .println("GetWebMailFeaturesTest.testGetWebMailFeaturesWithSplCharsInDomain...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("junk@foo!#+()$&*^-bar.com");
        getWebMailFeaturesParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testBusinessFeaturesEnabledSuccess() throws Exception {
        System.out.println("GetWebMailFeaturesTest.testBusinessFeaturesEnabledSuccess...");
        BooleanType pa = getWebMailFeaturesParams(params).getBusinessFeaturesEnabled();
        assertNotNull("BusinessFeaturesEnabled is null.", pa);
        assertTrue("BusinessFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testFullFeaturesEnabledSuccess() throws Exception {
        System.out.println("GetWebMailFeaturesTest.testFullFeaturesEnabledSuccess...");
        BooleanType pa = getWebMailFeaturesParams(params).getFullFeaturesEnabled();
        assertNotNull("FullFeaturesEnabled is null.", pa);
        assertTrue("FullFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testLastFullFeaturesConnectionDateSuccess() throws Exception {
        System.out.println("GetWebMailFeaturesTest.testBusinessFeaturesEnabledSuccess...");
        Object date = getWebMailFeaturesParams(params).getLastFullFeaturesConnectionDate();
        assertNull("LastFullFeaturesConnectionDate is not null.", date);
    }

}
