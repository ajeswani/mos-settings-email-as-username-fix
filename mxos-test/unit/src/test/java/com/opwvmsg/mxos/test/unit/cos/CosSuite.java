package com.opwvmsg.mxos.test.unit.cos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to COS
@SuiteClasses({
        CosCreateTest.class,
        CosDeleteTest.class,
        CosBaseTest.class,
        CosExternalAccountTest.class
    })
public class CosSuite {
}
