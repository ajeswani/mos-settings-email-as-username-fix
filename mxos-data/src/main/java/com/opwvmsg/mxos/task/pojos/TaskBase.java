
package com.opwvmsg.mxos.task.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Task base object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "taskId",
    "folderId",
    "name",
    "owner",
    "priority",
    "status",
    "isPrivate",
    "colorLabel",
    "categories",
    "notes",
    "createdDate",
    "updatedDate",
    "startDate",
    "dueDate",
    "completedDate",
    "reminderDate",
    "progress"
})
public class TaskBase implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("taskId")
    private java.lang.String taskId;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    private java.lang.String folderId;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("name")
    private java.lang.String name;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("owner")
    private java.lang.String owner;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("priority")
    private TaskBase.Priority priority;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    private TaskBase.Status status;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isPrivate")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType isPrivate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("colorLabel")
    private java.lang.Integer colorLabel;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("categories")
    private java.lang.String categories;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("notes")
    private java.lang.String notes;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("createdDate")
    private java.lang.String createdDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("updatedDate")
    private java.lang.String updatedDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("startDate")
    private java.lang.String startDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("dueDate")
    private java.lang.String dueDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("completedDate")
    private java.lang.String completedDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("reminderDate")
    private java.lang.String reminderDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("progress")
    private java.lang.Integer progress;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("taskId")
    public java.lang.String getTaskId() {
        return taskId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("taskId")
    public void setTaskId(java.lang.String taskId) {
        this.taskId = taskId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public java.lang.String getFolderId() {
        return folderId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public void setFolderId(java.lang.String folderId) {
        this.folderId = folderId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("name")
    public java.lang.String getName() {
        return name;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("name")
    public void setName(java.lang.String name) {
        this.name = name;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("owner")
    public java.lang.String getOwner() {
        return owner;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("owner")
    public void setOwner(java.lang.String owner) {
        this.owner = owner;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("priority")
    public TaskBase.Priority getPriority() {
        return priority;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("priority")
    public void setPriority(TaskBase.Priority priority) {
        this.priority = priority;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    public TaskBase.Status getStatus() {
        return status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    public void setStatus(TaskBase.Status status) {
        this.status = status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isPrivate")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getIsPrivate() {
        return isPrivate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isPrivate")
    public void setIsPrivate(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType isPrivate) {
        this.isPrivate = isPrivate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("colorLabel")
    public java.lang.Integer getColorLabel() {
        return colorLabel;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("colorLabel")
    public void setColorLabel(java.lang.Integer colorLabel) {
        this.colorLabel = colorLabel;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("categories")
    public java.lang.String getCategories() {
        return categories;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("categories")
    public void setCategories(java.lang.String categories) {
        this.categories = categories;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("notes")
    public java.lang.String getNotes() {
        return notes;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("notes")
    public void setNotes(java.lang.String notes) {
        this.notes = notes;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("createdDate")
    public java.lang.String getCreatedDate() {
        return createdDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("createdDate")
    public void setCreatedDate(java.lang.String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("updatedDate")
    public java.lang.String getUpdatedDate() {
        return updatedDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("updatedDate")
    public void setUpdatedDate(java.lang.String updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("startDate")
    public java.lang.String getStartDate() {
        return startDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("startDate")
    public void setStartDate(java.lang.String startDate) {
        this.startDate = startDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("dueDate")
    public java.lang.String getDueDate() {
        return dueDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("dueDate")
    public void setDueDate(java.lang.String dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("completedDate")
    public java.lang.String getCompletedDate() {
        return completedDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("completedDate")
    public void setCompletedDate(java.lang.String completedDate) {
        this.completedDate = completedDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("reminderDate")
    public java.lang.String getReminderDate() {
        return reminderDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("reminderDate")
    public void setReminderDate(java.lang.String reminderDate) {
        this.reminderDate = reminderDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("progress")
    public java.lang.Integer getProgress() {
        return progress;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("progress")
    public void setProgress(java.lang.Integer progress) {
        this.progress = progress;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum Priority {

        LOW("Low"),
        MEDIUM("Medium"),
        HIGH("High");
        private final java.lang.String value;

        private Priority(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static TaskBase.Priority fromValue(java.lang.String value) {
            for (TaskBase.Priority c: TaskBase.Priority.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static TaskBase.Priority fromOrdinal(java.lang.Integer value) {
            for (TaskBase.Priority c: TaskBase.Priority.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum Status {

        NOT_STARTED("Not started"),
        IN_PROGRESS("In progress"),
        DONE("Done"),
        WAITING("Waiting"),
        DEFERRED("Deferred");
        private final java.lang.String value;

        private Status(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static TaskBase.Status fromValue(java.lang.String value) {
            for (TaskBase.Status c: TaskBase.Status.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static TaskBase.Status fromOrdinal(java.lang.Integer value) {
            for (TaskBase.Status c: TaskBase.Status.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

}
