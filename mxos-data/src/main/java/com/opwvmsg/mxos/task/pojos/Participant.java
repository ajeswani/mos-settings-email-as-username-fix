
package com.opwvmsg.mxos.task.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Participant object of Task
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "participantId",
    "participantType",
    "participantName",
    "participantEmail",
    "participantStatus",
    "participantMessage",
    "participantHref"
})
public class Participant implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantId")
    private java.lang.String participantId;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantType")
    private Participant.ParticipantType participantType;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantName")
    private java.lang.String participantName;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantEmail")
    private java.lang.String participantEmail;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantStatus")
    private Participant.ParticipantStatus participantStatus;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantMessage")
    private java.lang.String participantMessage;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantHref")
    private java.lang.String participantHref;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantId")
    public java.lang.String getParticipantId() {
        return participantId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantId")
    public void setParticipantId(java.lang.String participantId) {
        this.participantId = participantId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantType")
    public Participant.ParticipantType getParticipantType() {
        return participantType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantType")
    public void setParticipantType(Participant.ParticipantType participantType) {
        this.participantType = participantType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantName")
    public java.lang.String getParticipantName() {
        return participantName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantName")
    public void setParticipantName(java.lang.String participantName) {
        this.participantName = participantName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantEmail")
    public java.lang.String getParticipantEmail() {
        return participantEmail;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantEmail")
    public void setParticipantEmail(java.lang.String participantEmail) {
        this.participantEmail = participantEmail;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantStatus")
    public Participant.ParticipantStatus getParticipantStatus() {
        return participantStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantStatus")
    public void setParticipantStatus(Participant.ParticipantStatus participantStatus) {
        this.participantStatus = participantStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantMessage")
    public java.lang.String getParticipantMessage() {
        return participantMessage;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantMessage")
    public void setParticipantMessage(java.lang.String participantMessage) {
        this.participantMessage = participantMessage;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantHref")
    public java.lang.String getParticipantHref() {
        return participantHref;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("participantHref")
    public void setParticipantHref(java.lang.String participantHref) {
        this.participantHref = participantHref;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum ParticipantStatus {

        NONE("none"),
        ACCEPTED("accepted"),
        DECLINE("decline"),
        TENTATIVE("tentative");
        private final java.lang.String value;

        private ParticipantStatus(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static Participant.ParticipantStatus fromValue(java.lang.String value) {
            for (Participant.ParticipantStatus c: Participant.ParticipantStatus.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static Participant.ParticipantStatus fromOrdinal(Integer value) {
            for (Participant.ParticipantStatus c: Participant.ParticipantStatus.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum ParticipantType {

        USER("user"),
        USER_GROUP("userGroup"),
        RESOURCE("resource"),
        RESOURCE_GROUP("resourceGroup"),
        EXTERNAL_USER("externalUser");
        private final java.lang.String value;

        private ParticipantType(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static Participant.ParticipantType fromValue(java.lang.String value) {
            for (Participant.ParticipantType c: Participant.ParticipantType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static Participant.ParticipantType fromOrdinal(Integer value) {
            for (Participant.ParticipantType c: Participant.ParticipantType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

}
