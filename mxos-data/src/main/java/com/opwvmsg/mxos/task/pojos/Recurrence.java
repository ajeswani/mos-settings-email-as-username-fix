
package com.opwvmsg.mxos.task.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Recurrence object of Task
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "type",
    "daysInWeek",
    "daysInMonth",
    "monthInYear",
    "recurAfterInterval",
    "endDate",
    "notification",
    "occurrences"
})
public class Recurrence implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    private Recurrence.Type type;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("daysInWeek")
    private Recurrence.DaysInWeek daysInWeek;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("daysInMonth")
    private java.lang.Integer daysInMonth;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("monthInYear")
    private java.lang.Integer monthInYear;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("recurAfterInterval")
    private java.lang.Integer recurAfterInterval;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("endDate")
    private java.lang.String endDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("notification")
    private java.lang.String notification;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("occurrences")
    private java.lang.Integer occurrences;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public Recurrence.Type getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public void setType(Recurrence.Type type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("daysInWeek")
    public Recurrence.DaysInWeek getDaysInWeek() {
        return daysInWeek;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("daysInWeek")
    public void setDaysInWeek(Recurrence.DaysInWeek daysInWeek) {
        this.daysInWeek = daysInWeek;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("daysInMonth")
    public java.lang.Integer getDaysInMonth() {
        return daysInMonth;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("daysInMonth")
    public void setDaysInMonth(java.lang.Integer daysInMonth) {
        this.daysInMonth = daysInMonth;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("monthInYear")
    public java.lang.Integer getMonthInYear() {
        return monthInYear;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("monthInYear")
    public void setMonthInYear(java.lang.Integer monthInYear) {
        this.monthInYear = monthInYear;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("recurAfterInterval")
    public java.lang.Integer getRecurAfterInterval() {
        return recurAfterInterval;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("recurAfterInterval")
    public void setRecurAfterInterval(java.lang.Integer recurAfterInterval) {
        this.recurAfterInterval = recurAfterInterval;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("endDate")
    public java.lang.String getEndDate() {
        return endDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("endDate")
    public void setEndDate(java.lang.String endDate) {
        this.endDate = endDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("notification")
    public java.lang.String getNotification() {
        return notification;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("notification")
    public void setNotification(java.lang.String notification) {
        this.notification = notification;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("occurrences")
    public java.lang.Integer getOccurrences() {
        return occurrences;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("occurrences")
    public void setOccurrences(java.lang.Integer occurrences) {
        this.occurrences = occurrences;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum DaysInWeek {

        MONDAY("Monday"),
        TUESDAY("Tuesday"),
        WEDNESDAY("Wednesday"),
        THURSDAY("Thursday"),
        FRIDAY("Friday"),
        SATURDAY("Saturday"),
        SUNDAY("Sunday");
        private final java.lang.String value;

        private DaysInWeek(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static Recurrence.DaysInWeek fromValue(java.lang.String value) {
            for (Recurrence.DaysInWeek c: Recurrence.DaysInWeek.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static Recurrence.DaysInWeek fromOrdinal(java.lang.Integer value) {
            for (Recurrence.DaysInWeek c: Recurrence.DaysInWeek.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum Type {

        NONE("None"),
        DAILY("Daily"),
        MONTHLY("Monthly"),
        YEARLY("Yearly");
        private final java.lang.String value;

        private Type(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static Recurrence.Type fromValue(java.lang.String value) {
            for (Recurrence.Type c: Recurrence.Type.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static Recurrence.Type fromOrdinal(java.lang.Integer value) {
            for (Recurrence.Type c: Recurrence.Type.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

}
