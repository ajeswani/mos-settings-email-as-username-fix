
package com.opwvmsg.mxos.task.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Attachment object of Task
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "attachmentName",
    "attachmentSize",
    "attachmentBody"
})
public class Attachment implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentName")
    private java.lang.String attachmentName;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentSize")
    private Long attachmentSize;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentBody")
    private java.lang.String attachmentBody;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentName")
    public java.lang.String getAttachmentName() {
        return attachmentName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentName")
    public void setAttachmentName(java.lang.String attachmentName) {
        this.attachmentName = attachmentName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentSize")
    public Long getAttachmentSize() {
        return attachmentSize;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentSize")
    public void setAttachmentSize(Long attachmentSize) {
        this.attachmentSize = attachmentSize;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentBody")
    public java.lang.String getAttachmentBody() {
        return attachmentBody;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("attachmentBody")
    public void setAttachmentBody(java.lang.String attachmentBody) {
        this.attachmentBody = attachmentBody;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
