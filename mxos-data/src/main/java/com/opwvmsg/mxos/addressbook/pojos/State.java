
package com.opwvmsg.mxos.addressbook.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * State for OX login
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "session",
    "cookie1",
    "cookie2",
    "cookie3"
})
public class State implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("session")
    private String session;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie1")
    private String cookie1;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie2")
    private String cookie2;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie3")
    private String cookie3;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("session")
    public String getSession() {
        return session;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("session")
    public void setSession(String session) {
        this.session = session;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie1")
    public String getCookie1() {
        return cookie1;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie1")
    public void setCookie1(String cookie1) {
        this.cookie1 = cookie1;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie2")
    public String getCookie2() {
        return cookie2;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie2")
    public void setCookie2(String cookie2) {
        this.cookie2 = cookie2;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie3")
    public String getCookie3() {
        return cookie3;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cookie3")
    public void setCookie3(String cookie3) {
        this.cookie3 = cookie3;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
