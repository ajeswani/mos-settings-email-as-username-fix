
package com.opwvmsg.mxos.addressbook.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Address book object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "addressBookBase",
    "contacts",
    "groups",
    "subscriptions",
    "notifications"
})
public class AddressBook implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("addressBookBase")
    private AddressBookBase addressBookBase;
    /**
     * contacts
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("contacts")
    private Set<Contact> contacts = new HashSet<Contact>();
    /**
     * groups
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("groups")
    private Set<Group> groups = new HashSet<Group>();
    /**
     * subscriptions
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptions")
    private Set<Subscription> subscriptions = new HashSet<Subscription>();
    /**
     * notifications
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("notifications")
    private Set<Notification> notifications = new HashSet<Notification>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("addressBookBase")
    public AddressBookBase getAddressBookBase() {
        return addressBookBase;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("addressBookBase")
    public void setAddressBookBase(AddressBookBase addressBookBase) {
        this.addressBookBase = addressBookBase;
    }

    /**
     * contacts
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("contacts")
    public Set<Contact> getContacts() {
        return contacts;
    }

    /**
     * contacts
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("contacts")
    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    /**
     * groups
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("groups")
    public Set<Group> getGroups() {
        return groups;
    }

    /**
     * groups
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("groups")
    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    /**
     * subscriptions
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptions")
    public Set<Subscription> getSubscriptions() {
        return subscriptions;
    }

    /**
     * subscriptions
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptions")
    public void setSubscriptions(Set<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    /**
     * notifications
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("notifications")
    public Set<Notification> getNotifications() {
        return notifications;
    }

    /**
     * notifications
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("notifications")
    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
