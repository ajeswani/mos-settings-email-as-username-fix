/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;
/**
 * All possible query list supported by MxOS.
 *
 * @see <i>mxos Reference Guide</i>
 *
 * @author mxos-dev
 */

/**
 * All possible backend protocols for CRUD interface.
 * NOTE: We also have RME protocol type Enums in utils.paf.intermail.rme.AbstractOperation.
 *       But those are not duplicate.  CRUD protocol can be non-RME.  Protocols in PAF
 *       is limited within RME.
 */
public enum CRUDProtocol {
    /**
     * <tt>unknown</tt>: Unknown
     *
     * @since mxos 2.0
     */
    unknown,

    /**
     * <tt>rme</tt>: Regular RME
     *
     * @since mxos 2.0
     */
    rme,

    /**
     * <tt>slrme</tt>: Stateless RME over HTTP
     *
     * @since mxos 2.0
     */
    httprme,
}
