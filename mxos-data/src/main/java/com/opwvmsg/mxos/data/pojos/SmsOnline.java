
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * SMS Online object of smsServices
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "smsOnlineEnabled",
    "internationalSMSAllowed",
    "internationalSMSEnabled",
    "maxSMSPerDay",
    "concatenatedSMSAllowed",
    "concatenatedSMSEnabled",
    "maxConcatenatedSMSSegments",
    "maxPerCaptchaSMS",
    "maxPerCaptchaDurationMins"
})
public class SmsOnline implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("smsOnlineEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsOnlineEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("internationalSMSAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType internationalSMSAllowed;
    @org.codehaus.jackson.annotate.JsonProperty("internationalSMSEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType internationalSMSEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("maxSMSPerDay")
    private Integer maxSMSPerDay;
    @org.codehaus.jackson.annotate.JsonProperty("concatenatedSMSAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType concatenatedSMSAllowed;
    @org.codehaus.jackson.annotate.JsonProperty("concatenatedSMSEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType concatenatedSMSEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("maxConcatenatedSMSSegments")
    private Integer maxConcatenatedSMSSegments;
    @org.codehaus.jackson.annotate.JsonProperty("maxPerCaptchaSMS")
    private Integer maxPerCaptchaSMS;
    @org.codehaus.jackson.annotate.JsonProperty("maxPerCaptchaDurationMins")
    private Integer maxPerCaptchaDurationMins;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("smsOnlineEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmsOnlineEnabled() {
        return smsOnlineEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smsOnlineEnabled")
    public void setSmsOnlineEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsOnlineEnabled) {
        this.smsOnlineEnabled = smsOnlineEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("internationalSMSAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getInternationalSMSAllowed() {
        return internationalSMSAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("internationalSMSAllowed")
    public void setInternationalSMSAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType internationalSMSAllowed) {
        this.internationalSMSAllowed = internationalSMSAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("internationalSMSEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getInternationalSMSEnabled() {
        return internationalSMSEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("internationalSMSEnabled")
    public void setInternationalSMSEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType internationalSMSEnabled) {
        this.internationalSMSEnabled = internationalSMSEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxSMSPerDay")
    public Integer getMaxSMSPerDay() {
        return maxSMSPerDay;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxSMSPerDay")
    public void setMaxSMSPerDay(Integer maxSMSPerDay) {
        this.maxSMSPerDay = maxSMSPerDay;
    }

    @org.codehaus.jackson.annotate.JsonProperty("concatenatedSMSAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getConcatenatedSMSAllowed() {
        return concatenatedSMSAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("concatenatedSMSAllowed")
    public void setConcatenatedSMSAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType concatenatedSMSAllowed) {
        this.concatenatedSMSAllowed = concatenatedSMSAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("concatenatedSMSEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getConcatenatedSMSEnabled() {
        return concatenatedSMSEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("concatenatedSMSEnabled")
    public void setConcatenatedSMSEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType concatenatedSMSEnabled) {
        this.concatenatedSMSEnabled = concatenatedSMSEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxConcatenatedSMSSegments")
    public Integer getMaxConcatenatedSMSSegments() {
        return maxConcatenatedSMSSegments;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxConcatenatedSMSSegments")
    public void setMaxConcatenatedSMSSegments(Integer maxConcatenatedSMSSegments) {
        this.maxConcatenatedSMSSegments = maxConcatenatedSMSSegments;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxPerCaptchaSMS")
    public Integer getMaxPerCaptchaSMS() {
        return maxPerCaptchaSMS;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxPerCaptchaSMS")
    public void setMaxPerCaptchaSMS(Integer maxPerCaptchaSMS) {
        this.maxPerCaptchaSMS = maxPerCaptchaSMS;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxPerCaptchaDurationMins")
    public Integer getMaxPerCaptchaDurationMins() {
        return maxPerCaptchaDurationMins;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxPerCaptchaDurationMins")
    public void setMaxPerCaptchaDurationMins(Integer maxPerCaptchaDurationMins) {
        this.maxPerCaptchaDurationMins = maxPerCaptchaDurationMins;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
