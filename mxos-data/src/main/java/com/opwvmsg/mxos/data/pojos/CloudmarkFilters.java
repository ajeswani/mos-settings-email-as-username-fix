
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * cloudmark filters object of mailReceipt filters
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "spamfilterEnabled",
    "spamPolicy",
    "spamAction",
    "cleanAction",
    "suspectAction"
})
public class CloudmarkFilters implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamfilterEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType spamfilterEnabled;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamPolicy")
    private com.opwvmsg.mxos.data.enums.MxosEnums.SpamPolicy spamPolicy;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamAction")
    private String spamAction;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cleanAction")
    private String cleanAction;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("suspectAction")
    private String suspectAction;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamfilterEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSpamfilterEnabled() {
        return spamfilterEnabled;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamfilterEnabled")
    public void setSpamfilterEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType spamfilterEnabled) {
        this.spamfilterEnabled = spamfilterEnabled;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamPolicy")
    public com.opwvmsg.mxos.data.enums.MxosEnums.SpamPolicy getSpamPolicy() {
        return spamPolicy;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamPolicy")
    public void setSpamPolicy(com.opwvmsg.mxos.data.enums.MxosEnums.SpamPolicy spamPolicy) {
        this.spamPolicy = spamPolicy;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamAction")
    public String getSpamAction() {
        return spamAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spamAction")
    public void setSpamAction(String spamAction) {
        this.spamAction = spamAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cleanAction")
    public String getCleanAction() {
        return cleanAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cleanAction")
    public void setCleanAction(String cleanAction) {
        this.cleanAction = cleanAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("suspectAction")
    public String getSuspectAction() {
        return suspectAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("suspectAction")
    public void setSuspectAction(String suspectAction) {
        this.suspectAction = suspectAction;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
