
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Sender Blocking object of mailReceipt filters
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "senderBlockingAllowed",
    "senderBlockingEnabled",
    "rejectAction",
    "rejectFolder",
    "allowedSendersList",
    "blockedSendersList",
    "blockSendersPABActive",
    "blockSendersPABAccess"
})
public class SenderBlocking implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("senderBlockingAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType senderBlockingAllowed;
    @org.codehaus.jackson.annotate.JsonProperty("senderBlockingEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType senderBlockingEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("rejectAction")
    private com.opwvmsg.mxos.data.enums.MxosEnums.RejectActionType rejectAction;
    @org.codehaus.jackson.annotate.JsonProperty("rejectFolder")
    private String rejectFolder;
    @org.codehaus.jackson.annotate.JsonProperty("allowedSendersList")
    private List<String> allowedSendersList;
    @org.codehaus.jackson.annotate.JsonProperty("blockedSendersList")
    private List<String> blockedSendersList;
    @org.codehaus.jackson.annotate.JsonProperty("blockSendersPABActive")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType blockSendersPABActive;
    @org.codehaus.jackson.annotate.JsonProperty("blockSendersPABAccess")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType blockSendersPABAccess;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("senderBlockingAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSenderBlockingAllowed() {
        return senderBlockingAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("senderBlockingAllowed")
    public void setSenderBlockingAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType senderBlockingAllowed) {
        this.senderBlockingAllowed = senderBlockingAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("senderBlockingEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSenderBlockingEnabled() {
        return senderBlockingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("senderBlockingEnabled")
    public void setSenderBlockingEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType senderBlockingEnabled) {
        this.senderBlockingEnabled = senderBlockingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rejectAction")
    public com.opwvmsg.mxos.data.enums.MxosEnums.RejectActionType getRejectAction() {
        return rejectAction;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rejectAction")
    public void setRejectAction(com.opwvmsg.mxos.data.enums.MxosEnums.RejectActionType rejectAction) {
        this.rejectAction = rejectAction;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rejectFolder")
    public String getRejectFolder() {
        return rejectFolder;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rejectFolder")
    public void setRejectFolder(String rejectFolder) {
        this.rejectFolder = rejectFolder;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowedSendersList")
    public List<String> getAllowedSendersList() {
        return allowedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowedSendersList")
    public void setAllowedSendersList(List<String> allowedSendersList) {
        this.allowedSendersList = allowedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSendersList")
    public List<String> getBlockedSendersList() {
        return blockedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSendersList")
    public void setBlockedSendersList(List<String> blockedSendersList) {
        this.blockedSendersList = blockedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockSendersPABActive")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getBlockSendersPABActive() {
        return blockSendersPABActive;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockSendersPABActive")
    public void setBlockSendersPABActive(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType blockSendersPABActive) {
        this.blockSendersPABActive = blockSendersPABActive;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockSendersPABAccess")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getBlockSendersPABAccess() {
        return blockSendersPABAccess;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockSendersPABAccess")
    public void setBlockSendersPABAccess(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType blockSendersPABAccess) {
        this.blockSendersPABAccess = blockSendersPABAccess;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
