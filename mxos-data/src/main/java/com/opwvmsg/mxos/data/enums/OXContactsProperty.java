/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All Contacts attributes referenced in OX by this application.
 * 
 * @author mxos-dev
 */
public enum OXContactsProperty implements DataMap.Property {

    // common
    timezone, timestamp, data, folder, folder_id, login_info,

    // error
    error, category, code, error_id, error_params,

    // login
    name, password, session, id, client,

    // base
    creation_date, last_modified, private_flag, color_label, categories, note,

    // name
    first_name, second_name, last_name, display_name, suffix, nickname, title,
    yomiFirstName, yomiLastName, yomiCompany, email3, userfield19, uid, telephone_other,
    // image
    image1, image1_url, json, file,

    // personalInfo
    marital_status, spouse_name,

    // events
    birthday, anniversary,

    // personalInfoAddress
    street_home, city_home, state_home, postal_code_home, country_home,

    // personalInfoCommunication
    telephone_home1, telephone_home2, telephone_ip, cellular_telephone2, fax_home, email2, instant_messenger2, telephone_pager,

    // workInfo
    company, department, position, manager_name, assistant_name, telephone_assistant, employee_type, url,

    // workInfoAddress
    street_business, city_business, state_business, postal_code_business, country_business,

    // workInfoCommunication
    telephone_business1, telephone_business2, cellular_telephone1, fax_business, email1, instant_messenger1,

    columns, 

    // groups
    distribution_list, number_of_distribution_list, mark_as_distributionlist, memberId, mail_field, mail, sort, order, memberFolderId,  
}
