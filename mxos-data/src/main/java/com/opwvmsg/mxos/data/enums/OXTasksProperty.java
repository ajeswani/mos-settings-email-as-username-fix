/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All Tasks attributes referenced in OX by this application.
 * 
 * @author mxos-dev
 */
public enum OXTasksProperty implements DataMap.Property {

    // common
    timezone, timestamp, data, folder_id, login_info, columns, 

    // error
    error, category, code, error_id, error_params,

    // login
    name, password, session, client,

    // base
    id, folder, title, organizer, priority, status, private_flag, color_label, categories,
    note, creation_date, last_modified, start_date, end_date, date_completed,
    alarm, percent_completed,
    
    // recurrence
    recurrence_type, days, day_in_month, month, interval, until, notification, occurrences,

    // participant
    type, display_name, mail, confirmation, confirm_message,
    
    // details
    actual_costs, actual_duration, billing_information, target_costs,
    target_duration, currency, trip_meter, companies,
    
    // attachments

    // Other
    sort,
    order;
}
