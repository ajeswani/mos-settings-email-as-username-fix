
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * COS object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "base",
    "webMailFeatures",
    "credentials",
    "generalPreferences",
    "mailSend",
    "mailReceipt",
    "mailAccess",
    "mailStore",
    "socialNetworks",
    "externalAccounts",
    "smsServices",
    "internalInfo"
})
public class Cos implements Serializable
{

    /**
     * Base object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("base")
    private Base base;
    /**
     * WebMailFeatures object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("webMailFeatures")
    private WebMailFeatures webMailFeatures;
    /**
     * Credentials object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("credentials")
    private Credentials credentials;
    /**
     * GeneralPreferences object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("generalPreferences")
    private GeneralPreferences generalPreferences;
    /**
     * MailSend object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailSend")
    private MailSend mailSend;
    /**
     * MailReceipt object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailReceipt")
    private MailReceipt mailReceipt;
    /**
     * MailAccess object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailAccess")
    private MailAccess mailAccess;
    /**
     * MailStore object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailStore")
    private MailStore mailStore;
    /**
     * SocialNetworks object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("socialNetworks")
    private SocialNetworks socialNetworks;
    /**
     * ExternalAccounts object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("externalAccounts")
    private ExternalAccounts externalAccounts;
    /**
     * SMSServices object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServices")
    private SmsServices smsServices;
    /**
     * InternalInfo object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("internalInfo")
    private InternalInfo internalInfo;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Base object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("base")
    public Base getBase() {
        return base;
    }

    /**
     * Base object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("base")
    public void setBase(Base base) {
        this.base = base;
    }

    /**
     * WebMailFeatures object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("webMailFeatures")
    public WebMailFeatures getWebMailFeatures() {
        return webMailFeatures;
    }

    /**
     * WebMailFeatures object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("webMailFeatures")
    public void setWebMailFeatures(WebMailFeatures webMailFeatures) {
        this.webMailFeatures = webMailFeatures;
    }

    /**
     * Credentials object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("credentials")
    public Credentials getCredentials() {
        return credentials;
    }

    /**
     * Credentials object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("credentials")
    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    /**
     * GeneralPreferences object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("generalPreferences")
    public GeneralPreferences getGeneralPreferences() {
        return generalPreferences;
    }

    /**
     * GeneralPreferences object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("generalPreferences")
    public void setGeneralPreferences(GeneralPreferences generalPreferences) {
        this.generalPreferences = generalPreferences;
    }

    /**
     * MailSend object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailSend")
    public MailSend getMailSend() {
        return mailSend;
    }

    /**
     * MailSend object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailSend")
    public void setMailSend(MailSend mailSend) {
        this.mailSend = mailSend;
    }

    /**
     * MailReceipt object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailReceipt")
    public MailReceipt getMailReceipt() {
        return mailReceipt;
    }

    /**
     * MailReceipt object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailReceipt")
    public void setMailReceipt(MailReceipt mailReceipt) {
        this.mailReceipt = mailReceipt;
    }

    /**
     * MailAccess object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailAccess")
    public MailAccess getMailAccess() {
        return mailAccess;
    }

    /**
     * MailAccess object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailAccess")
    public void setMailAccess(MailAccess mailAccess) {
        this.mailAccess = mailAccess;
    }

    /**
     * MailStore object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailStore")
    public MailStore getMailStore() {
        return mailStore;
    }

    /**
     * MailStore object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailStore")
    public void setMailStore(MailStore mailStore) {
        this.mailStore = mailStore;
    }

    /**
     * SocialNetworks object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("socialNetworks")
    public SocialNetworks getSocialNetworks() {
        return socialNetworks;
    }

    /**
     * SocialNetworks object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("socialNetworks")
    public void setSocialNetworks(SocialNetworks socialNetworks) {
        this.socialNetworks = socialNetworks;
    }

    /**
     * ExternalAccounts object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("externalAccounts")
    public ExternalAccounts getExternalAccounts() {
        return externalAccounts;
    }

    /**
     * ExternalAccounts object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("externalAccounts")
    public void setExternalAccounts(ExternalAccounts externalAccounts) {
        this.externalAccounts = externalAccounts;
    }

    /**
     * SMSServices object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServices")
    public SmsServices getSmsServices() {
        return smsServices;
    }

    /**
     * SMSServices object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServices")
    public void setSmsServices(SmsServices smsServices) {
        this.smsServices = smsServices;
    }

    /**
     * InternalInfo object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("internalInfo")
    public InternalInfo getInternalInfo() {
        return internalInfo;
    }

    /**
     * InternalInfo object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("internalInfo")
    public void setInternalInfo(InternalInfo internalInfo) {
        this.internalInfo = internalInfo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
