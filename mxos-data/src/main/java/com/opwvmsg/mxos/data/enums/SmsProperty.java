/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All attributes used by SMS application.
 *
 * @see <i>Email Mx LDAP schema Reference Guide</i>
 * @author mxos-dev
 */
public enum SmsProperty implements DataMap.Property {
    newMsisdn,
    fromAddress, toAddress, message,
    smsType,
    authorizeMsisdn,
    pid, dcs, udh, validity, deliveryReportRequest, messageNL,
    messageFR, messageEN, messageDE,
    apicall, timestamp, key, payload, resultcode, crud,

    // Common param (between mailbox and meta)
    status
}
