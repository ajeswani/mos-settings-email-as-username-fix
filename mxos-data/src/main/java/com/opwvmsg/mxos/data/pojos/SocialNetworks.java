
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * SocialNetworks object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "socialNetworkIntegrationAllowed",
    "socialNetworkSites"
})
public class SocialNetworks implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkIntegrationAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType socialNetworkIntegrationAllowed;
    /**
     * socialNetworkSites
     * <p>
     * socialNetworkSites object of socialNetworks
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSites")
    private List<SocialNetworkSite> socialNetworkSites;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkIntegrationAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSocialNetworkIntegrationAllowed() {
        return socialNetworkIntegrationAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkIntegrationAllowed")
    public void setSocialNetworkIntegrationAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType socialNetworkIntegrationAllowed) {
        this.socialNetworkIntegrationAllowed = socialNetworkIntegrationAllowed;
    }

    /**
     * socialNetworkSites
     * <p>
     * socialNetworkSites object of socialNetworks
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSites")
    public List<SocialNetworkSite> getSocialNetworkSites() {
        return socialNetworkSites;
    }

    /**
     * socialNetworkSites
     * <p>
     * socialNetworkSites object of socialNetworks
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSites")
    public void setSocialNetworkSites(List<SocialNetworkSite> socialNetworkSites) {
        this.socialNetworkSites = socialNetworkSites;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
