
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MailSend object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "fromAddress",
    "futureDeliveryEnabled",
    "maxFutureDeliveryDaysAllowed",
    "maxFutureDeliveryMessagesAllowed",
    "alternateFromAddress",
    "replyToAddress",
    "useRichTextEditor",
    "includeOrginalMailInReply",
    "originalMailSeperatorCharacter",
    "autoSaveSentMessages",
    "addSignatureForNewMails",
    "addSignatureInReplyType",
    "signature",
    "signatures",
    "autoSpellCheckEnabled",
    "confirmPromptOnDelete",
    "includeReturnReceiptReq",
    "numDelayedDeliveryMessagesPending",
    "maxSendMessageSizeKB",
    "maxAttachmentSizeKB",
    "maxAttachmentsInSession",
    "maxAttachmentsToMessage",
    "maxCharactersPerPage",
    "filters"
})
public class MailSend implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("fromAddress")
    private java.lang.String fromAddress;
    @org.codehaus.jackson.annotate.JsonProperty("futureDeliveryEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType futureDeliveryEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("maxFutureDeliveryDaysAllowed")
    private Integer maxFutureDeliveryDaysAllowed;
    @org.codehaus.jackson.annotate.JsonProperty("maxFutureDeliveryMessagesAllowed")
    private Integer maxFutureDeliveryMessagesAllowed;
    @org.codehaus.jackson.annotate.JsonProperty("alternateFromAddress")
    private java.lang.String alternateFromAddress;
    @org.codehaus.jackson.annotate.JsonProperty("replyToAddress")
    private java.lang.String replyToAddress;
    @org.codehaus.jackson.annotate.JsonProperty("useRichTextEditor")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType useRichTextEditor;
    @org.codehaus.jackson.annotate.JsonProperty("includeOrginalMailInReply")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType includeOrginalMailInReply;
    @org.codehaus.jackson.annotate.JsonProperty("originalMailSeperatorCharacter")
    private java.lang.String originalMailSeperatorCharacter;
    @org.codehaus.jackson.annotate.JsonProperty("autoSaveSentMessages")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType autoSaveSentMessages;
    @org.codehaus.jackson.annotate.JsonProperty("addSignatureForNewMails")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType addSignatureForNewMails;
    @org.codehaus.jackson.annotate.JsonProperty("addSignatureInReplyType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType addSignatureInReplyType;
    @org.codehaus.jackson.annotate.JsonProperty("signature")
    private java.lang.String signature;
    @org.codehaus.jackson.annotate.JsonProperty("signatures")
    private List<Signature> signatures;
    @org.codehaus.jackson.annotate.JsonProperty("autoSpellCheckEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType autoSpellCheckEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("confirmPromptOnDelete")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType confirmPromptOnDelete;
    @org.codehaus.jackson.annotate.JsonProperty("includeReturnReceiptReq")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType includeReturnReceiptReq;
    @org.codehaus.jackson.annotate.JsonProperty("numDelayedDeliveryMessagesPending")
    private List<java.lang.String> numDelayedDeliveryMessagesPending;
    @org.codehaus.jackson.annotate.JsonProperty("maxSendMessageSizeKB")
    private Integer maxSendMessageSizeKB;
    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentSizeKB")
    private Integer maxAttachmentSizeKB;
    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentsInSession")
    private Integer maxAttachmentsInSession;
    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentsToMessage")
    private Integer maxAttachmentsToMessage;
    @org.codehaus.jackson.annotate.JsonProperty("maxCharactersPerPage")
    private Integer maxCharactersPerPage;
    /**
     * filters object of mailSend
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("filters")
    private Filters filters;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("fromAddress")
    public java.lang.String getFromAddress() {
        return fromAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fromAddress")
    public void setFromAddress(java.lang.String fromAddress) {
        this.fromAddress = fromAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("futureDeliveryEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getFutureDeliveryEnabled() {
        return futureDeliveryEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("futureDeliveryEnabled")
    public void setFutureDeliveryEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType futureDeliveryEnabled) {
        this.futureDeliveryEnabled = futureDeliveryEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxFutureDeliveryDaysAllowed")
    public Integer getMaxFutureDeliveryDaysAllowed() {
        return maxFutureDeliveryDaysAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxFutureDeliveryDaysAllowed")
    public void setMaxFutureDeliveryDaysAllowed(Integer maxFutureDeliveryDaysAllowed) {
        this.maxFutureDeliveryDaysAllowed = maxFutureDeliveryDaysAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxFutureDeliveryMessagesAllowed")
    public Integer getMaxFutureDeliveryMessagesAllowed() {
        return maxFutureDeliveryMessagesAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxFutureDeliveryMessagesAllowed")
    public void setMaxFutureDeliveryMessagesAllowed(Integer maxFutureDeliveryMessagesAllowed) {
        this.maxFutureDeliveryMessagesAllowed = maxFutureDeliveryMessagesAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("alternateFromAddress")
    public java.lang.String getAlternateFromAddress() {
        return alternateFromAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("alternateFromAddress")
    public void setAlternateFromAddress(java.lang.String alternateFromAddress) {
        this.alternateFromAddress = alternateFromAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("replyToAddress")
    public java.lang.String getReplyToAddress() {
        return replyToAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("replyToAddress")
    public void setReplyToAddress(java.lang.String replyToAddress) {
        this.replyToAddress = replyToAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("useRichTextEditor")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getUseRichTextEditor() {
        return useRichTextEditor;
    }

    @org.codehaus.jackson.annotate.JsonProperty("useRichTextEditor")
    public void setUseRichTextEditor(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType useRichTextEditor) {
        this.useRichTextEditor = useRichTextEditor;
    }

    @org.codehaus.jackson.annotate.JsonProperty("includeOrginalMailInReply")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getIncludeOrginalMailInReply() {
        return includeOrginalMailInReply;
    }

    @org.codehaus.jackson.annotate.JsonProperty("includeOrginalMailInReply")
    public void setIncludeOrginalMailInReply(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType includeOrginalMailInReply) {
        this.includeOrginalMailInReply = includeOrginalMailInReply;
    }

    @org.codehaus.jackson.annotate.JsonProperty("originalMailSeperatorCharacter")
    public java.lang.String getOriginalMailSeperatorCharacter() {
        return originalMailSeperatorCharacter;
    }

    @org.codehaus.jackson.annotate.JsonProperty("originalMailSeperatorCharacter")
    public void setOriginalMailSeperatorCharacter(java.lang.String originalMailSeperatorCharacter) {
        this.originalMailSeperatorCharacter = originalMailSeperatorCharacter;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoSaveSentMessages")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getAutoSaveSentMessages() {
        return autoSaveSentMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoSaveSentMessages")
    public void setAutoSaveSentMessages(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType autoSaveSentMessages) {
        this.autoSaveSentMessages = autoSaveSentMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addSignatureForNewMails")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getAddSignatureForNewMails() {
        return addSignatureForNewMails;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addSignatureForNewMails")
    public void setAddSignatureForNewMails(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType addSignatureForNewMails) {
        this.addSignatureForNewMails = addSignatureForNewMails;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addSignatureInReplyType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType getAddSignatureInReplyType() {
        return addSignatureInReplyType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addSignatureInReplyType")
    public void setAddSignatureInReplyType(com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType addSignatureInReplyType) {
        this.addSignatureInReplyType = addSignatureInReplyType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signature")
    public java.lang.String getSignature() {
        return signature;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signature")
    public void setSignature(java.lang.String signature) {
        this.signature = signature;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signatures")
    public List<Signature> getSignatures() {
        return signatures;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signatures")
    public void setSignatures(List<Signature> signatures) {
        this.signatures = signatures;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoSpellCheckEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getAutoSpellCheckEnabled() {
        return autoSpellCheckEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoSpellCheckEnabled")
    public void setAutoSpellCheckEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType autoSpellCheckEnabled) {
        this.autoSpellCheckEnabled = autoSpellCheckEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("confirmPromptOnDelete")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getConfirmPromptOnDelete() {
        return confirmPromptOnDelete;
    }

    @org.codehaus.jackson.annotate.JsonProperty("confirmPromptOnDelete")
    public void setConfirmPromptOnDelete(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType confirmPromptOnDelete) {
        this.confirmPromptOnDelete = confirmPromptOnDelete;
    }

    @org.codehaus.jackson.annotate.JsonProperty("includeReturnReceiptReq")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getIncludeReturnReceiptReq() {
        return includeReturnReceiptReq;
    }

    @org.codehaus.jackson.annotate.JsonProperty("includeReturnReceiptReq")
    public void setIncludeReturnReceiptReq(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType includeReturnReceiptReq) {
        this.includeReturnReceiptReq = includeReturnReceiptReq;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numDelayedDeliveryMessagesPending")
    public List<java.lang.String> getNumDelayedDeliveryMessagesPending() {
        return numDelayedDeliveryMessagesPending;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numDelayedDeliveryMessagesPending")
    public void setNumDelayedDeliveryMessagesPending(List<java.lang.String> numDelayedDeliveryMessagesPending) {
        this.numDelayedDeliveryMessagesPending = numDelayedDeliveryMessagesPending;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxSendMessageSizeKB")
    public Integer getMaxSendMessageSizeKB() {
        return maxSendMessageSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxSendMessageSizeKB")
    public void setMaxSendMessageSizeKB(Integer maxSendMessageSizeKB) {
        this.maxSendMessageSizeKB = maxSendMessageSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentSizeKB")
    public Integer getMaxAttachmentSizeKB() {
        return maxAttachmentSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentSizeKB")
    public void setMaxAttachmentSizeKB(Integer maxAttachmentSizeKB) {
        this.maxAttachmentSizeKB = maxAttachmentSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentsInSession")
    public Integer getMaxAttachmentsInSession() {
        return maxAttachmentsInSession;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentsInSession")
    public void setMaxAttachmentsInSession(Integer maxAttachmentsInSession) {
        this.maxAttachmentsInSession = maxAttachmentsInSession;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentsToMessage")
    public Integer getMaxAttachmentsToMessage() {
        return maxAttachmentsToMessage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxAttachmentsToMessage")
    public void setMaxAttachmentsToMessage(Integer maxAttachmentsToMessage) {
        this.maxAttachmentsToMessage = maxAttachmentsToMessage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxCharactersPerPage")
    public Integer getMaxCharactersPerPage() {
        return maxCharactersPerPage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxCharactersPerPage")
    public void setMaxCharactersPerPage(Integer maxCharactersPerPage) {
        this.maxCharactersPerPage = maxCharactersPerPage;
    }

    /**
     * filters object of mailSend
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("filters")
    public Filters getFilters() {
        return filters;
    }

    /**
     * filters object of mailSend
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("filters")
    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
