/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All Tasks attributes used by this application.
 * 
 * @author mxos-dev
 */
public enum TasksProperty implements DataMap.Property {

    // common
    oxHttpURL, moveToFolderId,

    // login
    userId, password, sessionId, cookieString,

    // base
    taskId, folderId, name, owner, priority, status, isPrivate, colorLabel, categories,
    notes, createdDate, updatedDate, startDate, dueDate, completedDate, reminderDate, progress,

    // recurrence
    type, daysInWeek, dayInMonth, monthInYear, recurAfterInterval, endDate, notification, occurrences,

    // participant
    participantId, participantType, participantName, participantEmail, participantStatus,
    participantMessage, participantHref,
    
    // details
    actualCost, actualWork, billing, targetCost, targetWork, currency, mileage, companies,
    
    // attachments
    attachmentName, attachmentSize, attachmentBody,

    // base search/sort/filter
    query, sortKey, sortOrder, filter,

    // list of contacts, groups
    tasksList,
    
    // Attachment Attributes
    attchmentName,
}
