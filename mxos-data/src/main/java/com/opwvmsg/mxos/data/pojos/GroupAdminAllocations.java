
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Group Admin Allocations object of mailStore
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "maxAdminStorageSizeKB",
    "maxAdminUsers",
    "adminNumUsers",
    "adminUsedStorageSizeKB"
})
public class GroupAdminAllocations implements Serializable
{

    @JsonProperty("maxAdminStorageSizeKB")
    private Long maxAdminStorageSizeKB;
    @JsonProperty("maxAdminUsers")
    private Long maxAdminUsers;
    @JsonProperty("adminNumUsers")
    private Long adminNumUsers;
    @JsonProperty("adminUsedStorageSizeKB")
    private Long adminUsedStorageSizeKB;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("maxAdminStorageSizeKB")
    public Long getMaxAdminStorageSizeKB() {
        return maxAdminStorageSizeKB;
    }

    @JsonProperty("maxAdminStorageSizeKB")
    public void setMaxAdminStorageSizeKB(Long maxAdminStorageSizeKB) {
        this.maxAdminStorageSizeKB = maxAdminStorageSizeKB;
    }

    @JsonProperty("maxAdminUsers")
    public Long getMaxAdminUsers() {
        return maxAdminUsers;
    }

    @JsonProperty("maxAdminUsers")
    public void setMaxAdminUsers(Long maxAdminUsers) {
        this.maxAdminUsers = maxAdminUsers;
    }

    @JsonProperty("adminNumUsers")
    public Long getAdminNumUsers() {
        return adminNumUsers;
    }

    @JsonProperty("adminNumUsers")
    public void setAdminNumUsers(Long adminNumUsers) {
        this.adminNumUsers = adminNumUsers;
    }

    @JsonProperty("adminUsedStorageSizeKB")
    public Long getAdminUsedStorageSizeKB() {
        return adminUsedStorageSizeKB;
    }

    @JsonProperty("adminUsedStorageSizeKB")
    public void setAdminUsedStorageSizeKB(Long adminUsedStorageSizeKB) {
        this.adminUsedStorageSizeKB = adminUsedStorageSizeKB;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
