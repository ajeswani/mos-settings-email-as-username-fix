
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MailAccount object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "accountId",
    "accountName",
    "accountType",
    "serverAddress",
    "serverPort",
    "timeoutSecs",
    "emailAddress",
    "accountUserName",
    "accountPassword",
    "displayImage",
    "fetchMailsEnabled",
    "autoFetchMails",
    "fetchedMailsFolderName",
    "leaveEmailsOnServer",
    "fromAddressInReply",
    "mailSendEnabled",
    "mailSendSMTPAddress",
    "mailSendSMTPPort",
    "smtpSSLEnabled",
    "useSmtpLogin",
    "smtpLogin",
    "smtpPassword",
    "sendFolderName",
    "draftsFolderName",
    "trashFolderName",
    "spamFolderName"
})
public class MailAccount implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("accountId")
    private Integer accountId;
    @org.codehaus.jackson.annotate.JsonProperty("accountName")
    private java.lang.String accountName;
    @org.codehaus.jackson.annotate.JsonProperty("accountType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccountType accountType;
    @org.codehaus.jackson.annotate.JsonProperty("serverAddress")
    private java.lang.String serverAddress;
    @org.codehaus.jackson.annotate.JsonProperty("serverPort")
    private Integer serverPort;
    @org.codehaus.jackson.annotate.JsonProperty("timeoutSecs")
    private Integer timeoutSecs;
    @org.codehaus.jackson.annotate.JsonProperty("emailAddress")
    private java.lang.String emailAddress;
    @org.codehaus.jackson.annotate.JsonProperty("accountUserName")
    private java.lang.String accountUserName;
    @org.codehaus.jackson.annotate.JsonProperty("accountPassword")
    private java.lang.String accountPassword;
    @org.codehaus.jackson.annotate.JsonProperty("displayImage")
    private java.lang.String displayImage;
    @org.codehaus.jackson.annotate.JsonProperty("fetchMailsEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType fetchMailsEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("autoFetchMails")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType autoFetchMails;
    @org.codehaus.jackson.annotate.JsonProperty("fetchedMailsFolderName")
    private java.lang.String fetchedMailsFolderName;
    @org.codehaus.jackson.annotate.JsonProperty("leaveEmailsOnServer")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType leaveEmailsOnServer;
    @org.codehaus.jackson.annotate.JsonProperty("fromAddressInReply")
    private java.lang.String fromAddressInReply;
    @org.codehaus.jackson.annotate.JsonProperty("mailSendEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType mailSendEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("mailSendSMTPAddress")
    private java.lang.String mailSendSMTPAddress;
    @org.codehaus.jackson.annotate.JsonProperty("mailSendSMTPPort")
    private Integer mailSendSMTPPort;
    @org.codehaus.jackson.annotate.JsonProperty("smtpSSLEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpSSLEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("useSmtpLogin")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType useSmtpLogin;
    @org.codehaus.jackson.annotate.JsonProperty("smtpLogin")
    private java.lang.String smtpLogin;
    @org.codehaus.jackson.annotate.JsonProperty("smtpPassword")
    private java.lang.String smtpPassword;
    @org.codehaus.jackson.annotate.JsonProperty("sendFolderName")
    private java.lang.String sendFolderName;
    @org.codehaus.jackson.annotate.JsonProperty("draftsFolderName")
    private java.lang.String draftsFolderName;
    @org.codehaus.jackson.annotate.JsonProperty("trashFolderName")
    private java.lang.String trashFolderName;
    @org.codehaus.jackson.annotate.JsonProperty("spamFolderName")
    private java.lang.String spamFolderName;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("accountId")
    public Integer getAccountId() {
        return accountId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountId")
    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountName")
    public java.lang.String getAccountName() {
        return accountName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountName")
    public void setAccountName(java.lang.String accountName) {
        this.accountName = accountName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccountType getAccountType() {
        return accountType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountType")
    public void setAccountType(com.opwvmsg.mxos.data.enums.MxosEnums.AccountType accountType) {
        this.accountType = accountType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("serverAddress")
    public java.lang.String getServerAddress() {
        return serverAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("serverAddress")
    public void setServerAddress(java.lang.String serverAddress) {
        this.serverAddress = serverAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("serverPort")
    public Integer getServerPort() {
        return serverPort;
    }

    @org.codehaus.jackson.annotate.JsonProperty("serverPort")
    public void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    @org.codehaus.jackson.annotate.JsonProperty("timeoutSecs")
    public Integer getTimeoutSecs() {
        return timeoutSecs;
    }

    @org.codehaus.jackson.annotate.JsonProperty("timeoutSecs")
    public void setTimeoutSecs(Integer timeoutSecs) {
        this.timeoutSecs = timeoutSecs;
    }

    @org.codehaus.jackson.annotate.JsonProperty("emailAddress")
    public java.lang.String getEmailAddress() {
        return emailAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("emailAddress")
    public void setEmailAddress(java.lang.String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountUserName")
    public java.lang.String getAccountUserName() {
        return accountUserName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountUserName")
    public void setAccountUserName(java.lang.String accountUserName) {
        this.accountUserName = accountUserName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountPassword")
    public java.lang.String getAccountPassword() {
        return accountPassword;
    }

    @org.codehaus.jackson.annotate.JsonProperty("accountPassword")
    public void setAccountPassword(java.lang.String accountPassword) {
        this.accountPassword = accountPassword;
    }

    @org.codehaus.jackson.annotate.JsonProperty("displayImage")
    public java.lang.String getDisplayImage() {
        return displayImage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("displayImage")
    public void setDisplayImage(java.lang.String displayImage) {
        this.displayImage = displayImage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fetchMailsEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getFetchMailsEnabled() {
        return fetchMailsEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fetchMailsEnabled")
    public void setFetchMailsEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType fetchMailsEnabled) {
        this.fetchMailsEnabled = fetchMailsEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoFetchMails")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getAutoFetchMails() {
        return autoFetchMails;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoFetchMails")
    public void setAutoFetchMails(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType autoFetchMails) {
        this.autoFetchMails = autoFetchMails;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fetchedMailsFolderName")
    public java.lang.String getFetchedMailsFolderName() {
        return fetchedMailsFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fetchedMailsFolderName")
    public void setFetchedMailsFolderName(java.lang.String fetchedMailsFolderName) {
        this.fetchedMailsFolderName = fetchedMailsFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("leaveEmailsOnServer")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getLeaveEmailsOnServer() {
        return leaveEmailsOnServer;
    }

    @org.codehaus.jackson.annotate.JsonProperty("leaveEmailsOnServer")
    public void setLeaveEmailsOnServer(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType leaveEmailsOnServer) {
        this.leaveEmailsOnServer = leaveEmailsOnServer;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fromAddressInReply")
    public java.lang.String getFromAddressInReply() {
        return fromAddressInReply;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fromAddressInReply")
    public void setFromAddressInReply(java.lang.String fromAddressInReply) {
        this.fromAddressInReply = fromAddressInReply;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailSendEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getMailSendEnabled() {
        return mailSendEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailSendEnabled")
    public void setMailSendEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType mailSendEnabled) {
        this.mailSendEnabled = mailSendEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailSendSMTPAddress")
    public java.lang.String getMailSendSMTPAddress() {
        return mailSendSMTPAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailSendSMTPAddress")
    public void setMailSendSMTPAddress(java.lang.String mailSendSMTPAddress) {
        this.mailSendSMTPAddress = mailSendSMTPAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailSendSMTPPort")
    public Integer getMailSendSMTPPort() {
        return mailSendSMTPPort;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailSendSMTPPort")
    public void setMailSendSMTPPort(Integer mailSendSMTPPort) {
        this.mailSendSMTPPort = mailSendSMTPPort;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpSSLEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmtpSSLEnabled() {
        return smtpSSLEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpSSLEnabled")
    public void setSmtpSSLEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpSSLEnabled) {
        this.smtpSSLEnabled = smtpSSLEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("useSmtpLogin")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getUseSmtpLogin() {
        return useSmtpLogin;
    }

    @org.codehaus.jackson.annotate.JsonProperty("useSmtpLogin")
    public void setUseSmtpLogin(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType useSmtpLogin) {
        this.useSmtpLogin = useSmtpLogin;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpLogin")
    public java.lang.String getSmtpLogin() {
        return smtpLogin;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpLogin")
    public void setSmtpLogin(java.lang.String smtpLogin) {
        this.smtpLogin = smtpLogin;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpPassword")
    public java.lang.String getSmtpPassword() {
        return smtpPassword;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpPassword")
    public void setSmtpPassword(java.lang.String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    @org.codehaus.jackson.annotate.JsonProperty("sendFolderName")
    public java.lang.String getSendFolderName() {
        return sendFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("sendFolderName")
    public void setSendFolderName(java.lang.String sendFolderName) {
        this.sendFolderName = sendFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("draftsFolderName")
    public java.lang.String getDraftsFolderName() {
        return draftsFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("draftsFolderName")
    public void setDraftsFolderName(java.lang.String draftsFolderName) {
        this.draftsFolderName = draftsFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("trashFolderName")
    public java.lang.String getTrashFolderName() {
        return trashFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("trashFolderName")
    public void setTrashFolderName(java.lang.String trashFolderName) {
        this.trashFolderName = trashFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("spamFolderName")
    public java.lang.String getSpamFolderName() {
        return spamFolderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("spamFolderName")
    public void setSpamFolderName(java.lang.String spamFolderName) {
        this.spamFolderName = spamFolderName;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
