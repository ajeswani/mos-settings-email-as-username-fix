/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All LDAP attributes used by this application.
 *
 * @see <i>Email Mx LDAP schema Reference Guide</i>
 * @author Aricent
 */
public enum HeaderProperty implements DataMap.Property {

    /**
     * <tt>Keywords</tt>: Specifies message Keywords.
     *
     * @since mxos 2.0
     */
    keywords,

    /**
     * <tt>Date</tt>: Specifies message Date header. This is sent Date of the
     * message.
     *
     * @since mxos 2.0
     */
    date,

    /**
     * <tt>ReceivedFrom</tt>: Specifies message header 'from'.
     * This is kept for sorting purpose.
     *
     * @since mxos 2.0
     */
    receivedfrom,

    /**
     * <tt>Subject</tt>: Specifies message header 'Subject'. This is kept for
     * sorting purpose.
     *
     * @since mxos 2.0
     */
    subject,

    /**
     * <tt>HeaderSummary</tt>: Specifies message header summary.
     *
     * @since mxos 2.0
     */
    headersummary,

    /**
     * <tt>Sentto</tt>: Specifies message header 'to'. This is kept for sorting
     * purpose.
     *
     * @since mxos 2.0
     */
    sentto,

    /**
     * <tt>Cc</tt>: Specifies message header 'Cc'. This is kept for sorting
     * purpose.
     *
     * @since mxos 2.0
     */
    cc,
    
    /**
     * <tt>from</tt>: Specifies from address.
     * 
     * @since mxos 2.0
     */
    from,

    /**
     * <tt>to</tt>: Specifies to address.
     * 
     * @since mxos 2.0
     */
    to,

    /**
     * <tt>bcc</tt>: Specifies address in bcc.
     * 
     * @since mxos 2.0
     */
    bcc,

    /**
     * <tt>sender</tt>: Specifies sender address.
     * 
     * @since mxos 2.0
     */
    sender,

    /**
     * <tt>replyTo</tt>: Specifies replyTo address.
     * 
     * @since mxos 2.0
     */
    replyTo,

    /**
     * <tt>inReplyTo</tt>: Specifies inReplyTo address.
     * 
     * @since mxos 2.0
     */
    inReplyTo,

    /**
     * <tt>msgId</tt>: Message id of the message.
     * 
     * @since mxos 2.0
     */
    msgId,

    /**
     * <tt>bodyStructure</tt>: Specifies bodyStructure of the message.
     * 
     * @since mxos 2.0
     */
    bodyStructure

}
