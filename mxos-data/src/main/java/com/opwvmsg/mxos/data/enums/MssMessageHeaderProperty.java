/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * MSS attributes for the Message Header
 * 
 * @see <i>Email Mx LDAP schema Reference Guide</i>
 * @author Aricent
 */
public enum MssMessageHeaderProperty implements DataMap.Property {

    /**
     * <tt>from</tt>: Specifies from address.
     * 
     * @since mxos 2.0
     */
    from("from"),

    /**
     * <tt>to</tt>: Specifies to address.
     * 
     * @since mxos 2.0
     */
    to("to"),

    /**
     * <tt>cc</tt>: Specifies address in cc.
     * 
     * @since mxos 2.0
     */
    cc("cc"),

    /**
     * <tt>bcc</tt>: Specifies address in bcc.
     * 
     * @since mxos 2.0
     */
    bcc("bcc"),

    /**
     * <tt>subject</tt>: Specifies subject
     * 
     * @since mxos 2.0
     */
    subject("subject"),

    /**
     * <tt>sender</tt>: Specifies sender
     * 
     * @since mxos 2.0
     */
    sender("sender"),

    /**
     * <tt>replyTo</tt>: Specifies replyTo address
     * 
     * @since mxos 2.0
     */
    replyTo("reply-to"),

    /**
     * <tt>inReplyTo</tt>: Specifies inReplyTo address.
     * 
     * @since mxos 2.0
     */
    inReplyTo("in-reply-to"),

    /**
     * <tt>messageId</tt>: Specifies messageId of the message.
     * 
     * @since mxos 2.0
     */
    messageId("message-id"),

    /**
     * <tt>date</tt>: Specifies arrival date of the message.
     * 
     * @since mxos 2.0
     */
    date("date"),

    /**
     * <tt>bodyStructure</tt>: Specifies bodyStructure of the message.
     * 
     * @since mxos 2.0
     */
    bodyStructure("body-structure");

    private String value;

    MssMessageHeaderProperty(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}