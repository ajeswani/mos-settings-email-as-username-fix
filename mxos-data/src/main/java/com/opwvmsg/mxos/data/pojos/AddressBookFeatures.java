
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Address book features object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "maxContacts",
    "maxGroups",
    "maxContactsPerGroup",
    "maxContactsPerPage",
    "createContactsFromOutgoingEmails"
})
public class AddressBookFeatures implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("maxContacts")
    private Integer maxContacts;
    @org.codehaus.jackson.annotate.JsonProperty("maxGroups")
    private Integer maxGroups;
    @org.codehaus.jackson.annotate.JsonProperty("maxContactsPerGroup")
    private Integer maxContactsPerGroup;
    @org.codehaus.jackson.annotate.JsonProperty("maxContactsPerPage")
    private Integer maxContactsPerPage;
    @org.codehaus.jackson.annotate.JsonProperty("createContactsFromOutgoingEmails")
    private com.opwvmsg.mxos.data.enums.MxosEnums.CreateContactsFromOutgoingEmails createContactsFromOutgoingEmails;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("maxContacts")
    public Integer getMaxContacts() {
        return maxContacts;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxContacts")
    public void setMaxContacts(Integer maxContacts) {
        this.maxContacts = maxContacts;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxGroups")
    public Integer getMaxGroups() {
        return maxGroups;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxGroups")
    public void setMaxGroups(Integer maxGroups) {
        this.maxGroups = maxGroups;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxContactsPerGroup")
    public Integer getMaxContactsPerGroup() {
        return maxContactsPerGroup;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxContactsPerGroup")
    public void setMaxContactsPerGroup(Integer maxContactsPerGroup) {
        this.maxContactsPerGroup = maxContactsPerGroup;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxContactsPerPage")
    public Integer getMaxContactsPerPage() {
        return maxContactsPerPage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxContactsPerPage")
    public void setMaxContactsPerPage(Integer maxContactsPerPage) {
        this.maxContactsPerPage = maxContactsPerPage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("createContactsFromOutgoingEmails")
    public com.opwvmsg.mxos.data.enums.MxosEnums.CreateContactsFromOutgoingEmails getCreateContactsFromOutgoingEmails() {
        return createContactsFromOutgoingEmails;
    }

    @org.codehaus.jackson.annotate.JsonProperty("createContactsFromOutgoingEmails")
    public void setCreateContactsFromOutgoingEmails(com.opwvmsg.mxos.data.enums.MxosEnums.CreateContactsFromOutgoingEmails createContactsFromOutgoingEmails) {
        this.createContactsFromOutgoingEmails = createContactsFromOutgoingEmails;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
