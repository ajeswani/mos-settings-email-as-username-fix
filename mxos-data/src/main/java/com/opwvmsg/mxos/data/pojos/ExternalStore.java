
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * External Store object of mailStore
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "externalStoreAccessAllowed",
    "maxExternalStoreSizeMB"
})
public class ExternalStore implements Serializable
{

    @JsonProperty("externalStoreAccessAllowed")
    private String externalStoreAccessAllowed;
    @JsonProperty("maxExternalStoreSizeMB")
    private Integer maxExternalStoreSizeMB;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("externalStoreAccessAllowed")
    public String getExternalStoreAccessAllowed() {
        return externalStoreAccessAllowed;
    }

    @JsonProperty("externalStoreAccessAllowed")
    public void setExternalStoreAccessAllowed(String externalStoreAccessAllowed) {
        this.externalStoreAccessAllowed = externalStoreAccessAllowed;
    }

    @JsonProperty("maxExternalStoreSizeMB")
    public Integer getMaxExternalStoreSizeMB() {
        return maxExternalStoreSizeMB;
    }

    @JsonProperty("maxExternalStoreSizeMB")
    public void setMaxExternalStoreSizeMB(Integer maxExternalStoreSizeMB) {
        this.maxExternalStoreSizeMB = maxExternalStoreSizeMB;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
