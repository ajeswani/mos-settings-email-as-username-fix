
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Credentials object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "passwordStoreType",
    "password",
    "passwordRecoveryAllowed",
    "passwordRecoveryPreference",
    "passwordRecoveryMsisdn",
    "passwordRecoveryMsisdnStatus",
    "lastPasswordRecoveryMsisdnStatusChangeDate",
    "passwordRecoveryEmail",
    "passwordRecoveryEmailStatus",
    "lastLoginAttemptDate",
    "lastSuccessfulLoginDate",
    "failedLoginAttempts",
    "lastFailedLoginDate",
    "maxFailedLoginAttempts",
    "failedCaptchaLoginAttempts",
    "maxFailedCaptchaLoginAttempts"
})
public class Credentials implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordStoreType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.PasswordStoreType passwordStoreType;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("password")
    private java.lang.String password;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType passwordRecoveryAllowed;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryPreference")
    private com.opwvmsg.mxos.data.enums.MxosEnums.PasswordRecoveryPreference passwordRecoveryPreference;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryMsisdn")
    private java.lang.String passwordRecoveryMsisdn;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryMsisdnStatus")
    private com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus passwordRecoveryMsisdnStatus;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastPasswordRecoveryMsisdnStatusChangeDate")
    private Object lastPasswordRecoveryMsisdnStatusChangeDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryEmail")
    private java.lang.String passwordRecoveryEmail;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryEmailStatus")
    private com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus passwordRecoveryEmailStatus;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastLoginAttemptDate")
    private Object lastLoginAttemptDate;
    @org.codehaus.jackson.annotate.JsonProperty("lastSuccessfulLoginDate")
    private Object lastSuccessfulLoginDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("failedLoginAttempts")
    private Integer failedLoginAttempts;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastFailedLoginDate")
    private Object lastFailedLoginDate;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maxFailedLoginAttempts")
    private Integer maxFailedLoginAttempts;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("failedCaptchaLoginAttempts")
    private Integer failedCaptchaLoginAttempts;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maxFailedCaptchaLoginAttempts")
    private Integer maxFailedCaptchaLoginAttempts;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordStoreType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.PasswordStoreType getPasswordStoreType() {
        return passwordStoreType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordStoreType")
    public void setPasswordStoreType(com.opwvmsg.mxos.data.enums.MxosEnums.PasswordStoreType passwordStoreType) {
        this.passwordStoreType = passwordStoreType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("password")
    public java.lang.String getPassword() {
        return password;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("password")
    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getPasswordRecoveryAllowed() {
        return passwordRecoveryAllowed;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryAllowed")
    public void setPasswordRecoveryAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType passwordRecoveryAllowed) {
        this.passwordRecoveryAllowed = passwordRecoveryAllowed;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryPreference")
    public com.opwvmsg.mxos.data.enums.MxosEnums.PasswordRecoveryPreference getPasswordRecoveryPreference() {
        return passwordRecoveryPreference;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryPreference")
    public void setPasswordRecoveryPreference(com.opwvmsg.mxos.data.enums.MxosEnums.PasswordRecoveryPreference passwordRecoveryPreference) {
        this.passwordRecoveryPreference = passwordRecoveryPreference;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryMsisdn")
    public java.lang.String getPasswordRecoveryMsisdn() {
        return passwordRecoveryMsisdn;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryMsisdn")
    public void setPasswordRecoveryMsisdn(java.lang.String passwordRecoveryMsisdn) {
        this.passwordRecoveryMsisdn = passwordRecoveryMsisdn;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryMsisdnStatus")
    public com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus getPasswordRecoveryMsisdnStatus() {
        return passwordRecoveryMsisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryMsisdnStatus")
    public void setPasswordRecoveryMsisdnStatus(com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus passwordRecoveryMsisdnStatus) {
        this.passwordRecoveryMsisdnStatus = passwordRecoveryMsisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastPasswordRecoveryMsisdnStatusChangeDate")
    public Object getLastPasswordRecoveryMsisdnStatusChangeDate() {
        return lastPasswordRecoveryMsisdnStatusChangeDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastPasswordRecoveryMsisdnStatusChangeDate")
    public void setLastPasswordRecoveryMsisdnStatusChangeDate(Object lastPasswordRecoveryMsisdnStatusChangeDate) {
        this.lastPasswordRecoveryMsisdnStatusChangeDate = lastPasswordRecoveryMsisdnStatusChangeDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryEmail")
    public java.lang.String getPasswordRecoveryEmail() {
        return passwordRecoveryEmail;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryEmail")
    public void setPasswordRecoveryEmail(java.lang.String passwordRecoveryEmail) {
        this.passwordRecoveryEmail = passwordRecoveryEmail;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryEmailStatus")
    public com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus getPasswordRecoveryEmailStatus() {
        return passwordRecoveryEmailStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("passwordRecoveryEmailStatus")
    public void setPasswordRecoveryEmailStatus(com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus passwordRecoveryEmailStatus) {
        this.passwordRecoveryEmailStatus = passwordRecoveryEmailStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastLoginAttemptDate")
    public Object getLastLoginAttemptDate() {
        return lastLoginAttemptDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastLoginAttemptDate")
    public void setLastLoginAttemptDate(Object lastLoginAttemptDate) {
        this.lastLoginAttemptDate = lastLoginAttemptDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastSuccessfulLoginDate")
    public Object getLastSuccessfulLoginDate() {
        return lastSuccessfulLoginDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastSuccessfulLoginDate")
    public void setLastSuccessfulLoginDate(Object lastSuccessfulLoginDate) {
        this.lastSuccessfulLoginDate = lastSuccessfulLoginDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("failedLoginAttempts")
    public Integer getFailedLoginAttempts() {
        return failedLoginAttempts;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("failedLoginAttempts")
    public void setFailedLoginAttempts(Integer failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastFailedLoginDate")
    public Object getLastFailedLoginDate() {
        return lastFailedLoginDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastFailedLoginDate")
    public void setLastFailedLoginDate(Object lastFailedLoginDate) {
        this.lastFailedLoginDate = lastFailedLoginDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maxFailedLoginAttempts")
    public Integer getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maxFailedLoginAttempts")
    public void setMaxFailedLoginAttempts(Integer maxFailedLoginAttempts) {
        this.maxFailedLoginAttempts = maxFailedLoginAttempts;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("failedCaptchaLoginAttempts")
    public Integer getFailedCaptchaLoginAttempts() {
        return failedCaptchaLoginAttempts;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("failedCaptchaLoginAttempts")
    public void setFailedCaptchaLoginAttempts(Integer failedCaptchaLoginAttempts) {
        this.failedCaptchaLoginAttempts = failedCaptchaLoginAttempts;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maxFailedCaptchaLoginAttempts")
    public Integer getMaxFailedCaptchaLoginAttempts() {
        return maxFailedCaptchaLoginAttempts;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maxFailedCaptchaLoginAttempts")
    public void setMaxFailedCaptchaLoginAttempts(Integer maxFailedCaptchaLoginAttempts) {
        this.maxFailedCaptchaLoginAttempts = maxFailedCaptchaLoginAttempts;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
