/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All AddressBook attributes used by this application.
 * 
 * @author mxos-dev
 */
public enum AddressBookProperty implements DataMap.Property {

    // common
    oxHttpURL, contactId, folderId, moveToFolderId, buddyName, imProvider,

    // login
    userId, password, sessionId, cookieString,

    // base
    created, updated, isPrivate, colorLabel, categories, notes, 
    // custom fields
    pager, yomiFirstName, yomiLastName, yomiCompany, email3, fileName, uid, otherPhone,

    // base search/sort/filter
    query, sortKey, sortOrder, filter,

    // name
    firstName, middleName, lastName, displayName, prefix, suffix, nickName,

    // personalInfo
    maritalStatus, spouseName, homeWebPage, children,

    // personalInfo address
    personalInfoStreet, personalInfoCity, personalInfoStateOrProvince, personalInfoPostalCode, personalInfoCountry, personalInfoAddress,

    // personalInfo communication
    personalInfoPhone1, personalInfoPhone2, personalInfoMobile, personalInfoFax, personalInfoEmail, personalInfoImAddress, personalInfoVoip,

    // workInfo
    companyName, department, title, manager, assistant, assistantPhone, employeeId, webPage,

    // workInfo address
    workInfoStreet, workInfoCity, workInfoStateOrProvince, workInfoPostalCode, workInfoCountry, workInfoAddress,

    // workInfo communication
    workInfoPhone1, workInfoPhone2, workInfoMobile, workInfoFax, workInfoEmail, workInfoImAddress,

    // common address
    street, city, stateOrProvince, postalCode, country,

    // common communication
    phone1, phone2, mobile, fax, email, imAddress, voip,

    // group
    groupId, sharedContactsId, sharedGroupId, externalMemberId, memberId, customFields, groupName, memberName, groupDescription, 
    memberEmail, memberFolderId, mailField,

    // subscriptions
    subscriptionId,

    // notification
    notificationId,

    // contactsImage
    actualImage,

    // events
    type, date, birthDate, anniversaryDate, birthDay, birthMonth, birthYear, anniversaryDay, anniversaryMonth, anniversaryYear,

    // list of contacts, groups
    contactsList, groupsList
}
