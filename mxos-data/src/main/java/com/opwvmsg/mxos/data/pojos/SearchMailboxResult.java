
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MailBox object of SearchMailbox Result
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "userName",
    "domain",
    "mailboxId",
    "status",
    "type",
    "email",
    "allowedDomains",
    "emailAliases",
    "cosId"
})
public class SearchMailboxResult implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("userName")
    private java.lang.String userName;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    private java.lang.String domain;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    private Long mailboxId;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    private com.opwvmsg.mxos.data.enums.MxosEnums.Status status;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    private java.lang.String type;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    private java.lang.String email;
    @org.codehaus.jackson.annotate.JsonProperty("allowedDomains")
    private List<java.lang.String> allowedDomains;
    @org.codehaus.jackson.annotate.JsonProperty("emailAliases")
    private List<java.lang.String> emailAliases;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cosId")
    private java.lang.String cosId;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("userName")
    public java.lang.String getUserName() {
        return userName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("userName")
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    public java.lang.String getDomain() {
        return domain;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    public void setDomain(java.lang.String domain) {
        this.domain = domain;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    public Long getMailboxId() {
        return mailboxId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    public void setMailboxId(Long mailboxId) {
        this.mailboxId = mailboxId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    public com.opwvmsg.mxos.data.enums.MxosEnums.Status getStatus() {
        return status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    public void setStatus(com.opwvmsg.mxos.data.enums.MxosEnums.Status status) {
        this.status = status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public java.lang.String getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public void setType(java.lang.String type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    public java.lang.String getEmail() {
        return email;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    public void setEmail(java.lang.String email) {
        this.email = email;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowedDomains")
    public List<java.lang.String> getAllowedDomains() {
        return allowedDomains;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowedDomains")
    public void setAllowedDomains(List<java.lang.String> allowedDomains) {
        this.allowedDomains = allowedDomains;
    }

    @org.codehaus.jackson.annotate.JsonProperty("emailAliases")
    public List<java.lang.String> getEmailAliases() {
        return emailAliases;
    }

    @org.codehaus.jackson.annotate.JsonProperty("emailAliases")
    public void setEmailAliases(List<java.lang.String> emailAliases) {
        this.emailAliases = emailAliases;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cosId")
    public java.lang.String getCosId() {
        return cosId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cosId")
    public void setCosId(java.lang.String cosId) {
        this.cosId = cosId;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
