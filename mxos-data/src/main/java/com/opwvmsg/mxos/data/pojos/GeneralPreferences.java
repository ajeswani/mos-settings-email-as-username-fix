
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * GeneralPreferences object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "charset",
    "locale",
    "timezone",
    "parentalControlEnabled",
    "recycleBinEnabled",
    "preferredUserExperience",
    "userThemesAvailable",
    "preferredTheme",
    "webmailMTA"
})
public class GeneralPreferences implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("charset")
    private String charset;
    @org.codehaus.jackson.annotate.JsonProperty("locale")
    private String locale;
    @org.codehaus.jackson.annotate.JsonProperty("timezone")
    private String timezone;
    @org.codehaus.jackson.annotate.JsonProperty("parentalControlEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType parentalControlEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("recycleBinEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType recycleBinEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("preferredUserExperience")
    private com.opwvmsg.mxos.data.enums.MxosEnums.PreferredUserExperience preferredUserExperience;
    @org.codehaus.jackson.annotate.JsonProperty("userThemesAvailable")
    private List<String> userThemesAvailable;
    @org.codehaus.jackson.annotate.JsonProperty("preferredTheme")
    private String preferredTheme;
    @org.codehaus.jackson.annotate.JsonProperty("webmailMTA")
    private String webmailMTA;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("charset")
    public String getCharset() {
        return charset;
    }

    @org.codehaus.jackson.annotate.JsonProperty("charset")
    public void setCharset(String charset) {
        this.charset = charset;
    }

    @org.codehaus.jackson.annotate.JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @org.codehaus.jackson.annotate.JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    @org.codehaus.jackson.annotate.JsonProperty("timezone")
    public String getTimezone() {
        return timezone;
    }

    @org.codehaus.jackson.annotate.JsonProperty("timezone")
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @org.codehaus.jackson.annotate.JsonProperty("parentalControlEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getParentalControlEnabled() {
        return parentalControlEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("parentalControlEnabled")
    public void setParentalControlEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType parentalControlEnabled) {
        this.parentalControlEnabled = parentalControlEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("recycleBinEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getRecycleBinEnabled() {
        return recycleBinEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("recycleBinEnabled")
    public void setRecycleBinEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType recycleBinEnabled) {
        this.recycleBinEnabled = recycleBinEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("preferredUserExperience")
    public com.opwvmsg.mxos.data.enums.MxosEnums.PreferredUserExperience getPreferredUserExperience() {
        return preferredUserExperience;
    }

    @org.codehaus.jackson.annotate.JsonProperty("preferredUserExperience")
    public void setPreferredUserExperience(com.opwvmsg.mxos.data.enums.MxosEnums.PreferredUserExperience preferredUserExperience) {
        this.preferredUserExperience = preferredUserExperience;
    }

    @org.codehaus.jackson.annotate.JsonProperty("userThemesAvailable")
    public List<String> getUserThemesAvailable() {
        return userThemesAvailable;
    }

    @org.codehaus.jackson.annotate.JsonProperty("userThemesAvailable")
    public void setUserThemesAvailable(List<String> userThemesAvailable) {
        this.userThemesAvailable = userThemesAvailable;
    }

    @org.codehaus.jackson.annotate.JsonProperty("preferredTheme")
    public String getPreferredTheme() {
        return preferredTheme;
    }

    @org.codehaus.jackson.annotate.JsonProperty("preferredTheme")
    public void setPreferredTheme(String preferredTheme) {
        this.preferredTheme = preferredTheme;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailMTA")
    public String getWebmailMTA() {
        return webmailMTA;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailMTA")
    public void setWebmailMTA(String webmailMTA) {
        this.webmailMTA = webmailMTA;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
