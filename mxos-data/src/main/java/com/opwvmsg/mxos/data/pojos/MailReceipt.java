
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MailReceipt object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "receiveMessages",
    "forwardingEnabled",
    "copyOnForward",
    "forwardingAddress",
    "maxNumForwardingAddresses",
    "filterHTMLContent",
    "displayHeaders",
    "webmailDisplayWidth",
    "webmailDisplayFields",
    "maxMailsPerPage",
    "previewPaneEnabled",
    "ackReturnReceiptReq",
    "deliveryScope",
    "addressesForLocalDelivery",
    "autoReplyMode",
    "autoReplyMessage",
    "maxReceiveMessageSizeKB",
    "mobileMaxReceiveMessageSizeKB",
    "adminControl",
    "senderBlocking",
    "filters"
})
public class MailReceipt implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("receiveMessages")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType receiveMessages;
    @org.codehaus.jackson.annotate.JsonProperty("forwardingEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType forwardingEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("copyOnForward")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType copyOnForward;
    @org.codehaus.jackson.annotate.JsonProperty("forwardingAddress")
    private List<java.lang.String> forwardingAddress;
    @org.codehaus.jackson.annotate.JsonProperty("maxNumForwardingAddresses")
    private Integer maxNumForwardingAddresses;
    @org.codehaus.jackson.annotate.JsonProperty("filterHTMLContent")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType filterHTMLContent;
    @org.codehaus.jackson.annotate.JsonProperty("displayHeaders")
    private com.opwvmsg.mxos.data.enums.MxosEnums.DisplayHeadersType displayHeaders;
    @org.codehaus.jackson.annotate.JsonProperty("webmailDisplayWidth")
    private Integer webmailDisplayWidth;
    @org.codehaus.jackson.annotate.JsonProperty("webmailDisplayFields")
    private java.lang.String webmailDisplayFields;
    @org.codehaus.jackson.annotate.JsonProperty("maxMailsPerPage")
    private Integer maxMailsPerPage;
    @org.codehaus.jackson.annotate.JsonProperty("previewPaneEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType previewPaneEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("ackReturnReceiptReq")
    private MailReceipt.AckReturnReceiptReq ackReturnReceiptReq;
    @org.codehaus.jackson.annotate.JsonProperty("deliveryScope")
    private com.opwvmsg.mxos.data.enums.MxosEnums.DeliveryScope deliveryScope;
    @org.codehaus.jackson.annotate.JsonProperty("addressesForLocalDelivery")
    private List<java.lang.String> addressesForLocalDelivery;
    @org.codehaus.jackson.annotate.JsonProperty("autoReplyMode")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AutoReplyMode autoReplyMode;
    @org.codehaus.jackson.annotate.JsonProperty("autoReplyMessage")
    private java.lang.String autoReplyMessage;
    @org.codehaus.jackson.annotate.JsonProperty("maxReceiveMessageSizeKB")
    private Integer maxReceiveMessageSizeKB;
    @org.codehaus.jackson.annotate.JsonProperty("mobileMaxReceiveMessageSizeKB")
    private Integer mobileMaxReceiveMessageSizeKB;
    @org.codehaus.jackson.annotate.JsonProperty("adminControl")
    private AdminControl adminControl;
    /**
     * sender blocking filters object of mailReceipt filters
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("senderBlocking")
    private SenderBlocking senderBlocking;
    /**
     * filters object of mailReceipt
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("filters")
    private Filters filters;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("receiveMessages")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getReceiveMessages() {
        return receiveMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("receiveMessages")
    public void setReceiveMessages(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType receiveMessages) {
        this.receiveMessages = receiveMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("forwardingEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getForwardingEnabled() {
        return forwardingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("forwardingEnabled")
    public void setForwardingEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType forwardingEnabled) {
        this.forwardingEnabled = forwardingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("copyOnForward")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getCopyOnForward() {
        return copyOnForward;
    }

    @org.codehaus.jackson.annotate.JsonProperty("copyOnForward")
    public void setCopyOnForward(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType copyOnForward) {
        this.copyOnForward = copyOnForward;
    }

    @org.codehaus.jackson.annotate.JsonProperty("forwardingAddress")
    public List<java.lang.String> getForwardingAddress() {
        return forwardingAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("forwardingAddress")
    public void setForwardingAddress(List<java.lang.String> forwardingAddress) {
        this.forwardingAddress = forwardingAddress;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxNumForwardingAddresses")
    public Integer getMaxNumForwardingAddresses() {
        return maxNumForwardingAddresses;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxNumForwardingAddresses")
    public void setMaxNumForwardingAddresses(Integer maxNumForwardingAddresses) {
        this.maxNumForwardingAddresses = maxNumForwardingAddresses;
    }

    @org.codehaus.jackson.annotate.JsonProperty("filterHTMLContent")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getFilterHTMLContent() {
        return filterHTMLContent;
    }

    @org.codehaus.jackson.annotate.JsonProperty("filterHTMLContent")
    public void setFilterHTMLContent(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType filterHTMLContent) {
        this.filterHTMLContent = filterHTMLContent;
    }

    @org.codehaus.jackson.annotate.JsonProperty("displayHeaders")
    public com.opwvmsg.mxos.data.enums.MxosEnums.DisplayHeadersType getDisplayHeaders() {
        return displayHeaders;
    }

    @org.codehaus.jackson.annotate.JsonProperty("displayHeaders")
    public void setDisplayHeaders(com.opwvmsg.mxos.data.enums.MxosEnums.DisplayHeadersType displayHeaders) {
        this.displayHeaders = displayHeaders;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailDisplayWidth")
    public Integer getWebmailDisplayWidth() {
        return webmailDisplayWidth;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailDisplayWidth")
    public void setWebmailDisplayWidth(Integer webmailDisplayWidth) {
        this.webmailDisplayWidth = webmailDisplayWidth;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailDisplayFields")
    public java.lang.String getWebmailDisplayFields() {
        return webmailDisplayFields;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailDisplayFields")
    public void setWebmailDisplayFields(java.lang.String webmailDisplayFields) {
        this.webmailDisplayFields = webmailDisplayFields;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxMailsPerPage")
    public Integer getMaxMailsPerPage() {
        return maxMailsPerPage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxMailsPerPage")
    public void setMaxMailsPerPage(Integer maxMailsPerPage) {
        this.maxMailsPerPage = maxMailsPerPage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("previewPaneEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getPreviewPaneEnabled() {
        return previewPaneEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("previewPaneEnabled")
    public void setPreviewPaneEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType previewPaneEnabled) {
        this.previewPaneEnabled = previewPaneEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("ackReturnReceiptReq")
    public MailReceipt.AckReturnReceiptReq getAckReturnReceiptReq() {
        return ackReturnReceiptReq;
    }

    @org.codehaus.jackson.annotate.JsonProperty("ackReturnReceiptReq")
    public void setAckReturnReceiptReq(MailReceipt.AckReturnReceiptReq ackReturnReceiptReq) {
        this.ackReturnReceiptReq = ackReturnReceiptReq;
    }

    @org.codehaus.jackson.annotate.JsonProperty("deliveryScope")
    public com.opwvmsg.mxos.data.enums.MxosEnums.DeliveryScope getDeliveryScope() {
        return deliveryScope;
    }

    @org.codehaus.jackson.annotate.JsonProperty("deliveryScope")
    public void setDeliveryScope(com.opwvmsg.mxos.data.enums.MxosEnums.DeliveryScope deliveryScope) {
        this.deliveryScope = deliveryScope;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addressesForLocalDelivery")
    public List<java.lang.String> getAddressesForLocalDelivery() {
        return addressesForLocalDelivery;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addressesForLocalDelivery")
    public void setAddressesForLocalDelivery(List<java.lang.String> addressesForLocalDelivery) {
        this.addressesForLocalDelivery = addressesForLocalDelivery;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyMode")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AutoReplyMode getAutoReplyMode() {
        return autoReplyMode;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyMode")
    public void setAutoReplyMode(com.opwvmsg.mxos.data.enums.MxosEnums.AutoReplyMode autoReplyMode) {
        this.autoReplyMode = autoReplyMode;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyMessage")
    public java.lang.String getAutoReplyMessage() {
        return autoReplyMessage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyMessage")
    public void setAutoReplyMessage(java.lang.String autoReplyMessage) {
        this.autoReplyMessage = autoReplyMessage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxReceiveMessageSizeKB")
    public Integer getMaxReceiveMessageSizeKB() {
        return maxReceiveMessageSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxReceiveMessageSizeKB")
    public void setMaxReceiveMessageSizeKB(Integer maxReceiveMessageSizeKB) {
        this.maxReceiveMessageSizeKB = maxReceiveMessageSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mobileMaxReceiveMessageSizeKB")
    public Integer getMobileMaxReceiveMessageSizeKB() {
        return mobileMaxReceiveMessageSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mobileMaxReceiveMessageSizeKB")
    public void setMobileMaxReceiveMessageSizeKB(Integer mobileMaxReceiveMessageSizeKB) {
        this.mobileMaxReceiveMessageSizeKB = mobileMaxReceiveMessageSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminControl")
    public AdminControl getAdminControl() {
        return adminControl;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminControl")
    public void setAdminControl(AdminControl adminControl) {
        this.adminControl = adminControl;
    }

    /**
     * sender blocking filters object of mailReceipt filters
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("senderBlocking")
    public SenderBlocking getSenderBlocking() {
        return senderBlocking;
    }

    /**
     * sender blocking filters object of mailReceipt filters
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("senderBlocking")
    public void setSenderBlocking(SenderBlocking senderBlocking) {
        this.senderBlocking = senderBlocking;
    }

    /**
     * filters object of mailReceipt
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("filters")
    public Filters getFilters() {
        return filters;
    }

    /**
     * filters object of mailReceipt
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("filters")
    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum AckReturnReceiptReq {

        ALWAYS("always"),
        ASK("ask"),
        NEVER("never");
        private final java.lang.String value;

        private AckReturnReceiptReq(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static MailReceipt.AckReturnReceiptReq fromValue(java.lang.String value) {
            for (MailReceipt.AckReturnReceiptReq c: MailReceipt.AckReturnReceiptReq.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MailReceipt.AckReturnReceiptReq fromOrdinal(Integer value) {
            for (MailReceipt.AckReturnReceiptReq c: MailReceipt.AckReturnReceiptReq.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

}
