
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Base object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "userName",
    "domain",
    "firstName",
    "lastName",
    "mailboxId",
    "status",
    "lastStatusChangeDate",
    "notificationMailSentStatus",
    "type",
    "groupName",
    "groupAdmin",
    "email",
    "msisdn",
    "msisdnStatus",
    "lastMsisdnStatusChangeDate",
    "allowedDomains",
    "maxNumAllowedDomains",
    "emailAliases",
    "maxNumAliases",
    "customFields",
    "extensionAttributes",
    "cosId"
})
public class Base implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("userName")
    private java.lang.String userName;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    private java.lang.String domain;
    @org.codehaus.jackson.annotate.JsonProperty("firstName")
    private java.lang.String firstName;
    @org.codehaus.jackson.annotate.JsonProperty("lastName")
    private java.lang.String lastName;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    private Long mailboxId;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    private com.opwvmsg.mxos.data.enums.MxosEnums.Status status;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastStatusChangeDate")
    private Object lastStatusChangeDate;
    @org.codehaus.jackson.annotate.JsonProperty("notificationMailSentStatus")
    private com.opwvmsg.mxos.data.enums.MxosEnums.NotificationMailSentStatus notificationMailSentStatus;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    private com.opwvmsg.mxos.data.enums.MxosEnums.Type type;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("groupName")
    private java.lang.String groupName;
    @org.codehaus.jackson.annotate.JsonProperty("groupAdmin")
    private List<java.lang.String> groupAdmin;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    private java.lang.String email;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("msisdn")
    private java.lang.String msisdn;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("msisdnStatus")
    private com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus msisdnStatus;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastMsisdnStatusChangeDate")
    private Object lastMsisdnStatusChangeDate;
    @org.codehaus.jackson.annotate.JsonProperty("allowedDomains")
    private List<java.lang.String> allowedDomains;
    @org.codehaus.jackson.annotate.JsonProperty("maxNumAllowedDomains")
    private Integer maxNumAllowedDomains;
    @org.codehaus.jackson.annotate.JsonProperty("emailAliases")
    private List<java.lang.String> emailAliases;
    @org.codehaus.jackson.annotate.JsonProperty("maxNumAliases")
    private Integer maxNumAliases;
    @org.codehaus.jackson.annotate.JsonProperty("customFields")
    private java.lang.String customFields;
    @org.codehaus.jackson.annotate.JsonProperty("extensionAttributes")
    private java.lang.String extensionAttributes;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cosId")
    private java.lang.String cosId;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("userName")
    public java.lang.String getUserName() {
        return userName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("userName")
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    public java.lang.String getDomain() {
        return domain;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    public void setDomain(java.lang.String domain) {
        this.domain = domain;
    }

    @org.codehaus.jackson.annotate.JsonProperty("firstName")
    public java.lang.String getFirstName() {
        return firstName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("firstName")
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastName")
    public java.lang.String getLastName() {
        return lastName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastName")
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    public Long getMailboxId() {
        return mailboxId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    public void setMailboxId(Long mailboxId) {
        this.mailboxId = mailboxId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    public com.opwvmsg.mxos.data.enums.MxosEnums.Status getStatus() {
        return status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("status")
    public void setStatus(com.opwvmsg.mxos.data.enums.MxosEnums.Status status) {
        this.status = status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastStatusChangeDate")
    public Object getLastStatusChangeDate() {
        return lastStatusChangeDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastStatusChangeDate")
    public void setLastStatusChangeDate(Object lastStatusChangeDate) {
        this.lastStatusChangeDate = lastStatusChangeDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("notificationMailSentStatus")
    public com.opwvmsg.mxos.data.enums.MxosEnums.NotificationMailSentStatus getNotificationMailSentStatus() {
        return notificationMailSentStatus;
    }

    @org.codehaus.jackson.annotate.JsonProperty("notificationMailSentStatus")
    public void setNotificationMailSentStatus(com.opwvmsg.mxos.data.enums.MxosEnums.NotificationMailSentStatus notificationMailSentStatus) {
        this.notificationMailSentStatus = notificationMailSentStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public com.opwvmsg.mxos.data.enums.MxosEnums.Type getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public void setType(com.opwvmsg.mxos.data.enums.MxosEnums.Type type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("groupName")
    public java.lang.String getGroupName() {
        return groupName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("groupName")
    public void setGroupName(java.lang.String groupName) {
        this.groupName = groupName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("groupAdmin")
    public List<java.lang.String> getGroupAdmin() {
        return groupAdmin;
    }

    @org.codehaus.jackson.annotate.JsonProperty("groupAdmin")
    public void setGroupAdmin(List<java.lang.String> groupAdmin) {
        this.groupAdmin = groupAdmin;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    public java.lang.String getEmail() {
        return email;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    public void setEmail(java.lang.String email) {
        this.email = email;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("msisdn")
    public java.lang.String getMsisdn() {
        return msisdn;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("msisdn")
    public void setMsisdn(java.lang.String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("msisdnStatus")
    public com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus getMsisdnStatus() {
        return msisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("msisdnStatus")
    public void setMsisdnStatus(com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus msisdnStatus) {
        this.msisdnStatus = msisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastMsisdnStatusChangeDate")
    public Object getLastMsisdnStatusChangeDate() {
        return lastMsisdnStatusChangeDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastMsisdnStatusChangeDate")
    public void setLastMsisdnStatusChangeDate(Object lastMsisdnStatusChangeDate) {
        this.lastMsisdnStatusChangeDate = lastMsisdnStatusChangeDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowedDomains")
    public List<java.lang.String> getAllowedDomains() {
        return allowedDomains;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowedDomains")
    public void setAllowedDomains(List<java.lang.String> allowedDomains) {
        this.allowedDomains = allowedDomains;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxNumAllowedDomains")
    public Integer getMaxNumAllowedDomains() {
        return maxNumAllowedDomains;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxNumAllowedDomains")
    public void setMaxNumAllowedDomains(Integer maxNumAllowedDomains) {
        this.maxNumAllowedDomains = maxNumAllowedDomains;
    }

    @org.codehaus.jackson.annotate.JsonProperty("emailAliases")
    public List<java.lang.String> getEmailAliases() {
        return emailAliases;
    }

    @org.codehaus.jackson.annotate.JsonProperty("emailAliases")
    public void setEmailAliases(List<java.lang.String> emailAliases) {
        this.emailAliases = emailAliases;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxNumAliases")
    public Integer getMaxNumAliases() {
        return maxNumAliases;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxNumAliases")
    public void setMaxNumAliases(Integer maxNumAliases) {
        this.maxNumAliases = maxNumAliases;
    }

    @org.codehaus.jackson.annotate.JsonProperty("customFields")
    public java.lang.String getCustomFields() {
        return customFields;
    }

    @org.codehaus.jackson.annotate.JsonProperty("customFields")
    public void setCustomFields(java.lang.String customFields) {
        this.customFields = customFields;
    }

    @org.codehaus.jackson.annotate.JsonProperty("extensionAttributes")
    public java.lang.String getExtensionAttributes() {
        return extensionAttributes;
    }

    @org.codehaus.jackson.annotate.JsonProperty("extensionAttributes")
    public void setExtensionAttributes(java.lang.String extensionAttributes) {
        this.extensionAttributes = extensionAttributes;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cosId")
    public java.lang.String getCosId() {
        return cosId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("cosId")
    public void setCosId(java.lang.String cosId) {
        this.cosId = cosId;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
