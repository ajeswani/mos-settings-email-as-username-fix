
package com.opwvmsg.mxos.data.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Base object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "entity",
    "logEventType",
    "domainType",
    "status",
    "msisdnStatus",
    "type",
    "accessType",
    "intAccessType",
    "booleanType",
    "preferredUserExperience",
    "passwordRecoveryPreference",
    "autoReplyMode",
    "blockedSenderAction",
    "deliveryScope",
    "bmiSpamType",
    "cloudmarkActionType",
    "commtouchActionType",
    "mcAfeeVirusActionType",
    "spamPolicy",
    "rejectActionType",
    "smsServicesMsisdnStatusType",
    "displayHeadersType",
    "trueOrFalse",
    "addressBookProviderType",
    "eventRecordingType",
    "signatureInReplyType",
    "accountType",
    "maritalStatus",
    "passwordStoreType",
    "notificationMailSentStatus",
    "createContactsFromOutgoingEmails",
    "adminRejectActionFlag",
    "adminDefaultDispositionAction"
})
public class MxosEnums implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("entity")
    private MxosEnums.Entity entity;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("logEventType")
    private MxosEnums.LogEventType logEventType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("domainType")
    private MxosEnums.DomainType domainType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    private MxosEnums.Status status;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("msisdnStatus")
    private MxosEnums.MsisdnStatus msisdnStatus;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private MxosEnums.Type type;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accessType")
    private MxosEnums.AccessType accessType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("intAccessType")
    private MxosEnums.IntAccessType intAccessType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("booleanType")
    private MxosEnums.BooleanType booleanType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("preferredUserExperience")
    private MxosEnums.PreferredUserExperience preferredUserExperience;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("passwordRecoveryPreference")
    private MxosEnums.PasswordRecoveryPreference passwordRecoveryPreference;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("autoReplyMode")
    private MxosEnums.AutoReplyMode autoReplyMode;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("blockedSenderAction")
    private MxosEnums.BlockedSenderAction blockedSenderAction;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deliveryScope")
    private MxosEnums.DeliveryScope deliveryScope;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("bmiSpamType")
    private MxosEnums.BmiSpamType bmiSpamType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cloudmarkActionType")
    private MxosEnums.CloudmarkActionType cloudmarkActionType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("commtouchActionType")
    private MxosEnums.CommtouchActionType commtouchActionType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("mcAfeeVirusActionType")
    private MxosEnums.McAfeeVirusActionType mcAfeeVirusActionType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("spamPolicy")
    private MxosEnums.SpamPolicy spamPolicy;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("rejectActionType")
    private MxosEnums.RejectActionType rejectActionType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("smsServicesMsisdnStatusType")
    private MxosEnums.SmsServicesMsisdnStatusType smsServicesMsisdnStatusType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("displayHeadersType")
    private MxosEnums.DisplayHeadersType displayHeadersType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("trueOrFalse")
    private MxosEnums.TrueOrFalse trueOrFalse;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("addressBookProviderType")
    private MxosEnums.AddressBookProviderType addressBookProviderType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("eventRecordingType")
    private MxosEnums.EventRecordingType eventRecordingType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("signatureInReplyType")
    private MxosEnums.SignatureInReplyType signatureInReplyType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accountType")
    private MxosEnums.AccountType accountType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("maritalStatus")
    private MxosEnums.MaritalStatus maritalStatus;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("passwordStoreType")
    private MxosEnums.PasswordStoreType passwordStoreType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("notificationMailSentStatus")
    private MxosEnums.NotificationMailSentStatus notificationMailSentStatus;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("createContactsFromOutgoingEmails")
    private MxosEnums.CreateContactsFromOutgoingEmails createContactsFromOutgoingEmails;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("adminRejectActionFlag")
    private MxosEnums.AdminRejectActionFlag adminRejectActionFlag;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("adminDefaultDispositionAction")
    private MxosEnums.AdminDefaultDispositionAction adminDefaultDispositionAction;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("entity")
    public MxosEnums.Entity getEntity() {
        return entity;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("entity")
    public void setEntity(MxosEnums.Entity entity) {
        this.entity = entity;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("logEventType")
    public MxosEnums.LogEventType getLogEventType() {
        return logEventType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("logEventType")
    public void setLogEventType(MxosEnums.LogEventType logEventType) {
        this.logEventType = logEventType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("domainType")
    public MxosEnums.DomainType getDomainType() {
        return domainType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("domainType")
    public void setDomainType(MxosEnums.DomainType domainType) {
        this.domainType = domainType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public MxosEnums.Status getStatus() {
        return status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    public void setStatus(MxosEnums.Status status) {
        this.status = status;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("msisdnStatus")
    public MxosEnums.MsisdnStatus getMsisdnStatus() {
        return msisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("msisdnStatus")
    public void setMsisdnStatus(MxosEnums.MsisdnStatus msisdnStatus) {
        this.msisdnStatus = msisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public MxosEnums.Type getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(MxosEnums.Type type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accessType")
    public MxosEnums.AccessType getAccessType() {
        return accessType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accessType")
    public void setAccessType(MxosEnums.AccessType accessType) {
        this.accessType = accessType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("intAccessType")
    public MxosEnums.IntAccessType getIntAccessType() {
        return intAccessType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("intAccessType")
    public void setIntAccessType(MxosEnums.IntAccessType intAccessType) {
        this.intAccessType = intAccessType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("booleanType")
    public MxosEnums.BooleanType getBooleanType() {
        return booleanType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("booleanType")
    public void setBooleanType(MxosEnums.BooleanType booleanType) {
        this.booleanType = booleanType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("preferredUserExperience")
    public MxosEnums.PreferredUserExperience getPreferredUserExperience() {
        return preferredUserExperience;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("preferredUserExperience")
    public void setPreferredUserExperience(MxosEnums.PreferredUserExperience preferredUserExperience) {
        this.preferredUserExperience = preferredUserExperience;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("passwordRecoveryPreference")
    public MxosEnums.PasswordRecoveryPreference getPasswordRecoveryPreference() {
        return passwordRecoveryPreference;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("passwordRecoveryPreference")
    public void setPasswordRecoveryPreference(MxosEnums.PasswordRecoveryPreference passwordRecoveryPreference) {
        this.passwordRecoveryPreference = passwordRecoveryPreference;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("autoReplyMode")
    public MxosEnums.AutoReplyMode getAutoReplyMode() {
        return autoReplyMode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("autoReplyMode")
    public void setAutoReplyMode(MxosEnums.AutoReplyMode autoReplyMode) {
        this.autoReplyMode = autoReplyMode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("blockedSenderAction")
    public MxosEnums.BlockedSenderAction getBlockedSenderAction() {
        return blockedSenderAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("blockedSenderAction")
    public void setBlockedSenderAction(MxosEnums.BlockedSenderAction blockedSenderAction) {
        this.blockedSenderAction = blockedSenderAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deliveryScope")
    public MxosEnums.DeliveryScope getDeliveryScope() {
        return deliveryScope;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deliveryScope")
    public void setDeliveryScope(MxosEnums.DeliveryScope deliveryScope) {
        this.deliveryScope = deliveryScope;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("bmiSpamType")
    public MxosEnums.BmiSpamType getBmiSpamType() {
        return bmiSpamType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("bmiSpamType")
    public void setBmiSpamType(MxosEnums.BmiSpamType bmiSpamType) {
        this.bmiSpamType = bmiSpamType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cloudmarkActionType")
    public MxosEnums.CloudmarkActionType getCloudmarkActionType() {
        return cloudmarkActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cloudmarkActionType")
    public void setCloudmarkActionType(MxosEnums.CloudmarkActionType cloudmarkActionType) {
        this.cloudmarkActionType = cloudmarkActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("commtouchActionType")
    public MxosEnums.CommtouchActionType getCommtouchActionType() {
        return commtouchActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("commtouchActionType")
    public void setCommtouchActionType(MxosEnums.CommtouchActionType commtouchActionType) {
        this.commtouchActionType = commtouchActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("mcAfeeVirusActionType")
    public MxosEnums.McAfeeVirusActionType getMcAfeeVirusActionType() {
        return mcAfeeVirusActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("mcAfeeVirusActionType")
    public void setMcAfeeVirusActionType(MxosEnums.McAfeeVirusActionType mcAfeeVirusActionType) {
        this.mcAfeeVirusActionType = mcAfeeVirusActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("spamPolicy")
    public MxosEnums.SpamPolicy getSpamPolicy() {
        return spamPolicy;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("spamPolicy")
    public void setSpamPolicy(MxosEnums.SpamPolicy spamPolicy) {
        this.spamPolicy = spamPolicy;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("rejectActionType")
    public MxosEnums.RejectActionType getRejectActionType() {
        return rejectActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("rejectActionType")
    public void setRejectActionType(MxosEnums.RejectActionType rejectActionType) {
        this.rejectActionType = rejectActionType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("smsServicesMsisdnStatusType")
    public MxosEnums.SmsServicesMsisdnStatusType getSmsServicesMsisdnStatusType() {
        return smsServicesMsisdnStatusType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("smsServicesMsisdnStatusType")
    public void setSmsServicesMsisdnStatusType(MxosEnums.SmsServicesMsisdnStatusType smsServicesMsisdnStatusType) {
        this.smsServicesMsisdnStatusType = smsServicesMsisdnStatusType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("displayHeadersType")
    public MxosEnums.DisplayHeadersType getDisplayHeadersType() {
        return displayHeadersType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("displayHeadersType")
    public void setDisplayHeadersType(MxosEnums.DisplayHeadersType displayHeadersType) {
        this.displayHeadersType = displayHeadersType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("trueOrFalse")
    public MxosEnums.TrueOrFalse getTrueOrFalse() {
        return trueOrFalse;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("trueOrFalse")
    public void setTrueOrFalse(MxosEnums.TrueOrFalse trueOrFalse) {
        this.trueOrFalse = trueOrFalse;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("addressBookProviderType")
    public MxosEnums.AddressBookProviderType getAddressBookProviderType() {
        return addressBookProviderType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("addressBookProviderType")
    public void setAddressBookProviderType(MxosEnums.AddressBookProviderType addressBookProviderType) {
        this.addressBookProviderType = addressBookProviderType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("eventRecordingType")
    public MxosEnums.EventRecordingType getEventRecordingType() {
        return eventRecordingType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("eventRecordingType")
    public void setEventRecordingType(MxosEnums.EventRecordingType eventRecordingType) {
        this.eventRecordingType = eventRecordingType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("signatureInReplyType")
    public MxosEnums.SignatureInReplyType getSignatureInReplyType() {
        return signatureInReplyType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("signatureInReplyType")
    public void setSignatureInReplyType(MxosEnums.SignatureInReplyType signatureInReplyType) {
        this.signatureInReplyType = signatureInReplyType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accountType")
    public MxosEnums.AccountType getAccountType() {
        return accountType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accountType")
    public void setAccountType(MxosEnums.AccountType accountType) {
        this.accountType = accountType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("maritalStatus")
    public MxosEnums.MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("maritalStatus")
    public void setMaritalStatus(MxosEnums.MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("passwordStoreType")
    public MxosEnums.PasswordStoreType getPasswordStoreType() {
        return passwordStoreType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("passwordStoreType")
    public void setPasswordStoreType(MxosEnums.PasswordStoreType passwordStoreType) {
        this.passwordStoreType = passwordStoreType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("notificationMailSentStatus")
    public MxosEnums.NotificationMailSentStatus getNotificationMailSentStatus() {
        return notificationMailSentStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("notificationMailSentStatus")
    public void setNotificationMailSentStatus(MxosEnums.NotificationMailSentStatus notificationMailSentStatus) {
        this.notificationMailSentStatus = notificationMailSentStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("createContactsFromOutgoingEmails")
    public MxosEnums.CreateContactsFromOutgoingEmails getCreateContactsFromOutgoingEmails() {
        return createContactsFromOutgoingEmails;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("createContactsFromOutgoingEmails")
    public void setCreateContactsFromOutgoingEmails(MxosEnums.CreateContactsFromOutgoingEmails createContactsFromOutgoingEmails) {
        this.createContactsFromOutgoingEmails = createContactsFromOutgoingEmails;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("adminRejectActionFlag")
    public MxosEnums.AdminRejectActionFlag getAdminRejectActionFlag() {
        return adminRejectActionFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("adminRejectActionFlag")
    public void setAdminRejectActionFlag(MxosEnums.AdminRejectActionFlag adminRejectActionFlag) {
        this.adminRejectActionFlag = adminRejectActionFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("adminDefaultDispositionAction")
    public MxosEnums.AdminDefaultDispositionAction getAdminDefaultDispositionAction() {
        return adminDefaultDispositionAction;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("adminDefaultDispositionAction")
    public void setAdminDefaultDispositionAction(MxosEnums.AdminDefaultDispositionAction adminDefaultDispositionAction) {
        this.adminDefaultDispositionAction = adminDefaultDispositionAction;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum AccessType {

        ALL("all"),
        TRUSTED("trusted"),
        NONE("none");
        private final String value;

        private AccessType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.AccessType fromValue(String value) {
            for (MxosEnums.AccessType c: MxosEnums.AccessType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.AccessType fromOrdinal(Integer value) {
            for (MxosEnums.AccessType c: MxosEnums.AccessType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum AccountType {

        POP_3("pop3"),
        POP_3_SSL("pop3ssl"),
        IMAP("imap");
        private final String value;

        private AccountType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.AccountType fromValue(String value) {
            for (MxosEnums.AccountType c: MxosEnums.AccountType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.AccountType fromOrdinal(Integer value) {
            for (MxosEnums.AccountType c: MxosEnums.AccountType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum AddressBookProviderType {

        MSS("mss"),
        PLAXO("plaxo"),
        SUN("sun"),
        VOX("vox");
        private final String value;

        private AddressBookProviderType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.AddressBookProviderType fromValue(String value) {
            for (MxosEnums.AddressBookProviderType c: MxosEnums.AddressBookProviderType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.AddressBookProviderType fromOrdinal(Integer value) {
            for (MxosEnums.AddressBookProviderType c: MxosEnums.AddressBookProviderType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum AdminDefaultDispositionAction {

        ACCEPT("accept"),
        REJECT("reject");
        private final String value;

        private AdminDefaultDispositionAction(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.AdminDefaultDispositionAction fromValue(String value) {
            for (MxosEnums.AdminDefaultDispositionAction c: MxosEnums.AdminDefaultDispositionAction.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.AdminDefaultDispositionAction fromOrdinal(Integer value) {
            for (MxosEnums.AdminDefaultDispositionAction c: MxosEnums.AdminDefaultDispositionAction.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum AdminRejectActionFlag {

        DROP("drop"),
        FORWARD("forward");
        private final String value;

        private AdminRejectActionFlag(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.AdminRejectActionFlag fromValue(String value) {
            for (MxosEnums.AdminRejectActionFlag c: MxosEnums.AdminRejectActionFlag.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.AdminRejectActionFlag fromOrdinal(Integer value) {
            for (MxosEnums.AdminRejectActionFlag c: MxosEnums.AdminRejectActionFlag.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum AutoReplyMode {

        NONE("none"),
        VACATION("vacation"),
        REPLY("reply"),
        ECHO("echo");
        private final String value;

        private AutoReplyMode(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.AutoReplyMode fromValue(String value) {
            for (MxosEnums.AutoReplyMode c: MxosEnums.AutoReplyMode.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.AutoReplyMode fromOrdinal(Integer value) {
            for (MxosEnums.AutoReplyMode c: MxosEnums.AutoReplyMode.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum BlockedSenderAction {

        BOUNCE("bounce"),
        REJECT("reject"),
        SIDELINE("sideline"),
        TOSS("toss"),
        NONE("none");
        private final String value;

        private BlockedSenderAction(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.BlockedSenderAction fromValue(String value) {
            for (MxosEnums.BlockedSenderAction c: MxosEnums.BlockedSenderAction.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.BlockedSenderAction fromOrdinal(Integer value) {
            for (MxosEnums.BlockedSenderAction c: MxosEnums.BlockedSenderAction.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum BmiSpamType {

        DISCARD("discard"),
        NONE("none"),
        FOLDER_DELIVER("folder deliver"),
        ADD_HEADER("add header"),
        SUBJECT_PREFIX("subject prefix");
        private final String value;

        private BmiSpamType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.BmiSpamType fromValue(String value) {
            for (MxosEnums.BmiSpamType c: MxosEnums.BmiSpamType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.BmiSpamType fromOrdinal(Integer value) {
            for (MxosEnums.BmiSpamType c: MxosEnums.BmiSpamType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum BooleanType {

        NO("no"),
        YES("yes");
        private final String value;

        private BooleanType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.BooleanType fromValue(String value) {
            for (MxosEnums.BooleanType c: MxosEnums.BooleanType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.BooleanType fromOrdinal(Integer value) {
            for (MxosEnums.BooleanType c: MxosEnums.BooleanType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum CloudmarkActionType {

        ACCEPT("accept"),
        DELETE("delete"),
        QUARANTINE("quarantine"),
        TAG("tag"),
        HEADER("header");
        private final String value;

        private CloudmarkActionType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.CloudmarkActionType fromValue(String value) {
            for (MxosEnums.CloudmarkActionType c: MxosEnums.CloudmarkActionType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.CloudmarkActionType fromOrdinal(Integer value) {
            for (MxosEnums.CloudmarkActionType c: MxosEnums.CloudmarkActionType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum CommtouchActionType {

        DISCARD("discard"),
        ALLOW("allow"),
        REJECT("reject"),
        TAG("tag");
        private final String value;

        private CommtouchActionType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.CommtouchActionType fromValue(String value) {
            for (MxosEnums.CommtouchActionType c: MxosEnums.CommtouchActionType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.CommtouchActionType fromOrdinal(Integer value) {
            for (MxosEnums.CommtouchActionType c: MxosEnums.CommtouchActionType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum CreateContactsFromOutgoingEmails {

        DISABLED("disabled"),
        FOR_ALL_ADDRESSES("forAllAddresses"),
        ONLY_FOR_ADDRESSES_IN_TO_FIELD("onlyForAddressesInToField");
        private final String value;

        private CreateContactsFromOutgoingEmails(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.CreateContactsFromOutgoingEmails fromValue(String value) {
            for (MxosEnums.CreateContactsFromOutgoingEmails c: MxosEnums.CreateContactsFromOutgoingEmails.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.CreateContactsFromOutgoingEmails fromOrdinal(Integer value) {
            for (MxosEnums.CreateContactsFromOutgoingEmails c: MxosEnums.CreateContactsFromOutgoingEmails.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum DeliveryScope {

        LOCAL("local"),
        GLOBAL("global");
        private final String value;

        private DeliveryScope(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.DeliveryScope fromValue(String value) {
            for (MxosEnums.DeliveryScope c: MxosEnums.DeliveryScope.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.DeliveryScope fromOrdinal(Integer value) {
            for (MxosEnums.DeliveryScope c: MxosEnums.DeliveryScope.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum DisplayHeadersType {

        HTML("html"),
        TEXT("text");
        private final String value;

        private DisplayHeadersType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.DisplayHeadersType fromValue(String value) {
            for (MxosEnums.DisplayHeadersType c: MxosEnums.DisplayHeadersType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.DisplayHeadersType fromOrdinal(Integer value) {
            for (MxosEnums.DisplayHeadersType c: MxosEnums.DisplayHeadersType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum DomainType {

        LOCAL("local"),
        NONAUTH("nonauth"),
        REWRITE("rewrite");
        private final String value;

        private DomainType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.DomainType fromValue(String value) {
            for (MxosEnums.DomainType c: MxosEnums.DomainType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.DomainType fromOrdinal(Integer value) {
            for (MxosEnums.DomainType c: MxosEnums.DomainType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum Entity {

        ADDRESS_BOOK("addressBook"),
        TASKS("tasks");
        private final String value;

        private Entity(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.Entity fromValue(String value) {
            for (MxosEnums.Entity c: MxosEnums.Entity.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.Entity fromOrdinal(Integer value) {
            for (MxosEnums.Entity c: MxosEnums.Entity.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum EventRecordingType {

        ALWAYS("always"),
        TRUE("true"),
        FALSE("false"),
        NEVER("never");
        private final String value;

        private EventRecordingType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.EventRecordingType fromValue(String value) {
            for (MxosEnums.EventRecordingType c: MxosEnums.EventRecordingType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.EventRecordingType fromOrdinal(Integer value) {
            for (MxosEnums.EventRecordingType c: MxosEnums.EventRecordingType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum IntAccessType {

        _0("0"),
        _1("1"),
        _2("2");
        private final String value;

        private IntAccessType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.IntAccessType fromValue(String value) {
            for (MxosEnums.IntAccessType c: MxosEnums.IntAccessType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.IntAccessType fromOrdinal(Integer value) {
            for (MxosEnums.IntAccessType c: MxosEnums.IntAccessType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum LogEventType {

        C("C"),
        R("R"),
        U("U"),
        D("D"),
        I("I");
        private final String value;

        private LogEventType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.LogEventType fromValue(String value) {
            for (MxosEnums.LogEventType c: MxosEnums.LogEventType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.LogEventType fromOrdinal(Integer value) {
            for (MxosEnums.LogEventType c: MxosEnums.LogEventType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum MaritalStatus {

        MARRIED("Married"),
        SINGLE("Single"),
        DIVORCEE("Divorcee");
        private final String value;

        private MaritalStatus(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.MaritalStatus fromValue(String value) {
            for (MxosEnums.MaritalStatus c: MxosEnums.MaritalStatus.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.MaritalStatus fromOrdinal(Integer value) {
            for (MxosEnums.MaritalStatus c: MxosEnums.MaritalStatus.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum McAfeeVirusActionType {

        ALLOW("allow"),
        CLEAN("clean"),
        DISCARD("discard"),
        REJECT("reject"),
        REPAIR("repair");
        private final String value;

        private McAfeeVirusActionType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.McAfeeVirusActionType fromValue(String value) {
            for (MxosEnums.McAfeeVirusActionType c: MxosEnums.McAfeeVirusActionType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.McAfeeVirusActionType fromOrdinal(Integer value) {
            for (MxosEnums.McAfeeVirusActionType c: MxosEnums.McAfeeVirusActionType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum MsisdnStatus {

        DEACTIVATED("deactivated"),
        ACTIVATED("activated");
        private final String value;

        private MsisdnStatus(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.MsisdnStatus fromValue(String value) {
            for (MxosEnums.MsisdnStatus c: MxosEnums.MsisdnStatus.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.MsisdnStatus fromOrdinal(Integer value) {
            for (MxosEnums.MsisdnStatus c: MxosEnums.MsisdnStatus.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum NotificationMailSentStatus {

        NOT_SENT("notSent"),
        SENT_FOR_INACTIVE_STATUS("sentForInactiveStatus"),
        SENT_BEFORE_SUSPENDED_STATUS("sentBeforeSuspendedStatus"),
        SENT_FOR_SUSPENDED_STATUS("sentForSuspendedStatus"),
        SENT_AFTER_SUSPENDED_STATUS("sentAfterSuspendedStatus"),
        SENT_BEFORE_DELETED_STATUS("sentBeforeDeletedStatus");
        private final String value;

        private NotificationMailSentStatus(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.NotificationMailSentStatus fromValue(String value) {
            for (MxosEnums.NotificationMailSentStatus c: MxosEnums.NotificationMailSentStatus.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.NotificationMailSentStatus fromOrdinal(Integer value) {
            for (MxosEnums.NotificationMailSentStatus c: MxosEnums.NotificationMailSentStatus.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum PasswordRecoveryPreference {

        NONE("none"),
        EMAIL("email"),
        SMS("sms"),
        ALL("all");
        private final String value;

        private PasswordRecoveryPreference(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.PasswordRecoveryPreference fromValue(String value) {
            for (MxosEnums.PasswordRecoveryPreference c: MxosEnums.PasswordRecoveryPreference.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.PasswordRecoveryPreference fromOrdinal(Integer value) {
            for (MxosEnums.PasswordRecoveryPreference c: MxosEnums.PasswordRecoveryPreference.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum PasswordStoreType {

        CLEAR("clear"),
        UNIX("unix"),
        SSHA_1("ssha1"),
        MD_5_PO("md5-po"),
        SHA_1("sha1"),
        CUSTOM_1("custom1"),
        CUSTOM_2("custom2"),
        CUSTOM_3("custom3"),
        CUSTOM_4("custom4"),
        CUSTOM_5("custom5"),
        BCRYPT("bcrypt");
        private final String value;

        private PasswordStoreType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.PasswordStoreType fromValue(String value) {
            for (MxosEnums.PasswordStoreType c: MxosEnums.PasswordStoreType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.PasswordStoreType fromOrdinal(Integer value) {
            for (MxosEnums.PasswordStoreType c: MxosEnums.PasswordStoreType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum PreferredUserExperience {

        BASIC("basic"),
        RICH("rich"),
        MOBILE("mobile"),
        TABLET("tablet");
        private final String value;

        private PreferredUserExperience(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.PreferredUserExperience fromValue(String value) {
            for (MxosEnums.PreferredUserExperience c: MxosEnums.PreferredUserExperience.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.PreferredUserExperience fromOrdinal(Integer value) {
            for (MxosEnums.PreferredUserExperience c: MxosEnums.PreferredUserExperience.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum RejectActionType {

        DROP("drop"),
        FORWARD("forward");
        private final String value;

        private RejectActionType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.RejectActionType fromValue(String value) {
            for (MxosEnums.RejectActionType c: MxosEnums.RejectActionType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.RejectActionType fromOrdinal(Integer value) {
            for (MxosEnums.RejectActionType c: MxosEnums.RejectActionType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum SignatureInReplyType {

        AFTER_ORIGINAL_MESSAGE("afterOriginalMessage"),
        BEFORE_ORIGINAL_MESSAGE("beforeOriginalMessage");
        private final String value;

        private SignatureInReplyType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.SignatureInReplyType fromValue(String value) {
            for (MxosEnums.SignatureInReplyType c: MxosEnums.SignatureInReplyType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.SignatureInReplyType fromOrdinal(Integer value) {
            for (MxosEnums.SignatureInReplyType c: MxosEnums.SignatureInReplyType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum SmsServicesMsisdnStatusType {

        DEACTIVATED("deactivated"),
        ACTIVATED("activated");
        private final String value;

        private SmsServicesMsisdnStatusType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.SmsServicesMsisdnStatusType fromValue(String value) {
            for (MxosEnums.SmsServicesMsisdnStatusType c: MxosEnums.SmsServicesMsisdnStatusType.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.SmsServicesMsisdnStatusType fromOrdinal(Integer value) {
            for (MxosEnums.SmsServicesMsisdnStatusType c: MxosEnums.SmsServicesMsisdnStatusType.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum SpamPolicy {

        MILD("mild"),
        MODERATE("moderate"),
        AGGRESSIVE("aggressive");
        private final String value;

        private SpamPolicy(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.SpamPolicy fromValue(String value) {
            for (MxosEnums.SpamPolicy c: MxosEnums.SpamPolicy.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.SpamPolicy fromOrdinal(Integer value) {
            for (MxosEnums.SpamPolicy c: MxosEnums.SpamPolicy.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum Status {

        OPEN("open"),
        CLOSING("closing"),
        LOCKED("locked"),
        CLOSED("closed"),
        PENDING("pending"),
        INACTIVE("inactive"),
        ACTIVE("active"),
        MAINTENANCE("maintenance"),
        SUSPENDED("suspended"),
        DELETED("deleted"),
        PROXY("proxy");
        private final String value;

        private Status(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.Status fromValue(String value) {
            for (MxosEnums.Status c: MxosEnums.Status.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.Status fromOrdinal(Integer value) {
            for (MxosEnums.Status c: MxosEnums.Status.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum TrueOrFalse {

        FALSE("false"),
        TRUE("true");
        private final String value;

        private TrueOrFalse(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.TrueOrFalse fromValue(String value) {
            for (MxosEnums.TrueOrFalse c: MxosEnums.TrueOrFalse.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.TrueOrFalse fromOrdinal(Integer value) {
            for (MxosEnums.TrueOrFalse c: MxosEnums.TrueOrFalse.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum Type {

        NORMAL("normal"),
        GROUP_ADMIN("groupAdmin"),
        GROUP_MAILBOX("groupMailbox"),
        MAILING_LIST_SUBSCRIBER("mailingListSubscriber"),
        MAILING_LIST("mailingList"),
        FORWARD_ONLY("forwardOnly");
        private final String value;

        private Type(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static MxosEnums.Type fromValue(String value) {
            for (MxosEnums.Type c: MxosEnums.Type.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static MxosEnums.Type fromOrdinal(Integer value) {
            for (MxosEnums.Type c: MxosEnums.Type.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

}
