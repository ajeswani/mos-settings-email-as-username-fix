/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All the config keys configured mxos.properties.
 *
 * @author mxos-dev
 */
public enum SystemProperty implements DataMap.Property {
    configHost,
    configPort,
    defaultHost,
    defaultApp,
    defaultSearchApp,
    masterServerRoleEnabled,
    deploymentMode,
    configConnectAttempts,
    configConnectInterval,
    userDateFormat,
    loadStubContextOnConfigError,
    createMssMailboxOnProxyStatus,
    maxMailAccessAllowedIps,
    maxAddAddressesForLocalDelivery,
    maxAddAllowedSendersList,
    maxAddBlockedSendersList,
    maxAddSieveBlockedSenders,
    ldapConnectionValidationWithPinging,
    ldapReadMailboxFilter,
    ldapCosObjectClasses,
    ldapCosBaseDn,
    ldapConnRetryInterval,
    ldapConnectTimeout,
    ldapPoolMaxSize,
    loadServices,
    loadExternalLdapPool,
    loadRulesOrder,
    domainMigrationInProgress,
    migratedDomains,
    megMaxConnections,
    megMySqlConnectionURL,
    megMySqlUser,
    megMySqlPassword,
    groupMailboxEnabled,
    getMailboxReturnLdapAttributes,
    mssVersion,
    storeUserNameAsEmail,
    returnUserNameWithDomain,
    messageFlags,
    createMessageIsPrivateMsg,
    createMessageAccessId,
    createMessageLegacyRMEOptions,
    createMessageSLRMEOptions,
    createMessageFolderId,
    createMessageExpireTime,
    legalUsername,
    legalPeerIp,
    createMessageFromAddress,
    loggingGateway,
    sendMailGateway,
    defaultSocialNetworkSiteAccessEnabled,
    resetFailedLoginAttemptsOnAuthSuccessOfLockedAccount,
    updateMSSAfterAuthenticationEnabled,
    advancedAuthenticationEnabled,
    badPasswordWindowEnabled,
    badPasswordDelayEnabled,
    checkLockedAccountEnabled,
    regexValidationTimeout,


    maaPoolInitSize,
    maaPoolMaxSize,
    maaTimeoutMS,
    maaEnabled,

    readMessageOffset,
    readMessageLength,
    readMessageLAUsername,
    readMessageLAPeerIp,

    jmxEnabled,
    requestMonitoringEnabled,
    mxosHost,
    mxosPort, 
    snmpEnabled, 
    incomingRequestMonitoringEnabled, 
    defaultPreEncyrptedPasswordStoreType,
    
    notifyBackend,
    notifyImapEnabled,
    notifyPublishHttpConnectionPoolEnabled,
    notifyPublishHttpPoolMaxSize,
    notifyPublishHttpConTimeoutMS,
    notifyPublishThreadPoolMaxSize,
    notifyPublishHttpReadTimeout,
    
    httpRmeEnabled,
    httpRmeEnabledServices,
    systemFolders,
    oxRMIHost,
    oxRMIPort,
    oxAdminMaster,
    oxAdminMasterPassword,
    rmiMaxPoolStubs,
    mxosMaxConnections,
    
    appSuiteIntegrated,
    rmiClientTimeout,
    rmiReadTimeout,
    oxCreateContextEnabled,
    oxMaxQuota,
    oxDefaultTimezone,
    oxDefaultLocale,
    oxSSOEnabled,
    oxSetMaxFileUploadSize,
    oxMaxFileUploadSize,
    oxPLMNAddressEnabled,
    oxAsyncContextCreation,
    oxOpenwaveVariantPrefix,
    oxImapUrl,
    oxSmtpUrl,
    oxSpamFilterEnabled,
    oxUserAccess,
    oxAuthBypassKey,
    oxHttpConnectionTimeout,
    oxHttpReadTimeout,
    
    updateRecentFlagAfterCreate,
    disableNotificationFlag,
    
    sortedMsgMetadataListCountDefault,
    msgMetadataSortingOrderDefault,
    msgUpdateMessageKey,
    defaultCN, 
    includeGroupAdminToAllocations,
    
    samlProvider, 
    msgBodyBlobBase64Encoded, 
    msgSubjectBase64Encoded,
    ldapCosCachingEnabled,
    ldapCosCacheFetchRefreshCount
}
