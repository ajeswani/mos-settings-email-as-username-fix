/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All possible query list supported by MxOS.
 *
 * @see <i>mxos Reference Guide</i>
 *
 * @author mxos-dev
 */
public enum MxOSPOJOs implements DataMap.Property {
    // Domain API
    domain, domains,
    // Mailbox API
    mailbox, mailboxes, mailboxId, fwdAddressList, status, familyMailboxDn, adminRealmDn, groupName, type,
    base, bases, webMailFeatures, webMailFeaturesAddressBook, credentials, generalPreferences, mailAccess,
    mailStore, externalStore, groupAdminAllocCountsDn, groupAdminAllocations,
    mailReceipt, forwardingAddresses, addressesForLocalDelivery,
    allowedSendersList, blockedSendersList, sieveFiltersBlockedSendersList,
    mailSend, internalInfo,
    messageEventRecords,
    mssLinkInfo, smsServices, socialNetworks, adminControl,adminBlockedSendersList,adminApprovedSendersList,
    signature, signatureId, allSignatures,
    // Folder API
    folderId, folder, folders,
    
    // Message API

    messageUid, messageId, message, messages, messageBody, messageFlags, messageFlagsMap, headerSummary,
    receivedFrom, sentTo, messageMetaData, messageSummary, lastAccessTime,
    
    //External Accounts
    externalAccounts, mailAccount,
    
    //Address Book related
    session, contact, contactBase, contactName, contactsPersonalInfo, contactsPersonalInfoAllEvents,
    contactsPersonalInfoEvents,
    contactsPersonalInfoAddress, contactsPersonalInfoCommunication,
    contactId, contactIdList, contactsWorkInfo, contactsWorkInfoAddress, contactsWorkInfoCommunication,
    contactActualImage, contactImage,
    groups, groupId, groupIdList, groupBase, allMembers, members, memberId, allExternalMembers, 
    externalMembers, memberName, allGroupsBase, allContactBase, allContacts,
    
    // Notify Subscription related
    notify,
    subscriptions,
    
    // rme related
    mailboxInfo, folderInfo, imapMessageUids,externalLdapAttributeMap, accessInfo,
    
    // saml related
    samlRequest, samlAttributes,
    
    //Tasks related
    taskId, taskBase, recurrence, participant, details, attachement,
    allTasks, taskIdList, allTaskBase,    
}
