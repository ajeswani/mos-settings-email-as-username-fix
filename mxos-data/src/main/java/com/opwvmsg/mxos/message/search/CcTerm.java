/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import java.util.List;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements comparisons for the CC Address header.
 *
 */
public final class CcTerm extends AddressTerm {

    private static final long serialVersionUID = -7371719490545666422L;

    /**
     * Constructor
     * @param address	The Address to be compared
     */
    public CcTerm(String address) {
	super(address);
    }

    /**
     * The address comparator.
     *
     * @param metadata	The address comparison is applied to this Message
     * @return		true if the comparison succeeds, otherwise false
     */
    public boolean match(Metadata metadata) {
        List<String> ccs;

        try {
            ccs = metadata.getCc();
        } catch (Exception e) {
            return false;
        }

        if (ccs == null)
            return false;

        for (String cc : ccs) {
            if (super.match(cc))
                return true;
        }

        return false;
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof CcTerm))
	    return false;
	return super.equals(obj);
    }

    @Override
    public String toString() {
        return new StringBuffer("(").append(MessageProperty.cc.name())
                .append("~").append(pattern).append(")").toString();
    }
}
