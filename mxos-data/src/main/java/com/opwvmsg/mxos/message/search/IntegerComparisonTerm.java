/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

/**
 * This class implements comparisons for integers.
 *
 */
public abstract class IntegerComparisonTerm extends ComparisonTerm {

    private static final long serialVersionUID = 8713156988028365673L;
    /**
     * The number.
     *
     * @serial
     */
    protected int number;

    protected IntegerComparisonTerm(int comparison, int number) {
	this.comparison = comparison;
	this.number = number;
    }

    /**
     * Return the number to compare with.
     */
    public int getNumber() {
	return number;
    }

    /**
     * Return the type of comparison.
     */
    public int getComparison() {
	return comparison;
    }

    protected boolean match(int i) {
	switch (comparison) {
	    case LE: 
		return i <= number;
	    case LT:
		return i < number;
	    case EQ:
		return i == number;
	    case NE:
		return i != number;
	    case GT:
		return i > number;
	    case GE:
		return i >= number;
	    default:
		return false;
	}
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof IntegerComparisonTerm))
	    return false;
	IntegerComparisonTerm ict = (IntegerComparisonTerm)obj;
	return ict.number == this.number && super.equals(obj);
    }

    /**
     * Compute a hashCode for this object.
     */
    public int hashCode() {
	return number + super.hashCode();
    }
}
