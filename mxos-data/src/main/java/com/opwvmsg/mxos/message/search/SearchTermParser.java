/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;
import java.util.Vector;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.search.AndTerm;
import com.opwvmsg.mxos.message.search.CcTerm;
import com.opwvmsg.mxos.message.search.FromTerm;
import com.opwvmsg.mxos.message.search.NotTerm;
import com.opwvmsg.mxos.message.search.OrTerm;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.message.search.SubjectTerm;
import com.opwvmsg.mxos.message.search.ToTerm;

/**
 * Utility class for all Term related operations.
 *
 * @author mxos-dev
 */
public class SearchTermParser {

    protected static final char START = '(';
    protected static final char END = ')';
    protected static final char AND = '&';
    protected static final char OR = '|';
    protected static final char NOT = '!';
    protected static final char EQUAL = '=';
    protected static final char APPROX = '~';
    protected static final String START_AND = "(&";
    protected static final String START_OR = "(|";
    protected static final String START_NOT = "(!";

    /**
     * Method to generate query string representation from SearchTerm.
     *
     * @param st SearchTerm
     * @return string, query string representation
     */
    public static String parse(final SearchTerm st) {
        String s = "";
        if (st != null) {
            s = st.toString();
        }
        return s;
    }
    /**
     * Method to parse the given String to SearchTerm objects.
     *
     * @param query - searchTerm query
     * @return SearchTerm, SearchTerm Object
     */
    public static SearchTerm parse(String query) throws Exception {
        try {
            SearchTerm st = null;
            if (query == null || query.equals("")) {
                return null;
            }
            if (query.startsWith(START_AND)) {
                // Complex Term - AND
                Vector<String> terms = parseTerms(query.substring(2,
                        query.length() - 1));
                SearchTerm[] t = new SearchTerm[terms.size()];
                int index = 0;
                for (String temp : terms) {
                    t[index++] = parse(temp);
                }
                st = new AndTerm(t);
            } else if (query.startsWith(START_OR)) {
                // Complex Term - OR
                Vector<String> terms = parseTerms(query.substring(2,
                        query.length() - 1));
                SearchTerm[] t = new SearchTerm[terms.size()];
                int index = 0;
                for (String temp : terms) {
                    t[index++] = parse(temp);
                }
                st = new OrTerm(t);
            } else if (query.startsWith(START_NOT)) {
                // Complex Term - NOT
                Vector<String> terms = parseTerms(query.substring(2,
                        query.length() - 1));
                SearchTerm[] t = new SearchTerm[terms.size()];
                int index = 0;
                for (String temp : terms) {
                    t[index++] = parse(temp);
                }
                st = new NotTerm(t[0]);
            } else {
                // Simple SearchTerm
                query = query.substring(1, query.length() - 1);
                if (query.contains("~")) {
                    String[] pair = query.split("~");
                    st = prepareTerm(pair[0], pair[1]);
                } else if (query.contains("=")) {
                    String[] pair = query.split("=");
                    st = prepareTerm(pair[0], pair[1]);
                }
            }
            return st;
        } catch (Exception e) {
            throw new Exception("Error while parsing the searchTerm:" + query);
        }
    }
    /**
     * Method to parse complex terms.
     *
     * @param complexTerm
     * @return
     */
    private static Vector<String> parseTerms(String complexTerm) {
        Vector<String> terms = new Vector<String>();
        int starts = 0;
        int sp = 0;
        int ends = 0;
        int ep = 0;
        char[] tokens = complexTerm.toCharArray();
        for (char token : tokens) {
            switch (token) {
            case START:
                starts++;
                break;
            case END:
                ends++;
                break;
            default:
                break;
            }
            ep++;
            if (starts == ends) {
                terms.add(complexTerm.substring(sp, ep));
                sp = ep;
            }
        }
        return terms;
    }
    /**
     * Method to prepare the SearchTerm object from key and value.
     *
     * @param key - search key
     * @param value - search value
     * @return SearchTerm, SearchTerm object
     */
    private static SearchTerm prepareTerm(String key, String value) {
        SearchTerm st = null;
        // subject case
        if (key.equals(MessageProperty.subject.name())) {
            st = new SubjectTerm(value);
        } else if (key.equals(MessageProperty.to.name())) {
            st = new ToTerm(value);
        } else if (key.equals(MessageProperty.from.name())) {
            st = new FromTerm(value);
        } else if (key.equals(MessageProperty.cc.name())) {
            st = new CcTerm(value);
        }
        return st;
    }
}
