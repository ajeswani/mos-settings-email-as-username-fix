/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements the logical AND operator on individual
 * SearchTerms.
 *
 */
public final class AndTerm extends SearchTerm {

    private static final long serialVersionUID = 3842361769387944221L;
    /**
     * The array of terms on which the AND operator should be
     * applied.
     *
     * @serial
     */
    protected SearchTerm[] terms;

    /**
     * Constructor that takes two terms.
     * 
     * @param t1 first term
     * @param t2 second term
     */
    public AndTerm(SearchTerm t1, SearchTerm t2) {
	terms = new SearchTerm[2];
	terms[0] = t1;
	terms[1] = t2;
    }

    /**
     * Constructor that takes an array of SearchTerms.
     * 
     * @param t  array of terms
     */
    public AndTerm(SearchTerm[] t) {
	terms = new SearchTerm[t.length]; // clone the array
	for (int i = 0; i < t.length; i++)
	    terms[i] = t[i];
    }

    /**
     * Return the search terms.
     */
    public SearchTerm[] getTerms() {
	return (SearchTerm[])terms.clone();
    }

    /**
     * The AND operation. <p>
     *
     * The terms specified in the constructor are applied to
     * the given object and the AND operator is applied to their results.
     *
     * @param msg       The specified SearchTerms are applied to this Message
     *                  and the AND operator is applied to their results.
     * @return          true if the AND succeds, otherwise false
     */
    public boolean match(Metadata metadata) {
        for (int i=0; i < terms.length; i++)
            if (!terms[i].match(metadata))
                return false;
        return true;
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof AndTerm))
	    return false;
	AndTerm at = (AndTerm)obj;
	if (at.terms.length != terms.length)
	    return false;
	for (int i=0; i < terms.length; i++)
	    if (!terms[i].equals(at.terms[i]))
		return false;
	return true;
    }

    /**
     * Compute a hashCode for this object.
     */
    public int hashCode() {
	int hash = 0;
	for (int i=0; i < terms.length; i++)
	    hash += terms[i].hashCode();
	return hash;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("(&");
        for (int i=0; i < terms.length; i++) {
            sb.append(terms[i].toString());
        }
        sb.append(")");
        return sb.toString();
    }
}
