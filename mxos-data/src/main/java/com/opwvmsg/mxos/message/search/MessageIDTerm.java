/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This term models the RFC822 "MessageId" - a message-id for 
 * Internet messages that is supposed to be unique per message.
 * Clients can use this term to search a folder for a message given
 * its MessageId. <p>
 *
 * The MessageId is represented as a String.
 *
 */
public final class MessageIDTerm extends StringTerm {

    private static final long serialVersionUID = -3087312154455717928L;

    /**
     * Constructor.
     *
     * @param msgid  the msgid to search for
     */
    public MessageIDTerm(String msgid) {
	// Note: comparison is case-insensitive
	super(msgid);
    }

    /**
     * The match method.
     *
     * @param messageId	the match is applied to this Message's 
     *			Message-ID header
     * @return		true if the match succeeds, otherwise false
     */
    public boolean match(Metadata metadata) {
        if (super.match(metadata.getBlobMessageId()))
            return true;
        else
            return false;
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof MessageIDTerm))
	    return false;
	return super.equals(obj);
    }

    @Override
    public String toString() {
        return new StringBuffer("(").append(MessageProperty.messageId.name())
                .append("=").append(pattern).append(")").toString();
    }
}
