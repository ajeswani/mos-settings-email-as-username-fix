/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import org.apache.commons.codec.binary.Base64;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements comparisons for the message Subject header. The
 * comparison is case-insensitive. The pattern is a simple string that must
 * appear as a substring in the Subject.
 */
public final class SubjectTerm extends StringTerm {

    private static final long serialVersionUID = -4719640144135289107L;

    /**
     * Constructor.
     * 
     * @param pattern the pattern to search for
     */
    public SubjectTerm(String pattern) {
        // Note: comparison is case-insensitive
        super(pattern);
    }

    /**
     * The match method.
     * 
     * @param msg the pattern match is applied to this Message's subject header
     * @return true if the pattern match succeeds, otherwise false
     */
    public boolean match(Metadata metadata) {
        String subj = null;
        String value = System
                .getProperty(SystemProperty.msgSubjectBase64Encoded.name());
        if (Boolean.parseBoolean(value)) {
            Base64 base64 = new Base64();
            subj = new String(base64.decode(metadata.getSubject().getBytes()));
        } else {
            subj = metadata.getSubject();
        }
        if (subj == null)
            return false;

        return super.match(subj);
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
        if (!(obj instanceof SubjectTerm))
            return false;
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return new StringBuffer("(").append(MessageProperty.subject.name())
                .append("~").append(pattern).append(")").toString();
    }
}
