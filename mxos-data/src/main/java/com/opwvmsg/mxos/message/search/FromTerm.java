/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements comparisons for the From Address header.
 *
 */
public final class FromTerm extends AddressTerm {

    private static final long serialVersionUID = -7371719490545666422L;

    /**
     * Constructor
     * @param address	The Address to be compared
     */
    public FromTerm(String address) {
	super(address);
    }

    /**
     * The address comparator.
     *
     * @param metadata	The address comparison is applied to this Message
     * @return		true if the comparison succeeds, otherwise false
     */
    public boolean match(Metadata metadata) {
        String from =  metadata.getFrom();

        if (from == null)
            return false;

        return super.match(from);
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof FromTerm))
	    return false;
	return super.equals(obj);
    }

    @Override
    public String toString() {
        return new StringBuffer("(").append(MessageProperty.from.name())
                .append("~").append(pattern).append(")").toString();
    }
}
