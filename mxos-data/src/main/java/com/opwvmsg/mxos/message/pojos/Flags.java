
package com.opwvmsg.mxos.message.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Flags object of MessageMetaData
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "flagRecent",
    "flagSeen",
    "flagUnread",
    "flagAns",
    "flagFlagged",
    "flagDel",
    "flagBounce",
    "flagPriv",
    "flagDraft"
})
public class Flags implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagRecent")
    private Boolean flagRecent;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagSeen")
    private Boolean flagSeen;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagUnread")
    private Boolean flagUnread;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagAns")
    private Boolean flagAns;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagFlagged")
    private Boolean flagFlagged;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagDel")
    private Boolean flagDel;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagBounce")
    private Boolean flagBounce;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagPriv")
    private Boolean flagPriv;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagDraft")
    private Boolean flagDraft;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagRecent")
    public Boolean getFlagRecent() {
        return flagRecent;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagRecent")
    public void setFlagRecent(Boolean flagRecent) {
        this.flagRecent = flagRecent;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagSeen")
    public Boolean getFlagSeen() {
        return flagSeen;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagSeen")
    public void setFlagSeen(Boolean flagSeen) {
        this.flagSeen = flagSeen;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagUnread")
    public Boolean getFlagUnread() {
        return flagUnread;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagUnread")
    public void setFlagUnread(Boolean flagUnread) {
        this.flagUnread = flagUnread;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagAns")
    public Boolean getFlagAns() {
        return flagAns;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagAns")
    public void setFlagAns(Boolean flagAns) {
        this.flagAns = flagAns;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagFlagged")
    public Boolean getFlagFlagged() {
        return flagFlagged;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagFlagged")
    public void setFlagFlagged(Boolean flagFlagged) {
        this.flagFlagged = flagFlagged;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagDel")
    public Boolean getFlagDel() {
        return flagDel;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagDel")
    public void setFlagDel(Boolean flagDel) {
        this.flagDel = flagDel;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagBounce")
    public Boolean getFlagBounce() {
        return flagBounce;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagBounce")
    public void setFlagBounce(Boolean flagBounce) {
        this.flagBounce = flagBounce;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagPriv")
    public Boolean getFlagPriv() {
        return flagPriv;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagPriv")
    public void setFlagPriv(Boolean flagPriv) {
        this.flagPriv = flagPriv;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagDraft")
    public Boolean getFlagDraft() {
        return flagDraft;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("flagDraft")
    public void setFlagDraft(Boolean flagDraft) {
        this.flagDraft = flagDraft;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
