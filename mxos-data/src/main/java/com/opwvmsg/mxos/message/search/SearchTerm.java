/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import java.io.Serializable;

import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * Search criteria are expressed as a tree of search-terms, forming
 * a parse-tree for the search expression. <p>
 *
 * Search-terms are represented by this class. This is an abstract
 * class; subclasses implement specific match methods. <p>
 *
 * Search terms are serializable, which allows storing a search term
 * between sessions.
 *
 * <strong>Warning:</strong>
 * Serialized objects of this class may not be compatible with future
 * JavaMail API releases.  The current serialization support is
 * appropriate for short term storage. <p>
 *
 * <strong>Warning:</strong>
 * Search terms that include references to objects of type
 * <code>Message.RecipientType</code> will not be deserialized
 * correctly on JDK 1.1 systems.  While these objects will be deserialized
 * without throwing any exceptions, the resulting objects violate the
 * <i>type-safe enum</i> contract of the <code>Message.RecipientType</code>
 * class.  Proper deserialization of these objects depends on support
 * for the <code>readReplace</code> method, added in JDK 1.2.
 *
 */
public abstract class SearchTerm implements Serializable {

    private static final long serialVersionUID = -5177750631069031387L;

    /**
     * This method applies a specific match criterion to the given
     * message and returns the result.
     *
     * @param msg	The match criterion is applied on this message
     * @return		true, it the match succeeds, false if the match fails
     */

    public abstract boolean match(Metadata metadata);
    /**
     * Method to convert to string re-presentation.
     *
     * @return String, string representation
     */
    public abstract String toString();
}
