/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

/**
 * This class implements Message Address comparisons.
 *
 */

public abstract class AddressTerm extends StringTerm {

    private static final long serialVersionUID = 2005405551929769980L;

    protected AddressTerm(String address) {
	super(address);
    }
}
