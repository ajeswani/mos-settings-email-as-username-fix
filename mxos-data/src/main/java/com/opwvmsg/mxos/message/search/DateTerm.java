/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import java.util.Date;

/**
 * This class implements comparisons for Dates
 *
 */
public abstract class DateTerm extends ComparisonTerm {

    private static final long serialVersionUID = 1610863835810908983L;
    /**
     * The date.
     *
     * @serial
     */
    protected Date date;

    /**
     * Constructor.
     * @param comparison the comparison type
     * @param date  The Date to be compared against
     */
    protected DateTerm(int comparison, Date date) {
	this.comparison = comparison;
	this.date = date;
    }

    /**
     * Return the Date to compare with.
     */
    public Date getDate() {
	return new Date(date.getTime());
    }

    /**
     * Return the type of comparison.
     */
    public int getComparison() {
	return comparison;
    }

    /**
     * The date comparison method.
     *
     * @param d	the date in the constructor is compared with this date
     * @return  true if the dates match, otherwise false
     */
    protected boolean match(Date d) {
	switch (comparison) {
	    case LE: 
		return d.before(date) || d.equals(date);
	    case LT:
		return d.before(date);
	    case EQ:
		return d.equals(date);
	    case NE:
		return !d.equals(date);
	    case GT:
		return d.after(date);
	    case GE:
		return d.after(date) || d.equals(date);
	    default:
		return false;
	}
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof DateTerm))
	    return false;
	DateTerm dt = (DateTerm)obj;
	return dt.date.equals(this.date) && super.equals(obj);
    }

    /**
     * Compute a hashCode for this object.
     */
    public int hashCode() {
	return date.hashCode() + super.hashCode();
    }
}
