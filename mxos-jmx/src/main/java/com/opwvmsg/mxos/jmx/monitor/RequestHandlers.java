/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.monitor;

import static com.opwvmsg.mxos.jmx.setup.JMXConstants.SECONDS_IN_MILLIS;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.ReflectionException;
import javax.management.RuntimeOperationsException;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.jmx.counter.HandlerStats;
import com.opwvmsg.mxos.jmx.counter.RequestStats;

/**
 * Request Mbean implementation.
 * 
 * @author ajeswani
 */
public class RequestHandlers implements RequestHandlersMBean, DynamicMBean {

    private static final String VOID_TYPE = "void";
    private static final String LONG_TYPE = "long";
    private static final String RESET_MIN_PROCESSING_TIME = "resetMinProcessingTime";
    private static final String RESET_MAX_PROCESSING_TIME = "resetMaxProcessingTime";
    private static final String MIN_PROCESSING_TIME = "MinProcessingTime";
    private static final String MAX_PROCESSING_TIME = "MaxProcessingTime";
    private static final String AVG_PROCESSING_TIME = "AvgProcessingTime";
    private static final String NUM_TOTAL_REQUESTS = "NumTotalRequests";
    private static final String NUM_ACTIVE_REQUESTS = "NumActiveRequests";
    RequestStats requestStats = RequestStats.INSTANCE;

    @Override
    public long getNumActiveRequests() {
        return HandlerStats.ACTIVE_REQUESTS.getCount();
    }

    @Override
    public long getNumTotalRequests() {
        return requestStats.getCount();
    }

    @Override
    public long getAvgProcessingTime() {
        return requestStats.getAvg() / SECONDS_IN_MILLIS;
    }

    @Override
    public long getMaxProcessingTime() {
        return requestStats.getMax() / SECONDS_IN_MILLIS;
    }

    @Override
    public long getMinProcessingTime() {
        return requestStats.getMin() / SECONDS_IN_MILLIS;
    }

    @Override
    public void resetMaxProcessingTime() {
        requestStats.resetMax();
    }

    @Override
    public void resetMinProcessingTime() {
        requestStats.resetMin();
    }

    @Override
    public Object getAttribute(String attribute_name)
            throws AttributeNotFoundException, MBeanException,
            ReflectionException {

        // Check attribute_name to avoid NullPointerException later on
        if (attribute_name == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException(
                    "Attribute name cannot be null"));
        }

        // Call the corresponding getter for a recognized attribute_name
        if (attribute_name.equals(NUM_ACTIVE_REQUESTS)) {
            return getNumActiveRequests();
        }
        if (attribute_name.equals(NUM_TOTAL_REQUESTS)) {
            return getNumTotalRequests();
        }
        if (attribute_name.equals(AVG_PROCESSING_TIME)) {
            return getAvgProcessingTime();
        }
        if (attribute_name.equals(MAX_PROCESSING_TIME)) {
            return getMaxProcessingTime();
        }
        if (attribute_name.equals(MIN_PROCESSING_TIME)) {
            return getMinProcessingTime();
        }

        // If attribute_name has not been recognized
        throw (new AttributeNotFoundException("Cannot find " + attribute_name
                + " attribute in " + getClass().getName()));
    }

    @Override
    public void setAttribute(Attribute attribute)
            throws AttributeNotFoundException, InvalidAttributeValueException,
            MBeanException, ReflectionException {
        // TODO Auto-generated method stub

    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object invoke(String operationName, Object[] params,
            String[] signature) throws MBeanException, ReflectionException {

        if (operationName == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException(
                    "Operation name cannot be null"));
        }

        if (operationName.equals(RESET_MAX_PROCESSING_TIME)) {
            resetMaxProcessingTime();
        } else if (operationName.equals(RESET_MIN_PROCESSING_TIME)) {
            resetMinProcessingTime();
        } else {
            // unrecognized operation name:
            throw new ReflectionException(new NoSuchMethodException(
                    operationName), "Cannot find the operation ");
        }
        return null;
    }

    /**
     * getMBeanInfo.
     * 
     * @return MBeanInfo.
     */
    public MBeanInfo getMBeanInfo() {
        String s = getClass().getName();
        String s1 = "Request Handlers Dynamic MBean";
        MBeanAttributeInfo[] attrs = null;
        MBeanOperationInfo[] opers = null;

        String isRequestMonitoringEnabled = System
                .getProperty(SystemProperty.incomingRequestMonitoringEnabled
                        .name());

        boolean enabled = Boolean.valueOf(isRequestMonitoringEnabled);

        if (enabled) {
            opers = new MBeanOperationInfo[2];
            attrs = new MBeanAttributeInfo[5];
        } else {
            attrs = new MBeanAttributeInfo[2];
        }

        attrs[0] = new MBeanAttributeInfo(NUM_ACTIVE_REQUESTS, LONG_TYPE,
                "Number of active requests", true, false, false);
        attrs[1] = new MBeanAttributeInfo(NUM_TOTAL_REQUESTS, LONG_TYPE,
                "Number of total requests", true, false, false);

        if (enabled) {
            opers[0] = new MBeanOperationInfo(RESET_MAX_PROCESSING_TIME,
                    "Reset max processing time for requests", null, VOID_TYPE,
                    MBeanOperationInfo.ACTION);
            opers[1] = new MBeanOperationInfo(RESET_MIN_PROCESSING_TIME,
                    "Rest min processing time for requests", null, VOID_TYPE,
                    MBeanOperationInfo.ACTION);

            attrs[2] = new MBeanAttributeInfo(AVG_PROCESSING_TIME, LONG_TYPE,
                    "Avg processing time", true, false, false);
            attrs[3] = new MBeanAttributeInfo(MAX_PROCESSING_TIME, LONG_TYPE,
                    "Max processing time", true, false, false);
            attrs[4] = new MBeanAttributeInfo(MIN_PROCESSING_TIME, LONG_TYPE,
                    "Min processing time", true, false, false);
        }
        return new MBeanInfo(s, s1, attrs, null, opers, null);
    }
}
