/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.monitor;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.ReflectionException;
import javax.management.RuntimeOperationsException;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

/**
 * Connection pools Mbean implementation.
 * 
 * @author ajeswani
 */
public class ConnectionPools implements ConnectionPoolsMBean, DynamicMBean {

    private static final String LONG_TYPE = "long";
    private static final String NUM_MAX_LDAP_CONNECTIONS = "NumMaxLDAPConnections";
    private static final String NUM_MAX_LDAP_SEARCH_CONNECTIONS = "NumMaxLDAPSearchConnections";
    private static final String NUM_MAX_MMG_CONNECTIONS = "NumMaxMMGConnections";
    private static final String NUM_MAX_EXTERNAL_MTA_CONNECTIONS = "NumMaxExternalMTAConnections";
    private static final String NUM_MAX_MAA_CONNECTIONS = "NumMaxMAAConnections";
    private static final String NUM_ACTIVE_LDAP_CONNECTIONS = "NumActiveLDAPConnections";
    private static final String NUM_ACTIVE_LDAP_SEARCH_CONNECTIONS = "NumActiveLDAPSearchConnections";
    private static final String NUM_ACTIVE_MMG_CONNECTIONS = "NumActiveMMGConnections";
    private static final String NUM_ACTIVE_EXTERNAL_MTA_CONNECTIONS = "NumActiveExternalMTAConnections";
    private static final String NUM_ACTIVE_MAA_CONNECTIONS = "NumActiveMAAConnections";
    private static final String NUM_MAX_NOTIFY_CONNECTIONS = "NumMaxNotifyConnections";
    private static final String NUM_ACTIVE_NOTIFY_CONNECTIONS = "NumActiveNotifyConnections";

    private static final String NUM_MAX_MSSMETA_CONNECTIONS = "NumMaxMssMetaConnections";
    private static final String NUM_MAX_MSSSLMETA_CONNECTIONS = "NumMaxMssSLMetaConnections";
    private static final String NUM_MAX_OXADDRESSBOOK_CONNECTIONS = "NumMaxOXAddressBookConnections";
    private static final String NUM_MAX_OXSETTINGS_CONNECTIONS = "NumMaxOXSettingsConnections";
    private static final String NUM_MAX_OXHTTPSETTINGS_CONNECTIONS = "NumMaxOXHttpSettingsConnections";
    private static final String NUM_MAX_MAILBOX_CONNECTIONS = "NumMaxMailBoxConnections";
    private static final String NUM_MAX_WEADDRESSBOOK_CONNECTIONS = "NumMaxWEAddressBookConnections";
    private static final String NUM_MAX_MEG_CONNECTIONS = "NumMaxMegConnections";
    private static final String NUM_MAX_HTTP_CONNECTIONS = "NumMaxHttpConnections";
    private static final String NUM_ACTIVE_MSSMETA_CONNECTIONS = "NumActiveMssMetaConnections";
    private static final String NUM_ACTIVE_MSSSLMETA_CONNECTIONS = "NumActiveMssSLMetaConnections";
    private static final String NUM_ACTIVE_OXADDRESSBOOK_CONNECTIONS = "NumActiveOXAddressBookConnections";
    private static final String NUM_ACTIVE_OXSETTINGS_CONNECTIONS = "NumActiveOXSettingsConnections";
    private static final String NUM_ACTIVE_OXHTTPSETTINGS_CONNECTIONS = "NumActiveOXHttpSettingsConnections";
    private static final String NUM_ACTIVE_MAILBOX_CONNECTIONS = "NumActiveMailBoxConnections";
    private static final String NUM_ACTIVE_WEADDRESSBOOK_CONNECTIONS = "NumActiveWEAddressBookConnections";
    private static final String NUM_ACTIVE_MEG_CONNECTIONS = "NumActiveMegConnections";
    private static final String NUM_ACTIVE_HTTP_CONNECTIONS = "NumActiveHttpConnections";

    @Override
    public Object getAttribute(String attributeName)
            throws AttributeNotFoundException, MBeanException,
            ReflectionException {

        // Check attribute_name to avoid NullPointerException later on
        if (attributeName == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException(
                    "Attribute name cannot be null"));
        }

        // Call the corresponding getter for a recognized attribute_name
        if (attributeName.equals(NUM_MAX_LDAP_CONNECTIONS)) {
            return getNumMaxLDAPConnections();
        }
        if (attributeName.equals(NUM_MAX_LDAP_SEARCH_CONNECTIONS)) {
            return getNumMaxLDAPSearchConnections();
        }
        if (attributeName.equals(NUM_MAX_MMG_CONNECTIONS)) {
            return getNumMaxMMGConnections();
        }
        if (attributeName.equals(NUM_MAX_EXTERNAL_MTA_CONNECTIONS)) {
            return getNumMaxExternalMTAConnections();
        }
        if (attributeName.equals(NUM_MAX_MAA_CONNECTIONS)) {
            return getNumMaxMAAConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_LDAP_CONNECTIONS)) {
            return getNumActiveLDAPConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_LDAP_SEARCH_CONNECTIONS)) {
            return getNumActiveLDAPSearchConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_MMG_CONNECTIONS)) {
            return getNumActiveMMGConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_EXTERNAL_MTA_CONNECTIONS)) {
            return getNumActiveExternalMTAConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_MAA_CONNECTIONS)) {
            return getNumActiveMAAConnections();
        }
        if (attributeName.equals(NUM_MAX_NOTIFY_CONNECTIONS)) {
            return getNumMaxNotifyConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_NOTIFY_CONNECTIONS)) {
            return getNumActiveNotifyConnections();
        }

        if (attributeName.equals(NUM_MAX_MSSMETA_CONNECTIONS)) {
            return getNumMaxMssMetaConnections();
        }
        if (attributeName.equals(NUM_MAX_MSSSLMETA_CONNECTIONS)) {
            return getNumMaxMssSLMetaConnections();
        }
        if (attributeName.equals(NUM_MAX_OXADDRESSBOOK_CONNECTIONS)) {
            return getNumMaxOXAddressBookConnections();
        }
        if (attributeName.equals(NUM_MAX_OXSETTINGS_CONNECTIONS)) {
            return getNumMaxOXSettingsConnections();
        }
        if (attributeName.equals(NUM_MAX_OXHTTPSETTINGS_CONNECTIONS)) {
            return getNumMaxOXHttpSettingsConnections();
        }
        if (attributeName.equals(NUM_MAX_MAILBOX_CONNECTIONS)) {
            return getNumMaxMailBoxConnections();
        }
        if (attributeName.equals(NUM_MAX_WEADDRESSBOOK_CONNECTIONS)) {
            return getNumMaxWEAddressBookConnections();
        }
        if (attributeName.equals(NUM_MAX_MEG_CONNECTIONS)) {
            return getNumMaxMegConnections();
        }
        if (attributeName.equals(NUM_MAX_HTTP_CONNECTIONS)) {
            return getNumMaxHttpConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_MSSMETA_CONNECTIONS)) {
            return getNumActiveMssMetaConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_MSSSLMETA_CONNECTIONS)) {
            return getNumActiveMssSLMetaConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_OXADDRESSBOOK_CONNECTIONS)) {
            return getNumActiveOXAddressBookConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_OXSETTINGS_CONNECTIONS)) {
            return getNumActiveOXSettingsConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_OXHTTPSETTINGS_CONNECTIONS)) {
            return getNumActiveOXHttpSettingsConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_MAILBOX_CONNECTIONS)) {
            return getNumActiveMailBoxConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_WEADDRESSBOOK_CONNECTIONS)) {
            return getNumActiveWEAddressBookConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_MEG_CONNECTIONS)) {
            return getNumActiveMegConnections();
        }
        if (attributeName.equals(NUM_ACTIVE_HTTP_CONNECTIONS)) {
            return getNumActiveHttpConnections();
        }
        // If attribute_name has not been recognized
        throw (new AttributeNotFoundException("Cannot find " + attributeName
                + " attribute in " + getClass().getName()));
    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        return null;
    }

    @Override
    public MBeanInfo getMBeanInfo() {
        String s = getClass().getName();
        String s1 = "Connection Pools Dynamic MBean";
        MBeanAttributeInfo[] attrs = null;

        String bgcMode = System.getProperty(SystemProperty.deploymentMode
                .name());

        if (bgcMode.equalsIgnoreCase("bgc")) {
            attrs = new MBeanAttributeInfo[30];
        } else {
            attrs = new MBeanAttributeInfo[28];
        }

        attrs[0] = new MBeanAttributeInfo(NUM_MAX_LDAP_CONNECTIONS, LONG_TYPE,
                "Number of max ldap connections", true, false, false);
        attrs[1] = new MBeanAttributeInfo(NUM_ACTIVE_LDAP_CONNECTIONS,
                LONG_TYPE, "Number of active ldap connections", true, false,
                false);
        attrs[2] = new MBeanAttributeInfo(NUM_MAX_LDAP_SEARCH_CONNECTIONS,
                LONG_TYPE, "Number of max ldap search connections", true,
                false, false);
        attrs[3] = new MBeanAttributeInfo(NUM_ACTIVE_LDAP_SEARCH_CONNECTIONS,
                LONG_TYPE, "Number of active ldap search connections", true,
                false, false);
        attrs[4] = new MBeanAttributeInfo(NUM_MAX_MMG_CONNECTIONS, LONG_TYPE,
                "Number of max mmg connections", true, false, false);
        attrs[5] = new MBeanAttributeInfo(NUM_ACTIVE_MMG_CONNECTIONS,
                LONG_TYPE, "Number of active mmg connections", true, false,
                false);
        attrs[6] = new MBeanAttributeInfo(NUM_MAX_EXTERNAL_MTA_CONNECTIONS,
                LONG_TYPE, "Number of max external mta connections", true,
                false, false);
        attrs[7] = new MBeanAttributeInfo(NUM_ACTIVE_EXTERNAL_MTA_CONNECTIONS,
                LONG_TYPE, "Number of active external mta connections", true,
                false, false);
        attrs[8] = new MBeanAttributeInfo(NUM_MAX_NOTIFY_CONNECTIONS,
                LONG_TYPE, "Number of max notify http connections", true,
                false, false);
        attrs[9] = new MBeanAttributeInfo(NUM_ACTIVE_NOTIFY_CONNECTIONS,
                LONG_TYPE, "Number of active notify http connections", true,
                false, false);

        attrs[10] = new MBeanAttributeInfo(NUM_MAX_MSSMETA_CONNECTIONS,
                LONG_TYPE, "Number of max mss meta connections", true, false,
                false);
        attrs[11] = new MBeanAttributeInfo(NUM_ACTIVE_MSSMETA_CONNECTIONS,
                LONG_TYPE, "Number of active mss meta connections", true,
                false, false);
        attrs[12] = new MBeanAttributeInfo(NUM_MAX_MSSSLMETA_CONNECTIONS,
                LONG_TYPE, "Number of max mss si meta connections", true,
                false, false);
        attrs[13] = new MBeanAttributeInfo(NUM_ACTIVE_MSSSLMETA_CONNECTIONS,
                LONG_TYPE, "Number of active mss si meta connections", true,
                false, false);
        attrs[14] = new MBeanAttributeInfo(NUM_MAX_OXADDRESSBOOK_CONNECTIONS,
                LONG_TYPE, "Number of max ox address book connections", true,
                false, false);
        attrs[15] = new MBeanAttributeInfo(
                NUM_ACTIVE_OXADDRESSBOOK_CONNECTIONS, LONG_TYPE,
                "Number of active ox address book connections", true, false,
                false);
        attrs[16] = new MBeanAttributeInfo(NUM_MAX_OXSETTINGS_CONNECTIONS,
                LONG_TYPE, "Number of max ox settings connections", true,
                false, false);
        attrs[17] = new MBeanAttributeInfo(NUM_ACTIVE_OXSETTINGS_CONNECTIONS,
                LONG_TYPE, "Number of active ox settings connections", true,
                false, false);
        attrs[18] = new MBeanAttributeInfo(NUM_MAX_OXHTTPSETTINGS_CONNECTIONS,
                LONG_TYPE, "Number of max ox http settings connections", true,
                false, false);
        attrs[19] = new MBeanAttributeInfo(NUM_ACTIVE_OXHTTPSETTINGS_CONNECTIONS,
                LONG_TYPE, "Number of active ox http settings connections", true,
                false, false);
        attrs[20] = new MBeanAttributeInfo(NUM_MAX_MAILBOX_CONNECTIONS,
                LONG_TYPE, "Number of max mail box connections", true, false,
                false);
        attrs[21] = new MBeanAttributeInfo(NUM_ACTIVE_MAILBOX_CONNECTIONS,
                LONG_TYPE, "Number of active mail box connections", true,
                false, false);
        attrs[22] = new MBeanAttributeInfo(NUM_MAX_WEADDRESSBOOK_CONNECTIONS,
                LONG_TYPE, "Number of max we address book connections", true,
                false, false);
        attrs[23] = new MBeanAttributeInfo(
                NUM_ACTIVE_WEADDRESSBOOK_CONNECTIONS, LONG_TYPE,
                "Number of active we address book connections", true, false,
                false);
        attrs[24] = new MBeanAttributeInfo(NUM_MAX_MEG_CONNECTIONS, LONG_TYPE,
                "Number of max meg connections", true, false, false);
        attrs[25] = new MBeanAttributeInfo(NUM_ACTIVE_MEG_CONNECTIONS,
                LONG_TYPE, "Number of active meg connections", true, false,
                false);
        attrs[26] = new MBeanAttributeInfo(NUM_MAX_HTTP_CONNECTIONS, LONG_TYPE,
                "Number of max http connections", true, false, false);
        attrs[27] = new MBeanAttributeInfo(NUM_ACTIVE_HTTP_CONNECTIONS,
                LONG_TYPE, "Number of active http connections", true, false,
                false);

        if (bgcMode.equalsIgnoreCase("bgc")) {

            attrs[28] = new MBeanAttributeInfo(NUM_MAX_MAA_CONNECTIONS,
                    LONG_TYPE, "Number of max maa connections", true, false,
                    false);
            attrs[29] = new MBeanAttributeInfo(NUM_ACTIVE_MAA_CONNECTIONS,
                    LONG_TYPE, "Number of active maa connections", true, false,
                    false);
        }
        return new MBeanInfo(s, s1, attrs, null, null, null);
    }

    @Override
    public long getNumActiveExternalMTAConnections() {
        return ConnectionStats.ACTIVE_EXTERNAL_MTA.getCount();
    }

    @Override
    public long getNumActiveLDAPConnections() {
        return ConnectionStats.ACTIVE_LDAP.getCount();
    }

    @Override
    public long getNumActiveLDAPSearchConnections() {
        return ConnectionStats.ACTIVE_LDAPSEARCH.getCount();
    }

    @Override
    public long getNumActiveMAAConnections() {
        return ConnectionStats.ACTIVE_MAA.getCount();
    }

    @Override
    public long getNumActiveMMGConnections() {
        return ConnectionStats.ACTIVE_MMG.getCount();
    }

    @Override
    public long getNumMaxExternalMTAConnections() {
        return ConnectionStats.EXTERNAL_MTA.getCount();
    }

    @Override
    public long getNumMaxLDAPConnections() {
        return ConnectionStats.LDAP.getCount();
    }

    @Override
    public long getNumMaxLDAPSearchConnections() {
        return ConnectionStats.LDAPSEARCH.getCount();
    }

    @Override
    public long getNumMaxMAAConnections() {
        return ConnectionStats.MAA.getCount();
    }

    @Override
    public long getNumMaxMMGConnections() {
        return ConnectionStats.MMG.getCount();
    }

    @Override
    public long getNumMaxNotifyConnections() {
        return ConnectionStats.NOTIFY.getCount();
    }

    @Override
    public long getNumActiveNotifyConnections() {
        return ConnectionStats.ACTIVE_NOTIFY.getCount();
    }

    @Override
    public long getNumMaxMssMetaConnections() {
        return ConnectionStats.MSSMETA.getCount();
    }

    @Override
    public long getNumActiveMssMetaConnections() {
        return ConnectionStats.ACTIVE_MSSMETA.getCount();
    }

    @Override
    public long getNumMaxMssSLMetaConnections() {
        return ConnectionStats.MSSSLMETA.getCount();
    }

    @Override
    public long getNumActiveMssSLMetaConnections() {
        return ConnectionStats.ACTIVE_MSSSLMETA.getCount();
    }

    @Override
    public long getNumMaxOXAddressBookConnections() {
        return ConnectionStats.OXADDRESSBOOK.getCount();
    }

    @Override
    public long getNumActiveOXAddressBookConnections() {
        return ConnectionStats.ACTIVE_OXADDRESSBOOK.getCount();
    }

    @Override
    public long getNumMaxOXSettingsConnections() {
        return ConnectionStats.OXSETTINGS.getCount();
    }

    @Override
    public long getNumActiveOXSettingsConnections() {
        return ConnectionStats.ACTIVE_OXSETTINGS.getCount();
    }

    @Override
    public long getNumMaxOXHttpSettingsConnections() {
        return ConnectionStats.OXHTTPSETTINGS.getCount();
    }

    @Override
    public long getNumActiveOXHttpSettingsConnections() {
        return ConnectionStats.ACTIVE_OXHTTPSETTINGS.getCount();
    }

    @Override
    public long getNumMaxMailBoxConnections() {
        return ConnectionStats.MAILBOX.getCount();
    }

    @Override
    public long getNumActiveMailBoxConnections() {
        return ConnectionStats.ACTIVE_MAILBOX.getCount();
    }

    @Override
    public long getNumMaxWEAddressBookConnections() {
        return ConnectionStats.WEADDRESSBOOK.getCount();
    }

    @Override
    public long getNumActiveWEAddressBookConnections() {
        return ConnectionStats.ACTIVE_WEADDRESSBOOK.getCount();
    }

    @Override
    public long getNumMaxMegConnections() {
        return ConnectionStats.MEG.getCount();
    }

    @Override
    public long getNumActiveMegConnections() {
        return ConnectionStats.ACTIVE_MEG.getCount();
    }

    @Override
    public long getNumMaxHttpConnections() {
        return ConnectionStats.HTTP.getCount();
    }

    @Override
    public long getNumActiveHttpConnections() {
        return ConnectionStats.ACTIVE_HTTP.getCount();
    }

    @Override
    public Object invoke(String actionName, Object[] params, String[] signature)
            throws MBeanException, ReflectionException {
        return null;
    }

    @Override
    public void setAttribute(Attribute attribute)
            throws AttributeNotFoundException, InvalidAttributeValueException,
            MBeanException, ReflectionException {
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        return null;
    }
}
