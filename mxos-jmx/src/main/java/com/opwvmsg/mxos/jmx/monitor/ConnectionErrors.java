/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.monitor;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.ReflectionException;
import javax.management.RuntimeOperationsException;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;

/**
 * Connection errors Mbean implementation.
 * 
 * @author ajeswani
 */
public class ConnectionErrors implements ConnectionErrorsMBean, DynamicMBean {

    private static final String VOID_TYPE = "void";
    private static final String LONG_TYPE = "long";
    private static final String NUM_LDAP_CONNECTION_ERRORS = "NumLDAPConnectionErrors";
    private static final String NUM_LDAP_SEARCH_CONNECTION_ERRORS = "NumLDAPSearchConnectionErrors";
    private static final String NUM_MSS_CONNECTION_ERRORS = "NumMSSConnectionErrors";
    private static final String NUM_MAA_CONNECTION_ERRORS = "NumMAAConnectionErrors";
    private static final String NUM_SDUP_CONNECTION_ERRORS = "NumSDUPConnectionErrors";
    private static final String NUM_MMG_CONNECTION_ERRORS = "NumMMGConnectionErrors";
    private static final String NUM_VAMP_CONNECTION_ERRORS = "NumVAMPConnectionErrors";
    private static final String NUM_FBI_CONNECTION_ERRORS = "NumFBIConnectionErrors";
    private static final String NUM_MTA_CONNECTION_ERRORS = "NumMTAConnectionErrors";
    private static final String NUM_CONFIG_DB_CONNECTION_ERRORS = "NumConfigDBConnectionErrors";
    private static final String NUM_NOTIFY_CONNECTION_ERRORS = "NumNotifyConnectionErrors";

    private static final String RESET_NUM_LDAP_CONNECTION_ERRORS = "resetNumLDAPConnectionErrors";
    private static final String RESET_NUM_LDAP_SEARCH_CONNECTION_ERRORS = "resetNumLDAPSearchConnectionErrors";
    private static final String RESET_NUM_MSS_CONNECTION_ERRORS = "resetNumMSSConnectionErrors";
    private static final String RESET_NUM_MAA_CONNECTION_ERRORS = "resetNumMAAConnectionErrors";
    private static final String RESET_NUM_SDUP_CONNECTION_ERRORS = "resetNumSDUPConnectionErrors";
    private static final String RESET_NUM_MMG_CONNECTION_ERRORS = "resetNumMMGConnectionErrors";
    private static final String RESET_NUM_VAMP_CONNECTION_ERRORS = "resetNumVAMPConnectionErrors";
    private static final String RESET_NUM_FBI_CONNECTION_ERRORS = "resetNumFBIConnectionErrors";
    private static final String RESET_NUM_MTA_CONNECTION_ERRORS = "resetNumMTAConnectionErrors";
    private static final String RESET_NUM_CONFIG_DB_CONNECTION_ERRORS = "resetNumConfigDBConnectionErrors";
    private static final String RESET_NUM_NOTIFY_CONNECTION_ERRORS = "resetNumNotifyConnectionErrors";

    /**
     * getNumLDAPConnectionErrors.
     * 
     * @return no. of ldap connection errors.
     */
    @Override
    public long getNumLDAPConnectionErrors() {
        return ConnectionErrorStats.LDAP.getCount();
    }

    /**
     * getNumLDAPSearchConnectionErrors.
     * 
     * @return no. of ldap SearchConnection errors.
     */
    @Override
    public long getNumLDAPSearchConnectionErrors() {
        return ConnectionErrorStats.LDAPSEARCH.getCount();
    }

    /**
     * getNumMSSConnectionErrors.
     * 
     * @return no. of ldap MSSConnection errors.
     */
    @Override
    public long getNumMSSConnectionErrors() {
        return ConnectionErrorStats.MSS.getCount();
    }

    /**
     * getNumMAAConnectionErrors.
     * 
     * @return no. of ldap MAAConnection errors.
     */
    @Override
    public long getNumMAAConnectionErrors() {
        return ConnectionErrorStats.MAA.getCount();
    }

    /**
     * getNumSDUPConnectionErrors.
     * 
     * @return no. of SDUPConnection errors.
     */
    @Override
    public long getNumSDUPConnectionErrors() {
        return ConnectionErrorStats.SDUP.getCount();
    }

    /**
     * getNumMMGConnectionErrors.
     * 
     * @return no. of MMGConnection errors.
     */
    @Override
    public long getNumMMGConnectionErrors() {
        return ConnectionErrorStats.MMG.getCount();
    }

    /**
     * getNumVAMPConnectionErrors.
     * 
     * @return no. of VAMPConnection errors.
     */
    @Override
    public long getNumVAMPConnectionErrors() {
        return ConnectionErrorStats.VAMP.getCount();
    }

    /**
     * getNumFBIConnectionErrors.
     * 
     * @return no. of FBIConnection errors.
     */
    @Override
    public long getNumFBIConnectionErrors() {
        return ConnectionErrorStats.FBI.getCount();
    }

    /**
     * getNumMTAConnectionErrors.
     * 
     * @return no. of MTAConnection errors.
     */
    @Override
    public long getNumMTAConnectionErrors() {
        return ConnectionErrorStats.MTA.getCount();
    }

    /**
     * getNumConfigDBConnectionErrors.
     * 
     * @return no. of ConfigDBConnection errors.
     */
    @Override
    public long getNumConfigDBConnectionErrors() {
        return ConnectionErrorStats.CONFIGDB.getCount();
    }

    @Override
    public long getNumNotifyConnectionErrors() {
        return ConnectionErrorStats.NOTIFY.getCount();
    }

    /**
     * resetNumLDAPConnectionErrors.
     */
    @Override
    public void resetNumLDAPConnectionErrors() {
        ConnectionErrorStats.LDAP.reset();
    }

    /**
     * resetNumLDAPSearchConnectionErrors.
     */
    @Override
    public void resetNumLDAPSearchConnectionErrors() {
        ConnectionErrorStats.LDAPSEARCH.reset();
    }

    /**
     * resetNumMSSConnectionErrors.
     */
    @Override
    public void resetNumMSSConnectionErrors() {
        ConnectionErrorStats.MSS.reset();
    }

    /**
     * resetNumMAAConnectionErrors.
     */
    @Override
    public void resetNumMAAConnectionErrors() {
        ConnectionErrorStats.MAA.reset();
    }

    /**
     * resetNumSDUPConnectionErrors.
     */
    @Override
    public void resetNumSDUPConnectionErrors() {
        ConnectionErrorStats.SDUP.reset();
    }

    /**
     * resetNumMMGConnectionErrors.
     */
    @Override
    public void resetNumMMGConnectionErrors() {
        ConnectionErrorStats.MMG.reset();
    }

    /**
     * resetNumVAMPConnectionErrors.
     */
    @Override
    public void resetNumVAMPConnectionErrors() {
        ConnectionErrorStats.VAMP.reset();
    }

    /**
     * resetNumFBIConnectionErrors.
     */
    @Override
    public void resetNumFBIConnectionErrors() {
        ConnectionErrorStats.FBI.reset();
    }

    /**
     * resetNumMTAConnectionErrors.
     */
    @Override
    public void resetNumMTAConnectionErrors() {
        ConnectionErrorStats.MTA.reset();
    }

    /**
     * resetNumConfigDBConnectionErrors.
     */
    @Override
    public void resetNumConfigDBConnectionErrors() {
        ConnectionErrorStats.CONFIGDB.reset();
    }

    @Override
    public void resetNumNotifyConnectionErrors() {
        ConnectionErrorStats.NOTIFY.reset();
    }

    @Override
    public Object getAttribute(String attributeName)
            throws AttributeNotFoundException, MBeanException,
            ReflectionException {

        // Check attribute_name to avoid NullPointerException later on
        if (attributeName == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException(
                    "Attribute name cannot be null"));
        }

        // Call the corresponding getter for a recognized attribute_name
        if (attributeName.equals(NUM_LDAP_CONNECTION_ERRORS)) {
            return getNumLDAPConnectionErrors();
        }
        if (attributeName.equals(NUM_LDAP_SEARCH_CONNECTION_ERRORS)) {
            return getNumLDAPSearchConnectionErrors();
        }
        if (attributeName.equals(NUM_MSS_CONNECTION_ERRORS)) {
            return getNumMSSConnectionErrors();
        }
        if (attributeName.equals(NUM_MAA_CONNECTION_ERRORS)) {
            return getNumMAAConnectionErrors();
        }
        if (attributeName.equals(NUM_SDUP_CONNECTION_ERRORS)) {
            return getNumSDUPConnectionErrors();
        }
        if (attributeName.equals(NUM_MMG_CONNECTION_ERRORS)) {
            return getNumMMGConnectionErrors();
        }
        if (attributeName.equals(NUM_VAMP_CONNECTION_ERRORS)) {
            return getNumVAMPConnectionErrors();
        }
        if (attributeName.equals(NUM_FBI_CONNECTION_ERRORS)) {
            return getNumFBIConnectionErrors();
        }
        if (attributeName.equals(NUM_MTA_CONNECTION_ERRORS)) {
            return getNumMTAConnectionErrors();
        }
        if (attributeName.equals(NUM_CONFIG_DB_CONNECTION_ERRORS)) {
            return getNumConfigDBConnectionErrors();
        }
        if (attributeName.equals(NUM_NOTIFY_CONNECTION_ERRORS)) {
            return getNumNotifyConnectionErrors();
        }

        // If attribute_name has not been recognized
        throw (new AttributeNotFoundException("Cannot find " + attributeName
                + " attribute in " + getClass().getName()));
    }

    @Override
    public void setAttribute(Attribute attribute)
            throws AttributeNotFoundException, InvalidAttributeValueException,
            MBeanException, ReflectionException {
    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        return null;
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        return null;
    }

    @Override
    public Object invoke(String actionName, Object[] params, String[] signature)
            throws MBeanException, ReflectionException {
        // Check actionName to avoid NullPointerException later on
        if (actionName == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException(
                    "Action name cannot be null"));
        }

        // Call the corresponding reset for a recognized actionName
        if (actionName.equals(RESET_NUM_LDAP_CONNECTION_ERRORS)) {
            resetNumLDAPConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_LDAP_SEARCH_CONNECTION_ERRORS)) {
            resetNumLDAPSearchConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_MSS_CONNECTION_ERRORS)) {
            resetNumMSSConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_MAA_CONNECTION_ERRORS)) {
            resetNumMAAConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_SDUP_CONNECTION_ERRORS)) {
            resetNumSDUPConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_MMG_CONNECTION_ERRORS)) {
            resetNumMMGConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_VAMP_CONNECTION_ERRORS)) {
            resetNumVAMPConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_FBI_CONNECTION_ERRORS)) {
            resetNumFBIConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_MTA_CONNECTION_ERRORS)) {
            resetNumMTAConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_CONFIG_DB_CONNECTION_ERRORS)) {
            resetNumConfigDBConnectionErrors();
        }
        if (actionName.equals(RESET_NUM_NOTIFY_CONNECTION_ERRORS)) {
            resetNumNotifyConnectionErrors();
        }       
        return null;
    }

    @Override
    public MBeanInfo getMBeanInfo() {
        String s = getClass().getName();
        String s1 = "Connection Errors Dynamic MBean";
        MBeanAttributeInfo[] attrs = null;
        MBeanOperationInfo[] opers = null;

        String bgcMode = System.getProperty(SystemProperty.deploymentMode
                .name());

        if (bgcMode.equalsIgnoreCase("bgc")) {
            opers = new MBeanOperationInfo[11];
            attrs = new MBeanAttributeInfo[11];
        } else {
            opers = new MBeanOperationInfo[7];
            attrs = new MBeanAttributeInfo[7];
        }
        attrs[0] = new MBeanAttributeInfo(NUM_LDAP_CONNECTION_ERRORS,
                LONG_TYPE, "Number of ldap connection errors", true, false,
                false);
        attrs[1] = new MBeanAttributeInfo(NUM_LDAP_SEARCH_CONNECTION_ERRORS,
                LONG_TYPE, "Number of ldap search connection errors", true,
                false, false);
        attrs[2] = new MBeanAttributeInfo(NUM_MSS_CONNECTION_ERRORS, LONG_TYPE,
                "Number of external mss connection errors", true, false, false);
        attrs[3] = new MBeanAttributeInfo(NUM_MMG_CONNECTION_ERRORS, LONG_TYPE,
                "Number of mmg connection errors", true, false, false);
        attrs[4] = new MBeanAttributeInfo(NUM_MTA_CONNECTION_ERRORS, LONG_TYPE,
                "Number of mta connection errors", true, false, false);
        attrs[5] = new MBeanAttributeInfo(NUM_CONFIG_DB_CONNECTION_ERRORS,
                LONG_TYPE, "Number of configdb connection errors", true, false,
                false);
        attrs[6] = new MBeanAttributeInfo(NUM_NOTIFY_CONNECTION_ERRORS,
                LONG_TYPE, "Number of notify connection errors", true, false,
                false);

        opers[0] = new MBeanOperationInfo(RESET_NUM_LDAP_CONNECTION_ERRORS,
                "Reset count of ldap connection errors", null, VOID_TYPE,
                MBeanOperationInfo.ACTION);
        opers[1] = new MBeanOperationInfo(
                RESET_NUM_LDAP_SEARCH_CONNECTION_ERRORS,
                "Reset count of ldap search connection errors", null,
                VOID_TYPE, MBeanOperationInfo.ACTION);
        opers[2] = new MBeanOperationInfo(RESET_NUM_MSS_CONNECTION_ERRORS,
                "Reset count of mss connection errors", null, VOID_TYPE,
                MBeanOperationInfo.ACTION);
        opers[3] = new MBeanOperationInfo(RESET_NUM_MMG_CONNECTION_ERRORS,
                "Reset count of mmg connection errors", null, VOID_TYPE,
                MBeanOperationInfo.ACTION);
        opers[4] = new MBeanOperationInfo(RESET_NUM_MTA_CONNECTION_ERRORS,
                "Reset count of mta connection errors", null, VOID_TYPE,
                MBeanOperationInfo.ACTION);
        opers[5] = new MBeanOperationInfo(
                RESET_NUM_CONFIG_DB_CONNECTION_ERRORS,
                "Reset count of configdb connection errors", null, VOID_TYPE,
                MBeanOperationInfo.ACTION);
        opers[6] = new MBeanOperationInfo(RESET_NUM_NOTIFY_CONNECTION_ERRORS,
                "Reset count of notify connection errors", null, VOID_TYPE,
                MBeanOperationInfo.ACTION);

        if (bgcMode.equalsIgnoreCase("bgc")) {

            attrs[7] = new MBeanAttributeInfo(NUM_MAA_CONNECTION_ERRORS,
                    LONG_TYPE, "Number of maa connection errors", true, false,
                    false);
            attrs[8] = new MBeanAttributeInfo(NUM_SDUP_CONNECTION_ERRORS,
                    LONG_TYPE, "Number of sdup connection errors", true, false,
                    false);
            attrs[9] = new MBeanAttributeInfo(NUM_FBI_CONNECTION_ERRORS,
                    LONG_TYPE, "Number of fbi connection errors", true, false,
                    false);
            attrs[10] = new MBeanAttributeInfo(NUM_VAMP_CONNECTION_ERRORS,
                    LONG_TYPE, "Number of vamp connection errors", true, false,
                    false);

            opers[7] = new MBeanOperationInfo(RESET_NUM_MAA_CONNECTION_ERRORS,
                    "Reset count of mta connection errors", null, VOID_TYPE,
                    MBeanOperationInfo.ACTION);
            opers[8] = new MBeanOperationInfo(RESET_NUM_SDUP_CONNECTION_ERRORS,
                    "Reset count of sdup connection errors", null, VOID_TYPE,
                    MBeanOperationInfo.ACTION);
            opers[9] = new MBeanOperationInfo(RESET_NUM_FBI_CONNECTION_ERRORS,
                    "Reset count of fbi connection errors", null, VOID_TYPE,
                    MBeanOperationInfo.ACTION);
            opers[10] = new MBeanOperationInfo(
                    RESET_NUM_VAMP_CONNECTION_ERRORS,
                    "Reset count of vamp connection errors", null, VOID_TYPE,
                    MBeanOperationInfo.ACTION);

        }
        return new MBeanInfo(s, s1, attrs, null, opers, null);
    }

}
