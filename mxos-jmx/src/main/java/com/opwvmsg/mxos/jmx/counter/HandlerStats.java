/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.counter;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Counter for requests stats.
 *
 * @author ajeswani
 */
public enum HandlerStats {
    ACTIVE_REQUESTS;

    private AtomicLong count = new AtomicLong();

    /**
     * increment.
     */
    public void increment() {
        count.getAndIncrement();
    }

    /**
     * decrement.
     */
    public void decrement() {
        count.getAndDecrement();
    }

    /**
     * reset.
     */
    public void reset() {
        count.set(0);
    }

    /**
     * getCount.
     * 
     * @return long
     */
    public long getCount() {
        return count.get();
    }

    /**
     * decrement.
     * 
     * @param count long
     */
    public void setCount(long count) {
        this.count.set(count);
    }
}
