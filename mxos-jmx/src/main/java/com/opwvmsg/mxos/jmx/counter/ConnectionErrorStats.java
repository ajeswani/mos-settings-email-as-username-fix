/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.counter;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Counter for connection error stats.
 * 
 * @author ajeswani
 */
public enum ConnectionErrorStats {
    LDAP, LDAPSEARCH, MSS, MAA, SDUP, MMG, VAMP, FBI, MTA, CONFIGDB, NOTIFY;

    private AtomicLong count = new AtomicLong();

    /**
     * Method to increment.
     */
    public void increment() {
        count.getAndIncrement();
    }

    /**
     * Method to decrement.
     */
    public void decrement() {
        count.getAndDecrement();
    }

    /**
     * Method to reset.
     */
    public void reset() {
        count.set(0);
    }

    /**
     * Method to get count.
     * 
     * @return returns count
     */
    public long getCount() {
        return count.get();
    }

    /**
     * Method to set count.
     * 
     * @param count
     */
    public void setCount(long count) {
        this.count.set(count);
    }
}
