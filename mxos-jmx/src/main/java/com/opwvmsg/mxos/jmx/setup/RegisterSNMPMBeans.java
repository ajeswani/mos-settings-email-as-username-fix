/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.jmx.setup;

import java.io.File;
import java.lang.management.ManagementFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import com.adventnet.adaptors.snmp.snmpsupport.SmartSnmpAdaptor;
import com.adventnet.snmp.snmp2.agent.TrapRequestEvent;
import com.adventnet.utils.appender.SnmpTrapAppenderExtension;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Register SNMP Adapter.
 * 
 * @author ajeswani
 */
public class RegisterSNMPMBeans {

    public static final void setupSNMP() throws MalformedObjectNameException,
            InstanceAlreadyExistsException, MBeanRegistrationException,
            NotCompliantMBeanException, MxOSException {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        // register snmp adapter
        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        String snmpAgentPath = home + System.getProperty("snmpAgentPath");

        File snmpAgent = new File(snmpAgentPath);

        if (!snmpAgent.exists() || !snmpAgent.isDirectory()) {
            throw new MxOSException("Incorrect configuration of WebNMS Agent. "
                    + "Path is not a valid directory:"
                    + snmpAgent.getAbsolutePath());
        }

        SmartSnmpAdaptor smartAdaptor = new SmartSnmpAdaptor(
                snmpAgent.getAbsolutePath());

        ObjectName name = new ObjectName(SNMPConstants.SNMP_ADAPTER_MBEAN);
        if (!mbs.isRegistered(name)) {
            mbs.registerMBean(smartAdaptor, name);
        }
        initAppenderWithTrapService(smartAdaptor);
    }

    public static final void stopSNMP() throws InstanceNotFoundException,
            MalformedObjectNameException, MBeanRegistrationException {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        ObjectName name = new ObjectName(SNMPConstants.SNMP_ADAPTER_MBEAN);
        mbs.unregisterMBean(name);
    }

    /**
     * Start appender with trap service.
     * 
     * @param smartAdaptor SmartSnmpAdaptor
     */
    private static void initAppenderWithTrapService(
            SmartSnmpAdaptor smartAdaptor) {
        // Instantiating Trap Appender with the SNMP Adaptor's SnmpTrapService
        com.adventnet.utils.appender.SnmpTrapAppenderExtension appender = (SnmpTrapAppenderExtension) Logger
                .getRootLogger().getAppender(SNMPConstants.SNMP_TRAP_LOG);
        appender.setTrapIndex(SNMPConstants.SNMP_TRAP_INDEX);
        appender.setTrapSource(TrapRequestEvent.TFTABLE);
        appender.setTrapListener(smartAdaptor.getSnmpTrapService());
    }
}
