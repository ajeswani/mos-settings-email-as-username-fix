/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.setup;

import java.lang.management.ManagementFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.monitor.ConnectionErrors;
import com.opwvmsg.mxos.jmx.monitor.ConnectionErrorsMBean;
import com.opwvmsg.mxos.jmx.monitor.ConnectionPools;
import com.opwvmsg.mxos.jmx.monitor.ConnectionPoolsMBean;
import com.opwvmsg.mxos.jmx.monitor.RequestHandlers;
import com.opwvmsg.mxos.jmx.monitor.RequestHandlersMBean;

/**
 * Register JMX MBeans.
 * 
 * @author ajeswani
 */
public class RegisterMBeans {

    public static final void startMonitoring()
            throws MalformedObjectNameException,
            InstanceAlreadyExistsException, MBeanRegistrationException,
            NotCompliantMBeanException, MxOSException {

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        // register connection pools mbean
        ObjectName name = new ObjectName(JMXConstants.CONNECTION_POOLS_MBEAN);
        if (!mbs.isRegistered(name)) {
            ConnectionPoolsMBean connectionPools = new ConnectionPools();
            mbs.registerMBean(connectionPools, name);
        }

        // register connection errors mbean
        name = new ObjectName(JMXConstants.BROKEN_CONNECTIONS_MBEAN);
        if (!mbs.isRegistered(name)) {
            ConnectionErrorsMBean connectionErrors = new ConnectionErrors();
            mbs.registerMBean(connectionErrors, name);
        }

        // register request handlers mbean
        name = new ObjectName(JMXConstants.REQUEST_HANDLERS_MBEAN);
        if (!mbs.isRegistered(name)) {
            RequestHandlersMBean requestHandlers = new RequestHandlers();
            mbs.registerMBean(requestHandlers, name);
        }

        // register notification bean
        name = new ObjectName(SNMPConstants.NOTIFICATION_BROADCASTER_MBEAN);
        NotificationBroadcasterSupportImpl nbsi = NotificationBroadcasterSupportImpl
                .getInstance();
        if (!mbs.isRegistered(name)) {
            mbs.registerMBean(nbsi, name);
        }

        String isSNMPEnabled = System.getProperty(SystemProperty.snmpEnabled
                .name());
        boolean enabled = Boolean.valueOf(isSNMPEnabled);
        if (enabled) {
            // register snmp mbean
            RegisterSNMPMBeans.setupSNMP();
            nbsi.sendStartNotif(System.getProperty("defaultApp"));
        }
    }

    public static final void stopMonitoring()
            throws MalformedObjectNameException, MBeanRegistrationException,
            InstanceNotFoundException {

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        // unregister connection pools mbean
        ObjectName name = new ObjectName(JMXConstants.CONNECTION_POOLS_MBEAN);
        if (mbs.isRegistered(name)) {
            mbs.unregisterMBean(name);
        }

        // unregister connection errors mbean
        name = new ObjectName(JMXConstants.BROKEN_CONNECTIONS_MBEAN);
        if (mbs.isRegistered(name)) {
            mbs.unregisterMBean(name);
        }

        // unregister request handlers mbean
        name = new ObjectName(JMXConstants.REQUEST_HANDLERS_MBEAN);
        if (mbs.isRegistered(name)) {
            mbs.unregisterMBean(name);
        }

        NotificationBroadcasterSupportImpl nbsi = NotificationBroadcasterSupportImpl
                .getInstance();
        nbsi.sendStopNotif(System.getProperty("defaultApp"));
        if (mbs.isRegistered(name)) {
            mbs.unregisterMBean(name);
        }

        String isSNMPEnabled = System.getProperty(SystemProperty.snmpEnabled
                .name());
        boolean enabled = Boolean.valueOf(isSNMPEnabled);
        if (enabled) {
            // unregister snmp mbean
            RegisterSNMPMBeans.stopSNMP();
        }
    }
}
