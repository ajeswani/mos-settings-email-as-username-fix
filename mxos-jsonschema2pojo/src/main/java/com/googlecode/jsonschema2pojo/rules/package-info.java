/**
 * Provides the schema rules which can be 'applied' to perform the code
 * generation steps associated with JSON schema nodes.
 */
package com.googlecode.jsonschema2pojo.rules;
