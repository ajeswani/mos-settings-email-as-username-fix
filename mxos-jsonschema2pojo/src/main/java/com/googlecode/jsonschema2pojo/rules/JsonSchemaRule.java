package com.googlecode.jsonschema2pojo.rules;

import org.codehaus.jackson.JsonNode;

import com.googlecode.jsonschema2pojo.Schema;
import com.sun.codemodel.JClassContainer;
import com.sun.codemodel.JType;

/**
 * Applies a JSON schema.
 *
 * @see <a href="http://tools.ietf.org/html/draft-zyp-json-schema-03#section-5">
 *      http://tools.ietf.org/html/draft-zyp-json-schema-03#section-5</a>
 */
public class JsonSchemaRule implements SchemaRule<JClassContainer, JType> {

    private final RuleFactory ruleFactory;

    /**
     * Constructor.
     *
     * @param ruleFactory
     *            ruleFactory
     */
    protected JsonSchemaRule(RuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    /**
     * Applies this schema rule to take the required code generation steps.
     * <p>
     * At the root of a schema document this rule should be applied (schema
     * documents contain a schema), but also in many places within the document.
     * Each property of type "object" is itself defined by a schema, the items
     * attribute of an array is a schema, the additionalProperties attribute of
     * a schema is also a schema.
     * <p>
     * Where the schema value is a $ref, the ref URI is assumed to be applicable
     * as a URL (from which content will be read). Where the ref URI has been
     * encountered before, the root Java type created by that schema will be
     * re-used (generation steps won't be repeated).
     *
     * @param nodeName
     *            the name of the node to which this format is applied
     * @param schemaNode
     *            the format node
     * @param generatableType
     *            the type which which is being formatted e.g. for
     *            <code>{ "type" : "string", "format" : "uri" }</code> the
     *            baseType would be java.lang.String
     * @param schema
     *            the schema within which this schema rule is being applied
     * @return the Java type that is appropriate for the format value
     */
    @Override
    public JType apply(String nodeName, JsonNode schemaNode,
            JClassContainer generatableType, Schema schema) {

        if (schemaNode.has("$ref")) {
            schema = Schema.create(schema, schemaNode.get("$ref")
                    .getTextValue());
            schemaNode = schema.getContent();

            if (schema.isGenerated()) {
                return schema.getJavaType();
            }

            return apply(nodeName, schemaNode, generatableType, schema);
        }

        JType javaType;
        if (schemaNode.has("enum")) {
            javaType = ruleFactory.getEnumRule().apply(nodeName,
                    schemaNode.get("enum"), generatableType, schema);
        } else {
            javaType = ruleFactory.getTypeRule().apply(nodeName, schemaNode,
                    generatableType.getPackage(), schema);
        }
        schema.setJavaTypeIfEmpty(javaType);

        return javaType;
    }
}
