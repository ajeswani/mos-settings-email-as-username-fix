package com.googlecode.jsonschema2pojo;

import java.io.IOException;
import java.net.URL;

import com.sun.codemodel.JCodeModel;

/**
 * Generates Java types from a JSON schema.
 */
public interface SchemaMapper {

    /**
     * Reads a schema and adds generated types to the given code model.
     *
     * @param codeModel
     *            the java code-generation context that should be used to
     *            generated new types
     * @param className
     *            the name of the parent class the represented by this schema
     * @param packageName
     *            the target package that should be used for generated types
     * @param schema
     *            the schema document content
     * @throws IOException
     *             if the schema content cannot be read
     */
    void generate(JCodeModel codeModel, String className, String packageName,
            URL schema) throws IOException;

}
