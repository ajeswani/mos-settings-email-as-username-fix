package com.googlecode.jsonschema2pojo.exception;

/**
 * Represents an unexpected error during Java code generation.
 */
public class GenerationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * @param message message
     */
    public GenerationException(String message) {
        super(message);
    }

    /**
     * Default constructor.
     * @param cause cause
     */
    public GenerationException(Throwable cause) {
        super(cause);
    }

}
