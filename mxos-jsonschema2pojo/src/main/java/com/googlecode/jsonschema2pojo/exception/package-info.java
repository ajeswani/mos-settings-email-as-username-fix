/**
 * Provides exception types, thrown during schema parsing or code generation
 * when errors occur.
 */
package com.googlecode.jsonschema2pojo.exception;
