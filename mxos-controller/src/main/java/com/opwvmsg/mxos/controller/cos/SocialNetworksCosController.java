/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *k
 * $Id: $
 */

package com.opwvmsg.mxos.controller.cos;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSocialNetworksService;

import org.apache.log4j.Logger;

/**
 * SocialNetworksCosController class that controls all the requests for
 * SocialNetwork.
 *
 * @author mxos-dev
 */
@Path("/cos/v2/{cosId}/socialNetworks")
public class SocialNetworksCosController {
    private static Logger logger = Logger
            .getLogger(SocialNetworksCosController.class);

    /**
     * Retrieve SocialNetworks.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return SocialNetworks object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSocialNetworks(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get SocialNetworks Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            ICosSocialNetworksService socialNetworksService =
                (ICosSocialNetworksService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosSocialNetworksService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final SocialNetworks socialNetworks = socialNetworksService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, socialNetworks);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SocialNetworks.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSocialNetworks(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update SocialNetworks Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ICosSocialNetworksService socialNetworksService =
                (ICosSocialNetworksService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosSocialNetworksService.name());
            socialNetworksService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
