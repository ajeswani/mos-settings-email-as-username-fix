/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.addressbook;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsBaseService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsExternalMembersService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsMembersService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataParam;

/**
 * GroupsController class that controls all the requests for addressbook groups.
 * 
 * @author mxos-dev
 */
@Path("/addressBook/v2/{userId}/groups")
public class GroupsController {
    private static Logger logger = Logger.getLogger(GroupsController.class);

    /**
     * Create Group.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createGroups(@PathParam("userId") final String userId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create groups Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);

            final long gId = ((IGroupsService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.GroupsService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, gId);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create multiple Groups.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response createMultipleGroups(
            @PathParam("userId") final String userId,
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @FormDataParam("sessionId") final String sessionId,
            @FormDataParam("cookieString") final String cookieString)
            throws Exception {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create multiple Groups Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            MultivaluedMap<String, String> inputParams = new MultivaluedMapImpl();
            inputParams.add(AddressBookProperty.userId.name(), userId);
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
            inputParams.add(AddressBookProperty.groupsList.name(),
                    writer.toString());
            if (sessionId != null && !sessionId.isEmpty()) {
                inputParams.add(AddressBookProperty.sessionId.name(),
                        URLDecoder.decode(sessionId, MxOSConstants.UTF8));
            }
            if (cookieString != null && !cookieString.isEmpty()) {
                inputParams.add(AddressBookProperty.cookieString.name(),
                        URLDecoder.decode(cookieString, MxOSConstants.UTF8));
            }

            final List<Long> gIds = ((IGroupsService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.GroupsService.name()))
                    .createMultiple(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, gIds);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete all Groups.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAllGroups(@PathParam("userId") final String userId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete all groups Received for userId(").append(userId)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);

            ((IGroupsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsService.name()))
                    .deleteAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete the particular Group.
     * 
     * @param userId String
     * @param groupId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{groupId}")
    public Response deleteGroups(@PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete group Received for userId(").append(userId).append(
                    ")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);

            ((IGroupsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve groups Base.
     * 
     * @param String
     * @param String
     * @param uriInfo UriInfo
     * @return Response - groups Base object
     */
    @GET
    @Path("/{groupId}/base")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGroupsBase(@PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Groups Base Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);
            final GroupBase groupBase = ((IGroupsBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.GroupsBaseService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, groupBase);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of groups Base objects.
     * 
     * @param uriInfo UriInfo
     * @return Response - List of contact Base objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/base/list")
    public Response listGroupsBase(@PathParam("userId") final String userId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer("List groups base Request Received"));
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            final List<GroupBase> groupBaseObjects = ((IGroupsBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.GroupsBaseService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, groupBaseObjects);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update groups Base.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Path("/{groupId}/base")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateGroupsBase(@PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update groups Base Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.groupId.name(), groupId);
            ((IGroupsBaseService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsBaseService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create groups Members.
     * 
     * @param userId String
     * @param groupId String
     * @param memberId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{groupId}/members/{memberId}")
    public Response createGroupsMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @PathParam("memberId") final String memberId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create groups members Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.groupId.name(), groupId);
            inputParams.add(AddressBookProperty.memberId.name(), memberId);

            ((IGroupsMembersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsMemberService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve groups Members.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - groups Base object
     */
    @GET
    @Path("/{groupId}/members/{memberId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGroupsMembers(@PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @PathParam("memberId") final String memberId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Groups Members Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.memberId.name(), memberId);

            final Member member = ((IGroupsMembersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.GroupsMemberService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, member);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve all groups Members.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - groups Base object
     */
    @GET
    @Path("/{groupId}/members")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllGroupsMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Groups Members Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);

            final List<Member> memberObjects = ((IGroupsMembersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.GroupsMemberService.name()))
                    .readAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, memberObjects);

        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Groups Members.
     * 
     * @param userId String
     * @param groupId String
     * @param memberId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{groupId}/members/{memberId}")
    public Response deleteGroupsMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @PathParam("memberId") final String memberId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete groups members Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.memberId.name(), memberId);

            ((IGroupsMembersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsMemberService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete all Groups Members.
     * 
     * @param userId String
     * @param groupId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{groupId}/members")
    public Response deleteAllGroupsMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete all groups members Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);

            ((IGroupsMembersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsMemberService.name()))
                    .deleteAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create groups external Members.
     * 
     * @param userId String
     * @param groupId String
     * @param memberId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{groupId}/externalMembers/{memberName}")
    public Response createGroupsExternalMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @PathParam("memberName") final String memberName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create groups external members Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.groupId.name(), groupId);
            inputParams.add(AddressBookProperty.memberName.name(), memberName);

            ((IGroupsExternalMembersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsExternalMemberService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve groups external Members.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - groups Base object
     */
    @GET
    @Path("/{groupId}/externalMembers/{memberName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGroupsExternalMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @PathParam("memberName") final String memberName,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Groups Members Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.memberName.name(), memberName);

            final ExternalMember member = ((IGroupsExternalMembersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.GroupsExternalMemberService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, member);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve all groups external Members.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - groups Base object
     */
    @GET
    @Path("/{groupId}/externalMembers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllGroupsExternalMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Groups Members Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);

            final List<ExternalMember> memberObjects = ((IGroupsExternalMembersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.GroupsExternalMemberService.name()))
                    .readAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, memberObjects);

        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Groups Members.
     * 
     * @param userId String
     * @param groupId String
     * @param memberId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{groupId}/externalMembers/{memberName}")
    public Response deleteGroupsExternalMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @PathParam("memberName") final String memberName,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete groups external members Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.memberName.name(), memberName);

            ((IGroupsExternalMembersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsExternalMemberService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete all Groups Members.
     * 
     * @param userId String
     * @param groupId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{groupId}/externalMembers")
    public Response deleteAllGroupsExternalMembers(
            @PathParam("userId") final String userId,
            @PathParam("groupId") final String groupId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete all groups external members Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.groupId.name(), groupId);

            ((IGroupsExternalMembersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsExternalMemberService.name()))
                    .deleteAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Move multiple Contacts.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - contactId object
     * @throws Exception Exception
     */
    @POST
    @Path("/move")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response moveMultipleGroups(
            @PathParam("userId") final String userId,
            @FormDataParam("sessionId") String sessionId,
            @FormDataParam("cookieString") String cookieString,
            @FormDataParam("folderId") String folderId,
            @FormDataParam("moveToFolderId") String moveToFolderId,
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail)
            throws Exception {

        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Move multiple Groups Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            MultivaluedMap<String, String> inputParams = new MultivaluedMapImpl();
            inputParams.add(AddressBookProperty.userId.name(), userId);
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
            inputParams.add(AddressBookProperty.groupsList.name(),
                    writer.toString());
            inputParams.add(AddressBookProperty.sessionId.name(),
                    URLDecoder.decode(sessionId, MxOSConstants.UTF8));
            inputParams.add(AddressBookProperty.cookieString.name(),
                    URLDecoder.decode(cookieString, MxOSConstants.UTF8));
            inputParams.add(AddressBookProperty.folderId.name(),
                    URLDecoder.decode(folderId, MxOSConstants.UTF8));
            inputParams.add(AddressBookProperty.moveToFolderId.name(),
                    URLDecoder.decode(moveToFolderId, MxOSConstants.UTF8));

            ((IGroupsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.GroupsService.name()))
                    .moveMultiple(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
