/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;

/**
 * AliasController class that controls all the requests for Mailbox Alias.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/base/emailAliases")
public class AliasController {
    private static Logger logger = Logger.getLogger(AliasController.class);

    /**
     * Create Mutiple Alias.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Alias object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleAlias(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Multiple Aliases Request Received for eamil(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IEmailAliasService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAliasService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Create Alias.
     *
     * @param email String
     * @param emailAliases String
     * @param inputParams MultivaluedMap<String, String>
     * @return Alias object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{emailAlias}")
    public Response createAlias(@PathParam("email") final String email,
            @PathParam("emailAlias") final String emailAlias,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Aliases Request Received for eamil(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.emailAlias.name(), emailAlias);
            ((IEmailAliasService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAliasService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Alias.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Alias object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAlias(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Aliases Request Received for eamil(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> aliases = ((IEmailAliasService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailAliasService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, aliases);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Alias.
     *
     * @param email String
     * @param oldEmailAlias String
     * @param newEmailAlias String
     * @param inputParams MultivaluedMap<String, String>
     * @return Alias object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("{oldEmailAlias}/{newEmailAlias}")
    public Response updateAlias(@PathParam("email") final String email,
            @PathParam("oldEmailAlias") final String oldEmailAlias,
            @PathParam("newEmailAlias") final String newEmailAlias,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Aliases Request Received for eamil(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams
                    .add(MailboxProperty.oldEmailAlias.name(), oldEmailAlias);
            inputParams
                    .add(MailboxProperty.newEmailAlias.name(), newEmailAlias);
            ((IEmailAliasService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAliasService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Alias.
     *
     * @param email String
     * @param emailAlias String
     * @param uriInfo UriInfo
     * @return Alias object
     */
    @Path("/{emailAlias}")
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteAlias(@PathParam("email") final String email,
            @PathParam("emailAlias") final String emailAlias,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete Aliases Request Received for eamil(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(MailboxProperty.emailAlias.name(),
                    emailAlias);
            ((IEmailAliasService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAliasService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Alias.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Alias object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public Response listAlias(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "List Aliases Request Received for eamil(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> aliases = ((IEmailAliasService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailAliasService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, aliases);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
