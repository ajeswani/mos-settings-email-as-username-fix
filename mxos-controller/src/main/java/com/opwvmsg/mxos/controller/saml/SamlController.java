/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.saml;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.saml.ISamlService;

/**
 * SamlController class that controls all the requests for SAML.
 *
 * @author mxos-dev
 */
@Path("/v2/saml")
public class SamlController {
    private static Logger logger = Logger.getLogger(SamlController.class);

    /**
     * Retrieve SAML request.
     * 
     * @param acsURL String
     * @param uriInfo UriInfo
     * @return Mailbox object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/samlAuthentication/{acsURL}")
    public Response getSAMLRequest(
            @PathParam("acsURL") final String acsURL,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get SAML Request Received with acsURL(").append(
                    acsURL).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MxOSConstants.ACS_URL,
                    acsURL);
            final String samlRequest = ((ISamlService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.SamlService.name()))
                    .getSAMLRequest(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, samlRequest);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Decode SAML response from an Identity Provider.
     * 
     * @param samlAssertion String
     * @param inputParams MultivaluedMap<String, String>
     * @return AssertionSummary POJO
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/decodeSamlAssertion")
    public Response decodeSAMLResponse(
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            String samlAssertion = null;
            if (inputParams.containsKey(MxOSConstants.SAML_ASSERTION)) {
                samlAssertion = inputParams.get(MxOSConstants.SAML_ASSERTION)
                        .get(0);
            }
            StringBuffer sb = new StringBuffer(
                    "Decode SAML Request Received for samlAssertion(").append(
                    samlAssertion).append(")");
            logger.info(sb);
        }
        try {
            Map<String, String> samlAttributes = ((ISamlService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.SamlService.name()))
                    .decodeSAMLAssertion(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, samlAttributes);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
