/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller.process;

import java.util.Iterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;

/**
 * Send Mail Controller class that sends the mail to MTA.
 *
 * @author mxos-dev
 */
@Path("/sendMail")
public class SendMailController {
    private static Logger logger = Logger.getLogger(SendMailController.class);

    
    /**
     * Sends mail to MTA.
     *
     * @param fromAddress String
     * @param toAddress String
     * @param inputParams MultivaluedMap<String, String>
     * @return response
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{fromAddress}/{toAddress}")
    public Response sendMail(
            @PathParam("fromAddress") final String fromAddress,
            @PathParam("toAddress") final String toAddress,
            final MultivaluedMap<String, String> inputParams) {

        logger.info("Send Mail Request Received. Request Details:");
        logger.info("fromAddress:" + fromAddress);
        logger.info("toAddress:" + toAddress);

        if (logger.isDebugEnabled()) {
            Iterator<String> keys = inputParams.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (key.contains("password")) {
                    logger.debug(key + ":" + "XXXXXXXX");
                } else {
                    logger.debug(key + ":" + inputParams.getFirst(key));
                }
            }
        }

        try {
            inputParams.add(MailboxProperty.fromAddress.name(), fromAddress);
            inputParams.add(MailboxProperty.toAddress.name(), toAddress);

            ((ISendMailService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SendMailService.name()))
                    .process(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Sends mail to MTA.
     *
     * @param fromAddress String
     * @param inputParams MultivaluedMap<String, String>
     * @return response
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{fromAddress}")
    public Response sendMail(
            @PathParam("fromAddress") final String fromAddress,
            final MultivaluedMap<String, String> inputParams) {

        logger.info("Send Mail Request Received. Request Details:");
        logger.info("fromAddress:" + fromAddress);

        if (logger.isDebugEnabled()) {
            Iterator<String> keys = inputParams.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (key.equalsIgnoreCase("password")) {
                    logger.debug(key + ":" + "XXXXXXXX");
                } else {
                    logger.debug(key + ":" + inputParams.getFirst(key));
                }
            }
        }

        try {
            inputParams.add(MailboxProperty.fromAddress.name(), fromAddress);

            ((ISendMailService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SendMailService.name()))
                    .process(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
