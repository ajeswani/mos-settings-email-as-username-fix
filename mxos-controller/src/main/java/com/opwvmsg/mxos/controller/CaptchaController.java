/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.captcha.ICaptchaService;

/**
 * CaptchaController class that updates Captcha flow.
 * 
 * @author mxos-dev
 */
@Path("/captcha/v2/{email}")
public class CaptchaController {
    private static Logger logger = Logger.getLogger(CaptchaController.class);

    /**
     * Validate Captcha of the user.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, authentication is "Sucess" if successful else "Failed"
     *         with proper error code
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response validateCaptcha(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Validate CAPTCHA Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((ICaptchaService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CaptchaService.name()))
                    .validateCaptcha(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
