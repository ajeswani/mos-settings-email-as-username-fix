/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMessageEventRecordsService;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;

import org.apache.log4j.Logger;

/**
 * InternalInfoController class that controls all the requests for InternalInfo.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/internalInfo")
public class MailInternalInfoController {
    private static Logger logger = Logger
            .getLogger(MailInternalInfoController.class);

    /**
     * Retrieve InternalInfo.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return InternalInfo object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInternalInfo(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Get InternalInfo Request Received for email(" + email
                    + ")");
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final InternalInfo internalInfo = ((IInternalInfoService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.InternalInfoService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, internalInfo);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update InternalInfo.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateInternalInfo(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update InternalInfo Request Received for email("
                    + email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IInternalInfoService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.InternalInfoService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MessageEventRecords.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return MessageEventRecords object
     */
    @GET
    @Path("/messageEventRecords")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessageEventRecords(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Get InternalInfo:MessageEventRecords Request" 
                    + " Received for email(" + email + ")");
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final MessageEventRecords msgEventRecords =
                    ((IMessageEventRecordsService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MessageEventRecordsService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, msgEventRecords);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MessageEventRecords.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Path("/messageEventRecords")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMessageEventRecords(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update InternalInfo:MessageEventRecords Request" 
                    + " Received for email(" + email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMessageEventRecordsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MessageEventRecordsService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
