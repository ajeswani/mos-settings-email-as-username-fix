/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsNotificationsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsOnlineService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;

import org.apache.log4j.Logger;

/**
 * SmsServicesController class that controls all the requests for SmsServices.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/smsServices")
public class SmsServicesController {
    private static Logger logger = Logger
            .getLogger(SmsServicesController.class);

    /**
     * Retrieve SmsServices.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SmsServices object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSmsServices(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get SmsServices Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            ISmsServicesService smsServicesService =
                (ISmsServicesService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SmsServicesService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final SmsServices smsServices = smsServicesService.read(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, smsServices);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SmsServices.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSmsServices(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update SmsServices Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ISmsServicesService smsServicesService =
                (ISmsServicesService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SmsServicesService.name());
            smsServicesService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve SmsService SmsOnline.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SmsOnline object
     */
    @GET
    @Path("/smsOnline")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSmsOnline(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt Filters Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final SmsOnline smsOnline = ((ISmsOnlineService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.SmsOnlineService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, smsOnline);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SmsOnline.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Path("/smsOnline")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSmsOnline(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update smsOnline Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((ISmsOnlineService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SmsOnlineService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve SmsService SmsNotifications.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SmsNotifications object
     */
    @GET
    @Path("/smsNotifications")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSmsNotifications(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt Filters Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final SmsNotifications smsNotifications =
                ((ISmsNotificationsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SmsNotificationsService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, smsNotifications);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SmsService SmsNotifications.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Path("/smsNotifications")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSmsNotifications(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update smsNotifications Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((ISmsNotificationsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SmsNotificationsService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
