/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.cos;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsOnlineService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsServicesService;

import org.apache.log4j.Logger;

/**
 * CosSmsServicesController class that controls all the requests for SmsServices
 * COS.
 *
 * @author mxos-dev
 */
@Path("/cos/v2/{cosId}/smsServices")
public class CosSmsServicesController {
    private static Logger logger = Logger
            .getLogger(CosSmsServicesController.class);

    /**
     * Retrieve SmsServices COS.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return SmsServices object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCosSmsServices(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "GET SmsServices Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            ICosSmsServicesService cosSmsServicesService =
                (ICosSmsServicesService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosSmsServicesService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final SmsServices smsServices = cosSmsServicesService.read(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, smsServices);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve SmsService SmsOnline.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return SmsOnline object
     */
    @GET
    @Path("/smsOnline")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSmsOnline(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "GET SmsServices SMSOnline Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final SmsOnline smsOnline = ((ICosSmsOnlineService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosSmsOnlineService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, smsOnline);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SmsServices.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSmsServices(@PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update SmsServices Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ICosSmsServicesService cosSmsServicesService =
                (ICosSmsServicesService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosSmsServicesService.name());
            cosSmsServicesService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SmsOnline.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Path("/smsOnline")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSmsOnline(@PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update smsOnline Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosSmsOnlineService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosSmsOnlineService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
