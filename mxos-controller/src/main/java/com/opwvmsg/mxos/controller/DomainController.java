/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.domain.IDomainService;

/**
 * DomainController class that controls all the requests for Domain, retrieve
 * and delete operations.
 *
 * @author mxos-dev
 */
@Path("/domain/v2")
public class DomainController {
    private static Logger logger = Logger.getLogger(DomainController.class);

    /**
     * Create Domain.
     *
     * @param domain String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{domain}")
    public Response createDomain(@PathParam("domain") final String domain,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create DomainRequest Received for domain(").append(domain)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(DomainProperty.domain.name(), domain);
            ((IDomainService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.DomainService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Domain.
     *
     * @param domain String
     * @param uriInfo UriInfo
     * @return Response - Domain object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{domain}")
    public Response getDomain(@PathParam("domain") final String domain,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get DomainRequest Received for domain(").append(domain)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(DomainProperty.domain.name(),
                    domain);
            final Domain domainObj = ((IDomainService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.DomainService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, domainObj);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Domain.
     *
     * @param domain String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{domain}")
    public Response updateDomain(@PathParam("domain") final String domain,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update DomainRequest Received for domain(").append(domain)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(DomainProperty.domain.name(), domain);
            ((IDomainService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.DomainService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Domain.
     *
     * @param domain String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/{domain}")
    public Response deletDomain(@PathParam("domain") final String domain,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete DomainRequest Received for domain(").append(domain)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(DomainProperty.domain.name(),
                    domain);
            ((IDomainService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.DomainService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * List Domains.
     *
     * @param uriInfo UriInfo
     * @return Response - List of Domain objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/search")
    public Response listDomain(@Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer("List Domain Request Received"));
        }
        try {
            final List<Domain> domains = ((IDomainService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.DomainService.name()))
                    .search(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, domains);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
