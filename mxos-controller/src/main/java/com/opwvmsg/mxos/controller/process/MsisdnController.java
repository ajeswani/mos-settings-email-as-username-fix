/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller.process;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnDeactivationService;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnReactivationService;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnSwapService;

/**
 * MsisdnController class that controls all the requests for Fiona Flows.
 *
 * @author mxos-dev
 */
@Path("/")
public class MsisdnController {
    private static Logger logger = Logger.getLogger(MsisdnController.class);

    /**
     * Deactivates the MSISDN.
     *
     * @param msisdn String
     * @param inputParams MultivaluedMap<String, String>
     * @return - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("msisdnDeactivationFlow/{msisdn}")
    public Response msisdnDeactivate(@PathParam("msisdn") final String msisdn,
            final MultivaluedMap<String, String> inputParams) {

        if (logger.isInfoEnabled()) {
            logger.info("Fiona Deactivate Request Received for recipient("
                    + msisdn + ")");
        }
        try {
            inputParams.add(MailboxProperty.msisdn.name(), msisdn);
            ((IMsisdnDeactivationService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MsisdnDeactivationService.name()))
                    .process(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Reactivates the MSISDN.
     *
     * @param msisdn String
     * @param inputParams MultivaluedMap<String, String>
     * @return - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("msisdnReactivationFlow/{msisdn}")
    public Response fionaReactivate(@PathParam("msisdn") final String msisdn,
            final MultivaluedMap<String, String> inputParams) {

        if (logger.isInfoEnabled()) {
            logger.info("Fiona Reactivate Request Received for recipient("
                    + msisdn + ")");
        }
        try {
            inputParams.add(MailboxProperty.msisdn.name(), msisdn);
            ((IMsisdnReactivationService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MsisdnReactivationService.name()))
                    .process(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Swap the MSISDN.
     *
     * @param currentMSISDN String
     * @param newMSISDN String
     * @param inputParams MultivaluedMap<String, String>
     * @return - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("msisdnSwapFlow/{currentMSISDN}/{newMSISDN}")
    public Response fionaSwap(
            @PathParam("currentMSISDN") final String currentMSISDN,
            @PathParam("newMSISDN") final String newMSISDN,
            final MultivaluedMap<String, String> inputParams) {

        if (logger.isInfoEnabled()) {
            logger.info("Fiona Swap Received for recipient(" + currentMSISDN
                    + ")");
        }
        try {
            inputParams
                    .add(MailboxProperty.currentMSISDN.name(), currentMSISDN);
            inputParams.add(MailboxProperty.newMSISDN.name(), newMSISDN);
            ((IMsisdnSwapService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MsisdnSwapService.name()))
                    .process(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
