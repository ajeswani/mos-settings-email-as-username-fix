/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesAddressBookService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesService;

import org.apache.log4j.Logger;

/**
 * WebMailFeaturesController class that controls all the requests for
 * WebMailFeatures.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/webMailFeatures")
public class WebMailFeaturesController {
    private static Logger logger = Logger
            .getLogger(WebMailFeaturesController.class);

    /**
     * Retrieve WebMailFeatures.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return WebMailFeatures object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWebMailFeatures(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get WebMailFeatures Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            IWebMailFeaturesService webMailFeaturesService =
                (IWebMailFeaturesService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.WebMailFeaturesService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final WebMailFeatures webMailFeatures = webMailFeaturesService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, webMailFeatures);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update WebMailFeatures.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateWebMailFeatures(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update WebMailFeatures Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            IWebMailFeaturesService webMailFeaturesService =
                (IWebMailFeaturesService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.WebMailFeaturesService.name());
            webMailFeaturesService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve WebMailFeaturesAddressBook.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return AddressBook object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/addressBookFeatures")
    public Response getAddressBook(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get WebMailFeaturesAddressBook Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IWebMailFeaturesAddressBookService webMailFeaturesAddressBookService = (IWebMailFeaturesAddressBookService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.WebMailFeaturesAddressBookService
                                    .name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final AddressBookFeatures webMailFeaturesAddressBook = webMailFeaturesAddressBookService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, webMailFeaturesAddressBook);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update WebMailFeaturesAddressBook.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/addressBookFeatures")
    public Response updateAddressBook(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update WebMailFeaturesAddressBook Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            IWebMailFeaturesAddressBookService webMailFeaturesAddressBookService = (IWebMailFeaturesAddressBookService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.WebMailFeaturesAddressBookService.name());
            webMailFeaturesAddressBookService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
