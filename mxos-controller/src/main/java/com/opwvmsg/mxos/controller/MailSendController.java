/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.McAfeeFilters;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendSignatureService;
import com.opwvmsg.mxos.interfaces.service.mailbox.INumDelayedDeliveryMessagesPendingService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendBMIFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendCommtouchFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendMcAfeeFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendService;

/**
 * MailSendController class that controls all the requests for MailSend.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/mailSend")
public class MailSendController {
    private static Logger logger = Logger.getLogger(MailSendController.class);

    /**
     * Retrieve MailSend.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return MailSend object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSend(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailSend Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final MailSend mailSend = ((IMailSendService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.MailSendService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailSend);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailSend.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailSend(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailSend Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailSendService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailSendService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Filters.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return MailSend object
     */
    @Path("/filters")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSendFilters(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailSend Filters Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final Filters filters = ((IMailSendFiltersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailSendFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, filters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve BMI Filters.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return MailSend object
     */
    @Path("/filters/bmiFilters")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSendBmiFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailSend Filters Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final BmiFilters bmiFilters = ((IMailSendBMIFiltersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailSendBMIFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, bmiFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Bmi filters.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @Path("/filters/bmiFilters")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateBmiFilters(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailSend Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailSendBMIFiltersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailSendBMIFiltersService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Commtouch Filters.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return MailSend object
     */
    @Path("/filters/commtouchFilters")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSendCommtouchFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailSend Filters Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final CommtouchFilters commtouchFilters = ((IMailSendCommtouchFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailSendCommtouchFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, commtouchFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Commtouch Filters.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @Path("/filters/commtouchFilters")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateCommtouchFilters(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailSend Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailSendCommtouchFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailSendCommtouchFiltersService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve mcafeeFilters.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return MailSend object
     */
    @Path("/filters/mcafeeFilters")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSendMcafeeFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailSend Filters Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final McAfeeFilters mcafeeFilters = ((IMailSendMcAfeeFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(ServiceEnum.MailSendMcAfeeFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mcafeeFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update McAfeeFilters.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @Path("/filters/mcafeeFilters")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMcAfeeFilters(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailSend Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailSendMcAfeeFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(ServiceEnum.MailSendMcAfeeFiltersService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create Mutiple DelayedDeliveryMessagesPending.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @Path("/numDelayedDeliveryMessagesPending")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleMailSendDelayedDeliveryMessagesPending(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Multiple DelayedDeliveryMessagesPending Request Received for eamil(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((INumDelayedDeliveryMessagesPendingService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailSendDeliveryMessagesPendingService
                                    .name())).create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create DelayedDeliveryMessagesPending.
     * 
     * @param email String
     * @param numDelayedDeliveryMessagesPending String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @Path("/numDelayedDeliveryMessagesPending/{numDelayedDeliveryMessagesPending}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMailSendDelayedDeliveryMessagesPending(
            @PathParam("email") final String email,
            @PathParam("numDelayedDeliveryMessagesPending") final String numDelayedDeliveryMessagesPending,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create DelayedDeliveryMessagesPending Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name(),
                    numDelayedDeliveryMessagesPending);
            ((INumDelayedDeliveryMessagesPendingService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailSendDeliveryMessagesPendingService
                                    .name())).create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve DelayedDeliveryMessagesPending.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return List<String>
     */
    @Path("/numDelayedDeliveryMessagesPending")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSendDelayedDeliveryMessagesPending(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get DelayedDeliveryMessagesPending Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);

            final List<String> deliveryMessagesPendingService = ((INumDelayedDeliveryMessagesPendingService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailSendDeliveryMessagesPendingService
                                    .name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, deliveryMessagesPendingService);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update DelayedDeliveryMessagesPending.
     * 
     * @param email String
     * @param oldNumDelayedDeliveryMessagesPending String
     * @param newNumDelayedDeliveryMessagesPending String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @Path("/numDelayedDeliveryMessagesPending/{oldNumDelayedDeliveryMessagesPending}/{newNumDelayedDeliveryMessagesPending}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailSendDelayedDeliveryMessagesPending(
            @PathParam("email") final String email,
            @PathParam("oldNumDelayedDeliveryMessagesPending") final String oldNumDelayedDeliveryMessagesPending,
            @PathParam("newNumDelayedDeliveryMessagesPending") final String newNumDelayedDeliveryMessagesPending,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update DelayedDeliveryMessagesPending Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams
                    .add(MailboxProperty.oldNumDelayedDeliveryMessagesPending
                            .name(), oldNumDelayedDeliveryMessagesPending);
            inputParams
                    .add(MailboxProperty.newNumDelayedDeliveryMessagesPending
                            .name(), newNumDelayedDeliveryMessagesPending);
            ((INumDelayedDeliveryMessagesPendingService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailSendDeliveryMessagesPendingService
                                    .name())).update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete DelayedDeliveryMessagesPending.
     * 
     * @param email String
     * @param numDelayedDeliveryMessagesPending String
     * @param uriInfo UriInfo
     * @return String
     */
    @Path("/numDelayedDeliveryMessagesPending/{numDelayedDeliveryMessagesPending}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response deleteMailSendDelayedDeliveryMessagesPending(
            @PathParam("email") final String email,
            @PathParam("numDelayedDeliveryMessagesPending") final String numDelayedDeliveryMessagesPending,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete DelayedDeliveryMessagesPending Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);

            uriInfo.getQueryParameters().add(
                    MailboxProperty.numDelayedDeliveryMessagesPending.name(),
                    numDelayedDeliveryMessagesPending);
            ((INumDelayedDeliveryMessagesPendingService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailSendDeliveryMessagesPendingService
                                    .name())).delete(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create Signature.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return signatureId
     */
    @Path("/signatures")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMailSendSignature(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create Mail Send Signature Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IMailSendSignatureService mailSendSignatureService = (IMailSendSignatureService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailSendSignatureService.name());
            inputParams.add(MailboxProperty.email.name(), email);
            final long sId = mailSendSignatureService.create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, sId);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Signature.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return Signature object
     */
    @Path("/signatures")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSendSignature(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailSend Signature Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IMailSendSignatureService mailSendSignatureService = (IMailSendSignatureService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailSendSignatureService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<Signature> mailSendSignatures = mailSendSignatureService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailSendSignatures);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Signature.
     * 
     * @param email String
     * @param signatureId String
     * @param uriInfo UriInfo
     * @return Signature object
     */
    @Path("/signatures/{signatureId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailSendSignatureBySignatureId(
            @PathParam("email") final String email,
            @PathParam("signatureId") final String signatureId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailSend Signature Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IMailSendSignatureService mailSendSignatureService = (IMailSendSignatureService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailSendSignatureService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.signatureId.name(), signatureId);
            final Signature mailSendSignatures = mailSendSignatureService
                    .readSignature(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailSendSignatures);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Signature.
     * 
     * @param email String
     * @param signatureId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @Path("/signatures/{signatureId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailSendSignature(
            @PathParam("email") final String email,
            @PathParam("signatureId") final String signatureId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailSend Signature Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IMailSendSignatureService mailSendSignatureService = (IMailSendSignatureService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailSendSignatureService.name());
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.signatureId.name(), signatureId);
            mailSendSignatureService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Signature.
     * 
     * @param email String
     * @param signatureId String
     * @param uriInfo UriInfo
     * @return
     */
    @Path("/signatures/{signatureId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMailSendSignature(
            @PathParam("email") final String email,
            @PathParam("signatureId") final String signatureId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete MailSend Signature Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IMailSendSignatureService mailSendSignatureService = (IMailSendSignatureService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailSendSignatureService.name());

            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.signatureId.name(), signatureId);
            mailSendSignatureService.delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
