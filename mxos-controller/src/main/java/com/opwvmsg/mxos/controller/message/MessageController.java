/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.controller.message;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.message.search.SearchTermParser;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IBodyService;
import com.opwvmsg.mxos.interfaces.service.message.IHeaderService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;

/**
 * MessageController class that controls all the requests for Message.
 * 
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/folders/{folderName}/messages")
public class MessageController {
    private static Logger logger = Logger.getLogger(MessageController.class);

    /**
     * Create Message.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - messageId String
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{messageId}")
    public Response createMessage(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Message Request Received for Message(").append(
                    messageId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.folderName.name(), folderName);
            inputParams.add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            final String msgId = ((IMessageService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.MessageService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, msgId);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create Message.
     * 
     * @param email String
     * @param folderName String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - messageId String
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMessage(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Message Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.folderName.name(), folderName);

            final String msgId = ((IMessageService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.MessageService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, msgId);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Message.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Message object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{messageId}")
    public Response getMessage(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Message Request Received for Message(").append(
                    messageId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            uriInfo.getQueryParameters().add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            final Message message = ((IMessageService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.MessageService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, message);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Message.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Message object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/{messageId}")
    public Response deleteMessage(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete Message Request Received for Message(").append(
                    messageId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            uriInfo.getQueryParameters().add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            ((IMessageService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MessageService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Message.
     * 
     * @param email String
     * @param folderName String
     * @param inputParams MultivaluedMap<String, String>
     * @return Message object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes("application/x-www-form-urlencoded")
    public Response deleteAllOrMultiMessages(
            @PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @FormParam("messageId") String messageId, @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Delete messages from folder Request Received");
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            
            /* execute multiple or all operation */
            if (uriInfo.getQueryParameters().containsKey(
                    MessageProperty.messageId.name())) {
                final StringBuilder sb = new StringBuilder(
                "Executing delete multiple.");
                if (logger.isDebugEnabled()) {
                    sb.append(" For email:")
                            .append(email)
                            .append(", Folder:")
                            .append(folderName)
                            .append(", MessageIds: ")
                            .append(uriInfo.getQueryParameters().get(
                                    MessageProperty.messageId.name()));
                    logger.debug(sb);
                } else if (logger.isInfoEnabled()) {
                    logger.info(sb);
                }
                ((IMessageService) MxOSApp.getInstance().getContext()
                        .getService(ServiceEnum.MessageService.name()))
                        .deleteMulti(uriInfo.getQueryParameters());
            } else {
                if (logger.isInfoEnabled()) {
                    logger.info("Executing delete all");
                }
                ((IMessageService) MxOSApp.getInstance().getContext()
                        .getService(ServiceEnum.MessageService.name()))
                        .deleteAll(uriInfo.getQueryParameters());
            }
            
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Copy Message.
     * 
     * @param email String
     * @param messageId String
     * @param folderName String
     * @param toFolderId Integer
     * @param inputParams MultivaluedMap<String, String>
     * @return Message object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{messageId}/copy/{toFolderName}")
    public Response copyMessage(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            @PathParam("toFolderName") final String toFolderName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Copy Message Request Received for Message(").append(
                    messageId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.srcFolderName.name(), folderName);
            inputParams.add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            inputParams.add(FolderProperty.toFolderName.name(), toFolderName);
            ((IMessageService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.MessageService.name()))
                    .copy(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Copy All Messages.
     *
     * @param email String
     * @param folderName String
     * @param toFolderId Integer
     * @param inputParams MultivaluedMap<String, String>
     * @return Message object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/copy/{toFolderName}")
    public Response copyAllOrMultipleMessages(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("toFolderName") final String toFolderName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Copy Messages Request Received");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.srcFolderName.name(), folderName);
            inputParams.add(FolderProperty.toFolderName.name(),
                    toFolderName);
            
            /* execute multiple or all operation */
            if (inputParams.containsKey(MessageProperty.messageId.name())) {
                final StringBuilder sb = new StringBuilder(
                        "Executing copy multiple.");
                if (logger.isDebugEnabled()) {
                    sb.append(" For email:")
                            .append(email)
                            .append(", Srcfolder:")
                            .append(folderName)
                            .append(", ToFolder:")
                            .append(toFolderName)
                            .append(", MessageIds: ")
                            .append(inputParams.get(MessageProperty.messageId
                                    .name()));
                    logger.debug(sb);
                } else if (logger.isInfoEnabled()) {
                    logger.info(sb);
                }
                ((IMessageService) MxOSApp.getInstance()
                        .getContext().getService(ServiceEnum.MessageService.name()))
                        .copyMulti(inputParams);
            } else {
                if (logger.isInfoEnabled()) {
                    logger.info("Executing copy all");
                }
                ((IMessageService) MxOSApp.getInstance().getContext()
                        .getService(ServiceEnum.MessageService.name()))
                        .copyAll(inputParams);
            }
            
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Move Message.
     * 
     * @param email String
     * @param messageId String
     * @param folderName String
     * @param toFolderId Integer
     * @param inputParams MultivaluedMap<String, String>
     * @return Message object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{messageId}/move/{toFolderName}")
    public Response moveMessage(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            @PathParam("toFolderName") final String toFolderName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Move Message Request Received for Message(").append(
                    messageId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.srcFolderName.name(), folderName);
            inputParams.add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            inputParams.add(FolderProperty.toFolderName.name(), toFolderName);
            ((IMessageService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MessageService.name()))
                    .move(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Move All Messages.
     * 
     * @param email String
     * @param folderName String
     * @param toFolderId Integer
     * @param inputParams MultivaluedMap<String, String>
     * @return Message object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/move/{toFolderName}")
    public Response moveAllOrMultipleMessages(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("toFolderName") final String toFolderName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Move Messages Request Received");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.srcFolderName.name(), folderName);
            inputParams.add(FolderProperty.toFolderName.name(), toFolderName);
            
            /* execute multiple or all operation */
            if (inputParams.containsKey(MessageProperty.messageId.name())) {
                final StringBuilder sb = new StringBuilder(
                "Executing move multiple.");
                if (logger.isDebugEnabled()) {
                    sb.append(" For email:")
                            .append(email)
                            .append(", Folder:")
                            .append(folderName)
                            .append(", MessageIds: ")
                            .append(inputParams.get(
                                    MessageProperty.messageId.name()));
                    logger.debug(sb);
                } else if (logger.isInfoEnabled()) {
                    logger.info(sb);
                }
                ((IMessageService) MxOSApp.getInstance().getContext()
                        .getService(ServiceEnum.MessageService.name()))
                        .moveMulti(inputParams);
            } else {
                if (logger.isInfoEnabled()) {
                    logger.info("Executing move all");
                }
                ((IMessageService) MxOSApp.getInstance().getContext()
                        .getService(ServiceEnum.MessageService.name()))
                        .moveAll(inputParams);
            }
            
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Message Metadata using the messageId.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Metadata object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{messageId}/metadata")
    public Response getMetadata(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            @Context final UriInfo uriInfo) {
        StringBuffer sb = new StringBuffer(
                "Get Message Metadata Request Received for MessageId(").append(
                messageId).append(")");
        logger.info(sb);
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            uriInfo.getQueryParameters().add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            final Metadata metadata = ((IMetadataService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MetadataService.name()))
                    .read(uriInfo.getQueryParameters());

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, metadata);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve Multiple Message Metadata using the messageIds.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Metadata object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/metadata")
    public Response getMultipleMetadata(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @Context final UriInfo uriInfo) {
        final StringBuffer sb = new StringBuffer(
                "Get Multiple MessageMetadata Request Received. ");
        if (logger.isDebugEnabled()) {
            sb.append(" For email:")
              .append(email)
              .append(", folder:")
              .append(folderName)
              .append(", MessageIds: ")
              .append(uriInfo.getQueryParameters().get(MessageProperty.messageId.name()));
            logger.debug(sb);
        }else if(logger.isInfoEnabled()){
            logger.info(sb);    
        }
        
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);

            final Map<String, Metadata> metaDatas = ((IMetadataService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MetadataService.name()))
                    .readMulti(uriInfo.getQueryParameters());

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, metaDatas);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    

    /**
     * Update Multiple Message Metadata using the messageIds.
     * 
     * @param email String
     * @param folderName String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/metadata")
    public Response updateMultipleMetadata(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            final MultivaluedMap<String, String> inputParams) {
        final StringBuffer sb = new StringBuffer(
                "Update Multiple MessageMetadata Request Received. ");
        if (logger.isDebugEnabled()) {
            sb.append(" For email:")
              .append(email)
              .append(", folder:")
              .append(folderName)
              .append(", MessageIds: ")
              .append(inputParams.get(
                      MessageProperty.messageId.name()));
            logger.debug(sb);
        }else if(logger.isInfoEnabled()){
            logger.info(sb);    
        }        
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.folderName.name(), folderName);            
            ((IMetadataService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MetadataService.name()))
                    .updateMulti(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }    
    
    
    /**
     * Update Message Metadata.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Response - MessageSummary object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{messageId}/metadata")
    public Response updateMetadata(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update Message Metadata Request Received for Message: "
                    + messageId);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.folderName.name(), folderName);
            inputParams.add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            ((IMetadataService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MetadataService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Retrieve Message Metadatas from a folder.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Metadata object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/metadata/list")
    public Response listMetadata(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @Context final UriInfo uriInfo) {
        StringBuffer sb = new StringBuffer(
                "List Message Metadata Request Received");
        logger.info(sb);
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            final Map<String, Metadata> metadatas = ((IMetadataService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MetadataService.name()))
                    .list(uriInfo.getQueryParameters());

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, metadatas);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MessageUUIDs and UIDs for Message Metadatas inside given folder.
     * 
     * @param email String
     * @param folderName String
     * @param uriInfo UriInfo
     * @return Map of Metadata objects populated with MessageUUIDs and UIDs.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/metadata/uid/list")
    public Response listMetadataUID(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @Context final UriInfo uriInfo) {
        StringBuffer sb = new StringBuffer(
                "List Message UUIDs and UIDs for Message Metadata Request Received");
        logger.info(sb);
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            final Map<String, Metadata> metadataUIDList = ((IMetadataService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MetadataService.name()))
                    .listUIDs(uriInfo.getQueryParameters());

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, metadataUIDList);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Message Header using the messageId.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Header object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{messageId}/header")
    public Response getMessageHeader(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            @Context final UriInfo uriInfo) {
        StringBuffer sb = new StringBuffer(
                "Get Message Header Request Received for MessageId(").append(
                messageId).append(")");
        logger.info(sb);
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            uriInfo.getQueryParameters().add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            final Header header = ((IHeaderService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.HeaderService.name()))
                    .read(uriInfo.getQueryParameters());

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, header);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Message Body using the messageId.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Body object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{messageId}/body")
    public Response getBody(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            @Context final UriInfo uriInfo) {
        StringBuffer sb = new StringBuffer(
                "Get Message Body Request Received for MessageId(").append(
                messageId).append(")");
        logger.info(sb);
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            uriInfo.getQueryParameters().add(MessageProperty.messageId.name(),
                    String.valueOf(messageId));
            final Body messageBody = ((IBodyService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.BodyService.name()))
                    .read(uriInfo.getQueryParameters());

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, messageBody);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Search Message Metadatas from a folder.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Metadata object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/metadata/search")
    public Response search(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @Context final UriInfo uriInfo) {
        logger.info("Search Message Metadata Request Received. FolderName= "
                + folderName + " and email= " + email);
        if (logger.isDebugEnabled()) {
            logger.debug("Parameters = " + uriInfo.getQueryParameters());
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            final Map<String, Map<String, Metadata>> metaMap;
            if (uriInfo.getQueryParameters().containsKey(MxOSConstants.QUERY)) {
                final String query = uriInfo.getQueryParameters().get("query")
                        .get(0);
                SearchTerm searchTerm = SearchTermParser.parse(query);
                metaMap = ((IMetadataService) MxOSApp.getInstance()
                        .getContext()
                        .getService(ServiceEnum.MetadataService.name()))
                        .search(uriInfo.getQueryParameters(), searchTerm);
            } else {
                metaMap = ((IMetadataService) MxOSApp.getInstance()
                        .getContext()
                        .getService(ServiceEnum.MetadataService.name()))
                        .search(uriInfo.getQueryParameters(), null);
            }

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, metaMap);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve Message Body using the messageId.
     * 
     * @param email String
     * @param folderName String
     * @param messageId String
     * @param uriInfo UriInfo
     * @return Body object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{messageId}/body")
    public Response updateBody(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @PathParam("messageId") final String messageId,
            final MultivaluedMap<String, String> inputParams) {
        StringBuffer sb = new StringBuffer(
                "Update Message Body Request Received for messageId(").append(
                messageId).append(")");
        logger.info(sb);
        try {

            inputParams.add(MessageProperty.messageId.name(), messageId);
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.folderName.name(), folderName);

            ((IBodyService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.BodyService.name()))
                    .update(inputParams);

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
