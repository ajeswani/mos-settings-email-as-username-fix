/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.notify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.NotificationProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.notify.INotifyService;
import com.opwvmsg.mxos.interfaces.service.notify.ISubscriptionsService;
import com.opwvmsg.mxos.notify.pojos.Notify;
import org.codehaus.jettison.json.JSONObject;

import static com.opwvmsg.mxos.error.NotifyError.NTF_INVALID_PUBLISH_MESSAGE;
import static com.opwvmsg.mxos.error.NotifyError.NTF_INVALID_TOPIC;
import static com.opwvmsg.mxos.error.NotifyError.NTF_UNABLE_TO_PUBLISH;

/**
 * NotifyController class that controls all the requests for Notify.
 *
 * @author mxos-dev
 */
@Path("/notify/v2")
public class NotifyController {
    private static Logger logger = Logger.getLogger(NotifyController.class);
    
    private static final List<String> PUBLISH_RESP_ERROR = new ArrayList<String>();
    static{
        PUBLISH_RESP_ERROR.add(NTF_INVALID_PUBLISH_MESSAGE.name());
        PUBLISH_RESP_ERROR.add(NTF_INVALID_TOPIC.name());
        PUBLISH_RESP_ERROR.add(NTF_UNABLE_TO_PUBLISH.name());
    }

    /**
     * Retrieve Notify Information.
     *
     * @param topic String
     * @param uriInfo UriInfo
     * @return Response - Notify object
     */
    @Path("/{topic}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNotify(@PathParam("topic") final String topic,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "GET Notify Request Received for topic(").append(
                    topic).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(NotificationProperty.topic.name(),
                    topic);
            final Notify notify = ((INotifyService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.NotifyService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, notify);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete all Notify subscriptions.
     *
     * @param topic String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{topic}")
    public Response deleteNotify(@PathParam("topic") final String topic,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "DELETE all Notify mapped subscriptions Request Received for topic(")
                    .append(topic).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(NotificationProperty.topic.name(),
                    topic);
            ((INotifyService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.NotifyService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create NotifySubscription.
     *
     * @param topic String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - void
     * @throws Exception Exception
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{topic}/subscriptions")
    public Response createNotifySubscription(@PathParam("topic") final String topic,
            final MultivaluedMap<String, String> inputParams) throws Exception {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "PUT Notify-Subscription Request Received for topic(")
                    .append(topic).append(")");
            logger.info(sb);
            sb = new StringBuffer("PUT data:-")
                    .append(" entry: " + inputParams.entrySet())
                    .append(", keys: " + inputParams.keySet())
                    .append(",values: " + inputParams.values());
            logger.debug(sb.toString());
        }
        try {
            inputParams.add(NotificationProperty.topic.name(), topic);
           ((ISubscriptionsService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.SubscriptionsService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Notify subscriptions.
     *
     * @param topic String
     * @param uriInfo UriInfo
     * @return Response - Notify subscriptions object
     */
    @Path("/{topic}/subscriptions")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNotifySubscriptions(@PathParam("topic") final String topic,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "GET Notify-Subscriptions Request Received for topic(").append(
                    topic).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(NotificationProperty.topic.name(),
                    topic);
            final List<String> subscriptions = ((ISubscriptionsService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.SubscriptionsService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, subscriptions);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Notify subscription.
     *
     * @param topic String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{topic}/subscriptions")
    public Response deleteNotifySubscription(
            @PathParam("topic") final String topic,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "DELETE Notify-Subscription Request Received for topic(")
                    .append(topic).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(NotificationProperty.topic.name(),
                    topic);
            ((ISubscriptionsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SubscriptionsService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{topic}/publish")
    public Response publishNotify(@PathParam("topic") final String topic,
            final JSONObject jsonParams) {
        if (logger.isInfoEnabled()) {
            final StringBuffer sb = new StringBuffer(
                    "POST Notify-Publish Request Received for topic(")
                    .append(topic).append(")").append(" with Message:")
                    .append(jsonParams);
            logger.info(sb);
        }
        try {
            final Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            inputParams.put(NotificationProperty.topic.name(),
                    Arrays.asList(topic));
            inputParams.put(NotificationProperty.notifyMessage.name(),
                    Arrays.asList(jsonParams.toString()));

            ((INotifyService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.NotifyService.name()))
                    .publish(inputParams);
        } catch (final Exception e) {
            String errCode = e.getMessage();
            if (e instanceof MxOSException) {
                errCode = ((MxOSException) e).getCode();
                if (PUBLISH_RESP_ERROR.contains(errCode)) {
                    return ControllerUtils.generateErrorResponse(
                            MediaType.APPLICATION_JSON,
                            ControllerUtils.generateHTTPStatus(e), e);
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug(String.format(
                        "NotifyPublish for topic %s failed. Error: %s", topic,
                        errCode));
            }
        }

        /* for publish regardless of result always sending 200OK!! */
        return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                Response.Status.OK, null);
    }
}
