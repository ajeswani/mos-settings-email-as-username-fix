/*

 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;

/**
 * A {@link ServletContextListener} implementation that start the
 * MxOSApplication instance.
 *
 * @author mxos-dev
 */
public class MxOSAppListener implements ServletContextListener {

    private static Logger logger = Logger
            .getLogger(MxOSAppListener.class);

    /**
     * Method being called just before MxOS deploy.
     *
     * @param event object to access {@link ServletContext}.
     */
    public void contextInitialized(ServletContextEvent event) {
        try {
            MxOSApp.getInstance();

            logger.info("MxOS Process Version " + getMOSVersion()
                    + " is started.");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Method being called just before MxOS undeploy.
     *
     * @param event object to access {@link ServletContext}.
     */
    public void contextDestroyed(ServletContextEvent event) {

        MxOSApp.stopInstance();

        logger.info("MxOS Process is stopped.");

    }

    /**
     * Method to get the mOS version.
     *
     * @return mOS Version
     */
    public String getMOSVersion() {
        String mOSVersion = "unknown";
        String path = "/META-INF/maven/com.opwvmsg.mxos/mxos-controller/pom.properties";
        InputStream stream = null;
        try {
            Properties p = new Properties();
            stream = getClass().getResourceAsStream(path);
            if (stream != null) {
                p.load(stream);
                mOSVersion = p.getProperty("version");
            }
        } catch (IOException e) {
            logger.warn("IOExceoption occured while loading pom.properties");
        } catch (Exception e) {
            logger.warn("Exception occured while loading pom.properties");
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (Exception e) {

                }
            }
        }
        return mOSVersion;
    }

}
