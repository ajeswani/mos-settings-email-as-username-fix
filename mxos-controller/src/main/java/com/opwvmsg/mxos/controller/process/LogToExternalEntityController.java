/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller.process;

import java.util.Iterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ILogToExternalEntityService;

/**
 * Log to ExternalEntity Controller class that sends the log message to External
 * Interfaces.
 *
 * @author mxos-dev
 */
@Path("/logToExternalEntity")
public class LogToExternalEntityController {
    private static Logger logger = Logger
            .getLogger(LogToExternalEntityController.class);

    /**
     * Sends log message to ExternalEntity.
     *
     * @param inputParams MultivaluedMap<String, String>
     * @return response
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response sendLog(final MultivaluedMap<String, String> inputParams) {

        logger.info("LogToExternalEntity Request Received. Request Details:");
        Iterator<String> keys = inputParams.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            logger.info(key + ":" + inputParams.getFirst(key));
        }

        try {
            ((ILogToExternalEntityService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LogToExternalEntityService.name()))
                    .process(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
