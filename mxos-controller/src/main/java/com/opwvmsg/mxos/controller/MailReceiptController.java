/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.McAfeeFilters;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAddressForDeliveryService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminApprovedSendersListService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminBlockedSendersListService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedSendersListService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IBlockedSendersListService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptBMIFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptCloudmarkFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptCommtouchFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptMcAfeeFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISenderBlockingService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISieveBlockedSendersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminControlService;

import org.apache.log4j.Logger;

/**
 * MailReceiptController class that controls all the requests for Mailbox
 * MailReceipt.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/mailReceipt")
public class MailReceiptController {
    private static Logger logger = Logger
            .getLogger(MailReceiptController.class);

    /**
     * Retrieve MailReceipt.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return MailReceipt object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceipt(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final MailReceipt mailReceipt = ((IMailReceiptService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailReceiptService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailReceipt);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return MailReceipt object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceipt(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailReceiptService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailReceiptService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailReceipt Multiple AddressForLocalDelivery.
     *
     * @param email String
     * @param addressForLocalDelivery String
     * @param inputParams MultivaluedMap<String, String>
     * @return MailReceipt SenderBlocking object
     */
    @PUT
    @Path("/addressesForLocalDelivery")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleAddressesForLocalDelivery(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create AddressForLocalDelivery Request Received "
                            + "for email(").append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IAddressForDeliveryService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AddressForLocalDeliveryService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailReceipt AddressForLocalDelivery.
     *
     * @param email String
     * @param addressForLocalDelivery String
     * @param inputParams MultivaluedMap<String, String>
     * @return MailReceipt SenderBlocking object
     */
    @PUT
    @Path("/addressesForLocalDelivery/{addressForLocalDelivery}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createAddressesForLocalDelivery(
            @PathParam("email") final String email,
            @PathParam("addressForLocalDelivery") final String
            addressForLocalDelivery,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create AddressForLocalDelivery Request Received "
                    + "for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.addressForLocalDelivery.name(),
                    addressForLocalDelivery);
            ((IAddressForDeliveryService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AddressForLocalDeliveryService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt AddressForLocalDelivery.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SenderBlocking object
     */
    @GET
    @Path("/addressesForLocalDelivery")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAddressForLocalDelivery(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get AddressForLocalDelivery SenderBlocking Request" 
                    + " Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> addresses = ((IAddressForDeliveryService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AddressForLocalDeliveryService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, addresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt AddressForLocalDelivery.
     *
     * @param email String
     * @param oldAddressForLocalDelivery String
     * @param newAddressForLocalDelivery String
     * @param inputParams MultivaluedMap<String, String>
     * @return MailReceipt SenderBlocking object
     */
    @POST
    @Path("/addressesForLocalDelivery/{oldAddressForLocalDelivery}/{newAddressForLocalDelivery}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateAddressesForLocalDelivery(
            @PathParam("email") final String email,
            @PathParam("oldAddressForLocalDelivery")
            final String oldAddressForLocalDelivery,
            @PathParam("newAddressForLocalDelivery") 
            final String newAddressForLocalDelivery,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update AddressForLocalDelivery Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldAddressForLocalDelivery.name(),
                    oldAddressForLocalDelivery);
            inputParams.add(MailboxProperty.newAddressForLocalDelivery.name(),
                    newAddressForLocalDelivery);
            ((IAddressForDeliveryService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AddressForLocalDeliveryService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailReceipt AddressForLocalDelivery.
     *
     * @param email String
     * @param addressForLocalDelivery String
     * @param uriInfo UriInfo
     * @return SenderBlocking object
     */
    @DELETE
    @Path("/addressesForLocalDelivery/{addressForLocalDelivery}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAddressForLocalDelivery(
            @PathParam("email") final String email,
            @PathParam("addressForLocalDelivery") 
            final String addressForLocalDelivery,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete AddressForLocalDelivery Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.addressForLocalDelivery.name(),
                    addressForLocalDelivery);
            ((IAddressForDeliveryService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AddressForLocalDeliveryService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt SenderBlocking.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SenderBlocking object
     */
    @GET
    @Path("/senderBlocking")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptSenderBlocking(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt SenderBlocking Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final SenderBlocking senderBlocking =
                ((ISenderBlockingService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SenderBlockingService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, senderBlocking);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt SenderBlocking.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return MailReceipt SenderBlocking object
     */
    @POST
    @Path("/senderBlocking")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptSenderBlocking(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((ISenderBlockingService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SenderBlockingService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailReceipt Multiple AllowedSender.
     *
     * @param email String
     * @param allowedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/senderBlocking/allowedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleAllowedSender(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create AllowedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IAllowedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    
    /**
     * Create MailReceipt AllowedSender.
     *
     * @param email String
     * @param allowedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/senderBlocking/allowedSendersList/{allowedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createAllowedSender(@PathParam("email") final String email,
            @PathParam("allowedSender") final String sender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create AllowedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.allowedSender.name(), sender);
            ((IAllowedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt AllowedSender.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return AllowedSender objects
     */
    @GET
    @Path("/senderBlocking/allowedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllowedSender(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get AllowedSender SenderBlocking Request Received"
                    + " for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> addresses = ((IAllowedSendersListService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.AllowedSendersListService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, addresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt AddressForLocalDelivery.
     *
     * @param email String
     * @param oldAllowedSender String
     * @param newAllowedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @POST
    @Path("/senderBlocking/allowedSendersList/{oldAllowedSender}/{newAllowedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateAllowedSender(@PathParam("email") final String email,
            @PathParam("oldAllowedSender") final String oldAllowedSender,
            @PathParam("newAllowedSender") final String newAllowedSender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update AllowedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldAllowedSender.name(),
                    oldAllowedSender);
            inputParams.add(MailboxProperty.newAllowedSender.name(),
                    newAllowedSender);
            ((IAllowedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedSendersListService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailReceipt AllowedSender.
     *
     * @param email String
     * @param allowedSender String
     * @param uriInfo UriInfo
     */
    @DELETE
    @Path("/senderBlocking/allowedSendersList/{allowedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAllowedSender(@PathParam("email") final String email,
            @PathParam("allowedSender") final String sender,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete AllowedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.allowedSender.name(), sender);
            ((IAllowedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedSendersListService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailReceipt Multiple BlockedSender.
     *
     * @param email String
     * @param blockedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/senderBlocking/blockedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleBlockedSender(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IBlockedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.BlockedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Create MailReceipt BlockedSender.
     *
     * @param email String
     * @param blockedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/senderBlocking/blockedSendersList/{blockedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createBlockedSender(@PathParam("email") final String email,
            @PathParam("blockedSender") final String blockedSender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams
                    .add(MailboxProperty.blockedSender.name(), blockedSender);
            ((IBlockedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.BlockedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt BlockedSender.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return BlockedSender objects
     */
    @GET
    @Path("/senderBlocking/blockedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBlockedSender(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> addresses = ((IBlockedSendersListService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.BlockedSendersListService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, addresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt blocked sender.
     *
     * @param email String
     * @param oldBlockedSender String
     * @param newBlockedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @POST
    @Path("/senderBlocking/blockedSendersList/{oldBlockedSender}/{newBlockedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateBlockedSender(@PathParam("email") final String email,
            @PathParam("oldBlockedSender") final String oldBlockedSender,
            @PathParam("newBlockedSender") final String newBlockedSender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update BlockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldBlockedSender.name(),
                    oldBlockedSender);
            inputParams.add(MailboxProperty.newBlockedSender.name(),
                    newBlockedSender);
            ((IBlockedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.BlockedSendersListService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailReceipt blockedSender.
     *
     * @param email String
     * @param blockedSender String
     * @param uriInfo UriInfo
     */
    @DELETE
    @Path("/senderBlocking/blockedSendersList/{blockedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteBlockedSender(@PathParam("email") final String email,
            @PathParam("blockedSender") final String blockedSender,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.blockedSender.name(), blockedSender);
            ((IBlockedSendersListService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.BlockedSendersListService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt Filters.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Filters object
     */
    @GET
    @Path("/filters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt Filters Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final Filters filters = ((IMailReceiptFiltersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailReceiptFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, filters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt SieveFilters.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SieveFilters object
     */
    @GET
    @Path("/filters/sieveFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptSieveFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt SieveFilters Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final SieveFilters sieveFilters =
                ((IMailReceiptSieveFiltersService) MxOSApp.getInstance()
                    .getContext().getService(
                            ServiceEnum.MailReceiptSieveFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, sieveFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt SieveFilters.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return SieveFilters object
     */
    @POST
    @Path("/filters/sieveFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptSieveFilters(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailReceiptSieveFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailReceiptSieveFiltersService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailReceipt Multiple Sieve blockedsender.
     *
     * @param email String
     * @param blockedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/filters/sieveFilters/blockedSenders")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleSieveBlockedSender(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((ISieveBlockedSendersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SieveBlockedSendersService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailReceipt Sieve blockedsender.
     *
     * @param email String
     * @param blockedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/filters/sieveFilters/blockedSenders/{blockedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createSieveBlockedSender(
            @PathParam("email") final String email,
            @PathParam("blockedSender") final String blockedSender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams
                    .add(MailboxProperty.blockedSender.name(), blockedSender);
            ((ISieveBlockedSendersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SieveBlockedSendersService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    
    /**
     * Retrieve MailReceipt Sieve blockedsender.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return AllowedSender objects
     */
    @GET
    @Path("/filters/sieveFilters/blockedSenders")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSieveBlockedSender(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> addresses = ((ISieveBlockedSendersService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.SieveBlockedSendersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, addresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt Sieve blockedsender.
     *
     * @param email String
     * @param oldBlockedSender String
     * @param newBlockedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @POST
    @Path("/filters/sieveFilters/blockedSenders/{oldBlockedSender}/{newBlockedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSieveBlockedSender(
            @PathParam("email") final String email,
            @PathParam("oldBlockedSender") final String oldBlockedSender,
            @PathParam("newBlockedSender") final String newBlockedSender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update BlockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldBlockedSender.name(),
                    oldBlockedSender);
            inputParams.add(MailboxProperty.newBlockedSender.name(),
                    newBlockedSender);
            ((ISieveBlockedSendersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SieveBlockedSendersService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailReceipt Sieve blockedSender.
     *
     * @param email String
     * @param blockedSender String
     * @param uriInfo UriInfo
     */
    @DELETE
    @Path("/filters/sieveFilters/blockedSenders/{blockedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSieveBlockedSender(
            @PathParam("email") final String email,
            @PathParam("blockedSender") final String blockedSender,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete blockedSender Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.blockedSender.name(), blockedSender);
            ((ISieveBlockedSendersService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SieveBlockedSendersService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt BmiFilters.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return BmiFilters object
     */
    @GET
    @Path("/filters/bmiFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptBmiFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt BmiFilters Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final BmiFilters bmifilters =
                ((IMailReceiptBMIFiltersService)MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.MailReceiptBMIFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, bmifilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt BmiFilters.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return BmiFilters object
     */
    @POST
    @Path("/filters/bmiFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptBmiFilters(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailReceiptBMIFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(ServiceEnum.MailReceiptBMIFiltersService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt CommontouchFilters.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return CommontouchFilters object
     */
    @GET
    @Path("/filters/commtouchFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptCommontouchFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt CommontouchFilters Request Received"
                    + " for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final CommtouchFilters commontouchFilters =
                ((IMailReceiptCommtouchFiltersService) MxOSApp.getInstance()
                    .getContext().getService(
                            ServiceEnum.MailReceiptCommtouchFiltersService
                                    .name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, commontouchFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt CommontouchFilters.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return CommontouchFilters object
     */
    @POST
    @Path("/filters/commtouchFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptCommontouchFilters(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailReceiptCommtouchFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailReceiptCommtouchFiltersService
                                    .name())).update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt CloudmarkFilters.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return CloudmarkFilters object
     */
    @GET
    @Path("/filters/cloudmarkFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptCloudmarkFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt CloudmarkFilters Request Received"
                    + " for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final CloudmarkFilters cloudmarkFilters =
                ((IMailReceiptCloudmarkFiltersService) MxOSApp.getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailReceiptCloudmarkFiltersService
                                    .name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, cloudmarkFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt CloudmarkFilters.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return CloudmarkFilters object
     */
    @POST
    @Path("/filters/cloudmarkFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptCloudmarkFilters(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailReceiptCloudmarkFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailReceiptCloudmarkFiltersService
                                    .name())).update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt McAfeeFilters.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return McAfeeFilters object
     */
    @GET
    @Path("/filters/mcafeeFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptMcAfeeFilters(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt McAfeeFilters Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final McAfeeFilters mcAfeeFilters =
                ((IMailReceiptMcAfeeFiltersService) MxOSApp.getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailReceiptMcAfeeFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mcAfeeFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt McAfeeFilters.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return McAfeeFilters object
     */
    @POST
    @Path("/filters/mcafeeFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptMcAfeeFilters(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailReceiptMcAfeeFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.MailReceiptMcAfeeFiltersService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve MailReceipt Admin Control.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return AdminControl object
     */
    @GET
    @Path("/adminControl")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptAdminControl(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt AdminControl Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final AdminControl adminControl = ((IAdminControlService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.AdminControlService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, adminControl);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt Admin Control.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return AdminControl object
     */
    @POST
    @Path("/adminControl")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptAdminControl(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt AdminControl Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IAdminControlService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AdminControlService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Create single admin blocked senders.The input (admin blocked sender
     * param) in url contains the parameter value only at the end of the url
     * 
     * @param email String
     * @param adminBlockedSendersList String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/adminControl/adminBlockedSendersList/{adminBlockedSendersList}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createAdminBlockedSender(
            @PathParam("email") final String email,
            @PathParam("adminBlockedSendersList") final String adminBlockedSendersList,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create adminBlockedSendersList Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.adminBlockedSendersList.name(),
                    adminBlockedSendersList);
            ((IAdminBlockedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminBlockedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create multiple admin blocked senders.Multi Values for the input
     * parameter admin blocked sender are passed in the request body (and not in
     * the url)
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/adminControl/adminBlockedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleAdminBlockedSender(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create MultipleAdminBlockedSender Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IAdminBlockedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminBlockedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Admin BlockedSenders List.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return AdminBlockedSender objects
     */
    @GET
    @Path("/adminControl/adminBlockedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAdminBlockedSender(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get adminBlockedSendersList Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> adminBlockedAddresses = ((IAdminBlockedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminBlockedSendersListService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, adminBlockedAddresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update admin blocked sender list.
     * 
     * @param email String
     * @param oldBlockedSender String
     * @param newBlockedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @POST
    @Path("/adminControl/adminBlockedSendersList/{oldAdminBlockedSender}/{newAdminBlockedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateAdminBlockedSender(
            @PathParam("email") final String email,
            @PathParam("oldAdminBlockedSender") final String oldAdminBlockedSender,
            @PathParam("newAdminBlockedSender") final String newAdminBlockedSender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update AdminBlockedSendersList Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldAdminBlockedSender.name(),
                    oldAdminBlockedSender);
            inputParams.add(MailboxProperty.newAdminBlockedSender.name(),
                    newAdminBlockedSender);
            ((IAdminBlockedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminBlockedSendersListService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailReceipt blockedSender.
     * 
     * @param email String
     * @param blockedSender String
     * @param uriInfo UriInfo
     */
    @DELETE
    @Path("/adminControl/adminBlockedSendersList/{adminBlockedSendersList}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAdminBlockedSendersList(
            @PathParam("email") final String email,
            @PathParam("adminBlockedSendersList") final String adminBlockedSendersList,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete adminBlockedSendersList Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.adminBlockedSendersList.name(),
                    adminBlockedSendersList);
            ((IAdminBlockedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminBlockedSendersListService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create single admin approved senders.The input (admin approved sender
     * param) in url contains the parameter value only at the end of the url
     * 
     * @param email String
     * @param adminApprovedSendersList String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/adminControl/adminApprovedSendersList/{adminApprovedSendersList}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createAdminApprovedSender(
            @PathParam("email") final String email,
            @PathParam("adminApprovedSendersList") final String adminApprovedSendersList,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create adminApprovedSendersList Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.adminApprovedSendersList.name(),
                    adminApprovedSendersList);
            ((IAdminApprovedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminApprovedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create multiple admin approved senders.Multi Values for the input
     * parameter (admin approved sender) are passed in the request body (and not
     * in the url)
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/adminControl/adminApprovedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleAdminApprovedSender(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create MultipleAdminApprovedSender Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IAdminApprovedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminApprovedSendersListService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Admin ApprovedSenders List.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return AdminApprovedSender objects
     */
    @GET
    @Path("/adminControl/adminApprovedSendersList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAdminApprovedSender(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get AdminApprovedSender Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> adminBlockedAddresses = ((IAdminApprovedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminApprovedSendersListService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, adminBlockedAddresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update admin approved sender list.
     * 
     * @param email String
     * @param oldAdminApprovedSender String
     * @param newAdminApprovedSender String
     * @param inputParams MultivaluedMap<String, String>
     */
    @POST
    @Path("/adminControl/adminApprovedSendersList/{oldAdminApprovedSender}/{newAdminApprovedSender}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateAdminApprovedSender(
            @PathParam("email") final String email,
            @PathParam("oldAdminApprovedSender") final String oldAdminApprovedSender,
            @PathParam("newAdminApprovedSender") final String newAdminApprovedSender,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update AdminApprovedSendersList Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldAdminApprovedSender.name(),
                    oldAdminApprovedSender);
            inputParams.add(MailboxProperty.newAdminApprovedSender.name(),
                    newAdminApprovedSender);
            ((IAdminApprovedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminApprovedSendersListService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailReceipt approved sender.
     * 
     * @param email String
     * @param adminApprovedSendersList String
     * @param uriInfo UriInfo
     */
    @DELETE
    @Path("/adminControl/adminApprovedSendersList/{adminApprovedSendersList}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAdminApprovedSendersList(
            @PathParam("email") final String email,
            @PathParam("adminApprovedSendersList") final String adminApprovedSendersList,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete adminApprovedSendersList Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.adminApprovedSendersList.name(),
                    adminApprovedSendersList);
            ((IAdminApprovedSendersListService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.AdminApprovedSendersListService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
