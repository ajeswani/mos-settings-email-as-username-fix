/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.addressbook;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoCommunicationService;

/**
 * ContactsWorkInfoCommunicationController class that controls all the requests
 * for contact's Work Info Communication sub object.
 * 
 * @author mxos-dev
 */
@Path("/addressBook/v2/{userId}/contacts/{contactId}/workInfo/communication")
public class ContactsWorkInfoCommunicationController {
    private static Logger logger = Logger
            .getLogger(ContactsWorkInfoCommunicationController.class);

    /**
     * Retrieve Contacts WorkInfo Communication.
     * 
     * @param userId String
     * @param sessionId String
     * @param contactId String
     * @param uriInfo UriInfo
     * 
     * @return Response - Contacts WorkInfo Communication
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactsWorkInfoCommunication(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Contacts WorkInfo Communication Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            final Communication communication = ((IContactsWorkInfoCommunicationService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsWorkInfoCommunicationService
                                    .name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, communication);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update workinfo communication.
     * 
     * @param userId String
     * @param sessionId String
     * @param contactId String
     * @param inputParams MultivaluedMap<String, String>
     * 
     * @return Response - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateContactsWorkInfoCommunication(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update contact Name Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.contactId.name(), contactId);
            ((IContactsWorkInfoCommunicationService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsWorkInfoCommunicationService
                                    .name())).update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
