/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedDomainService;

import org.apache.log4j.Logger;

/**
 * AllowedDomainsController class that controls all the requests for Mailbox
 * AllowedDomains.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/base/allowedDomains")
public class AllowedDomainsController {
    private static Logger logger = Logger
            .getLogger(AllowedDomainsController.class);

    /**
     * Create MultipleAllowedDomain.
     *
     * @param email String
     * @param allowedDomain String
     * @param inputParams MultivaluedMap<String, String>
     * @return AllowedDomains object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleAllowedDomain(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create AllowedDomain Request Received for eamil(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IAllowedDomainService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedDomainService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Create AllowedDomain.
     *
     * @param email String
     * @param allowedDomain String
     * @param inputParams MultivaluedMap<String, String>
     * @return AllowedDomains object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{allowedDomain}")
    public Response createAllowedDomain(@PathParam("email") final String email,
            @PathParam("allowedDomain") final String allowedDomain,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create AllowedDomain Request Received for eamil(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams
                    .add(MailboxProperty.allowedDomain.name(), allowedDomain);

            ((IAllowedDomainService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedDomainService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve AllowedDomains.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return AllowedDomains object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllowedDomains(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get AllowedDomain Request Received for eamil(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> allowedDomains = ((IAllowedDomainService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.AllowedDomainService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, allowedDomains);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update AllowedDomains.
     *
     * @param email String
     * @param oldAllowedDomain String
     * @param newAllowedDomain String
     * @param inputParams MultivaluedMap<String, String>
     * @return AllowedDomains object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("{oldAllowedDomain}/{newAllowedDomain}")
    public Response updateAllowedDomains(
            @PathParam("email") final String email,
            @PathParam("oldAllowedDomain") final String oldAllowedDomain,
            @PathParam("newAllowedDomain") final String newAllowedDomain,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update AllowedDomain Request Received for eamil(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldAllowedDomain.name(),
                    oldAllowedDomain);
            inputParams.add(MailboxProperty.newAllowedDomain.name(),
                    newAllowedDomain);
            ((IAllowedDomainService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedDomainService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete AllowedDomains.
     *
     * @param email String
     * @param allowedDomain String
     * @param uriInfo UriInfo
     * @return AllowedDomains object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/{allowedDomain}")
    public Response deleteAllowedDomains(
            @PathParam("email") final String email,
            @PathParam("allowedDomain") final String allowedDomain,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete AllowedDomain Request Received for eamil(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.allowedDomain.name(), allowedDomain);
            ((IAllowedDomainService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AllowedDomainService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * List AllowedDomains.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return AllowedDomains object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public Response listAllowedDomains(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "List AllowedDomain Request Received for eamil(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> allowedDomains = ((IAllowedDomainService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.AllowedDomainService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, allowedDomains);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
