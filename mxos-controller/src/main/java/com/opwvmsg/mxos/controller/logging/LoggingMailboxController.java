/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */

package com.opwvmsg.mxos.controller.logging;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailForwardService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;

/**
 * API calls via MAA logging.
 * 
 * @author Aricent
 */

@Path("/logging/mailbox/v2")
public class LoggingMailboxController {
    private static Logger logger = Logger
            .getLogger(LoggingMailboxController.class);

    /**
     * Create Mailbox via MAA for logging.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - MailboxId object
     * @throws Exception Exception
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{email}")
    public Response createMailbox(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) throws Exception {
        if (logger.isInfoEnabled()) {
            logger.info("Create Mailbox Logging Request Recieved for email("
            		+ email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            final long mailboxId = ((IMailboxService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.LoggingMailboxService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailboxId);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Mailbox via MAA for logging.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/{email}")
    public Response deleteMailbox(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Delete Mailbox Logging Request Recieved for email("
                        + email + ")");
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            ((IMailboxService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingMailboxService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Status via MAA for logging.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response Response
     */
    @Path("/{email}/base")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateBase(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update Mailbox Base Logging Request Recieved for email("
                        + email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailboxBaseService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingMailboxBaseService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update password via MAA for logging.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("{email}/credentials")
    public Response updateCredentials(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Credentials Logging Request Received for eamil(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((ICredentialService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingCredentialsService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update OOO via MAA for logging.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response Response
     */
    @Path("/{email}/mailReceipt")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateOutofOffice(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update Mailbox OOO Logging Request Recieved for email("
                        + email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailReceiptService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingMailReceiptService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create Alias via MAA for logging.
     *
     * @param email String
     * @param emailAlias String
     * @param inputParams MultivaluedMap<String, String>
     * @return Alias object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{email}/base/emailAliases/{emailAlias}")
    public Response createAlias(@PathParam("email") final String email,
            @PathParam("emailAlias") final String emailAlias,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Add Alias Logging Request Recieved for email("
                        + email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.emailAlias.name(), emailAlias);
            ((IEmailAliasService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingEmailAliasService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Alias via MAA for logging.
     *
     * @param email String
     * @param oldEmailAlias String
     * @param newEmailAlias String
     * @param inputParams MultivaluedMap<String, String>
     * @return Alias object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{email}/base/emailAliases/{oldEmailAlias}/{newEmailAlias}")
    public Response updateAlias(@PathParam("email") final String email,
            @PathParam("oldEmailAlias") final String oldEmailAlias,
            @PathParam("newEmailAlias") final String newEmailAlias,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update Alias Logging Request Recieved for email("
                        + email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams
                    .add(MailboxProperty.oldEmailAlias.name(), oldEmailAlias);
            inputParams
                    .add(MailboxProperty.newEmailAlias.name(), newEmailAlias);
            ((IEmailAliasService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingEmailAliasService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Alias via MAA for logging.
     *
     * @param email String
     * @param emailAlias String
     * @param uriInfo UriInfo
     * @return Alias object
     */
    @Path("/{email}/base/emailAliases/{emailAlias}")
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteAlias(@PathParam("email") final String email,
            @PathParam("emailAlias") final String emailAlias,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Delete Alias Logging Request Recieved for email("
                        + email + ")");
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(MailboxProperty.emailAlias.name(),
                    emailAlias);
            ((IEmailAliasService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingEmailAliasService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Add MailForwardingAddress via MAA for logging.
     *
     * @param email String
     * @param frdAdrs String
     * @param inputParams MultivaluedMap<String, String>
     * @return ForwardingAddress object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{email}/mailReceipt/forwardingAddress/{forwardingAddress}")
    public Response addMailForwdingAddress(
            @PathParam("email") final String email,
            @PathParam("forwardingAddress") final String frdAdrs,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Add ForwardAddress Logging Request Recieved for email("
                        + email + ")");
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.forwardingAddress.name(), frdAdrs);
            ((IMailForwardService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingMailForwardService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
