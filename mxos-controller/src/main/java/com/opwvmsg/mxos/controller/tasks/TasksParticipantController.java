/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.tasks;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksParticipantService;
import com.opwvmsg.mxos.task.pojos.Participant;

/**
 * ParticipantsController class that controls all the requests for tasks.
 * 
 * @author mxos-dev
 */
@Path("/task/v2/{userId}/tasks/{taskId}/participants")
public class TasksParticipantController {
    private static Logger logger = Logger
            .getLogger(TasksParticipantController.class);

    /**
     * Create Tasks Participant.
     * 
     * @param userId String
     * @param taskId String
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * 
     * @return Response - Empty object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createTasksParticipant(
            @PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update tasks Participant Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(TasksProperty.userId.name(), userId);
            inputParams.add(TasksProperty.taskId.name(), taskId);
            ((ITasksParticipantService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.TasksParticipantService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Retrieve task participants.
     * 
     * @param userId String
     * @param taskId String
     * @param participantId String
     * @param uriInfo UriInfo
     * 
     * @return Response - participant object
     */
    @Path("/{participantId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTasksParticipant(
            @PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @PathParam("participantId") final String participantId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Tasks Participant Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.taskId.name(), taskId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.participantId.name(), participantId);
            final Participant recurrence = ((ITasksParticipantService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksParticipantService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, recurrence);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Tasks Participant.
     * 
     * @param userId String
     * @param taskId String
     * @param participantId String
     * @param inputParams MultivaluedMap<String, String>
     * 
     * @return Response - Empty object
     */
    @Path("/{participantId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateTasksParticipant(
            @PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @PathParam("participantId") final String participantId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update tasks Participant Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(TasksProperty.userId.name(), userId);
            inputParams.add(TasksProperty.taskId.name(), taskId);
            inputParams.add(TasksProperty.participantId.name(), participantId);
            ((ITasksParticipantService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.TasksParticipantService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Retrieve task participants.
     * 
     * @param userId String
     * @param taskId String
     * @param participantId String
     * @param uriInfo UriInfo
     * 
     * @return Response - participant object
     */
    @Path("/{participantId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTasksParticipant(
            @PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @PathParam("participantId") final String participantId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Tasks Participant Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.taskId.name(), taskId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.participantId.name(), participantId);
            ((ITasksParticipantService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksParticipantService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Retrieve List of task participants.
     * 
     * @param userId String
     * @param taskId String
     * @param uriInfo UriInfo
     * 
     * @return Response - participant object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listTasksParticipants(
            @PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Tasks Participant Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.taskId.name(), taskId);
            final List<Participant> list = ((ITasksParticipantService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksParticipantService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, list);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
