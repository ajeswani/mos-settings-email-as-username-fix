/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.cos;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosWebMailFeaturesAddressBookService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosWebMailFeaturesService;

import org.apache.log4j.Logger;

/**
 * CosWebMailFeaturesController class that controls all the requests for
 * WebMailFeatures Cos service.
 *
 * @author mxos-dev
 */
@Path("/cos/v2/{cosId}/webMailFeatures")
public class CosWebMailFeaturesController {
    private static Logger logger = Logger
            .getLogger(CosWebMailFeaturesController.class);

    /**
     * Retrieve WebMailFeatures for Cos service.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return WebMailFeatures object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWebMailFeatures(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Get WebMailFeatures Request Received for cosId: "
                    + cosId);
        }
        try {
            ICosWebMailFeaturesService service = (ICosWebMailFeaturesService)
            MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosWebMailFeaturesService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final WebMailFeatures webMailFeatures = service.read(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, webMailFeatures);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update WebMailFeatures for Cos service.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateWebMailFeatures(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update WebMailFeatures Request Received for cosId: "
                    + cosId);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ICosWebMailFeaturesService service = (ICosWebMailFeaturesService)
            MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosWebMailFeaturesService.name());
            service.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve WebMailFeaturesAddressBook for Cos service.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return AddressBook object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/addressBookFeatures")
    public Response getAddressBook(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Get WebMailFeaturesAddressBook Request Received for cosId: "
                    + cosId);
        }
        try {
            ICosWebMailFeaturesAddressBookService service = (ICosWebMailFeaturesAddressBookService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.CosWebMailFeaturesAddressBookService
                                    .name());
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final AddressBookFeatures addressBook = service.read(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, addressBook);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update WebMailFeaturesAddressBook for Cos service.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/addressBookFeatures")
    public Response updateAddressBook(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update WebMailFeaturesAddressBook Request Received for cosId: "
                    + cosId);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ICosWebMailFeaturesAddressBookService service = (ICosWebMailFeaturesAddressBookService)
            MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosWebMailFeaturesAddressBookService.name());
            service.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
