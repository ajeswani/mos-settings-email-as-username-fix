/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.cos;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMessageEventRecordsService;

import org.apache.log4j.Logger;

/**
 * CosInternalInfoController class that controls all the requests for
 * InternalInfo Cos service.
 *
 * @author mxos-dev
 */
@Path("/cos/v2/{cosId}/internalInfo")
public class CosInternalInfoController {
    private static Logger logger = Logger
            .getLogger(CosInternalInfoController.class);

    /**
     * Retrieve InternalInfo for Cos service.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return InternalInfo object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInternalInfo(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Get InternalInfo Request Received for cosId: "
                    + cosId);
        }
        try {
            ICosInternalInfoService service = (ICosInternalInfoService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosInternalInfoService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final InternalInfo internalInfo = service.read(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, internalInfo);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update InternalInfo for Cos service.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateInternalInfo(@PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update InternalInfo Request Received for cosId: "
                    + cosId);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ICosInternalInfoService service = (ICosInternalInfoService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosInternalInfoService.name());
            service.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MessageEventRecords for Cos service.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return MessageEventRecords object
     */
    @GET
    @Path("/messageEventRecords")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessageEventRecords(
            @PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info("Get InternalInfo:MessageEventRecords" +
            		" Request Received for cosId: " + cosId);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final MessageEventRecords msgEventRecords =
                    ((ICosMessageEventRecordsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.CosMessageEventRecordsService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, msgEventRecords);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MessageEventRecords for COS.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     */
    @POST
    @Path("/messageEventRecords")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMessageEventRecords(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            logger.info("Update InternalInfo:MessageEventRecords Request" +
            		" Received for cosId: " + cosId);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosMessageEventRecordsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.CosMessageEventRecordsService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
