/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.InvalidSessionException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.exception.SmsGatewayException;
import com.opwvmsg.mxos.exception.ServiceUnavailableException;
/**
 * Util class of controllers.
 * 
 * @author mxos-dev
 */
public final class ControllerUtils {

    /**
     * Default private constructor.
     */
    private ControllerUtils() {

    }

    /**
     * Get HTTP Status.
     * 
     * @param mxosException MxOSException
     * @return Status Status
     */
    public static Response.Status generateHTTPStatus(Exception mxosException) {
        if (mxosException instanceof ApplicationException) {
            return Response.Status.INTERNAL_SERVER_ERROR;
        } else if (mxosException instanceof AuthorizationException) {
            return Response.Status.FORBIDDEN;
        } else if (mxosException instanceof InvalidRequestException) {
            return Response.Status.BAD_REQUEST;
        } else if (mxosException instanceof NotFoundException) {
            return Response.Status.NOT_FOUND;
        } else if (mxosException instanceof SmsGatewayException) {
            return Response.Status.INTERNAL_SERVER_ERROR;
        } else if (mxosException instanceof InvalidSessionException) {
            return Response.Status.UNAUTHORIZED;
        } else if (mxosException instanceof ServiceUnavailableException) {
                return Response.Status.SERVICE_UNAVAILABLE;
        } else if (mxosException instanceof MxOSException) {
            return Response.Status.INTERNAL_SERVER_ERROR;
        } else {
            return Response.Status.INTERNAL_SERVER_ERROR;
        }
    }

    /**
     * Method to generate Response.
     * 
     * @param outputType String
     * @param httpStatus Status
     * @param outputPojo Object
     * @return Response Response
     */
    public static Response generateResponse(String outputType,
            Response.Status httpStatus, Object outputPojo) {
        ResponseBuilder r = Response.status(httpStatus);
        r.type(outputType);
        r.entity(outputPojo);
        return r.build();
    }

    /**
     * Method to generate Response.
     * 
     * @param outputType String
     * @param httpStatus Status
     * @param outputPojo Object
     * @param cookies Cookies
     * @return Response Response
     */
    public static Response generateResponseWithCookies(String outputType,
            Response.Status httpStatus, Object outputPojo, String cookieString) {
        ResponseBuilder r = Response.status(httpStatus);
        r.type(outputType);
        r.entity(outputPojo);
        if (cookieString != null && !cookieString.isEmpty()) {
            String[] cookies = cookieString.split(";");
            if (cookies != null) {
                for (String cookie : cookies) {
                    String[] cookieItem = cookie.split("=");
                    if (cookieItem != null && cookieItem.length>=2) {
                        NewCookie newCookie = new NewCookie(cookieItem[0], cookieItem[1]);
                        r.cookie(newCookie);
                    }
                }
            }
        }
        return r.build();
    }

    /**
     * Method to generate Error Response.
     * 
     * @param outputType String
     * @param httpStatus Status
     * @param mxosException MxOSException
     * @return Response Response
     */
    public static Response generateErrorResponse(String outputType,
            Response.Status httpStatus, Exception mxosException) {
        MxOSException e = null;
        if (mxosException == null) {
            e = new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name());
        } else {
            if (mxosException instanceof MxOSException) {

                try {
                    ErrorCode errorCode = ErrorCode
                            .valueOf(((MxOSException) mxosException).getCode());
                    switch (errorCode) {
                    case LDP_CONNECTION_ERROR:
                    case MSS_CONNECTION_ERROR:
                    case PSX_CONNECTION_ERROR:
                    case MXS_CONNECTION_ERROR:
                    case ABS_CONNECTION_ERROR:
                    case RMI_CONNECTION_ERROR:
                    case OXS_CONNECTION_ERROR:
                        e = new ServiceUnavailableException(errorCode.name(),
                                mxosException);
                        break;
                    default:
                        e = new MxOSException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                mxosException);
                    }
                } catch (IllegalArgumentException iae) {
                    e = new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name());
                }
            }
        }
        if (mxosException instanceof MxOSException) {
            e = (MxOSException) mxosException;
        }
        if (mxosException.getCause() != null) {
            e.setLongMessage(mxosException.getCause().getMessage());
        } else {
            e.setLongMessage(mxosException.getMessage());
        }
        return generateResponse(outputType, httpStatus, e.toString());
    }
}
