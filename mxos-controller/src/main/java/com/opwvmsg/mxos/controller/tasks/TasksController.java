/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.tasks;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksService;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksBaseService;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataParam;

/**
 * taskController class that controls all the requests for tasks.
 * 
 * @author mxos-dev
 */
@Path("/task/v2/{userId}")
public class TasksController {
    private static Logger logger = Logger.getLogger(TasksController.class);

    /**
     * Login user.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - contactId object
     * @throws Exception Exception
     */
    @Path("/login")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response loginUser(
            @PathParam("userId") final String userId,
            final MultivaluedMap<String, String> inputParams) throws Exception {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Login Request Received for userId (").append(userId)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(ExternalProperty.entity.name(),
                    Entity.TASKS.toString());
            inputParams.add(ExternalProperty.userId.name(), userId);
            final ExternalSession session = ((IExternalLoginService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.ExternalLoginService.name()))
                    .login(inputParams);
            return ControllerUtils.generateResponseWithCookies(
                    MediaType.APPLICATION_JSON, Response.Status.OK, session,
                    session.getCookieString());
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Logout user.
     * 
     * @param userId String
     * @param uriInfo
     * @return Response
     * @throws Exception Exception
     */
    @Path("/logout")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response logoutUser(
            @PathParam("userId") final String userId,
            @Context final UriInfo uriInfo) throws Exception {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Logout Request Received for userId (").append(userId)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(ExternalProperty.entity.name(),
                    Entity.TASKS.toString());
            uriInfo.getQueryParameters().add(ExternalProperty.userId.name(),
                    userId);
            ((IExternalLoginService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ExternalLoginService.name()))
                    .logout(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create Tasks.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - taskId object
     * @throws Exception Exception
     */
    @Path("/tasks")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createTask(@PathParam("userId") final String userId,
            final MultivaluedMap<String, String> inputParams) throws Exception {

        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Task Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(TasksProperty.userId.name(), userId);
            final long cId = ((ITasksService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.TasksService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, cId);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create multiple tasks.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - taskId object
     * @throws Exception Exception
     */
    @Path("/tasks")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response createMultipleTasks(
            @PathParam("userId") final String userId,
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @FormDataParam("sessionId") final String sessionId,
            @FormDataParam("cookieString") final String cookieString)
            throws Exception {

        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create multiple Tasks Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            MultivaluedMap<String, String> inputParams = new MultivaluedMapImpl();
            inputParams.add(TasksProperty.userId.name(), userId);
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
            inputParams.add(TasksProperty.tasksList.name(),
                    writer.toString());
            if (sessionId != null && !sessionId.isEmpty()) {
                inputParams.add(TasksProperty.sessionId.name(),
                        URLDecoder.decode(sessionId, MxOSConstants.UTF8));
            }
            if (cookieString != null && !cookieString.isEmpty()) {
                inputParams.add(TasksProperty.cookieString.name(),
                        URLDecoder.decode(cookieString, MxOSConstants.UTF8));
            }
            final List<Long> cIds = ((ITasksService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.TasksService.name()))
                    .createMultiple(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, cIds);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Delete Task.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Path("/tasks/{taskId}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response delete(@PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete task Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.taskId.name(), taskId);
            ((ITasksService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.TasksService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Task.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @Path("/tasks")
    @DELETE
    @Consumes("application/x-www-form-urlencoded")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteAll(@PathParam("userId") final String userId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete task Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            ((ITasksService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.TasksService.name()))
                    .deleteAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of task objects.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - List of task objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/tasks/list")
    public Response listTasks(@PathParam("userId") final String userId,
            @QueryParam("sessionId") final String sessionId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer("List task Request Received"));
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            final List<Task> taskObjects = ((ITasksService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, taskObjects);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Confirm Task.
     * 
     * @param userId String
     * @param taskId String
     * @param uriInfo UriInfo
     * @return Response - Task Base object
     */
    @GET
    @Path("/tasks/{taskId}/confirm")
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmTask(@PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Tasks Base Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.taskId.name(), taskId);
            ((ITasksService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksService.name()))
                    .confirm(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve task Base.
     * 
     * @param userId String
     * @param taskId Integer
     * @param uriInfo UriInfo
     * @return Response - Task Base object
     */
    @GET
    @Path("/tasks/{taskId}/base")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTaskBase(@PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Tasks Base Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.taskId.name(), taskId);
            final TaskBase taskBase = ((ITasksBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksBaseService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, taskBase);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update task Base.
     * 
     * @param userId String
     * @param taskId Integer
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Path("/tasks/{taskId}/base")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updatetTaskBase(@PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update task Base Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(TasksProperty.userId.name(), userId);
            inputParams.add(TasksProperty.taskId.name(), taskId);
            ((ITasksBaseService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.TasksBaseService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of task Base objects.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - List of task Base objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/tasks/base/list")
    public Response listTaskBase(@PathParam("userId") final String userId,
            @QueryParam("sessionId") final String sessionId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer("List tasks base Request Received"));
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            final List<TaskBase> taskBaseObjects = ((ITasksBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksBaseService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, taskBaseObjects);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
