/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.cos;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailAccessService;

import org.apache.log4j.Logger;

/**
 * CosMailAccessController class that controls all the requests for MailAccess
 * COS.
 *
 * @author mxos-dev
 */
@Path("/cos/v2/{cosId}/mailAccess")
public class CosMailAccessController {
    private static Logger logger = Logger
            .getLogger(CosMailAccessController.class);

    /**
     * Retrieve MailAccess COS.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return MailAccess object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailAccess(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "GET MailAccess Request Received for cosId(").append(cosId)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final MailAccess mailAccess = ((ICosMailAccessService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosMailAccessService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailAccess);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailAccess COS.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailAccess(@PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailAccess Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosMailAccessService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosMailAccessService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}