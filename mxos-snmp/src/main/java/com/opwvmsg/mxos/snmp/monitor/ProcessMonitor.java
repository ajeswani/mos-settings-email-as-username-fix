/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.snmp.monitor;

/**
 * Monitors MxOS process by check if the REST API is working.
 * 
 * @author mxos-dev
 * 
 */
public interface ProcessMonitor {

    /**
     * Verifies REST API controller is working at a specific interval.
     * 
     * @return boolean
     */
    public boolean checkController();

}
