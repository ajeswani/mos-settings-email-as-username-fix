/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.snmp.setup;

import java.net.UnknownHostException;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ReflectionException;

import com.adventnet.utils.jmx.Utilities;

/**
 * Interface for Notification Mbean.
 * 
 * @author ajeswani
 */
public class NotificationBroadcasterSupportImpl extends
        NotificationBroadcasterSupport implements DynamicMBean {
    private long seqnum = 0l;
    private static NotificationBroadcasterSupportImpl notificationBean = 
        new NotificationBroadcasterSupportImpl();

    /**
     * Default Constructor.
     */
    private NotificationBroadcasterSupportImpl() {
    }

    /**
     * Method to get singleton instance.
     * 
     * @return NotificationBroadcasterSupportImpl
     */
    public static NotificationBroadcasterSupportImpl getInstance() {
        return notificationBean;
    }

    /**
     * Get the MbeanInfo.
     * @return MBeanInfo
     */
    public MBeanInfo getMBeanInfo() {
        String s = getClass().getName();
        String s1 = "A sample Dynamic MBean";
        MBeanAttributeInfo[] attrs = new MBeanAttributeInfo[0];
        MBeanOperationInfo[] opers = new MBeanOperationInfo[1];
        try {
            opers[0] = new MBeanOperationInfo("sendNotification", this
                    .getClass().getMethod("sendNotification",
                            new Class[] {Notification.class }));
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new MBeanInfo(s, s1, attrs, null, opers, null);
    }

    public Object getAttribute(String s) throws AttributeNotFoundException,
            MBeanException, ReflectionException {
        return Utilities.getAttribute(this, s);
    }

    /**
     * Get attribute corresponding to an array of key.
     * 
     * @return AttributeList
     */
    public AttributeList getAttributes(String s[]) {
        return Utilities.getAttributes(this, s);
    }

    public void setAttribute(Attribute attribute)
            throws AttributeNotFoundException, InvalidAttributeValueException,
            MBeanException, ReflectionException {
        Utilities.setAttribute(this, attribute);
    }

    /**
     * Set the attributes.
     * 
     * @param AttributeList
     */
    public AttributeList setAttributes(AttributeList attributelist) {
        return Utilities.setAttributes(this, attributelist);
    }

    /**
     * Invoke a method.
     * 
     * @param s String
     * @param aobj Object[][]
     * @param as String[]
     * @throws MBeanException
     * @throws ReflectionException
     */
    public Object invoke(String s, Object aobj[], String as[])
            throws MBeanException, ReflectionException {
        return Utilities.invoke(this, s, aobj, as);
    }

    /**
     * Send start Notification.
     * 
     * @param app String
     */
    public void sendStartNotif(String app) {
        try {
            Notification notif = new Notification("mosNotification",
                    "MXOS Process", seqnum++, "MXOS " + app
                            + " process is started/restarted on host:"
                            + getHostname());
            sendNotification(notif);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Send stop Notification.
     * 
     * @param app String
     */
    public void sendStopNotif(String app) {
        try {
            Notification notif = new Notification("mosNotification",
                    "MXOS Process", seqnum++, "MXOS " + app
                            + " process is stopped on host:" + getHostname());
            sendNotification(notif);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Get the host Name.
     * @return String
     * @throws UnknownHostException
     */
    private String getHostname() throws UnknownHostException {
        String localHostname = "localhost";
        java.net.InetAddress localMachine = null;
        localMachine = java.net.InetAddress.getLocalHost();
        localHostname = localMachine.getHostName();
        return localHostname;
    }
}
