/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.snmp.monitor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.util.Iterator;
import java.util.Properties;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.xml.DOMConfigurator;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.setup.NotificationBroadcasterSupportImpl;
import com.opwvmsg.mxos.jmx.setup.RegisterSNMPMBeans;
import com.opwvmsg.mxos.jmx.setup.SNMPConstants;

/**
 * ProcessHandler to monitor MxOS.
 * 
 * @author mxos-dev
 */
public class ProcessHandler {

    private static final String PROCESS_MONITOR_INTERVAL = "processMonitorInterval";
    private static final String PROCESS_MONITOR_URL = "processMonitorUrl";

    public static void main(String args[]) throws MalformedObjectNameException,
            InstanceAlreadyExistsException, MBeanRegistrationException,
            NotCompliantMBeanException, MxOSException, NumberFormatException,
            FileNotFoundException, IOException {

        loadMxOSProperties();

        loadLog4jConfig();

        registerNotificationMbean();

        RegisterSNMPMBeans.setupSNMP();

        String serverUrl = System.getProperty(PROCESS_MONITOR_URL);

        while (true) {

            ProcessMonitor processMonitor = new ProcessMonitorImpl(serverUrl);
            if (!processMonitor.checkController()) {
                NotificationBroadcasterSupportImpl.getInstance().sendStopNotif(
                        "Server");
            }

            try {
                Thread.sleep(Long.valueOf(System
                        .getProperty(PROCESS_MONITOR_INTERVAL)));
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    private static void registerNotificationMbean()
            throws MalformedObjectNameException,
            InstanceAlreadyExistsException, MBeanRegistrationException,
            NotCompliantMBeanException {
        // register notification bean
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        // register connection pools mbean
        ObjectName name = new ObjectName(
                SNMPConstants.NOTIFICATION_BROADCASTER_MBEAN);
        NotificationBroadcasterSupportImpl nbsi = NotificationBroadcasterSupportImpl
                .getInstance();
        if (!mbs.isRegistered(name)) {
            mbs.registerMBean(nbsi, name);
        }
    }

    /**
     * Load Mx Os properties.
     * 
     * @throws MxOSException
     */
    public static void loadMxOSProperties() throws MxOSException {

        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }

        Properties config = null;
        InputStream in = null;
        try {
            in = new FileInputStream(home + "/config/mxos.properties");
            config = new Properties();
            try {
                config.load(in);
            } catch (IOException e) {
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        } catch (FileNotFoundException e1) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e1);
        }

        try {
            Iterator<Object> keys = config.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                System.setProperty(key, config.getProperty(key));
            }
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**
     * Load log4j configuration.
     * 
     * @throws MxOSException
     */
    public static void loadLog4jConfig() throws MxOSException {
        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }
        String log4jFile = home + "/config/log4j.xml";
        // init log4j configuration from property file
        DOMConfigurator.configure(log4jFile);
    }
}
