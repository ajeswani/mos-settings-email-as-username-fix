/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via REST API of MxOS.
 *
 * @author mxos-dev
 */
public class RestMailboxService extends AbstractRestService implements IMailboxService {
    private static Logger logger = Logger.getLogger(RestMailboxService.class);
    
    @Override
    public long create(final Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_SUB_URL,
                pool.isCustom(), inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
            final Long mailboxId = resp.getEntity(Long.class); 
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return mailboxId;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_SUB_URL,
                pool.isCustom(), inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.delete(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public Mailbox read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.get(subUrl, inputParams);
            final Mailbox mailbox = resp.getEntity(Mailbox.class);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return mailbox;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Mailbox> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOXES_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.get(subUrl.toString(), inputParams);
            final List<Mailbox> mailboxes = resp.getEntity(List.class);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return mailboxes;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

}
