/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailForwardService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

/**
 * MailForward service exposed to client which is responsible for doing basic
 * forwarding address related activities e.g create, update, read and delete
 * forwarding addresses via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMailForwardService extends AbstractRestService implements IMailForwardService {
    private static Logger logger = Logger
            .getLogger(RestMailForwardService.class);
    
    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp;
        List<String> forwardingAddress = inputParams
                .get(MailboxProperty.forwardingAddress.name());
        if (forwardingAddress != null && forwardingAddress.size() > 1) {
            temp = new StringBuffer(RestConstants.FORWARDING_ADDRESS_SUB_URL)
                    .toString();
        } else {
            temp = new StringBuffer(
                    RestConstants.SINGLE_FORWARDING_ADDRESS_SUB_URL).toString();
        }
        final String subUrl = RestConstants.getFinalSubUrl(temp,
                pool.isCustom(), inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<String> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.FORWARDING_ADDRESS_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.get(subUrl, inputParams);
            List<String> list = resp.getEntity(
                    new GenericType<List<String>>() { });
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return list;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered",
                    e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.FORWARDING_ADDRESS_SUB_URL).append(
                "/{oldForwardingAddress}/{newForwardingAddress}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.post(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.FORWARDING_ADDRESS_SUB_URL).append(
                "/{forwardingAddress}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.delete(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
