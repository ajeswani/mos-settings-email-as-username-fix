package com.opwvmsg.mxos.rest.service.notify;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.enums.NotificationProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.notify.INotifyService;
import com.opwvmsg.mxos.notify.pojos.Notify;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;

public class RestNotifyService extends AbstractRestService implements INotifyService {
    private static Logger logger = Logger.getLogger(RestNotifyService.class);
    
    @Override
    public Notify read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.NOTIFY_TOPIC, inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            final ClientResponse r = rest.get(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return r.getEntity(Notify.class);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.NOTIFY_TOPIC, inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            rest.delete(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }
    
    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        logger.error("CREATE not supported for Notify.");
        throw new UnsupportedOperationException(
                "CREATE not supported for Notify.");
    }
    
    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        logger.error("UPDATE not supported for Notify.");
        throw new UnsupportedOperationException(
                "UPDATE not supported for Notify.");
    }

    @Override
    public void publish(Map<String, List<String>> inputParams)
            throws MxOSException {
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.NOTIFY_TOPIC_PUBLISH, inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            rest.postJson(subUrl, inputParams.get(NotificationProperty.notifyMessage.name()).get(0));
        } catch (final MxOSException e) {
            e.printStackTrace();
            throw e;
        } catch (final Exception e) {
            e.printStackTrace();
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

}
