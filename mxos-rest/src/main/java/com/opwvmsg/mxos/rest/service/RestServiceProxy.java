/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.ServiceUnavailableException;
import com.opwvmsg.mxos.interfaces.service.RetryStrategy;

/**
 * Class for Proxy Rest Service.
 *
 * @author mxos-dev
 */
public class RestServiceProxy implements java.lang.reflect.InvocationHandler {
    private static Logger logger = Logger.getLogger(RestServiceProxy.class);

    // List of error codes which will be used for retry operation
    private static Set<String> RETRY_ERROR_CODES = new HashSet<String>() {
        {
            add(MailboxError.MBX_UNABLE_TO_MSSLINKINFO_GET.name());
            add(MailboxError.MBX_UNABLE_TO_GET_MAILBOX_ACCESS_INFO.name());
            add(MailboxError.MBX_UNABLE_TO_GET.name());
            add(ErrorCode.LDP_CONNECTION_ERROR.name());
            add(ErrorCode.MXS_CONNECTION_ERROR.name());
            add(ErrorCode.MSS_CONNECTION_ERROR.name());
            add(ErrorCode.ABS_CONNECTION_ERROR.name());
            add(ErrorCode.GEN_INTERNAL_ERROR.name());
            // TODO: Eventually this should be removed and we need
            // retries to be done within mOS
            add(MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name());

            // TODO: This is a temporary hack as a "catch - all" from everything
            // coming from mOS.  Eventually we want to do one of the fw things:
            //	* Develop our custom provider to parse text/html
            //  * Get rid of Jersey in the client side
            add(ErrorCode.MXS_GENERAL_ERROR.name());
            
            //Error codes for AddressBook Migration
            //AddressBook Login
            add(AddressBookError.ABS_INVALID_USERNAME.name());
            add(AddressBookError.ABS_UNABLE_TO_LOGIN.name());
         
            //Contacts API
            add(AddressBookError.ABS_CONTACTS_MULTIPLE_UNABLE_TO_CREATE.name());
         
            //Contacts Base API
            add(AddressBookError.ABS_CONTACTBASE_UNABLE_TO_UPDATE.name());
         
            //Contacts Name API
            add(AddressBookError.ABS_CONTACTNAME_UNABLE_TO_UPDATE.name());
         
            //Contacts PersonalInfo API
            add(AddressBookError.ABS_PERSONALINFO_UNABLE_TO_UPDATE.name());
            add(AddressBookError.ABS_PERSONALINFO_ADDRESS_UNABLE_TO_UPDATE.name());
            add(AddressBookError.ABS_PERSONALINFO_COMMUNICATION_UNABLE_TO_UPDATE.name());
         
        //Contacts PersonalInfo Event
            add(AddressBookError.ABS_PERSONALINFO_EVENTS_UNABLE_TO_UPDATE.name());
         
            //Contacts WorkInfo API
            add(AddressBookError.ABS_WORKINFO_UNABLE_TO_UPDATE.name());
            add(AddressBookError.ABS_WORKINFO_ADDRESS_UNABLE_TO_UPDATE.name());
            add(AddressBookError.ABS_WORKINFO_COMMUNICATION_UNABLE_TO_UPDATE.name());
         
            //Contacts folder
            add(AddressBookError.ABS_CONTACTS_FOLDER_UNABLE_TO_GET.name());
         
            //Contacts image
            add(AddressBookError.ABS_CONTACTS_IMAGE_UNABLE_TO_UPDATE.name());
         
            //Groups
            add(AddressBookError.ABS_GROUPS_MULTIPLE_UNABLE_TO_CREATE.name());
         
            add(AddressBookError.ABS_GROUPBASE_UNABLE_TO_UPDATE.name());
            add(AddressBookError.ABS_GROUPS_MEMBERS_UNABLE_TO_PUT.name());
            add(AddressBookError.ABS_GROUPS_EXTERNAL_MEMBERS_UNABLE_TO_PUT.name());
         
            //Connection error
            add(AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name());
         
            //AddressBook API
            add(AddressBookError.ABS_UNABLE_TO_DELETE.name());

        }
    };

    private Object obj;
    private int retryAttempt = 0;
    private Retryer<Object> retryer;
    private RetryStrategy retryStrategy;

    public static Object newInstance(Object obj, RetryStrategy retryStrategy) {
        return java.lang.reflect.Proxy.newProxyInstance(obj.getClass()
                .getClassLoader(), obj.getClass().getInterfaces(),
                new RestServiceProxy(obj, retryStrategy));
    }

    private RestServiceProxy(Object obj, RetryStrategy retryStrategy) {
        this.obj = obj;
        this.retryStrategy = retryStrategy;
        retryer = RetryerBuilder
                .<Object> newBuilder()
                .retryIfExceptionOfType(ServiceUnavailableException.class)
                .withWaitStrategy(
                        WaitStrategies.incrementingWait(
                                retryStrategy.getSleepBetweenRetries(),
                                TimeUnit.MILLISECONDS,
                                retryStrategy.getSleepBetweenRetries(),
                                TimeUnit.MILLISECONDS))
                .withStopStrategy(
                        StopStrategies.stopAfterAttempt(retryStrategy
                                .getNumRetries())).build();
    }

    public Object invoke(Object proxy, final Method m, final Object[] args)
            throws Throwable {

        Callable<Object> callable = new Callable<Object>() {
            public Object call() throws Exception {
                Object result = null;
                try {
                    logger.trace(new StringBuilder("Calling ")
                            .append(obj.getClass().getSimpleName()).append(".")
                            .append(m.getName()).append(" method for ")
                            .append(retryAttempt + 1).append(" time(s)."));
                    result = m.invoke(obj, copyArgs(args));
                    // Method executed successfully, reset retryAttempt
                    retryAttempt = 0;
                } catch (Exception e) {
                    if (e instanceof InvocationTargetException) {
                        Throwable ex = ((InvocationTargetException) e)
                                .getTargetException();
                        if (ex instanceof MxOSException) {
                            MxOSException me = (MxOSException) ex;
                            String errorCode = me.getCode();
                            if (RETRY_ERROR_CODES.contains(errorCode)) {
                                retryAttempt++;
                                MxOSException se = new ServiceUnavailableException(
                                        errorCode);
                                se.setShortMessage(me.getShortMessage());
                                se.setLongMessage(me.getLongMessage());
                                throw se;
                            } else {
                                throw me;
                            }
                        }
                    }
                    throw e;
                }
                return result;
            }
        };

        Object result;
        long start = System.nanoTime();

        try {
            result = retryer.call(callable);
        } catch (RetryException e) {
            if (e.getCause() instanceof MxOSException) {
                throw e.getCause();
            } else {
                throw new MxOSException(ErrorCode.SDK_RETRY_ERROR.name(),
                        "Error still exists after retrying " + retryAttempt
                                + "times...");
            }
        } catch (ExecutionException e) {
            if (e.getCause() instanceof MxOSException) {
                throw e.getCause();
            }
            throw new MxOSException(
                    "Error during retry caused by excecution error", e);
        }

        long end = System.nanoTime();
        logger.trace(String.format("Attempt[" + (retryAttempt + 1)
                + "], %s took %d ns", m.getName(), (end - start)));
        return result;
    }

    private Object[] copyArgs(final Object[] args) {
        Map<String, List<String>> argsOrig = (Map<String, List<String>>) args[0];
        Map<String, List<String>> argsCopy = new HashMap<String, List<String>>();

        for (String key : argsOrig.keySet()) {
            List<String> value = argsOrig.get(key);
            if (key.equals(MailboxProperty.email.name())
                    || key.equals(MailboxProperty.folderName.name())
                    || key.equals(FolderProperty.toFolderName.name())
                    || key.equals(FolderProperty.srcFolderName.name())
                    || key.equals(FolderProperty.folderName.name())
                    || key.equals(MessageProperty.messageId.name())) {
                argsCopy.put(key, new ArrayList<String>(value));
            } else {
                argsCopy.put(key, value);
            }
        }

        final Object[] newArgs = new Object[] { argsCopy };
        return newArgs;
    }
}