/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Credential service exposed to client which is responsible for doing basic
 * Credential related activities e.g read credentials, authenticate and update
 * password via REST API of MxOS.
 *
 * @author mxos-dev
 */
public class RestCredentialService extends AbstractRestService implements ICredentialService {
    private static Logger logger = Logger
            .getLogger(RestCredentialService.class);

    @Override
    public Credentials read(final Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CREDENTIAL_SUB_URL, inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            final ClientResponse r = rest.get(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return r.getEntity(Credentials.class);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        RestCRUD rest = null;
        // call logging api if custom and update 'password'
        final boolean isCustom = pool.isCustom();
        if (isCustom && inputParams != null && inputParams.containsKey(
                MailboxProperty.password.toString())) {
            final Map<String, List<String>> tempParams =
                new HashMap<String, List<String>>();
            tempParams.putAll(inputParams);
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.CREDENTIAL_SUB_URL,
                    pool.isCustom(), tempParams);
            rest = null;
            try {
                rest = pool.borrowObject();
                rest.post(subUrl, tempParams);
            } catch (final MxOSException e) {
                logger.error("MxOSException encountered", e);
                throw e;
            } catch (final Exception e) {
                logger.error("Exception encountered", e);
                throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
            } finally {
                // remove the key "password" entry from inputParams
                inputParams.remove(MailboxProperty.password.toString());
                logger.info("Removed entry for the key "
                        + MailboxProperty.password.toString()
                        + " from inputParams");
                if (rest != null) {
                    try {
                        pool.returnObject(rest);
                    } catch (final Exception e) {
                        logger.error(
                                "Exception encountered in finally clause while returning the connection",
                                e);
                        throw new MxOSException(
                                ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                    }
                }
            }
            // Return if there are no other attributes exist for credentials
            // update.
            if (!(inputParams.size() > 1)) {
                logger.info("No other attributes exist for credentials update. Going to return");
                if (logger.isDebugEnabled()) {
                    logger.debug("end");
                }
                return;
            }
        }
        
        logger.info("End of update " + MailboxProperty.password.toString()
                + " with logging api. Continuing with rest of the api");
        subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CREDENTIAL_SUB_URL, inputParams);
        rest = null;
        try {
            rest = pool.borrowObject();
            rest.post(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void authenticate(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        if (inputParams.get(MailboxProperty.userName.name()) != null) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.AUTH_SUB_URL_USERNAME, inputParams);
        } else {
            subUrl = RestConstants.getFinalSubUrl(RestConstants.AUTH_SUB_URL,
                    inputParams);
        }
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            rest.post(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public Mailbox authenticateAndGetMailbox(Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        if (inputParams.get(MailboxProperty.userName.name()) != null) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.AUTH_AND_GETMAILBOX_SUB_URL_USERNAME,
                    inputParams);
        } else {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.AUTH_AND_GETMAILBOX_SUB_URL, inputParams);
        }
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            final ClientResponse r = rest.post(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return r.getEntity(Mailbox.class);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }
}
