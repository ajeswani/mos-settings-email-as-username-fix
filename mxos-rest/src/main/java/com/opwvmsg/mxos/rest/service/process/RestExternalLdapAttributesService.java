/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.process;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.process.IExternalLdapAttributesService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;


public class RestExternalLdapAttributesService extends AbstractRestService implements IExternalLdapAttributesService {
    private static Logger logger = Logger.getLogger(RestExternalLdapAttributesService.class);

    @Override
    public Map<String, List<String>> readAttributes(
            Map<String, List<String>> inputParams) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("RestExternalLdapAttributesRetrieveService readAttributes() start"));
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GET_EXTLDP_ATTRIBUTE_SUB_URL, inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            final ClientResponse r=rest.get(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("RestExternalLdapAttributesRetrieveService readAttributes() end"));
            }
            if(r.getStatus() == javax.ws.rs.core.Response.Status.OK.getStatusCode())
                return r.getEntity(new GenericType<Map<String,List<String>>>() {});
            else{
                String error=r.getEntity(String.class);
                logger.error("Non 2XX response("+r.getStatus()+" response)received for REST request : "+error);
                throw new Exception(error);
            }
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }


}
