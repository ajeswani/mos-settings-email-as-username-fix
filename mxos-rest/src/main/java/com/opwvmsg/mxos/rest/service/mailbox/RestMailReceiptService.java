/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;

/**
 * MailReceipt service exposed to client which is responsible for doing basic
 * MailReceipt related activities e.g read and update mailreceipt via REST API
 * of MxOS.
 *
 * @author mxos-dev
 */
public class RestMailReceiptService extends AbstractRestService implements IMailReceiptService {
    private static Logger logger = Logger
            .getLogger(RestMailReceiptService.class);

    @Override
    public MailReceipt read(Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILRECEIPT_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.get(subUrl, inputParams);
            final MailReceipt mailReceipt = resp.getEntity(MailReceipt.class);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return mailReceipt;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        RestCRUD rest = null;
        ClientResponse resp = null;
        // call logging api if custom and update 'autoReplyMessage'
        final boolean isCustom = pool.isCustom();
        if (isCustom && inputParams != null && inputParams.containsKey(
                MailboxProperty.autoReplyMessage.toString())) {
            final Map<String, List<String>> tempParams =
                new HashMap<String, List<String>>();
            tempParams.putAll(inputParams);
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MAILRECEIPT_SUB_URL,
                    pool.isCustom(), tempParams);
            rest = null;
            try {
                rest = pool.borrowObject();
                resp = rest.post(subUrl, tempParams);
            } catch (final MxOSException e) {
                logger.error("MxOSException encountered", e);
                throw e;
            } catch (final Exception e) {
                logger.error("Exception encountered", e);
                throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
            } finally {
                // remove the key "autoReplyMessage" entry from inputParams
                inputParams.remove(MailboxProperty.autoReplyMessage.toString());
                logger.info("Removed entry for the key "
                        + MailboxProperty.autoReplyMessage.toString()
                        + " from inputParams");
                if (resp != null) {
                    try {
                        resp.close();
                    } catch (final Exception e) {
                        logger.warn("WARN: Unabled to close mOS Response.");
                    }
                }
                if (rest != null) {
                    try {
                        pool.returnObject(rest);
                    } catch (final Exception e) {
                        logger.error(
                                "Exception encountered in finally clause while returning the connection",
                                e);
                        throw new MxOSException(
                                ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                    }
                }
            }
            // Return if there are no other attributes exist for mailReceipt
            // update.
            if (!(inputParams.size() > 1)) {
                logger.info("No other attributes exist for mailbox update. Going to return");
                if (logger.isDebugEnabled()) {
                    logger.debug("end");
                }
                return;
            }
        }
        // end of update "autoReplyMessage" with logging api
        // continue with rest of the apis
        logger.info("End of update "
                + MailboxProperty.autoReplyMessage.toString()
                + " with logging api. Continuing with rest of the api");
        subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILRECEIPT_SUB_URL, inputParams);
        rest = null;
        resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.post(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
