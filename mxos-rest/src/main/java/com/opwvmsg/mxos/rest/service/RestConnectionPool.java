/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service;

import java.util.Properties;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.rest.service.RestCRUD;

/**
 * This class provides connection pool for HTTP REST.
 * 
 * @author mxos-dev
 */
public class RestConnectionPool {
    private static Logger logger = Logger.getLogger(RestConnectionPool.class);
    private final boolean custom;
    private static RestCRUD instance;

    
    /**
     * Constructor. Configure mxos-host in hosts file.
     * 
     * @throws Exception Exception.
     */
    RestConnectionPool(final Properties p) {
        createRestCRUDPool(p);
        if (p.containsKey(ContextProperty.CUSTOM)) {
            this.custom = Boolean.parseBoolean(p
                    .getProperty(ContextProperty.CUSTOM));
        } else {
            this.custom = false;
        }
        logger.info("Connection MxOS Custom APIs = ".concat(String
                .valueOf(custom)));
        if (this.custom) {
            logger.info("Loading Custom Rest Context for connection "
                    + "to MxOS Custom APIs.");
        }
    }

    /**
     * Create mxosObjectPool to perform CRUD operation using REST.
     * 
     * @throws Exception Exception
     */
    private void createRestCRUDPool(final Properties p) {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        try {
            String baseUrl = null;
            if (p.containsKey(ContextProperty.MXOS_BASE_URL)) {
                baseUrl = p.getProperty(ContextProperty.MXOS_BASE_URL);
            } else {
                System.err.println(ContextProperty.MXOS_BASE_URL
                        + " must be set.");
                throw new IllegalArgumentException(
                        ContextProperty.MXOS_BASE_URL + " must be set.");
            }
            logger.info("MxOS URL = ".concat(baseUrl));
            int maxConnections;
            if (p.containsKey(ContextProperty.MXOS_MAX_CONNECTIONS)) {
                maxConnections = Integer.parseInt(p
                        .getProperty(ContextProperty.MXOS_MAX_CONNECTIONS));
            } else {
                System.err.println(ContextProperty.MXOS_MAX_CONNECTIONS
                        + " must be set.");
                throw new IllegalArgumentException(
                        ContextProperty.MXOS_MAX_CONNECTIONS + " must be set.");
            }
            logger.info("Max Connections = ".concat(String
                    .valueOf(maxConnections)));
            if (maxConnections <= 0) {
                System.err.println(ContextProperty.MXOS_MAX_CONNECTIONS
                        + " must be a positive integer.");
                throw new IllegalArgumentException(
                        ContextProperty.MXOS_MAX_CONNECTIONS
                                + " must be a positive integer.");
            }
            Integer connectTimeout;
            // Set Connection Timeout
            if (p.containsKey(ContextProperty.MXOS_CONNECTION_TIMEOUT)) {
                connectTimeout = Integer.parseInt(p
                        .getProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT));
            } else {
                connectTimeout = 180000;
            }
            // Set Read Timeout
            Integer readTimeout;
            if (p.containsKey(ContextProperty.MXOS_READ_TIMEOUT)) {
                readTimeout = Integer.parseInt(p
                        .getProperty(ContextProperty.MXOS_READ_TIMEOUT));
            } else {
                readTimeout = 180000;
            }
            
            instance = new RestCRUD(baseUrl, maxConnections, connectTimeout, readTimeout);
            logger.info("RESTMxosObjectPool Created with default values...");
        } catch (Exception e) {
            logger.error(
                    "Problem occured while creating RESTMxosObjectPool with "
                            + " default values...", e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    /**
     * Method to borrow connection.
     * 
     * @return connection connection
     * @throws Exception in case no connection is available.
     */
    public RestCRUD borrowObject() throws Exception {
        return instance;
    }

    /**
     * Method to return the connection back.
     * 
     * @param restCRUD restCRUD
     * @throws Exception on any error
     */
    public void returnObject(RestCRUD restCRUD) throws Exception {
        // We dont have to do anything here. Keeping this function for now for
        // not breaking anything that is existing.
    }

    /**
     * @return the custom
     */
    public boolean isCustom() {
        return custom;
    }

    public void destroy() throws Exception {
        instance.close();
    }
}