/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * REST Constants.
 * 
 * @author mxos-dev
 */
public final class RestConstants {
    private static Logger logger = Logger.getLogger(RestConstants.class);

    // Domain APIs
    public static final String DOMAIN_SUB_URL = "/domain/v2/{domain}";
    public static final String DOMAIN_SEARCH_SUB_URL = "/domain/v2/search";
    // SAML APIs
    public static final String SAML_AUTHENTICATION_SUB_URL = "/v2/saml/samlAuthentication/{acsURL}";
    public static final String SAML_ASSERTION_SUB_URL = "/v2/saml/decodeSamlAssertion";
    // COS APIs
    public static final String COS_SUB_URL = "/cos/v2/{cosId}";
    // Base
    public static final String COS_BASE_SUB_URL = "/cos/v2/{cosId}/base";
    public static final String COS_BASE_SEARCH_SUB_URL = "/cos/v2/base/search";
    // COS General Preferences
    public static final String COS_GENERAL_PREFERENCES_SUB_URL = "/cos/v2/{cosId}/generalPreferences";
    // COS Credentials
    public static final String COS_CREDENTIAL_SUB_URL = "/cos/v2/{cosId}/credentials";

    // MailSend
    public static final String COS_MAILSEND_SUB_URL = "/cos/v2/{cosId}/mailSend";

    // COS Sms Services
    public static final String COS_SMSSERVICES_SERVICE = "/cos/v2/{cosId}/smsServices";
    public static final String COS_SMSONLINE_SERVICE = "/cos/v2/{cosId}/smsServices/smsOnline";
    public static final String COS_MAILACCESS_SERVICE = "/cos/v2/{cosId}/mailAccess";

    // MailStore
    // COS SocialNetworks
    public static final String COS_MAILSTORE_SUB_URL = "/cos/v2/{cosId}/mailStore";
    public static final String COS_SOCIALNETWORKS_SUB_URL = "/cos/v2/{cosId}/socialNetworks";
    public static final String COS_EXTERNALSTORE_SUB_URL = "/cos/v2/{cosId}/mailStore/externalStore";
    // Mailbox APIs
    public static final String MAILBOX_SUB_URL = "/mailbox/v2/{email}";
    public static final String MAILBOXES_SUB_URL = "/mailbox/v2/mailboxes";
    // Base
    public static final String MAILBOX_BASE_SUB_URL = "/mailbox/v2/{email}/base";
    public static final String MAILBOX_BASE_SEARCH_SUB_URL = "/mailbox/v2/base/search";
    public static final String ALIAS_SUB_URL = "/mailbox/v2/{email}/base/emailAliases";
    public static final String SINGLE_ALIAS_SUB_URL = "/mailbox/v2/{email}/base/emailAliases/{emailAlias}";
    public static final String ALLOWED_DOMAINS_SUB_URL = "/mailbox/v2/{email}/base/allowedDomains";
    public static final String SINGLE_ALLOWED_DOMAINS_SUB_URL = "/mailbox/v2/{email}/base/allowedDomains/{allowedDomain}";
    // MssLinkInfo APIs
    public static final String MSS_LINK_INFO_SUB_URL = "/mailbox/v2/{email}/mssLinkInfo";
    // webmailfeatures
    public static final String WEBMAILFEATURES_SUB_URL = "/mailbox/v2/{email}/webMailFeatures";
    public static final String COS_WEBMAILFEATURES_SUB_URL = "/cos/v2/{cosId}/webMailFeatures";
    public static final String WEBMAILFEATURES_ADDRESBOOK_SUB_URL = "/mailbox/v2/{email}/webMailFeatures/addressBookFeatures";
    public static final String COS_WEBMAILFEATURES_ADDRESBOOK_SUB_URL = "/cos/v2/{cosId}/webMailFeatures/addressBookFeatures";
    // Credentials
    public static final String AUTH_SUB_URL = "mailbox/v2/{email}/authenticate";
    public static final String AUTH_SUB_URL_USERNAME = "mailbox/v2/{userName}/authenticate";
    public static final String AUTH_AND_GETMAILBOX_SUB_URL = "mailbox/v2/{email}/authenticateAndGetMailbox";
    public static final String AUTH_AND_GETMAILBOX_SUB_URL_USERNAME = "mailbox/v2/{userName}/authenticateAndGetMailbox";

    public static final String CAPTCH_SUB_URL = "captcha/v2/{email}";
    public static final String CREDENTIAL_SUB_URL = "/mailbox/v2/{email}/credentials";
    // GenPrefs
    public static final String GENERAL_PREFERENCES_SUB_URL = "mailbox/v2/{email}/generalPreferences";
    // MailSend
    public static final String MAILSEND_SUB_URL = "/mailbox/v2/{email}/mailSend";
    public static final String MAILSEND_FILTERS_SUB_URL = "/mailbox/v2/{email}/mailSend/filters";
    public static final String MAILSEND_BMIFILTERS_SUB_URL = "/mailbox/v2/{email}/mailSend/filters/bmiFilters";
    public static final String MAILSEND_COMMTOUCH_FILTERS_SUB_URL = "/mailbox/v2/{email}/mailSend/filters/commtouchFilters";
    public static final String MAILSEND_MCAFEE_FILTERS_SUB_URL = "/mailbox/v2/{email}/mailSend/filters/mcafeeFilters";
    public static final String MAILSEND_DELIVERY_MESSAGES_PENDING_SUB_URL = "/mailbox/v2/{email}/mailSend/numDelayedDeliveryMessagesPending";
    public static final String SINGLE_MAILSEND_DELIVERY_MESSAGES_PENDING_SUB_URL = "/mailbox/v2/{email}/mailSend/numDelayedDeliveryMessagesPending/{numDelayedDeliveryMessagesPending}";
    public static final String MAILSEND_SIGNATURE_SUB_URL = "/mailbox/v2/{email}/mailSend/signatures";
    // MialReceipt
    public static final String MAILRECEIPT_SUB_URL = "/mailbox/v2/{email}/mailReceipt";
    public static final String FORWARDING_ADDRESS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/forwardingAddress";
    public static final String SINGLE_FORWARDING_ADDRESS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/forwardingAddress/{forwardingAddress}";
    public static final String ADDRESSES_FOR_LOCAL_DELIVERY_SUB_URL = "/mailbox/v2/{email}/mailReceipt/addressesForLocalDelivery";
    public static final String SINGLE_ADDRESSES_FOR_LOCAL_DELIVERY_SUB_URL = "/mailbox/v2/{email}/mailReceipt/addressesForLocalDelivery/{addressForLocalDelivery}";
    public static final String MAILRECEIPT_SENDERBLOCKING_SUB_URL = "/mailbox/v2/{email}/mailReceipt/senderBlocking";
    public static final String MAILRECEIPT_ALLOWED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/senderBlocking/allowedSendersList";
    public static final String MAILRECEIPT_SINGLE_ALLOWED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/senderBlocking/allowedSendersList/{allowedSender}";
    public static final String MAILRECEIPT_BLOCKED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/senderBlocking/blockedSendersList";
    public static final String MAILRECEIPT_SINGLE_BLOCKED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/senderBlocking/blockedSendersList/{blockedSender}";
    public static final String MAILRECEIPT_FILTERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters";
    public static final String MAILRECEIPT_SIEVEFILTERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters/sieveFilters";
    public static final String MAILRECEIPT_SIEVE_BLOCKED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters/sieveFilters/blockedSenders";
    public static final String MAILRECEIPT_SINGLE_SIEVE_BLOCKED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters/sieveFilters/blockedSenders/{blockedSender}";
    public static final String MAILRECEIPT_BMIFILTERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters/bmiFilters";
    public static final String MAILRECEIPT_COMMTOUCH_FILTERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters/commtouchFilters";
    public static final String MAILRECEIPT_CLOUDMARK_FILTERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters/cloudmarkFilters";
    public static final String MAILRECEIPT_MCAFEE_FILTERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/filters/mcafeeFilters";
    public static final String MAILRECEIPT_ADMIN_APPROVED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/adminControl/adminApprovedSendersList";
    public static final String MAILRECEIPT_SINGLE_ADMIN_APPROVED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/adminControl/adminApprovedSendersList/{adminApprovedSendersList}";
    public static final String MAILRECEIPT_ADMIN_BLOCKED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/adminControl/adminBlockedSendersList";
    public static final String MAILRECEIPT_SINGLE_ADMIN_BLOCKED_SENDERS_SUB_URL = "/mailbox/v2/{email}/mailReceipt/adminControl/adminBlockedSendersList/{adminBlockedSendersList}";

    public static final String COS_MAILRECEIPT_SUB_URL = "/cos/v2/{cosId}/mailReceipt";
    public static final String COS_MAILRECEIPT_SIEVEFILTERS_SUB_URL = "/cos/v2/{cosId}/mailReceipt/filters/sieveFilters";
    public static final String COS_MAILRECEIPT_CLOUDMARKFILTERS_SUB_URL = "/cos/v2/{cosId}/mailReceipt/filters/cloudmarkFilters";
    public static final String COS_MAILRECEIPT_SENDERBLOCKING_SUB_URL = "/cos/v2/{cosId}/mailReceipt/senderBlocking";
    public static final String COS_MAILRECEIPT_FILTERS_SUB_URL = "/cos/v2/{cosId}/mailReceipt/filters";
    // Admin Control
    public static final String MAILRECEIPT_ADMIN_CONTROL_SUB_URL = "/mailbox/v2/{email}/mailReceipt/adminControl";    
    // mailaccess
    public static final String MAILACCESS_SUB_URL = "/mailbox/v2/{email}/mailAccess";
    public static final String MAILACCESS_ALLOWED_IPS_SUB_URL = "/mailbox/v2/{email}/mailAccess/allowedIPs";
    public static final String MAILACCESS_SINGLE_ALLOWED_IPS_SUB_URL = "/mailbox/v2/{email}/mailAccess/allowedIPs/{allowedIP}";
    // mailstore
    public static final String MAILSTORE_SUB_URL = "/mailbox/v2/{email}/mailStore";
    public static final String EXTERNAL_STORE_SUB_URL = "/mailbox/v2/{email}/mailStore/externalStore";
    public static final String GROUP_ADMIN_ALLOCATIONS_SUB_URL = "/mailbox/v2/{email}/mailStore/groupAdminAllocations";
    // socialnetworking
    public static final String SOCIALNETWORKS_SUB_URL = "/mailbox/v2/{email}/socialNetworks";
    public static final String SOCIALNETWORKS_SITE_SUB_URL = "/mailbox/v2/{email}/socialNetworks/socialNetworkSites";
    public static final String SOCIALNETWORKS_SINGLE_SITE_SUB_URL = "/mailbox/v2/{email}/socialNetworks/socialNetworkSites/{socialNetworkSite}";
    // external accounts
    public static final String EXTERNALACCOUNT_SUB_URL = "/mailbox/v2/{email}/externalAccounts";
    public static final String EXTERNALACCOUNT_MAILACCOUNT_SUB_URL = "/mailbox/v2/{email}/externalAccounts/mailAccounts";
    public static final String COS_EXTERNALACCOUNT_SUB_URL = "/cos/v2/{cosId}/externalAccounts";

    // sms services
    public static final String SMSSERVICES_SUB_URL = "/mailbox/v2/{email}/smsServices";
    public static final String SMSSERVICES_SMSONLINE_SUB_URL = "/mailbox/v2/{email}/smsServices/smsOnline";
    public static final String SMSSERVICES_SMSNOTIFICATION_SUB_URL = "/mailbox/v2/{email}/smsServices/smsNotifications";
    // internalinfo
    public static final String INTERNAL_INFO_SUB_URL = "/mailbox/v2/{email}/internalInfo";
    public static final String MESSAGE_EVENT_RECORDS_SUB_URL = "/mailbox/v2/{email}/internalInfo/messageEventRecords";
    public static final String COS_INTERNAL_INFO_SUB_URL = "/cos/v2/{cosId}/internalInfo";
    public static final String COS_MESSAGE_EVENT_RECORDS_SUB_URL = "/cos/v2/{cosId}/internalInfo/messageEventRecords";

    // folders
    public static final String FOLDER_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}";
    public static final String FOLDER_LIST_SUB_URL = "/mailbox/v2/{email}/folders/list";
    public static final String FOLDER_DELETE_ALL_SUB_URL = "/mailbox/v2/{email}/folders";
    // messaging
    public static final String MESSAGE_URL = "/mailbox/v2/{email}/folders/{folderName}/messages";
    public static final String MESSAGE_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/{messageId}";
    public static final String MESSAGE_BYUID_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/uid/{uid}";
    public static final String MESSAGE_COPY_SUB_URL = "/mailbox/v2/{email}/folders/{srcFolderName}/messages/{messageId}/copy/{toFolderName}";
    public static final String MESSAGE_COPY_ALL_OR_MULTI_SUB_URL = "/mailbox/v2/{email}/folders/{srcFolderName}/messages/copy/{toFolderName}";
    public static final String MESSAGE_MOVE_SUB_URL = "/mailbox/v2/{email}/folders/{srcFolderName}/messages/{messageId}/move/{toFolderName}";
    public static final String MESSAGE_MOVE_ALL_OR_MULTI_SUB_URL = "/mailbox/v2/{email}/folders/{srcFolderName}/messages/move/{toFolderName}";
    public static final String MESSAGE_METADATA_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/{messageId}/metadata";
    public static final String MESSAGE_MULTI_METADATA_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/metadata";
    public static final String MESSAGE_METADATA_LIST_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/metadata/list";
    public static final String MESSAGE_METADATA_UID_LIST_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/metadata/uid/list";
    public static final String MESSAGE_HEADER_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/{messageId}/header";
    public static final String MESSAGE_BODY_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/{messageId}/body";
    public static final String MESSAGE_METADATA_SEARCH_SUB_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/metadata/search";
    public static final String MESSAGE_UPDATE_URL = "/mailbox/v2/{email}/folders/{folderName}/messages/{messageId}/body";

    // Process URLs
    public static final String MSISDN_DEACTIVATION_SUB_URL = "/msisdnDeactivationFlow/{msisdn}";
    public static final String MSISDN_REACTIVATION_SUB_URL = "/msisdnReactivationFlow/{msisdn}";
    public static final String MSISDN_SWAP_SUB_URL = "/msisdnSwapFlow/{currentMSISDN}/{newMSISDN}";
    public static final String AUTHORIZE_MSISDN_SUB_URL = "/authorizeMSISDN/{smsType}/{authorizeMsisdn}";
    public static final String SEND_SMS_SUB_URL = "/sendSMS/{smsType}/{fromAddress}/{toAddress}";
    public static final String SEND_MAIL_URL = "/sendMail/{fromAddress}/{toAddress}";
    public static final String SEND_MAIL_SUB_URL = "/sendMail/{fromAddress}";
    public static final String LOG_TO_EXT_ENTITY_SUB_URL = "/logToExternalEntity";

    
    public static final String GET_EXTLDP_ATTRIBUTE_SUB_URL = "/externalLdap/getAttributes/{filterValue}";
    
    // for Logging APIs
    public static final String LOGGING_SUB_URL = "/logging";

    // Address Book Service URLs
    public static final String ADDRESS_BOOK_SUB_URL = "/addressBook/v2/{userId}";
    public static final String CONTACTS_BASE_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/base";
    public static final String CONTACTS_LIST_SUB_URL = "/addressBook/v2/{userId}/contacts/list";
    public static final String CONTACTS_BASE_LIST_SUB_URL = "/addressBook/v2/{userId}/contacts/base/list";
    public static final String CONTACTS_NAME_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/name";
    public static final String CONTACTS_IMAGE_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/image";
    public static final String CONTACTS_ACTUAL_IMAGE_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/actualImage";
    public static final String CONTACTS_CREATE_SUB_URL = "/addressBook/v2/{userId}/contacts";
    public static final String CONTACTS_DELETE_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}";
    public static final String CONTACTS_MOVE_SUB_URL = "/addressBook/v2/{userId}/contacts/move";
    public static final String ABS_LOGIN_SUB_URL = "/addressBook/v2/{userId}/login";
    public static final String ABS_LOGOUT_SUB_URL = "/addressBook/v2/{userId}/logout";
    public static final String PERSONAL_INFO_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/personalInfo";
    public static final String PERSONAL_INFO_ADDRESS_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/personalInfo/address";
    public static final String PERSONAL_INFO_COMMUNICATION_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/personalInfo/communication";
    public static final String PERSONAL_INFO_ALL_EVENTS_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/personalInfo/events";
    public static final String PERSONAL_INFO_EVENTS_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/personalInfo/events/{type}";
    public static final String WORK_INFO_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/workInfo";
    public static final String WORK_INFO_ADDRESS_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/workInfo/address";
    public static final String WORK_INFO_COMMUNICATION_SUB_URL = "/addressBook/v2/{userId}/contacts/{contactId}/workInfo/communication";
    public static final String GROUPS_SUB_URL = "/addressBook/v2/{userId}/groups";
    public static final String GROUPS_DELETE_SUB_URL = "/addressBook/v2/{userId}/groups/{groupId}";
    public static final String GROUPS_BASE_SUB_URL = "/addressBook/v2/{userId}/groups/{groupId}/base";
    public static final String GROUPS_MEMBERS_SUB_URL = "/addressBook/v2/{userId}/groups/{groupId}/members/{memberId}";
    public static final String GROUPS_ALL_MEMBERS_SUB_URL = "/addressBook/v2/{userId}/groups/{groupId}/members";
    public static final String GROUPS_EXTERNALMEMBERS_SUB_URL = "/addressBook/v2/{userId}/groups/{groupId}/externalMembers/{memberName}";
    public static final String GROUPS_ALL_EXTERNALMEMBERS_SUB_URL = "/addressBook/v2/{userId}/groups/{groupId}/externalMembers";
    public static final String GROUPS_BASE_LIST_SUB_URL = "/addressBook/v2/{userId}/groups/base/list";
    public static final String GROUPS_MOVE_SUB_URL = "/addressBook/v2/{userId}/groups/move";

    // Tasks Service URLs
    public static final String TASKS_LOGIN_SUB_URL = "/task/v2/{userId}/login";
    public static final String TASKS_LOGOUT_SUB_URL = "/task/v2/{userId}/logout";
    public static final String TASKS_SUB_URL = "/task/v2/{userId}/tasks";
    public static final String TASKS_DELETE_SUB_URL = "/task/v2/{userId}/tasks/{taskId}";
    public static final String TASKS_LIST_SUB_URL = "/task/v2/{userId}/tasks/list";
    public static final String TASKS_CONFIRM_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/confirm";
    // Task base URLs
    public static final String TASKS_BASE_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/base";
    public static final String TASKS_BASE_LIST_SUB_URL = "/task/v2/{userId}/tasks/base/list";
    // Task Recurrence URLs
    public static final String TASKS_RECURRENCE_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/recurrence";
    // Task Participant URLs
    public static final String TASKS_PARTICIPANTS_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/participants/{email}";
    public static final String TASKS_PARTICIPANTS_LIST_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/participants/list";
    // Task Details URLs
    public static final String TASKS_DETAILS_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/details";
    // Task Attachments URLs
    public static final String TASKS_ATTACHMENTS_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/attachments";
    public static final String TASKS_ATTACHMENTS_LIST_SUB_URL = "/task/v2/{userId}/tasks/{taskId}/attachments/list";
    
    // Notify-Subscription URLs
    public static final String NOTIFY_TOPIC = "/notify/v2/{topic}";
    public static final String NOTIFY_TOPIC_SUBS = "/notify/v2/{topic}/subscriptions";
    public static final String NOTIFY_TOPIC_PUBLISH = "/notify/v2/{topic}/publish";

    /**
     * Utility class - Private constructor.
     */
    private RestConstants() {

    }

    /**
     * Method to replacing the PlaceHolders to make final URL.
     * 
     * @param subUrl sub url of the respective api
     * @param userParams Map<String, List<String>>
     * @return String final url
     * @throws MxOSException on any error
     */
    public static String getFinalSubUrl(final String subUrl,
            final Map<String, List<String>> userParams) throws MxOSException {
        // default isCustom is false;
        return getFinalSubUrl(subUrl, false, userParams);
    }

    /**
     * Method to replacing the PlaceHolders to make final URL.
     * 
     * @param subUrl sub url of the respective api
     * @param isCustom true for custom urls
     * @param userParams Map<String, List<String>>
     * @return String final url
     * @throws MxOSException on any error
     */
    public static String getFinalSubUrl(final String subUrl,
            final boolean isCustom, final Map<String, List<String>> userParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String errorMsg = "Bad request, please check the request and parameters.";
        final String errorMessage = "Invalid input parameters.";
        if (userParams == null) {
            logger.error("Bad request, no input parameters in request");
            throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(), errorMsg);
        }
        final StringBuffer temp = new StringBuffer();
        if (isCustom) {
            temp.append(RestConstants.LOGGING_SUB_URL);
        }
        temp.append(subUrl);
        final String url = temp.toString();
        final Pattern pattern = Pattern.compile("\\{.*?\\}");
        final Matcher matcher = pattern.matcher(url);
        StringBuilder value = new StringBuilder(url.length());
        int index = 0;
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            // + 1 and - 1 to cut off the { and }
            String key = url.substring(start + 1, end - 1);
            if (userParams.containsKey(key)) {
                if (userParams.get(key) == null
                        || userParams.get(key).size() == 0
                        || "".equals(userParams.get(key).get(0))) {
                    logger.error(new StringBuilder(
                            "Bad request, input parameter ").append(key)
                            .append(" is null in request"));
                    throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(),
                            errorMsg);
                }
                // index is the end of the last template, so add anything
                // between that template and the current template
                value.append(url.substring(index, start));
                // add the substituted value
                try {
                    value.append(URLEncoder.encode(userParams.get(key).get(0),
                            MxOSConstants.UTF8).replaceAll("\\+", "%20"));
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered while translating into into application/x-www-form-urlencoded format",
                            e);
                    throw new InvalidRequestException(
                            ErrorCode.MXS_INPUT_ERROR.name(), errorMessage);
                }
                userParams.remove(key);
            } else {
                logger.error(new StringBuilder("Bad request, input parameter ")
                        .append(key).append(" not present in request"));
                throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(),
                        errorMsg);
            }
            index = end;
        }
        // end the remained
        value.append(url.substring(index));

        final Matcher matcher1 = pattern.matcher(value.toString());
        if (matcher1.find()) {
            throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(), errorMsg);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return value.toString();
    }
}

