/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

/**
 * Contacts service exposed to client which is responsible for doing basic
 * Contacts related activities e.g create, update, read, search and delete
 * Contact via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestContactsService extends AbstractRestService implements
        IContactsService {
    private static Logger logger = Logger.getLogger(RestContactsService.class);

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_CREATE_SUB_URL, pool.isCustom(),
                inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            final ClientResponse r = rest.put(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return r.getEntity(Long.class);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.ABS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_DELETE_SUB_URL, pool.isCustom(),
                inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            rest.delete(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.ABS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_CREATE_SUB_URL, pool.isCustom(),
                inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            File file = File.createTempFile(MxOSConstants.EXTENSION,
                    MxOSConstants.EXTENSION);
            byte[] dataBytes = inputParams
                    .get(AddressBookProperty.contactsList.name()).get(0)
                    .getBytes(MxOSConstants.UTF8);
            FileUtils.writeByteArrayToFile(file, dataBytes);
            inputParams.remove(AddressBookProperty.contactsList.name());
            final ClientResponse r = rest.postMultipart(subUrl, file, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return r.getEntity(new GenericType<List<Long>>() {
            });
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.ABS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public List<Contact> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }

        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_LIST_SUB_URL, pool.isCustom(),
                inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            final ClientResponse r = rest.get(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return r.getEntity(new GenericType<List<Contact>>() {
            });
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void moveMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_MOVE_SUB_URL, pool.isCustom(),
                inputParams);
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            File file = File.createTempFile(MxOSConstants.EXTENSION,
                    MxOSConstants.EXTENSION);
            byte[] dataBytes = inputParams
                    .get(AddressBookProperty.contactsList.name()).get(0)
                    .getBytes(MxOSConstants.UTF8);
            FileUtils.writeByteArrayToFile(file, dataBytes);
            inputParams.remove(AddressBookProperty.contactsList.name());
            rest.postMultipart(subUrl, file, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.ABS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }
}
