/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.message;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Message service exposed to client which is responsible for doing basic
 * Message related activities e.g create, update, read, search and delete
 * Message via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMessageService extends AbstractRestService implements IMessageService {
    private static Logger logger = Logger.getLogger(RestMessageService.class);

    @Override
    public String create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MESSAGE_SUB_URL, inputParams);
        } else {
            subUrl = RestConstants.getFinalSubUrl(RestConstants.MESSAGE_URL,
                    inputParams);
        }
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
            final String messageId = resp.getEntity(String.class);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return messageId;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public Message read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.get(subUrl, inputParams);
            final Message message = resp.getEntity(Message.class);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return message;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public Message readByUID(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_BYUID_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.get(subUrl, inputParams);
            final Message message = resp.getEntity(Message.class);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return message;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void updateByUID(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_BYUID_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            rest.post(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return;
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }    
    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        List<String> messageIds = inputParams.get(MessageProperty.messageId
                .name());
        String subUrl = null;
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            if (messageIds.size() > 1) {
                subUrl = RestConstants.getFinalSubUrl(
                        RestConstants.MESSAGE_URL, inputParams);
                rest = pool.borrowObject();
                resp = rest.delete(subUrl, inputParams);
            } else {
                subUrl = RestConstants.getFinalSubUrl(
                        RestConstants.MESSAGE_SUB_URL, inputParams);
                rest = pool.borrowObject();
                resp = rest.delete(subUrl, inputParams);
            }
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        subUrl = RestConstants.getFinalSubUrl(RestConstants.MESSAGE_URL,
                inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.delete(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void copy(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        List<String> messageIds = inputParams.get(MessageProperty.messageId
                .name());
        String subUrl = null;
        RestCRUD rest = null;
        ClientResponse resp = null;
        if (messageIds.size() > 1) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MESSAGE_COPY_ALL_OR_MULTI_SUB_URL, inputParams);
        } else {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MESSAGE_COPY_SUB_URL, inputParams);
        }
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public void copyAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_COPY_ALL_OR_MULTI_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public void move(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        List<String> messageIds = inputParams.get(MessageProperty.messageId
                .name());
        String subUrl = null;
        RestCRUD rest = null;
        ClientResponse resp = null;
        if (messageIds.size() > 1) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MESSAGE_MOVE_ALL_OR_MULTI_SUB_URL, inputParams);
        } else {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MESSAGE_MOVE_SUB_URL, inputParams);
        }
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public void moveAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_MOVE_ALL_OR_MULTI_SUB_URL, inputParams);
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public void deleteMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        List<String> messageIds = inputParams.get(MessageProperty.messageId
                .name());
        String subUrl = null;
        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            if (messageIds.size() >= 1) {
                subUrl = RestConstants.getFinalSubUrl(
                        RestConstants.MESSAGE_URL, inputParams);
                rest = pool.borrowObject();
                resp = rest.delete(subUrl, inputParams);
            } else {
                throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(),
                        "No messageId provided in Multiple DELETE request.");
            }
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void copyMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        List<String> messageIds = inputParams.get(MessageProperty.messageId
                .name());
        String subUrl = null;
        RestCRUD rest = null;
        ClientResponse resp = null;
        if (messageIds.size() >= 1) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MESSAGE_COPY_ALL_OR_MULTI_SUB_URL,
                    inputParams);
        } else {
            throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(),
                    "No messageId provided in Multiple COPY request.");
        }
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public void moveMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        List<String> messageIds = inputParams.get(MessageProperty.messageId
                .name());
        String subUrl = null;
        RestCRUD rest = null;
        ClientResponse resp = null;
        if (messageIds.size() >= 1) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.MESSAGE_MOVE_ALL_OR_MULTI_SUB_URL, inputParams);
        } else {
            throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(),
                    "No messageId provided in Multiple MOVE request.");
        }
        try {
            rest = pool.borrowObject();
            resp = rest.put(subUrl, inputParams);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }
}
