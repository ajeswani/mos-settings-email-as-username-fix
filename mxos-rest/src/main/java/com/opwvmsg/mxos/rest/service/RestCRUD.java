/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Generated;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.exception.ServiceUnavailableException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.client.apache4.ApacheHttpClient4;
import com.sun.jersey.client.apache4.config.ApacheHttpClient4Config;
import com.sun.jersey.client.apache4.config.DefaultApacheHttpClient4Config;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.Boundary;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

/**
 * RestClient class is responsible for doing HTTP rest requests.
 * 
 * @author mxos-dev
 */
public final class RestCRUD {
    private static Logger logger = Logger.getLogger(RestCRUD.class);
    private static String baseURL;
    private static PoolingClientConnectionManager connectionPool;
    private static WebResource webResource;

    /**
     * Constructor.
     * 
     * @param baseURL - base url of mxos
     */
    public RestCRUD(final String baseURL, final int maxConnections,
            final Integer connectionTimeout, final Integer readTimeout) {
        webResource = getWebResource(baseURL, maxConnections,
                connectionTimeout, readTimeout);
        RestCRUD.baseURL = baseURL;
    }

    /**
     * Method to get Web Resources.
     * 
     * @param subURL - Base URL for REST request.
     * @param connectTimeout - connection time out value in ms
     * @param readTimeout - read time out value in ms
     * @return webResource object
     */
    private static WebResource getWebResource(final String baseURL,
            final int maxConnections, final Integer connectTimeout,
            final Integer readTimeout) {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }

        connectionPool = new PoolingClientConnectionManager();
        connectionPool.setDefaultMaxPerRoute(maxConnections);
        connectionPool.setMaxTotal(maxConnections);
        final ApacheHttpClient4Config config = new DefaultApacheHttpClient4Config();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
                Boolean.TRUE);
        config.getFeatures().put(JsonParser.Feature.INTERN_FIELD_NAMES.name(),
                Boolean.FALSE);
        // Set Connection Timeout
        // jersey-client
        config.getProperties().put(ClientConfig.PROPERTY_CONNECT_TIMEOUT,
                connectTimeout);
        // jersey-apache-client4
        config.getProperties().put(CoreConnectionPNames.CONNECTION_TIMEOUT,
                connectTimeout);
        // Set Read Timeout
        // jersey-client
        config.getProperties().put(ClientConfig.PROPERTY_READ_TIMEOUT,
                readTimeout);
        // jersey-apache-client4
        config.getProperties()
                .put(CoreConnectionPNames.SO_TIMEOUT, readTimeout);
        config.getProperties().put(
                ApacheHttpClient4Config.PROPERTY_CONNECTION_MANAGER,
                connectionPool);
        final WebResource webResource = ApacheHttpClient4.create(config)
                .resource(baseURL);
        webResource.type(MxOSConstants.APP_URLENCODED);
        webResource.accept(MediaType.APPLICATION_JSON);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return webResource;
    }

    /**
     * Close HTTP resource.
     */
    public void close() {
        connectionPool.shutdown();
        logger.info("mOS connection closed!!!");
    }

    /**
     * Does HTTP-REST GET request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP GET parameters (will be appended to URL) in
     *            key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     */
    public static ClientResponse get(final String subURL,
            final Map<String, List<String>> params) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        logger.info(new StringBuffer("GET request being made for the url ")
                .append(baseURL).append(subURL));
        final ClientResponse response = webResource.path(subURL)
                .queryParams(getMultivaluedMap(params))
                .get(ClientResponse.class);
        logger.info(new StringBuilder("Response : ").append(response));
        validateResponse(response);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return response;
    }

    /**
     * Does HTTP-REST PUT request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP PUT body parameters in key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     */
    public static ClientResponse put(final String subURL,
            final Map<String, List<String>> params) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        logger.info(new StringBuffer("PUT request being made for the url ")
                .append(baseURL).append(subURL));
        final ClientResponse response = webResource.path(subURL).put(
                ClientResponse.class, getMultivaluedMap(params));
        logger.info(new StringBuilder("Response : ").append(response));
        validateResponse(response);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return response;
    }

    /**
     * Does HTTP-REST POST request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP POST body parameters in key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     */
    public static ClientResponse post(final String subURL,
            final Map<String, List<String>> params) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        logger.info(new StringBuffer("POST request being made for the url ")
                .append(baseURL).append(subURL));
        final ClientResponse response = webResource.path(subURL).post(
                ClientResponse.class, getMultivaluedMap(params));
        logger.info(new StringBuilder("Response : ").append(response));
        validateResponse(response);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return response;
    }

    /**
     * Does HTTP-REST POST multipart request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP POST body parameters in key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     */
    public static ClientResponse postMultipart(final String subURL,
            final File file, final Map<String, List<String>> formParams) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        logger.info(new StringBuffer("POST Multi request being made for the url ")
                .append(baseURL).append(subURL));
        FormDataMultiPart multiPart = new FormDataMultiPart().field("file",
                file, MediaType.MULTIPART_FORM_DATA_TYPE);
        multiPart.bodyPart(new FileDataBodyPart("file", file));
        if (formParams != null && formParams.size()>0) {
            for (String key : formParams.keySet()) {
                multiPart.bodyPart(new FormDataBodyPart(key, formParams.get(key).get(0)));
            }
        }

        ClientResponse response = webResource.path(subURL)
                .type(MediaType.MULTIPART_FORM_DATA)
                .type(Boundary.addBoundary(MediaType.MULTIPART_FORM_DATA_TYPE))
                .accept(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, multiPart);
        logger.info(new StringBuilder("Response : ").append(response));
        validateResponse(response);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return response;
    }

    public static ClientResponse postJson(final String subURL, final Object obj)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }

        final String baseUrl = webResource.getURI().toString();
        if (logger.isDebugEnabled()) {
            logger.debug("RestCRUD baseUrl = " + baseUrl);
        }
        // TODO - read the connection timeout and read timeout
        final WebResource webResourceJson = getWebResource(baseUrl, 1, 180000,
                180000);
        webResourceJson.type(MediaType.APPLICATION_JSON);

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(obj.toString());
        } catch (JSONException e) {
            logger.error("JSONException for input object");
            throw new MxOSException(ErrorCode.MXS_INPUT_ERROR.name(), e);
        }

        logger.info("Post request being made for the url ".concat(subURL));

        final ClientResponse response = webResourceJson.path(subURL).post(
                ClientResponse.class, jsonObj);
        logger.info(new StringBuilder("Response : ").append(response));
        validateResponse(response);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        /* replacing before next call */
        return response;
    }


    /**
     * Does HTTP-REST DELETE request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP DELETE parameters (will be appended to URL) in
     *            key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     */
    public static ClientResponse delete(final String subURL,
            final Map<String, List<String>> params) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        logger.info(new StringBuffer("DELETE request being made for the url ")
                .append(baseURL).append(subURL));
        final ClientResponse response = webResource.path(subURL)
                .queryParams(getMultivaluedMap(params))
                .delete(ClientResponse.class);
        logger.info(new StringBuilder("Response : ").append(response));
        validateResponse(response);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return response;
    }

    /**
     * Method to convert Map<String, List<String>> to MultivaluedMap. TODO -
     * This is a temparary solution to convert map to MultivaluedMap.
     * 
     * @param params Map<String, List<String>>
     * @return MultivaluedMap<String, String> map
     * @throws Exception if any error while UTF-8 encoding
     */
    private static MultivaluedMap<String, String> getMultivaluedMap(
            final Map<String, List<String>> params) throws MxOSException {
        StringBuilder requestData = null;
        if (logger.isDebugEnabled()) {
            logger.debug("start");
            requestData = new StringBuilder(
                    "Request data in the format Parameter : [Value] - ");
        }
        final MultivaluedMap<String, String> map = new MultivaluedMapImpl();
        for (Entry<String, List<String>> entry : params.entrySet()) {
            List<String> values = new ArrayList<String>();
            if (logger.isDebugEnabled()) {
                requestData.append(entry.getKey()).append(" : ")
                        .append(entry.getValue()).append(", ");
            }
            for (String value : entry.getValue()) {
                values.add(value);
            }
            map.put(entry.getKey(), values);
        }
        if (logger.isDebugEnabled()) {
            requestData.setLength(requestData.length() - 2);
            logger.debug(requestData);
            logger.debug("end");
        }
        return map;
    }

    private static void validateResponse(final ClientResponse response)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        if (response == null) {
            logger.error("Response received is null");
            throw new MxOSException(ErrorCode.MXS_OUTPUT_ERROR.name(),
                    "No Response");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Response status is - ".concat(String.valueOf(response
                    .getStatus())));
        }
        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            try {
                MxOSException exp = null;
                MxOSError error = response.getEntity(MxOSError.class);
                if (error != null) {
                    switch (response.getStatus()) {
                    case 503:
                        exp = new ServiceUnavailableException(error.getCode());
                        break;
                    case 500:
                        exp = new ApplicationException(error.getCode());
                        break;
                    case 404:
                        exp = new NotFoundException(error.getCode());
                        break;
                    case 403:
                        exp = new AuthorizationException(error.getCode());
                        break;
                    case 400:
                        exp = new InvalidRequestException(error.getCode());
                        break;
                    default:
                        exp = new MxOSException(error.getCode());
                    }
                    exp.setRequestParams(error.getRequestParams());
                    exp.setOperationType(error.getOperationType());
                    exp.setShortMessage(error.getShortMessage());
                    exp.setLongMessage(error.getLongMessage());
                } else {
                    logger.error("Unable to parse the error response");
                    exp = new MxOSException(ErrorCode.MXS_OUTPUT_ERROR.name(),
                            "Unable to parse the error response.");
                }
                throw exp;
            } catch (MxOSException e) {
                throw e;
            } catch (Exception e) {
                throw new MxOSException(ErrorCode.MXS_GENERAL_ERROR.name(), e);
            } finally {
                if (response != null) {
                    try {
                        response.close();
                    } catch (final Exception e) {
                        throw new MxOSException(
                                ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                    }
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }
}

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({ "code", "requestParams", "operationType", "shortMessage",
        "longMessage" })
class MxOSError implements Serializable {
    private static final long serialVersionUID = 7369355033341304297L;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("code")
    private String code;
    @JsonProperty("requestParams")
    private String requestParams;
    @JsonProperty("operationType")
    private String operationType;
    @JsonProperty("shortMessage")
    private String shortMessage;
    @JsonProperty("longMessage")
    private String longMessage;

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("requestParams")
    public String getRequestParams() {
        return requestParams;
    }

    @JsonProperty("requestParams")
    public void setRequestParams(String requestParams) {
        this.requestParams = requestParams;
    }

    @JsonProperty("operationType")
    public String getOperationType() {
        return operationType;
    }

    @JsonProperty("operationType")
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    @JsonProperty("shortMessage")
    public String getShortMessage() {
        return shortMessage;
    }

    @JsonProperty("shortMessage")
    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    @JsonProperty("longMessage")
    public String getLongMessage() {
        return longMessage;
    }

    @JsonProperty("longMessage")
    public void setLongMessage(String longMessage) {
        this.longMessage = longMessage;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }
}