/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.common;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Rest Service for Login operations like login and logout.
 * 
 * @author mxos-dev
 */
public class RestExternalLoginService extends AbstractRestService implements IExternalLoginService {
    private static Logger logger = Logger.getLogger(RestExternalLoginService.class);

    @Override
    public ExternalSession login(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        if (inputParams != null
                && inputParams.containsKey(ExternalProperty.entity.name())) {
            final Entity entity = Entity.fromValue(inputParams.remove(
                    ExternalProperty.entity.name()).get(0));
            switch(entity) {
                case ADDRESS_BOOK:
                    subUrl = RestConstants.getFinalSubUrl(
                            RestConstants.ABS_LOGIN_SUB_URL, pool
                                    .isCustom(), inputParams);
                    break;
                case TASKS:
                    subUrl = RestConstants.getFinalSubUrl(
                            RestConstants.TASKS_LOGIN_SUB_URL, pool
                                    .isCustom(), inputParams);
                    break;
            }
        }
        if (subUrl == null || subUrl.isEmpty()) {
            final String errorMsg = new StringBuilder(
                    "Bad request, bad url parameter ").append(subUrl)
                    .append(" is null in request").toString();
            logger.error(errorMsg);
            throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(), errorMsg);
        }
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            final ClientResponse r = rest.post(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return r.getEntity(ExternalSession.class);
        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public void logout(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        if (inputParams != null
                && inputParams.containsKey(ExternalProperty.entity.name())) {
            final Entity entity = Entity.fromValue(inputParams.remove(
                    ExternalProperty.entity.name()).get(0));
            switch(entity) {
                case ADDRESS_BOOK:
                    subUrl = RestConstants.getFinalSubUrl(
                            RestConstants.ABS_LOGOUT_SUB_URL, pool
                                    .isCustom(), inputParams);
                    break;
                case TASKS:
                    subUrl = RestConstants.getFinalSubUrl(
                            RestConstants.TASKS_LOGOUT_SUB_URL, pool
                                    .isCustom(), inputParams);
                    break;
            }
        }
        if (subUrl == null || subUrl.isEmpty()) {
            final String errorMsg = new StringBuilder(
                    "Bad request, bad url parameter ").append(subUrl)
                    .append(" is null in request").toString();
            logger.error(errorMsg);
            throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(), errorMsg);
        }
        RestCRUD rest = null;
        try {
            rest = pool.borrowObject();
            rest.get(subUrl, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }

        } catch (final MxOSException e) {
            logger.error("MxOSException encountered", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error(
                            "Exception encountered in finally clause while returning the connection",
                            e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

}
