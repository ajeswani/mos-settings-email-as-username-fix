/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSService;
import com.opwvmsg.mxos.interfaces.service.RetryStrategy;

/**
 * Abstract MxOS Service.
 * 
 * @author mxos-dev
 */
public abstract class AbstractRestService implements IMxOSService {
    private static Logger logger = Logger.getLogger(AbstractRestService.class);

    public RestConnectionPool pool;

    public void init(final RestConnectionPool pool) {
        this.pool = pool;
    }

    public <T> T setupRetryStrategy(T service, RetryStrategy retryStrategy)
            throws MxOSException {
        // Return dynamic proxy, so calls can be intercepted
        return (T) RestServiceProxy.newInstance(this, retryStrategy);
    }
}