/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubMailboxBaseService implements IMailboxBaseService {

    @Override
    public Base read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        String email = "foo@bar.com";
        if (inputParams.containsKey(MailboxProperty.email.name())) {
            email = inputParams.get(MailboxProperty.email.name()).get(0);
        }
        return PopulateMailbox.getMailbox(email).getBase();
    }

    @Override
    public void update(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public List<Base> search(
            final Map<String, List<String>> inputParams) throws MxOSException {
        List<Base> list = new ArrayList<Base>();
        Base base = PopulateMailbox.getMailbox("user1@foo.com").getBase();
        list.add(base);
        base = PopulateMailbox.getMailbox("user2@foo.com").getBase();
        list.add(base);
        return list;
    }
}
