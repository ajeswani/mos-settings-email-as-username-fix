/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;

/**
 * Populate the Data.
 * 
 * @author Aricent
 * 
 */
public final class PopulateMailbox {
    public static Mailbox mailbox;
    private static void loadMailbox() {
        InputStream is = null;
        try {
            // JsonFactory jfactory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper();
            is = StubUtils.getRource("/sample-mailbox.json");
            if (is != null) {
                mailbox = mapper.readValue(is, Mailbox.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * Populate create.
     * 
     * @param email email
     * @return long mailboxId.
     */
    public static Long create(String email) {
        return new Long(email.hashCode());
    }

    /**
     * populate mailboxes data.
     * 
     * @return List of mailbox objects
     */
    public static List<Mailbox> getMailboxes() {
        loadMailbox();
        List<Mailbox> mailboxes = new ArrayList<Mailbox>();
        mailboxes.add(mailbox);
        Mailbox mb1 = mailbox;
        mb1.getBase().setEmail("foo2@bar2.com");
        mailboxes.add(mb1);
        return mailboxes;
    }
    
    /**
     * populate mailbox data.
     * 
     * @return Mailbox mailbox object
     */
    public static Mailbox getMailbox(String email) {
        loadMailbox();
        mailbox.getBase().setEmail(email);
        return mailbox;
    }
    
    /**
     * 
     * return the list of mail boxes
     * 
     * @param email
     * @return List of Mailbox object
     */
    public static List<Mailbox> getMailboxes(List<String> emails) {
    	    	
        List<Mailbox> mailboxes = new ArrayList<Mailbox>();
        
        Mailbox mb1;
        for(int i=0; i<emails.size(); i++){
        	loadMailbox();
         	mb1 = mailbox;
        	mb1.getBase().setEmail(emails.get(i));
        	mailboxes.add(mb1);
        	mailbox = null;
        }
        return mailboxes;
    }
    
    /**
     * populate WebMailFeatures data
     * 
     * @return WebMailFeatures WebMailFeatures object
     */
    public static WebMailFeatures getWebMailFeatures() {
        loadMailbox();
        return mailbox.getWebMailFeatures();
    }
    
    /**
     * populate WebMailFeaturesAddressBook data
     * 
     * @return AddressBook AddressBook object
     */
    public static AddressBookFeatures getWebMailFeaturesAddressBook() {
        loadMailbox();
        return mailbox.getWebMailFeatures().getAddressBookFeatures();
    }

    /**
     * populate COS data
     * 
     * @return Cos Cos object
     */
    public static Credentials getCredentials() {
        loadMailbox();
        return mailbox.getCredentials();
    }

    /**
     * populate GeneralPreferences data
     * 
     * @return
     * 
     * @return GeneralPreferences GeneralPreferences object
     */
    public static GeneralPreferences getGeneralPreferences() {
        loadMailbox();
        return mailbox.getGeneralPreferences();
    }

    /**
     * populate MailSend data
     * 
     * @return MailSend MailSend object
     */
    public static MailSend getMailSend() {
        loadMailbox();
        return mailbox.getMailSend();
    }

    /**
     * populate MailReceipt data
     * 
     * @return MailReceipt MailReceipt object
     */
    public static MailReceipt getMailReceipt() {
        loadMailbox();
        return mailbox.getMailReceipt();
    }

    /**
     * populate MailAccess data
     * 
     * @return MailAccess MailAccess object
     */
    public static MailAccess getMailAccess() {
        loadMailbox();
        return mailbox.getMailAccess();
    }

    /**
     * populate MailStore data
     * 
     * @return MailStore MailStore object
     */
    public static MailStore getMailStore() {
        loadMailbox();
        return mailbox.getMailStore();
    }

    /**
     * populate ExternalAccounts data
     * 
     * @return ExternalAccounts ExternalAccounts object
     */
    public static ExternalAccounts getExternalAccounts() {
        loadMailbox();
        return mailbox.getExternalAccounts();
    }
    
    /**
     * populate SocialNetworks data
     * 
     * @return SocialNetworks SocialNetworks object
     */
    public static SocialNetworks getSocialNetworks() {
        loadMailbox();
        return mailbox.getSocialNetworks();
    }

    /**
     * populate SmsServices data
     * 
     * @return SmsServices SmsServices object
     */
    public static SmsServices getSmsServices() {
        loadMailbox();
        return mailbox.getSmsServices();
    }

    /**
     * populate InternalInfo data
     * 
     * @return InternalInfo InternalInfo object
     */
    public static InternalInfo getInternalInfo() {
        loadMailbox();
        return mailbox.getInternalInfo();
    }
    
    /**
     * populate MssLinkInfo data.
     * 
     * @return MssLinkInfo object
     */
    public static MssLinkInfo getMssLinkInfo(String email) {
        final MssLinkInfo linkInfo = new MssLinkInfo();
        loadMailbox();
        linkInfo.setAutoReplyHost(mailbox.getInternalInfo()
                .getMessageStoreHosts().get(0));
        linkInfo.setMailboxId(mailbox.getBase().getMailboxId());
        linkInfo.setAutoReplyHost(mailbox.getInternalInfo().getAutoReplyHost());
        linkInfo.setRealm(mailbox.getInternalInfo().getRealm());
        return linkInfo;
    }
}
