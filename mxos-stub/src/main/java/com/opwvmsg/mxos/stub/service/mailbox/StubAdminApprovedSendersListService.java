/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminApprovedSendersListService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * Stub implementation for admin approved senders operation which will be exposed
 * to the client. This implementation is responsible for carrying out create,
 * read, update,delete operations
 * 
 * @author mxos-dev
 * 
 */
public class StubAdminApprovedSendersListService implements
        IAdminApprovedSendersListService {

    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public List<String> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        String email = inputParams.get(MailboxProperty.email.name()).get(0);
        String type = PopulateMailbox.getMailbox(email).getBase().getType().name();
        if (type != null && type.equals("1"))
            return PopulateMailbox.getMailbox(email).getMailReceipt()
                    .getAdminControl().getAdminApprovedSendersList();
        else
            throw new MxOSException(
                    MailboxError.MBX_NOT_GROUP_ADMIN_ACCOUNT.name(),
                    "Not a valid family administrator");
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

}
