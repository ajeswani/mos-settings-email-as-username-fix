/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalAccountsService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * Mailbox external account operations interface which will be exposed to the
 * client. This interface is responsible for doing mailbox external account
 * related operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class StubExternalAccountService implements IExternalAccountsService {

    @Override
    public ExternalAccounts read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return PopulateMailbox.getExternalAccounts();
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {

    }
}
