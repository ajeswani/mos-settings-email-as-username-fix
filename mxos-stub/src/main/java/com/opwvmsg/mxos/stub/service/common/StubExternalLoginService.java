/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.common;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;

/**
 * Login service exposed to client which is responsible for doing OX login
 * related activities via Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubExternalLoginService implements IExternalLoginService {

    @Override
    public ExternalSession login(Map<String, List<String>> inputParams)
            throws MxOSException {

        ExternalSession session = new ExternalSession();
        session.setSessionId("ffbc30f2dc4d48bda98618a5f4d3f188");
        session.setCookieString("open-xchange-public-session=e1aad3786a164ca895bab07040ff9cdf;"
                + "open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=632226c45fe146a09f8d562074aec397;"
                + "JSESSIONID=0ca661e98e6a40a395fb83fa5e218558.APP1");

        return session;
    }

    @Override
    public void logout(final Map<String, List<String>> inputParams)
            throws MxOSException {
    }
}
