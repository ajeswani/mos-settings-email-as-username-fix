/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.opwvmsg.mxos.addressbook.pojos.SharedContact;
import com.opwvmsg.mxos.addressbook.pojos.SharedGroup;
import com.opwvmsg.mxos.addressbook.pojos.Subscription;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoSubscriptionsService;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsPersonalInfoSubscriptionsService implements
        IContactsPersonalInfoSubscriptionsService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long notificationId = 2;
        if (inputParams.containsKey(AddressBookProperty.notificationId.name())) {
            notificationId = Long.parseLong(inputParams.get(
                    AddressBookProperty.notificationId.name()).get(0));
        }
        return notificationId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public Subscription read(Map<String, List<String>> inputParams)
            throws MxOSException {

        Subscription subscription = new Subscription();
        subscription.setOtherUserId("Bob");

        Set<SharedContact> sharedContacts = new HashSet<SharedContact>();

        SharedContact sharedContact = new SharedContact();
        sharedContact.setCreated("2011-06-28T17:26:54Z");
        sharedContact.setUpdated("2011-06-28T17:26:54Z");
        sharedContact
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/contacts/{John}");
        sharedContact.setSharedUserId("ABUserName");

        sharedContacts.add(sharedContact);
        subscription.setSharedContacts(sharedContacts);

        Set<SharedGroup> sharedGroups = new HashSet<SharedGroup>();

        SharedGroup sharedGroup = new SharedGroup();
        sharedGroup.setCreated("2011-06-28T17:26:54Z");
        sharedGroup.setUpdated("2011-06-28T17:26:54Z");
        sharedGroup
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/groups/{Openwave-Mainline}");
        sharedGroup.setSharedGroupId("Openwave-Mainline1");

        sharedGroups.add(sharedGroup);

        subscription.setSharedGroups(sharedGroups);

        return subscription;
    }

    @Override
    public List<Subscription> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<Subscription> subscriptions = new ArrayList<Subscription>();

        Subscription subscription1 = new Subscription();

        subscription1.setOtherUserId("Bob");

        Set<SharedContact> sharedContacts1 = new HashSet<SharedContact>();

        SharedContact sharedContact1 = new SharedContact();
        sharedContact1.setCreated("2011-06-28T17:26:54Z");
        sharedContact1.setUpdated("2011-06-28T17:26:54Z");
        sharedContact1
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/contacts/{John}");
        sharedContact1.setSharedUserId("ABUserName");

        sharedContacts1.add(sharedContact1);
        subscription1.setSharedContacts(sharedContacts1);

        Set<SharedGroup> sharedGroups1 = new HashSet<SharedGroup>();

        SharedGroup sharedGroup1 = new SharedGroup();
        sharedGroup1.setCreated("2011-06-28T17:26:54Z");
        sharedGroup1.setUpdated("2011-06-28T17:26:54Z");
        sharedGroup1
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/groups/{Openwave-Mainline}");
        sharedGroup1.setSharedGroupId("Openwave-Mainline1");

        sharedGroups1.add(sharedGroup1);

        subscription1.setSharedGroups(sharedGroups1);

        Subscription subscription2 = new Subscription();

        subscription2.setOtherUserId("Bob");

        Set<SharedContact> sharedContacts2 = new HashSet<SharedContact>();

        SharedContact sharedContact2 = new SharedContact();
        sharedContact2.setCreated("2011-06-28T17:26:54Z");
        sharedContact2.setUpdated("2011-06-28T17:26:54Z");
        sharedContact2
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/contacts/{Jane}");
        sharedContact2.setSharedUserId("ABUserName");

        sharedContacts2.add(sharedContact2);
        subscription2.setSharedContacts(sharedContacts2);

        Set<SharedGroup> sharedGroups2 = new HashSet<SharedGroup>();

        SharedGroup sharedGroup2 = new SharedGroup();
        sharedGroup2.setCreated("2011-06-28T17:26:54Z");
        sharedGroup2.setUpdated("2011-06-28T17:26:54Z");
        sharedGroup2
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/groups/{Openwave-CDC}");
        sharedGroup2.setSharedGroupId("Openwave-Mainline1");

        sharedGroups2.add(sharedGroup2);

        subscription2.setSharedGroups(sharedGroups2);

        subscriptions.add(subscription1);
        subscriptions.add(subscription2);

        return subscriptions;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        return;
    }
}
