/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.stub.utils.PopulateMessage;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.message.IBodyService;

/**
 * Message Body service exposed to client which is responsible for doing basic
 * message Body related activities e.g read Message Body, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class StubBodyService implements IBodyService {

    @Override
    public Body read(Map<String, List<String>> inputParams)
            throws MxOSException {
        String messageId = "08bd2d14-b875-11e2-bd3f-8f232b03b603";
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            messageId = inputParams.get(MessageProperty.messageId.name())
                    .get(0);
        }
        return PopulateMessage.getMessageBody(messageId);
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
       return;
        
    }
}
