/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.message;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.stub.utils.PopulateMessage;

/**
 * Message Header service exposed to client which is responsible for doing basic
 * message Header related activities e.g read Message Header, etc. directly in
 * the database.
 * 
 * @author mxos-dev
 */
public class StubMetadataService implements IMetadataService {

    
    @Override
    public Metadata read(final Map<String, List<String>> inputParams)
            throws MxOSException{
        String messageId = "2342342342";
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            messageId = inputParams.get(MessageProperty.messageId.name())
                    .get(0);
        }
        return PopulateMessage.getMessageMetaData(messageId);
    }
    
    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException{}
    
    @Override
    public void updateMulti(final Map<String, List<String>> inputParams)
            throws MxOSException{}

    @Override
    public Map<String, Metadata> list(
            final Map<String, List<String>> inputParams) throws MxOSException {
        String messageId = "45947506-df14-11e2-9607-158da8862fd2";
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            messageId = inputParams.get(MessageProperty.messageId.name())
                    .get(0);
        }
        Map<String, Metadata> data = new HashMap<String, Metadata>();
        data.put(messageId, PopulateMessage.getMessageMetaData(messageId));
        return data;
    }

    @Override
    public Map<String, Metadata> listUIDs(
            final Map<String, List<String>> inputParams) throws MxOSException {
        String messageId = "50770988-f306-11e2-bde6-086a36e277d8";
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            messageId = inputParams.get(MessageProperty.messageId.name())
                    .get(0);
        }
        Map<String, Metadata> data = new HashMap<String, Metadata>();
        data.put(messageId, PopulateMessage.getMessageMetaDataUid(messageId));
        return data;
    }
    
    @Override
    public Map<String, Metadata> readMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        String messageId = "2342342342";
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            messageId = inputParams.get(MessageProperty.messageId.name())
                    .get(0);
        }
        Map<String, Metadata> data = new HashMap<String, Metadata>();
        data.put(messageId, PopulateMessage.getMessageMetaData(messageId));
        return data;
    }

    @Override
    public Map<String, Map<String, Metadata>> search(
            Map<String, List<String>> inputParams,
            SearchTerm searchTerm) throws MxOSException {
        String messageId = "f8e03551-07ae-3303-960b-ad0e18575bc0";
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            messageId = inputParams.get(MessageProperty.messageId.name())
                    .get(0);
        }
        final Map<String, Metadata> data
            = new HashMap<String, Metadata>();
        final Map<String, Map<String, Metadata>> searchData
            = new HashMap<String, Map<String, Metadata>>();
        Metadata metaData = new Metadata();
        metaData.setUid(1002l);
        metaData.setType("EMAIL");
        metaData.setPriority("normal");
        metaData.setFlagRecent(true);
        metaData.setFlagSeen(true);
        metaData.setFlagUnread(false);
        metaData.setFlagAns(false);
        metaData.setFlagFlagged(false);
        metaData.setFlagDel(false);
        metaData.setFlagBounce(false);
        metaData.setFlagPriv(false);
        metaData.setFlagDraft(false);
        metaData.setKeywords(Arrays.asList("abcd", "xyz", "pqr"));
        metaData.setHasAttachments(true);
        metaData.setDeliverNDR(4);
        metaData.setSentDate(6234234123l);
        metaData.setArrivalTime(6234234123l);
        metaData.setLastAccessedTime(6234234236l);
        metaData.setExpireOn(232344000l);
        metaData.setSize(696l);
        metaData.setThreadId("thread-1231451");
        metaData.setFrom("Bob <bob@openwave.com>");
        metaData.setTo(Arrays.asList("Foo <foo@openwave.com>"));
        metaData.setCc(Arrays.asList("Foo <foo@openwave.com>"));
        metaData.setSubject("Test Subject");
        data.put(messageId, metaData);
        searchData.put("0", data);
        return searchData;
    }
}
