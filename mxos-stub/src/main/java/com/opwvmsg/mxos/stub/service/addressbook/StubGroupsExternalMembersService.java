/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsExternalMembersService;

/**
 * Groups External Members service exposed to client which is responsible for
 * doing basic Group related activities e.g create, update, read, search and
 * delete External Members via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubGroupsExternalMembersService implements
        IGroupsExternalMembersService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long externalMemberId = 2;
        if (inputParams
                .containsKey(AddressBookProperty.externalMemberId.name())) {
            externalMemberId = Long.parseLong(inputParams.get(
                    AddressBookProperty.externalMemberId.name()).get(0));
        }
        return externalMemberId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public ExternalMember read(Map<String, List<String>> inputParams)
            throws MxOSException {

        ExternalMember externalMember = new ExternalMember();
        externalMember.setMemberName("Jane");
        externalMember.setMemberEmail("jane@example.org");

        return externalMember;
    }

    @Override
    public List<ExternalMember> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<ExternalMember> sharedContacts = new ArrayList<ExternalMember>();

        ExternalMember externalMember1 = new ExternalMember();
        externalMember1.setMemberName("Jane");
        externalMember1.setMemberEmail("jane@example.org");

        ExternalMember externalMember2 = new ExternalMember();
        externalMember2.setMemberName("Jane");
        externalMember2.setMemberEmail("jane@example.org");

        sharedContacts.add(externalMember1);
        sharedContacts.add(externalMember2);

        return sharedContacts;
    }
}
