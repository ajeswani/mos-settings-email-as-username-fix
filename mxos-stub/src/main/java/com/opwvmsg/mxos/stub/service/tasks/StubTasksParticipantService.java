/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.tasks;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksParticipantService;
import com.opwvmsg.mxos.task.pojos.Participant;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubTasksParticipantService implements ITasksParticipantService {

    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public Participant read(Map<String, List<String>> inputParams)
            throws MxOSException {
        Participant participant = new Participant();
        return participant;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public List<Participant> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        return null;
    }
}
