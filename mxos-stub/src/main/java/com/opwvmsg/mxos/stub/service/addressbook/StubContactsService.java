/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.MaritalStatus;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsService implements IContactsService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long userId = 2;
        if (inputParams.containsKey(AddressBookProperty.userId.name())) {
            userId = Long.parseLong(inputParams.get(
                    AddressBookProperty.userId.name()).get(0));
        }
        return userId;
    }

    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        String userId = null;
        if (inputParams.containsKey(AddressBookProperty.userId.name())) {
            userId = inputParams.get(AddressBookProperty.userId.name()).get(0);
        }
        final Long id = Long.parseLong(userId);
        List<Long> contactIdList = new ArrayList<Long>() {
            {
                add(id);
            }
        };
        return contactIdList;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public List<Contact> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        List<Contact> list = new ArrayList<Contact>();

        // Base Information
        ContactBase contactBase1 = new ContactBase();
        contactBase1.setContactId("123");
        contactBase1.setCreated("2011-06-28T17:26:54Z");
        contactBase1.setUpdated("2011-06-28T17:26:54Z");
        contactBase1.setIsPrivate(BooleanType.YES);
        contactBase1.setNotes("some notes about the contact1");
        contactBase1.setCustomFields("comma separated custom field1");

        // Name Information
        Name contactName1 = new Name();
        contactName1.setDisplayName("DisplayName");
        contactName1.setFirstName("FirstName");
        contactName1.setMiddleName("MiddleName");
        contactName1.setLastName("LastName");
        contactName1.setNickName("NickName");
        contactName1.setPrefix("Prefix");
        contactName1.setSuffix("Suffix");

        // Work Information
        WorkInfo contactWI1 = new WorkInfo();
        contactWI1.setCompanyName("CompanyName");
        contactWI1.setAssistant("Assistant");
        contactWI1.setAssistantPhone("999999999");
        contactWI1.setDepartment("Department");
        contactWI1.setEmployeeId("123");
        contactWI1.setManager("Manager");
        contactWI1.setTitle("Title");
        contactWI1.setWebPage("https://www.workWebPage.com");

        Address contactWIA1 = new Address();
        contactWIA1.setName("Name");
        contactWIA1.setStreet("Street");
        contactWIA1.setCity("City");
        contactWIA1.setStateOrProvince("State");
        contactWIA1.setPostalCode("4113234");
        contactWIA1.setCountry("Country");
        contactWIA1.setCountryCode("CA");
        contactWIA1
                .setUnstructuredAddress("Street. City. Country. CountryCode");

        Communication contactWIC1 = new Communication();
        contactWIC1.setPhone1("123456");
        contactWIC1.setPhone2("123456");
        contactWIC1.setVoip("123456");
        contactWIC1.setFax("123456");
        contactWIC1.setEmail("email@workEmail.com");
        contactWIC1.setMobile("123456");
        contactWIC1.setImAddress("email@workIM.com");

        contactWI1.setAddress(contactWIA1);
        contactWI1.setCommunication(contactWIC1);

        // Personal Information
        PersonalInfo contactPI1 = new PersonalInfo();
        contactPI1.setChildren("Child");
        contactPI1.setSpouseName("SpouseName");
        contactPI1.setHomeWebpage("https://www.homeWebpage.com");
        contactPI1.setMaritalStatus(MaritalStatus.MARRIED);

        Address contactPIA1 = new Address();
        contactPIA1.setName("Name1");
        contactPIA1.setStreet("Street1");
        contactPIA1.setCity("City1");
        contactPIA1.setStateOrProvince("State1");
        contactPIA1.setPostalCode("411111");
        contactPIA1.setCountry("Country1");
        contactPIA1.setCountryCode("CA1");
        contactPIA1
                .setUnstructuredAddress("Street1. City1. Country1. CountryCode1");

        Communication contactPIC1 = new Communication();
        contactPIC1.setPhone1("08080808");
        contactPIC1.setPhone2("08080808");
        contactPIC1.setVoip("08080808");
        contactPIC1.setFax("08080808");
        contactPIC1.setEmail("email@personalEmail.com");
        contactPIC1.setMobile("08080808");
        contactPIC1.setImAddress("email@personalIM.com");

        List<Event> eventList = new ArrayList<Event>();
        Event contactPIE1 = new Event();
        contactPIE1.setType(Event.Type.BIRTHDAY);
        contactPIE1.setDate("1980-06-11T00:00:00Z");
        eventList.add(contactPIE1);

        Event contactPIE2 = new Event();
        contactPIE2.setType(Event.Type.ANNIVERSARY);
        contactPIE2.setDate("2004-11-23T00:00:00Z");
        eventList.add(contactPIE2);

        contactPI1.setAddress(contactPIA1);
        contactPI1.setCommunication(contactPIC1);
        contactPI1.setEvents(eventList);

        // Image
        Image contactImage1 = new Image();
        contactImage1.setImageUrl("http://mxosHost:mxosPort/ImageURL");
        Contact contact1 = new Contact();

        contact1.setContactBase(contactBase1);
        contact1.setName(contactName1);
        contact1.setPersonalInfo(contactPI1);
        contact1.setWorkInfo(contactWI1);
        contact1.setImage(contactImage1);

        // Base Information
        ContactBase contactBase2 = new ContactBase();
        contactBase2.setContactId("345");
        contactBase2.setCreated("2013-06-28T17:26:54Z");
        contactBase2.setUpdated("2013-06-28T17:26:54Z");
        contactBase2.setIsPrivate(BooleanType.YES);
        contactBase2.setNotes("some notes about the contact2");
        contactBase2.setCustomFields("comma separated custom field2");

        // Name Information
        Name contactName2 = new Name();
        contactName2.setDisplayName("_Display_Name_");
        contactName2.setFirstName("_First_Name_");
        contactName2.setMiddleName("_Middle_Name_");
        contactName2.setLastName("_Last_Name_");
        contactName2.setNickName("_Nick_Name_");
        contactName2.setPrefix("_Prefix_");
        contactName2.setSuffix("_Suffix_");

        // Work Information
        WorkInfo contactWI2 = new WorkInfo();
        contactWI2.setCompanyName("_Company_Name_");
        contactWI2.setAssistant("_Assistant)");
        contactWI2.setAssistantPhone("888888888");
        contactWI2.setDepartment("_Department_");
        contactWI2.setEmployeeId("456");
        contactWI2.setManager("_Manager_");
        contactWI2.setTitle("_Title_");
        contactWI2.setWebPage("https://www.work_Web_Page.com");

        Address contactWIA2 = new Address();
        contactWIA2.setName("_Name_");
        contactWIA2.setStreet("_Street_");
        contactWIA2.setCity("_City_");
        contactWIA2.setStateOrProvince("_State_");
        contactWIA2.setPostalCode("88888888");
        contactWIA2.setCountry("_Country_");
        contactWIA2.setCountryCode("AA");
        contactWIA2
                .setUnstructuredAddress("Street11. City11. Country11. CountryCode11");

        Communication contactWIC2 = new Communication();
        contactWIC2.setPhone1("87878787");
        contactWIC2.setPhone2("87878787");
        contactWIC2.setVoip("87878787");
        contactWIC2.setFax("87878787");
        contactWIC2.setEmail("email@workEmail123.com");
        contactWIC2.setMobile("878787");
        contactWIC2.setImAddress("email@workIM123.com");

        contactWI2.setAddress(contactWIA2);
        contactWI2.setCommunication(contactWIC2);

        // Personal Information
        PersonalInfo contactPI2 = new PersonalInfo();
        contactPI2.setChildren("Child, Child2");
        contactPI2.setHomeWebpage("https://www.homeWebpage123.com");
        contactPI2.setMaritalStatus(MaritalStatus.SINGLE);

        Address contactPIA2 = new Address();
        contactPIA2.setName("Name1");
        contactPIA2.setStreet("Street1");
        contactPIA2.setCity("City1");
        contactPIA2.setStateOrProvince("State1");
        contactPIA2.setPostalCode("411111");
        contactPIA2.setCountry("Country1");
        contactPIA2.setCountryCode("CA1");
        contactPIA1
                .setUnstructuredAddress("Street1. City1. Country1. CountryCode1");

        Communication contactPIC2 = new Communication();
        contactPIC2.setPhone1("08080808");
        contactPIC2.setPhone2("08080808");
        contactPIC2.setVoip("08080808");
        contactPIC2.setFax("08080808");
        contactPIC2.setEmail("email@personalEmail.com");
        contactPIC2.setMobile("08080808");
        contactPIC2.setImAddress("email@personalIM.com");

        List<Event> eventList1 = new ArrayList<Event>();
        Event contactPIE11 = new Event();
        contactPIE11.setType(Event.Type.BIRTHDAY);
        contactPIE11.setDate("1980-06-11T00:00:00Z");
        eventList1.add(contactPIE11);

        Event contactPIE22 = new Event();
        contactPIE22.setType(Event.Type.ANNIVERSARY);
        contactPIE22.setDate("2004-11-23T00:00:00Z");
        eventList1.add(contactPIE22);

        contactPI2.setAddress(contactPIA2);
        contactPI2.setCommunication(contactPIC2);
        contactPI2.setEvents(eventList1);

        // Image
        Image contactImage2 = new Image();
        contactImage2.setImageUrl("http://mxosHost:mxosPort/ImageURL");
        Contact contact2 = new Contact();

        contact2.setContactBase(contactBase2);
        contact2.setName(contactName2);
        contact2.setPersonalInfo(contactPI2);
        contact2.setWorkInfo(contactWI2);
        contact2.setImage(contactImage2);

        list.add(contact1);
        list.add(contact2);
        return list;
    }

    @Override
    public void moveMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
    }
}
