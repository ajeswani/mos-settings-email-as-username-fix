/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.GroupLink;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoGroupLinksService;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsPersonalInfoGroupLinksService implements
        IContactsPersonalInfoGroupLinksService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long notificationId = 2;
        if (inputParams.containsKey(AddressBookProperty.notificationId.name())) {
            notificationId = Long.parseLong(inputParams.get(
                    AddressBookProperty.notificationId.name()).get(0));
        }
        return notificationId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public GroupLink read(Map<String, List<String>> inputParams)
            throws MxOSException {

        GroupLink groupLink = new GroupLink();
        groupLink.setGroupId("MxOSTeam");
        groupLink
                .setHref("http://mxos.com:8080/addressbook/v1/{ABUserName}/groups/{MxOSTeam}/members/{Bob}");

        return groupLink;
    }

    @Override
    public List<GroupLink> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<GroupLink> groupLinks = new ArrayList<GroupLink>();

        GroupLink groupLink1 = new GroupLink();
        groupLink1.setGroupId("MxOSTeam1");
        groupLink1
                .setHref("http://mxos.com:8080/addressbook/v1/{ABUserName}/groups/{MxOSTeam}/members/{Bob}");

        GroupLink groupLink2 = new GroupLink();
        groupLink2.setGroupId("MxOSTeam2");
        groupLink2
                .setHref("http://mxos.com:8080/addressbook/v1/{ABUserName}/groups/{MxOSTeam}/members/{John}");

        groupLinks.add(groupLink1);
        groupLinks.add(groupLink2);

        return groupLinks;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        return;
    }
}
