/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.logging;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
/**
 * Mailbox logging service exposed to client which is responsible for doing basic
 * mailbox related logging activities.
 *
 * @author mxos-dev
 */
public class StubLoggingMailboxService implements IMailboxService {
    /**
     * Default Constructor.
     */
    public StubLoggingMailboxService() {
    }

    @Override
    public long create(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }

	@Override
	public Mailbox read(Map<String, List<String>> inputParams)
			throws MxOSException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Mailbox> list(Map<String, List<String>> inputParams)
			throws MxOSException {
		// TODO Auto-generated method stub
		return null;
	}

}
