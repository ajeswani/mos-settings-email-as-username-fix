/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.stub.service;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceLoader;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * mailbox related activities e.g create mailbox, delete mailbox, etc. directly
 * in the database.
 *
 * @author mxos-dev
 */
public final class StubMxOSContext extends IMxOSContext {
    private static Logger logger = Logger.getLogger(StubMxOSContext.class);

    /**
     * Default constructor.
     *
     * @throws Exception Exception
     */
    public StubMxOSContext() throws Exception {
        serviceMap = ServiceLoader.loadServices(ContextEnum.STUB);
        logger.info("StubMxOSContext created...");
    }

    @Override
    public void init(final String contextId, final Properties p)
            throws MxOSException {
        this.contextId = contextId;
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }
}
