/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * MailSend service exposed to client which is responsible for doing basic
 * MailSend related activities e.g read and update MailSend via Stub of
 * MxOS.
 *
 * @author mxos-dev
 */
public class StubMailSendService implements IMailSendService {

    @Override
    public MailSend read(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return PopulateMailbox.getMailSend();
    }

    @Override
    public void update(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }
}
