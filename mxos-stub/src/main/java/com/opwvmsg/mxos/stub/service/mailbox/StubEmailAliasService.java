/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * EmailAlias service exposed to client which is responsible for doing basic
 * EmailAlias related activities e.g create, update, read and delete aliases via
 * Stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubEmailAliasService implements IEmailAliasService {

    @Override
    public void create(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public List<String> read(
            final Map<String, List<String>> inputParams) throws MxOSException {
        String email = "foo@bar.com";
        if (inputParams.containsKey(MailboxProperty.email.name())) {
            email = inputParams.get(MailboxProperty.email.name()).get(0);
        }
        return PopulateMailbox.getMailbox(email).getBase().getEmailAliases();
    }

    @Override
    public void update(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public void delete(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }
}
