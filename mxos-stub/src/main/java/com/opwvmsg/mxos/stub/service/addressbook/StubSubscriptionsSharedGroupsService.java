/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.SharedGroup;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.ISubscriptionsSharedGroupsService;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubSubscriptionsSharedGroupsService implements
        ISubscriptionsSharedGroupsService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long sharedGroupId = 2;
        if (inputParams.containsKey(AddressBookProperty.sharedGroupId.name())) {
            sharedGroupId = Long.parseLong(inputParams.get(
                    AddressBookProperty.sharedGroupId.name()).get(0));
        }
        return sharedGroupId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public SharedGroup read(Map<String, List<String>> inputParams)
            throws MxOSException {

        SharedGroup sharedGroup = new SharedGroup();
        sharedGroup.setCreated("2011-06-28T17:26:54Z");
        sharedGroup.setUpdated("2011-06-28T17:26:54Z");
        sharedGroup
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/groups/{Openwave-Mainline}");
        sharedGroup.setSharedGroupId("Openwave-Mainline1");

        return sharedGroup;
    }

    @Override
    public List<SharedGroup> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<SharedGroup> sharedGroups = new ArrayList<SharedGroup>();

        SharedGroup sharedGroup1 = new SharedGroup();
        sharedGroup1.setCreated("2011-06-28T17:26:54Z");
        sharedGroup1.setUpdated("2011-06-28T17:26:54Z");
        sharedGroup1
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/groups/{Openwave-Mainline}");
        sharedGroup1.setSharedGroupId("Openwave-Mainline1");

        SharedGroup sharedGroup2 = new SharedGroup();
        sharedGroup2.setCreated("2011-06-28T17:26:54Z");
        sharedGroup2.setUpdated("2011-06-28T17:26:54Z");
        sharedGroup2
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/groups/{Openwave-Mainline}");
        sharedGroup2.setSharedGroupId("Openwave-Mainline2");

        sharedGroups.add(sharedGroup1);
        sharedGroups.add(sharedGroup2);

        return sharedGroups;
    }
}
