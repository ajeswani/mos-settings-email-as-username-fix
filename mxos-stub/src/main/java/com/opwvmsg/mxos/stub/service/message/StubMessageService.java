/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.stub.utils.PopulateMessage;

/**
 * Message service exposed to client which is responsible for doing basic
 * Message related activities e.g create, update, read, search and delete
 * Message via Stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubMessageService implements IMessageService {
    private static String messageId = "2342342342";
    private static long uid = 123456;

    @Override
    public String create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return messageId;

    }

    @Override
    public Message read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (inputParams.containsKey(MessageProperty.messageId.name())) {
            messageId = inputParams.get(MessageProperty.messageId.name())
                    .get(0);
        }
        return PopulateMessage.getMessage(messageId);
    }

    @Override
    public Message readByUID(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (inputParams.containsKey(MessageProperty.uid.name())) {
            try {
                uid = Integer.parseInt(inputParams.get(
                        MessageProperty.uid.name()).get(0));
            } catch (Exception e) {

            }
        }
        final Message message = PopulateMessage.getMessage(messageId);
        message.getMetadata().setUid(uid);
        return message;
    }

    @Override
    public void updateByUID(final Map<String, List<String>> inputParams)
        throws MxOSException {
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
        throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
        throws MxOSException {
        return;
    }

    @Override
    public void copy(final Map<String, List<String>> inputParams)
            throws MxOSException {
    }

    @Override
    public void copyAll(final Map<String, List<String>> inputParams)
            throws MxOSException {
    }

    @Override
    public void move(Map<String, List<String>> inputParams)
            throws MxOSException {
    }

    @Override
    public void moveAll(Map<String, List<String>> inputParams)
            throws MxOSException {
    }

    @Override
    public void deleteMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
    }

    @Override
    public void copyMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
    }

    @Override
    public void moveMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
    }
}
