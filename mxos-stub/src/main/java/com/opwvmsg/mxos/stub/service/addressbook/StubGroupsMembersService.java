/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsMembersService;

/**
 * Groups Members service exposed to client which is responsible for doing basic
 * Group related activities e.g create, update, read, search and delete Members
 * via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubGroupsMembersService implements IGroupsMembersService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long externalMemberId = 2;
        if (inputParams.containsKey(AddressBookProperty.memberId.name())) {
            externalMemberId = Long.parseLong(inputParams.get(
                    AddressBookProperty.memberId.name()).get(0));
        }
        return externalMemberId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public Member read(Map<String, List<String>> inputParams)
            throws MxOSException {

        Member member = new Member();
        member.setMemberId("Bob");
        member.setHref("http://mxos.com:8080/addressbook/v1/{Bob}/shared/{ABUserName}/groups/{MxOSTeam}");

        return member;
    }

    @Override
    public List<Member> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<Member> sharedContacts = new ArrayList<Member>();

        Member member1 = new Member();
        member1.setMemberId("Bob");
        member1.setHref("http://mxos.com:8080/addressbook/v1/{Bob}/shared/{ABUserName}/groups/{MxOSTeam}");

        Member member2 = new Member();
        member2.setMemberId("John");
        member2.setHref("http://mxos.com:8080/addressbook/v1/{John}/shared/{ABUserName}/groups/{MxOSTeam}");

        sharedContacts.add(member1);
        sharedContacts.add(member2);

        return sharedContacts;
    }
}
