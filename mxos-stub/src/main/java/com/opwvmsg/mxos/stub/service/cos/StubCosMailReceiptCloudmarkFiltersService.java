/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.stub.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptCloudmarkFiltersService;
import com.opwvmsg.mxos.stub.utils.PopulateCos;

/**
 * Mailbox mailReceipt cloudmark filter operations interface which will be
 * exposed to the client. This interface is responsible for doing mailReceipt
 * cloudmark filter related operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class StubCosMailReceiptCloudmarkFiltersService implements
        ICosMailReceiptCloudmarkFiltersService {

    @Override
    public CloudmarkFilters read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return PopulateCos.getMailReceipt().getFilters().getCloudmarkFilters();
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {

    }
}
