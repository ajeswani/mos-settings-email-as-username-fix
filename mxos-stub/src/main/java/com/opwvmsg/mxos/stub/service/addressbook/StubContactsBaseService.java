/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsBaseService;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsBaseService implements IContactsBaseService {

    @Override
    public List<ContactBase> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        List<ContactBase> list = new ArrayList<ContactBase>();

        ContactBase contactBase1 = new ContactBase();
        contactBase1.setContactId("Bob");
        contactBase1.setCreated("2011-06-28T17:26:54Z");
        contactBase1.setUpdated("2011-06-28T17:26:54Z");
        contactBase1.setIsPrivate(BooleanType.YES);
        contactBase1.setNotes("some notes about the contact1");
        contactBase1.setCustomFields("comma separated custom field1");

        ContactBase contactBase2 = new ContactBase();
        contactBase2.setContactId("John");
        contactBase2.setCreated("2011-06-28T18:26:54Z");
        contactBase2.setUpdated("2011-06-28T18:26:54Z");
        contactBase2.setIsPrivate(BooleanType.NO);
        contactBase2.setNotes("some notes about the contact2");
        contactBase2.setCustomFields("comma separated custom field2");

        list.add(contactBase1);
        list.add(contactBase2);

        return list;
    }

    @Override
    public ContactBase read(Map<String, List<String>> inputParams)
            throws MxOSException {
        ContactBase contactBase = new ContactBase();
        contactBase.setContactId("Bob");
        contactBase.setCreated("2011-06-28T17:26:54Z");
        contactBase.setUpdated("2011-06-28T17:26:54Z");
        contactBase.setIsPrivate(BooleanType.YES);
        contactBase.setNotes("some notes about the contact");
        contactBase.setCustomFields("comma separated custom field");
        return contactBase;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
