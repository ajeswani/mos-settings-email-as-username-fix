/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.process;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.util.HashMap;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.process.IExternalLdapAttributesService;

public class StubExternalLdapAttributesService implements IExternalLdapAttributesService {

    @Override
    public Map<String, List<String>> readAttributes(
            Map<String, List<String>> inputParams) throws MxOSException {
        Map <String, List<String>> attributeMap=new HashMap<String, List<String>>();
        List <String>msisdnList=new LinkedList<String>();
        msisdnList.add("458784548448");
        List <String>mailloginList=new LinkedList<String>();
        mailloginList.add("458784548448");
        attributeMap.put("msisdn",msisdnList );
        attributeMap.put("maillogin",mailloginList);
        return attributeMap;
    }

}
