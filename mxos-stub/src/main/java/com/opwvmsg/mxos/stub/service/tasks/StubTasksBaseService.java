/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksBaseService;
import com.opwvmsg.mxos.task.pojos.TaskBase;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubTasksBaseService implements ITasksBaseService {

    @Override
    public List<TaskBase> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        List<TaskBase> list = new ArrayList<TaskBase>();

        TaskBase contactBase1 = new TaskBase();
        contactBase1.setTaskId("Bob");
        contactBase1.setCreatedDate("2011-06-28T17:26:54Z");
        contactBase1.setUpdatedDate("2011-06-28T17:26:54Z");
        contactBase1.setIsPrivate(BooleanType.YES);
        contactBase1.setNotes("some notes about the contact1");

        TaskBase contactBase2 = new TaskBase();
        contactBase2.setTaskId("John");
        contactBase2.setCreatedDate("2011-06-28T18:26:54Z");
        contactBase2.setUpdatedDate("2011-06-28T18:26:54Z");
        contactBase2.setIsPrivate(BooleanType.NO);
        contactBase2.setNotes("some notes about the contact2");

        list.add(contactBase1);
        list.add(contactBase2);

        return list;
    }

    @Override
    public TaskBase read(Map<String, List<String>> inputParams)
            throws MxOSException {
        TaskBase contactBase = new TaskBase();
        contactBase.setTaskId("Bob");
        contactBase.setCreatedDate("2011-06-28T17:26:54Z");
        contactBase.setUpdatedDate("2011-06-28T17:26:54Z");
        contactBase.setIsPrivate(BooleanType.YES);
        contactBase.setNotes("some notes about the contact");
        return contactBase;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
