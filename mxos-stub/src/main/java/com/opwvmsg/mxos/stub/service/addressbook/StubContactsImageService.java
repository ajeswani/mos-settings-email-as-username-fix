/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsImageService;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g read and update contact name via Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsImageService implements IContactsImageService {

    @Override
    public Image read(Map<String, List<String>> inputParams)
            throws MxOSException {

        Image image = new Image();
        image.setHeight(180);
        image.setWidth(120);
        image.setSizeKB(1024L);
        image.setImageUrl("imageURLstring");

        return image;
    }
    
    @Override
    public byte[] readImage(Map<String, List<String>> inputParams)
            throws MxOSException {

        return new byte[0];
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
