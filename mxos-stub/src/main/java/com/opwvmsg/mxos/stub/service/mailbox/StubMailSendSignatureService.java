/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendSignatureService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * Mailbox External Mail Account operations interface which will be exposed to
 * the client. This interface is responsible for doing MailAccount related
 * operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class StubMailSendSignatureService implements
 IMailSendSignatureService {

    @Override
    public Long create(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Signature> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return PopulateMailbox.getMailSend().getSignatures();
    }

    @Override
    public Signature readSignature(final Map<String, List<String>> inputParams)
    throws MxOSException{
        return PopulateMailbox.getMailSend().getSignatures().get(0);
    }
    
    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

}
