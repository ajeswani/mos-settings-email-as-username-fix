/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.ExternalStore;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalStoreService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * Mailbox External Store operations interface which will be exposed to the
 * client. This interface is responsible for doing ExternalStore related
 * operations (like Read, Update etc.).
 * 
 * @author ajeswani
 */
public class StubExternalStoreService implements IExternalStoreService {

    @Override
    public ExternalStore read(Map<String, List<String>> inputParams)
            throws MxOSException {
        return (ExternalStore) PopulateMailbox.getMailStore().getExternalStore();
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
    }
}
