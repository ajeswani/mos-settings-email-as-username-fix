/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoAddressService;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsPersonalInfoAddressService implements
        IContactsPersonalInfoAddressService {

    @Override
    public Address read(Map<String, List<String>> inputParams)
            throws MxOSException {

        Address address = new Address();
        address.setName("611 Lakeview Apartments");
        address.setStreet("2100, Seaport Blvd");
        address.setCity("Redwood city");
        address.setStateOrProvince("California");
        address.setPostalCode("45-765-24");
        address.setCountry("USA");
        address.setCountryCode("ISO 3166 Country code");

        return address;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
