/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISieveBlockedSendersService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * SieveBlockedSenders service exposed to client which is responsible for doing
 * basic SieveBlockedSenders related activities e.g create, update, read and
 * delete of Sieve Blocked Senders via Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubSieveBlockedSendersService implements ISieveBlockedSendersService {

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public List<String> read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return PopulateMailbox.getMailReceipt().getFilters().getSieveFilters()
                .getBlockedSenders();
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
