/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.saml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.saml.ISamlService;

/**
 * SAML service exposed to client which is responsible for doing SAML related
 * activities e.g getSAMLRequest, decodeSAMLResponse, etc.
 * 
 * @author mxos-dev
 */
public class StubSamlService implements ISamlService {

    @Override
    public String getSAMLRequest(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return "eJx9kc1OwzAQhF8l8j1%2FLSBk1akKCFGpRVGb9sDNTbapS7I2XieCt8" +
        		"dtqFQ4cPBh7Z3x7LeT6WfbBD1YUhoFS6OEBYClrhTWgm2K5%2F" +
        		"CeTbMJybYxfNa5A67gowNygdch8fODYJ1FriUp4ihbIO5Kvp4tF3wUJdxY7X" +
        		"SpGxbMiMA6%2F9GjRupasGuwvSphs1oIdnDOEI%2Fj9st5%2B3HUK1srbKFSMip1G8uSToc" +
        		"F8yfBpNQ7c9wjgFJVY6paGoTGQFVhvUPZ7A%2FKHOXx3XdTLolUD4LtZUNwuqEO5khOohNslKTjMBmFaVqk" +
        		"d%2Fz2hifJGwvyn8wPCgcSfwaMrgfcDU3EX4oiD1c%2BsYXSnU16VYF99QrBtstcWyc9" +
        		"hu2FthezgS0%2Fh7JXUP9nKi8kWXbxncRXRtlQ%2FV5Z9g2%2FoazW&RelayState=https%3A%" +
        		"2F%2Fmytest3.virginmedia.com%2Fhome%2Findex";
    }

    @Override
    public Map<String, String> decodeSAMLAssertion(
            Map<String, List<String>> inputParams) throws MxOSException {
        Map<String,String> samlAttributes = new HashMap<String, String>();
        samlAttributes.put("vmLoginID", "test@test.com");
        return samlAttributes;
    }
}
