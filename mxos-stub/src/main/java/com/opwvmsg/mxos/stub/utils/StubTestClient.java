package com.opwvmsg.mxos.stub.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.McAfeeFilters;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.VoiceMail;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedDomainService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalAccountsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IGeneralPreferenceService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalMailAccountService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailForwardService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptBMIFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptCloudmarkFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptMcAfeeFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendBMIFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendCommtouchFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendMcAfeeFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailStoreService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISenderBlockingService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsNotificationsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsOnlineService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IVoiceMailService;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksService;

public class StubTestClient {

    static IMxOSContext stubContext = null;

    /**
     * @param args
     */
    public static void main(String[] args) {
        String contextId = "Sample-Stub-2.0";
        Properties props = new Properties();
        props.put(ContextProperty.MXOS_CONTEXT_TYPE,
                ContextEnum.STUB.name());
        try {
            stubContext = MxOSContextFactory.getInstance().getContext(
                    contextId, props);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        mailboxService();
        emailAliasService();
        allowedDomainService();
        mailForwardService();
        credentialService();
        generalPreferenceService();
        mailSendService();
        mailReceiptService();
        mailAccessService();
        mailStoreService();
        mailInternalInfoService();
        folderService();
        addressBookService();
        externalAccountService();
        mailAccountsService();
        mailReceiptBMIFiltersService();
        mailReceiptCloudmarkFiltersService();
        mailReceiptFiltersService();
        mailReceiptMcAfeeFiltersService();
        mailReceiptSenderBlockingService();
        mailReceiptSieveFiltersService();
        mailSendBMIFiltersService();
        mailSendCommontouchFiltersService();
        mailSendFiltersService();
        mailSendMcAfeeFiltersService();
        smsNotificationService();
        smsOnlineService();
        smsService();
        voiceMailService();
    }

    /**
	 * 
	 */
    public static void mailboxService() {
        System.out.println("Testing mailboxService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailboxService mailboxService = (IMailboxService) stubContext
                    .getService(ServiceEnum.MailboxService.name());
            long mailboxId = mailboxService.create(inputParams);
            System.out.println("Returned Object : " + mailboxId);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void emailAliasService() {
        System.out.println("Testing emailAliasService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IEmailAliasService mailAliasService = (IEmailAliasService) stubContext
                    .getService(ServiceEnum.MailAliasService.name());
            List<String> emailAliases = mailAliasService.read(inputParams);
            System.out.println("Returned Object : " + emailAliases);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void allowedDomainService() {
        System.out.println("Testing allowedDomainService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IAllowedDomainService allowedDomainService = (IAllowedDomainService) stubContext
                    .getService(ServiceEnum.AllowedDomainService.name());

            List<String> allowedDomains = allowedDomainService
                    .read(inputParams);
            System.out.println("Returned Object : " + allowedDomains);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailForwardService() {
        System.out.println("Testing mailForwardService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailForwardService mailForwardService = (IMailForwardService) stubContext
                    .getService(ServiceEnum.MailForwardService.name());

            List<String> forwardAddresses = mailForwardService
                    .read(inputParams);
            System.out.println("Returned Object : " + forwardAddresses);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void credentialService() {
        System.out.println("Testing credentialService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            ICredentialService credentialService = (ICredentialService) stubContext
                    .getService(ServiceEnum.CredentialService.name());

            Credentials credentials = credentialService.read(inputParams);
            System.out.println("Returned Object : " + credentials);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void generalPreferenceService() {
        System.out.println("Testing generalPreferenceService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IGeneralPreferenceService generalPreferenceService = (IGeneralPreferenceService) stubContext
                    .getService(ServiceEnum.GeneralPreferenceService
                            .name());

            GeneralPreferences genPref = generalPreferenceService
                    .read(inputParams);
            System.out.println("Returned Object : " + genPref);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailSendService() {
        System.out.println("Testing mailSendService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailSendService mailSendService = (IMailSendService) stubContext
                    .getService(ServiceEnum.MailSendService.name());

            MailSend mailSend = mailSendService.read(inputParams);
            System.out.println("Returned Object : " + mailSend);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailReceiptService() {
        System.out.println("Testing mailReceiptService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailReceiptService mailReceiptService = (IMailReceiptService) stubContext
                    .getService(ServiceEnum.MailReceiptService.name());

            MailReceipt mailReceipt = mailReceiptService.read(inputParams);
            System.out.println("Returned Object : " + mailReceipt);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailAccessService() {
        System.out.println("Testing mailAccessService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailAccessService mailAccessService = (IMailAccessService) stubContext
                    .getService(ServiceEnum.MailAccessService.name());

            MailAccess mailAccess = mailAccessService.read(inputParams);
            System.out.println("Returned Object : " + mailAccess);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailStoreService() {
        System.out.println("Testing mailStoreService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailStoreService mailStoreService = (IMailStoreService) stubContext
                    .getService(ServiceEnum.MailStoreService.name());

            MailStore mailStore = mailStoreService.read(inputParams);
            System.out.println("Returned Object : " + mailStore);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailInternalInfoService() {
        System.out.println("Testing mailInternalInfoService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IInternalInfoService mailInternalInfoService = (IInternalInfoService) stubContext
                    .getService(ServiceEnum.InternalInfoService
                            .name());

            InternalInfo internalInfo = mailInternalInfoService
                    .read(inputParams);
            System.out.println("Returned Object : " + internalInfo);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void folderService() {
        System.out.println("Testing folderService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IFolderService folderService = (IFolderService) stubContext
                    .getService(ServiceEnum.FolderService.name());
            Folder folder = folderService.read(inputParams);
            System.out.println("Returned Object : " + folder);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void addressBookService() {
        System.out.println("Testing addressBookService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            ITasksService addressBookService = (ITasksService) stubContext
                    .getService(ServiceEnum.AddressBookService.name());

            long addressBookId = addressBookService.create(inputParams);
            System.out.println("Returned Object : " + addressBookId);
            addressBookService.delete(inputParams);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void externalAccountService() {
        System.out.println("Testing externalAccountService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IExternalAccountsService externalAccountService = (IExternalAccountsService) stubContext
                    .getService(ServiceEnum.ExternalAccountService
                            .name());

            ExternalAccounts externalAccounts = externalAccountService
                    .read(inputParams);
            System.out.println("Returned Object : " + externalAccounts);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
	 * 
	 */
    public static void mailAccountsService() {
        System.out.println("Testing mailAccountsService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IExternalMailAccountService mailAccountsService = (IExternalMailAccountService) stubContext
                    .getService(ServiceEnum.ExternalMailAccountService.name());

            List<MailAccount> mailAccounts = mailAccountsService.read(inputParams);
            System.out.println("Returned Object : " + mailAccounts);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailReceiptBMIFiltersService() {
        System.out.println("Testing mailReceiptBMIFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailReceiptBMIFiltersService mailReceiptBMIFiltersService = (IMailReceiptBMIFiltersService) stubContext
                    .getService(ServiceEnum.MailReceiptBMIFiltersService
                            .name());

            BmiFilters bmiFilters = mailReceiptBMIFiltersService
                    .read(inputParams);
            System.out.println("Returned Object : " + bmiFilters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailReceiptCloudmarkFiltersService() {
        System.out
                .println("Testing mailReceiptCloudmarkFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailReceiptCloudmarkFiltersService mailReceiptCloudmarkFiltersService = (IMailReceiptCloudmarkFiltersService) stubContext
                    .getService(ServiceEnum.MailReceiptCloudmarkFiltersService
                            .name());

            CloudmarkFilters cloudMarkFilters = mailReceiptCloudmarkFiltersService
                    .read(inputParams);
            System.out.println("Returned Object : " + cloudMarkFilters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailReceiptFiltersService() {
        System.out.println("Testing mailReceiptFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailReceiptFiltersService mailReceiptFiltersService = (IMailReceiptFiltersService) stubContext
                    .getService(ServiceEnum.MailReceiptFiltersService
                            .name());

            Filters mailbox = mailReceiptFiltersService.read(inputParams);
            System.out.println("Returned Object : " + mailbox);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailReceiptMcAfeeFiltersService() {
        System.out
                .println("Testing mailReceiptMcAfeeFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailReceiptMcAfeeFiltersService mailReceiptMcAfeeFiltersService = (IMailReceiptMcAfeeFiltersService) stubContext
                    .getService(ServiceEnum.MailReceiptMcAfeeFiltersService
                            .name());

            McAfeeFilters mcAfeeFilters = mailReceiptMcAfeeFiltersService
                    .read(inputParams);
            System.out.println("Returned Object : " + mcAfeeFilters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailReceiptSenderBlockingService() {
        System.out
                .println("Testing mailReceiptSenderBlockingService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            ISenderBlockingService mailReceiptSenderBlockingService = (ISenderBlockingService) stubContext
                    .getService(ServiceEnum.SenderBlockingService
                            .name());

            SenderBlocking senderBlocking = mailReceiptSenderBlockingService
                    .read(inputParams);
            System.out.println("Returned Object : " + senderBlocking);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailReceiptSieveFiltersService() {
        System.out.println("Testing mailReceiptSieveFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailReceiptSieveFiltersService mailReceiptSieveFiltersService = (IMailReceiptSieveFiltersService) stubContext
                    .getService(ServiceEnum.MailReceiptSieveFiltersService
                            .name());

            SieveFilters sieveFilters = mailReceiptSieveFiltersService
                    .read(inputParams);
            System.out.println("Returned Object : " + sieveFilters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailSendBMIFiltersService() {
        System.out.println("Testing mailSendBMIFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailSendBMIFiltersService mailSendBMIFiltersService = (IMailSendBMIFiltersService) stubContext
                    .getService(ServiceEnum.MailSendBMIFiltersService
                            .name());

            BmiFilters bmiFilters = mailSendBMIFiltersService.read(inputParams);
            System.out.println("Returned Object : " + bmiFilters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailSendCommontouchFiltersService() {
        System.out
                .println("Testing mailSendCommontouchFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailSendCommtouchFiltersService mailSendCommtouchFiltersService = (IMailSendCommtouchFiltersService) stubContext
                    .getService(ServiceEnum.MailSendCommtouchFiltersService
                            .name());

            CommtouchFilters commtouchFilters = mailSendCommtouchFiltersService
                    .read(inputParams);
            System.out.println("Returned Object : " + commtouchFilters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailSendFiltersService() {
        System.out.println("Testing mailSendFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailSendFiltersService mailSendFilterService = (IMailSendFiltersService) stubContext
                    .getService(ServiceEnum.MailSendFiltersService
                            .name());

            Filters filters = mailSendFilterService.read(inputParams);
            System.out.println("Returned Object : " + filters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void mailSendMcAfeeFiltersService() {
        System.out.println("Testing mailSendMcAfeeFiltersService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IMailSendMcAfeeFiltersService mcAfeeFilterService = (IMailSendMcAfeeFiltersService) stubContext
                    .getService(ServiceEnum.MailSendMcAfeeFiltersService
                            .name());

            McAfeeFilters mcAfeeFilters = mcAfeeFilterService.read(inputParams);
            System.out.println("Returned Object : " + mcAfeeFilters);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void smsNotificationService() {
        System.out.println("Testing smsNotificationService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            ISmsNotificationsService smsNotificationService = (ISmsNotificationsService) stubContext
                    .getService(ServiceEnum.SmsNotificationsService
                            .name());

            SmsNotifications smsNotifications = smsNotificationService
                    .read(inputParams);
            System.out.println("Returned Object : " + smsNotifications);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void smsOnlineService() {
        System.out.println("Testing smsOnlineService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            ISmsOnlineService smsOnlineService = (ISmsOnlineService) stubContext
                    .getService(ServiceEnum.SmsOnlineService.name());

            SmsOnline smsOnline = smsOnlineService.read(inputParams);
            System.out.println("Returned Object : " + smsOnline);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void smsService() {
        System.out.println("Testing smsService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            ISmsServicesService smsServicesService = (ISmsServicesService) stubContext
                    .getService(ServiceEnum.SmsServicesService.name());
            SmsServices smsServices = smsServicesService.read(inputParams);
            System.out.println("Returned Object : " + smsServices);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
	 * 
	 */
    public static void voiceMailService() {
        System.out.println("Testing voiceMailService service...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        List<String> email = new ArrayList<String>();
        email.add("zz123456@openwave.com");
        inputParams.put(MailboxProperty.email.name(), email);
        try {
            IVoiceMailService voiceMailService = (IVoiceMailService) stubContext
                    .getService(ServiceEnum.VoiceMailService.name());

            VoiceMail voiceMail = voiceMailService.read(inputParams);
            System.out.println("Returned Object : " + voiceMail);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
