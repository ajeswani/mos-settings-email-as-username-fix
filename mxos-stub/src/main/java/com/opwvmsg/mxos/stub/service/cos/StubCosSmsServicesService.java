/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.stub.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsServicesService;
import com.opwvmsg.mxos.stub.utils.PopulateCos;

/**
 * COS Mailbox SMS Service operations interface which will be exposed to the client.
 * This interface is responsible for doing mailbox SMS Service related
 * operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class StubCosSmsServicesService implements ICosSmsServicesService {

    @Override
    public SmsServices read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return PopulateCos.getSmsServices();
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {

    }
}
