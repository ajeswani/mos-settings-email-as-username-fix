/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.utils;

/**
 * Constants for SMS API's.
 *
 * @author mxos-dev
 */
public interface SmsConstants {

    String MXOS_HOME = "MXOS_HOME";

    String SDUP_NAMESPACE_URI = "sdupNamespaceUri";
    String SDUP_SERVICENAME = "sdupServiceName";

    String VAMP_PARAM_SOURCE = "source";
    String VAMP_PARAM_RECIPIENT = "recipient";
    String VAMP_PARAM_MESSAGE = "message";
    String VAMP_PARAM_AUTH = "auth";
    String VAMP_PARAM_ID = "id";
    String VAMP_PARAM_SENDER = "sender";
    String VAMP_PARAM_PRODUCT = "product";
    String VAMP_PARAM_SERVICE_CODE = "servicecode";
    String VAMP_PARAM_TYPE = "type";

    String VAMP_PID = "pid";
    String VAMP_DCS = "dcs";
    String VAMP_UDH = "udh";
    String VAMP_DELIVERY_REPORT_REQUEST = "deliveryReportRequest";
    String VAMP_MESSAGE_NL = "messageNL";
    String VAMP_MESSAGE_FR = "messageFR";
    String VAMP_MESSAGE_EN = "messageEN";
    String VAMP_MESSAGE_DE = "messageDE";

    String FBI_PARAM_SOURCE = "source";
    String FBI_PARAM_RECIPIENT = "recipient";
    String FBI_PARAM_MESSAGE = "message";
    String FBI_PARAM_AUTH = "auth";
    String FBI_PARAM_ID = "id";
    String FBI_PARAM_SENDER = "sender";
    String FBI_PARAM_PRODUCT = "product";
    String FBI_PARAM_SERVICE_CODE = "servicecode";
    String FBI_PARAM_TYPE = "type";

    String MAA_PARAM_APP_LOGIN = "applogin";
    String MAA_PARAM_APP_PASSWORD = "apppassword";
    String MAA_PARAM_APP_CALL = "apicall";
    String MAA_PARAM_APP_TIMESTAMP = "timestamp";
    String MAA_PARAM_KEY = "key";
    String MAA_PARAM_PAYLOAD = "payload";
    String MAA_PARAM_RESULTCODE = "resultcode";
    String MAA_PARAM_CRUD = "crud";

    String VAMP_RESPONSE_SUCCESS_CODE = "0";
    String FBI_RESPONSE_SUCCESS_CODE = "0";
    int SDUP_RESPONSE_SUCCESS_CODE = 0;
    String MAA_RESPONSE_SUCCESS_CODE = "0";

    String SDUP_SERVICE_PROVIDER = "serviceProvider";
    String SDUP_SERVICE_PROVIDER_TYPE = "serviceProviderType";
    String SDUP_SMS_MO_BARRING = "smsMObarring";
    String SDUP_STATUS = "status";

    String VAMP_RESPONSE_ERROR_TAG = "error";
    String VAMP_RESPONSE_CODE_TAG = "code";

    String FBI_RESPONSE_ERROR_TAG = "error";
    String FBI_RESPONSE_CODE_TAG = "code";

    String MAA_RESPONSE_RESULT_TAG = "RESULT";
    String MAA_RESPONSE_CODE_TAG = "code";

    String SMS_GATEWAY_CFG_FILENAME = "gateway-config.xml";
    String MSISDN_EMAIL_MESSAGE_FILENAME = "msisdn-messages.xml";

    String SENDSMSGATEWAY = "SendSMSGateway";
    String AUTHGATEWAY = "AuthGateway";
    String LOGTOEXTERNALENTITYGATEWAY = "LogToExternalEntityGateway";
    String DEPOSITMAILGATEWAY = "DepositMailGateway";
    String SENDMAILGATEWAY = "SendMailGateway";

    String MSISDN_MESSAGE_XML_TAG_MESSAGE = "message";
    String MSISDN_MESSAGE_XML_TAG_MESSAGE_ID = "id";
    String MSISDN_MESSAGE_XML_TAG_MESSAGE_LANG = "lang";

    String SENDSMSGATEWAYS = "SendSMSGateways";
    String AUTHGATEWAYS = "AuthGateways";
    String LOGTOEXTERNALENTITYGATEWAYS = "LogToExternalEntityGateways";
    String DEPOSITMAILGATEWAYS = "DepositMailGateways";
    String SENDMAILGATEWAYS = "SendMailGateways";

    String TYPE = "type";
    String PROVIDER = "Provider";
    String URL = "URL";
    String AUTH = "Auth";

    String SOURCE = "Source";
    String PRODUCT = "Product";
    String SERVICECODE = "ServiceCode";
    String CONTENTTYPE = "ContentType";
    String SENDSMSTEMPERRORLIST = "SendSmsTempErrorList";
    String SENDSMSSNMPSENDLIST = "SendSmsSnmpList";
    String SENDSMSTIMEOUTSNMPSEND = "SendSmsTimeoutSnmpSend";
    String SENDSMSTIMEOUT = "SendSmsTimeout";
    String SENDSMSHOST = "SendSMSHost";
    String SENDSMSPORT = "SendSMSPort";
    String SENDSMSMAXCONNECTIONS = "SendSMSMaxConnections";
   
    String SENDMAILHOST = "SendMailHost";
    String SENDMAILPORT = "SendMailPort";
    String SENDMAILMAXCONNECTIONS = "SendMailMaxConnections";
    String SENDMAILCONNECTIONTIMEOUT = "SendMailConnectionTimeout";
    String SENDMAILTIMEOUT = "SendMailTimeout";
    String SENDMAILDEBUG = "SendMailDebug";
    String SENDMAILAUTHENABLED = "SendMailAuthEnabled";
    String SENDMAILMECHANISMS = "SendMailMechanisms";
    String SENDMAILUSERSET = "SendMailUseRSet";    
    String SENDMAILSSLENABLED = "SendMailSSLEnabled";
    String SENDMAILSSLCHECKSERVERIDENTITY = "SendMailSSLCheckServerIdentity";
    String SENDMAILSASLENABLED = "SendMailSaslEnabled";
    String SENDMAILSASLMECHANISMS = "SendMailSaslMechanisms";
    String SENDMAILSASLREALM = "SendMailSaslRealm";
    String SENDMAILQUITWAIT = "SendMailQuitWait";

    String ENDPOINT_URL = "Endpoint-URL";
    String CLIENTAPP = "ClientApp";
    String DOGETUP = "doGetUP";
    String SERVICEPROVIDER = "ServiceProvider";
    String SERVICEPROVIDERTYPE = "ServiceProviderType";
    String STATUS = "Status";
    String SMSMOBARRING = "SMSMoBarring";
    String AUTHTEMPERRORLIST = "AuthTempErrorList";

    String CCOFFSET = "ccOffset";
    String NDCOFFSET = "ndcOffset";
    String HLROFFSET = "hlrOffset";
    String REGIONOFFSET = "regionOffset";

    String NAMESPACEURI = "NameSpaceUri";
    String SERVICENAME = "ServiceName";

    String APPLOGIN = "AppLogin";
    String APPPASSWORD = "AppPassword";
    String LOGGINGURL = "LoggingURL";
    String LOGGINGTIMEOUT = "LoggingTimeout";

    String NOTAPPLICABLE = "NA";

    int SMTP_SUCCESS = 250;

    String INTERNATION_PLUS_PREFIX = "+";
    String INTERNATION_NUMBER_PREFIX = "00";

    String SMTP_TRANSPORT = "smtp";
    String MAIL_SMTP_HOST = "mail.smtp.host";
    String MAIL_SMTP_PORT = "mail.smtp.port";
    String MAIL_SMTP_CONNECTION_TIMEOUT = "mail.smtp.connectiontimeout";
    String MAIL_SMTP_TIMEOUT = "mail.smtp.timeout";
    String MAIL_SMTP_DEBUG = "mail.smtp.debug";
    String MAIL_SMTP_AUTH = "mail.smtp.auth";
    String MAIL_SMTP_AUTH_MECHANISMS = "mail.smtp.auth.mechanisms";
    String MAIL_SMTP_USE_RSET = "mail.smtp.userset";
    String MAIL_SMTP_SSL_ENABLED = "mail.smtp.ssl.enable";
    String MAIL_SMTP_SSL_CHECK_SERVER_IDENTITY = "mail.smtp.ssl.checkserveridentity";
    String MAIL_SMTP_SASL_ENABLED = "mail.smtp.sasl.enable";
    String MAIL_SMTP_SASL_MECHANISMS = "mail.smtp.sasl.mechanisms";
    String MAIL_SMTP_SASL_REALM = "mail.smtp.sasl.realm";
    String MAIL_SMTP_QUITWAIT = "mail.smtp.quitwait";

    String MMG_SMTP_POOL = "mmg";
    String MTA_SMTP_POOL = "mta";

    String HEADER_FROM = "from: ";
    String HEADER_TO = "to: ";
    String HEADER_SUBJECT = "subject: ";
    String PLACEHOLDER_OLD_MSISDN = "%OLD_MSISDN%";
    String PLACEHOLDER_NEW_MSISDN = "%NEW_MSISDN%";

    /**
     * 
     * Different MessageLocale.
     *
     */
    enum MessageLocale {
        en_US,
        en_UK,
        de_DE,
        fr_FR,
        nl_NL,
        UNK;

        /**
         * Get Message Locale by value.
         * 
         * @param value String
         * @return MessageLocale
         */
        public static MessageLocale getByValue(String value) {
            MessageLocale returnVal = UNK;
            for (MessageLocale type : values()) {
                if (type.name().equalsIgnoreCase(value)) {
                    returnVal = type;
                    break;
                }
            }
            return returnVal;
        }
    }

    /**
     * 
     * Different MsisdnMessageId.
     *
     */
    enum MsisdnMessageId {
        MBX_MSG_SALUTATION,
        MBX_MSG_SIGNATURE,
        MBX_MSG_SUBJECT,
        MBX_MSG_PWD_RECOVERY_DEACTIVATE_MSISDN,
        MBX_MSG_SMS_FEATURE_DEACTIVATE_MSISDN,
        MBX_MSG_PROXIMUS_REACTIVATE_MSISDN,
        MBX_MSG_PWD_RECOVERY_REACTIVATE_MSISDN,
        MBX_MSG_SMS_FEATURE_REACTIVATE_MSISDN,
        MBX_MSG_PROXIMUS_SWAP_MSISDN,
        MBX_MSG_PWD_RECOVERY_SWAP_MSISDN,
        MBX_MSG_SMS_FEATURE_SWAP_MSISDN
    }
    
    String SMTP_CONTENT_TYPE_ISO_8859 = "iso-8859-1";
}
