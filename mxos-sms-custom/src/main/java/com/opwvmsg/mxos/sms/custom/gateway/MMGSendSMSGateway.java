/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.gateway;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.SmsGatewayException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.sms.custom.config.SendSmsConfigBean;
import com.opwvmsg.mxos.sms.custom.smtp.SmtpConnection;
import com.opwvmsg.mxos.sms.custom.smtp.SmtpConnectionPool;
import com.opwvmsg.mxos.sms.custom.smtp.SmtpConnectionPoolFactory;
import com.opwvmsg.mxos.sms.custom.utils.CustomActionUtils;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;
import com.sun.mail.smtp.SMTPAddressFailedException;
import com.sun.mail.smtp.SMTPSendFailedException;

/**
 * Send SMS Gateway implementation that sends mail using. External MMG
 * Interface.
 * 
 * @author mxos-dev
 */
public class MMGSendSMSGateway implements ISendSMSGateway {
    private static Logger logger = Logger.getLogger(MMGSendSMSGateway.class);
    private SendSmsConfigBean config;

    /**
     * 
     * @param configBean SendSmsConfigBean
     */
    public MMGSendSMSGateway(final SendSmsConfigBean configBean) {
        this.config = configBean;
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("MMGSendSMSGateway process() start."));
            logger.debug("configBean : " + config.toString());
        }

        final String msisdn = inputParams.get(SmsProperty.toAddress.name())
                .get(0);

        int rc = SmsConstants.SMTP_SUCCESS;

        SmtpConnectionPool smtpPool = null;
        SmtpConnection smtpConn = null;
        try {
            smtpPool = SmtpConnectionPoolFactory.getInstance().getSMTPPool(
                    SmsConstants.MMG_SMTP_POOL, config.getHost(),
                    config.getPort(), config.getMaxConnections(),
                    config.getProperties());
            smtpConn = smtpPool.borrowObject();
            smtpConn.connect();
            ConnectionStats.ACTIVE_MMG.increment();
        } catch (Exception e) {
            logger.error("Error received for MSISDN : " + msisdn
                    + " during MMG SMTP Connection Pool Creation.", e);
            ConnectionErrorStats.MMG.increment();
            throw new SmsGatewayException(
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getMessage()
                            .trim());
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully borrowed a SMTP connection"
                    + " from Connection Pool. "
                    + "Sending the message for msisdn : " + msisdn);
        }
        try {
            final String fromAddress = inputParams.get(
                    SmsProperty.fromAddress.name()).get(0);

            smtpConn.sendSMTPMessage(fromAddress.replace(
                    SmsConstants.INTERNATION_PLUS_PREFIX,
                    SmsConstants.INTERNATION_NUMBER_PREFIX), msisdn.replace(
                    SmsConstants.INTERNATION_PLUS_PREFIX,
                    SmsConstants.INTERNATION_NUMBER_PREFIX),
                    inputParams.get(SmsProperty.message.name()).get(0));

        } catch (UnsupportedEncodingException e) {
            logger.error("MIME Message Encoding Error received for MSISDN : "
                    + msisdn, e);
            throw new ApplicationException(
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getMessage()
                            .trim());
        } catch (AddressException e) {
            logger.error("MMG Error(AddressException) received for MSISDN : "
                    + msisdn, e);
            throw new SmsGatewayException(
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getMessage()
                            .trim());
        } catch (SendFailedException e) {
            rc = -1;
            if (e instanceof SMTPAddressFailedException) {
                rc = ((SMTPAddressFailedException) e).getReturnCode();
            } else if (e instanceof SMTPSendFailedException) {
                rc = ((SMTPSendFailedException) e).getReturnCode();
            }
            if (CustomActionUtils.isErrorCodeInList(config.getTempErrorList(),
                    String.valueOf(rc))) {
                logger.error("MMG Temporary Error received for MSISDN : "
                        + msisdn, e);
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_TEMPORARY_ERROR.name(), e
                                .getMessage().trim());
            } else if (CustomActionUtils.isErrorCodeInList(
                    config.getSnmpSendList(), String.valueOf(rc))) {
                logger.error("MMG Permanent Error received for MSISDN : "
                        + msisdn, e);
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_PERM_SNMP_ERROR.name(), e
                                .getMessage().trim());
            } else {
                logger.error("MMG Permanent Error received for MSISDN : "
                        + msisdn, e);

                throw new SmsGatewayException(
                        CustomError.SEND_SMS_PERMANENT_ERROR.name(), e
                                .getMessage().trim());
            }
        } catch (MessagingException e) {
            rc = -1;
            if (e instanceof SendFailedException) {
                if (e.getNextException() instanceof SMTPAddressFailedException) {
                    rc = ((SMTPAddressFailedException) e).getReturnCode();
                } else if (e.getNextException() instanceof SMTPSendFailedException) {
                    rc = ((SMTPSendFailedException) e).getReturnCode();
                }
            } else if (e instanceof SMTPAddressFailedException) {
                rc = ((SMTPAddressFailedException) e).getReturnCode();
            } else if (e instanceof SMTPSendFailedException) {
                rc = ((SMTPSendFailedException) e).getReturnCode();
            }
            if (CustomActionUtils.isErrorCodeInList(config.getTempErrorList(),
                    String.valueOf(rc))) {
                logger.error("MMG Temporary Error received for MSISDN : "
                        + msisdn, e);
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_TEMPORARY_ERROR.name(), e
                                .getMessage().trim());
            } else if (CustomActionUtils.isErrorCodeInList(
                    config.getSnmpSendList(), String.valueOf(rc))) {
                logger.error("MMG Permanent Error received for MSISDN : "
                        + msisdn, e);
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_PERM_SNMP_ERROR.name(), e
                                .getMessage().trim());
            } else {
                logger.error("MMG Permanent Error received for MSISDN : "
                        + msisdn, e);
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_PERMANENT_ERROR.name(), e
                                .getMessage().trim());
            }
        } catch (Exception e) {
            logger.error("Error while sending the message to MMG for MSISDN : "
                    + msisdn, e);
            throw new ApplicationException(
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getMessage()
                            .trim());
        } finally {
            if (smtpPool != null && smtpConn != null) {
                try {
                    smtpPool.returnObject(smtpConn);
                    ConnectionStats.ACTIVE_MMG.decrement();
                } catch (final Exception e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }

        logger.info("SMS sent(via MMG) successfully to MSISDN " + msisdn);

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("MMGSendSMSGateway process end."));
        }

    }
}
