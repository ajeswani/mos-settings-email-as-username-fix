/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Belgacom-mxos/bgc-mxos-lib/src/main/java/com/openwave/mxos/data/LoggingResponseBean.java#2 $
 */

package com.opwvmsg.mxos.sms.custom;

/**
 * @author mxos-dev
 *
 */
public class LoggingResponseBean {
    private String code;
    private String message;
    private StringBuffer fullMessage;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the fullMessage
     */
    public StringBuffer getFullMessage() {
        return fullMessage;
    }

    /**
     * @param fullMessage the fullMessage to set
     */
    public void setFullMessage(StringBuffer fullMessage) {
        this.fullMessage = fullMessage;
    }

    /**
     * Method to return the bean data as string.
     * @return String bean data.
     */
    public String toString() {
        return "LoggingResponseBean : returnCode=" + code + ", message="
                + message;
    }

}
