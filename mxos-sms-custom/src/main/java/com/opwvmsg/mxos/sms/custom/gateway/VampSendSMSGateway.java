/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.gateway;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.SmsGatewayException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.sms.custom.config.SendSmsConfigBean;
import com.opwvmsg.mxos.sms.custom.response.VampSendSMSResponseBean;
import com.opwvmsg.mxos.sms.custom.utils.CustomActionUtils;
import com.opwvmsg.mxos.sms.custom.utils.HTTPHelper;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * Send SMS Gateway implementation class that sends SMS using External VAMP.
 * Interface.
 * 
 * @author mxos-dev
 */
public class VampSendSMSGateway implements ISendSMSGateway {

    private static Logger logger = Logger.getLogger(VampSendSMSGateway.class);
    private SendSmsConfigBean configBean;

    /**
     * Constructor
     * 
     * @param configBean SendSmsConfigBean
     */
    public VampSendSMSGateway(final SendSmsConfigBean configBean) {
        this.configBean = configBean;
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("VampSendSMSGateway process start."));
            logger.debug("configBean : " + configBean.toString());
        }

        final String msisdn = inputParams.get(SmsProperty.toAddress.name())
                .get(0);

        final PostMethod method = CustomActionUtils.createVAMPPostMessage(
                configBean, inputParams);

        if (logger.isDebugEnabled()) {
            logger.debug("VAMP HTTP Request PostMethod created Successfully");
        }

        StringBuffer sb1 = new StringBuffer();
        for (int i = 0; i < method.getParameters().length; i++) {
            sb1.append(method.getParameters()[i].getName() + ":"
                    + method.getParameters()[i].getValue() + "  ");
        }

        logger.info("VAMP HTTP Request Parameters : " + sb1);

        if (logger.isDebugEnabled()) {
            logger.debug("Sending HTTP request to VAMP Interface");
        }

        StringBuffer response;
        try {
            response = HTTPHelper.sendHTTPRequest(method,
                    configBean.getTimeout());
        } catch (HttpException e) {
            logger.error("HTTP Exception while sending the message to VAMP."
                    + " Sending SEND_SMS_TEMPORARY_ERROR to FEP.", e);
            throw new ApplicationException(
                    CustomError.SEND_SMS_TEMPORARY_ERROR.name(),
                    "HTTP Exception while sending the message");
        } catch (IOException e) {
            ConnectionErrorStats.VAMP.increment();
            if (configBean.isTimeoutSnmpSend()) {
                logger.error("IOException while sending the message to VAMP."
                        + " Sending SEND_SMS_PERM_SNMP_ERROR to FEP.", e);
                throw new ApplicationException(
                        CustomError.SEND_SMS_PERM_SNMP_ERROR.name(),
                        "NA: IOException");
            } else {
                logger.error("IOException while sending the message to VAMP."
                        + " Sending SEND_SMS_PERMANENT_ERROR to FEP.", e);
                throw new ApplicationException(
                        CustomError.SEND_SMS_PERMANENT_ERROR.name(),
                        "NA: IOException");
            }
        } catch (Exception e) {
            logger.error("Exception while sending the message to VAMP."
                    + " Sending SEND_SMS_PERMANENT_ERROR to FEP.", e);
            throw new ApplicationException(
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(),
                    "Exception while sending the message");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Response received from VAMP:" + response);
        }

        final VampSendSMSResponseBean responseBean = CustomActionUtils
                .parseVAMPResponse(response);

        logger.info("Response Bean received from VAMP:"
                + responseBean.toString());

        if (!responseBean.getCode().equalsIgnoreCase(
                SmsConstants.VAMP_RESPONSE_SUCCESS_CODE)) {
            if (CustomActionUtils.isErrorCodeInList(
                    configBean.getTempErrorList(), responseBean.getCode())) {
                logger.error("VAMP Temporary Error received for MSISDN : "
                        + msisdn + " . Status Code: " + responseBean.getCode());
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_TEMPORARY_ERROR.name(),
                        responseBean.getErrorInfo());
            } else if (CustomActionUtils.isErrorCodeInList(
                    configBean.getSnmpSendList(), responseBean.getCode())) {
                logger.error("VAMP Permanent SNMP Error received for MSISDN : "
                        + msisdn + " . Status Code: " + responseBean.getCode());
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_PERM_SNMP_ERROR.name(),
                        responseBean.getErrorInfo());
            } else {
                logger.error("VAMP Permanent Error received for MSISDN : "
                        + msisdn + " . Status Code: " + responseBean.getCode());
                throw new SmsGatewayException(
                        CustomError.SEND_SMS_PERMANENT_ERROR.name(),
                        responseBean.getErrorInfo());
            }
        } else {
            logger.info("SMS sent(via VAMP) successfully to MSISDN " + msisdn);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("VampSendSMSGateway process end."));
        }

    }
}
