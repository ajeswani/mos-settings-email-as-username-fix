/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.sms.custom.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

import javax.xml.stream.Location;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.gateway.IAuthorizationGateway;
import com.opwvmsg.mxos.sms.custom.gateway.ILogToExternalEntityGateway;
import com.opwvmsg.mxos.sms.custom.gateway.ISendMailGateway;
import com.opwvmsg.mxos.sms.custom.gateway.ISendSMSGateway;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * The SMSGatewaysConfigParsar class used to load the SMS Gateway Config from.
 * configuration File.
 * 
 * @author mxos-dev
 */
public class SmsGatewaysConfigParsar {

    private static Logger logger = Logger
            .getLogger(SmsGatewaysConfigParsar.class);

    /**
     * Default constructor.
     */
    protected SmsGatewaysConfigParsar() {
    }

    /**
     * Parse XML by using local file.
     * 
     * @param fileName String
     * @param smsGateways Map<String, Map<String, Object>>
     * @return boolean
     * @throws MxOSException if any
     */
    public static boolean parse(final String fileName,
            Map<String, Map<String, Object>> smsGateways) throws MxOSException {
        final XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = null;
        try {
            xmlr = xmlif.createXMLStreamReader(fileName, new FileInputStream(
                    fileName));
        } catch (FileNotFoundException e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Failed to parse gateway-config.xml file:" 
                    + e.getMessage());
        } catch (XMLStreamException e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Failed to parse gateway-config.xml file:" 
                    + e.getMessage());
        }
        return parse(xmlr, smsGateways);
    }

    /**
     * Parse XML by using InputStream.
     * 
     * @param is InputStream
     * @param smsGateways Map<String, Map<String, Object>>
     * @return boolean
     * @throws MxOSException if any
     */
    public static boolean parse(InputStream is,
            Map<String, Map<String, Object>> smsGateways) 
    throws MxOSException {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = null;
        try {
            xmlr = xmlif.createXMLStreamReader(is);
        } catch (XMLStreamException e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Failed to parse gateway-config.xml file:" 
                    + e.getMessage());
        }
        return parse(xmlr, smsGateways);
    }

    /**
     * Parse the XML reader and create the Config Bean Objects.
     * 
     * @param xmlr
     * @param smsGateways
     * @return boolean
     * @throws MxOSException if any
     */
    private static boolean parse(XMLStreamReader xmlr,
            Map<String, Map<String, Object>> smsGateways) 
    throws MxOSException {

        boolean ret = false;
        int eventType = 0;
        SendSmsConfigBean curSmsBean = null;
        AuthorizationConfigBean curAuthBean = null;
        LogToExternalEntityConfigBean curlogToEnternalEntityBean = null;
        SendMailConfigBean curSendMailConfigBean = null;

        LOOP: try {
            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                case XMLEvent.END_DOCUMENT:
                    break LOOP;
                case XMLEvent.END_ELEMENT:
                    String endElName = xmlr.getLocalName();
                    if (SmsConstants.SENDSMSGATEWAY.equals(endElName)) {
                        ISendSMSGateway smsGateway = (ISendSMSGateway) 
                        getInstance(curSmsBean);
                        curSmsBean.setProperties();
                        smsGateways.get(SmsConstants.SENDSMSGATEWAYS).put(
                                curSmsBean.getType(), smsGateway);
                    } else if (SmsConstants.AUTHGATEWAY.equals(endElName)) {
                        IAuthorizationGateway authGateway = 
                            (IAuthorizationGateway) getInstance(curAuthBean);
                        smsGateways.get(SmsConstants.AUTHGATEWAYS).put(
                                curAuthBean.getType(), authGateway);
                    } else if (SmsConstants.LOGTOEXTERNALENTITYGATEWAY
                            .equals(endElName)) {
                        ILogToExternalEntityGateway loggingGateway = 
                            (ILogToExternalEntityGateway) 
                            getInstance(curlogToEnternalEntityBean);
                        smsGateways.get(
                                SmsConstants.LOGTOEXTERNALENTITYGATEWAYS).put(
                                curlogToEnternalEntityBean.getType(),
                                loggingGateway);
                    } else if (SmsConstants.SENDMAILGATEWAY
                            .equals(endElName)) {
                        curSendMailConfigBean.setProperties();
                        ISendMailGateway sendMailGateway = 
                            (ISendMailGateway) 
                            getInstance(curSendMailConfigBean);
                        smsGateways.get(SmsConstants.SENDMAILGATEWAYS).put(
                                curSendMailConfigBean.getType(),
                                sendMailGateway);
                    }
                    break;
                case XMLEvent.START_ELEMENT:
                    String startElName = xmlr.getLocalName();
                    if (SmsConstants.SENDSMSGATEWAYS.equals(startElName)) {
                        smsGateways.put(SmsConstants.SENDSMSGATEWAYS,
                                new HashMap<String, Object>());
                    } else if (SmsConstants.AUTHGATEWAYS.equals(startElName)) {
                        smsGateways.put(SmsConstants.AUTHGATEWAYS,
                                new HashMap<String, Object>());
                    } else if (SmsConstants.LOGTOEXTERNALENTITYGATEWAYS
                            .equals(startElName)) {
                        smsGateways.put(
                                SmsConstants.LOGTOEXTERNALENTITYGATEWAYS,
                                new HashMap<String, Object>());
                    } else if (SmsConstants.DEPOSITMAILGATEWAYS
                            .equals(startElName)) {
                        smsGateways.put(SmsConstants.DEPOSITMAILGATEWAYS,
                                new HashMap<String, Object>());
                    } else if (SmsConstants.SENDMAILGATEWAYS
                            .equals(startElName)) {
                        smsGateways.put(SmsConstants.SENDMAILGATEWAYS,
                                new HashMap<String, Object>());
                    } else if (SmsConstants.SENDSMSGATEWAY
                            .equals(startElName)) {
                        String type = xmlr.getAttributeValue(null,
                                SmsConstants.TYPE);
                        String provider = xmlr.getAttributeValue(null,
                                SmsConstants.PROVIDER);
                        curSmsBean = new SendSmsConfigBean();
                        curSmsBean.setType(type);
                        curSmsBean.setProvider(provider);
                    } else if (SmsConstants.AUTHGATEWAY.equals(startElName)) {
                        String type = xmlr.getAttributeValue(null,
                                SmsConstants.TYPE);
                        String provider = xmlr.getAttributeValue(null,
                                SmsConstants.PROVIDER);
                        curAuthBean = new AuthorizationConfigBean();
                        curAuthBean.setType(type);
                        curAuthBean.setProvider(provider);
                    } else if (SmsConstants.LOGTOEXTERNALENTITYGATEWAY
                            .equals(startElName)) {
                        String type = xmlr.getAttributeValue(null,
                                SmsConstants.TYPE);
                        String provider = xmlr.getAttributeValue(null,
                                SmsConstants.PROVIDER);
                        curlogToEnternalEntityBean = 
                            new LogToExternalEntityConfigBean();
                        curlogToEnternalEntityBean.setType(type);
                        curlogToEnternalEntityBean.setProvider(provider);
                    } else if (SmsConstants.SENDMAILGATEWAY
                            .equals(startElName)) {
                        String type = xmlr.getAttributeValue(null,
                                SmsConstants.TYPE);
                        String provider = xmlr.getAttributeValue(null,
                                SmsConstants.PROVIDER);
                        curSendMailConfigBean = new SendMailConfigBean();
                        curSendMailConfigBean.setType(type);
                        curSendMailConfigBean.setProvider(provider);
                    } else if (SmsConstants.URL.equals(startElName)) {
                        curSmsBean.setUrl(xmlr.getElementText());
                    } else if (SmsConstants.AUTH.equals(startElName)) {
                        curSmsBean.setAuth(xmlr.getElementText());
                    } else if (SmsConstants.SOURCE.equals(startElName)) {
                        curSmsBean.setSource(xmlr.getElementText());
                    } else if (SmsConstants.PRODUCT.equals(startElName)) {
                        curSmsBean.setProduct(xmlr.getElementText());
                    } else if (SmsConstants.SERVICECODE.equals(startElName)) {
                        curSmsBean.setServiceCode(xmlr.getElementText());
                    } else if (SmsConstants.CONTENTTYPE.equals(startElName)) {
                        curSmsBean.setContentType(xmlr.getElementText());
                    } else if (SmsConstants.SENDSMSTEMPERRORLIST
                            .equals(startElName)) {
                        curSmsBean.setTempErrorList(xmlr.getElementText());
                    } else if (SmsConstants.SENDSMSSNMPSENDLIST
                            .equals(startElName)) {
                        curSmsBean.setSnmpSendList(xmlr.getElementText());
                    } else if (SmsConstants.SENDSMSTIMEOUTSNMPSEND
                            .equals(startElName)) {
                        curSmsBean.setTimeoutSnmpSend(Boolean.parseBoolean(xmlr
                                .getElementText()));
                    } else if (SmsConstants.SENDSMSTIMEOUT
                            .equals(startElName)) {
                        curSmsBean.setTimeout(Integer.parseInt(xmlr
                                .getElementText()));
                    } else if (SmsConstants.SENDSMSHOST.equals(startElName)) {
                        curSmsBean.setHost(xmlr.getElementText());
                    } else if (SmsConstants.SENDSMSPORT.equals(startElName)) {
                        curSmsBean.setPort(xmlr.getElementText());
                    } else if (SmsConstants.SENDSMSMAXCONNECTIONS
                            .equals(startElName)) {
                        curSmsBean.setMaxConnections(Integer.parseInt(xmlr
                                .getElementText()));
                    } else if (SmsConstants.ENDPOINT_URL.equals(startElName)) {
                        curAuthBean.setEndPointUrl(xmlr.getElementText());
                    } else if (SmsConstants.CLIENTAPP.equals(startElName)) {
                        curAuthBean.setClientApp(xmlr.getElementText());
                    } else if (SmsConstants.DOGETUP.equals(startElName)) {
                        curAuthBean.setDogetUp(Boolean.parseBoolean(xmlr
                                .getElementText()));
                    } else if (SmsConstants.SERVICEPROVIDER
                            .equals(startElName)) {
                        curAuthBean.setServiceProvider(xmlr.getElementText());
                    } else if (SmsConstants.SERVICEPROVIDERTYPE
                            .equals(startElName)) {
                        curAuthBean.setServiceProviderType(xmlr
                                .getElementText());
                    } else if (SmsConstants.STATUS.equals(startElName)) {
                        curAuthBean.setStatus(xmlr.getElementText());
                    } else if (SmsConstants.SMSMOBARRING.equals(startElName)) {
                        curAuthBean.setSmsMoBarring(xmlr.getElementText());
                    } else if (SmsConstants.AUTHTEMPERRORLIST
                            .equals(startElName)) {
                        curAuthBean.setTempErrorList(xmlr.getElementText());
                    } else if (SmsConstants.CCOFFSET.equals(startElName)) {
                        curAuthBean.setCcOffset(xmlr.getElementText());
                    } else if (SmsConstants.NDCOFFSET.equals(startElName)) {
                        curAuthBean.setNdcOffset(xmlr.getElementText());
                    } else if (SmsConstants.HLROFFSET.equals(startElName)) {
                        curAuthBean.setHlrOffset(xmlr.getElementText());
                    } else if (SmsConstants.REGIONOFFSET.equals(startElName)) {
                        curAuthBean.setRegionOffset(xmlr.getElementText());
                    } else if (SmsConstants.NAMESPACEURI.equals(startElName)) {
                        curAuthBean.setNameSpaceUri(xmlr.getElementText());
                    } else if (SmsConstants.SERVICENAME.equals(startElName)) {
                        curAuthBean.setServiceName(xmlr.getElementText());
                    } else if (SmsConstants.LOGGINGURL.equals(startElName)) {
                        curlogToEnternalEntityBean
                                .setUrl(xmlr.getElementText());
                    } else if (SmsConstants.APPLOGIN.equals(startElName)) {
                        curlogToEnternalEntityBean.setAppLogin(xmlr
                                .getElementText());
                    } else if (SmsConstants.APPPASSWORD.equals(startElName)) {
                        curlogToEnternalEntityBean.setAppPassword(xmlr
                                .getElementText());
                    } else if (SmsConstants.LOGGINGTIMEOUT.equals(startElName)) {
                        curlogToEnternalEntityBean.setTimeout(Integer
                                .parseInt(xmlr.getElementText()));
                    } else if (SmsConstants.SENDMAILHOST.equals(startElName)) {
                        curSendMailConfigBean.setHost(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILPORT.equals(startElName)) {
                        curSendMailConfigBean.setPort(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILMAXCONNECTIONS
                            .equals(startElName)) {
                        curSendMailConfigBean.setMaxConnections(Integer
                                .parseInt(xmlr.getElementText()));
                    } else if (SmsConstants.SENDMAILCONNECTIONTIMEOUT
                            .equals(startElName)) {
                        curSendMailConfigBean.setConnectionTimeout(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILTIMEOUT.equals(startElName)) {
                        curSendMailConfigBean.setTimeout(xmlr
                                .getElementText());
                    } else if (SmsConstants.SENDMAILMECHANISMS
                            .equals(startElName)) {
                        curSendMailConfigBean.setMechanisms(xmlr
                                .getElementText());
                    } else if (SmsConstants.SENDMAILUSERSET.equals(startElName)) {
                        curSendMailConfigBean.setUseRSet(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILDEBUG.equals(startElName)) {
                        curSendMailConfigBean.setDebug(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILAUTHENABLED
                            .equals(startElName)) {
                        curSendMailConfigBean.setAuthEnabled(
                                xmlr.getElementText());
                    }else if (SmsConstants.SENDMAILSSLENABLED
                            .equals(startElName)) {
                        curSendMailConfigBean.setSslEnabled(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILSSLCHECKSERVERIDENTITY
                            .equals(startElName)) {
                        curSendMailConfigBean.setSslCheckServerIdentity(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILSASLENABLED
                            .equals(startElName)) {
                        curSendMailConfigBean.setSaslEnabled(xmlr.getElementText());
                    } else if (SmsConstants.SENDMAILSASLMECHANISMS
                            .equals(startElName)) {
                        curSendMailConfigBean.setSaslMechanisms(xmlr
                                .getElementText());
                    }else if (SmsConstants.SENDMAILSASLREALM
                            .equals(startElName)) {
                        curSendMailConfigBean.setSaslRealm(xmlr
                                .getElementText());
                    } else if (SmsConstants.SENDMAILQUITWAIT
                            .equals(startElName)) {
                        curSendMailConfigBean.setQuitWait(xmlr.getElementText());
                    } 
                    break;
                default:
                    break;
                }
                ret = true;
            }
        } catch (Exception e) {
            ret = false;
            Location loc = xmlr.getLocation();
            logger.error("Error in parsing gateway-config.xml file:"
                    + getLocInfo(loc), e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        return ret;
    }

    /**
     * Return XML location info.
     * 
     * @param loc
     * @return String
     */
    private static String getLocInfo(Location loc) {
        return loc.getSystemId() + ", line:" + loc.getLineNumber() + ", col:"
                + loc.getColumnNumber() + ", off:" + loc.getCharacterOffset();
    }

    /**
     * Create the instance of ISendSMSGateway using the reflection. TODO: Check
     * if the below two functions can be generalised
     * 
     * @param bean
     * @return ISendSMSGateway
     * @throws Exception if any
     */
    private static ISendSMSGateway getInstance(SendSmsConfigBean bean)
            throws Exception {
        if (bean != null && bean.getProvider() != null
                && !bean.getProvider().equals("")) {
            Class<?> c = Class.forName(bean.getProvider());
            if (c != null) {
                Class[] paramTypes = {SendSmsConfigBean.class };
                Constructor cons = c.getConstructor(paramTypes);
                Object[] args = {bean };
                return (ISendSMSGateway) cons.newInstance(args);
            }
        }
        throw new ApplicationException("Unable to create instance of : " 
                + bean);
    }

    /**
     * Create the instance of IAuthorizationGateway using the reflection.
     * 
     * @param bean
     * @return IAuthorizationGateway
     * @throws Exception if any
     */
    private static IAuthorizationGateway getInstance(
            AuthorizationConfigBean bean) throws Exception {
        if (bean != null && bean.getProvider() != null
                && !bean.getProvider().equals("")) {
            Class<?> c = Class.forName(bean.getProvider());
            if (c != null) {
                Class[] paramTypes = {AuthorizationConfigBean.class };
                Constructor cons = c.getConstructor(paramTypes);
                Object[] args = {bean };
                return (IAuthorizationGateway) cons.newInstance(args);
            }
        }
        throw new ApplicationException("Unable to create instance of : " 
                + bean);
    }

    /**
     * Create the instance of IAuthorizationGateway using the reflection.
     * 
     * @param bean
     * @return ILogToExternalEntityGateway
     * @throws Exception if any
     */
    private static ILogToExternalEntityGateway getInstance(
            LogToExternalEntityConfigBean bean) throws Exception {
        if (bean != null && bean.getProvider() != null
                && !bean.getProvider().equals("")) {
            Class<?> c = Class.forName(bean.getProvider());
            if (c != null) {
                Class[] paramTypes = {
                        LogToExternalEntityConfigBean.class };
                Constructor cons = c.getConstructor(paramTypes);
                Object[] args = {
                        bean };
                return (ILogToExternalEntityGateway) cons.newInstance(args);
            }
        }
        throw new ApplicationException("Unable to create instance of : " 
                + bean);
    }

    /**
     * Create the instance of ISendSMSGateway using the reflection. TODO: Check
     * if the below two functions can be generalised
     * 
     * @param bean
     * @return ISendMailGateway
     * @throws Exception if any
     */
    private static ISendMailGateway getInstance(SendMailConfigBean bean)
            throws Exception {
        if (bean != null && bean.getProvider() != null
                && !bean.getProvider().equals("")) {
            Class<?> c = Class.forName(bean.getProvider());
            if (c != null) {
                Class[] paramTypes = {
                        SendMailConfigBean.class };
                Constructor cons = c.getConstructor(paramTypes);
                Object[] args = {
                        bean };
                return (ISendMailGateway) cons.newInstance(args);
            }
        }
        throw new ApplicationException("Unable to create instance of : " 
                + bean);
    }
}
