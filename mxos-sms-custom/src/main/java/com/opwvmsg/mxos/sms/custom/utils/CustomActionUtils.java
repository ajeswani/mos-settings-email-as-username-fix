/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.utils;

import static com.opwvmsg.mxos.sms.custom.utils.SmsConstants.MessageLocale;
import static com.opwvmsg.mxos.sms.custom.utils.SmsConstants.MsisdnMessageId;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.Consts;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import be.belgacom.mobile.sdup.data.xsd.MSISDN;
import be.belgacom.mobile.sdup.service.reader.SdupReaderService;
import be.belgacom.mobile.sdup.service.reader.SdupReaderServicePortType;
import be.belgacom.mobile.sdup.service.reader.xsd.SdupReaderServiceResponse;
import be.belgacom.mobile.sdup.service.xsd.KeyValuePair;
import be.belgacom.mobile.sdup.service.xsd.SubscriberDataWrapper;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.message.MessageServiceHelper;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.mss.MssMetaCRUD.SystemFolder;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.SmsGatewayException;
import com.opwvmsg.mxos.sms.custom.config.AuthorizationConfigBean;
import com.opwvmsg.mxos.sms.custom.config.LogToExternalEntityConfigBean;
import com.opwvmsg.mxos.sms.custom.config.SendSmsConfigBean;
import com.opwvmsg.mxos.sms.custom.gateway.MsisdnEmailMessageFactory;
import com.opwvmsg.mxos.sms.custom.response.FBISendSMSResponseBean;
import com.opwvmsg.mxos.sms.custom.response.MaaResponseBean;
import com.opwvmsg.mxos.sms.custom.response.SdupAuthorizationResponseBean;
import com.opwvmsg.mxos.sms.custom.response.VampSendSMSResponseBean;

/**
 * Utility class for SMS API's.
 * 
 * @author mxos-dev
 */
public class CustomActionUtils {

    private static Logger logger = Logger.getLogger(CustomActionUtils.class);

    /**
     * Method to create VAMP Post message.
     * 
     * @param configBean SendSmsConfigBean
     * @param inputParams Map<String, List<String>>
     * @return PostMethod
     */
    public static PostMethod createVAMPPostMessage(
            final SendSmsConfigBean configBean,
            final Map<String, List<String>> inputParams) {

        PostMethod method = new PostMethod(configBean.getUrl());

        method.addParameter(SmsConstants.VAMP_PARAM_SOURCE,
                configBean.getSource());

        method.addParameter(SmsConstants.VAMP_PARAM_AUTH, configBean
                .getAuth());

        method.addParameter(SmsConstants.VAMP_PARAM_PRODUCT,
                configBean.getProduct());

        method.addParameter(SmsConstants.VAMP_PARAM_SERVICE_CODE,
                configBean.getServiceCode());

        method.addParameter(SmsConstants.VAMP_PARAM_TYPE,
                configBean.getContentType());

        method.addParameter(SmsConstants.VAMP_PARAM_ID,
                String.valueOf((new Date()).getTime()));

        method.addParameter(SmsConstants.VAMP_PARAM_RECIPIENT, (inputParams
                .get(SmsProperty.toAddress.name()).get(0)).replace(
                SmsConstants.INTERNATION_PLUS_PREFIX,
                SmsConstants.INTERNATION_NUMBER_PREFIX));

        method.addParameter(SmsConstants.VAMP_PARAM_MESSAGE,
                inputParams.get(SmsProperty.message.name()).get(0));

        method.addParameter(SmsConstants.VAMP_PARAM_SENDER, (inputParams
                .get(SmsProperty.fromAddress.name()).get(0)).replace(
                SmsConstants.INTERNATION_PLUS_PREFIX,
                SmsConstants.INTERNATION_NUMBER_PREFIX));

        if (inputParams.get(SmsProperty.pid.name()) != null) {
            method.addParameter(SmsConstants.VAMP_PID,
                    inputParams.get(SmsProperty.pid.name()).get(0));
        }

        if (inputParams.get(SmsProperty.dcs.name()) != null) {
            method.addParameter(SmsConstants.VAMP_DCS,
                    inputParams.get(SmsProperty.dcs.name()).get(0));
        }
        if (inputParams.get(SmsProperty.udh.name()) != null) {
            method.addParameter(SmsConstants.VAMP_UDH,
                    inputParams.get(SmsProperty.udh.name()).get(0));
        }
        if (inputParams.get(SmsProperty.deliveryReportRequest.name()) != 
            null) {
            method.addParameter(SmsConstants.VAMP_DELIVERY_REPORT_REQUEST,
                    inputParams.get(SmsProperty.deliveryReportRequest.name())
                            .get(0));
        }
        if (inputParams.get(SmsProperty.messageNL.name()) != null) {
            method.addParameter(SmsConstants.VAMP_MESSAGE_NL,
                    inputParams.get(SmsProperty.messageNL.name()).get(0));
        }
        if (inputParams.get(SmsProperty.messageFR.name()) != null) {
            method.addParameter(SmsConstants.VAMP_MESSAGE_FR,
                    inputParams.get(SmsProperty.messageFR.name()).get(0));
        }
        if (inputParams.get(SmsProperty.messageEN.name()) != null) {
            method.addParameter(SmsConstants.VAMP_MESSAGE_EN,
                    inputParams.get(SmsProperty.messageEN.name()).get(0));
        }
        if (inputParams.get(SmsProperty.messageDE.name()) != null) {
            method.addParameter(SmsConstants.VAMP_MESSAGE_DE,
                    inputParams.get(SmsProperty.messageDE.name()).get(0));
        }
        return method;
    }

    /**
     * Method to create http post message for MAA.
     * @param configBean LogToExternalEntityConfigBean
     * @param inputParams Map<String, List<String>>
     * @return
     */
    public static HttpPost createMaaHttpPost(
            final LogToExternalEntityConfigBean configBean,
            final Map<String, List<String>> inputParams) {

        HttpPost httpPost = new HttpPost(configBean.getUrl());

        List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_APP_LOGIN,
                configBean.getAppLogin()));
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_APP_PASSWORD,
                configBean.getAppPassword()));
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_APP_CALL,
                inputParams.get(SmsProperty.apicall.name()).get(0)));
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_APP_TIMESTAMP,
                inputParams.get(SmsProperty.timestamp.name()).get(0)));
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_KEY, inputParams
                .get(SmsProperty.key.name()).get(0)));
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_PAYLOAD,
                inputParams.get(SmsProperty.payload.name()).get(0)));
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_RESULTCODE,
                inputParams.get(SmsProperty.resultcode.name()).get(0)));
        nvps.add(new BasicNameValuePair(SmsConstants.MAA_PARAM_CRUD,
                inputParams.get(SmsProperty.crud.name()).get(0)));

        StringBuffer sb = new StringBuffer();
        for (BasicNameValuePair nvp: nvps) {
            sb.append(nvp.getName() + ":" + nvp.getValue() + "  ");
        }
        logger.info("MAA HTTP Request Parameters : " + sb);

        httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));

        return httpPost;
    }

    /**
     * Method to parse the VAMP response and create VampSendSMSResponseBean.
     * 
     * @param xmlContent StringBuffer
     * @return VampSendSMSResponseBean
     * @throws ApplicationException
     * @throws SmsGatewayException
     */
    public static VampSendSMSResponseBean parseVAMPResponse(
            final StringBuffer xmlContent) throws SmsGatewayException {

        final String content = xmlContent.toString();
        VampSendSMSResponseBean vampResponseBean = 
            new VampSendSMSResponseBean();
        vampResponseBean.setCode(SmsConstants.VAMP_RESPONSE_SUCCESS_CODE);

        try {
            InputStream is = 
                new ByteArrayInputStream(content.getBytes("UTF-8"));

            final XMLInputFactory xmlif = XMLInputFactory.newInstance();
            final XMLStreamReader xmlr = xmlif.createXMLStreamReader(is);
            int eventType = 0;

            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                case XMLEvent.END_DOCUMENT:
                    break;
                case XMLEvent.START_ELEMENT:
                    String name = xmlr.getLocalName();
                    if (name.equalsIgnoreCase(SmsConstants
                            .VAMP_RESPONSE_ERROR_TAG)) {

                        vampResponseBean.setCode(xmlr.getAttributeValue(null,
                                SmsConstants.VAMP_RESPONSE_CODE_TAG));

                        vampResponseBean.setMessage(xmlr.getElementText());
                    }
                    break;
                default:
                    break;
                }
            }
        } catch (Exception e) {
            logger.error("Failed to parse the VAMP Response:" + xmlContent, e);
            throw new SmsGatewayException(
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(),
                    "Failed to parse the VAMP Response");
        }

        return vampResponseBean;
    }

    /**
     * Method to parse the VAMP response and create VampSendSMSResponseBean.
     * 
     * @param xmlContent StringBuffer
     * @return MaaResponseBean
     * @throws ApplicationException
     */
    public static MaaResponseBean 
    parseMaaResponse(final StringBuffer xmlContent)
            throws ApplicationException {

        final String content = xmlContent.toString();
        MaaResponseBean maaResponseBean = new MaaResponseBean();
        maaResponseBean.setCode(SmsConstants.MAA_RESPONSE_SUCCESS_CODE);

        try {
            final InputStream is = new ByteArrayInputStream(
                    content.getBytes("UTF-8"));

            final XMLInputFactory xmlif = XMLInputFactory.newInstance();
            final XMLStreamReader xmlr = xmlif.createXMLStreamReader(is);
            int eventType = 0;

            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                case XMLEvent.END_DOCUMENT:
                    break;
                case XMLEvent.START_ELEMENT:
                    String name = xmlr.getLocalName();
                    if (SmsConstants.MAA_RESPONSE_RESULT_TAG.equals(name)) {
                        String attrCodeValue = xmlr.getAttributeValue(null,
                                SmsConstants.MAA_RESPONSE_CODE_TAG);
                        maaResponseBean.setCode(attrCodeValue);
                        String elementValue = xmlr.getElementText();
                        maaResponseBean.setMessage(elementValue);
                    }
                    break;
                default:
                    break;
                }
            }
        } catch (Exception e) {
            throw new ApplicationException(
                    CustomError.LOG_TO_EXTERNAL_ENTITY_ERROR.name(),
                    "Failed to parse the MAA Response");
        }

        return maaResponseBean;
    }

    /**
     * Method to create FBI Post message.
     * 
     * @param configBean SendSmsConfigBean
     * @param inputParams Map<String, List<String>>
     * @return PostMethod
     */
    public static PostMethod createFBIPostMessage(
            final SendSmsConfigBean configBean,
            final Map<String, List<String>> inputParams) {

        PostMethod method = new PostMethod(configBean.getUrl());

        method.addParameter(SmsConstants.FBI_PARAM_SOURCE,
                configBean.getSource());

        method.addParameter(SmsConstants.FBI_PARAM_AUTH, configBean.getAuth());

        method.addParameter(SmsConstants.FBI_PARAM_PRODUCT,
                configBean.getProduct());

        method.addParameter(SmsConstants.FBI_PARAM_SERVICE_CODE,
                configBean.getServiceCode());

        method.addParameter(SmsConstants.FBI_PARAM_TYPE,
                configBean.getContentType());

        method.addParameter(SmsConstants.FBI_PARAM_ID,
                String.valueOf((new Date()).getTime()));

        method.addParameter(SmsConstants.FBI_PARAM_RECIPIENT, (inputParams
                .get(SmsProperty.toAddress.name()).get(0)).replace(
                SmsConstants.INTERNATION_PLUS_PREFIX,
                SmsConstants.INTERNATION_NUMBER_PREFIX));

        method.addParameter(SmsConstants.FBI_PARAM_MESSAGE,
                inputParams.get(SmsProperty.message.name()).get(0));

        method.addParameter(SmsConstants.FBI_PARAM_SENDER, (inputParams
                .get(SmsProperty.fromAddress.name()).get(0)).replace(
                SmsConstants.INTERNATION_PLUS_PREFIX,
                SmsConstants.INTERNATION_NUMBER_PREFIX));

        return method;
    }

    /**
     * Method to parse the FBI response and create FBISendSMSResponseBean.
     * 
     * @param xmlContent StringBuffer
     * @return FBISendSMSResponseBean
     * @throws ApplicationException
     */
    public static FBISendSMSResponseBean parseFBIResponse(
            final StringBuffer xmlContent) throws ApplicationException {

        final String content = xmlContent.toString();
        FBISendSMSResponseBean fbiResponseBean = new FBISendSMSResponseBean();
        fbiResponseBean.setCode(SmsConstants.FBI_RESPONSE_SUCCESS_CODE);

        try {
            InputStream is = new ByteArrayInputStream(content
                    .getBytes("UTF-8"));

            final XMLInputFactory xmlif = XMLInputFactory.newInstance();
            final XMLStreamReader xmlr = xmlif.createXMLStreamReader(is);
            int eventType = 0;

            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                case XMLEvent.END_DOCUMENT:
                    break;
                case XMLEvent.START_ELEMENT:
                    String name = xmlr.getLocalName();
                    if (name.equalsIgnoreCase(SmsConstants
                            .FBI_RESPONSE_ERROR_TAG)) {

                        fbiResponseBean.setCode(xmlr.getAttributeValue(null,
                                SmsConstants.FBI_RESPONSE_CODE_TAG));

                        fbiResponseBean.setMessage(xmlr.getElementText());
                    }
                    break;
                default:
                    break;
                }
            }
        } catch (Exception e) {
            throw new ApplicationException(
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(),
                    "Failed to parse the FBI Response XML");
        }

        return fbiResponseBean;
    }

    /**
     * Method to check if the error is temporary or permanent error.
     * 
     * @param errorList String
     * @param errorCode String
     * @return boolean
     */
    public static boolean isErrorCodeInList(final String errorList,
            final String errorCode) {
        String[] errors = errorList.split(",");

        if (errorList != null) {
            for (int i = 0; i < errors.length; i++) {
                if (errorCode.equals(errors[i])){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Method to create the MSISDN Object for authorizeMSISDN Interface.
     * 
     * @param strMsisdn String
     * @param configBean AuthorizationConfigBean
     * @return MSISDN
     * @throws InvalidRequestException
     */
    public static MSISDN createMSISDN(final String strMsisdn,
            AuthorizationConfigBean configBean) throws 
            InvalidRequestException {

        MSISDN msisdn = new MSISDN();
        msisdn.setCc(Integer.parseInt(strMsisdn.substring(
                configBean.getCcOffset()[0] + 1,
                configBean.getCcOffset()[1] + 1)));
        msisdn.setNdc(Integer.parseInt(strMsisdn.substring(
                configBean.getNdcOffset()[0] + 1,
                configBean.getNdcOffset()[1] + 1)));
        msisdn.setHlr(Integer.parseInt(strMsisdn.substring(
                configBean.getHlrOffset()[0] + 1,
                configBean.getHlrOffset()[1] + 1)));
        msisdn.setRegion(Integer.parseInt(strMsisdn.substring(
                configBean.getRegionOffset()[0] + 1,
                configBean.getRegionOffset()[1] + 1)));
        msisdn.setSn(Integer.parseInt(strMsisdn.substring(
                (configBean.getRegionOffset()[1] + 1), strMsisdn.length())));

        return msisdn;
    }

    /**
     * Method to create the SdupReaderServicePortType Object for 
     * authorizeMSISDN Interface.
     * 
     * @param configBean AuthorizationConfigBean
     * @return SdupReaderServicePortType
     * @throws ApplicationException
     */
    public static SdupReaderServicePortType createSDUPReaderService(
            final AuthorizationConfigBean configBean)
            throws ApplicationException {

        URL baseUrl = be.belgacom.mobile.sdup.service.reader.
        SdupReaderService.class
                .getResource(".");

        URL url = null;
        SdupReaderServicePortType readerServicePort = null;

        try {
            url = new URL(baseUrl, configBean.getEndPointUrl());

            QName qn = new QName(configBean.getNameSpaceUri(),
                    configBean.getServiceName());

            SdupReaderService readerService = new SdupReaderService(url, qn);

            readerServicePort = readerService
                    .getSdupReaderServiceSOAP11PortHttp();

        } catch (Exception e) {
            throw new ApplicationException(
                    CustomError.MSISDN_AUTHORIZE_PERMANENT_ERROR.name(),
                    "Failed to construct the PDU for authorizeMSISDN:"
                            + e.getMessage());
        }
        return readerServicePort;
    }

    /**
     * Method to send the SDUP request to the Server.
     * 
     * @param configBean AuthorizationConfigBean
     * @param strMsisdn String
     * @return SdupReaderServiceResponse
     * @throws MxOSException
     */
    public static SdupReaderServiceResponse sendSDUPRequest(
            final AuthorizationConfigBean configBean, final String strMsisdn)
            throws MxOSException {

        final MSISDN msisdn = createMSISDN(strMsisdn, configBean);

        final SdupReaderServicePortType readerServicePort = 
            createSDUPReaderService(configBean);

        final SdupReaderServiceResponse sdupResponse = readerServicePort
                .findSubscriberByMsisdn(configBean.getClientApp(), msisdn,
                        configBean.getDogetUp());

        return sdupResponse;
    }

    /**
     * Method to parse the SDUP response.
     * 
     * @param sdupResp sdupResp
     * @return SdupAuthorizationResponseBean
     * @throws SmsGatewayException
     */
    public static SdupAuthorizationResponseBean parseSDUPResponse(
            final SdupReaderServiceResponse sdupResp)
            throws SmsGatewayException {

        SdupAuthorizationResponseBean sdupResponseBean = 
            new SdupAuthorizationResponseBean();

        if (sdupResp.getStatusCode() == null) {
            throw new SmsGatewayException(
                    CustomError.MSISDN_AUTHORIZE_TEMPORARY_ERROR.name(),
                    "Status Code not present in the SDUP Response");
        }

        sdupResponseBean.setStatusCode(sdupResp.getStatusCode());
        sdupResponseBean.setStatusMessage(sdupResp.getStatusMessage()
                .getValue());

        if (sdupResponseBean.getStatusCode() != 
            SmsConstants.SDUP_RESPONSE_SUCCESS_CODE) {
            return sdupResponseBean;
        }

        Iterator<SubscriberDataWrapper> itr = sdupResp
                .getSubscriberDataWrapper().iterator();

        while (itr.hasNext()) {
            SubscriberDataWrapper subscriberDataIter = itr.next();

            List<KeyValuePair> keyValueList = subscriberDataIter
                    .getKeyValuePairs();
            Iterator<KeyValuePair> keyValueIter = keyValueList.iterator();

            while (keyValueIter.hasNext()) {
                KeyValuePair keyValue = keyValueIter.next();
                JAXBElement<String> key = keyValue.getKey();

                if ((key.getValue()
                        .equalsIgnoreCase(SmsConstants
                                .SDUP_SERVICE_PROVIDER_TYPE))
                        && (keyValue.getValues() != null)) {
                    sdupResponseBean.setServiceProviderType(keyValue
                            .getValues().get(0));
                }

                if ((key.getValue()
                        .equalsIgnoreCase(SmsConstants.SDUP_SERVICE_PROVIDER))
                        && (keyValue.getValues() != null)) {
                    sdupResponseBean.setServiceProvider(keyValue.getValues()
                            .get(0));
                }

                if ((key.getValue()
                        .equalsIgnoreCase(SmsConstants.SDUP_SMS_MO_BARRING))
                        && (keyValue.getValues() != null)) {
                    sdupResponseBean.setSMSMoBarring(keyValue.getValues()
                            .get(0));
                }

                if ((key.getValue().equalsIgnoreCase(SmsConstants.SDUP_STATUS))
                        && (keyValue.getValues() != null)) {
                    sdupResponseBean.setStatus(keyValue.getValues().get(0));
                }
            }

            if ((sdupResponseBean.getServiceProvider() != null)
                    && (sdupResponseBean.getServiceProviderType() != null)
                    && (sdupResponseBean.getSMSMoBarring() != null)
                    && (sdupResponseBean.getStatus() != null)) {
                break;
            }
        }

        return sdupResponseBean;
    }

    /**
     * Method to identify if the MSISDN is Valid.
     * 
     * @param configBean AuthorizationConfigBean
     * @param sdupRspBean SdupAuthorizationResponseBean
     * @return boolean
     */
    public static boolean isValidMsisdn(
            final AuthorizationConfigBean configBean,
            final SdupAuthorizationResponseBean sdupRspBean) {

        if (!configBean.getStatus()
                .equalsIgnoreCase(SmsConstants.NOTAPPLICABLE)) {
            if (((sdupRspBean.getStatus() == null) || (!configBean.getStatus()
                    .contains(sdupRspBean.getStatus())))) {
                return false;
            }
        }

        if (!configBean.getServiceProviderType().equalsIgnoreCase(
                SmsConstants.NOTAPPLICABLE)) {
            if ((sdupRspBean.getServiceProviderType() == null)
                    || (!configBean.getServiceProviderType().contains(
                            sdupRspBean.getServiceProviderType()))) {
                return false;
            }
        }

        if (!configBean.getServiceProvider().equalsIgnoreCase(
                SmsConstants.NOTAPPLICABLE)) {
            if ((sdupRspBean.getServiceProvider() == null)
                    || (!configBean.getServiceProvider().contains(
                            sdupRspBean.getServiceProvider()))) {
                return false;
            }
        }

        if (!configBean.getSmsMoBarring().equalsIgnoreCase(
                SmsConstants.NOTAPPLICABLE)) {
            if ((sdupRspBean.getSMSMoBarring() == null)
                    || (!configBean.getSmsMoBarring().contains(
                            sdupRspBean.getSMSMoBarring()))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns SDUP Namespace URI from mxos Config File.
     * 
     * @return String
     */
    public static String getNamespaceUri() {
        return System.getProperty(SmsConstants.SDUP_NAMESPACE_URI);
    }

    /**
     * Returns SDUP Servicename from mxos Config File.
     * 
     * @return String
     */
    public static String getSdupServiceName() {
        return System.getProperty(SmsConstants.SDUP_SERVICENAME);
    }

    /**
     * Returns SDUP Servicename from mxos Config File.
     * 
     * @param locale
     * @return String
     */
    public static String getMsisdnMessageSubject(MessageLocale locale) {
        MsisdnEmailMessageFactory msgFactory;
        try {
            msgFactory = MsisdnEmailMessageFactory.getInstance();
        } catch (MxOSException e) {
            logger.error("Error in getting message factory instance."
                    + e.getMessage());
            return null;
        }

        if (MessageLocale.UNK.equals(locale)) {
            locale = MessageLocale.en_US;
        }
        return msgFactory.getMessage(MsisdnMessageId.MBX_MSG_SUBJECT.name(),
                locale.name());
    }

    /**
     * Returns MSISDN message content.
     * 
     * @param messageId
     * @param locale
     * @param objects
     * @return MsisdnMessageContent
     */
    public static String getMsisdnMessageContent(MsisdnMessageId messageId,
            MessageLocale locale, String... objects) {
        StringBuilder content = new StringBuilder();
        MsisdnEmailMessageFactory msgFactory;
        try {
            msgFactory = MsisdnEmailMessageFactory.getInstance();
        } catch (MxOSException e) {
            logger.error("Error in getting message factory instance."
                    + e.getMessage());
            return content.toString();
        }
        boolean allFlag = false;

        switch (locale) {
        case UNK:
            allFlag = true;
        case en_US:
            prepareMessage(content, messageId, msgFactory, MessageLocale.en_US);
            if (allFlag) {
                content.append("\n");
            } else {
                break;
            }
        case de_DE:
            prepareMessage(content, messageId, msgFactory, MessageLocale.de_DE);
            if (allFlag) {
                content.append("\n");
            } else {
                break;
            }
        case fr_FR:
            prepareMessage(content, messageId, msgFactory, MessageLocale.fr_FR);
            if (allFlag) {
                content.append("\n");
            } else {
                break;
            }
        case nl_NL:
            prepareMessage(content, messageId, msgFactory, MessageLocale.nl_NL);
            if (allFlag) {
                content.append("\n");
            } else {
                break;
            }
        case en_UK:
            prepareMessage(content, messageId, msgFactory, MessageLocale.en_UK);
            if (allFlag) {
                content.append("\n");
            } else {
                break;
            }
        default:
            break;
        }
        String message = content.toString();
        if (MsisdnMessageId.MBX_MSG_PROXIMUS_SWAP_MSISDN.equals(messageId)
                && null != objects) {
            message = message.replace(SmsConstants.PLACEHOLDER_OLD_MSISDN,
                    objects[0]);
            message = message.replace(SmsConstants.PLACEHOLDER_NEW_MSISDN,
                    objects[1]);
        }
        return message;
    }

    /**
     * depositMail
     *
     */
    public static void depositMail(MxOSRequestState reqState, String toAddress,
            String subject, String message, String msgStoreHost,
            long mailboxId, String msgLocale) {
        StringBuilder emailContent = new StringBuilder();
        emailContent.append(SmsConstants.HEADER_FROM).append(System.getProperty(
                SystemProperty.createMessageFromAddress.name()))
                .append("\n");
        emailContent.append(SmsConstants.HEADER_TO).append(toAddress).append("\n");
        emailContent.append(SmsConstants.HEADER_SUBJECT).append(subject)
                .append("\n");
        emailContent.append(message);

        Map<String, List<String>> inputParams = reqState.getInputParams();
        inputParams.put(MxOSPOJOs.receivedFrom.name(), new ArrayList<String>());
        inputParams.get(MxOSPOJOs.receivedFrom.name()).add(System.getProperty(
                SystemProperty.createMessageFromAddress.name()));
        inputParams.put(MxOSPOJOs.message.name(), new ArrayList<String>());
        inputParams.get(MxOSPOJOs.message.name()).add(emailContent.toString());
        DataMap additionalParams = reqState.getAdditionalParams();
        additionalParams.setProperty(MailboxProperty.messageStoreHost, msgStoreHost);
        additionalParams.setProperty(MailboxProperty.mailboxId,
                String.valueOf(mailboxId));

        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        IBlobCRUD blobCRUD = null;
        try {
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(reqState);
            metaCRUD = metaCRUDPool.borrowObject();
            blobCRUD = CRUDUtils.getBlobCRUD(reqState);
            // Default FolderName INBOX
            reqState.getInputParams().put(
                    FolderProperty.folderName.name(),
                    new ArrayList<String>());
            reqState.getInputParams().get(
                    FolderProperty.folderName.name()).add(
                            SystemFolder.INBOX.name());
            MessageServiceHelper.create(metaCRUD, blobCRUD, reqState);
        } catch (MxOSException e) {
            logger.warn("Error while deposit mail.", e);
        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    logger.warn("Error in finally clause while returing "
                            + "Meta CRUD pool:" + e.getMessage());
                }
            }
        }
        logger.debug("Locale:: " + msgLocale);
        logger.debug("Message:: " + message);
    }

    private static void prepareMessage(StringBuilder content,
            MsisdnMessageId messageId, MsisdnEmailMessageFactory msgFactory,
            MessageLocale locale) {
        content.append(msgFactory.getMessage(
                MsisdnMessageId.MBX_MSG_SALUTATION.name(), locale.name()));
        content.append(msgFactory.getMessage(messageId.name(), locale.name()))
                .append("\n");
        content.append(
                msgFactory.getMessage(MsisdnMessageId.MBX_MSG_SIGNATURE.name(),
                        locale.name())).append("\n");
    }

}