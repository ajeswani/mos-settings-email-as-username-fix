/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.MultivaluedMap;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.process.ILogToExternalEntityService;
import com.opwvmsg.mxos.sms.custom.LoggingResponseBean;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.misc.ConnectionErrorStats;


/**
 * Utility class for MAA API's.
 *
 * @author mxos-dev
 */
public class MAAUtils {
    private static Logger logger = Logger.getLogger(MAAUtils.class);

    private MAAUtils() {
        throw new IllegalAccessError("Don't instantiate me!");
    }

    public static void logToMailAPIAdapter(String apiCall, String email,
            String payload, String resultcode) {
        Map<String, List<String>> inputParams =
            new HashMap<String, List<String>>();
        inputParams.put(SmsProperty.apicall.name(), new ArrayList<String>());
        inputParams.get(SmsProperty.apicall.name()).add(apiCall);
        inputParams.put(SmsProperty.timestamp.name(), new ArrayList<String>());
        String time = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss:SSS")
                .format(new Date(System.currentTimeMillis()));
        inputParams.get(SmsProperty.timestamp.name()).add(time);
        inputParams.put(SmsProperty.key.name(), new ArrayList<String>());
        inputParams.get(SmsProperty.key.name()).add(email);
        inputParams.put(SmsProperty.payload.name(), new ArrayList<String>());
        inputParams.get(SmsProperty.payload.name()).add("payload");
        inputParams.put(SmsProperty.resultcode.name(), 
                new ArrayList<String>());
        inputParams.get(SmsProperty.resultcode.name()).add("");
        inputParams.put(SmsProperty.crud.name(), new ArrayList<String>());
        inputParams.get(SmsProperty.crud.name()).add(
                MxosEnums.LogEventType.U.toString());

        try {
            ((ILogToExternalEntityService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LogToExternalEntityService.name()))
                    .process(inputParams);
        } catch (MxOSException e) {
            logger.error(e);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    /**
     * changeMailboxStatusViaMAA.
     * 
     * @param email string.
     * @param status string.
     * @throws MxOSException if any.
     */
    public static void changeMailboxStatusViaMAA(
            Map<String, List<String>> params) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Start : changeMailboxStatusViaMAA");
        }
        try {
            ((IMailboxBaseService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.LoggingMailboxBaseService.name()))
                    .update(params);
        } catch (Exception e) {
            logger.error(e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End : changeMailboxStatusViaMAA");
        }
        return;
    }

    /**
     * Method to parse the MAA response.
     * 
     * @param xmlContent xmlContent
     * @return LoggingResponseBean LoggingResponseBean
     */
    public static LoggingResponseBean parseResponse(StringBuffer xmlContent) {
        LoggingResponseBean maaResponseBean = null;
        if (xmlContent == null || xmlContent.equals("")) {
            return maaResponseBean;
        }
        maaResponseBean = new LoggingResponseBean();
        maaResponseBean.setFullMessage(xmlContent);
        String content = xmlContent.toString();
        try {
            InputStream is = 
                new ByteArrayInputStream(content.getBytes("UTF-8"));

            XMLInputFactory xmlif = XMLInputFactory.newInstance();
            XMLStreamReader xmlr = xmlif.createXMLStreamReader(is);
            if (logger.isDebugEnabled()) {
                logger.debug("Got the xml stream reader : " + xmlr);
            }
            int eventType = 0;

            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                case XMLEvent.END_DOCUMENT:
                    break;
                case XMLEvent.START_ELEMENT:
                    String name = xmlr.getLocalName();
                    logger.debug("Element name : " + name);
                    if ("RESULT".equals(name) && maaResponseBean != null) {
                        String attrCodeValue = xmlr.getAttributeValue(null,
                                "code");
                        logger.debug("Attribute code value is : "
                                + attrCodeValue);
                        maaResponseBean.setCode(attrCodeValue);
                        String elementValue = xmlr.getElementText();
                        logger.debug("Element value : " + elementValue);
                        maaResponseBean.setMessage(elementValue);
                    }
                    break;
                default:
                    break;
                }
            }
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        } catch (Exception e) {
            logger.error(e);
        }
        return maaResponseBean;
    }

    /**
     * Returns the domain from email.
     * 
     * @param email email
     * @return domain domain
     */
    public static final String getDomainFromEmail(final String email) {
        String domain = null;
        if (email != null && email.contains(MxOSConstants.AT_THE_RATE)) {
            domain = email.split(MxOSConstants.AT_THE_RATE)[1];
        }
        return domain;
    }

    /**
     * Returns the domain from email.
     * 
     * @param email email
     * @return username username
     */
    public static final String getUsernameFromEmail(final String email) {
        String username = null;
        if (email != null && email.contains(MxOSConstants.AT_THE_RATE)) {
            username = email.split(MxOSConstants.AT_THE_RATE)[0];
        }
        return username;
    }
}
