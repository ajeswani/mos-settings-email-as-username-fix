/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.config;

import java.util.Properties;

import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * Config Bean class with the properties for SendSMS Service.
 *
 * @author mxos-dev
 */
public class SendSmsConfigBean {

    private static final long serialVersionUID = -685341212567532344L;
    private String type;
    private String provider;
    private String url;
    private String auth;
    private String source;
    private String product;
    private String serviceCode;
    private String contentType;
    private String tempErrorList;
    private String snmpSendList;
    private boolean timeoutSnmpSend;
    private int timeout;
    private String host;
    private String port;
    private boolean debug;
    private int connectionTimeout;
    private int maxConnections;
    private Properties properties;

    /**
     * Default Constructor.
     */
    public SendSmsConfigBean() {
        type = null;
        provider = null;
        url = null;
        auth = null;
        source = null;
        product = null;
        serviceCode = null;
        contentType = null;
        tempErrorList = null;
        timeout = 0;
        host = null;
        port = "25";
        maxConnections = 0;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the auth
     */
    public String getAuth() {
        return auth;
    }

    /**
     * @param auth the auth to set
     */
    public void setAuth(String auth) {
        this.auth = auth;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the serviceCode
     */
    public String getServiceCode() {
        return serviceCode;
    }

    /**
     * @param serviceCode the serviceCode to set
     */
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the tempErrorList
     */
    public String getTempErrorList() {
        return tempErrorList;
    }

    /**
     * @param tempErrorList the tempErrorList to set
     */
    public void setTempErrorList(String tempErrorList) {
        this.tempErrorList = tempErrorList;
    }

    /**
     * @return the timeout
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the maxConnections
     */
    public int getMaxConnections() {
        return maxConnections;
    }

    /**
     * @param maxConnections the maxConnections to set
     */
    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }


    /**
     * @return the snmpSendList
     */
    public String getSnmpSendList() {
        return snmpSendList;
    }

    /**
     * @param snmpSendList the snmpSendList to set
     */
    public void setSnmpSendList(String snmpSendList) {
        this.snmpSendList = snmpSendList;
    }

    /**
     * @return the timeoutSnmpSend
     */
    public boolean isTimeoutSnmpSend() {
        return timeoutSnmpSend;
    }

    /**
     * @param timeoutSnmpSend the timeoutSnmpSend to set
     */
    public void setTimeoutSnmpSend(boolean timeoutSnmpSend) {
        this.timeoutSnmpSend = timeoutSnmpSend;
    }

    /**
     * @return the properties
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties() {
        properties = new Properties();
        // Need to fill the properties
        properties.put(SmsConstants.MAIL_SMTP_DEBUG, isDebug());
        properties.put(SmsConstants.MAIL_SMTP_CONNECTION_TIMEOUT,
                getConnectionTimeout());
        properties.put(SmsConstants.MAIL_SMTP_TIMEOUT, getTimeout());
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SendSmsConfigBean [type=").append(type)
                .append(", provider=").append(provider).append(", url=")
                .append(url).append(", auth=").append(auth).append(", source=")
                .append(source).append(", product=").append(product)
                .append(", serviceCode=").append(serviceCode)
                .append(", contentType=").append(contentType)
                .append(", tempErrorList=").append(tempErrorList)
                .append(", snmpSendList=").append(snmpSendList)
                .append(", timeoutSnmpSend=").append(timeoutSnmpSend)
                .append(", timeout=").append(timeout).append(", host=")
                .append(host).append(", port=").append(port)
                .append(", maxConnections=").append(maxConnections).append("]");
        return builder.toString();
    }

    /**
     * @return the debug
     */
    public boolean isDebug() {
        return debug;
    }

    /**
     * @param debug the debug to set
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * @return the connectionTimeout
     */
    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    /**
     * @param connectionTimeout the connectionTimeout to set
     */
    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

  
   
}
