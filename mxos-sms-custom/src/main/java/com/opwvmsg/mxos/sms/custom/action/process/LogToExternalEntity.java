/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.action.process;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.gateway.ILogToExternalEntityGateway;
import com.opwvmsg.mxos.sms.custom.gateway.SmsGatewayFactory;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;

/**
 * Action class for LogToExternal Entity.
 *
 * @author mxos-dev
 */
public class LogToExternalEntity implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(LogToExternalEntity.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("LogToExternalEntity action start."));
        }

        final String loggingGateway = System
                .getProperty(SystemProperty.loggingGateway.name());
        if (loggingGateway == null) {
            throw new ApplicationException(
                    CustomError.
                    LOG_TO_EXTERNAL_ENTITY_INVALID_GATEWAY_CONFIGURED.name(),
                    "No Gateway Configured for Event Logging"
                    + " in mxos.properties");
        }
        ((ILogToExternalEntityGateway) SmsGatewayFactory.getInstance()
                .getSmsGatewayContext(SmsConstants.LOGTOEXTERNALENTITYGATEWAYS,
                        loggingGateway)).process(
                                requestState.getInputParams());

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("LogToExternalEntity action end."));
        }
    }
}
