/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;

import com.opwvmsg.mxos.exception.ApplicationException;

/**
 * Helper class to send the HTTP Request.
 *
 * @author mxos-dev
 */
public class HTTPHelper {
    /**
     * 
     * @param method PostMethod
     * @param timeout int
     * @return StringBuffer
     * @throws ApplicationException if any
     * @throws HttpException if any
     * @throws IOException if any
     */
    public static StringBuffer sendHTTPRequest(final PostMethod method,
            final int timeout) throws ApplicationException, HttpException,
            IOException {

        // Send the HTTP Request and receive the response
        final HttpClient client = new HttpClient();

        client.setConnectionTimeout(timeout);

        client.executeMethod(method);

        BufferedReader bufferRead = null;

        bufferRead = new BufferedReader(new InputStreamReader(
                method.getResponseBodyAsStream()));
        String xmlLine = null;
        StringBuffer sb = new StringBuffer();
        while (((xmlLine = bufferRead.readLine()) != null)) {
            sb.append(xmlLine);
        }
        return sb;

    }
}
