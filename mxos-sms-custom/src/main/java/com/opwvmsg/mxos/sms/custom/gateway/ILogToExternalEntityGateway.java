/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.sms.custom.gateway;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Gateway interface for logging to the external Entity.
 * 
 * @author mxos-dev
 */
public interface ILogToExternalEntityGateway {

    /**
     * Method to log the event to the External Interface.
     * 
     * @param inputParams Map<String, List<String>>
     * @throws MxOSException
     */
    void process(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
