/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.custom.meg.crud;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException;
import com.opwvmsg.mxos.custom.meg.actions.ICustomStatusCRUD;
import com.opwvmsg.mxos.custom.meg.actions.MegCustomStatus;
import com.opwvmsg.mxos.custom.meg.crud.ConnectionManager.ConnectionAndHost;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * MySQL MEG CRUD APIs to access MEG Database during migration activity.
 * 
 * @author mxos-dev
 */
public class MySQLMegStatusCRUD implements ICustomStatusCRUD {
    private static Logger logger = Logger.getLogger(MySQLMegStatusCRUD.class);
    private Connection connection;
    private boolean status = false;
    private int activeHostIndex;
    private int megMaxRetries;

    /**
     * @return the activeHostIndex
     */
    public int getActiveHostIndex() {
        return activeHostIndex;
    }

    private int megRetryInterval;

    /**
     * Constructor
     * 
     * @throws MxOSException
     */
    MySQLMegStatusCRUD() throws MxOSException {
        megRetryInterval = Integer.parseInt(System
                .getProperty(MySQLConstants.MEG_RETRY_INTERVAL));
        megMaxRetries = Integer.parseInt(System
                .getProperty(MySQLConstants.MEG_MAX_RETRIES));
        int retryCount = 0;
        ConnectionAndHost connectionAndHost = null;
        do {
            connectionAndHost = (ConnectionAndHost) MySQLConnectionManager
                    .getInstance().getConnection();
            if (connectionAndHost != null)
                break;
            try {
                Thread.sleep(megRetryInterval);
                retryCount++;
            } catch (InterruptedException e) {
                logger.error("Error during the establish connection!");
            }
        } while (retryCount <= megMaxRetries);
        if (connectionAndHost == null) {
            logger.error("Failed to establish a connection with the MySQL. Retried for "
                    + retryCount + " times... giving up!");
            throw new ApplicationException(
                    CustomError.MEG_CONNECTION_ERROR.name(),
                    "Failed to establish a connection with the MySQL");
        }
        connection = (Connection) connectionAndHost.getConnection();
        activeHostIndex = connectionAndHost.getConnIndex();
    }

    @Override
    public void commit() throws ApplicationException {
        try {
            connection.commit();
        } catch (final Exception e) {
            logger.error("Error while commit the MEG", e);
            throw new ApplicationException(
                    CustomError.MEG_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void rollback() throws ApplicationException {
        try {
            connection.rollback();
        } catch (final Exception e) {
            logger.error("Error while rollback the MEG", e);
            throw new ApplicationException(
                    CustomError.MEG_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * Close database connection.
     * 
     * @throws Exception Throws Exception.
     */
    public boolean isConnected() {
        return this.status;
    }

    /**
     * Close database connection.
     * 
     * @throws Exception Throws Exception.
     */
    public void close() throws Exception {
        if (connection != null) {
            connection.close();
            this.status = false;
        }
    }

    /**
     * Creates a new connection to MySQL using the ConnectionManager.
     */
    public void getNewConnection() {
        try {

            // connect to the database using replication driver
            ConnectionAndHost connectionAndHost = MySQLConnectionManager
                    .getInstance().getConnection();

            if (connectionAndHost != null) {
                connection = (Connection) connectionAndHost.getConnection();
                activeHostIndex = connectionAndHost.getConnIndex();
                this.status = true;
                connection.setAutoCommit(false);
                connection
                        .setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            }
        } catch (SQLException e) {
            logger.info("SQLException while creating a connection with MYSQL",
                    e);
            connection = null;
        }
    }

    @Override
    public MegCustomStatus readStatus(final String email) throws MxOSException {
        java.sql.PreparedStatement st = null;
        ResultSet resultSet = null;
        if (logger.isDebugEnabled()) {
            logger.debug("MySQLMegStatusCRUD readStatus called.");
        }
        int retryCount = 0;
        do {
            try {
                if (connection == null) {
                    Thread.sleep(megRetryInterval);
                    getNewConnection();
                    if (null == connection) {
                        retryCount++;
                        continue;
                    }
                }

                connection.setReadOnly(true);

                st = connection
                        .prepareStatement("SELECT status FROM migration_status WHERE userid LIKE ?");

                int paramIndex = 1;
                st.setString(paramIndex, email);

                resultSet = st.executeQuery();
                retryCount = Integer.MAX_VALUE;
                if (resultSet.next()) {
                    int intStatus = resultSet.getInt(1);
                    for (MegCustomStatus s : MegCustomStatus.values()) {
                        if (s.ordinal() == intStatus) {
                            return s;
                        }
                    }
                }
                // Not Found
                logger.warn("Given Mailbox not found in MEG database: " + email);
                return MegCustomStatus.notMigrated;
            } catch (final MySQLNonTransientConnectionException e) {
                this.status = false;
                logger.error("SQLError while reading the MEG", e);
                throw new ApplicationException(
                        CustomError.MEG_CONNECTION_ERROR.name(), e);
            } catch (final CommunicationsException e) {
                logger.info(
                        "SQL CommunicationsException while readStatus from MEG. Trying to reconnect",
                        e);
                connection = null;
                retryCount++;
            } catch (final Exception e) {
                logger.error("Error while reading the MEG", e);
                throw new ApplicationException(
                        CustomError.MEG_READ_ERROR.name(), e);
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                } catch (final Exception e1) {
                    this.status = false;
                    logger.error("Error while closing rs, and st", e1);
                    throw new ApplicationException(
                            CustomError.MEG_CONNECTION_ERROR.name(), e1);
                }
                try {
                    if (st != null) {
                        st.close();
                    }
                } catch (final Exception e1) {
                    this.status = false;
                    logger.error("Error while closing rs, and st", e1);
                    throw new ApplicationException(
                            CustomError.MEG_CONNECTION_ERROR.name(), e1);
                }
            }
        } while (retryCount <= megMaxRetries);
        if ((retryCount >= megMaxRetries) && (connection == null)) {
            logger.error("Failed to perform readStatus from MySQL. Retried for "
                    + retryCount + " times... giving up!");
            throw new ApplicationException(
                    CustomError.MEG_CONNECTION_ERROR.name(),
                    "Failed to perform readStatus from MySQL.");
        }
        return null;
    }

    @Override
    public void updateStatus(final String email, final MegCustomStatus status)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("MySQLMegStatusCRUD updateStatus called.");
        }
        int retryCount = 0;
        do {
            java.sql.PreparedStatement st = null;
            try {
                if (connection == null) {
                    Thread.sleep(megRetryInterval);
                    getNewConnection();
                    if (null == connection) {
                        retryCount++;
                        continue;
                    }
                }

                connection.setReadOnly(false);

                final StringBuffer query = new StringBuffer();
                query.append("UPDATE migration_status SET status = ? WHERE userid = ? AND status = ?");

                st = connection.prepareStatement(query.toString());

                int paramIndex = 1;
                st.setInt(paramIndex++, status.ordinal());
                st.setString(paramIndex++, email);
                st.setInt(paramIndex++, status.previousStatus());

                st.executeUpdate();
                retryCount = Integer.MAX_VALUE;
            } catch (final MySQLNonTransientConnectionException e) {
                this.status = false;
                logger.error("SQLError while reading the MEG", e);
                throw new ApplicationException(
                        CustomError.MEG_CONNECTION_ERROR.name(), e);
            } catch (final CommunicationsException e) {
                logger.info(
                        "SQL CommunicationsException while updateStatus from MEG. Trying to reconnect",
                        e);
                connection = null;
                retryCount++;
            } catch (final Exception e) {
                logger.error("Error while updating the MEG", e);
                throw new ApplicationException(
                        CustomError.MEG_UPDATE_ERROR.name(), e);
            } finally {
                try {
                    if (st != null) {
                        st.close();
                    }
                } catch (final Exception e1) {
                    this.status = false;
                    logger.error("Error while closing st", e1);
                    throw new ApplicationException(
                            CustomError.MEG_CONNECTION_ERROR.name(), e1);
                }
            }
        } while (retryCount <= megMaxRetries);
        if ((retryCount >= megMaxRetries) && (connection == null)) {
            logger.error("Failed to perform updateStatus from MySQL. Retried for "
                    + retryCount + " times... giving up!");
            throw new ApplicationException(
                    CustomError.MEG_CONNECTION_ERROR.name(),
                    "Failed to perform updateStatus from MySQL.");
        }
    }

}
