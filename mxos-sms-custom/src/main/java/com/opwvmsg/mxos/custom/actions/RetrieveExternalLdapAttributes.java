package com.opwvmsg.mxos.custom.actions;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.external.ldap.ExternalLdapObject;
import com.opwvmsg.mxos.backend.external.ldap.ExternalLDAPUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

public class RetrieveExternalLdapAttributes implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(RetrieveExternalLdapAttributes.class);
    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Inside RetrieveExternalLdapAttributes.run() ");
        }
        ICRUDPool<ExternalLdapObject> extldapConnectionPool = null;
        ExternalLdapObject externalldapObject=null;
        try{
            extldapConnectionPool=MxOSApp.getInstance().getCURLdapPool();
            externalldapObject=extldapConnectionPool.borrowObject();
            ExternalLDAPUtils.retireveExternalLdapAttributes(requestState, externalldapObject);
        }catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get external ldap attributes.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name()+e.getMessage(), e);
        } finally {
            if (extldapConnectionPool != null && externalldapObject != null) {
                try {
                    extldapConnectionPool.returnObject(externalldapObject);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning External Ldap pool:"
                                    + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting RetrieveExternalLdapAttributes.run()");
        }

    }

}
