/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.custom.meg.actions;

import com.opwvmsg.mxos.backend.crud.ITransaction;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to access Migration Status from MEG.
 *
 * @author mxos-dev
 */
public interface ICustomStatusCRUD extends ITransaction {

    /**
     * Method to read migration status from MEG.
     * 
     * @param email email
     * @return status MegCustomStatus
     * @throws MxOSException if any error.
     */
    MegCustomStatus readStatus(final String email)
            throws MxOSException;

    /**
     * Method to update migration status into MEG.
     * 
     * @param email email
     * @param status status to be set
     * @throws MxOSException if any error.
     */
    void updateStatus(final String email,
            final MegCustomStatus status) throws MxOSException;

}
