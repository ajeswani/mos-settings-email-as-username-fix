/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.custom.meg.actions;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.custom.meg.crud.MegConnectionPool;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums.Status;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

/**
 * Action class to get Mailbox Custom Status from legacy systems.
 * 
 * @author mxos-dev
 */
public class GetMegCustomStatus implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMegCustomStatus.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetMegCustomStatus action start.");
        }
        if (requestState.getServiceName() == ServiceEnum.MailboxesService) {
            final List<Mailbox> mailboxes = ((List<Mailbox>) requestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.mailboxes));
            for (final Mailbox mailbox : mailboxes) {
                final String email = mailbox.getBase().getEmail();
                try {
                    getCustomStatus(email, mailbox.getBase());
                } catch (Exception e) {
                    logger.warn("Error while getting custom status of " + email);
                    continue;
                }
            }
        } else {
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            // Do not deep into MEG if the Domain is not in migration
            final Base base;
            if (requestState.getServiceName() == ServiceEnum.MailboxBaseService) {
                base = (Base) requestState.getDbPojoMap().getPropertyAsObject(
                        MxOSPOJOs.base);
            } else {
                base = ((Mailbox) requestState.getDbPojoMap()
                        .getPropertyAsObject(MxOSPOJOs.mailbox)).getBase();
            }
            getCustomStatus(email, base);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("GetMegCustomStatus action end.");
        }
    }
    /**
     * Method to read Custom Status.
     * @param email email
     * @param base base
     * @throws MxOSException if any
     */
    private static void getCustomStatus(final String email, final Base base)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("getCustomStatus start.");
        }
        final String domain = ActionUtils.getDomainFromEmail(email);
        if (domain == null || domain.equals("")) {
            logger.warn("The mailbox's domain is empty, so reading " +
            		"MEG status is not required for " + email);
            return;
        }
        final String domainMig = System
                .getProperty(SystemProperty.domainMigrationInProgress.name());
        if (domainMig != null && domainMig.equalsIgnoreCase(domain)) {
            ICustomStatusCRUD statusCRUD = null;
            try {
                statusCRUD = MegConnectionPool.getInstance().borrowObject();
                MegCustomStatus status = statusCRUD.readStatus(email);
                switch (status) {
                case notMigrated:
                    base.setStatus(Status.PROXY);
                    break;
                case inMigration:
                    base.setStatus(Status.MAINTENANCE);
                    break;
                case migrated:
                    // No change of status needed.
                    break;
                }
            } catch (final MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error("Error while get mailbox base.", e);
                throw new ApplicationException(
                        CustomError.MBX_UNABLE_TO_GET_CUSTOM_STATUS.name(), e);
            } finally {
                if (statusCRUD != null) {
                    try {
                        MegConnectionPool.getInstance()
                                .returnObject(statusCRUD);
                    } catch (final Exception e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                "Error in finally clause while returing "
                                        + "customStatus CRUD pool:"
                                        + e.getMessage());
                    }
                }
            }
        } else {
            logger.info("The mailbox's domain is not/already migrated, " +
            		"so reading MEG status is not required for " + email);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("getCustomStatus end.");
        }
    }
}
