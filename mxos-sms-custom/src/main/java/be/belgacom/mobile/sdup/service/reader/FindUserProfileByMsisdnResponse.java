/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.reader;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import be.belgacom.mobile.sdup.service.reader.xsd.SdupReaderServiceResponse;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content 
 * contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" 
 *         type="{http://reader.service.sdup.mobile.belgacom.be/xsd}
 *         SdupReaderServiceResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "_return" })
@XmlRootElement(name = "findUserProfileByMsisdnResponse")
public class FindUserProfileByMsisdnResponse {

    @XmlElementRef(name = "return", namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        type = JAXBElement.class)
    protected JAXBElement<SdupReaderServiceResponse> _return;

    /**
     * Gets the value of the return property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}
     *         {@link SdupReaderServiceResponse }{@code >}
     * 
     */
    public JAXBElement<SdupReaderServiceResponse> getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value allowed object is {@link JAXBElement }{@code <}
     *            {@link SdupReaderServiceResponse }{@code >}
     * 
     */
    public void setReturn(JAXBElement<SdupReaderServiceResponse> value) {
        this._return = ((JAXBElement<SdupReaderServiceResponse>) value);
    }

}
