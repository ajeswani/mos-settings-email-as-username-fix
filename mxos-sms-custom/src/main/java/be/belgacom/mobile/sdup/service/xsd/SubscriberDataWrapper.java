/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SubscriberDataWrapper complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content 
 * contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberDataWrapper">
 *   &lt;complexContent>
 *     &lt;extension base=
 *     "{http://service.sdup.mobile.belgacom.be/xsd}DataWrapper">
 *       &lt;sequence>
 *         &lt;element name="data" type=
 *         "{http://service.sdup.mobile.belgacom.be/xsd}
 *         UserProfileDataWrapper" minOccurs="0"/>
 *         &lt;element name="les" type=
 *         "{http://service.sdup.mobile.belgacom.be/xsd}
 *         UserProfileDataWrapper" minOccurs="0"/>
 *         &lt;element name="mms" type=
 *         "{http://service.sdup.mobile.belgacom.be/xsd}
 *         UserProfileDataWrapper" minOccurs="0"/>
 *         &lt;element name="payments" type=
 *         "{http://service.sdup.mobile.belgacom.be/xsd}
 *         UserProfileDataWrapper" minOccurs="0"/>
 *         &lt;element name="vpn" type=
 *         "{http://service.sdup.mobile.belgacom.be/xsd}
 *         UserProfileDataWrapper" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberDataWrapper", propOrder = { "data", "les", "mms",
        "payments", "vpn" })
public class SubscriberDataWrapper extends DataWrapper {

    @XmlElementRef(name = "data", 
            namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            type = JAXBElement.class)
    protected JAXBElement<UserProfileDataWrapper> data;
    @XmlElementRef(name = "les", 
            namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            type = JAXBElement.class)
    protected JAXBElement<UserProfileDataWrapper> les;
    @XmlElementRef(name = "mms", 
            namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            type = JAXBElement.class)
    protected JAXBElement<UserProfileDataWrapper> mms;
    @XmlElementRef(name = "payments", 
            namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            type = JAXBElement.class)
    protected JAXBElement<UserProfileDataWrapper> payments;
    @XmlElementRef(name = "vpn", 
            namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            type = JAXBElement.class)
    protected JAXBElement<UserProfileDataWrapper> vpn;

    /**
     * Gets the value of the data property.
     * 
     * @return possible object is {@link JAXBElement } {@code <}
     *         {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public JAXBElement<UserProfileDataWrapper> getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value allowed object is {@link JAXBElement } {@code <}
     *            {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public void setData(JAXBElement<UserProfileDataWrapper> value) {
        this.data = ((JAXBElement<UserProfileDataWrapper>) value);
    }

    /**
     * Gets the value of the les property.
     * 
     * @return possible object is {@link JAXBElement } {@code <}
     *         {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public JAXBElement<UserProfileDataWrapper> getLes() {
        return les;
    }

    /**
     * Sets the value of the les property.
     * 
     * @param value allowed object is {@link JAXBElement } {@code <}
     *            {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public void setLes(JAXBElement<UserProfileDataWrapper> value) {
        this.les = ((JAXBElement<UserProfileDataWrapper>) value);
    }

    /**
     * Gets the value of the mms property.
     * 
     * @return possible object is {@link JAXBElement } {@code <}
     *         {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public JAXBElement<UserProfileDataWrapper> getMms() {
        return mms;
    }

    /**
     * Sets the value of the mms property.
     * 
     * @param value allowed object is {@link JAXBElement } {@code <}
     *            {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public void setMms(JAXBElement<UserProfileDataWrapper> value) {
        this.mms = ((JAXBElement<UserProfileDataWrapper>) value);
    }

    /**
     * Gets the value of the payments property.
     * 
     * @return possible object is {@link JAXBElement } {@code <}
     *         {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public JAXBElement<UserProfileDataWrapper> getPayments() {
        return payments;
    }

    /**
     * Sets the value of the payments property.
     * 
     * @param value allowed object is {@link JAXBElement } {@code <}
     *            {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public void setPayments(JAXBElement<UserProfileDataWrapper> value) {
        this.payments = ((JAXBElement<UserProfileDataWrapper>) value);
    }

    /**
     * Gets the value of the vpn property.
     * 
     * @return possible object is {@link JAXBElement } {@code <}
     *         {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public JAXBElement<UserProfileDataWrapper> getVpn() {
        return vpn;
    }

    /**
     * Sets the value of the vpn property.
     * 
     * @param value allowed object is {@link JAXBElement } {@code <}
     *            {@link UserProfileDataWrapper }{@code >}
     * 
     */
    public void setVpn(JAXBElement<UserProfileDataWrapper> value) {
        this.vpn = ((JAXBElement<UserProfileDataWrapper>) value);
    }

}
