/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

/**
 * package info.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = 
"http://data.sdup.mobile.belgacom.be/xsd",
elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package be.belgacom.mobile.sdup.data.xsd;
