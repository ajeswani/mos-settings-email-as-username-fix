/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.reader.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import be.belgacom.mobile.sdup.service.xsd.ServiceResponse;
import be.belgacom.mobile.sdup.service.xsd.SubscriberDataWrapper;

/**
 * <p>
 * Java class for SdupReaderServiceResponse complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content 
 * contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SdupReaderServiceResponse">
 *   &lt;complexContent>
 *     &lt;extension base=
 *     "{http://service.sdup.mobile.belgacom.be/xsd}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="statusCode" type=
 *         "{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="statusMessage" 
 *         type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subscriberDataWrapper" 
 *         type=
 *         "{http://service.sdup.mobile.belgacom.be/xsd}SubscriberDataWrapper"
 *          maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SdupReaderServiceResponse", propOrder = { "statusCode",
        "statusMessage", "subscriberDataWrapper" })
public class SdupReaderServiceResponse extends ServiceResponse {

    protected Integer statusCode;
    @XmlElementRef(name = "statusMessage", namespace = 
        "http://reader.service.sdup.mobile.belgacom.be/xsd", 
        type = JAXBElement.class)
    protected JAXBElement<String> statusMessage;
    @XmlElement(nillable = true)
    protected List<SubscriberDataWrapper> subscriberDataWrapper;

    /**
     * Gets the value of the statusCode property.
     * 
     * @return possible object is {@link Integer }
     * 
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value allowed object is {@link Integer }
     * 
     */
    public void setStatusCode(Integer value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the statusMessage property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }
     *         {@code >}
     * 
     */
    public JAXBElement<String> getStatusMessage() {
        return statusMessage;
    }

    /**
     * Sets the value of the statusMessage property.
     * 
     * @param value allowed object is {@link JAXBElement }
     * {@code <}{@link String }
     *            {@code >}
     * 
     */
    public void setStatusMessage(JAXBElement<String> value) {
        this.statusMessage = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the subscriberDataWrapper property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the subscriberDataWrapper property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getSubscriberDataWrapper().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubscriberDataWrapper }
     * 
     * @return List<SubscriberDataWrapper>
     * 
     * 
     */
    public List<SubscriberDataWrapper> getSubscriberDataWrapper() {
        if (subscriberDataWrapper == null) {
            subscriberDataWrapper = new ArrayList<SubscriberDataWrapper>();
        }
        return this.subscriberDataWrapper;
    }

}
