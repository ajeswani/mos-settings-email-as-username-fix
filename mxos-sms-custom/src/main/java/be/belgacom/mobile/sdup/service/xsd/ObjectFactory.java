package be.belgacom.mobile.sdup.service.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface.
 * and Java element interface generated in the be.belgacom.mobile.sdup.
 * service.xsd package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances 
 * of the Java representation for XML content. The Java representation of 
 * XML content can consist of schema derived interfaces and classes 
 * representing the binding of schema type definitions, element declarations 
 * and model groups. Factory methods for each of these are provided in 
 * this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _KeyValuePairKey_QNAME = new QName(
            "http://service.sdup.mobile.belgacom.be/xsd", "key");
    private final static QName _SubscriberDataWrapperLes_QNAME = new QName(
            "http://service.sdup.mobile.belgacom.be/xsd", "les");
    private final static QName _SubscriberDataWrapperVpn_QNAME = new QName(
            "http://service.sdup.mobile.belgacom.be/xsd", "vpn");
    private final static QName _SubscriberDataWrapperPayments_QNAME = 
        new QName(
            "http://service.sdup.mobile.belgacom.be/xsd", "payments");
    private final static QName _SubscriberDataWrapperMms_QNAME = new QName(
            "http://service.sdup.mobile.belgacom.be/xsd", "mms");
    private final static QName _SubscriberDataWrapperData_QNAME = new QName(
            "http://service.sdup.mobile.belgacom.be/xsd", "data");

    /**
     * Create a new ObjectFactory that can be used to create new instances of.
     * schema derived classes for package: be.belgacom.mobile. sdup.service.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceResponse }.
     * 
     * @return ServiceResponse
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link UserProfileDataWrapper }.
     * 
     * @return UserProfileDataWrapper
     */
    public UserProfileDataWrapper createUserProfileDataWrapper() {
        return new UserProfileDataWrapper();
    }

    /**
     * Create an instance of {@link DataWrapper }.
     * 
     * @return DataWrapper
     */
    public DataWrapper createDataWrapper() {
        return new DataWrapper();
    }

    /**
     * Create an instance of {@link KeyValuePair }.
     * 
     * @return KeyValuePair
     */
    public KeyValuePair createKeyValuePair() {
        return new KeyValuePair();
    }

    /**
     * Create an instance of {@link SubscriberDataWrapper }.
     * 
     * @return SubscriberDataWrapper
     * 
     */
    public SubscriberDataWrapper createSubscriberDataWrapper() {
        return new SubscriberDataWrapper();
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <}{@link String }{@code >}
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            name = "key", scope = KeyValuePair.class)
    public JAXBElement<String> createKeyValuePairKey(String value) {
        return new JAXBElement<String>(_KeyValuePairKey_QNAME, String.class,
                KeyValuePair.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement } {@code <}.
     * {@link UserProfileDataWrapper }{@code >}
     * 
     * @param value UserProfileDataWrapper
     * @return JAXBElement<UserProfileDataWrapper>
     */
    @XmlElementDecl(namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            name = "les", scope = SubscriberDataWrapper.class)
    public JAXBElement<UserProfileDataWrapper> createSubscriberDataWrapperLes(
            UserProfileDataWrapper value) {
        return new JAXBElement<UserProfileDataWrapper>(
                _SubscriberDataWrapperLes_QNAME, UserProfileDataWrapper.class,
                SubscriberDataWrapper.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement } {@code <}.
     * {@link UserProfileDataWrapper }{@code >}
     * 
     * @param value UserProfileDataWrapper
     * @return JAXBElement<UserProfileDataWrapper>
     */
    @XmlElementDecl(namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            name = "vpn", scope = SubscriberDataWrapper.class)
    public JAXBElement<UserProfileDataWrapper> createSubscriberDataWrapperVpn(
            UserProfileDataWrapper value) {
        return new JAXBElement<UserProfileDataWrapper>(
                _SubscriberDataWrapperVpn_QNAME, UserProfileDataWrapper.class,
                SubscriberDataWrapper.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement } {@code <}.
     * {@link UserProfileDataWrapper }{@code >}
     * 
     * @param value UserProfileDataWrapper
     * @return JAXBElement<UserProfileDataWrapper>
     */
    @XmlElementDecl(namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            name = "payments", scope = SubscriberDataWrapper.class)
    public JAXBElement<UserProfileDataWrapper> 
    createSubscriberDataWrapperPayments(
            UserProfileDataWrapper value) {
        return new JAXBElement<UserProfileDataWrapper>(
                _SubscriberDataWrapperPayments_QNAME,
                UserProfileDataWrapper.class, SubscriberDataWrapper.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement } {@code <}.
     * {@link UserProfileDataWrapper }{@code >}
     * 
     * @param value UserProfileDataWrapper
     * @return JAXBElement<UserProfileDataWrapper>
     */
    @XmlElementDecl(namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            name = "mms", scope = SubscriberDataWrapper.class)
    public JAXBElement<UserProfileDataWrapper> createSubscriberDataWrapperMms(
            UserProfileDataWrapper value) {
        return new JAXBElement<UserProfileDataWrapper>(
                _SubscriberDataWrapperMms_QNAME, UserProfileDataWrapper.class,
                SubscriberDataWrapper.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement } {@code <}.
     * {@link UserProfileDataWrapper }{@code >}
     * 
     * @param value UserProfileDataWrapper
     * @return JAXBElement<UserProfileDataWrapper>
     */
    @XmlElementDecl(namespace = "http://service.sdup.mobile.belgacom.be/xsd", 
            name = "data", scope = SubscriberDataWrapper.class)
    public JAXBElement<UserProfileDataWrapper> createSubscriberDataWrapperData(
            UserProfileDataWrapper value) {
        return new JAXBElement<UserProfileDataWrapper>(
                _SubscriberDataWrapperData_QNAME, UserProfileDataWrapper.class,
                SubscriberDataWrapper.class, value);
    }

}
