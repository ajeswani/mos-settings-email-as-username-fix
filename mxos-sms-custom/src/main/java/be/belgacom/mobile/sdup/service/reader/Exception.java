/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.reader;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import be.belgacom.mobile.sdup.xsd.SdupException;

/**
 * <p>
 * Java class for Exception complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained 
 * within this class.
 * 
 * <pre>
 * &lt;complexType name="Exception">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Exception" 
 *         type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Exception", propOrder = { "exception" })
@XmlSeeAlso({ SdupException.class })
public class Exception {

    @XmlElementRef(name = "Exception", 
            namespace = "http://reader.service.sdup.mobile.belgacom.be", 
            type = JAXBElement.class)
    protected JAXBElement<Object> exception;

    /**
     * Gets the value of the exception property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link Object }
     *         {@code >}
     * 
     */
    public JAXBElement<Object> getException() {
        return exception;
    }

    /**
     * Sets the value of the exception property.
     * 
     * @param value allowed object is {@link JAXBElement }
     * {@code <}{@link Object }{@code >}
     * 
     */
    public void setException(JAXBElement<Object> value) {
        this.exception = ((JAXBElement<Object>) value);
    }

}
