/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.snapshot.stats;

/**
 * All available stat status such as pass, pass and timeout.
 *
 * @author mxos-dev
 */
public enum StatStatus {

    /**
     * pass: Specifies successful operation.
     */
    pass,

    /**
     * fail: Specifies failed operation.
     */
    fail,

    /**
     * timeout: Specifies timed out operation.
     */
    timeout;

}
