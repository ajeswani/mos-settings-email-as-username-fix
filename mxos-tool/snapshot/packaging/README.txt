/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave
 * Messaging Systems Inc. The software may be used and/or copied only with the
 * written permission of Openwave MessagingSystems Inc. or in accordance with
 * the terms and conditions stipulated in the agreement/contract under which the
 * software has been supplied. $Id: $
 */

Snapshot Tool

NOTE: Read the notes completely before starting use of this tool.

1. This tool is useful to take a snapshot of an account with regards to Mailbox, Folders, Messages, 
   Contacts and Groups details.

Download and Installation:
--------------------------
1. Download snapshot-package-0.0.1-SNAPSHOT.tar.gz from Openwave Central Repository.
2. Extract the bundle on Linux/Windows.

USAGE:
--------------------------
1. Get the latest (if this is not the latest snapshot.tar.gz) from Openwave's Central Maven Repo/Jenkins.
2. This provides the script (snapshot) to be used by the Admin.
3. Configure config/snapshot.properties. Description is provide in the same file for each parameter.
4. The account id whose snapshot has to be taken can be provided in the snapshot script as an command line 
    parameter or in the properties file.
5. Configure log4j as per your requirements.
6. snapshot.sh - To take a snapshot of the account use snapshot.bat to use on Windows env.
7. Tool cannot load runtime configurations. Restart required on config/XXXXX changes.
8. A new folder with the name provided in the properties will be created and all the information related to the 
   account will be stored in the form of files in this output folder.  

