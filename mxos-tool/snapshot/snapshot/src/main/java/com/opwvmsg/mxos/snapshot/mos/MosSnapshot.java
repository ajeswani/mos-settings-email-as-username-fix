/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave
 * Messaging Systems Inc. The software may be used and/or copied only with the
 * written permission of Openwave MessagingSystems Inc. or in accordance with
 * the terms and conditions stipulated in the agreement/contract under which the
 * software has been supplied. $Id: $
 */
package com.opwvmsg.mxos.snapshot.mos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Group;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsBaseService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsNameService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsBaseService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsMembersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.snapshot.ISnapshot;
import com.opwvmsg.mxos.snapshot.commons.LegalInterceptorException;
import com.opwvmsg.mxos.snapshot.stats.SnapshotOperation;
import com.opwvmsg.mxos.snapshot.stats.SnapshotServiceEnum;
import com.opwvmsg.mxos.snapshot.stats.StatStatus;
import com.opwvmsg.mxos.snapshot.stats.Stats;
import com.opwvmsg.mxos.snapshot.utils.ToolUtils;

/**
 * mos implementation for all account snapshot Operations.
 * 
 * @author mxos-dev
 */
public class MosSnapshot implements ISnapshot {
    private static Logger logger = Logger.getLogger(MosSnapshot.class);

    /**
     * This operation is responsible for reading all the Contacts of the
     * account.
     * 
     * @param userId Parameters given by user.
     * @return returns List<Contact>.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the contacts.
     * 
     */
    public List<Contact> readContacts(String userId)
            throws LegalInterceptorException {
        long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug("Sending readContacts request ...");
        }
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            // contactBase
            IContactsBaseService contactBase = (IContactsBaseService) ToolUtils
                    .getContext().getService(
                            ServiceEnum.ContactsBaseService.name());
            inputParams.put(AddressBookProperty.userId.name(),
                    new ArrayList<String>());
            inputParams.get(AddressBookProperty.userId.name()).add(userId);

            List<ContactBase> contactBaseList = contactBase.list(inputParams);

            List<Contact> contactList = new ArrayList<Contact>();

            for (ContactBase base : contactBaseList) {

                Contact contact = new Contact();

                // base
                contact.setContactBase(base);

                // name
                IContactsNameService contactNameService = (IContactsNameService) ToolUtils
                        .getContext().getService(
                                ServiceEnum.ContactsNameService.name());
                inputParams.clear();
                inputParams.put(AddressBookProperty.userId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.userId.name()).add(userId);

                inputParams.put(AddressBookProperty.contactId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.contactId.name()).add(
                        base.getContactId());

                Name contactName = contactNameService.read(inputParams);
                contact.setName(contactName);

                // workInfo
                inputParams.clear();
                inputParams.put(AddressBookProperty.userId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.userId.name()).add(userId);

                inputParams.put(AddressBookProperty.contactId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.contactId.name()).add(
                        base.getContactId());

                IContactsWorkInfoService contactWIService = (IContactsWorkInfoService) ToolUtils
                        .getContext().getService(
                                ServiceEnum.ContactsWorkInfoService.name());

                WorkInfo contactWI = contactWIService.read(inputParams);
                contact.setWorkInfo(contactWI);

                // personalInfo
                inputParams.clear();
                inputParams.put(AddressBookProperty.userId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.userId.name()).add(userId);

                inputParams.put(AddressBookProperty.contactId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.contactId.name()).add(
                        base.getContactId());

                IContactsPersonalInfoService contactPIService = (IContactsPersonalInfoService) ToolUtils
                        .getContext().getService(
                                ServiceEnum.ContactsPersonalInfoService.name());

                PersonalInfo contactPI = contactPIService.read(inputParams);
                contact.setPersonalInfo(contactPI);

                contactList.add(contact);

            }
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadContacts, t0, StatStatus.pass);

            return contactList;
        } catch (Exception e) {
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadContacts, t0, StatStatus.fail);

            throw new LegalInterceptorException(
                    "mOS error while reading Contacts", e);
        }

    }

    /**
     * This operation is responsible for reading all the Folders of the account.
     * 
     * @param userId Parameters given by user.
     * @return returns List<Folder>.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the folders.
     * 
     */
    public List<Folder> readFolder(String userId)
            throws LegalInterceptorException {
        long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug("Sending readFolder request ...");
        }
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            IFolderService service = (IFolderService) ToolUtils.getContext()
                    .getService(ServiceEnum.FolderService.name());
            inputParams.put(MailboxProperty.email.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.email.name()).add(userId);

            List<Folder> folderList = service.list(inputParams);
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadFolders, t0, StatStatus.pass);

            return folderList;
        } catch (Exception e) {
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadFolders, t0, StatStatus.fail);

            throw new LegalInterceptorException(
                    "mOS error while reading Folders", e);
        }
    }

    /**
     * This operation is responsible for reading all the Groups of the account.
     * 
     * @param userId Parameters given by user.
     * @return returns List<Group>.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the groups.
     * 
     */
    public List<Group> readGroups(String userId)
            throws LegalInterceptorException {
        long t0 = Stats.startTimer();

        if (logger.isDebugEnabled()) {
            logger.debug("Sending readGroups request ...");
        }

        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            // contactBase
            IGroupsBaseService groupBase = (IGroupsBaseService) ToolUtils
                    .getContext().getService(
                            ServiceEnum.GroupsBaseService.name());
            inputParams.put(AddressBookProperty.userId.name(),
                    new ArrayList<String>());
            inputParams.get(AddressBookProperty.userId.name()).add(userId);

            List<GroupBase> groupBaseList = groupBase.list(inputParams);

            List<Group> groupList = new ArrayList<Group>();

            for (GroupBase base : groupBaseList) {

                Group group = new Group();

                // base
                group.setGroupBase(base);

                // Members
                IGroupsMembersService groupMemService = (IGroupsMembersService) ToolUtils
                        .getContext().getService(
                                ServiceEnum.GroupsMemberService.name());

                inputParams.clear();
                inputParams.put(AddressBookProperty.userId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.userId.name()).add(userId);
                inputParams.put(AddressBookProperty.groupId.name(),
                        new ArrayList<String>());
                inputParams.get(AddressBookProperty.groupId.name()).add(
                        base.getGroupId());

                List<Member> groupMemberList = groupMemService
                        .readAll(inputParams);

                if (groupMemberList.size() > 0) {
                    group.setMembers(groupMemberList);
                }
                groupList.add(group);

            }
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadGroups, t0, StatStatus.pass);

            return groupList;

        } catch (Exception e) {
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadGroups, t0, StatStatus.fail);
            throw new LegalInterceptorException(
                    "mOS error while reading Groups", e);
        }
    }

    /**
     * This operation is responsible for reading the Mailbox of the account.
     * 
     * @return returns Mailbox.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the mailBox.
     * 
     */
    public Mailbox readMailBox(String userId) throws LegalInterceptorException {
        long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug("Sending readMailbox request ...");
        }

        try {

            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            IMailboxService service = (IMailboxService) ToolUtils.getContext()
                    .getService(ServiceEnum.MailboxService.name());

            inputParams.put(MailboxProperty.email.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.email.name()).add(userId);

            Mailbox mailbox = service.read(inputParams);
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadMailbox, t0, StatStatus.pass);

            return mailbox;
        } catch (Exception e) {
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadMailbox, t0, StatStatus.fail);

            throw new LegalInterceptorException(
                    "mOS error while reading mailbox", e);
        }
    }

    /**
     * This operation is responsible for reading all the MessageMetaData of the
     * account.
     * 
     * @return returns map of messageId as key and the message meta data as the
     *         value
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the messageMetaData.
     * 
     */
    public Map<String, Metadata> readMessageMetaData(String userId,
            String folderName) throws LegalInterceptorException {
        long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug("Sending readMessageMetaData request ...");
        }
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            IMetadataService service = (IMetadataService) ToolUtils
                    .getContext().getService(
                            ServiceEnum.MetadataService.name());
            inputParams.put(MailboxProperty.email.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.email.name()).add(userId);

            inputParams.put(FolderProperty.folderName.name(),
                    new ArrayList<String>());
            inputParams.get(FolderProperty.folderName.name()).add(folderName);

            inputParams.put(MessageProperty.isAdmin.name(),
                    new ArrayList<String>());
            inputParams.get(MessageProperty.isAdmin.name()).add(
                    MxOSConstants.TRUE);

            Map<String, Metadata> messageMap = service.list(
                    inputParams);
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadMessageMetaData, t0, StatStatus.pass);

            return messageMap;
        } catch (Exception e) {
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadMessageMetaData, t0, StatStatus.fail);

            throw new LegalInterceptorException(
                    "mOS error while reading Messages", e);
        }
    }

    /**
     * This operation is responsible for reading a particular message
     * 
     * @param userId Parameters given by user.
     * @return returns Message.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the message.
     * 
     */
    public Message readMessage(String userId, String folderName,
            String messageId) throws LegalInterceptorException {
        long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug("Sending readMessages request ...");
        }
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            IMessageService service = (IMessageService) ToolUtils.getContext()
                    .getService(ServiceEnum.MessageService.name());
            inputParams.put(MailboxProperty.email.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.email.name()).add(userId);

            inputParams.put(FolderProperty.folderName.name(),
                    new ArrayList<String>());
            inputParams.get(FolderProperty.folderName.name()).add(folderName);

            inputParams.put(MessageProperty.isAdmin.name(),
                    new ArrayList<String>());
            inputParams.get(MessageProperty.isAdmin.name()).add(
                    MxOSConstants.TRUE);

            inputParams.put(MessageProperty.messageId.name(),
                    new ArrayList<String>());
            inputParams.get(MessageProperty.messageId.name()).add(messageId);

            Message message = service.read(inputParams);
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadMessages, t0, StatStatus.pass);

            return message;
        } catch (Exception e) {
            Stats.stopTimer(SnapshotServiceEnum.mosSnapShot,
                    SnapshotOperation.ReadMessages, t0, StatStatus.fail);

            throw new LegalInterceptorException(
                    "mOS error while reading Messages", e);
        }
    }
}
