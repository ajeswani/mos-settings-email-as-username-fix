/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave
 * Messaging Systems Inc. The software may be used and/or copied only with the
 * written permission of Openwave MessagingSystems Inc. or in accordance with
 * the terms and conditions stipulated in the agreement/contract under which the
 * software has been supplied. $Id: $
 */
package com.opwvmsg.mxos.snapshot;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.snapshot.mos.MosSnapshot;

/**
 * The SnapshotWorkerThread is the worker thread respons=ible for reading the messages of the user in the multithreaded environment.
 * 
 * @author mxos-dev
 */

public class SnapshotWorkerThread extends Thread {
    private static Logger logger = Logger.getLogger(SnapshotWorkerThread.class);
    private String email;
    public static int threadCounter = 0;
    private ISnapshot mosSnapshot;
    private String folderName;
    private ConcurrentSkipListSet<String> messageList;
    private Map<String, Metadata> messageMetadataMap;

    /**
     * Constructor.
     * 
     * @param messageList object to be read.
     * @param userId whose messages are to be read.
     * @param folderName of thh folder which contains the message.
     * @param messageMetadataMap is the map of the messageMetadata whose
     *            snapshot has to be taken.
     * @throws Exception is thrown if any error occurs while creation of the
     *             thread
     */
    public SnapshotWorkerThread(ConcurrentSkipListSet<String> messageList,
            String userId, String folderName,
            Map<String, Metadata> messageMetadataMap) throws Exception {

        logger.info("SnapshotWorkerThread created for " + userId + folderName);

        if (userId != null) {
            this.email = userId;
        }
        if (folderName != null) {
            this.folderName = folderName;
        }
        if (messageList != null) {
            this.messageList = messageList;
        }
        if (messageMetadataMap != null) {
            this.messageMetadataMap = messageMetadataMap;
        }

    }

    /**
     * This is the actual logic of thread where snapshot of messages will be
     * taken.
     * 
     */
    @Override
    public void run() {
        if (this.email != null) {
            try {
                logger.info(new StringBuffer("Snapshot of the email:")
                        .append(this.email));

                this.mosSnapshot = new MosSnapshot();
                // Start taking snapshot of messages
                snapshotMessage();
                logger.info(new StringBuffer("Snapshot done for mailbox: ")
                        .append(this.email));
            } catch (Exception e) {
                logger.error("Error migrating mailbox: " + this.email);
            }
        }

    }

    /**
     * Method where snapshot of messages will be taken.
     * 
     * @throws Exception
     */
    private void snapshotMessage() throws Exception {

        try {
            for (Map.Entry<String, Metadata> entry : messageMetadataMap
                    .entrySet()) {

                logger.info("Key = " + entry.getKey() + ", Value = "
                        + entry.getValue());

                if (!messageList.contains(entry.getKey())) {
                    messageList.add(entry.getKey());
                    Body msg = mosSnapshot.readMessage(email,
                            folderName, entry.getKey()).getBody();
                    logger.info("Key = " + entry.getKey() + ", Value = " + msg);
                    LegalInterceptorTool.writeToFile(
                            "Folder Name: "
                                    + folderName
                                    + " "
                                    + new ObjectMapper()
                                            .writeValueAsString(msg) + "\n",
                            "Message-" + this.email + ".dat");

                }
            }

        } catch (Exception e) {
            logger.error("Error reading mailbox: " + this.email
                    + " on destination system", e);
            throw e;
        }
    }
}
