/*
 * Copyright (c) 2005 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/intermail/IntermailConfigDbWriter.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.config.intermail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.util.Utilities;

/**
 * a Class to handle writing the logic surrounding writing a config.db to disk.
 */
public class IntermailConfigDbWriter {

    private File configDbFile = null;
    private File configDbParentDir = null;

    private static final String CONFIG_DB = "config.db";
    private static final String TIMESTAMP = "timestamp";

    private static final Logger logger = Logger.getLogger(IntermailConfigDbWriter.class);

    /**
     * Initialize a new config.db writer
     * 
     * @param configDbFile the config.db File where the data will be written
     */
    protected IntermailConfigDbWriter(File configDbFile) {
        this.configDbFile = configDbFile;
        configDbParentDir = configDbFile.getParentFile();
    }

    /**
     * Gets the config.db file used for this writer.
     * 
     * @return config.db File
     */
    protected File getConfigDbFile() {
        return configDbFile;
    }

    /**
     * Attempt to write the config.db. Log any problems, but don't throw any
     * exceptions.
     * 
     * @param currentDict the current config.db data to write.
     */
    protected void writeConfigDb(ConfigDict currentDict) {
        if (configDbFile == null) {
            logger.error("No conifg.db file specified!");
            return;
        }

        File timestampFile = new File(configDbParentDir, "timestamp");
        // get timestamp
        File timeLock = null;

        if ((timeLock = getFileLock(TIMESTAMP)) != null) {
            String timestamp = null;
            String[] timestampValues = currentDict.getValues("/*/common/conftimestamp");
            if (timestampValues != null && timestampValues.length > 0) {
                timestamp = timestampValues[0];
            }
            String fileStamp = null;
            byte[] timeBytes = null;
            try {
                FileInputStream timeInput = new FileInputStream(timestampFile);
                timeBytes = new byte[timeInput.available()];
                timeInput.read(timeBytes, 0, timeBytes.length);
                fileStamp = new String(timeBytes, "us-ascii");
            } catch (FileNotFoundException ignore) {
                // if the file doesn't exist, don't sweat it..
                // we'll create one
            } catch (IOException ignore2) {
            }
            if (fileStamp != null && fileStamp.equals(timestamp)
                    && configDbFile.exists()) {
                // ok, timestamp was the same, don't write
                unlockFile(timeLock);
                return;
            } else {
                try {
                    timeBytes = timestamp.getBytes("us-ascii");
                    FileOutputStream timeOutput = new FileOutputStream(
                            timestampFile);
                    timeOutput.write(timeBytes, 0, timeBytes.length);
                    timeOutput.flush();
                } catch (FileNotFoundException ignore) {
                } catch (IOException ignore) {
                }
            }
            unlockFile(timeLock);
        } else {
            logger.warn("Could not lock timestamp file");
            // couldn't lock the timestamp.. probably someone else
            // is updating it..
            return;
        }

        if (configDbFile.exists()
                || (configDbParentDir != null && configDbParentDir.exists())) {
            File cfgLock = null;
            if ((cfgLock = getFileLock(CONFIG_DB)) != null) {
                writeFile(currentDict);
                unlockFile(cfgLock);
            } else {
                logger.error("Could not get write lock on config.db");
            }
        } else {
            logger.error("Could not write config.db to directory: "
                    + configDbFile.getAbsolutePath());
        }
        // for some reason the timestamp lock sometimes fails above,
        // so we'll try again here.
        unlockFile(timeLock);
    }

    /**
     * Attempts to create a file on disk that will act as a file lock for the
     * config.db.
     * 
     * @return
     */
    private synchronized File getFileLock(String filename) {
        File lock = new File(configDbParentDir, filename + ".lk");
        try {
            if (lock.createNewFile()) {
                FileWriter writer = new FileWriter(lock);
                writer.write((new Date()).toString());
                writer.flush();
                return lock;
            }
        } catch (IOException ignore) {
            // exception is just another way we didn't get the lock.
        }
        return null;
    }

    /**
     * Removes the locking file from disk.
     * 
     * @param lock the File representing the lock
     */
    private void unlockFile(File lock) {
        if (lock != null && lock.exists()) {
            lock.delete();
        }
    }

    /**
     * Writes this config.db file to disk
     * 
     * @param currentDict the ConfigDict to write
     */
    private void writeFile(ConfigDict currentDict) {
        StringBuffer fileBuffer = new StringBuffer(10000);
        Set configDbKeys = currentDict.getAllKeys();
        Iterator keyIterator = configDbKeys.iterator();
        while (keyIterator.hasNext()) {
            String nextKey = (String)keyIterator.next();
            fileBuffer.append(currentDict.getOriginalKey(nextKey));
            fileBuffer.append(": ");
            String[] values = currentDict.getValues(nextKey);
            if (values.length == 0) {
                fileBuffer.append("    []\n");
            } else if (values.length == 1
                    && ((nextKey.length() + values[0].length() + 4) <= 80)) {
                fileBuffer.append("[" + escapeValue(values[0]) + "]\n");
            } else {
                fileBuffer.append("\n");
                for (int i = 0; i < values.length; i++) {
                    fileBuffer.append("    [" + escapeValue(values[i]) + "]\n");
                }
            }
        }

        String checksumText = getChecksum(fileBuffer);
        try {
            FileOutputStream outStream = new FileOutputStream(configDbFile);

            outStream.write(checksumText.getBytes("us-ascii"));
            outStream.write(fileBuffer.toString().getBytes("us-ascii"));
            outStream.flush();
        } catch (FileNotFoundException ignore) {
            // we've already checked this case
        } catch (UnsupportedEncodingException ignore2) {
            // can't happen
        } catch (IOException logMe) {
            logger.error("IO Error writing config.db: " + logMe);
        }

    }

    /**
     * Escapes the value so it can be safely written to the config.db
     * 
     * @param value the value to be escaped
     * @return escaped String.
     */
    private String escapeValue(String value) {
        StringBuffer newValue = new StringBuffer(value.length() * 2);
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            switch (c) {
            case '\r':
                newValue.append('\\');
                newValue.append('r');
                break;
            case '\n':
                newValue.append('\\');
                newValue.append('n');
                break;
            case '\t':
                newValue.append('\\');
                newValue.append('t');
                break;
            case '[':
            case ']':
            case '\\':
                newValue.append('\\');
                newValue.append(c);
                break;
            default:
                newValue.append(c);
            }
        }
        return newValue.toString();
    }

    /**
     * Gets the checksum to use with this file
     * 
     * @param fileBuffer the actual text of the file to be written
     * @return the checksum string for the file header
     */
    private String getChecksum(StringBuffer fileBuffer) {
        byte digest[] = new byte[16];
        byte[] fileBytes = new byte[0];
        try {
            fileBytes = fileBuffer.toString().getBytes("us-ascii");
        } catch (UnsupportedEncodingException ignore) {
        }
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            md5.update(fileBytes, 0, fileBuffer.length());
            md5.digest(digest, 0, 16);
        } catch (Exception e) {
            logger.error("Error computing config.db checksum");
            return null;
        }

        String newSum = Utilities.byteArrayToHexString(digest);
        for (int i = 0; i < 16; i++) {
            digest[i] = fileBytes[i];
        }
        String oldSum = Utilities.byteArrayToHexString(digest);

        return IntermailConfig.warningLine + newSum + "\n" + oldSum + "\n";

    }
}