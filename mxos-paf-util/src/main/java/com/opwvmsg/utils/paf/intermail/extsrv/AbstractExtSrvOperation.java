/*
 * H+ Copyright 2004 Openwave Systems, Inc. All Rights Reserved. The copyright
 * to the computer software herein is the property of Openwave Systems, Inc. The
 * software may be used and/or copied only with the written permission of
 * Openwave Systems, Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:
 * //paf/mainline/util/src/main/java/com/openwave/intermail/extsrv
 * /AbstractExtSrvOperation.java#1 $ H-
 */
package com.opwvmsg.utils.paf.intermail.extsrv;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This class extends AbstractOperation and implements connection specific
 * methods for an Extension Service operation.
 */
public abstract class AbstractExtSrvOperation extends AbstractOperation {
    // RME Ops implemented
    protected final static int EXT_RME_FASTCALL = 6;

    // Each extension service is defined to execute when certain pre-defined
    // "hooks" in the code are encountered. For example, one hook is defined
    // for when a message is read; another for when a message is created.
    // This hookName describes the hook that we encountered that lead to
    // this operation being executed.
    protected String hookName;

    private List servicesList = new ArrayList(0);

    // This is the service currently being operated on.
    protected ExtensionService currentService;

    /**
     * Load the list of enabled services for this hook.
     * 
     * @param hook The hook name
     */
    protected void initializeHook(String hook) {
        hookName = hook;
        servicesList = ServiceManager.getEnabledServicesList(hook);
    }

    /**
     * Gets an iterator for the list of services
     * 
     * @return iterator for services list
     */
    protected Iterator getServicesIterator() {
        return servicesList.iterator();
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#executeRmeOp(int)
     */
    protected void callRme(int id) throws IntermailException {
        callRme(id, AbstractOperation.RME_SEND_DEFAULT_CLIENT_INDEX, false);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#getConnection()
     */
    protected void connect(boolean usePool) throws IntermailException {
        if (rmeSocket == null || !rmeSocket.isReady()) {
            // get new rsock from pool
            rmeSocket = ServiceManager.getRmeSocket(currentService, usePool);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#releaseConnection()
     */
    protected void releaseConnection() throws IntermailException {
        if (rmeSocket != null) {
            ServiceManager.putRmeSocket(rmeSocket, currentService);
            rmeSocket = null;
        }
    }

    @Override
    protected void createHttpClient(boolean usePool, int port)
            throws IntermailException {
    }

    @Override
    protected void releaseHttpConnection() throws IntermailException {
        // TODO Auto-generated method stub
    }

}
