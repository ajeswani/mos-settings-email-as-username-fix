/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/extsrv/ImxData.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.extsrv;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * The ImxData object is the basic primitive object used in extserver RME
 * operations. It is a generic data structure that can hold many different types
 * of real primitives. Using this data type as a container allows the extension
 * services to change the schema without affecting the RME protocol.
 * Unfortunately, this works against us, since the client and server must keep
 * in sync regarding what the schema actually is and the mechanism to do this is
 * not portable to Java.
 */
public class ImxData {
    //constant types
    public final static int IMX_UNDEFINED = 0;
    public final static int IMX_INTEGER = 1;
    public final static int IMX_I4 = 2;
    public final static int IMX_BOOLEAN = 3;
    public final static int IMX_STRING = 4;
    public final static int IMX_DOUBLE = 5;
    public final static int IMX_DATETIME = 6;
    public final static int IMX_BASE64 = 7;
    public final static int IMX_STRUCT = 8;
    public final static int IMX_ARRAY = 9;
    public final static int IMX_TYPE_END = 10;

    public final static String[] typeNames = { "Undefined", "Integer",
            "Integer", "Boolean", "String", "Double", "Date/Time", "Base64",
            "Struct", "Array", "End" };

    private int type;

    // internal vars to hold all possible data
    // only one will be valid
    private int intData;
    private boolean boolData;
    private byte[] stringData;
    private ImxArray arrayData;
    private ImxStruct structData;

    /**
     * Constructor for this ImxData type from a stream.
     * 
     * @param inStream Stream containing the ImxData bytes
     * @throws IOException on any error
     */
    public ImxData(RmeInputStream inStream) throws IOException {
        type = inStream.read();
        switch (type) {
        case (IMX_BOOLEAN):
            boolData = inStream.readBoolean();
            break;
        case (IMX_INTEGER):
        case (IMX_I4):
            intData = inStream.readInt();
            break;
        case (IMX_STRING):
            stringData = inStream.readStringBytes();
            break;
        case (IMX_ARRAY):
            arrayData = new ImxArray(inStream);
            break;
        case (IMX_STRUCT):
            structData = new ImxStruct(inStream);
            break;
        default:
            throw new ClassCastException();
        }
    }

    /**
     * Create a new ImxData object containing a boolean value
     * 
     * @param bool the boolean value
     */
    public ImxData(boolean bool) {
        type = IMX_BOOLEAN;
        boolData = bool;
    }

    /**
     * Create a new ImxData object containing a integer value
     * 
     * @param intIn the integer value
     */
    public ImxData(int intIn) {
        type = IMX_INTEGER;
        intData = intIn;
    }

    /**
     * Create a new ImxData object containing a byte array value
     * 
     * @param stringIn the byte array input
     */
    public ImxData(byte[] stringIn) {
        type = IMX_STRING;
        stringData = stringIn;
    }

    /**
     * Create a new ImxData object containing a unicode String value. Note that
     * the String will be decoded using the UTF-8 charset. If this is not
     * desirable, use the byte[] constructor.
     * 
     * @param stringIn a unicode String
     */
    public ImxData(String stringIn) {
        byte[] bvalue = null;
        try {
            type = IMX_STRING;
            if (stringIn != null) {
                stringData = stringIn.getBytes("UTF-8");
            } else {
                stringData = null;            
            }
        } catch (UnsupportedEncodingException e) {
        }
    }

    /**
     * Create a new ImxData object containing an ImxArray
     * 
     * @param arrayIn ImxArray input
     */
    public ImxData(ImxArray arrayIn) {
        type = IMX_ARRAY;
        arrayData = arrayIn;
    }

    /**
     * Create a new ImxData object containing an ImxStruct
     * 
     * @param hashIn the ImxStruct input
     */
    public ImxData(ImxStruct hashIn) {
        type = IMX_STRUCT;
        structData = hashIn;
    }

    /**
     * Get the type represented by this ImxData object.
     * 
     * @return an integer type code.
     */
    public int getType() {
        return type;
    }

    /**
     * Get the data from this object as a boolean
     * 
     * @return the boolean value
     * @throws ClassCastException if the ImxData doesn't represent a boolean
     */
    public boolean getBoolean() {
        if (type == IMX_BOOLEAN) {
            return boolData;
        } else {
            throw new ClassCastException("Boolean requested when actual type was "
                    + getTypeName(type));
        }
    }

    /**
     * Get the data from this object as an integer
     * 
     * @return the integer value
     * @throws ClassCastException if the ImxData doesn't represent an integer
     */
    public int getInt() {
        if (type == IMX_INTEGER || type == IMX_I4) {
            return intData;
        } else {
            throw new ClassCastException("Integer requested when actual type was "
                    + getTypeName(type));
        }
    }

    /**
     * Get the data from this object as a byte array
     * 
     * @return the byte array value
     * @throws ClassCastException if the ImxData doesn't represent an ImxString
     */
    public byte[] getImxString() {
        if (type == IMX_STRING) {
            return stringData;
        } else {
            throw new ClassCastException("String requested when actual type was "
                    + getTypeName(type));
        }
    }

    /**
     * Get the data from this object as a unicode string. Note that the internal
     * ImxString (byte array) will be encoded using the UTF-8 charset. If this
     * is not desirable, get this data as a byte array instead.
     * 
     * @return the unicode String value
     * @throws ClassCastException if the ImxData doesn't represent an ImxString
     */
    public String getUtf8String() {
        if (type == IMX_STRING) {
            try {
                if (stringData != null) {
                    return new String(stringData, "UTF-8");
                } else {
                    return null;
                }
            } catch (UnsupportedEncodingException ue) {
            }
            return null;
        } else {
            throw new ClassCastException("String requested when actual type was "
                    + getTypeName(type));
        }
    }

    /**
     * Get the data from this object as an ImxArray
     * 
     * @return the ImxArray value
     * @throws ClassCastException if the ImxData doesn't represent an ImxArray
     */
    public ImxArray getArray() {
        if (type == IMX_ARRAY) {
            return arrayData;
        } else {
            throw new ClassCastException("Array requested when actual type was "
                    + getTypeName(type));
        }
    }

    /**
     * Get the data from this object as an ImxStruct
     * 
     * @return the ImxStruct value
     * @throws ClassCastException if the ImxData doesn't represent an ImxStruct
     */
    public ImxStruct getStruct() {
        if (type == IMX_STRUCT) {
            return structData;
        } else {
            throw new ClassCastException("Struct requested when actual type was "
                    + getTypeName(type));
        }
    }

    /**
     * Given an integer type code, return a String name for that type
     * 
     * @param type the integer type code
     * @return a String name for that type
     */
    private String getTypeName(int type) {
        if (type >= 0 && type <= typeNames.length) {
            return typeNames[type];
        } else {
            return "Invalid Type (" + type + ")";
        }
    }

    /**
     * Write this ImxData object to an output stream
     * 
     * @param outStream the output stream
     * @throws IOException on any IO error
     */
    public void writeImxData(RmeOutputStream outStream) throws IOException {
        outStream.writeByte(type);
        switch (type) {
        case (IMX_BOOLEAN):
            outStream.writeBoolean(boolData);
            break;
        case (IMX_INTEGER):
        case (IMX_I4):
            outStream.writeInt(intData);
            break;
        case (IMX_STRING):
            outStream.writeStringBytes(stringData);
            break;
        case (IMX_ARRAY):
            arrayData.writeImxArray(outStream);
            break;
        case (IMX_STRUCT):
            structData.writeImxStruct(outStream);
            break;
        }
    }

}

