/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class encapsulates the attribute datatype and attribute values of the
 * Message.
 * 
 * @author mOS-dev
 */
class MessageAttribute {
    short dataType;
    Object value;

    public static final short ATTR_TYPE_STRING = (short) 0x00;
    public static final short ATTR_UNIT8 = (short) 0x01;
    public static final short ATTR_UNIT16 = (short) 0x02;
    public static final short ATTR_UNIT32 = (short) 0x03;
    public static final short ATTR_UNIT64 = (short) 0x04;
    public static final short ATTR_UNIT128 = (short) 0x05;
    public static final short ATTR_VECTOR_STRING = (short) 0x06;
    public static final short ATTR_VECTOR_UNIT8 = (short) 0x07;
    public static final short ATTR_VECTOR_UNIT16 = (short) 0x08;
    public static final short ATTR_VECTOR_UNIT32 = (short) 0x09;
    public static final short ATTR_VECTOR_UNIT64 = (short) 0x0a;
    public static final short ATTR_VECTOR_UNIT128 = (short) 0x0b;

    /**
     * Constructor. Used for setting the dataType and value of the attribute
     * 
     * @param dataType of the attribute to be set
     * @param value of the attribute to be set
     */
    public MessageAttribute(short dataType, Object value){
        this.dataType = dataType;
        this.value = value;
    }
    
    /**
     * Constructor. Reads this MessageAttribute object from an RME stream
     * 
     * @param inStream the RME input stream
     * @param dataType of the attribute to be read
     * @throws IOException on any IO error
     */
    public MessageAttribute(short dataType, RmeInputStream inStream)
            throws IOException {
        this.dataType = dataType;
        switch (dataType) {
        case ATTR_TYPE_STRING:
            value = inStream.readByteArray();
            break;
        case ATTR_UNIT8:
            value = inStream.readByte();
            break;
        case ATTR_UNIT16:
            value = inStream.readShort();
            break;
        case ATTR_UNIT32:
            value = inStream.readInt();
            break;
        case ATTR_UNIT64:
            value = inStream.readLong64();
            break;
        case ATTR_UNIT128:
            value = inStream.readUUID();
            break;
        case ATTR_VECTOR_STRING:
            value = inStream.readByteStringArray();
            break;
        case ATTR_VECTOR_UNIT8:
            value = inStream.readByteArray();
            break;
        case ATTR_VECTOR_UNIT16:
            value = inStream.readStringArray();
            break;
        case ATTR_VECTOR_UNIT32:
            value = inStream.readIntArray();
            break;
        case ATTR_VECTOR_UNIT64:
            value = inStream.readLongArray();
            break;
        case ATTR_VECTOR_UNIT128:
            value = inStream.readUUIDArray();
            break;
        default:
            throw new IOException(
                    "Unsupported Data format received from MSS : " + dataType);
        }
    }

    /**
     * Writes value to outpurStream
     * 
     * @param outStream the RME outStream stream
     * @throws IOException on any IO error
     */
    public void write(MessageAttribute msgAttribute, RmeOutputStream outStream)
            throws IOException {
        short dataType = msgAttribute.getDataType();
        switch (dataType) {
        case ATTR_TYPE_STRING:
            outStream.writeStringBlob((String)msgAttribute.getValue());
            break;
        case ATTR_UNIT8:
            outStream.writeByte((Integer)msgAttribute.getValue());
            break;
        case ATTR_UNIT16:
            outStream.writeShort((Integer)msgAttribute.getValue());
            break;
        case ATTR_UNIT32:
            outStream.writeInt((Integer)msgAttribute.getValue());
            break;
        case ATTR_UNIT64:
            outStream.writeLong((Long)msgAttribute.getValue());
            break;
        case ATTR_UNIT128:
            outStream.writeUUID((UUID)msgAttribute.getValue());
            break;
        case ATTR_VECTOR_STRING:
            outStream.writeStringArrayBlob((String[])msgAttribute.getValue());
            break;
        case ATTR_VECTOR_UNIT8:
            outStream.writeByteArray((byte[])msgAttribute.getValue());
            break;
        case ATTR_VECTOR_UNIT16:
            outStream.writeShortArray((short[])msgAttribute.getValue());
            break;
        case ATTR_VECTOR_UNIT32:
            outStream.writeIntArray((int[])msgAttribute.getValue());
            break;
        case ATTR_VECTOR_UNIT64:
            outStream.writeLongArray((long[])msgAttribute.getValue());
            break;
        case ATTR_VECTOR_UNIT128:
            outStream.writeUUIDArray((UUID[])msgAttribute.getValue());
            break;
        default:
            throw new IOException(
                    "Unsupported Data format: " + dataType);
        }
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * @return the dataType
     */
    public short getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(short dataType) {
        this.dataType = dataType;
    }
}