package com.opwvmsg.utils.paf.intermail.mail;

public enum RmeDataModel {
    /**
     * <tt>CL</tt> : Connection-less RME 
     */
    CL,
    /**
     * <tt>Leopard</tt> : Leopard data model
     * @since Email Mx 9.0
     */
    Leopard
}
