/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/ReadMailbox.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.Mailbox;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation reads the basic mailbox information from the MSS
 */
public class ReadMailbox extends AbstractMssOperation {

    private String url;
    private String name;
    private Mailbox mbox;

    /**
     * Constructor for ReadMailbox
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     */
    public ReadMailbox(String host, String name) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_STATISTIC);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        // check args
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_STATISTIC);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_P_STATISTIC", host,
                            "dataModel=" + rmeDataModel }));
        }
    }

    /**
     * Gets the mailbox object returned from the MSS
     * 
     * @return mailbox
     */
    public Mailbox getMailbox() {
        return mbox;
    }

    // RME FUNCTIONS

    // mss stats constants
    private static final int MSSCountMessages = 4;
    private static final int MSSCountOtherBytes = 7;
    private static final int MSSAmountStored = 1;
    // arrays to send for stats
    private static final int[] mssReqArray = new int[] { MSSCountMessages,
            MSSCountOtherBytes };
    private static final int[] mssQualArray = new int[] { MSSAmountStored,
            MSSAmountStored };

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // client Id
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        // ms url
        outStream.writeString(url);
        // request array
        outStream.writeIntArray(mssReqArray);
        // qual array
        outStream.writeIntArray(mssQualArray);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        mbox = new Mailbox(inStream);
        logEvents = inStream.readLogEvent();
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation is not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation is not supported
    }

}
