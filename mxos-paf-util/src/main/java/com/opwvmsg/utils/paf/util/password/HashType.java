/*
 * Copyright 2003-2004 Openwave Systems Inc.  All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/HashType.java#1 $
 */

package com.opwvmsg.utils.paf.util.password;

import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Type-safe enumeration that represents a hash type for a password.
 *
 * @author Conrad Damon
 * @version $Revision: #1 $
 */

public class HashType implements Serializable {

    static final long serialVersionUID = 6615947314347252518L;

    /**
     * Identity hash (no hashing is done)
     */
    public static final HashType CLEAR = new HashType("clear");

    /**
     * MD5 (message digest) hash
     */
    public static final HashType MD5 = new HashType("md5");

    /**
     * Unix crypt hash
     */
    public static final HashType UNIX = new HashType("unix");

    /**
     * SHA (secure hash algorithm) hash
     */
    public static final HashType SHA = new HashType("sha");

    /**
     * SSHA1 hash
     */
    public static final HashType SSHA1 = new HashType("ssha1");
    
    /**
     * Custom1 hash
     */
    public static final HashType CUSTOM1 = new HashType("custom1");

    /**
     * Custom2 hash
     */
    public static final HashType CUSTOM2 = new HashType("custom2");

    /**
     * Custom3 hash
     */
    public static final HashType CUSTOM3 = new HashType("custom3");

    /**
     * Custom4 hash
     */
    public static final HashType CUSTOM4 = new HashType("custom4");

    /**
     * Custom5 hash
     */
    public static final HashType CUSTOM5 = new HashType("custom5");
    
    /**
     * Bcrypt hash
     */
    public static final HashType BCRYPT = new HashType("bcrypt");

    /**
     * Set of all defined values of HashType.  If this class is
     * modified to include additional values they must be added to
     * this array.  The array order is not significant.
     */
    private static HashType[] values = {
        CLEAR, MD5, UNIX, SHA, SSHA1, CUSTOM1, CUSTOM2, CUSTOM3, CUSTOM4,
        CUSTOM5, BCRYPT
    };

    /**
     * Map from field names to field values.
     */
    private static Map nameMap = new HashMap();

    static {
        for (int i = 0; i < values.length; i++) {
            nameMap.put(values[i].toString(), values[i]);
        }
    }

    /**
     * Get the instance with a given name, if any.
     * <p>
     * @param name the name of the hash type
     * @return the instance, or <code>null</code> if there is no
     *         field with that name.
     */
    public static HashType forName(String name) {
        return (HashType) nameMap.get(name);
    }

    /**
     * The field name.
     */
    private final String name;


    /**
     * Class constructor.
     *
     * @param name the name of this field
     */
    protected HashType(String name) {
        this.name = name;
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        return name;
    }

    /**
     * On deserialization, use the canonical instance for each HashType.
     */
    protected Object readResolve() throws ObjectStreamException {

        HashType instance = (HashType) nameMap.get(name);
        if (instance == null) {
            throw new InvalidObjectException("unknown HashType instance '" 
                                             + name + "'");
        }
        return instance;
    }
}
