/*
 * Copyright (c) 2002-2005 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/taglibs/config/GetTag.java#1 $
 */

package com.opwvmsg.utils.paf.taglibs.config;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.jsp.tagext.TagSupport;
import com.opwvmsg.utils.paf.jsp.tagext.TagUtilities;
import com.opwvmsg.utils.paf.util.EscapeType;


/**
 * Prints out a string with the value of the requested configuration item.
 * There is one required attribute, <code>key</code>, and two optional 
 * attributes, <code>host</code> and <code>app</code>. If not provided, 
 * the host defaults to "localhost" and the application defaults to "common".
 *
 * @author Conrad Damon
 * @version $Revision: #1 $
 */

public class GetTag extends TagSupport {

    private static final String DEFAULT_HOST = Config.getDefaultHost();
    private static final String DEFAULT_APP = Config.getDefaultApp();
    private static final int DEFAULT_SCOPE = PageContext.PAGE_SCOPE;
    private static final Type DEFAULT_TYPE = Type.STRING;
    private static final String DEFAULT_VAR = "config";

    // Attributes
    private String host = DEFAULT_HOST;

    private String app = DEFAULT_APP;

    private String key;

    private Type type = DEFAULT_TYPE;

    private String var;

    private int scope = DEFAULT_SCOPE;

    private String def;

    private boolean escapeXml = true;
    private boolean escapeXmlSet = false;


    public void setHost(String host) {
        this.host = host;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setType(String type) throws JspTagException {
        this.type = Type.forName(type);
        if (this.type == null) {
            throw new JspTagException("invalid type specified");
        }
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setScope(String scope) throws JspTagException {
        this.scope = getVarScope(scope);
    }

    public void setDefault(String def) {
        this.def = def;
    }

    public void setEscapeXml(boolean escapeXml) {
        this.escapeXml = escapeXml;
        this.escapeXmlSet = true;
    }

    public void release() {
        host = DEFAULT_HOST;
        app = DEFAULT_APP;
        type= DEFAULT_TYPE;
        scope = DEFAULT_SCOPE;
        key = null;
        var = null;
        def = null;
        escapeXml = true;
        escapeXmlSet = false;
    }

    /**
     * Prints out a string with the value of the requested configuration item.
     *
     * @throws JspException
     * @return <code>SKIP_BODY</code>, always.
     */
    public int doStartTag() throws JspException {
        boolean doEscape = true;
        if (escapeXmlSet) {
            doEscape = escapeXml;
        } else {
            doEscape = !TagUtilities.isEscapeCompatibilityMode(pageContext);
        }
        try {
            Config config = Config.getInstance();
            if (key != null) {
                if (type == Type.STRING) {
                    if (def == null) {
                        doOut(config.get(host, app, key), doEscape);
                    } else {
                        doOut(config.get(host, app, key, def), doEscape);
                    }
                } else if (type == Type.INT) {
                    if (def == null) {
                        doOut(new Integer(config.getInt(host, app, key)), 
                              doEscape);
                    } else {
                        int idef = Integer.parseInt(def);
                        doOut(new Integer(config.getInt(host, app, key, idef)),
                              doEscape);
                    }
                } else if (type == Type.FLOAT) {
                    if (def == null) {
                        doOut(new Float(config.getFloat(host, app, key)),
                              doEscape);
                    } else {
                        float fdef = Float.parseFloat(def);
                        doOut(new Float(config.getFloat(host, app, key, fdef)),
                              doEscape);
                    }
                } else if (type == Type.BOOLEAN) {
                    if (def == null) {                
                        doOut(new Boolean(config.getBoolean(host, app, key)),
                              doEscape);
                    } else {
                        boolean bdef = Boolean.valueOf(def).booleanValue();
                        doOut(new Boolean(config.getBoolean(host, app, key, 
                                                            bdef)),
                              doEscape);
                    }
                }
            } else {
                EscapeType escapeType = 
                    TagUtilities.findEscapeType(pageContext);
                pageContext.setAttribute((var != null) ? var : DEFAULT_VAR, 
                        new ConfigMap(config, host, app, type, def, 
                                      doEscape, escapeType), 
                        scope);
            }
        } catch (ConfigException e) {
            throw new JspTagException(e.toString());
        } catch (IOException e) {
            throw new JspTagException(e.toString());
        } catch (NumberFormatException e) {
            throw new JspTagException(e.toString());
        }
      
        return SKIP_BODY;
    }

    /**
     * Generates the output from this tag invocation.
     *
     * @param value The value to output.
     * @param escape The flag indicating whether XML escaping is needed.
     *
     * @throws IOException If an error occurs while writing to out.
     */
    private void doOut(Object value, boolean escape) throws IOException {
        if (var != null) {
            pageContext.setAttribute(var, value, scope);
        } else {
            String str = value.toString();
            if (escape) {
                str = TagUtilities.escapeXml(str, pageContext);
            }
            pageContext.getOut().print(str);
        }
    }
}
