/*
 * Copyright (c) 2002 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/jsp/tagext/Scope.java#1 $
 */

package com.opwvmsg.utils.paf.jsp.tagext;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.jsp.PageContext;

/**
 * This class is an enumeration of the scopes in which 
 * attributes can be set.
 *
 * @author Conrad Damon
 * @version $Revision: #1 $
 */
public class Scope {

    public static final Scope PAGE = 
            new Scope("page", PageContext.PAGE_SCOPE);
    public static final Scope REQUEST = 
            new Scope("request", PageContext.REQUEST_SCOPE);
    public static final Scope SESSION = 
            new Scope("session", PageContext.SESSION_SCOPE);
    public static final Scope APPLICATION = 
            new Scope("application", PageContext.APPLICATION_SCOPE);

    private static Map scopes = new HashMap();

    static {
        scopes.put(PAGE.toString(), PAGE);
        scopes.put(REQUEST.toString(), REQUEST);
        scopes.put(SESSION.toString(), SESSION);
        scopes.put(APPLICATION.toString(), APPLICATION);
    }

    /**
     * Get the <code>Scope</code> with the given name.
     *
     * @param name the name
     *
     * @return the <code>Scope</code> or <code>null</code> if none exists
     *     with this name
     */
    public static Scope forName(String name) {
        return (Scope) scopes.get(name);
    }


    /**
     * The name.
     */
    private final String name;

    /**
     * The page context constant for this scope.
     */
    private final int constant;


    /**
     * Create an instance with the given name.
     *
     * @param name the name
     * @param pageContextConstant the int value used be the 
     *    <code>PageContext</code> class to refer to this scope
     */
    private Scope(String name, int pageContextConstant) {
        this.name = name;
        this.constant = pageContextConstant;
    }


    /**
     * Get the constant used to refer to this scope by the
     * <code>PageContext</code> class.
     *
     * @return the int constant
     */
    public int getPageContextConstant() {
        return constant;
    }

    /**
     * Get the name of this instance.
     *
     * @return the name
     */
    public String toString() {
        return name;
    }
}
