/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/MimeInfo.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.mail.internet.ParameterList;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;
import com.opwvmsg.utils.paf.util.Utilities;

/**
 * This class encapsulates the data in an RME MimeInfo object. The MimeInfo
 * information is part of the overall MimeMsg object which itself is a subpart
 * of the Msg object.
 */
public class MimeInfo {
    private static final int SIMPLE_BODY_PART = 0;
    private static final int MULTI_BODY_PART = 1;
    private static final int MSG_BODY_PART = 2;
    
    protected static final String CONTENT_TYPE = "content-type";
    protected static final String CONTENT_DESCRIPTION = "content-description";
    protected static final String CONTENT_LANGUAGE = "content-language";
    protected static final String CONTENT_DISPOSITION = "content-disposition";
    protected static final String CONTENT_MD5 = "content-md5";
    protected static final String CONTENT_ID = "content-id";
    protected static final String CONTENT_TRANSFER_ENCODING = "content-transfer-encoding";
    protected static final String BOUNDARY = "boundary";
    
    // type is one of the 3 above constants
    private int type;
    private int headerOffset;
    private int bodyOffset; 
    private int headerLength; 
    private int totalSize; 
    private int numLines; 
    private int numHeaderLines = 0;
    private String mainType = "text";
    private String subType = "plain"; 
    private String contentId; 
    private String contentDesc; 
    private String boundary; 
    private String[] contentDisp; 
    private String[] contentLang;
    private String contentMD5;
    private String contentTranscoding; 
    private int numKeys;
    private Map keywords; // a hashmap of String[]
    private ParameterList typeParameters; // content-type parameters
    private ParameterList dispParameters; // content-disposition parameters
    // if this part is MultiMimePart, this will not be null
    private MimeInfo[] subParts = null;
    // if this part is MsgBodyPart, this will not be null
    private MimeMsg mimeMsg = null;

    /**
     * construct the MimeInfo from a message content.  This is a simple MIME parser tailored
     * to only getting the things that the RME MimeInfo needs.  It assumes three things:
     * - messages are properly CRLF line terminated
     * - boundaries are all well defined for multipart messages
     * - headers and bodies are separated by CRLFCRLF
     * Since the text stream is usually derived from the output of MimeMessage.writeTo
     * this should be OK.
     * 
     * @param content the array of bytes that makes up the message
     * @param offset the offset in the content array where this part starts
     * @param length the length of this body part
     */
    public MimeInfo(byte[] content, int offset, int length) {
        // find start of headers
        headerOffset = offset - 1;
        while (!Character.isLetterOrDigit((char)content[++headerOffset]));
        
        StringBuffer headerNameBuffer = new StringBuffer(20);
        StringBuffer headerValueBuffer = new StringBuffer(200);
        
        int[] numLinesArray = new int[1];
        numLinesArray[0] = 0;
        int currentPos = getNextHeader(content, headerOffset,
            headerNameBuffer, headerValueBuffer, numLinesArray);
        String headerName = headerNameBuffer.toString();
        String headerValue = headerValueBuffer.toString();
        keywords = new HashMap();
        while (headerName.length() > 0) {
            if (headerName.equals(CONTENT_TYPE)) {
                String []typeValues = Utilities.dsvToArray(headerValue, 
                    new char[] {';', '='}, true, new char[] {'\'', '\"'});
                typeValues[0] = unQuote(typeValues[0]);
                int typeSlashIndex = typeValues[0].indexOf('/');
                if (typeSlashIndex != -1) {
                    mainType = typeValues[0].substring(0,typeSlashIndex);
                    subType = typeValues[0].substring(typeSlashIndex + 1);
                } else {
                    mainType = typeValues[0];
                    subType = "";
                }
                for (int i = 1; i < typeValues.length; i += 2) {
                    typeValues[i] = unQuote(typeValues[i]);
                    typeValues[i + 1] = unQuote(typeValues[i + 1]);
                    
                    if (typeValues[i].equalsIgnoreCase(BOUNDARY)) {
                        boundary = typeValues[i + 1];
                    }
                    String[] seenValues = (String [])keywords.get(typeValues[i]);
                    if (seenValues == null) {
                        numKeys++;
                        keywords.put(typeValues[i], 
                            new String[] { typeValues[i + 1] } );
                    } else {
                        String[] newSeenValues = new String[seenValues.length + 1];
                        for (int j = 0; j < seenValues.length; j++) {
                            newSeenValues[j] = seenValues[j];                            
                        }
                        newSeenValues[seenValues.length] = typeValues[i + 1];
                        keywords.put(typeValues[i], newSeenValues);
                    }                        
                }
            } else if (headerName.equals(CONTENT_DISPOSITION)) {
                contentDisp = Utilities.dsvToArray(headerValue, 
                    new char[] {';', '='}, true, new char[] {'\'', '\"'});
                for (int i = 0; i < contentDisp.length; i++) {
                    contentDisp[i] = unQuote(contentDisp[i]);
                }
            } else if (headerName.equals(CONTENT_ID)) {
                contentId = unQuote(headerValue);
            } else if (headerName.equals(CONTENT_DESCRIPTION)) {
                contentDesc = unQuote(headerValue);
            } else if (headerName.equals(CONTENT_LANGUAGE)) {
                contentLang = Utilities.dsvToArray(headerValue, 
                    new char[] {';', '='}, true, new char[] {'\'', '\"'});
                for (int i = 0; i < contentLang.length; i++) {
                    contentLang[i] = unQuote(contentLang[i]);
                }
            } else if (headerName.equals(CONTENT_MD5)) {
                contentMD5 = unQuote(headerValue);
            } else if (headerName.equals(CONTENT_TRANSFER_ENCODING)) {
                contentTranscoding = unQuote(headerValue);
                /**
                 * Some MUAs send this value followed by white space,
                 * which will be compared in JavaMail. then it causes 
                 * mattching error, so trim it.
                 */
                if (contentTranscoding != null) {
                    contentTranscoding = contentTranscoding.trim();
                }
            }
            currentPos = getNextHeader(content, currentPos,
                  headerNameBuffer, headerValueBuffer, numLinesArray);
            headerName = headerNameBuffer.toString();
            headerValue = headerValueBuffer.toString();
        } 
        numHeaderLines = numLinesArray[0];
        headerLength = currentPos - offset;
        totalSize = length;
        bodyOffset = currentPos;
        numLinesArray[0] = 0;
        if (mainType.equalsIgnoreCase("multipart")) {
            type = MULTI_BODY_PART;
            ArrayList subPartsList = new ArrayList();
            int start = findNextBoundary(content, 
                offset + headerLength - 2, numLinesArray) + boundary.length() + 4;
            int nextBoundary = 0;
            if (start != -1) {
                do {
                    nextBoundary = findNextBoundary(content, start, numLinesArray);
                    if (nextBoundary != -1) {
                        subPartsList.add(new MimeInfo(content, start + 2, nextBoundary - start - 2));
                        start = nextBoundary + boundary.length() + 4;
                    } else {
                        // Missing end boundary; break out of the loop
                        break;
                    }
                } while (!isLastBoundary(content, start, numLinesArray));
            }
            numLines = numLinesArray[0] - 1;
            subParts = new MimeInfo[subPartsList.size()];
            subPartsList.toArray(subParts);
        } else if (mainType.equalsIgnoreCase("message")) {
            type = MSG_BODY_PART;
            mimeMsg = new MimeMsg(content, offset + headerLength, length - headerLength);
            MimeInfo msgInfo = mimeMsg.getMimeInfo();
            numLines = msgInfo.getNumHeaderLines() + msgInfo.getNumLines();
        } else {
            type = SIMPLE_BODY_PART;
            // calculate number of lines
            numLines = 0;
            currentPos = bodyOffset;
            while (currentPos < offset + length - 1) {
                if (content[currentPos] == '\r' && content[currentPos + 1] == '\n') {
                    numLines++;
                }
                currentPos++;
            }
            if (content[offset + length - 1] != '\n') {
                numLines++;
            }
        }
    }
    
    /**
     * Reads this mime info object from an RME stream.
     * 
     * @param inStream the rme input stream
     * @throws IOException on any IO error
     */
    public MimeInfo(RmeInputStream inStream) throws IOException {
        boolean haveIt = inStream.readBoolean();
        if (haveIt) {
            type = inStream.readInt();

            //mime data
            haveIt = inStream.readBoolean();
            if (haveIt) {
                headerOffset = inStream.readInt();
                bodyOffset = inStream.readInt();
                headerLength = inStream.readInt();
                totalSize = inStream.readInt();
                numLines = inStream.readInt();
                mainType = inStream.readString();
                subType = inStream.readString();
                contentId = inStream.readString();
                contentDesc = inStream.readString();
                boundary = inStream.readString();
                contentDisp = inStream.readStringArray();
                contentLang = inStream.readStringArray();
                contentMD5 = inStream.readString();

                contentTranscoding = inStream.readString();
                /**
                 * Some MUAs send this value followed by white space,
                 * which will be compared in JavaMail. then it causes 
                 * mattching error, so trim it.
                 */
                if (contentTranscoding != null) {
                    contentTranscoding = contentTranscoding.trim();
                }
                numKeys = inStream.readInt();
                keywords = new HashMap(numKeys);
                for (int i = 0; i < numKeys; i++) {
                    // for each key, read the parameters, and stick it in the
                    // list
                    String key = inStream.readString();
                    String[] params = inStream.readStringArray();
                    keywords.put(key, params);
                }
            }
            if (type == MULTI_BODY_PART) {
                int numParts = inStream.readInt();
                subParts = new MimeInfo[numParts];
                for (int i = 0; i < numParts; i++) {
                    subParts[i] = new MimeInfo(inStream);
                }
            } else if (type == MSG_BODY_PART) {
                mimeMsg = new MimeMsg(inStream);
            }
        }
    }
    
    /**
     * Writes this MimeInfo to an RME stream
     * 
     * @param outStream the RME stream
     * @throws IOException on any error
     */
    public void writeToStream(RmeOutputStream outStream) throws IOException {
        outStream.writeBoolean(true);
        outStream.writeInt(type);
        outStream.writeBoolean(true);
        outStream.writeInt(headerOffset);
        outStream.writeInt(bodyOffset);
        outStream.writeInt(headerLength);
        outStream.writeInt(totalSize);
        outStream.writeInt(numLines);
        outStream.writeString(mainType);
        outStream.writeString(subType);
        outStream.writeString(contentId);
        outStream.writeString(contentDesc);
        outStream.writeString(boundary);
        outStream.writeStringArray(contentDisp);
        outStream.writeStringArray(contentLang);
        outStream.writeString(contentMD5);
        outStream.writeString(contentTranscoding);
        outStream.writeInt(numKeys);
        if (numKeys > 0) {
            Iterator iter = keywords.keySet().iterator();
            while (iter.hasNext()) {
                String key = (String)iter.next();
                outStream.writeString(key);
                outStream.writeStringArray((String[])keywords.get(key));
            }
        }
        if (type == MULTI_BODY_PART) {
            outStream.writeInt(subParts.length);
            for (int i = 0; i < subParts.length; i++) {
                subParts[i].writeToStream(outStream);
            }
        } else if (type == MSG_BODY_PART) {
            mimeMsg.writeToStream(outStream);
        }
        
    }
    
    /**
     * Create a blank MimeInfo object.
     */
    protected MimeInfo() {
        mainType = "text";
        subType = "plain";
    }

    /**
     * True if this part is a multipart.
     */
    public boolean isMulti() {
        return type == MULTI_BODY_PART;
    }

    /**
     * True if this part is a message.
     */
    public boolean isMsg() {
        return type == MSG_BODY_PART;
    }
    
    /**
     * Get the number of lines in this message part.
     * 
     * @return number of lines
     */
    public int getNumLines() {
        return numLines;
    }

    /**
     * Get the total length of this part.
     * 
     * @return length
     */
    public int getTotalLength() {
        return totalSize;
    }

    /**
     * Get the header offset in this part.
     * 
     * @return header offest in bytes
     */
    public int getHeaderOffset() {
        return headerOffset;
    }

    /**
     * Get the body offset in this part.
     * 
     * @return body offest in bytes
     */
    public int getBodyOffset() {
        return bodyOffset;
    }

    /**
     * Get the header length of this part.
     * 
     * @return header size in bytes
     */
    public int getHeaderLength() {
        return headerLength;
    }

    /**
     * Get the main content type of this part (i.e "text" of
     * "text/plain").
     * 
     * @return main content type
     */
    public String getMainContentType() {
        return mainType;
    }

    /**
     * Get the sub content type of this part (i.e "plain" of
     * "text/plain").
     * 
     * @return sub content type
     */
    public String getSubContentType() {
        return subType;
    }
    
    /**
     * Get the number of keywords in this message.
     * 
     * @return number of keywords
     */
    public int getNumKeywords() {
        return numKeys;
    }
    
    /**
     * Get parameters associated with the content type of this
     * message part.
     * 
     * @return Map of keywords to String[] values
     */
    public Map getKeywords() {
        return keywords;
    }

    /**
     * Get parameters associated with the content type of this
     * message part.
     *
     * @return ParameterList of content type parameters
     */
    public ParameterList getTypeParameters() {
        if (typeParameters == null) {
            typeParameters = new ParameterList();
            if (keywords != null) {
                Iterator keys = keywords.keySet().iterator();
                while (keys.hasNext()) {
                    String key = (String)keys.next();
                    String[] value = (String[])keywords.get(key);
                    if (value.length > 0) {
                        typeParameters.set(key, value[0]);
                    }
                }
            }
        }
        return typeParameters;
    }
    
    /**
     * Get the number of sub-parts in this message part.
     * 
     * @return number of parts
     */
    public int getNumParts() {
        return subParts != null ? subParts.length : 0;
    }
    
    /**
     * Get the info for the sub-parts in this message part.
     * 
     * @return array of info for parts
     */
    public MimeInfo[] getParts() {
        return subParts;
    }
    
    /**
     * Get the content disposition for this message part.
     * 
     * @return content disposition
     */
    public String getContentDisposition() {
        if (contentDisp != null && contentDisp.length > 0) {
            return contentDisp[0];
        } else {
            return null;
        }
    }

    /**
     * Get parameters associated with the content disposition of this
     * message part.
     *
     * @return ParameterList of content disposition parameters
     */
    public ParameterList getDispParameters() {
        if (dispParameters == null) {
            dispParameters = new ParameterList();
            if (contentDisp != null) {
                for (int i = 1; i + 1 < contentDisp.length; i += 2) {
                    dispParameters.set(contentDisp[i], contentDisp[i+1]);
                }
            }
        }
        return dispParameters;
    }
    
    /**
     * Get the content transfer encoding for this message part.
     * 
     * @return String content transfer encoding
     */
    public String getContentTranscoding() {
        return contentTranscoding;
    }
    
    /**
     * Get the content id for this message part.
     * 
     * @return String content id
     */
    public String getContentID() {
        return contentId;
    }
    
    /**
     * Get the content MD5 for this message part.
     * 
     * @return String content MD5
     */
    public String getContentMD5() {
        return contentMD5;
    }
    
    /**
     * Get the content language array for this message part.
     * 
     * @return String[] content language 
     */
    public String[] getContentLanguage() {
        return contentLang != null && contentLang.length > 0 ?
                contentLang : null;
    }
    
    /**
     * Get the content description for this message part.
     * 
     * @return String content description
     */
    public String getContentDescription() {
        return contentDesc;
    }

    /**
     * Debugging function to dump the contents of this object to stdout
     *  
     */
    public void dump() {
        System.out.println("============= BEGIN MIME DATA ==========");
        System.out.println("Type: " + type);
        System.out.println("HdrOff: " + headerOffset);
        System.out.println("BodyOff: " + bodyOffset);
        System.out.println("HdrLen: " + headerLength);
        System.out.println("Total Size: " + totalSize);
        System.out.println("NumLines: " + numLines);
        System.out.println("Maintype: " + mainType);
        System.out.println("subtype: " + subType);
        System.out.println("contentId: " + contentId);
        System.out.println("contentDesc: " + contentDesc);
        System.out.println("boundary: " + boundary);

        System.out.println("contentDisp: " + dumpStringArray(contentDisp));
        System.out.println("contentLang: " + dumpStringArray(contentLang));

        System.out.println("md5: " + contentMD5);
        System.out.println("transcoding: " + contentTranscoding);
        System.out.println("Keywords: " + keywords.size());
        Iterator iter = keywords.keySet().iterator();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            System.out.println(key + ": "
                    + dumpStringArray((String[]) keywords.get(key)));
        }

        if (type == MULTI_BODY_PART) {
            int numParts = subParts.length;
            System.out.println("Dumping " + numParts + " subParts");
            for (int i = 0; i < numParts; i++) {
                subParts[i].dump();
            }
        } else if (type == MSG_BODY_PART) {
            System.out.println("Dumping Email Parts");
            mimeMsg.dump();
        }
        System.out.println("============= DONE MIME DATA ==========");
    }

    /**
     * A helper function to convert a string array to a comma separated string.
     * 
     * @param array the string array to process
     * @return the resulting combined string
     */
    private String dumpStringArray(String[] array) {
        String out = "";
        if (array == null || array.length == 0) {
            out += "EMPTY";
            return out;
        }
        for (int i = 0; i < array.length; i++) {
            out += i + ": " + array[i] + ", ";
        }
        return out;
    }
    
    /**
     * Trims whitespace and removes quotes surrounding the string.
     * 
     * @param in the string to clean up
     * @return the unquoted and trimmed string
     */
    protected static String unQuote(String in) {
        String quoted = in.trim();
        if (quoted.length() > 1 &&
                ((in.charAt(0) == '\"' && 
                        in.charAt(quoted.length() - 1) == '\"') ||
                        (in.charAt(0) == '\'' && 
                                in.charAt(quoted.length() - 1) == '\''))) {
            return quoted.substring(1,quoted.length() - 1);
        }
        return quoted;
    }
    
    /**
     * Gets the next header from the contest array starting at the current offset.
     * The StringBuffers headerName and headerValue will be populated with the results
     * and the new offset into the byte array is returned.
     * 
     * @param content the message text byte array
     * @param offset the current location where we're reading
     * @param headerName a buffer to hold the header name
     * @param headerValue a buffer to hold the header value
     * @param numLines (optional) an int[] where the first element will be
     *        increased by the number of lines encountered 
     * @return the current position in the content after reading this header
     */
    protected static int getNextHeader(byte[] content, int offset, StringBuffer headerName,
                              StringBuffer headerValue, int[] numLines) {
        headerName.setLength(0);
        headerValue.setLength(0);
        int currentPos = offset;
        boolean foundName = false;
        while (currentPos < content.length) {
            if (content.length > currentPos + 1 &&
                    content[currentPos] == '\r' && content[currentPos + 1] == '\n') {
                if (currentPos == offset) {
                    // leading CRLF is delimiter of header and body
                	currentPos += 2;
                	break;
                }
            	if (numLines != null) {
                    numLines[0]++;
                }
                if (content.length > currentPos + 2 &&
                        (content[currentPos + 2] == '\t' ||
                        content[currentPos + 2] == ' ')) {
                    if (!foundName) {
                        headerName.append(' ');
                    } else {
                        headerValue.append(' ');
                    }
                    currentPos += 3;
                    while (currentPos < content.length && (content[currentPos] == ' ' || content[currentPos] == '\t')) {
                        //skip all other whitespace chars
                        currentPos++;
                    }
                    continue;
                }
                currentPos += 2;
                break;
            }
            if (!foundName) {
                if (content[currentPos] == ':') {
                    foundName = true;
                } else {
                    headerName.append(Character.toLowerCase((char)content[currentPos]));
                }
            } else {
                headerValue.append((char)content[currentPos]);
            }
            currentPos++;
        }
        
        return currentPos;
    }

    /**
     * Finds the next MIME boundary starting from the offset point
     * @param content the message text byte array
     * @param offset the current location where we're reading
     * @param numLines (optional) an int[] where the first element will be
     *        increased by the number of lines encountered 
     * @return the position in the content *preceding* the boundary and previous CRLF
     */
    private int findNextBoundary(byte[] content, int offset, int[] numLines) {
        int currentPos = offset;
        int start = 0;
        String boundarySearch = "\r\n--" + boundary;
        int boundaryLength = boundarySearch.length();
        while (currentPos < content.length) {
            int matchPos = 0;
            start = currentPos;
            while (content[currentPos++] == boundarySearch.charAt(matchPos++)) {
                if (matchPos == 2) {
                    numLines[0]++;
                }
                if (currentPos == content.length) {
                    break;
                }
                if (matchPos == boundaryLength) {
                    return start;
                }
            }
            if (matchPos != 1) {
                // if we were matching, and failed, start again at the failed character
                currentPos --;
            }
        }
        return -1;
    }
    
    /**
     * Returns the number of header lines including the blank line.
     * This is only filled in when parsing, and not from the RME.
     * It is only used to aid the caclulation of total number of lines.
     * 
     * @return number of header lines
     */
    private int getNumHeaderLines() {
        return numHeaderLines;
    }
    
    /**
     * Returns true if the position of the offset in the content is at the 
     * end of the final boundary (i.e. checks for --\r\n after the last boundary
     * found)
     * 
     * @param content the message text byte array
     * @param offset the current location where we're reading
     * @param numLines (optional) an int[] where the first element will be
     *        increased by the number of lines encountered 
     * @return true if this was the final boundary
     */
    private boolean isLastBoundary(byte[] content, int offset, int[] numLines) {
        if (offset == -1) {
            return true;
        }
        if (content.length >= offset + 4 &&
                content[offset] == '-' &&
                content[offset + 1] == '-' &&
                content[offset + 2] == '\r' &&
                content[offset + 3] == '\n') {            
            numLines[0]++;
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("============= BEGIN MIME INFO ==========");
        buffer.append("\n");
        buffer.append("Type: ").append(type);
        buffer.append("\n");
        buffer.append("HdrOff: ").append(headerOffset);
        buffer.append("\n");
        buffer.append("BodyOff: ").append(bodyOffset);
        buffer.append("\n");
        buffer.append("HdrLen: ").append(headerLength);
        buffer.append("\n");
        buffer.append("Total Size: ").append(totalSize);
        buffer.append("\n");
        buffer.append("NumLines: ").append(numLines);
        buffer.append("\n");
        buffer.append("Maintype: ").append(mainType);
        buffer.append("\n");
        buffer.append("subtype: ").append(subType);
        buffer.append("\n");
        buffer.append("contentId: ").append(contentId);
        buffer.append("\n");
        buffer.append("contentDesc: ").append(contentDesc);
        buffer.append("\n");
        buffer.append("boundary: " + boundary);
        buffer.append("\n");

        buffer.append("contentDisp: ").append(dumpStringArray(contentDisp));
        buffer.append("\n");
        buffer.append("contentLang: ").append(dumpStringArray(contentLang));
        buffer.append("\n");

        buffer.append("md5: " + contentMD5);
        buffer.append("\n");
        buffer.append("transcoding: ").append(contentTranscoding);
        buffer.append("\n");
        buffer.append("Keywords: ").append(
                keywords != null ? keywords.size() :null);
        buffer.append("\n");
        if (keywords != null) {
            Iterator iter = keywords.keySet().iterator();
            while (iter.hasNext()) {
                String key = (String) iter.next();
                buffer.append(key).append(": ")
                        .append(dumpStringArray((String[]) keywords.get(key)));
                buffer.append("\n");
            }
        }

        if (type == MULTI_BODY_PART) {
            if (subParts != null) {
                int numParts = subParts.length;
                buffer.append("Dumping ").append(numParts).append(" subParts");
                buffer.append("\n");
                for (int i = 0; i < numParts; i++) {
                    subParts[i].toString();
                }
            }
        } else if (type == MSG_BODY_PART) {
            buffer.append("Dumping Email Parts");
            buffer.append("\n");
            mimeMsg.toString();
        }
        buffer.append("============= END MIME INFO  ==========");
        buffer.append("\n");
        return buffer.toString();
    }
}
