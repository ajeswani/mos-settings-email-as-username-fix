/*
 * Copyright (c) 2005 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/filters/CloseFilter.java#1 $
 */

package com.opwvmsg.utils.paf.filters;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;
   
import org.apache.log4j.Logger;

/**
 * This is an implementation of a simple servlet filter for
 * releasing resources after request processing.
 * </p>
 * A <i>closeable</i> object can be registered with a request by
 * using the <code>addCloseable</code> class method.
 * </p>
 * A <i>closeable</i> object must implement
 * <code>CloseFilter.Closeable</code>.
 * </p>
 * The <b>com.openwave.paf.filters.closeables</b> request attribute
 * contains a <code>Set</code> of <i>closeable</i> objects.
 * </p>
 * Given the nature of this filter all instances of
 * <code>Throwable</code> generated during processing of
 * <i>closeables</i> are ignored.
 *
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
public final class CloseFilter implements Filter {

    private static final Logger logger = Logger.getLogger(CloseFilter.class);

    private static final String REQUEST_CLOSEABLES =
            "com.openwave.paf.filters.closeables";

    /**
     * All <i>closeable</i> objects must implement this interface.
     */
    public interface Closeable {
        /**
         * Releases any system resources associated with the
         * implementing object.
         */
        public void close();
    }

    public void init(FilterConfig config) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) 
            throws ServletException, IOException {

        try {
            chain.doFilter(request, response);
        } finally {
            processCloseables(request);
        }
    }

    public void destroy() {
    }

    /**
     * Process the closeables in the given request.
     *
     * @param request servlet request which contains a set of
     *       closeables to process
     */
    protected void processCloseables(ServletRequest request) {
        synchronized (request) {
            try {
                processCloseables(
                        (Set)request.getAttribute(REQUEST_CLOSEABLES));
            } catch (Throwable ignore) {
                logger.debug("ignoring: " + ignore.getMessage(), ignore);
            } finally {
                request.removeAttribute(REQUEST_CLOSEABLES);
            }
        }
    }

    /**
     * Process the given set of closeables.  Each member of the set
     * must implement <code>CloseFilter.Closeable</code>.
     *
     * @param closeables a <code>Set</code> of closeable objects
     */
    protected void processCloseables(Set closeables) {
        if (closeables != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("closeables.size(): " + closeables.size());
            }
            Iterator objects = closeables.iterator();
            while (objects.hasNext()) {
                try {
                    CloseFilter.Closeable closeable =
                            (CloseFilter.Closeable)objects.next();
                    if (logger.isDebugEnabled()) {
                        logger.debug(closeable + ": attempt to close");
                    }
                    closeable.close();
                    if (logger.isDebugEnabled()) {
                        logger.debug(closeable + ": closed");
                    }
                } catch (Throwable ignore) {
                    logger.debug("ignoring: " + ignore.getMessage(), ignore);
                }
            }
        }
    }

    /**
     * Add the given <i>closeable</i> object to the given request.
     * </p>
     * A <i>closeable</i> object can be registered with a request
     * using the <code>addCloseable</code> class method.
     *
     * @param request the servlet request which should close this object
     * @param closeable the <i>closeable</i> object to add to the given request
     */
    public static void addCloseable(
            ServletRequest request, Closeable closeable) {
        synchronized (request) {
            Set closeables = (Set)request.getAttribute(REQUEST_CLOSEABLES);
            if (closeables == null) {
                closeables = new HashSet();
                request.setAttribute(REQUEST_CLOSEABLES, closeables);
            }
            if (closeables.add(closeable)) {
                if (logger.isDebugEnabled()) {
                    logger.debug(closeable + ": add to closeables");
                }
            }
        }
    }
}
