/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.http;

import java.util.Random;

/**
 * This class subclass helps manage multiple mss instances per host. It
 * implements the round-robin port shuffling for each host, and maintains a list
 * of pools for each instance.
 * 
 * @author mxos-dev
 */
public class HTTPSockPoolInstances {
	int basePort;
	int numPorts;
	HTTPConnectionPool[] httpPools;
	int curPortOffset;

	/**
	 * If server uses more than one port, assigns new sockets in a round robin
	 * fashion to different ports
	 * 
	 * @return the next port to use.
	 * 
	 */
	public synchronized int getNextPortOffset() {
		if (numPorts == 1) {
			return 0;
		} else {
			// increment curPortOffset, and mod it
			curPortOffset = (curPortOffset + 1) % numPorts;
			return curPortOffset;
		}
	}

	/**
	 * Given a port number, returns the next port in use for this mss (useful
	 * for failover.) not tied to the round robin distribution of ports like the
	 * above function.
	 * 
	 * @param portNum
	 *            the current port number
	 * @return the next port number in use
	 */
	public int getNextPort(final int portNum) {
		if (numPorts == 1) {
			return basePort;
		} else {
			// increment curPortOffset, and mod it
			return ((portNum - basePort) + 1) % numPorts;
		}
	}

	/**
	 * Initialize this object.
	 * 
	 * @param basePortIn
	 *            Mss base port
	 * @param numPortsIn
	 *            number of Mss ports (instances) on this host
	 * @param sockPoolsIn
	 *            an array of socket pools for each instance
	 */
	HTTPSockPoolInstances(final int basePortIn, final int numPortsIn,
			final HTTPConnectionPool[] sockPoolsIn) {
		basePort = basePortIn;
		numPorts = numPortsIn;
		httpPools = sockPoolsIn;
		curPortOffset = (new Random()).nextInt(numPorts);
	}

	/**
	 * Gets the next pool to use, assigned in round-robin fashion
	 * 
	 * @return the next SocketPool to use
	 */
	public HTTPConnectionPool getNextPool() {
		return httpPools[getNextPortOffset()];
	}

	/**
	 * Gets a specific pool from these instances.
	 * 
	 * @param portNum
	 *            the port number of the specific instance
	 * @return SocketPool with connections to that port
	 */
	public HTTPConnectionPool getSpecificPool(final int portNum) {
		if (portNum < basePort || portNum >= basePort + numPorts) {
			return null;
		}
		return httpPools[portNum - basePort];
	}

	/**
	 * Gets scoketPools associated with this instance
	 * 
	 * @return SocketPool array associated with this instance
	 */
	HTTPConnectionPool[] getSockPoolsArray() {
		return httpPools;
	}

	public int getNumPorts() {
		return numPorts;
	}

	public void setNumPorts(int numPorts) {
		this.numPorts = numPorts;
	}

	public int getBasePort() {
		return basePort;
	}

	public void setBasePort(int basePort) {
		this.basePort = basePort;
	}

	public int getCurPortOffset() {
		return curPortOffset;
	}

	public void setCurPortOffset(int curPortOffset) {
		this.curPortOffset = curPortOffset;
	}

	public HTTPConnectionPool[] getHttpPools() {
		return httpPools;
	}

	public void setHttpPools(HTTPConnectionPool[] httpPools) {
		this.httpPools = httpPools;
	}
}