/*
 * Copyright (c) 2005 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/Regex.java#1 $
 */
/*
 * Id: Perl5Compiler.java,v 1.21 2003/11/07 20:16:25 dfs Exp 
 *
 * ====================================================================
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2000 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Apache" and "Apache Software Foundation", "Jakarta-Oro" 
 *    must not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache" 
 *    or "Jakarta-Oro", nor may "Apache" or "Jakarta-Oro" appear in their 
 *    name, without prior written permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */
package com.opwvmsg.utils.paf.util;

/**
 * Perl5 regular expression utilities.
 *
 * @version @version@
 */

public final class Regex {

    /**
     * Given a character string, returns a Perl5 expression that interprets
     * each character of the original string literally.  In other words, all
     * special metacharacters are quoted/escaped.  This method is useful for
     * converting user input meant for literal interpretation into a safe
     * regular expression representing the literal input.
     * <p>
     * In effect, this method is the analog of the Perl5 quotemeta() builtin
     * method.
     * <p>
     * @param expression The expression to convert.
     * @return A String containing a Perl5 regular expression corresponding to
     *         a literal interpretation of the pattern.
     */
    public static final String quotemeta(char[] expression) {
        int ch;
        StringBuffer buffer;

        buffer = new StringBuffer(2*expression.length);
        for(ch = 0; ch < expression.length; ch++) {
            if(!OpCode._isWordCharacter(expression[ch])) {
                buffer.append('\\');
            }
            buffer.append(expression[ch]);
        }

        return buffer.toString();
    }

    /**
     * Given a character string, returns a Perl5 expression that interprets
     * each character of the original string literally.  In other words, all
     * special metacharacters are quoted/escaped.  This method is useful for
     * converting user input meant for literal interpretation into a safe
     * regular expression representing the literal input.
     * <p>
     * In effect, this method is the analog of the Perl5 quotemeta() builtin
     * method.
     * <p>
     * @param pattern The pattern to convert.
     * @return A String containing a Perl5 regular expression corresponding to
     *         a literal interpretation of the pattern.
     */
    public static final String quotemeta(String expression) {
        return quotemeta(expression.toCharArray());
    }
}
