/*
 * Copyright (c) 2004 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/filters/CacheFilter.java#1 $
 */
package com.opwvmsg.utils.paf.filters;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.util.BasicCache;
import com.opwvmsg.utils.paf.util.Cache;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * This filter is responsible for ensuring that the correct
 * caching control information is sent with every response.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class CacheFilter implements Filter {

    private static final Logger log = Logger.getLogger(CacheFilter.class);

    private static final int SECONDS_IN_DAY = 86400;

    /**
     * web.xml init parameter specifying what config.db key to use to get
     * the cache expiration time; use {@value} in web.xml. The value of
     * the config.db entry specifies the expiration time in days. <p>
     */
    public static final String EXPTIME_PARAM = "responseExpirationTimeKey";

    /**
     * web.xml init parameter specifiying which config.db key to use to get
     * the suffixes for which caching is allowedl use {@value} in web.xml.
     * The value of the config.db entry specifies the pages to be cached,
     * separated by "|". <p>
     */
    public static final String CACHE_ALLOWED_PARAM = "responseCacheAllowedKey";

    /**
     * The config.db key to use to get the Cache-Control parameter.
     * The value of the config.db entry is used default Cache-Control's
     *  parameter. <p>
     */
    public static final String CACHE_CONTROL = "responseCacheControl";

    /**
     * The config.db key to use to get the default Pragma parameter.
     * The value of the config.db entry is used default Pragma's parameter. <p>
     */
    public static final String PRAGMA = "responsePragma";

    private FilterConfig config;

    /**
     * Cache of previously checked request paths.
     */
    private Cache pathCache;

    /**
     * The expiration time in seconds.
     */
    private int expTime;

    /**
     * The expiration time in milliseconds.
     */
    private long expTimeMillis;

    /**
     * The path suffixes that indicate caching is enabled.
     */
    private String[] patterns;

    /**
     * The Cache-Control parameter of HTTP response.
     */
    private String cacheControl = "no-store";

    /**
     * The Pragma parameter of HTTP response.
     */
    private String pragma = "no-cache";

    /**
     * Initializes.
     *
     * @param config the filter configuration
     */
    public void init(FilterConfig config) {
        this.config = config;
        pathCache = new BasicCache();
        String expTimeDays = null;
        Config cfg = null;

        String configCacheAllowed =
                config.getInitParameter(CACHE_ALLOWED_PARAM);
        String configExpirationTime =
                config.getInitParameter(EXPTIME_PARAM);

        try {
            cfg = Config.getInstance();
            expTimeDays = cfg.get(configExpirationTime);
        } catch (ConfigException e) {
            log.error("Response expiration time cannot be read from config.");
        }
        if (expTimeDays != null) {
            try {
                int days = Integer.parseInt(expTimeDays);
                expTime = days*SECONDS_IN_DAY;
                expTimeMillis = expTime*1000;
                String patternString = cfg.get(configCacheAllowed);
                patterns = StringUtils.split(patternString, "|");
            } catch (NumberFormatException e) {
                log.error("Response expiration time '" + expTimeDays +
                          "' is not a valid integer.");
            } catch (ConfigException e) {
                log.error("Cache allowed patterns cannot be read from config.");
            }
        }
        if (patterns == null) {
            patterns = new String[0];
        }

        try {
            cacheControl = cfg.get(CACHE_CONTROL);
        } catch (ConfigException e) {
            log.error("Response Cache-Control cannot be read from config.");
        }
        try {
            pragma = cfg.get(PRAGMA);
        } catch (ConfigException e) {
            log.error("Response Pragma cannot be read from config.");
        }
    }

    /**
     * Cleans up when the filter is taken out of service.
     */
    public void destroy() {
        config = null;
        pathCache = null;
        patterns = null;
    }

    /**
     * Returns the configuration.
     *
     * @return the filter configuration
     */
    public FilterConfig getFilterConfig() {
        return config;
    }

    /**
     * Filters the request.
     *
     * @param request the request
     * @param response the response
     * @param chain the filter chain
     *
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse)response;
        String servletPath = httpRequest.getServletPath();
        String pathInfo = httpRequest.getPathInfo();
        String requestPath = (servletPath != null ? servletPath : "")
                             + (pathInfo != null ? pathInfo : "");

        if (isCachingEnabled(requestPath)) {
            setCacheControls(httpResponse);
        } else {
            setDefaultCacheControls(httpResponse);
        }

        chain.doFilter(request, response);
    }

    /**
     * Checks if a page matches any of the cache allowed
     * suffix patterns.
     *
     * @return <code>true</code> if the page allows caching,
     *   else <code>false</code>
     */
    private boolean isCachingEnabled(String path) {
        Boolean result = (Boolean) pathCache.get(path);
        if (result == null) {
            for (int i = 0; i < patterns.length; i++) {
                if (path.endsWith(patterns[i])) {
                    result = Boolean.TRUE;
                    break;
                }
            }
            if (result == null) {
                result = Boolean.FALSE;
            }
            pathCache.put(path, result);
        }
        return result.booleanValue();
    }

    private void setCacheControls(HttpServletResponse response) {
        response.setHeader("Cache-Control", "max-age=" + expTime);
        response.setDateHeader("Expires", System.currentTimeMillis()
                               + expTimeMillis);
    }

    private void setDefaultCacheControls(HttpServletResponse response) {
        if (cacheControl.length() != 0)
            response.setHeader("Cache-Control", cacheControl);
        if (pragma.length() != 0)
            response.setHeader("Pragma", pragma);
    }
}
