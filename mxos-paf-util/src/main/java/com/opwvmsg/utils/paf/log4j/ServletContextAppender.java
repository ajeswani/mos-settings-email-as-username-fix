/*
 *      Copyright (c) 2002 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/log4j/ServletContextAppender.java#1 $
 *
 */

package com.opwvmsg.utils.paf.log4j;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;

import javax.servlet.ServletContext;


/**
 * This appender is used to append to the servlet context log.
 *
 * The configuration for logging is specified in an XML file that is
 * named by the servlet context parameter "log4j-config" or located
 * in the default location "/WEB-INF/log4j-config.xml" (see
 * <code>Log4jListener</code>).  To make use of this appender, 
 * an application will define the appender in the configuration file.
 *
 * For example:
 * <pre>
 * &lt;appender name=&quot;SERVLET&quot; class=&quot;com.openwave.paf.log4j.ServletContextAppender&quot;&gt;
 *   &lt;layout class&quot;org.apache.log4j.PatternLayout&quot;&gt;
 *     &lt;param name=&quot;ConversionPattern&quot;
 *               value=&quot;%d %-5p [%t] %C{2} (%F:%L) - %m%n&quot;/&gt;
 *   &lt;/layout&gt;
 * &lt;/appender&gt;
 * </pre>
 *
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
public class ServletContextAppender extends AppenderSkeleton {

    /**
     * A reference to the current servlet context.
     */
    private static ServletContext context;

    /**
     * Set the servlet context for this appender.
     */
    static void setServletContext(ServletContext servletContext) {
        context = servletContext;
    }

    /**
     * Get the servlet context for this appender.
     */
    static ServletContext getServletContext() {
        return context;
    }

    /**
     * Does nothing.
     */
    public void activateOptions() {
    }

    /**
     * Formats and writes the event to the servlet context log if it
     * is non-null and prints to System.err otherwise.
     */
    public void append(LoggingEvent event) {
        if (context == null) {
            System.err.println(getLayout().format(event));
        } else {
            ThrowableInformation throwableInfo =
                    event.getThrowableInformation();
            if (throwableInfo != null) {
                context.log(getLayout().format(event),
                        throwableInfo.getThrowable());
            } else {
                context.log(getLayout().format(event));
            }
        }
    }

    /**
     * This appender requires a layout so this always returns true.
     */
    public boolean requiresLayout() {
        return true;
    }

    /**
     * Does nothing.
     */
    public void close() {
    }
}
