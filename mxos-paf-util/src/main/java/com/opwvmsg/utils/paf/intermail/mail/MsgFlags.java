/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.util.BitSet;

/**
 * This class encapsulates data for IMAP Flags.
 */
public class MsgFlags {

    private final static int FLAGS_TO_STORE = 6;

    // internal
    private int flagInt;
    private BitSet flagBitSet;

    /**
     * Message flag order.
     */
    public static enum MsgFlagIndex {
        flagrecent, flagseen, flagans, flagflagged, flagdraft, flagdel;
    }

    /**
     * Constructor an empty Flags object It clears all the flags
     */
    public MsgFlags() {
        flagBitSet = new BitSet(FLAGS_TO_STORE + 1);
    }

    /**
     * It clears all the flags
     * 
     */
    public void ClearFlags() {
        flagBitSet.clear();
    }

    /**
     * It sets a particular flag
     * 
     * @param flag of type MsgFlagIndex
     * @return the flagInt
     */
    public void SetFlag(MsgFlagIndex flag, boolean value) {
        switch (flag) {
        case flagrecent:
            flagBitSet.set(MsgFlagIndex.flagrecent.ordinal(), value);
            break;
        case flagseen:
            flagBitSet.set(MsgFlagIndex.flagseen.ordinal(), value);
            break;
        case flagans:
            flagBitSet.set(MsgFlagIndex.flagans.ordinal(), value);
            break;
        case flagflagged:
            flagBitSet.set(MsgFlagIndex.flagflagged.ordinal(), value);
            break;
        case flagdraft:
            flagBitSet.set(MsgFlagIndex.flagdraft.ordinal(), value);
            break;
        case flagdel:
            flagBitSet.set(MsgFlagIndex.flagdel.ordinal(), value);
            break;
        default:
            flagBitSet.clear();
            break;
        }
    }

    /**
     * Returns the integer value of the Flags which can be used for RME call
     * 
     * @return the flagInt
     */
    public int getFlagInt() {
        flagInt = 0;
        for (int i = 0; i < FLAGS_TO_STORE; i++) {
            flagInt += flagBitSet.get(i) ? (1 << i) : 0;
        }
        return flagInt;
    }

    /**
     * Returns the array of flags from integer value 
     * 
     * @return bits
     */
    public static boolean[] getMsgFlags(int flags) {
        boolean[] bits = new boolean[FLAGS_TO_STORE];
        for (int i = FLAGS_TO_STORE-1; i >= 0; i--) {
            bits[i] = (flags & (1 << i)) != 0;
        }
        return bits;
    }
}
