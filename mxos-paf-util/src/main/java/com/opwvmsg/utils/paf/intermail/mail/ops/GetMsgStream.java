/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation will fetch the message blob from the MSS.
 */
public class GetMsgStream extends AbstractMssOperation {

    // input
    private String name;
    private UUID folderUUID;
    private UUID msgUUID;
    private int options;

    // constants
    public static final int FETCH_MSG_BODY = 0x00;
    public static final int FETCH_MSG_HEADER_SUMMARY = 0x01;
    public static final int FETCH_MIME_AS_TEXT = 0x02;

    // output
    private byte[] msgBytes;
    private int numBytes;

    // internal
    private String url;
    private String hostHeader;

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID Folder from which we want list of messages.
     * @param msgUUIDs Only one msgUUID as part of this parameter.
     * @param options This field determines whether to fetch message body or
     *            header summary. Following values are supported for this
     *            parameter: 0x00 - Fetch message body. 0x01 - Fetch message
     *            header summary.
     */
    public GetMsgStream(String host, String name, String realm,
            UUID folderUUID, UUID msgUUID, int options) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_GETMSGSTREAM);
        this.folderUUID = folderUUID;
        this.msgUUID = msgUUID;
        this.options = options;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (folderUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "folderUUID", "GetMsgStream" }));
        }
        if (msgUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "msgUUID", "GetMsgStream" }));
        }
        if ((options != FETCH_MSG_BODY)
                && (options != FETCH_MSG_HEADER_SUMMARY)) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "options", "GetMsgStream" }));
        }
        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_SL_GETMSGSTREAM", host,
                            "dataModel" + rmeDataModel }));
        } else {
            callRme(MSS_SL_GETMSGSTREAM);
        }

    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_GETMSGSTREAM));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeUUID(folderUUID);
        outStream.writeUUID(msgUUID);
        outStream.writeInt(options);

        outStream.flush();
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        numBytes = inStream.readInt();
        logEvents = inStream.readLogEvent();
        this.msgBytes = new byte[numBytes];
        inStream.read(msgBytes, 0, numBytes);
    }

    /**
     * @return the msgBytes
     */
    public byte[] getMsgBytes() {
        return msgBytes;
    }

    /**
     * @return the numBytes
     */
    public int getNumBytes() {
        return numBytes;
    }
}
