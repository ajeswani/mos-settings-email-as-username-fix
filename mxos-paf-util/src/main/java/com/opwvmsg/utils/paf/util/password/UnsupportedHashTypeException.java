/*
 * Copyright (c) 2003 Openwave Systems Inc.  All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/UnsupportedHashTypeException.java#1 $
 */

package com.opwvmsg.utils.paf.util.password;

/**
 * This exception occurs when a provider does not support a given hash type.
 */
public class UnsupportedHashTypeException extends PasswordException {

    public UnsupportedHashTypeException() {
        super();
    }

    public UnsupportedHashTypeException(String s) {
        super(s);
    }

    public UnsupportedHashTypeException(Throwable e) {
        super(e);
    }

    public UnsupportedHashTypeException(String s, Throwable e) {
        super(s, e);
    }
}
