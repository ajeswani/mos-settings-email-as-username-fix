/*  
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 */
package com.opwvmsg.utils.paf.intermail.mail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

import javax.mail.MessagingException;
import javax.mail.internet.InternetHeaders;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;
import com.opwvmsg.utils.paf.util.Utilities;

/**
 * This class encapsulates all of the data returned from an RME Msg object.
 */
public class DocIdInfo {
    private int uid;
    private String msgId;
    private String folderPath;

    /**
     * Constructor for a placeholder Msg with no data
     * 
     * @param uid the uid of the message
     */
    public DocIdInfo(int uid, String msgId, String folderPath) {
        this.uid = uid;
        this.msgId = msgId;
        this.folderPath = folderPath;
    }

    /**
     * Reads this DocIdInfo object from an RME stream
     * 
     * @param inStream the rme input stream
     * @throws IOException on any IO error
     */
    public DocIdInfo(RmeInputStream inStream) throws IOException {
        boolean haveIt = inStream.readBoolean();
        if (haveIt) {
            msgId = inStream.readString();
            folderPath = inStream.readString();
            uid = inStream.readInt(); 
        }
    }

    /**
     * Gets the folderpath
     * @return folderPath
     */
    public String getFolderPath() {
        return folderPath;
    }

    /**
     * Get the integer uid of this message
     * 
     * @return uid
     */
    public int getUid() {
        return uid;
    }

    /**
     * Get the string Message ID of this message
     * 
     * @return message ID
     */
    public String getMsgId() {
        return msgId;
    }

}

