/*
 *      Copyright (c) 2002-2004 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/Cache.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.util.Map;

/**
 * This interface defines a class to be used as a cache for 
 * objects.  Implementations will select a specific caching
 * algorithm and concurrency behavior.  It is also up to 
 * implementations whether <code>null</code> is a permitted
 * value for a key or stored object.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public interface Cache {

    /**
     * Get an object from the cache.
     *
     * @param key the key for the desired object
     *
     * @return the object, or <code>null</code> if the object is not
     *    in the cache
     */
    public Object get(Object key);

    /**
     * Get the cache contents as a Map.  Implementations may return a
     * copy of the contents of the cache or they may return an
     * unmodifiable view onto the contents.
     *
     * @return a <code>Map</code> of the current contents of the cache 
     */
    public Map getMap();

    /**
     * Put an object into the cache.
     *
     * @param key the key for the object
     * @param value the object
     *
     * @return the object previously stored in the cache under this
     *    key, or <code>null</code> if there was no such object
     */
    public Object put(Object key, Object value);

    /**
     * Remove an object from the cache.
     *
     * @param key the key for the object to remove
     *
     * @return the removed object, or <code>null</code> if there was
     *    no such object
     */
    public Object remove(Object key);

    /**
     * Clear the contents of the cache.
     */
    public void clear();
}
