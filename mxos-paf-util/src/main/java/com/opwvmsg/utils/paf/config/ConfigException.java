/*
 * Copyright (c) 2002-2005 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/ConfigException.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config;

/**
 * A general-purpose exception for the Config API.
 */
public class ConfigException extends RuntimeException {

    /**
     * Create an instance.
     */  
    public ConfigException() {
        super();
    }

    /**
     * Create an instance with the specified message.
     *  
     * @param s the message
     */
    public ConfigException(String s) {
        super(s);
    }

    /**
     * Create an instance which wraps the given throwable.
     *  
     * @param e the throwable
     */
    public ConfigException(Throwable e) {
        super(e);
    }

    /**
     * Create an instance with the specified message and which wraps
     * the given throwable.
     *  
     * @param s the message
     * @param e the throwable
     */
    public ConfigException(String s, Throwable e) {
        super(s, e);
    }
}
