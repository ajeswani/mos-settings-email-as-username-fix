/** 
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id:  
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.DocIdInfo;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation will read a message from the MSS. It has a variety of options
 * to control how much of the message you will retrieve from the most basic of
 * meta data, to the headers, mime information, or the text itself.
 */
public class RetrieveDocIdInfo extends AbstractMssOperation {

    // input
    private String name;
    private String pathname;
    private String[] docids;
    protected int options;
    private int accessId;

    // output
    private DocIdInfo[] docidinfo;

    // internal
    private String url;
    private String[] pathnameArray;

    // constants
    public static final int READ_FOLDER_RECURSIVE = 0x01;

    /**
     * @param host The MSS hostname for the user
     * @param mssID The MSS id of the user
     * @param folderName The folder (or folder hint) for the messages
     * @param uids The uids of the messages to read
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            READ_FOLDER_RECURSIVE - xxxxxxxxxxxxxxxxx
     */
    public RetrieveDocIdInfo(String host, String mssID, String folderName,
            String[] docids, int accessId, int options) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = mssID;
        url = getMsUrl(host, mssID, MSS_P_CL_RETRIEVEDOCIDINFO);
        this.pathname = folderName;
        pathnameArray = buildPathnameArray(folderName);
        if (docids == null) {
            docids = new String[0];
        }
        this.docids = docids;
        this.accessId = accessId;
        this.options = options;

    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (pathname == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "pathname", "ReadMsgs" }));
        }
        callRme(MSS_P_CL_RETRIEVEDOCIDINFO);
    }

    /**
     * Get the message objects returned from the MSS
     * 
     * @return messages
     */
    public DocIdInfo[] getDocIdInfo() {
        return docidinfo;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeInt(accessId); // IM_MS_ACCESS_ADMIN
        outStream.writeString(url); // msURL
        outStream.writeInt(options); // options 1 for recursive
        outStream.writeStringArray(pathnameArray); // folderName
        outStream.writeStringArray(docids); // docIds
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // read DocIdInfo
        int numDocIdInfo = inStream.readInt();
        docidinfo = new DocIdInfo[numDocIdInfo];
        for (int i = 0; i < numDocIdInfo; i++) {
            docidinfo[i] = new DocIdInfo(inStream);
        }
        logEvents = inStream.readLogEventArray();
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation not supported
    }
}
