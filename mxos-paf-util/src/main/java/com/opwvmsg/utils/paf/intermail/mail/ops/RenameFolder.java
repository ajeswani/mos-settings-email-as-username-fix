/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.FolderInfo;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation will rename a folder.
 */
public class RenameFolder extends AbstractMssOperation {

    // input
    private String name;
    private String pathname;
    private String newPathname;
    private int options;
    private int accessId;

    private UUID folderUUID;
    private String newFolderName;
    private UUID newParentFolderUUID;
    private boolean isMoveOperation;
    private int uidValidity;

    // output
    private int numFolderInfo;
    private FolderInfo[] folderInfo;

    // internal
    private String url;
    private String[] pathnameArray;
    private String[] newPathnameArray;
    private String hostHeader;

    // constants
    public static final int RENAME_FOLDER_SUPPRESS_MERS = 0x01;

    /**
     * Constructor for RenameFolder for MSS_P_CL_RENAMEFOLDER
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder to rename
     * @param newPathname The new folder name
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            RENAME_FOLDER_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     */
    public RenameFolder(String host, String name, String pathname,
            String newPathname, int accessId, int options) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.pathname = pathname;
        this.newPathname = newPathname;
        url = getMsUrl(host, name, MSS_P_CL_RENAMEFOLDER);
        pathnameArray = buildPathnameArray(pathname);
        newPathnameArray = buildPathnameArray(newPathname);
        this.accessId = accessId;
        this.options = options;
    }

    /**
     * Constructor for RenameFolder - MSS_SL_RENAMEFOLDER
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID Folder UUID of a folder to be renamed/moved
     * @param newFolderName The new folder name
     * @param newParentFolderUUID For Move - Folder UUID under which you want to
     *            move the folder For Rename - Parent folder UUID of folder to
     *            be renamed
     * @param isMoveOperation - Whether to move ( TRUE ) or rename folder (
     *            FALSE )
     * @param uidValidity - uidValidity of a folder. For move operation
     *            uidValidity remains same as old folder.
     */
    public RenameFolder(String host, String name, String realm,
            UUID folderUUID, String newFolderName, UUID newParentFolderUUID,
            boolean isMoveOperation, int uidValidity) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.folderUUID = folderUUID;
        this.newFolderName = newFolderName;
        this.newParentFolderUUID = newParentFolderUUID;
        this.isMoveOperation = isMoveOperation;
        this.uidValidity = uidValidity;
        url = getMsUrl(host, name, MSS_SL_RENAMEFOLDER);
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        // check args
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            if (pathname == null || newPathname == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "pathname", "RenameFolder" }));
            }
            // if root folder or INBOX, disallow renaming
            if (pathnameArray.length == 0
                    || (pathnameArray.length == 1 && pathnameArray[0]
                            .equals("INBOX"))) {
                throw new IntermailException(LogEvent.WARNING,
                        LogEvent.formatLogString("Ms.FolderNotRenamable",
                                new String[] { pathname }));
            }
            callRme(MSS_P_CL_RENAMEFOLDER);
        } else { // Leopard
            if (folderUUID == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "folderUUID", "RenameFolder" }));
            }

            if (newParentFolderUUID == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "newParentFolderUUID",
                                        "RenameFolder" }));
            }
            if (newFolderName == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "pathname", "RenameFolder" }));
            }
            callRme(MSS_SL_RENAMEFOLDER);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(pathnameArray);
        outStream.writeStringArray(newPathnameArray);
        outStream.writeInt(accessId);
        if (outStream.rmeProtAtLeast(RME_CL_OPERATIONS_OPTIONS_VER)) {
            outStream.writeInt(options);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        logEvents = inStream.readLogEvent();
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_RENAMEFOLDER));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeUUID(folderUUID);
        outStream.writeString(newFolderName);
        outStream.writeUUID(newParentFolderUUID);
        outStream.writeBoolean(isMoveOperation);
        outStream.writeInt(uidValidity);
        outStream.flush();

    }

    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        numFolderInfo = inStream.readInt();
        folderInfo = new FolderInfo[numFolderInfo];
        if (numFolderInfo > 0) {
            for (int i = 0; i < numFolderInfo; i++) {
                folderInfo[i] = new FolderInfo(inStream);
            }
        }
        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the folderInfo
     */
    public FolderInfo[] getFolderInfo() {
        return folderInfo;
    }

}
