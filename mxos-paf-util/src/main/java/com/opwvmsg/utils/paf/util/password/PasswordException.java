/*
 * Copyright (c) 2003-2004 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/PasswordException.java#1 $
 */

package com.opwvmsg.utils.paf.util.password;

/**
 * This is a general-purpose exception for the password API.
 */
public class PasswordException extends Exception {

    public PasswordException() {
        super();
    }

    public PasswordException(String s) {
        super(s);
    }

    public PasswordException(Throwable e) {
        super(e);
    }

    public PasswordException(String s, Throwable e) {
        super(s, e);
    }
}
