/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.http;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * An HTTPConnection extends the DefaultHttpClient class and provides and adds
 * extra attributes to store the host and port. This shall be helpful in
 * managing the pool of HTTP Client
 * 
 * @author mxos-dev
 */
public class HTTPConnection extends DefaultHttpClient {
    private int rmeTimeout = 0;
    private String host;
    private int port;
    private HttpPost postMethod;
    private HttpResponse response;

    public HTTPConnection(final String hostIn, final int portIn,
            final int rmeTimeout) {
        this.host = hostIn;
        this.port = portIn;
        this.rmeTimeout = rmeTimeout;
        postMethod = new HttpPost();
        response = null;
    }

    /**
     * Sends HttpPost request.
     * 
     * @param postM: HTTP Post Method
     * @return HttpResponse
     * @throws ClientProtocolException
     * @throws IOException
     */
    public HttpResponse executePost(HttpPost post)
            throws ClientProtocolException, IOException {
        // Remove all the old headers.
        Header[] h1 = postMethod.getAllHeaders();
        for (int i = 0; i < h1.length; i++) {
            postMethod.removeHeaders(h1[i].getName());
        }

        postMethod.setHeaders(post.getAllHeaders());
        postMethod.setEntity(post.getEntity());
        postMethod.setURI(post.getURI());
        response = super.execute(postMethod);
        return response;
    }

    /**
     * closes the InputStream on the HttpConnection for making the same
     * connection reusable for next request.
     * 
     * @throws IllegalStateException
     * @throws IOException
     */
    void cleanup() throws IllegalStateException, IOException {
        if (response != null) {
            final HttpEntity en = response.getEntity();
            if (en != null) {
                en.getContent().close();
            }
        }
        response = null;
    }

    /**
     * @return the postMethod
     */
    public HttpPost getPostMethod() {
        return postMethod;
    }

    /**
     * @return the response
     */
    public HttpResponse getResponse() {
        return response;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(final int port) {
        this.port = port;
    }

    public int getRmeTimeout() {
        return rmeTimeout;
    }

    public void setRmeTimeout(final int rmeTimeout) {
        this.rmeTimeout = rmeTimeout;
    }
}