/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/DeleteFolder.java#1 $ 
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation will delete a folder.
 */
public class DeleteFolder extends AbstractMssOperation {

    // input
    private String name;
    private String pathname;
    private boolean force;
    private int options;
    private int accessId;

    private UUID parentFolderUUID;
    private UUID folderUUID;

    // output
    // -- no output --

    // internal
    private String url;
    private String[] pathnameArray;
    private String hostHeader;

    // constants
    public static final int DELETE_FOLDER_SUPPRESS_MERS = 0x01;

    /**
     * Constructor for DeleteFolder for MSS_P_CL_REMOVEFOLDER
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder to delete
     * @param force - if true, delete folder even if it's locked by a user
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            DELETE_FOLDER_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     */
    public DeleteFolder(String host, String name, String pathname,
            boolean force, int accessId, int options) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.pathname = pathname;
        url = getMsUrl(host, name, MSS_P_CL_REMOVEFOLDER);
        pathnameArray = buildPathnameArray(pathname);
        this.force = force;
        this.accessId = accessId;
        this.options = options;
    }

    /**
     * Constructor for DeleteFolder for MSS_SL_DELETEFOLDER
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param parentFolderUUID Parent FolderUUID of the folder to be deleted
     * @param folderUUID - folderUUID to be deleted
     * @param force - if true, delete folder even if it's locked by a user
     */
    public DeleteFolder(String host, String name, String realm,
            UUID parentFolderUUID, UUID folderUUID, boolean force) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.parentFolderUUID = parentFolderUUID;
        this.folderUUID = folderUUID;
        this.force = force;
        url = getMsUrl(host, name, MSS_SL_DELETEFOLDER);
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {

        // check args
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (this.rmeDataModel == RmeDataModel.CL) {
            if (pathname == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "pathname", "DeleteFolder" }));
            }
            callRme(MSS_P_CL_REMOVEFOLDER);
        } else { // Leopard
            if (parentFolderUUID == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "parentFolderUUID",
                                        "DeleteFolder" }));
            }

            if (folderUUID == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "folderUUID", "DeleteFolder" }));
            }
            callRme(MSS_SL_DELETEFOLDER);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(pathnameArray);
        outStream.writeBoolean(force);
        outStream.writeInt(accessId);
        if (outStream.rmeProtAtLeast(RME_CL_OPERATIONS_OPTIONS_VER)) {
            outStream.writeInt(options);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        logEvents = inStream.readLogEvent();
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_DELETEFOLDER));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);

        // parentFolderUUID
        outStream.writeUUID(parentFolderUUID);
        outStream.writeUUID(folderUUID);
        outStream.writeBoolean(force);
        outStream.flush();
    }

    @Override
    protected void receiveHttpData() throws IOException {
        logEvents = inStream.readLogEvent();
    }
}
