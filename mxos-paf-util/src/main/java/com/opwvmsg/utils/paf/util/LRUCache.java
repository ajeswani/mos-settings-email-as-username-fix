/*
 *      Copyright (c) 2003-2004 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/LRUCache.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * This cache implementation has LRU semantics.  The cache size is
 * limited to a maximum number of entries.  Once the cache has reached
 * its size limit, each subsequent put will cause the least recently
 * used entry to be removed. Nulls are permitted as a key and 
 * as stored objects.  The methods are fully synchronized for thread-safety.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class LRUCache implements Cache {

    /**
     * The map that contains the cached objects.
     */
    private Map cache = new HashMap();

    /**
     * The head of the linked list of entries.
     */
    private Entry head;

    private int maxEntries;

    private static final int DEFAULT_MAX_ENTRIES = 128;

    /**
     * Return an instance of this cache using any valid parameters
     * provided in the given <code>Map</code>.
     *
     * @param parameters a <code>Map</code> of parameters used to
     *        construct an instance of a <code>LRUCache</code> where
     *        the names and values are <code>String</code> instances
     *        and the names are identical to the constructor
     *        parameters (for example, "maxEntries")
     * @return an instance of a <code>LRUCache</code> constructed
     *        using the contents of the given parameters <code>Map</code>
     */
    public static Cache getInstance(Map parameters) {
        int maxEntries = DEFAULT_MAX_ENTRIES;
        if (parameters.get("maxEntries") != null) {
            maxEntries = Integer.parseInt(
                    (String)parameters.get("maxEntries"));
        }
        return new LRUCache(maxEntries);
    }

    /**
     * Create an instance with the given max number of entries.
     *
     * @param maxEntries maximum number of entries retained in this cache
     */
    public LRUCache(int maxEntries) {
        this.maxEntries = maxEntries;
        head = new Entry(null, null);
        initLinkedList();
    }


    private void initLinkedList() {
        head.previous = head;
        head.next = head;
    }

    /**
     * Get an object from the cache.
     *
     * @param key the key for the desired object
     *
     * @return the object, or <code>null</code> if the object is not
     *    in the cache
     */
    public synchronized Object get(Object key) {
        Entry entry = (Entry) cache.get(key);
        if (entry != null) {
            entry.accessed();
            return entry.getValue();
        } else {
            return null;
        }
    }

    /**
     * Put an object into the cache.
     *
     * @param key the key for the object
     * @param value the object
     *
     * @return the object previously stored in the cache under this
     *    key, or <code>null</code> if there was no such object
     */
    public synchronized Object put(Object key, Object value) {
        Entry entry = new Entry(key, value);
        Entry oldEntry = (Entry) cache.put(key, entry);
        if (oldEntry != null) {
            oldEntry.remove();
        } else {
            if (cache.size() > maxEntries) {
                Entry oldest = head.next;
                remove(oldest.getKey());
            }
        }
        entry.insertBefore(head);
        if (oldEntry != null) {
            return oldEntry.getValue();
        } else {
            return null;
        }
    }

    /**
     * Remove an object from the cache.
     *
     * @param key the key for the object to remove
     *
     * @return the removed object, or <code>null</code> if there was
     *    no such object
     */
    public synchronized Object remove(Object key) {
        Entry entry = (Entry) cache.remove(key);
        if (entry != null) {
            entry.remove();
            return entry.getValue();
        } else {
            return null;
        }
    }

    /**
     * Get the contents of the cache.</p>
     * 
     * Accessing entries in the returned <code>Map</code> does not
     * affect the last access time of the entries in the cache. 
     * Subclasses can provide different behavior if so desired.
     * 
     * @return map cache contents as a map
     */
    public synchronized Map getMap() {
        return Collections.unmodifiableMap(cache);
    }

    /**
     * Clear the contents of the cache.
     */
    public void clear() {
        cache.clear();
        initLinkedList();
    }

    /**
     * This class is used to hold the values stored in the 
     * underlying Map.  It maintains a doubly-linked list
     * of all the entries in the map.
     */
    private class Entry {
        private Object key;

        private Object value;

        private Entry previous;

        private Entry next;


        public Entry(Object key, Object value) {
            this.key = key;
            this.value = value;
        }


        public Object getKey() {
            return key;
        }

        public Object getValue() {
            return value;
        }

        /**
         * This entry has been accessed.
         */
        public void accessed() {
            remove();
            insertBefore(head);
        }

        /**
         * Insert this entry in the linked list before the 
         * given existing entry.
         *
         * @param entry the entry to insert before
         */
        public void insertBefore(Entry entry) {
            this.previous = entry.previous;
            this.next = entry;
            entry.previous = this;
            this.previous.next = this;
        }

        /**
         * Remove this entry from the linked list.
         */
        public void remove() {
            previous.next = next;
            next.previous = previous;
        }
    }
}
