/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MessageMetadata;
import com.opwvmsg.utils.paf.intermail.mail.AttributeDescription;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation will be used to search messages by attributes and returns
 * attributes.
 * 
 * @author mOS_dev
 */
public class GetMessageAttributes extends AbstractMssOperation {

    // input
    private String name; // Mailbox Id
    private UUID folderUUID; // folder from which we want list of messages.
    private short keyType; // Fetch messages sorted based on particular index
                           // like To/From/CC/Subject
    private String[] keys; // Query keys.
    private short[] attrType; // List of attribute types to request for in reply
    private boolean getPopDelted; // Get popDeleted Messages as deleted

    // output
    private MessageMetadata[] messageMetadata;
    private int numEntries;

    // internal
    private String url;
    private String hostHeader;

    // constants
    public static final short KEYTYPE_MESSAGE_UUID = 0;
    public static final short KEYTYPE_ATTACHMENTS = 4;
    public static final short KEYTYPE_RM_FLAG = 5;
    public static final short KEYTYPE_ARRIVAL_TIME = 6;
    public static final short KEYTYPE_SIZE = 11;
    public static final short KEYTYPE_UID = 12;
    public static final short KEYTYPE_PRIORITY = 16;
    public static final short KEYTYPE_SUBJECT = 18;
    public static final short KEYTYPE_FROM = 19;
    public static final short KEYTYPE_TO = 20;
    public static final short KEYTYPE_CC = 21;
    public static final short KEYTYPE_DATE = 22;
    public static final short KEYTYPE_CONVERSATION_HISTORY = 23;

    /**
     * Default Constructor
     * 
     * @param host The MSS hostname for the user.
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID folder from which we want list of messages.
     * @param keyType Fetch messages sorted based on particular index 0 -
     *            messageUUID, 4 - Attachments,5 - Richmail Flag,6 - arrival
     *            time,11 - size,12 - UID,16 - Priority,18- Subject,19 - From,20
     *            - To,21 - Cc,22 - Date,23 - Conversation History.
     * @param keys Query keys.
     * @param attrType: List of attribute types to request for in reply. If the
     *            vector is empty, MSS returns all supporting attributes. The
     *            attribute messageUUID is always included in the reply even
     *            though it is not specified. 0: messageUUID,1: bounce,2:
     *            msgDeliveredNDR,3: msgPrivate,4: hasAttachments,5: richmail
     *            flag,6: arrivalTime,7:
     *            msgExpirationSeconds,8:msgFirstSeenSeconds,9:
     *            timeFirstAccessed,10: timeLastAccessed,11: size,12: uid,13:
     *            msgFlags,14: keywords,15: msgType,16:priority,17:
     *            multipleMsgs,18: subject,19: from,20: to,21: cc,22: date,23:
     *            references
     * @param getPopDelted - false The messages that are marked as special
     *            deleted are not returned as deleted. true The messages that
     *            are marked as special deleted are returned as deleted
     *            messages.
     */
    public GetMessageAttributes(String host, String name, String realm,
            UUID folderUUID, short keyType, String[] keys, short[] attrType,
            boolean getPopDelted) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_GETMESSAGEATTRIBUTES);
        this.folderUUID = folderUUID;
        this.keyType = keyType;
        this.keys = keys;
        this.attrType = attrType;
        this.getPopDelted = getPopDelted;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (folderUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "folderUUID", "MSS_SL_GETMESSAGEATTRIBUTES" }));
        }
        if (keys == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "keys", "MSS_SL_GETMESSAGEATTRIBUTES" }));
        }
        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_SL_GETMESSAGEATTRIBUTES", host,
                            "dataModel=" + rmeDataModel }));
        } else {
            callRme(MSS_SL_GETMESSAGEATTRIBUTES);
        }
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_GETMESSAGEATTRIBUTES));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeStringBytes(url);
        outStream.writeUUID(folderUUID);
        outStream.writeShort(keyType);
        writeKeys(keyType, keys);
        outStream.writeShortArray(attrType);
        outStream.writeBoolean(getPopDelted);
        outStream.flush();
    }

    /**
     * This method is used to write keys to the outstream in the desired format
     * as expected by MSS
     * 
     * @throws IOException
     */
    private void writeKeys(short keyType, String[] keys) throws IOException {
        outStream.writeInt(keys.length);
        for (int i = 0; i < keys.length; i++) {
            switch (keyType) {
            case KEYTYPE_MESSAGE_UUID:
                outStream.writeUUIDBlob(UUID.fromString(keys[i]));
                break;
            default:
                throw new IOException("Unsupported Data received : " + keyType);
            }
        }
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        // Read Attribute Descriptions Array
        int numAttrDesc = inStream.readInt();
        AttributeDescription[] attrDescriptions = new AttributeDescription[numAttrDesc];
        for (int i = 0; i < numAttrDesc; i++)
            attrDescriptions[i] = new AttributeDescription(inStream);

        // Read Messages and its attributes.
        numEntries = inStream.readInt();
        messageMetadata = new MessageMetadata[numEntries];
        for (int i = 0; i < numEntries; i++) {
            messageMetadata[i] = new MessageMetadata(inStream, attrDescriptions);
        }

        // Read the Log Events
        logEvents = inStream.readLogEvent();
    }

    /**
     * Get the messageMetadata objects returned from the MSS
     * 
     * @return messageMetadata
     */
    public MessageMetadata[] getMessageMetadata() {
        return messageMetadata;
    }

    /**
     * @return the numEntries
     */
    public int getNumEntries() {
        return numEntries;
    }
}