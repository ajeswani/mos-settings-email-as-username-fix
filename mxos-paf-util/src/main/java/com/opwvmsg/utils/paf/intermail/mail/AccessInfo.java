/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class encapsulates all of the AccessInfo data RME
 * 
 * @author mxos-dev
 */
public class AccessInfo {
    private static final Logger logger = Logger.getLogger(AccessInfo.class);

    // Attributes
    private long lastLoginTime;
    private long lastSuccessfulLoginTime;
    private long lastFailedLoginTime;
    private long numFailedLoginAttempts;
    private int count = 0;

    // Constants for MailboxAccess Info Attribute Types
    public static final byte ATTR_LAST_LOGIN_TIME = (byte) 0x02;
    public static final byte ATTR_LAST_SUCCESSFUL_LOGIN_TIME = (byte) 0x03;
    public static final byte ATTR_LAST_FAILED_LOGIN_TIME = (byte) 0x04;
    public static final byte ATTR_NUM_FAILED_LOGIN_ATTEMPTS = (byte) 0x05;

    /**
     * Default Constructor.
     * 
     * @param inStream
     * @throws IOException
     */
    public AccessInfo() throws IOException {
        this.lastLoginTime = -1;
        this.lastSuccessfulLoginTime = -1;
        this.lastFailedLoginTime = -1;
        this.numFailedLoginAttempts = -1;
    }

    /**
     * Constructor. Reads the input Stream and constructs the AccessInfo.
     * 
     * @param inStream
     * @throws IOException
     */
    public AccessInfo(RmeInputStream inStream) throws IOException {
        byte attrType = 0;
        int numAttributes = inStream.readInt();
        for (int i = 0; i < numAttributes; i++) {
            attrType = inStream.readByte();
            int len = inStream.readInt();
            switch (attrType) {
            case ATTR_LAST_LOGIN_TIME:
                lastLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_LAST_SUCCESSFUL_LOGIN_TIME:
                lastSuccessfulLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_LAST_FAILED_LOGIN_TIME:
                lastFailedLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_NUM_FAILED_LOGIN_ATTEMPTS:
                numFailedLoginAttempts = inStream.readLong64Blob();
                break;
            default:
                logger.debug("Invalid Attribute Type " + attrType
                        + " received in the MSS_SL_MAILBOXACCESSINFO response.");
            }
        }
    }

    /**
     * Method to write AccessInfo.
     * 
     * @param accessInfo
     * @param outStream: OutStream to write MessageMetadata.
     * @return: void.
     * @throws: IOException
     */
    public void write(RmeOutputStream outStream) throws IOException {

        outStream.writeInt(this.count);
        if (this.lastLoginTime != -1) {
            outStream.writeByte(ATTR_LAST_LOGIN_TIME);
            outStream.writeLongBlob(lastLoginTime, true);
        }
        if (this.lastSuccessfulLoginTime != -1) {
            outStream.writeByte(ATTR_LAST_SUCCESSFUL_LOGIN_TIME);
            outStream.writeLongBlob(lastSuccessfulLoginTime, true);
        }
        if (this.lastFailedLoginTime != -1) {
            outStream.writeByte(ATTR_LAST_FAILED_LOGIN_TIME);
            outStream.writeLongBlob(lastFailedLoginTime, true);
        }
        if (this.numFailedLoginAttempts != -1) {
            outStream.writeByte(ATTR_NUM_FAILED_LOGIN_ATTEMPTS);
            outStream.writeLongBlob(numFailedLoginAttempts, true);
        }
    }

    /**
     * @return the lastLoginTime
     */
    public long getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * @return the lastSuccessfulLoginTime
     */
    public long getLastSuccessfulLoginTime() {
        return lastSuccessfulLoginTime;
    }

    /**
     * @return the lastFailedLoginTime
     */
    public long getLastFailedLoginTime() {
        return lastFailedLoginTime;
    }

    /**
     * @return the numFailedLoginAttempts
     */
    public long getNumFailedLoginAttempts() {
        return numFailedLoginAttempts;
    }

    /**
     * for debugging - output all of the mailboxAccessInfo parameters
     * 
     * @param out a PrintStream to dump the data.
     */
    public void dump() {
        System.out.println();
        System.out.println("DUMPING MailboxInfo ");
        System.out.println("lastLoginTime: " + this.getLastLoginTime());
        System.out.println("lastSuccessfulLoginTime: "
                + this.getLastSuccessfulLoginTime());
        System.out.println("lastFailedLoginTime: "
                + this.getLastFailedLoginTime());
        System.out.println("numFailedLoginAttempts: "
                + this.getNumFailedLoginAttempts());
        System.out.println("==== DONE ==== ");
        System.out.println();
    }

    /**
     * @param lastLoginTime the lastLoginTime to set
     */
    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        this.count++;
    }

    /**
     * @param lastSuccessfulLoginTime the lastSuccessfulLoginTime to set
     */
    public void setLastSuccessfulLoginTime(long lastSuccessfulLoginTime) {
        this.lastSuccessfulLoginTime = lastSuccessfulLoginTime;
        this.count++;
    }

    /**
     * @param lastFailedLoginTime the lastFailedLoginTime to set
     */
    public void setLastFailedLoginTime(long lastFailedLoginTime) {
        this.lastFailedLoginTime = lastFailedLoginTime;
        this.count++;
    }

    /**
     * @param numFailedLoginAttempts the numFailedLoginAttempts to set
     */
    public void setNumFailedLoginAttempts(long numFailedLoginAttempts) {
        this.numFailedLoginAttempts = numFailedLoginAttempts;
        this.count++;
    }

}
