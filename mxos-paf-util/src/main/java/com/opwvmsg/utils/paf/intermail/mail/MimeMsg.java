/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/MimeMsg.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.ArrayList;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class encapsulates all of the data returned from an RME MimeMsg object.
 * The MimeMsg object is a part of the overall Msg object.
 */
public class MimeMsg {
    
    private static String TO = "to";
    private static String FROM = "from";
    private static String CC = "cc";
    private static String BCC = "bcc";
    private static String SUBJECT = "subject";
    private static String DATE = "date";
    private static String MSG_ID = "message-id";
    private static String REPLY_TO = "reply-to";
    private static String IN_REPLY_TO = "in-reply-to";
    private static String SENDER = "sender";
    private static String PRIORITY = "priority";
    private static String MIME_VERSION= "mime-version";

    private String mimeVersion;
    private MimeInfo bodyPart;
    private int[] toOffsets;
    private int[] fromOffsets;
    private int[] ccOffsets;
    private int[] bccOffsets;
    private int[] subjectOffsets;
    private int[] dateOffsets;
    private int[] msgIdOffsets;
    private int[] replytoOffsets;
    private int[] inReplytoOffsets;
    private int[] allOffsets;
    private int[] senderOffsets;
    private int[] priorityOffsets;

    /** 
     * Constructs a new MimeMsg directly from content bytes.  This will parse
     * the message content.  The parser is kept simple, since the content is assumed
     * to be derived from a MimeMessage.writeTo which will enforce certain things
     * like crlf line endings and proper boundary markers.
     * 
     * @param content the bytes of the final message
     * @param offset the offset in the content where this message starts
     */
    public MimeMsg(byte[] content, int offset, int length) {
        ArrayList allOffsetsList = new ArrayList();
        int headerOffset = offset - 1;
        while (!Character.isLetterOrDigit((char)content[++headerOffset]));
        
        StringBuffer headerNameBuffer = new StringBuffer(20);
        StringBuffer headerValueBuffer = new StringBuffer(200);
        int start = headerOffset;
        int currentPos = MimeInfo.getNextHeader(content, headerOffset,
            headerNameBuffer, headerValueBuffer, null);
        String headerName = headerNameBuffer.toString();
        String headerValue = headerValueBuffer.toString();
        while (headerName.length() > 0) {
            allOffsetsList.add(new Integer(start));
            allOffsetsList.add(new Integer(currentPos - start - 2));
            if (headerName.equals(TO)) {
                toOffsets = addToOffsets(toOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(FROM)){
                fromOffsets = addToOffsets(fromOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(CC)){
                ccOffsets = addToOffsets(ccOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(BCC)){
                bccOffsets = addToOffsets(bccOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(SUBJECT)){
                subjectOffsets = addToOffsets(subjectOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(DATE)){
                dateOffsets = addToOffsets(dateOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(MSG_ID)){
                msgIdOffsets = addToOffsets(msgIdOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(REPLY_TO)){
                replytoOffsets = addToOffsets(replytoOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(IN_REPLY_TO)){
                inReplytoOffsets = addToOffsets(inReplytoOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(SENDER)){
                senderOffsets = addToOffsets(senderOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(PRIORITY)){
                priorityOffsets = addToOffsets(priorityOffsets, start, currentPos - start - 2);
            } else if (headerName.equals(MIME_VERSION)){
                mimeVersion = MimeInfo.unQuote(headerValue);
            }
            
            start = currentPos;
            currentPos = MimeInfo.getNextHeader(content, currentPos,
                headerNameBuffer, headerValueBuffer, null);
            headerName = headerNameBuffer.toString();
            headerValue = headerValueBuffer.toString();

        }
        allOffsets = new int[allOffsetsList.size()];
        for (int i = 0; i < allOffsetsList.size(); i++) {
            allOffsets[i] = ((Integer)allOffsetsList.get(i)).intValue();
        }

        bodyPart = new MimeInfo(content, offset, length);
    }
    
    /**
     * Adds the pair of offset and length passed in to the array.
     * Hopefully this function is not called often, since it is only used 
     * if we have more than one of the same header.
     * 
     * @param offsets the list of currently known offsets
     * @param offset the byte offset of the start of the header 
     * @param length the length of the header
     * @return the new int array containing the new offset and length
     */
    private int[] addToOffsets(int[] offsets, int offset, int length) {
        if (offsets == null) {
            return new int[] { offset, length };
        }
        int[] newOffsets = new int[offsets.length + 2];
        for (int i = 0; i < offsets.length; i++) {
            newOffsets[i] = offsets[i];
        }
        newOffsets[offsets.length] = offset;
        newOffsets[offsets.length + 1] = length;
        return newOffsets;
    }
    
    /**
     * Reads this MimeMsg object from an RME stream
     * 
     * @param inStream the rme input stream
     * @throws IOException on any IO error
     */

    public MimeMsg(RmeInputStream inStream) throws IOException {
        boolean haveIt = inStream.readBoolean();
        if (haveIt) {
            mimeVersion = inStream.readString();
            bodyPart = new MimeInfo(inStream);
            toOffsets = inStream.readIntArray();
            fromOffsets = inStream.readIntArray();
            ccOffsets = inStream.readIntArray();
            bccOffsets = inStream.readIntArray();
            subjectOffsets = inStream.readIntArray();
            dateOffsets = inStream.readIntArray();
            msgIdOffsets = inStream.readIntArray();
            replytoOffsets = inStream.readIntArray();
            inReplytoOffsets = inStream.readIntArray();
            allOffsets = inStream.readIntArray();
            senderOffsets = inStream.readIntArray();
            priorityOffsets = inStream.readIntArray();
        }

    }
    
    public void writeToStream(RmeOutputStream outStream) throws IOException {
        outStream.writeBoolean(true);
        outStream.writeString(mimeVersion);
        bodyPart.writeToStream(outStream);
        outStream.writeIntArray(toOffsets);
        outStream.writeIntArray(fromOffsets);
        outStream.writeIntArray(ccOffsets);
        outStream.writeIntArray(bccOffsets);
        outStream.writeIntArray(subjectOffsets);
        outStream.writeIntArray(dateOffsets);
        outStream.writeIntArray(msgIdOffsets);
        outStream.writeIntArray(replytoOffsets);
        outStream.writeIntArray(inReplytoOffsets);
        outStream.writeIntArray(allOffsets);
        outStream.writeIntArray(senderOffsets);
        outStream.writeIntArray(priorityOffsets);        
    }

    /**
     * Get the mime version used
     * 
     * @return mime version
     */
    public String getVersion() {
        return mimeVersion;
    }

    /**
     * Get the MimeInfo data for this part
     * 
     * @return MimeInfo object
     */
    public MimeInfo getMimeInfo() {
        return bodyPart;
    }

    /**
     * Debugging method to dump MimeMsg to stdout
     */
    protected void dump() {
        System.out.println("Dumping MIME Message");
        System.out.println("====================");
        System.out.println("MIME Version: " + mimeVersion);
        System.out.println("TO Offsets: " + dumpArray(toOffsets));
        System.out.println("FROM Offsets: " + dumpArray(fromOffsets));
        System.out.println("CC Offsets: " + dumpArray(ccOffsets));
        System.out.println("SUBJECT Offsets: " + dumpArray(subjectOffsets));
        System.out.println("REPLY Offsets: " + dumpArray(replytoOffsets));
        System.out.println("INREPLY Offsets: " + dumpArray(inReplytoOffsets));
        System.out.println("ALL Offsets: " + dumpArray(allOffsets));
        System.out.println("SENDER Offsets: " + dumpArray(senderOffsets));
        System.out.println("PRI Offsets: " + dumpArray(priorityOffsets));
        System.out.println("Body Parts:");
        bodyPart.dump();
        System.out.println("=====DONE MIME MESSAGE===============");
    }

    /**
     * A helper function to convert an int array to a single printable string
     * 
     * @param array the int array to process
     * @return the resulting combined string
     */
    private String dumpArray(int[] array) {
        String out = "";
        if (array == null || array.length == 0) {
            out += "EMPTY";
            return out;
        }
        for (int i = 0; i < array.length; i++) {
            out += i + ": " + array[i] + ", ";
        }
        return out;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("BEGIN MIME Message");
        buffer.append("\n");
        buffer.append("====================");
        buffer.append("\n");
        buffer.append("MIME Version: ").append(mimeVersion);
        buffer.append("\n");
        buffer.append("TO Offsets: ").append(dumpArray(toOffsets));
        buffer.append("\n");
        buffer.append("FROM Offsets: ").append(dumpArray(fromOffsets));
        buffer.append("\n");
        buffer.append("CC Offsets: ").append(dumpArray(ccOffsets));
        buffer.append("\n");
        buffer.append("SUBJECT Offsets: ").append(dumpArray(subjectOffsets));
        buffer.append("\n");
        buffer.append("REPLY Offsets: ").append(dumpArray(replytoOffsets));
        buffer.append("\n");
        buffer.append("INREPLY Offsets: ").append(dumpArray(inReplytoOffsets));
        buffer.append("\n");
        buffer.append("ALL Offsets: ").append(dumpArray(allOffsets));
        buffer.append("\n");
        buffer.append("SENDER Offsets: ").append(dumpArray(senderOffsets));
        buffer.append("\n");
        buffer.append("PRI Offsets: ").append(dumpArray(priorityOffsets));
        buffer.append("\n");
        buffer.append("Body Parts:");
        buffer.append("\n");
        buffer.append(bodyPart.toString());
        buffer.append("===== END MIME MESSAGE ===============");
        return buffer.toString();
    }
}

