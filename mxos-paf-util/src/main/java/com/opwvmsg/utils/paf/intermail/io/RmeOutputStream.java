/*
 * H+ Copyright 2004 Software.com, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of
 * Software.com, Inc. The software may be used and/or copied only with the
 * written permission of Software.com, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/io/RmeOutputStream.java#1 $ H-
 */
package com.opwvmsg.utils.paf.intermail.io;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * An RmeOutputStream is a BufferedOutputStream, except it's write methods are
 * unsynchronized. (This stream cannot be shared). It also borrows many
 * convenience classes for writing basic type from DataOutputStream and adds a
 * few more that are particular to the RME protocol
 */
public class RmeOutputStream extends BufferedOutputStream {

    private int rmeProtVer = AbstractOperation.RME_CURRENT_VERSION;
    public static final int UUID_SIZE = 0x10;
    public static final int LONG_SIZE = 0x8;
    private boolean littleEndian = false; // by default we'll use big endian

    /**
     * The basic constructor to be called from RmeSocket when the connection is
     * established
     * 
     * @param stream the outputStream returned from the socket
     */
    public RmeOutputStream(OutputStream stream) {
        super(stream, 2048);
    }

    /**
     * Sets the byte order flag for data on this stream
     * 
     * @param invert if true, use little endian (LSB first) order, if false, use
     *            big endian (MSB first) byte order.
     */
    public void setLittleEndian(boolean invert) {
        littleEndian = invert;
    }

    /**
     * Sets the RME version in use for this stream
     * 
     * @param rmeVer integer RME version number
     */
    public void setRmeVer(int rmeVer) {
        rmeProtVer = rmeVer;
    }

    /**
     * Gets the RME version number currently in use
     * 
     * @return RME version
     */
    public int getRmeVer() {
        return rmeProtVer;
    }

    /**
     * Tests to see if the RME version in use is greater than or equal to the
     * RME version supplied
     * 
     * @param rmeVer RME version to test
     * @return true if this stream's version is >= the version in use
     */
    public boolean rmeProtAtLeast(int rmeVer) {
        return rmeProtVer >= rmeVer;
    }

    /**
     * Writes the bytes required to do RME authentication Note this is no longer
     * used on most Intermail installs, but is still part of the required
     * handshake.
     * 
     * @param username the username to authenticate
     * @param password the password
     * @throws IOException on any socket error
     */
    public void sendAuth(String username, String password) throws IOException {
        writeString(username);
        writeString(password);
        writeInt(AbstractOperation.RME_NOAUTH);
        return;
    }

    /**
     * Writes a <code>boolean</code> to the underlying output stream as a 1-byte
     * value. The value <code>true</code> is written out as the value
     * <code>(byte)1</code>; the value <code>false</code> is written out as the
     * value <code>(byte)0</code>.
     * 
     * @param v a <code>boolean</code> value to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeBoolean(boolean v) throws IOException {
        write(v ? 1 : 0);
    }

    /**
     * Writes out a <code>byte</code> to the underlying output stream as a
     * 1-byte value.
     * 
     * @param v a <code>byte</code> value to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeByte(int v) throws IOException {
        write(v);
    }

    /**
     * Writes out a <code>byteArray</code> to the underlying output stream as a
     * 1-byte value.
     * 
     * @param v a <code>byte</code> value to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeByteArray(byte[] bytes) throws IOException {
        if (bytes == null) {
            writeInt(0);
            return;
        }
        writeInt(bytes.length);
        write(bytes, 0, bytes.length);
    }
    
    /**
     * Writes a <code>short</code> to the underlying output stream as two bytes,
     * high byte first.
     * 
     * @param v a <code>short</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeShort(int v) throws IOException {
        if (littleEndian) {
            write((v >>> 0) & 0xFF);
            write((v >>> 8) & 0xFF);
        } else {
            write((v >>> 8) & 0xFF);
            write((v >>> 0) & 0xFF);
        }
    }
    
    /**
     * Writes a <code>char</code> to the underlying output stream as a 2-byte
     * value, high byte first.
     * 
     * @param v a <code>char</code> value to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeChar(int v) throws IOException {
        write((v >>> 8) & 0xFF);
        write((v >>> 0) & 0xFF);
    }

    /**
     * Writes an <code>int</code> to the underlying output stream as four bytes,
     * high byte first.
     * 
     * @param v an <code>int</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeInt(int v) throws IOException {
        if (littleEndian) {
            write((v >>> 0) & 0xFF);
            write((v >>> 8) & 0xFF);
            write((v >>> 16) & 0xFF);
            write((v >>> 24) & 0xFF);
        } else {
            write((v >>> 24) & 0xFF);
            write((v >>> 16) & 0xFF);
            write((v >>> 8) & 0xFF);
            write((v >>> 0) & 0xFF);
        }
    }

    /**
     * This writes an empty COS object. Since this object is never used, it is
     * essentially one big constant.
     * 
     * @throws IOException on any IO error.
     */
    public final void writeEmptyCos() throws IOException {
        writeInt(0);// timestamp
        if (rmeProtVer >= AbstractOperation.RME_MX8_VER) {
            // quotaMaxTotalBytes is 64 bit
            // quotaMaxMsgBytes is 64 bit
            writeLong(-1);
            writeLong(-1);
        } else {
            writeInt(-1);
            writeInt(-1);
        }
        writeInt(-1);// notifyQuotaThresh
        writeInt(-1);// notifyBounce
        writeInt(-1);// quotaMaxMsgs
        writeInt(-1); // mers enabled
        writeString("");// mersConfiguration
        writeString("");// mersFileBase
        writeInt(-1);// mersFileSizeKB
        writeInt(-1);// mersMaxHeaderLength
        writeString("");// mailFolderQuota
        if (rmeProtVer >= AbstractOperation.RME_INDEXING_SERVER_VER) {
            writeString(""); // mailIndexHost
        }
        if (rmeProtVer >= AbstractOperation.RME_REMOTE_CALL_TRACING) {
            writeInt(0); // remoteCallTracingEnabled
        }
    }

    /**
     * Writes a <code>long</code> to the underlying output stream as eight
     * bytes, high byte first.
     * 
     * @param v a <code>long</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeLong(long v) throws IOException {
        if (littleEndian) {
            write((int) (v >>> 0) & 0xFF);
            write((int) (v >>> 8) & 0xFF);
            write((int) (v >>> 16) & 0xFF);
            write((int) (v >>> 24) & 0xFF);
            write((int) (v >>> 32) & 0xFF);
            write((int) (v >>> 40) & 0xFF);
            write((int) (v >>> 48) & 0xFF);
            write((int) (v >>> 56) & 0xFF);
        } else {
            write((int) (v >>> 56) & 0xFF);
            write((int) (v >>> 48) & 0xFF);
            write((int) (v >>> 40) & 0xFF);
            write((int) (v >>> 32) & 0xFF);
            write((int) (v >>> 24) & 0xFF);
            write((int) (v >>> 16) & 0xFF);
            write((int) (v >>> 8) & 0xFF);
            write((int) (v >>> 0) & 0xFF);
        }
    }

    /**
     * Writes a <code>longBlob</code> to the underlying output stream as BLOB
     * 
     * @param v a <code>long</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeLongBlob(long v, boolean writeSize) throws IOException {
        if (writeSize){
            writeInt(LONG_SIZE);
        }
        write((int) (v >>> 56) & 0xFF);
        write((int) (v >>> 48) & 0xFF);
        write((int) (v >>> 40) & 0xFF);
        write((int) (v >>> 32) & 0xFF);
        write((int) (v >>> 24) & 0xFF);
        write((int) (v >>> 16) & 0xFF);
        write((int) (v >>> 8) & 0xFF);
        write((int) (v >>> 0) & 0xFF);
    }

    /**
     * Writes a <code>longArray</code> to the underlying output stream as BLOB
     * 
     * @param v a <code>long</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeLongArray(long[] v) throws IOException {
        if (v == null) {
            writeInt(0);
            return;
        }
        int numShorts = v.length;
        for (int i = 0; i < numShorts; i++)
            writeLong(v[i]);
    }
    
    /**
     * Writes a <code>long</code> to the underlying output stream as eight
     * bytes, high byte first.
     * 
     * @param v a <code>long</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeLong128(long v) throws IOException {
        if (littleEndian) {
            write((int) (v >>> 0) & 0xFF);
            write((int) (v >>> 8) & 0xFF);
            write((int) (v >>> 16) & 0xFF);
            write((int) (v >>> 24) & 0xFF);
            write((int) (v >>> 32) & 0xFF);
            write((int) (v >>> 40) & 0xFF);
            write((int) (v >>> 48) & 0xFF);
            write((int) (v >>> 56) & 0xFF);
        } else {
            write((int) (v >>> 56) & 0xFF);
            write((int) (v >>> 48) & 0xFF);
            write((int) (v >>> 40) & 0xFF);
            write((int) (v >>> 32) & 0xFF);
            write((int) (v >>> 24) & 0xFF);
            write((int) (v >>> 16) & 0xFF);
            write((int) (v >>> 8) & 0xFF);
            write((int) (v >>> 0) & 0xFF);
        }
    }

    /**
     * Writes a <code>UUID</code> to the underlying output stream
     * 
     * @param v a <code>UUID</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeUUID(UUID v) throws IOException {
        if (littleEndian) {
            writeLong(v.getLeastSignificantBits());
            writeLong(v.getMostSignificantBits());
        } else {
            writeLong(v.getMostSignificantBits());
            writeLong(v.getLeastSignificantBits());
        }
    }
    
    
    /**
     * Writes a <code>UUIDBlob</code> to the underlying output stream as a BLOB
     * 
     * @param v a <code>UUID</code> to be written.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     * @since JDK1.0
     */
    public final void writeUUIDBlob(UUID v) throws IOException {
        writeInt(UUID_SIZE);
        writeLongBlob(v.getMostSignificantBits(), false);
        writeLongBlob(v.getLeastSignificantBits(), false);
    }

    /**
     * writes a unicode String as a UTF-8 encoded byte array. If the String
     * object does not represent a unicode string, the
     * <code>writeStringBytes</code> method must be used instead.
     * 
     * @param strIn a unicode String to send
     * @throws IOException on any IO error
     */
    public void writeString(String strIn) throws IOException {
        if (strIn == null) {
            writeInt(0);
        } else if (strIn.length() == 0) {
            // send the empty string (just null termintated)
            writeInt(1);
            write(0);
        } else {
            writeUTF8String(strIn);
        }
    }

    /**
     * writes a unicode String blob as a UTF-8 encoded byte array. If the String
     * object does not represent a unicode string, the
     * <code>writeStringBytes</code> method must be used instead.
     * 
     * @param strIn a unicode String to send
     * @throws IOException on any IO error
     */
    public void writeStringBlob(String strIn) throws IOException {
        if (strIn == null) {
            writeInt(0);
        } else if (strIn.length() == 0) {
            writeInt(0);
        } else {
            writeUTF8StringBlob(strIn);
        }
    }

    /**
     * Writes an array of unicode Strings.
     * 
     * @param strIn array of unicode Strings
     * @throws IOException on any IO error
     * @see #writeString(String)
     */
    public void writeStringArray(String[] strIn) throws IOException {
        if (strIn == null) {
            writeInt(0);
            return;
        }
        int numStrings = strIn.length;
        writeInt(numStrings);
        for (int i = 0; i < numStrings; i++)
            writeString(strIn[i]);
    }
    
    /**
     * Writes an array blob of unicode Strings.
     * 
     * @param strIn array of unicode Strings
     * @throws IOException on any IO error
     * @see #writeString(String)
     */
    public void writeStringArrayBlob(String[] strIn) throws IOException {
        if (strIn == null) {
            writeInt(0);
            return;
        }
        int numStrings = strIn.length;
        writeInt(numStrings);
        for (int i = 0; i < numStrings; i++)
            writeStringBlob(strIn[i]);
    }

    /**
     * Writes a byte array as an RME string. Use this method if your string is
     * not a UTF-8 string.
     * 
     * @param bytes the string bytes
     * @throws IOException on any IO error
     */
    public void writeStringBytes(byte[] bytes) throws IOException {
        writeInt(bytes.length + 1);
        write(bytes, 0, bytes.length);
        write(0);
    }
    
    
    /**
     * Writes a short array as an RME string.
     * 
     * @param array Integer array
     * @throws IOException on any IO error
     */
    public void writeShortArray(short[] array) throws IOException {
        int numElements = 0;
        if (array != null) {
            numElements = array.length;
        }
        writeInt(numElements);
        for (int i = 0; i < numElements; i++) {
            writeShort(array[i]);
        }
    }
    

    /**
     * Writes a integer array as an RME string.
     * 
     * @param array Integer array
     * @throws IOException on any IO error
     */
    public void writeIntArray(int[] array) throws IOException {
        int numElements = 0;
        if (array != null) {
            numElements = array.length;
        }
        writeInt(numElements);
        for (int i = 0; i < numElements; i++) {
            writeInt(array[i]);
        }
    }

    /**
     * Writes a UUID array as an RME string.
     * 
     * @param array UUID array
     * @throws IOException on any IO error
     */
    public void writeUUIDArray(UUID[] array) throws IOException {
        int numElements = 0;
        if (array != null) {
            numElements = array.length;
        }
        writeInt(numElements);
        for (int i = 0; i < numElements; i++) {
            writeUUID(array[i]);
        }
    }

    /**
     * Writes a LogEvent reference to the RME string. This method is responsible
     * for handling the "pointer" part of the data, while the LogEvent object
     * itself is responsible for writing the "struct" part of it's own data.
     * 
     * @param log the LogEvent to write
     * @throws IOException on any IO error
     */
    public void writeLogEvent(LogEvent log) throws IOException {
        if (log != null) {
            writeBoolean(true);
            log.send(this);
        } else {
            writeBoolean(false);
        }
    }

    /**
     * Flush the internal buffer
     * 
     * @throws IOException on any IO error
     */
    private void flushBuffer() throws IOException {
        if (count > 0) {
            out.write(buf, 0, count);
            count = 0;
        }
    }

    /**
     * Writes the specified byte to this buffered output stream.
     * 
     * @param b the byte to be written.
     * @exception IOException if an I/O error occurs.
     * @since JDK1.0
     */
    public void write(int b) throws IOException {
        if (count >= buf.length) {
            flushBuffer();
        }
        buf[count++] = (byte) b;
    }

    /**
     * Writes <code>len</code> bytes from the specified byte array starting at
     * offset <code>off</code> to this buffered output stream.
     * 
     * <p>
     * Ordinarily this method stores bytes from the given array into this
     * stream's buffer, flushing the buffer to the underlying output stream as
     * needed. If the requested length is at least as large as this stream's
     * buffer, however, then this method will flush the buffer and write the
     * bytes directly to the underlying output stream. Thus redundant
     * <code>BufferedOutputStream</code> s will not copy data unnecessarily.
     * 
     * @param b the data.
     * @param off the start offset in the data.
     * @param len the number of bytes to write.
     * @exception IOException if an I/O error occurs.
     */
    public void write(byte b[], int off, int len) throws IOException {
        if (len >= buf.length) {
            /*
             * If the request length exceeds the size of the output buffer,
             * flush the output buffer and then write the data directly. In this
             * way buffered streams will cascade harmlessly.
             */
            flushBuffer();
            out.write(b, off, len);
            return;
        }
        if (len > buf.length - count) {
            flushBuffer();
        }
        System.arraycopy(b, off, buf, count, len);
        count += len;
    }

    /**
     * This method will take a string and write out its bytes using the UTF-8
     * encoding. The one exception is that null characters will be stripped.
     * 
     * @param inString
     */
    private void writeUTF8String(String inString) throws IOException {

        byte[] tempArray = new byte[inString.length() * 3];
        int byteIndex = 0;
        for (int i = 0; i < inString.length(); i++) {
            char c = inString.charAt(i);
            if (c == 0)
                continue;
            if (c < 0x80) {
                tempArray[byteIndex++] = (byte) c;
            } else if (c < 0x800) {
                tempArray[byteIndex++] = (byte) (0xC0 | c >> 6);
                tempArray[byteIndex++] = (byte) (0x80 | c & 0x3F);
            } else {
                tempArray[byteIndex++] = (byte) (0xE0 | c >> 12);
                tempArray[byteIndex++] = (byte) (0x80 | c >> 6 & 0x3F);
                tempArray[byteIndex++] = (byte) (0x80 | c & 0x3F);
            }
        }
        writeInt(byteIndex + 1);
        write(tempArray, 0, byteIndex);
        write(0);// always null terminate
    }

    /**
     * This method will take a string and write out its bytes using the UTF-8
     * encoding blob. The one exception is that null characters will be stripped.
     * 
     * @param inString
     */
    private void writeUTF8StringBlob(String inString) throws IOException {

        byte[] tempArray = new byte[inString.length() * 3];
        int byteIndex = 0;
        for (int i = 0; i < inString.length(); i++) {
            char c = inString.charAt(i);
            if (c == 0)
                continue;
            if (c < 0x80) {
                tempArray[byteIndex++] = (byte) c;
            } else if (c < 0x800) {
                tempArray[byteIndex++] = (byte) (0xC0 | c >> 6);
                tempArray[byteIndex++] = (byte) (0x80 | c & 0x3F);
            } else {
                tempArray[byteIndex++] = (byte) (0xE0 | c >> 12);
                tempArray[byteIndex++] = (byte) (0x80 | c >> 6 & 0x3F);
                tempArray[byteIndex++] = (byte) (0x80 | c & 0x3F);
            }
        }
        writeInt(byteIndex);
        write(tempArray, 0, byteIndex);
    }

    /**
     * writes the String as a UTF-8 encoded byte array with no null termination
     * 
     * @param strIn String to send
     * @throws IOException on any IO error
     */
    public void writeStringBytes(String strIn) throws IOException {
        if (strIn == null) {
            return;
        } else if (strIn.length() == 0) {
            return;
        } else {
            byte[] tempArray = new byte[strIn.length() * 3];
            int byteIndex = 0;
            for (int i = 0; i < strIn.length(); i++) {
                char c = strIn.charAt(i);
                if (c == 0)
                    continue;
                if (c < 0x80) {
                    tempArray[byteIndex++] = (byte) c;
                } else if (c < 0x800) {
                    tempArray[byteIndex++] = (byte) (0xC0 | c >> 6);
                    tempArray[byteIndex++] = (byte) (0x80 | c & 0x3F);
                } else {
                    tempArray[byteIndex++] = (byte) (0xE0 | c >> 12);
                    tempArray[byteIndex++] = (byte) (0x80 | c >> 6 & 0x3F);
                    tempArray[byteIndex++] = (byte) (0x80 | c & 0x3F);
                }
            }
            writeInt(byteIndex);
            write(tempArray, 0, byteIndex);
        }
    }

    /**
     * Flushes this buffered output stream. This forces any buffered output
     * bytes to be written out to the underlying output stream.
     * 
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterOutputStream#out
     */
    public void flush() throws IOException {
        flushBuffer();
        out.flush();
    }

}