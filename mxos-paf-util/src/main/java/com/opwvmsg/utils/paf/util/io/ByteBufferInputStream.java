/**
 * Copyright 2006 Openwave Systems Inc.  All Rights Reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/io/ByteBufferInputStream.java#1 $
 */

package com.opwvmsg.utils.paf.util.io;

import java.io.InputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.log4j.Logger;

/**
 * A <code>ByteBufferInputStream</code> contains an internal buffer
 * that contains bytes that may be read from the stream.
 * <p>
 * Closing a <code>ByteBufferInputStream</code> has no effect.  The
 * methods in this class can be called after the stream has been
 * closed without generating an <code>IOException</code>. 
 *
 * @author Forrest Girouard
 * @reveision $Revision: #1 $
 */
public class ByteBufferInputStream extends InputStream {
    private static final Logger logger =
            Logger.getLogger(ByteBufferInputStream.class);
    protected final ByteBuffer buffer;
    private int markPosition;
    private int readLimit;
    private int readCount;

    /**
     * Creates a <code>ByteBufferInputStream</code> so that it uses
     * <code>buffer</code> as its byte buffer. The byte buffer is not
     * copied. The initial value of <code>offset</code> is 0 and the
     * initial value of <code>length</code> is the btyes remaining in
     * <code>buffer</code>.
     *
     * @param buffer the input buffer.
     */
    public ByteBufferInputStream(final ByteBuffer buffer) {
        this.buffer = buffer;
        if (logger.isDebugEnabled()) {
            logger.debug("position: " + buffer.position());
            logger.debug("remaining: " + buffer.remaining());
            logger.debug("capacity: " + buffer.capacity());
            logger.debug("limit: " + buffer.limit());
        }
        buffer.limit(buffer.capacity());
        buffer.position(0);
    }

    /**
     * Creates a <code>ByteBufferInputStream</code> so that it uses
     * <code>buffer</code> as its byte buffer. The byte buffer is not
     * copied. The initial value of <code>offset</code> is 0 and the
     * initial value of <code>length</code> is the btyes remaining in
     * <code>buffer</code>.
     *
     * @param buffer the input buffer.
     * @param offset the offset in the buffer of the first byte to read.
     * @param length the maximum number of bytes to read from the buffer.
     */
    public ByteBufferInputStream(final ByteBuffer buffer,
            int offset, int length) {
        this(buffer);
        if (logger.isDebugEnabled()) {
            logger.debug("offset: " + offset);
            logger.debug("length: " + length);
        }
        if (offset > buffer.capacity() || offset < 0) {
            throw new IllegalArgumentException("buffer.capacity: "
                    + buffer.capacity() + ": invalid offset: " + offset);
        }
        if (offset + length > buffer.capacity() || length < 0) {
            throw new IllegalArgumentException(
                    "buffer.capacity: " + buffer.capacity()
                    + ": invalid length: " + length
                    + ": offset: " + offset);
        }
        buffer.limit(offset + length);
        buffer.position(offset);
    }

    /**
     * Reads the next byte of data from this input stream. The value
     * byte is returned as an int in the range <code>0</code> to
     * <code>255</code>. If no byte is available because the end of
     * the stream has been reached, the value <code>-1</code> is
     * returned.
     *
     * @return the next byte of data, or <code>-1</code> if the end
     *         of the stream has been reached.
     */
    public synchronized int read() throws IOException {
        if (buffer.position() >= buffer.limit()) {
            return -1;
        }
        readCount++;
        return buffer.get() & 0xff;
    }
    
    /**
     * Reads up to <code>length<code> bytes of data into an array of
     * bytes from this input stream. If <code>offset</code> equals
     * <code>length</code>, then <code>-1</code> is returned to
     * indicate end of file. Otherwise, the number <code>k</code> of
     * bytes read is equal to the smaller of <code>len</code> and
     * <code>length-offset</code>. If <code>k</code> is positive, then
     * bytes <code>buf[offset]</code> through
     * <code>buf[offset+k-1]</code> are copied into <code>b[off]</code>
     * through <code>b[off+k-1]</code> in the manner performed by
     * <code>System.arraycopy</code>. The value <code>k</code> is added
     * into <code>offset</code>  and <code>k</code> is returned.
     * <p>
     * This <code>read</code> method cannot block. 
     *
     * @param buffer the buffer into which the data is read.
     * @param offset the start offset of the data.
     * @param length the maximum number of bytes read.
     * @return the total number of bytes read into the buffer, or
     *         <code>-1</code> if there is no more data because the
     *         end of the stream has been reached.
     */
    public synchronized int read(byte[] bytes, int offset, int length)
            throws IOException {
        // Read only what's left
        length = Math.min(length, buffer.remaining());
        if (length == 0) {
            length = -1;
        } else {
            buffer.get(bytes, offset, length);
            readCount += length;
        }
        return length;
    }

    /**
     * Returns the number of bytes that can be read (or skipped over)
     * from this input stream without blocking by the next caller of
     * a method for this input stream. The next caller might be the
     * same thread or another thread.
     * <p>
     * @return the number of bytes that can be read from this input
     *         stream without blocking.
     */
    public int available() throws IOException {
        return buffer.remaining();
    }

    /**
     * Marks the current position in this input stream. A subsequent
     * call to the <code>reset</code> method repositions this stream
     * at the last marked position so that subsequent reads re-read
     * the same bytes.
     * <p>
     * The <code>readlimit</code> arguments tells this input stream
     * to allow that many bytes to be read before the mark position
     * gets invalidated.
     * <p>
     * The general contract of <code>mark</code> is that, if the
     * method <code>markSupported</code> returns <code>true</code>,
     * the stream somehow remembers all the bytes read after the call
     * to <code>mark</code> and stands ready to supply those same
     * bytes again if and whenever the method <code>reset</code> is
     * called. However, the stream is not required to remember any
     * data at all if more than <code>readlimit</code> bytes are read
     * from the stream before <code>reset</code> is called. 
     *
     * @param readlimit the maximum limit of bytes that can be read
     *        before the mark position becomes invalid.
     */
    public void mark(int readLimit) {
        this.readLimit = readLimit;
        markPosition = buffer.position();
        readCount = 0;
    }

    /**
     * Tests if this input stream supports the <code>mark<code> and
     * <code>reset<code> methods. Whether or not <code>mark<code> and
     * <code>reset<code> are supported is an invariant property of a
     * particular input stream instance. The
     * <code>markSupported<code> method of
     * <code>ByteBufferInputStream<code> returns <code>true</code>.
     * <p>
     * @return <code>true<code> if this stream instance supports the
     *         <code>mark<code> and <code>reset<code> methods;
     *         <code>false<code> otherwise.
     */
    public boolean markSupported() {
        return true;
    }

    /**
     * Repositions this stream to the position at the time the
     * <code>mark<code> method was last called on this input stream.
     * <p>
     * If the method <code>mark</code> has not been called since the
     * stream was created, or the number of bytes read from the
     * stream since <code>mark<code> was last called is larger than
     * the argument to <code>mark</code> at that last call, then an
     * <code>IOException</code> will be thrown.
     * <p>
     * @throw java.io.IOException - if this stream has not been
     *        marked or if the mark has been invalidated.
     */
    public void reset() throws IOException {
        if (readLimit == -1) {
            throw new IOException("invaild mark");
        } else if (readCount > readLimit) {
            throw new IOException("readLimit exceeded");
        } else {
            buffer.position(markPosition);
        }
    }

    /**
     * Skips over and discards <code>n</code> bytes of data from this
     * input stream. The <code>skip</code> method may, for a variety
     * of reasons, end up skipping over some smaller number of bytes,
     * possibly <code>0</code>. This may result from any of a number
     * of conditions; reaching end of file before <code>n</code>
     * bytes have been skipped is only one possibility. The actual
     * number of bytes skipped is returned. If <code>n</code> is
     * negative, no bytes are skipped.
     *
     * @param n the number of bytes to be skipped.
     * @return the actual number of bytes skipped.
     @ throws IOException if an I/O error occurs.
     */
    public long skip(long n) throws IOException {
        long skipBytes = Math.min(n, buffer.remaining());
        buffer.position((int)(buffer.position() + skipBytes));
        return skipBytes;
    }

    /**
     * Returns this stream's position.
     *
     * @return The position of this stream.
     */
    public int position() {
        return buffer.position();
    }
}
