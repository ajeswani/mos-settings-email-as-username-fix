/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems, Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: 
 */
package com.opwvmsg.utils.paf.intermail.rme;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeServerSocket;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;

public abstract class AbstractRequestHandler extends Thread {

    private RmeServerSocket serverSock = null;
    private RmeSocket curSock = null;
    protected RmeInputStream inStream = null;
    protected RmeOutputStream outStream = null;
    protected String handlerName = null;

    private boolean requireClientIndex = false;
    private int currentOperationId = -1;

    private static final Logger logger = Logger.getLogger(AbstractRequestHandler.class);

    /**
     * Initializing with this constructor will make a request handler thread
     * that can listen on a port to accept connections. When the connection
     * closes, it listens on the port again for a new connection.
     * 
     * @param name a unique name for the Handler
     * @param serverSockIn an initialized RmeServerSocket
     */
    protected AbstractRequestHandler(String name, RmeServerSocket serverSockIn) {
        super(name);
        handlerName = name;
        serverSock = serverSockIn;
    }

    /**
     * Initializing with this constructor will make a request handler thread
     * that only works on the current (passed in) socket. It will keep
     * responding to requests on the socket as long as the socket stays open. If
     * the socket closes, the thread exits. This is useful for the config
     * workers, where a client connection is needed to start the work, but then
     * the socket changes into a server connection.
     * 
     * @param name a unique name for the handler
     * @param sockIn the open socket that is ready to listen to requests
     */
    protected AbstractRequestHandler(String name, RmeSocket sockIn) {
        super(name);
        handlerName = name;
        curSock = sockIn;
    }

    /**
     * By default, the request operations are assumed to be SRME requests, which
     * require no client index. To accept RME requests, use this method to set
     * the RequireClientIndex flag to true.
     * 
     * @param clientIndex if true, RME will be used; if false, SRME
     */
    public void setRequireClientIndex(boolean clientIndex) {
        requireClientIndex = clientIndex;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    public void run() {
        boolean error;
        do {
            currentOperationId = -1;
            error = false;
            try {
                if (serverSock != null) {
                    // accept a new rme socket
                    curSock = (RmeSocket) serverSock.accept();
                }

                inStream = (RmeInputStream) curSock.getInputStream();
                outStream = (RmeOutputStream) curSock.getOutputStream();

                while (curSock != null && curSock.isReady()) {
                    // RmeSocket is valid, start listening
                    boolean ok = true;

                    int status = 0;

                    int ctrl;
                    int retStat = 0;

                    ctrl = inStream.read();
                    if (ctrl == -1) {
                        throw new IntermailException(LogEvent.ERROR,
                                LogEvent.formatLogString("Nio.SocketClosed",
                                    new String[] { "Handler: " + handlerName }));
                    }
                    if (requireClientIndex) {
                        int ndx = inStream.readInt();
                    }

                    switch (ctrl) {
                    case AbstractOperation.RME_OP:
                        currentOperationId = inStream.readInt();
                        processRequest(currentOperationId);
                        break;
                    case AbstractOperation.RME_SYNC_DATA:
                        AbstractOperation.readTrailer(inStream);
                        break;
                    case AbstractOperation.RME_CLOSE:
                        curSock.close();
                        break;
                    default:
                        throw new IntermailException(
                                LogEvent.ERROR,
                                LogEvent.formatLogString(
                                    "Rme.ProtocolBadResponse",
                                    new String[] { handlerName + "-Bad Control" }));
                    }
                }
            } catch (IOException ioe) {
                error = true;
                logger.log(Level.ERROR, LogEvent.formatLogString(
                    "Nio.SocketClosed",
                    new String[] { "Handler: " + handlerName + ", IOException: "
                            + ioe.getMessage() }));
            } catch (IntermailException ie) {
                error = true;
                logger.log(ie.getLog4jLevel(), ie.getFormattedString());
            } finally {
                if (error && curSock != null) {
                    try {
                        curSock.close();
                    } catch (IOException io2) {
                    }
                }
            }

        } while (serverSock != null);

    }

    /**
     * Each handler will override this method to implement the logic to actually
     * handle each specific request.
     * 
     * @param id the operation ID
     * @throws IOException on any io error
     * @throws IntermailException on any RME error
     */
    protected abstract void processRequest(int id) throws IOException,
            IntermailException;

    /**
     * A convenience method so processRequest can handle both request and reply
     * and not have to worry about the rme details in between
     * 
     * @throws IOException
     * @throws IntermailException
     */
    protected void finishRequest() throws IOException, IntermailException {

        int ctrl = inStream.read();
        if (ctrl != AbstractOperation.RME_SYNC_DATA) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Rme.ProtocolBadResponse",
                        new String[] { handlerName + "-ID:"
                                + currentOperationId }));
        }
        if (!AbstractOperation.readTrailer(inStream)) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Rme.ProtocolBadResponse",
                        new String[] { handlerName + "-ID:"
                                + currentOperationId }));
        }

    }
    
}
