/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;

/**
 * Attribute descriptor used for decoding attributes. Holds the attribute Id and
 * its data type that shall be returned by MSS
 * 
 * @author mOS-dev
 */
public class AttributeDescription {
    short attributeId;
    short dataType;

    public AttributeDescription(RmeInputStream inStream) throws IOException {
        attributeId = inStream.readShort();
        dataType = inStream.readShort();
    }

    /**
     * @return the attributeId
     */
    public short getAttributeId() {
        return attributeId;
    }

    /**
     * @param attributeId the attributeId to set
     */
    public void setAttributeId(short attributeId) {
        this.attributeId = attributeId;
    }

    /**
     * @return the dataType
     */
    public short getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(short dataType) {
        this.dataType = dataType;
    }

};