/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/DeleteMsgs.java#1 $ 
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation will delete the specified messages
 */
public class DeleteMsgs extends AbstractMssOperation {

    // input
    private String name;
    private int[] uids;
    private String[] messageIds;
    private int options;
    private int accessId;
    private String fromPathname;

    // output
    // -- no output --

    // internal
    private String url;
    private String[] fromPathnameArray;

    // constants
    public static final int DELETE_MSGS_FOLDER_IS_HINT = 0x01;
    public static final int DELETE_MSGS_MULTIPLE_OK = 0x02;
    public static final int DELETE_MSGS_SUPPRESS_MERS = 0x04;

    /**
     * Constructor for DeleteMsgs
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param fromPathname The folder where the deleted messages reside
     * @param uids the list of uids to delete
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            DELETE_MSGS_FOLDER_IS_HINT - messages may not be in specified
     *            src folder <br>
     *            DELETE_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            DELETE_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     */
    public DeleteMsgs(String host, String name, String fromPathname,
            int[] uids, int accessId, int options) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.fromPathname = fromPathname;
        url = getMsUrl(host, name, MSS_P_CL_DELETEMSGS);
        fromPathnameArray = buildPathnameArray(fromPathname);
        this.uids = uids;
        this.accessId = accessId;
        this.options = options;
    }

    /**
     * Constructor for DeleteMsgs
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param fromPathname The folder where the deleted messages reside
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            DELETE_MSGS_FOLDER_IS_HINT - messages may not be in specified
     *            src folder <br>
     *            DELETE_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            DELETE_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     * @param messageIds the list of messageIds to delete
     */
    public DeleteMsgs(String host, String name, String fromPathname,
            int accessId, int options, String[] messageIds) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.fromPathname = fromPathname;
        url = getMsUrl(host, name, MSS_P_CL_DELETEMSGS);
        fromPathnameArray = buildPathnameArray(fromPathname);
        this.messageIds = messageIds;
        this.accessId = accessId;
        this.options = options;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (fromPathname == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "fromPathname", "DeleteMsgs" }));
        }
        if (uids == null) {
            uids = new int[0];
        }
        if (messageIds == null) {
            messageIds = new String[0];
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_CL_DELETEMSGS);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_P_CL_DELETEMSGS", host,
                            "dataModel=" + rmeDataModel }));
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(fromPathnameArray);

        /**
         * If initialized by passing messageIds then the first condition is
         * executed which deletes the messages with given messageIds from
         * folder.
         * 
         * If initialized with uids or empty the else condition gets executed.
         * The messages with given uids gets deleted and if empty all the
         * messages from folder gets deleted.
         */
        if (messageIds != null && messageIds.length > 0) {
            outStream.writeInt(messageIds.length);
            for (int i = 0; i < messageIds.length; i++) {
                outStream.write(1);
                outStream.writeInt(-1); // uids
                outStream.writeString(messageIds[i]); // msgids
            }
        } else {
            outStream.writeInt(uids.length);
            for (int i = 0; i < uids.length; i++) {
                outStream.write(1);
                outStream.writeInt(uids[i]); // uids
                outStream.writeString(null); // msgids
            }
        }
        outStream.writeInt(accessId);
        outStream.writeInt(options);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        logEvents = inStream.readLogEventArray();
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation is not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation is not supported
    }
}
