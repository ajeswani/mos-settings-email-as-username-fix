/*
 * Copyright (c) 2005 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/intermail/IntermailConfigProvider.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config.intermail;

import java.util.Map;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.config.spi.ConfigProvider;

/**
 * This class provides access to configuration information retrieved through
 *  the intermail config.db file.
 * <p>
 * Configuration data is identified by keys consisting of host, service, and 
 * name. 
 *
 * @author Jeff Levine
 * @version $Revision: #1 $
 */
public class IntermailConfigProvider implements ConfigProvider {

    /**
     * Gets an IntermailConfig object using the given setup information.
     * <p>
     * @param setup a map with any elements required to initialize the config provider, 
     *        "intermailDir" an alternate $INTERMAIL directory to use.
     *              If missing, the system property INTERMAIL will be used instead.
     *              The config.db is expected to be in $INTERMAIL/config/config.db
     *              If the directory $INTERMAIL/conifg exists, but config.db doesn't,
     *              then a new config.db will be created when the app loads.  This
     *              will speed up subsequent startups.  Otherwise the config information
     *              will be downloaded from the config server at startup.
     *        "confServHost" the config server host (if config.db isn't found)
     *        "confServPort" the config server port (if config.db isn't found)
     *              These two parameters are only required if a local config.db
     *              cannot be found.  They will only be used as the last resort;
     *              i.e any config.db values will take precendence over these keys.
     *              (If your config.db is out of date, it will automatically be updated,
     *               But if your out of date conifg.db contains the wrong confServHost/Port
     *               then you should delete it and use these two keys.
     *        "defaultHost" (the name for this host) If not specified, then the 
     *              default host will be determined by $INTERMAIL/config/hostname
     *              or the InetAddress.getLocalHost, in that order.
     *        "defaultApp" (the name for this app) If this is missing, the default app
     *              will be "common"
     *        "retryInterval" the wait time (in ms) before re-contacting the
     *                        server after an error.  Default is 15000 (15 seconds)
     *        "anacapaHost" the anacapa host name 
     *        "anacapaPort" the anacapa port name
     *               Anacapa host/port values are not true config keys, but are set 
     *               during the servlet initialization.  This config provider does not
     *               use them, but if they are specified, will present them as config keys.
     *               This is only required if there are other modules in your app that will
     *               still use anacapa for certain operations.
     *        "forceConfigDbWrite" if this key is set to any value it will force the
     *               config.db to be written, even if it's on the same host as the confServ.
     *               This key is only useful for sandboxes where the defaultHost may be
     *               set to the same hostname as the confserv, even when that sandbox
     *               is not really on the confServ Host.  Setting this key in a sandbox
     *               ensures that config.db updates will be written to your local file.
     *               This key should never be set in a production environment.
     *        "msgcatProperties" this key will set a location for the msgcat.properties file that 
     *               overrrides the value in the config.db.  This key is only 
     *               useful in sandbox situations where several instances of 
     *               webedge may share the same virtual host, but have different
     *               magcat.properties locations.  This key is also required if 
     *               you do not have a local config.db file, since the msgcat
     *               properties file is needed to properly report errors during
     *               the initialization of the config module.
     *        "localConfig"  This key is only useful for sandboxes and should
     *               never be used in production.  With this key set to true,
     *               the conf sever will normally not be contacted, and updates
     *               will not be received.  Also the checksum on the config.db
     *               file is ignored, allowing direct editing of the local 
     *               config.db file.  The only time the conf server may be
     *               contacted is if the config.db cannot be found, and
     *               a confservhost/port are specified.  In that case 
     *               a new config.db will be downloaded from the conf server,
     *               but once the new config.db is downloaded, the connection
     *               will be closed.
     * @return an IntermailConfig object
     * @throws ConfigException if the combination of environment and config keys supplied 
     *        is not enough to resolve a valid config database.
     */
    public Config getConfig (Map setup) throws ConfigException {
        return new IntermailConfig(setup);
    }

    /**
     * Tests whether this provider is capable of providing the 
     * Config API for the "intermail" provider type.
     *
     * @param type the desired type of provider
     *
     * @return <code>true</code> if the named type is handled by
     *         this provider, else <code>false</code>
     */
    public boolean isType(String type) {
        return type.equalsIgnoreCase("intermail");
    }
}
