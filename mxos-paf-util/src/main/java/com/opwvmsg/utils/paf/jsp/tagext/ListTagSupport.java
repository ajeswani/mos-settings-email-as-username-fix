/*
 * Copyright (c) 2002 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/jsp/tagext/ListTagSupport.java#1 $
 */

package com.opwvmsg.utils.paf.jsp.tagext;


/**
 * A generic list control tag.
 *
 * <p>
 *  TODO: this class should implement ListIterator interface.
 * </p>

 * @author Lee-Sung Chouanard
 * @author Vam Makam
 * @version $Revision: #1 $
 */
public class ListTagSupport extends BodyTagSupport {
    protected int maxItems;

    /**
     * Setter for tag parameter.
     * @param maxItems Maximum number of items in a page.
     */
    public void setMaxItems(String maxItems) {
        this.maxItems = Integer.parseInt(maxItems);
    }
    
    public int getMaxItems() {
        return maxItems;
    }

}
