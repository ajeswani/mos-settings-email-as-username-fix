/*
 * H+ Copyright 2004 Openwave Systems, Inc. All Rights Reserved. The copyright
 * to the computer software herein is the property of Openwave Systems, Inc. The
 * software may be used and/or copied only with the written permission of
 * Openwave Systems, Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/
 * AbstractMssOperation.java#2 $ H-
 */
package com.opwvmsg.utils.paf.intermail.mail;

import static com.opwvmsg.utils.paf.config.Config.ClusterSiteMode;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.http.HTTPConnectionManager;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;
import com.opwvmsg.utils.paf.util.Utilities;

import org.apache.log4j.Logger;

/**
 * This class extends AbstractOperation and implements connection specific
 * methods for an Mss operation.
 */
public abstract class AbstractMssOperation extends AbstractOperation {

    private static final Logger logger = Logger
            .getLogger(AbstractMssOperation.class);

    // every mss rme op must specify a host
    protected String host = null;
    private int mssPort = 0;

    // OUTBOUND MSS RME ID's we implement
    protected final static int MSS_P_CREATEMS = 1;
    protected final static int MSS_P_RESOLVEMS = 2;
    protected final static int MSS_P_DELETEMS = 5;

    protected final static int MSS_P_STATISTIC = 7;

    protected final static int MSS_P_INITAUTOREPLY = 17;
    protected final static int MSS_P_FETCHAUTOREPLY = 18;

    protected final static int MSS_P_LOCKMAILBOX = 26;
    protected final static int MSS_P_CL_CREATEFOLDER = 39;
    protected final static int MSS_P_CL_REMOVEFOLDER = 40;
    protected final static int MSS_P_CL_READFOLDER = 41;
    protected final static int MSS_P_CL_RENAMEFOLDER = 42;
    protected final static int MSS_P_CL_CREATEMSG = 43;
    protected final static int MSS_P_CL_DELETEMSGS = 44;
    protected final static int MSS_P_CL_RETRIEVEMSGS = 45;
    protected final static int MSS_P_CL_COPYMSGS = 46;
    protected final static int MSS_P_GETMAILBOXLOCKSTATE = 57;
    protected final static int MSS_P_CL_RETRIEVEDOCIDINFO = 62;
    protected final static int MSS_P_CL_MBOXLASTACCESSTIME = 64;
    protected final static int MSS_P_CL_UPDATEMSGCONTENT = 86;
    
    //Stateless RMEs
    protected final static int MSS_SL_GETMSGSTREAM =65;
    protected final static int MSS_SL_REMOVEMULTIMSGS =66;
    protected final static int MSS_SL_STOREMSGFLAGS = 67;
    protected final static int  MSS_SL_POPLIST = 68;
    protected final static int MSS_SL_RETRIEVESORTEDMSGS = 69;
    protected final static int MSS_SL_MAILBOXINFO = 70;
    protected final static int MSS_SL_IMAPLISTUIDS = 72;
    protected final static int MSS_SL_CREATEMS = 74;
    protected final static int MS_SL_CREATEMESSAGE = 28;
    protected final static int MSS_SL_IMAPSELECT = 71;
    protected final static int MSS_SL_MAILBOXMAINTENANCE = 73;
    protected final static int MSS_SL_CREATEFOLDER = 75;
    protected final static int MSS_SL_DELETEMS = 76;
    protected final static int MSS_SL_CREATEMESSAGE = 77;
    protected final static int MSS_SL_COPYMESSAGES = 78;
    protected final static int MSS_SL_FETCHAUTOREPLY = 79;
    protected final static int MSS_SL_OPERATEAUTOREPLY = 80;
    protected final static int MSS_SL_DELETEFOLDER = 83;
    protected final static int MSS_SL_GETMESSAGEATTRIBUTES = 84;
    protected final static int MSS_SL_RENAMEFOLDER = 85;
    protected final static int MSS_SL_SUBSCRIBE = 87;
    protected final static int MSS_SL_MAILBOXACCESSINFO = 88;
    protected final static int MSS_SL_UPDATEMAILBOXACCESSINFO = 89;
    protected final static int MSS_SL_UPDATEMSGCONTENT = 90;
    


    private String clusterName = null;
    private String mailboxId = null;
    private int mailboxBucket = 0;

    protected RmeDataModel rmeDataModel;

    private static final String DISABLE_REDIRECT_TO_SURROGATE_MSS = "disableRedirectToSurrogateMSS";

    private static final int MAX_BUCKETS = 1000;
    private static Map mssHostsTable = null;
    private static boolean isStatelessMSS = true;
    
    public final static String HTTP_CONTENT_OCTET_STREAM = "application/octet-stream";
    public final static String HTTP_RMECLASS_1 = "1";
    public final static String HTTP_RMEVERSION = "175";
    
    /**
     * disabled
     */
    AbstractMssOperation() {
    }
    
    /**
     * AbstractMssOperation constructor.
     * @param rmeDataModel RME data model that the operator uses.
     *                     The operator uses this information for controlling and configuring
     *                     RME execution.  Currently, operator supports CL (ConnectionLess)
     *                     and  Leopard (also called StateLess).
     *                     The data model primarily determines set of RME operations to use.
     *                     However, an RME operation set supports its own RME protocol type.
     *                     For example, Email Mx9.0 MSS only supports Leopard data model with
     *                     protocol HttpRME.  So the data model also determines RME protocol to use.
     */
    public AbstractMssOperation(final RmeDataModel rmeDataModel) {
        this.rmeDataModel = rmeDataModel;
        // we assume CL always comes with regular RME and Leopard with HttpRME.
        this.rmeProtocolType = (rmeDataModel == RmeDataModel.Leopard) ? RmeProtocolType.HTTP : RmeProtocolType.REGULAR; 
    }
    

    // ***********************************************************************
    /**
     * 
     * @param host
     * @param name
     * @param opId
     * @return ms URL
     */
    protected static String getMsUrl(String host, String name, int opId) {
        return "ms://" + host + "/DB/" + name;
    }

    /**
     * 
     * @param host
     * @param name
     * @param realm
     * @return hostKey
     */
    protected static String getHostHeader(String host, String name, String realm) {
        if (realm != null) {
            return name + "." + host + "." + realm;
        }
        return name + "." + host;
    }

    /**
     * Given a "/" separated pathname, return a string array with each folder as
     * an element in the array. This structure is required for transmission of a
     * pathname over RME.
     * 
     * @param pathIn the "/" separated pathname
     * @return a string array represeting the path
     */
    protected static String[] buildPathnameArray(String pathIn) {
        if (pathIn == null) {
            return null;
        }
        String[] pArray = Utilities.dsvToArray(pathIn, '/', false);
        return pArray;
    }
    
    /**
     * RME operation execution with data model sanity check.
     * @param dataModel The RME data model the caller is using.
     * @throws IntermailException thrown when caller and operator use different RME data model.
     *                            Following normal execute() may throw the exception as well.
     * @see com.openwave.intermail.io.AbstractOperation#execute() 
     */
    public void execute(RmeDataModel dataModel)
        throws IntermailException
    {
        if (dataModel != this.rmeDataModel) {
            throw new IntermailException(LogEvent.URGENT,
                    LogEvent.formatLogString("Ms.RmeDataModelMismatch", new String[] {
                            "callerModel=" + dataModel, "operatorModel=" + this.rmeDataModel }));
        }
        execute();
    }
    
    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#executeRmeOp(int)
     */
    protected void callRme(int id) throws IntermailException {
        callRme(id, false);
    }

    /**
     * exceute the RME operation using the default MSS client id.
     * 
     * @param opId the RME operation ID
     * @param saveConnection if false, this is not connection-less API and the
     *            connection will not be released to the pool when it completes.
     *            if true, the connection will be returned to the pool upon
     *            completion of the operation
     * @throws IntermailException on any IO error
     */
    protected void callRme(int opId, boolean saveConnection)
            throws IntermailException {
        // implement mss redirection
        while (true) {
            try {
                callRme(opId, AbstractOperation.RME_SEND_DEFAULT_CLIENT_INDEX,
                        saveConnection);
            } catch (IntermailException ie) {
                // check for MsLoadedByOtherMSS
                LogEvent event = getLogEvent();
                if (event != null
                        && event.getMsgId() == LogEvent
                                .getError("Ms.LoadedByOtherMSS")) {
                    // get mss port from log args
                    try {
                        mssPort = Integer.parseInt(event.getArg(0));
                    } catch (NumberFormatException nfe) {
                        // throw an exception about invalid mss port
                        throw new IntermailException(
                                LogEvent.ERROR,
                                "The MSS referred the operation to another MSS, "
                                        + "but specified an improperly formatted port number: "
                                        + event.getArg(0));
                    }
                    String newHost = event.getArg(1);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Response MsLoadedByOtherMSS from host "
                                + host + ", Redirecting to that MSS host "
                                + newHost);
                    }
                    if (newHost != null && newHost.length() > 0) {
                        // if there is an alternate host specified, use it
                        host = newHost;
                    }
                    continue;
                }
                throw ie;
            }
            break;
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#getConnection()
     */
    protected void connect(boolean usePool) throws IntermailException {
        // test connection
        if (rmeSocket == null || !rmeSocket.isReady()) {
            // get new rsock from pool
            try {
                rmeSocket = ConnectionManager
                        .getRmeSock(host, mssPort, usePool);
            } catch (IntermailException ie) {
                if (logger.isDebugEnabled()) {
                    logger.debug("connect :IntermailException due to "
                            + ie.getFormattedString());
                }

                Config config = Config.getInstance();
                boolean disableRedirectToSurrogateMSS = config.getBoolean(
                        DISABLE_REDIRECT_TO_SURROGATE_MSS, false);
                if (logger.isDebugEnabled()) {
                    logger.debug("disableRedirectToSurrogateMSS =  "
                            + disableRedirectToSurrogateMSS);
                }
                if (!disableRedirectToSurrogateMSS
                        && isStatelessMSS
                        && ie.getFormattedString() != null
                        && ie.getFormattedString().startsWith(
                                "NioConnServerFail")
                        && this.clusterName != null) {
                    executeMssFailOver(usePool);
                } else {
                    throw ie;
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#getConnection()
     */
    protected void createHttpClient(boolean usePool, int port)
            throws IntermailException {
        httpClient = HTTPConnectionManager.getInstance().getMssInstances(host)
                .getNextPool().borrowObject();
    }

    /*
     * Iterate the clusterHashMap key for getting active MSS connection.
     * @param usePool if true, will use a pool connection if available,
     * otherwise will open a new connection
     */
    private void executeMssFailOver(boolean usePool) throws IntermailException {
        String mssHost = null;
        IntermailException intermailEx = null;
        while ((mssHost = getSurrogateMSS()) != null) {
            try {
                rmeSocket = ConnectionManager.getRmeSock(mssHost, mssPort,
                        usePool);
            } catch (IntermailException ie) {
                intermailEx = ie;
                if (logger.isDebugEnabled()) {
                    logger.debug("executeMssFailOver : Tried connecting to MSS : "
                            + mssHost
                            + " got response "
                            + ie.getFormattedString());
                }
                continue;
            }
            // got the RME socket check whether its a working socket.
            // if its working then no need to try other Mss Hosts.
            if (rmeSocket == null || !rmeSocket.isReady()) {
                continue;
            }
            break;

        }
        if (rmeSocket == null || !rmeSocket.isReady()) {
            if (logger.isDebugEnabled()) {
                logger.debug("executeMssFailOver() Tried all Mss Hosts not getting working socket");
            }
            throw intermailEx;
        }
    }

    /*
     * Returns surrogate MSS as per the sequence of bucket ranges.
     */

    private String getSurrogateMSS() throws IntermailException {

        if (logger.isDebugEnabled()) {
            logger.debug("Entering  : getSurrogateMSS ()");
        }

        String surrogateMSS = null;
        String mssHostPos = null;
        String mssHostNeg = null;
        int lowestPos = 1000;
        int lowestNeg = 0;

        MssHostsTableHandler handler = MssHostsTableHandler.getInstance();
        mssHostsTable = handler.getClusterHashMap();
        synchronized (mssHostsTable) {
            Iterator clusterHashMapIterator = mssHostsTable.keySet().iterator();
            if (logger.isDebugEnabled()) {
                logger.debug("Entering  : getSurrogateMSS () mssHostsTable "
                        + mssHostsTable);
            }

            while (clusterHashMapIterator.hasNext()) {
                String clusterMssHost = (String) clusterHashMapIterator.next();
                if (logger.isDebugEnabled()) {
                    logger.debug("getSurrogateMSS : clusterMssHost: "
                            + clusterMssHost);
                }
                Object obj = MssHostsTableHandler.getInstance().getMssHostInfo(
                        clusterMssHost);
                if (obj != null) {
                    MssHostInfo mssInfo = (MssHostInfo) obj;
                    String configCluster = mssInfo.getClusterName();
                    if (logger.isDebugEnabled()) {
                        logger.debug("getSurrogateMSS : configCluster: "
                                + configCluster);
                    }
                    if (configCluster.equals(this.clusterName)) {
                        int minRange = mssInfo.getMin();
                        minRange = minRange - mailboxBucket;
                        if (minRange >= 0) {
                            if (lowestPos > minRange) {
                                lowestPos = minRange;
                                mssHostPos = mssInfo.getHostName();
                            }
                        } else {
                            if (lowestNeg > minRange) {
                                lowestNeg = minRange;
                                mssHostNeg = mssInfo.getHostName();
                            }
                        }
                    }
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getSurrogateMSS () lowestPos: " + lowestPos
                    + " lowestNeg: " + lowestNeg);
        }
        if (lowestPos < 1000) {
            surrogateMSS = mssHostPos.substring(mssHostPos.indexOf(':') + 1);
        } else if (lowestNeg < 0) {
            surrogateMSS = mssHostNeg.substring(mssHostNeg.indexOf(':') + 1);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    "No Surrogate MSS found for mailbox: " + this.mailboxId
                            + " in cluster: " + this.clusterName);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getSurrogateMSS : Surrogate MSS returned: "
                    + surrogateMSS + " for mailbox: " + this.mailboxId
                    + " in cluster: " + this.clusterName);
        }

        return surrogateMSS;

    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#releaseConnection()
     */
    protected void releaseConnection() throws IntermailException {
        if (rmeSocket != null) {
            ConnectionManager.putRmeSock(rmeSocket);
            rmeSocket = null;
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.AbstractOperation#releaseConnection()
     */
    protected void releaseHttpConnection() throws IntermailException {
        if (httpClient != null) {
            HTTPConnectionManager.returnHttpConnection(httpClient);
            httpClient = null;
        }
    }

    protected static long getMailboxHash(String key) {
        int len = key.length();
        long h = 0;
        long mask = 0x00000000FFFFFFFFL;

        for (int i = 0; i < len; i++) {
            h *= 16777619;
            h &= mask;
            h ^= (int) key.charAt(i);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Hash for mailboxId: " + key + " is: " + h);
        }
        return h;
    }

    protected String resolveClusterToMSS(String clusterName, String msName) {
        if (clusterName == null || msName == null) {
            logger.warn("Invalid clusterName and/or msName");
            return null;
        }
        //In case if the rmeDataModel is Leopard, we need not resolve the cluster
        if (rmeDataModel == RmeDataModel.Leopard){
            return clusterName;
        }
        MssHostsTableHandler handler = MssHostsTableHandler.getInstance();

        String siteName = Config.getInstance().get(Config.SITE_NAME,
                null);
        if (null != siteName && !"".equals(siteName)) {
            String[] siteModes = Config.getInstance().getArray(
                    Config.SITE_MODE);
            Map<String, String> siteModeMap = new HashMap<String, String>();
            if (null != siteModes) {
                for (String mode : siteModes) {
                    if (mode.contains("=")) {
                        String[] kvPair = mode.split("=");
                        siteModeMap.put(kvPair[0], kvPair[1]);
                    }
                }
            }
            if (siteModeMap.containsKey(siteName)) {
                ClusterSiteMode siteMode = ClusterSiteMode.getByValue(siteModeMap
                        .get(siteName));
                logger.info("Site mode for \"" + siteName + "\" is: " + siteMode);
                switch (siteMode) {
                case NORMAL:
                    clusterName = siteName + "#" + clusterName;
                    break;

                default:
                    // TODO: This has to be fixed later.
                    clusterName = "dummySite#dummyCluster";
                    break;
                }
                logger.info("Cluster name after it resolved: " + clusterName);
            }
        }

        // Distinguish between Stateful/Stateless users. We can not take this
        // decision on the basis of clusterHashMap because during migration
        // config.db will have this clusterHashMap key present and requests may
        // come from stateful users.

        if (handler.isClusterAvilable(clusterName)) {
            isStatelessMSS = true;
            this.clusterName = clusterName;
        } else {
            isStatelessMSS = false;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("resolveClusterToMSS :clusterName : " + clusterName
                    + " msName :" + msName + " isStatelessMSS : "
                    + isStatelessMSS);
        }
        String mssHost = null;

        if (isStatelessMSS) {
            // retrieve the MSS host through Hashing algo.
            mailboxId = msName;
            mailboxBucket = (int) (getMailboxHash(msName) % MAX_BUCKETS);
            if (logger.isDebugEnabled()) {
                logger.debug("resolveClusterToMSS bucket: " + mailboxBucket);
            }
            mssHost = handler.resolveClusterToMSS(clusterName, mailboxBucket);

            // In case of primary host down the value of mssHost will get null,
            // hence we
            // will fetch alternative mss host from mssHostTable, which will
            // perform their task.
            if (mssHost == null) {

                if (logger.isDebugEnabled()) {
                    logger.debug("mss host value is null, try to resolve alternative host");
                }
                try {
                    mssHost = getSurrogateMSS();
                    if (logger.isDebugEnabled()) {
                        logger.debug("resolveClusterToMSS :clusterName : "
                                + clusterName + " msName :" + msName
                                + " mssHost : " + mssHost);
                    }
                } catch (IntermailException ie) {
                    // We have tried to retrieve the next surrogate MSS to
                    // handle the request and there is no MSS left within the
                    // cluster which can serve this incoming request. Giving up
                    // now.
                    logger.error("resolveClusterToMSS :IntermailException : "
                            + ie);

                    mssHost = null;
                }
            }
        } else {
            mssHost = clusterName;
        }
        if (logger.isDebugEnabled()) {
            logger.debug("resolveClusterToMSS Returning mssHost as " + mssHost);
        }
        return mssHost;
    }
}
