/*
 * Copyright (c) 2000-2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/SelfGrowingPool.java#1 $
 */

package com.opwvmsg.utils.paf.util;

/**
 * This class wraps a <code>Pool</code> to provide a pool of
 * items which will grow.
 * <p>
 * This class was originally part of net.mobility.util
 *
 * @author Forrest Girouard
 */
public class SelfGrowingPool implements Pool {

    /**
     * The factory to use to create new items when the pool is empty.
     */
    private PoolItemFactory factory;

    /**
     * The number of items created.
     */
    private int items;

    /**
     * The backing pool.
     */
    private Pool pool;

    /**
     * Constructs a new, empty pool with the specified factory to use
     * to create new items.
     */
    public SelfGrowingPool(Pool pool, PoolItemFactory factory) {
        this.pool = pool;
        this.factory = factory;
    }

    /**
     * Returns (and removes) an item from this pool creating
     * a new item if none are available and the capacity will
     * not be exceeded.
     *
     * @return Object an available item or <code>null</code> if
     *          this pool is exhausted.
     */
    public Object getItem() {
        Object item = null;
        boolean create = false;
        synchronized (getLock()) {
            item = pool.getItem();
            if (item == null) {
                int capacity = pool.capacity();
                if (capacity == 0 || items < capacity) {
                    create = true;
                    items++;
                }
            }
        }
        if (create == true) {
            item = factory.create();
            if (item == null) {
                synchronized (getLock()) {
                    items--;
                }
            }
        }
        return item;
    }

    /**
     * Adds the specified item to this pool.
     *
     * If the item cannot be added to this pool without exceeding
     * the capacity then the item is returned.
     *
     * @param item an <code>Object</code> to be placed into this pool
     *          for later use.
     *
     * @return Object the specified item if the capacity of the
     *          pool would be exceeded by adding it an
     *          <code>null</code> otherwise.
     */
    public Object putItem(Object item) {
        return pool.putItem(item);
    }

    /**
     * Removes all items from this pool.
     */
    public void clear() {
        pool.clear();
    }

    /**
     * Returns the number of items currently in this pool.
     *
     * @return int the number of items currently in this pool.
     */
    public int size() {
        return pool.size();
    }

    /**
     * Returns the maximum number of items to be retained by this
     * pool.  
     *
     * When the size is equal to the capacity no further items are
     * retained by this pool.
     *
     * A value of zero indicates that this pool does not limit the
     * number of items retained.
     *
     * @return int the maximum number of items to be retained by this pool.
     */
    public int capacity() {
        return pool.capacity();
    }

    /**
     * Returns the object used for synchronization by this pool.
     *
     * @return Object the object used by this pool for synchronization.
     */
    public Object getLock() {
        return pool.getLock();
    }

}
