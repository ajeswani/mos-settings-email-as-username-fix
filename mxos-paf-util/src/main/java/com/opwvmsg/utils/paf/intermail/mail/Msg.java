/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/Msg.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.mail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

import javax.mail.MessagingException;
import javax.mail.internet.InternetHeaders;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;
import com.opwvmsg.utils.paf.util.Utilities;

/**
 * This class encapsulates all of the data returned from an RME Msg object.
 */
public class Msg {
    private byte[] text;
    private String[] headers;
    private MimeMsg mimeMsg;
    private int uid;
    private int sizeInBytes;
    private String msgId;
    private String[] customFlags;
    private boolean msgPrivate;
    private String msgType;
    private String msgPriority;
    private boolean seen;
    private long arrivalTime;
    private long msgFirstSeenTime;
    private long msgExpireTime;
    private String flags;
    InternetHeaders internetHeaders;

    public static final int MSG_FLAGS_SIZE = 7;

    public static final String DEFAULT_FLAGS = "-------";

    /**
     * Constructor for a placeholder Msg with no data
     * 
     * @param uid the uid of the message
     */
    public Msg(int uid) {
        this.uid = uid;
    }

    /**
     * Constructor for use by CreateMsg when receiving the return Msg
     * 
     * @param uid message uid
     * @param msgId the string message id
     * @param arrivalTime the arrival time of the message (in ms)
     * @param msgType the string type of the message
     * @param msgPriority the string priority of the message
     * @param flags the message flags (must be a string with 7 chars)
     */
    public Msg(int uid, String msgId, long arrivalTime, String msgType,
            String msgPriority, String flags) {
        this.uid = uid;
        this.msgId = msgId;
        this.arrivalTime = arrivalTime;
        this.msgType = msgType;
        this.msgPriority = msgPriority;
        this.flags = flags;
    }

    /**
     * Constructor for a suitable Msg object to pass to MessageExamine
     * 
     * @param text the raw text of the message
     */
    public Msg(byte[] text) {
        this.text = text;
        try {
            InternetHeaders iHdrs = new InternetHeaders(new ByteArrayInputStream(text));
            Enumeration headerEnums = iHdrs.getAllHeaderLines();
            ArrayList hList = new ArrayList(20);
            while (headerEnums.hasMoreElements()) {
                hList.add((String)headerEnums.nextElement());
            }
            headers = new String[hList.size()];
            hList.toArray(headers);
        } catch (MessagingException me) {
            this.headers = new String[0];
        }
        this.msgId = "";
        this.uid = -1;
        sizeInBytes = text.length;
    }

    /**
     * Reads this Message object from an RME stream
     * 
     * @param inStream the rme input stream
     * @throws IOException on any IO error
     */
    public Msg(RmeInputStream inStream) throws IOException {
        boolean haveIt = inStream.readBoolean();
        if (haveIt) {
            if (!inStream.rmeProtAtLeast(AbstractOperation.RME_IMCL_READ_BIN_MSG_VER)) {
                text = inStream.readStringBytes();
            }
            headers = inStream.readStringArrayInRaw();

            // read InternetHeaders
            try {
                byte[] byteHeaders = Utilities.arrayToDsv(headers, '\n').getBytes();
                internetHeaders = new InternetHeaders(
                        new ByteArrayInputStream(byteHeaders));
            } catch (MessagingException me) {
            }

            // read MimeInfo
            mimeMsg = new MimeMsg(inStream);
            uid = inStream.readInt(); //uid
            sizeInBytes = inStream.readInt();
            msgId = inStream.readString();
            customFlags = inStream.readStringArray();
            msgPrivate = inStream.readBoolean();
            msgType = inStream.readString();
            msgPriority = inStream.readString();
            // looks like the RME protocol
            // is subject to the Y2038 bug..
            // if that gets changed, we'll have to adapt here.
            msgFirstSeenTime = inStream.readInt();
            msgExpireTime = inStream.readInt();
            seen = inStream.readBoolean();
            arrivalTime = inStream.readInt();
            flags = readMsgFlags(inStream);
            if (inStream.rmeProtAtLeast(AbstractOperation.RME_INDEXING_SERVER_VER)) {
            	// Index Server
            	inStream.readString(); //reading doc id
            }
            if (inStream.rmeProtAtLeast(AbstractOperation.RME_IMCL_READ_BIN_MSG_VER)
                    && !inStream.rmeProtAtLeast(AbstractOperation.RME_IMCL_READ_PARTIAL_MSG_VER)) {
                text = new byte[sizeInBytes];
                inStream.read(text, 0, sizeInBytes);
            } else if (inStream.rmeProtAtLeast(AbstractOperation.RME_IMCL_READ_PARTIAL_MSG_VER)) {
                int partialSize = inStream.readInt();
                text = new byte[partialSize];
                inStream.read(text, 0, partialSize);
            }
            if (inStream.rmeProtAtLeast(AbstractOperation.RME_MX8_VER)) {
                inStream.readBoolean(); //hasAttachmentKnowKnown
                inStream.readBoolean(); //hasAttachmentKnow
            } 
        }
    }

    /**
     * Gets the message type.
     */
    public String getMsgType() {
        return msgType;
    }

    /**
     * Get the raw text of this message
     * 
     * @return text as byte array
     */
    public byte[] getText() {
        return text;
    }

    /**
     * Get the integer uid of this message
     * 
     * @return uid
     */
    public int getUid() {
        return uid;
    }

    /**
     * Get the string Message ID of this message
     * 
     * @return message ID
     */
    public String getMsgId() {
        return msgId;
    }
    
    /**
     * Get the String representing the message flags
     * 
     * @return message Flags string
     */
    public String getMsgPriority() {
        return msgPriority;
    }

    /**
     * Get the String representing the message flags
     * 
     * @return message Flags string
     */
    public String getMsgFlags() {
        return flags;
    }

    /**
     * Get the size of this message in bytes
     * 
     * @return size in bytes
     */
    public int getSizeInBytes() {
        return sizeInBytes;
    }

    /**
     * Get the String array of custom flags associated with this message.
     * This is what the MSS calls keywords, but JavaMail calls message flags.
     * 
     * @return custom flags
     */
    public String[] getCustomFlags() {
        return customFlags;
    }

    /**
     * Get the headers for this message (if parsed)
     * 
     * @return headers
     */
    public String[] getHeadersArray() {
        return headers;
    }

    /**
     * Get the Internet headers for this message (if parsed)
     *
     * @return InternetHeaders headers of the message.
     */
    public InternetHeaders getInternetHeaders() {
        return internetHeaders;
    }

    /**
     * Get all the headers for this header name, returned as a single String,
     * with headers separated by the delimiter. If the delimiter is null, only
     * the first header is returned.
     *
     * @param name the header for which the header value is required
     * @param delimiter delimiter
     * @return String header value
     */
    public String getHeader(String name, String delimiter) {
        String headerValue = null;
        if (internetHeaders != null) {
            headerValue = internetHeaders.getHeader(name, delimiter);
        }
        return headerValue;
    }
    
    /**
     * Method to get the required flag from flags String.
     *
     * @param mssFlagOrder
     * @return
     */
    public boolean getFlag(MssFlagOrder mssFlagOrder) {
        if (flags != null && ((flags.length() - 1) >= mssFlagOrder.ordinal())) {
            char flag = flags.charAt(mssFlagOrder.ordinal());
            return (flag == 'T' || flag == 't');
        } else {
            return false;
        }
    }
    /**
     * Get the arrival time of this message as a date in ms
     * 
     * @return arrival time
     */
    public long getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Get the Message expired time
     *
     * @return Long msgExpireTime
     */
    public Long getMsgExpireTime() {
        return msgExpireTime;             
    }
    
    /**
     * Get the MimeInfo for this message (if parsed)
     * 
     * @return MIME information
     */
    public MimeInfo getMimeInfo() {
        if (mimeMsg != null) {
            return mimeMsg.getMimeInfo();
        }
        return new MimeInfo();             
    }

    /**
     * Set the Message Headers for this message
     * 
     * @param headers
     */
    public void setHeadersArray(String []headers) {
        for (int i = 0, sizeInBytes = 0; i < headers.length; i++) {
            sizeInBytes += headers[i].length()+2; // + 2 is the \r\n
        }
        if (text != null) {
            sizeInBytes += 2 + text.length;
        }
        this.headers = headers;
    }
    
    /**
     * Set the Arrival Time for the Message
     * 
     * @param arrivalTime
     */
    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    
    /**
     * Set the Message Flags for this Message
     * 
     * @param flags
     */
    public void setMsgFlags(String flags) {
        this.flags = flags;
    }
    
    //internal convenience functions
    /**
     * A special function to write the message flags to the output stream. The
     * message flags are not written as a normal string.
     * 
     * @param outStream the output stream
     * @param flags the message flags
     * @throws IOException on any IO error
     */
    public static void writeMsgFlags(RmeOutputStream outStream, String flags)
            throws IOException {
        for (int i = 0; i < MSG_FLAGS_SIZE; i++) {
            outStream.write((byte) flags.charAt(i) & 0xFF);
        }
        outStream.write(0);
    }

    /**
     * A special function to read the message flags from an input stream.
     * 
     * @param inStream
     * @return the message flags
     * @throws IOException on any IO error
     */
    protected String readMsgFlags(RmeInputStream inStream) throws IOException {
        byte[] flagsArray = new byte[MSG_FLAGS_SIZE];
        inStream.read(flagsArray, 0, MSG_FLAGS_SIZE);
        inStream.read();
        return (new String(flagsArray, "us-ascii"));
    }

    /**
     * A debugging method to dump the contents of this object to a PrintStream
     * 
     * @param out the printstream to use
     */
    public void dump(java.io.PrintStream out) {
        out.println("DUMPING MESSAGE");
        out.println("===============");
        out.println("TEXT: " + new String(text));
        out.println("HEADERS: " + Utilities.arrayToDsv(headers, '\n'));
        mimeMsg.dump();
        out.println("UID: " + uid);
        out.println("SIZE: " + sizeInBytes);
        out.println("MSGID: " + msgId);
        out.println("CUSTOM FLAGS: " + Utilities.arrayToCsv(customFlags));
        out.println("PRIV: " + msgPrivate);
        out.println("TYPE: " + msgType);
        out.println("PRIOR: " + msgPriority);
        out.println("FIRSTSEEN: " + msgFirstSeenTime);
        out.println("FIRSTSEEN: " + new Date(msgFirstSeenTime * 1000));
        out.println("EXPIRE: " + msgExpireTime);
        out.println("EXPIRE: " + new Date(msgExpireTime * 1000));
        out.println("seen: " + seen);
        out.println("ARRIVAL: " + new Date(arrivalTime * 1000));
        out.println("ARRIVAL: " + arrivalTime);
        out.println("FLAGS: " + flags);
        out.println("==== DONE MESSAGE =========");
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n");
        buffer.append("BEGIN MESSAGE");
        buffer.append("\n");
        buffer.append("===============");
        buffer.append("\n");
        buffer.append("TEXT: " + new String(text));
        buffer.append("\n");
        buffer.append("HEADERS: ").append(Utilities.arrayToDsv(headers, '\n'));
        buffer.append("\n");
        buffer.append(mimeMsg.toString());
        buffer.append("UID: ").append(uid);
        buffer.append("\n");
        buffer.append("SIZE: ").append(sizeInBytes);
        buffer.append("\n");
        buffer.append("MSGID: ").append(msgId);
        buffer.append("\n");
        buffer.append("CUSTOM FLAGS: ").append(Utilities.arrayToCsv(customFlags));
        buffer.append("\n");
        buffer.append("PRIV: ").append(msgPrivate);
        buffer.append("\n");
        buffer.append("TYPE: ").append(msgType);
        buffer.append("\n");
        buffer.append("PRIOR: ").append(msgPriority);
        buffer.append("\n");
        buffer.append("FIRSTSEEN: ").append(msgFirstSeenTime);
        buffer.append("\n");
        buffer.append("FIRSTSEEN: ").append(new Date(msgFirstSeenTime * 1000));
        buffer.append("\n");
        buffer.append("EXPIRE: ").append(msgExpireTime);
        buffer.append("\n");
        buffer.append("EXPIRE: ").append(new Date(msgExpireTime * 1000));
        buffer.append("\n");
        buffer.append("seen: ").append(seen);
        buffer.append("\n");
        buffer.append("ARRIVAL: ").append(new Date(arrivalTime * 1000));
        buffer.append("\n");
        buffer.append("ARRIVAL: ").append(arrivalTime);
        buffer.append("\n");
        buffer.append("FLAGS: ").append(flags);
        buffer.append("\n");
        buffer.append("==== END MESSAGE =========");
        buffer.append("\n");
        return buffer.toString();
    }

    /**
     * Enum Message flag order.
     */
    public enum MssFlagOrder {
        flagrecent,
        flagseen,
        flagans,
        flagflagged,
        flagdraft,
        flagdel,
        flagres;

        public static MssFlagOrder getByValue(String value) {
            MssFlagOrder returnVal = MssFlagOrder.flagrecent;
            for (MssFlagOrder type : values()) {
                if (type.name().equalsIgnoreCase(value)) {
                    returnVal = type;
                    break;
                }
            }
            return returnVal;
        }
    }
}

