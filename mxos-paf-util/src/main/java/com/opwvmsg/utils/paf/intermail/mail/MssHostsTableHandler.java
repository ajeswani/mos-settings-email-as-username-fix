/*
 * Copyright (c) 2005-2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 *
 * $Id: //paf/paf31-903-1105-108-1/util/src/com/openwave/intermail/mail/MssHostsTableHandler.java#5 $
 *
 */
package com.opwvmsg.utils.paf.intermail.mail;

import static com.opwvmsg.utils.paf.config.intermail.IntermailConfig.CLUSTER_HASH_MAP;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;


/**
 * MssHostsTableHandler class maintains the table of multiple mss which define with different clusters.
 * Class is based of singleton design pattern, and serve the mss host table. Also it will update its tables when
 * it will notified by IntermailConfig for change in "ClusterHashMap" Key.
 *
 * @author mxos-dev
 *
 */
public class MssHostsTableHandler {

    private static Logger logger = Logger.getLogger(MssHostsTableHandler.class);
    
    private static MssHostsTableHandler handler;

    private static final boolean MSS_DEFAULT_RUNNING_STATUS = true;

    // Map will store mss host alnog with cluster name (cluster:mssHost) as key and range as string array like "range [min,max]
     private Map mssHostsTable = null;
     
     // Map will store mss host along with cluster (cluster:mssHost) name as
     // key and MssHostInfo object as value.
    private Map mssHostInfoTable = null;
    
    // Array of configured clusters
    private String []configuredClusterName = null;

    /**
     * Constructor
     */
    private MssHostsTableHandler() {
       init(false,null);
    }
    
   /**
    * Static method which returns instance of MssHostsTableHandler.
    * @return MssHostsTableHandler instance of MssHostsTableHandler
    */
    public static synchronized MssHostsTableHandler getInstance() {
        if (null == handler) {
            handler = new MssHostsTableHandler();
        }
        return handler;
    }
    
    /**
     * Method return the set of mssHost name 
     * @return set of mssHosts name
     */
    public Set getMssHostKeySet(){
        if(null == mssHostInfoTable) {
            return null;
        }
        return mssHostInfoTable.keySet();
    }
    
    /**
     * Method returns MssHostInfo corresponding to key.
     * @param key MssHostName like cluster:mssHostName.
     * @return MssHostInfo Object of MssHostInfo
     */
    public MssHostInfo getMssHostInfo (String key){
        Object mssHost = mssHostInfoTable.get(key);
        if (null != mssHost){
            return (MssHostInfo) mssHost;    
        }
        return null;
    }
    
    /**
     * Check whether mssHost is present in table or not.
     * @param mssHostName mss host name.
     * @return true if present or return false.
     */
    public boolean isMssHostPresent(String mssHostName) {
         if(null == mssHostName) {
             return false;
         }
         return mssHostInfoTable.keySet().contains(mssHostName);
    }
    
    /**
     * Method updates mss host table
     * @param mssHostName mss host name
     * @param mssHostStatus mss host status
     * @return true if it will update in tables or return false
     */
    public boolean updateTables(String mssHostName, boolean mssHostStatus) {
        boolean isUpdated = false;
        if (null == mssHostName){
            return isUpdated;
        }
        boolean isHostInfoTableUpdated =
                updateMssHostsInfoTable(mssHostName, mssHostStatus);
        boolean isHostTableUpdated =
                updateMssHostTable(mssHostName, mssHostStatus);
        if (isHostInfoTableUpdated && isHostTableUpdated){
            return true;
        }
        return false;    
    }

    /**
     * Method updates in mssHostInfoTable table
     * @param mssHostName mss host name
     * @param mssHostStatus mss host status
     * @return  true if it will update in table or return false
     */
    private boolean updateMssHostsInfoTable(String mssHostName,
             boolean mssHostStatus) {
        synchronized (mssHostInfoTable) {
            Object hostInfo = mssHostInfoTable.get(mssHostName);
            if (null == hostInfo) {
                return false;
            }
            ((MssHostInfo)hostInfo).setRunning(mssHostStatus);
        }
        return true;
    }

    /**
     * Method updates in mssHostsTable table
     * @param mssHostName mss host name
     * @param mssHostStatus mss host status
     * @return true if it will update in table or return false
     */
    
    private boolean updateMssHostTable(String mssHostName,
            boolean mssHostStatus) {
        synchronized (mssHostsTable) {
            if (null == mssHostsTable.get(mssHostName) && mssHostStatus){
                MssHostInfo hostInfo = getMssHostInfo(mssHostName);
                int [] range = new int[]{hostInfo.getMin(),hostInfo.getMax()};
                mssHostsTable.put(mssHostName, range);
                logger.info("mssHostsTable has been updated, " +
                         mssHostName +" key entry is added");
                return true;
            }else if (!mssHostStatus){
                if ( null != mssHostsTable.remove(mssHostName)){
                    logger.info("mssHostsTable is updated, " +
                            mssHostName +" key entry " +
                            "is removed from table");
                    return true;
                }
                        
            }
        }
        return false;
    }
    /**
     * By calling this method other module can notify this class that clusterHashMap key
     * get updated and according to it update both tables.
     */
    
    public void notifedKeyChange(String[] newKeys){
        if(newKeys == null) {
            logger.info("Got disableRedirectToSurrogateMSS key changed notification..." +
               " call again init method ");
            init(false,newKeys);
        }
        else {
            logger.info("Got clusterHashMap key changed notification..." +
               " call again init method ");
            init(true,newKeys);        
        }
        if (logger.isDebugEnabled()) {
            logger.debug("updated value of mssHostsTable :- " + mssHostsTable);
            logger.debug("updated value of mssHostInfoTable :- " + mssHostInfoTable);
        }
    }
    
/**
     * Method has been call two type. 
     * 1. At application up time, method reads the clusterHashMap key value and based on it prepares both tables
     * 2. When clusterHashMap key value has been changed, 
     *    new key values have provided by calling method
     * @param isUpdate Check whether method call while clusterHashMap key has been changed in config.db or not
     * @param newValue new value of config.db 
     */
     
    private void init(boolean isUpdate,String[] newValue) {
        try{
            String[] clusterLines = null;
            if (isUpdate && null != newValue){
                clusterLines = newValue;
            } else {
                Config config = Config.getInstance();
                clusterLines = config.getArray(CLUSTER_HASH_MAP);
            }
            for (int i =0; i< clusterLines.length; i++){
                if (isUpdate) {
                    logger.info ("clusterHashMap key has changed new value clusterLines["+i+"])" +
                            clusterLines[i]);    
                } else {
                    logger.info ("clusterHashMap key value from config.db clusterLines["+i+"])" +
                             clusterLines[i]);    
                }
                
            }                        
            if (clusterLines == null || clusterLines.length < 1) {
                logger.error( CLUSTER_HASH_MAP +" key value is null or no any value ");
                throw new ConfigException();
            }
            mssHostsTable = new HashMap();
            mssHostInfoTable = new HashMap();
            configuredClusterName = new String [clusterLines.length];
            for (int i = 0; clusterLines != null && i < clusterLines.length; i++) {
                String cluster = clusterLines[i];
                if(cluster.length() > 0){
                    prepareMSSHostTable(cluster);            
                    configuredClusterName[i] = cluster.substring(0, cluster.indexOf("="));
                }
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Value of mssHostsTable :- " + mssHostsTable);
                logger.debug("Value of mssHostInfoTable :- " + mssHostInfoTable);
            }
        } catch (ConfigException e) {
            if(logger.isDebugEnabled()) {
                logger.debug(CLUSTER_HASH_MAP + ": no such key");    
            }
        }        
    }

    /**
     * Method prepares both tables
     * @param cluster name of cluster
     */
    private  void prepareMSSHostTable(String cluster) {
        try {
            logger.info("prepareMSSHostTable : cluster " + cluster);
            if (cluster != null && cluster.trim().length() > 0) {
                cluster = cluster.trim();
                int index = cluster.indexOf("=");
                if (index > 0) {

                    logger.info("prepareMSSHostTable : configuredClusterName "
                            + Arrays.asList(configuredClusterName));
                    String clusterName = cluster.substring(0,cluster.indexOf("="));    
                    String[] hosts = cluster.substring(index + 1).split(",");
                    for (int j = 0; j < hosts.length; j++) {
                        String host = "";
                        int min = 0;
                        int max = 0;
                        index = hosts[j].indexOf(":");
                        if (index > 0) {
                            host = hosts[j].substring(0, index);
                            String buckets = hosts[j].substring(index + 1);
                            index = buckets.indexOf("-");
                            if (index > 0) {
                                try {
                                    min = Integer.parseInt(buckets.substring(0,
                                            index));
                                    max = Integer.parseInt(buckets
                                            .substring(index + 1));
                                    int[] bucketRange = new int[2];
                                    bucketRange[0] = min;
                                    bucketRange[1] = max;
                                    String fullName = clusterName+":"+host;
                                    logger.info("prepareMSSHostTable : "
                                            + "cluster:host " + fullName + " Min " + min
                                            + " Max " + max);
                                    synchronized (mssHostsTable) {
                                        mssHostsTable.put(fullName, bucketRange);
                                    }
                                    addInMssHostInfoTable(min,max,clusterName,host);
                                } catch (NumberFormatException nfe) {
                                    logger.warn("Discarding MSS host "
                                            + host
                                            + " because of NumberFormatException" +
                                            " Occured while parsing bucket range");
                                    continue;
                                }
                            } else {
                                logger.error("Unable to parse cluster line" +
                                        " because of missing - sign for host: "
                                        + host);
                            }
                        } else {
                            logger.error("Unable to parse cluster line because" +
                                    " of missing : sign for host: "
                                    + host);
                        }
                    }
                } else {
                    logger.error("Unable to parse cluster line because of " +
                            "missing = sign in cluster: "
                            + cluster);
                }
            }

        } catch (ConfigException e) {
            if(logger.isDebugEnabled()) {
                logger.debug(CLUSTER_HASH_MAP + ": no such key");
            }
        } 
    }
    
    /**
     * Method resolve primary mss host by cluster name and bucket no 
     * @param clusterName cluster name
     * @param bucket calculated bucket number based on user mailbox id
     * @return mssHost
     */
    public String resolveClusterToMSS(String clusterName, int bucket) {
        String mssHost = null;
        synchronized (mssHostsTable) {        
            Iterator it = mssHostsTable.keySet().iterator();        
            while (it.hasNext()) {
                String host = (String) it.next();
                MssHostInfo mssInfo = (MssHostInfo) mssHostInfoTable.get(host);
                if (mssInfo.getClusterName().equals(clusterName)) {
                    int min = mssInfo.getMin();
                    int max = mssInfo.getMax();
                    if (min <= bucket && bucket <= max) {
                        mssHost = host.substring(host.indexOf(':') + 1);
                        break;
                    }
                }
            }
        }
        return mssHost;
    }

    /**
     * Method add a new host value in MssHostInfoTable
     * @param min int minimum range of particular mss host
     * @param max int maximum range of particular mss host
     * @param clusterName String cluster name
     * @param hostName mss host name
     */
    private void addInMssHostInfoTable(int min, int max,
                     String clusterName, String hostName) {
        
        String mssfullName = clusterName.trim() +":" + hostName;
        MssHostInfo hostInfo = new MssHostInfo(mssfullName,clusterName,
                min, max,MSS_DEFAULT_RUNNING_STATUS);
        addInMssHostInfoTable(hostInfo);
        
    }

    /**
     *  Method add a new host value in MssHostInfoTable
     * @param hostInfo MssHostInfo object
     */
    private void addInMssHostInfoTable(MssHostInfo hostInfo){
        synchronized (mssHostInfoTable) {
            mssHostInfoTable.put(hostInfo.getHostName(), hostInfo);
        }
        
    }
    
    /**
     * Method returns cluster names array
     * @return String array
     */
    private  String[] getClusersArray() {
        return configuredClusterName;
    }

    /**
     * Method checks whether cluster name is present in
     * configuredClusterName array or not
     * @param clusterName cluster Name
     * @return Returns true if cluster name is present in array or return false
     */
    public boolean isClusterAvilable(String clusterName){
        if(null != getClusersArray()){
            for(int i =0; i <configuredClusterName.length; i++) {
                if (configuredClusterName[i].equals(clusterName)){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Method returns clusterHashMap
     * @return mssHostsTable
     */
    public Map getClusterHashMap() {
        return mssHostsTable;
    }
}
