/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.utils.paf.intermail.http;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.http.params.CoreConnectionPNames;

/**
 * HTTP RME connection pool factory to create HTTPConnection object to MSS.
 * 
 * @author mxos-dev
 */
class HTTPConnectionFactory extends BasePoolableObjectFactory<HTTPConnection> {

    private String hostName = null;
    private int port = 0;
    private int rmeTimeout = 0;

    public HTTPConnectionFactory(final String hostName, final int port,
            final int timeoutMS) {
        this.hostName = hostName;
        this.port = port;
        this.rmeTimeout = timeoutMS;
    }

    @Override
    public HTTPConnection makeObject() {
        HTTPConnection httpclient = new HTTPConnection(hostName, port,
                rmeTimeout);
        httpclient.getParams().setIntParameter(
                CoreConnectionPNames.CONNECTION_TIMEOUT, rmeTimeout);
        httpclient.getParams().setIntParameter(
                CoreConnectionPNames.SO_TIMEOUT, rmeTimeout);
        return httpclient;
    }

    @Override
    public void destroyObject(final HTTPConnection client) {
        client.getConnectionManager().shutdown();
    }
}