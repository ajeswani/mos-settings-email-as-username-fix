/*  H+ 
 *      Copyright 2004 Openwave Systems, Inc. All Rights Reserved.
 * 
 *      The copyright to the computer software herein is the property of 
 *      Openwave Systems, Inc. The software may be used and/or copied only 
 *      with the written permission of Openwave Systems, Inc. or in accordance 
 *      with the terms and conditions stipulated in the agreement/contract 
 *      under which the software has been supplied.
 * 
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/Inventory.java#1 $
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This class holds the folder inventory data Each folder can have several
 * inventories for each message type, prioity, and read/unread status. In each
 * inventory, there is a count of total messages and total private messages
 * along with data describing exactly which inventory it represents.
 */
public class Inventory {
    private String path;
    private long totBytes;
    private int totCount;
    private int totPrivate;
    private boolean unread;
    private String type;
    private String priority;

    /**
     * Reads the Inventory from the RME stream
     * 
     * @param inStream the stream
     * @throws IOException on any error
     */
    public Inventory(RmeInputStream inStream) throws IOException {
        boolean haveIt = inStream.readBoolean();
        if (haveIt) {
            // read inventories (
            path = inStream.readString();
            if (inStream.getRmeVer() >= AbstractOperation.RME_MX8_VER) {
                totBytes = inStream.readLong64();
            } else {
                totBytes = inStream.readLong();
            }
            
            //WARNING: we're trimming these values to
            //ints here
            totCount = (int) inStream.readLong();
            totPrivate = (int) inStream.readLong();
            unread = inStream.readBoolean();
            type = inStream.readString();
            priority = inStream.readString();
        }

    }

    /**
     * Get the folder pathname for this Inventory
     * 
     * @return pathanme
     */
    public String getPath() {
        return path;
    }

    /**
     * Get the total number of bytes for the messages in this Inventory
     * 
     * @return total number of bytes.
     */
    public long getMsgBytes() {
        return totBytes;
    }

    /**
     * Get the total number of messages represented in this Inventory
     * 
     * @return total number of messages
     */
    public int getMsgCount() {
        return totCount;
    }

    /**
     * Get the number of private messages in this inventory
     * 
     * @return number of private messages
     */
    public int getMsgPrivate() {
        return totPrivate;
    }

    /**
     * Test to see if this Inventory is representing unread or read messages
     * 
     * @return true if the Inventory represents unread messages, false otherwise
     */
    public boolean getUnread() {
        return unread;
    }

    /**
     * Get the message type that this Inventory represents
     * 
     * @return message type
     */
    public String getType() {
        return type;
    }

    /**
     * Get the message priority that this Inventory represents
     * 
     * @return message priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Debug dump to a printstream
     * 
     * @param out the stream used for printing
     */
    public void dump(final java.io.PrintStream out) {
        out.println("DUMPING INVENTORY ");
        out.println("Path => " + path);
        out.println("TotB => " + totBytes);
        out.println("#Msg => " + totCount);
        out.println("TotP => " + totPrivate);
        out.println("UnRd => " + unread);
        out.println("Type => " + type);
        out.println("Pri. => " + priority);
        out.println("==== DONE/INV ==== ");

    }
}