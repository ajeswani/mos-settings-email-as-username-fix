/*  H+
 *      Copyright 2004 Software.com, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Software.com, Inc.  The software may be used and/or copied only
 *      with the written permission of Software.com, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/io/RmeInputStream.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.io;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This class extends BufferedInputStream, but overrides the synchronized
 * methods with non-synchronized ones. Also, since the number of expected bytes
 * is always known in RME, the read methods will always return the exact number
 * of bytes requested, or fail with an exception. This differs from a regular
 * BufferedInputStream as methods in that class may return less bytes than
 * requested in some cases.
 * 
 * This class also implements methods similar to DataInputStream for primitive
 * RME type, plus implements a few basic compound types such as Strings and
 * other arrays of primitives. See the individual methods for a complete list of
 * type implemented.
 */
public class RmeInputStream extends BufferedInputStream {

    private int rmeProtVer = AbstractOperation.RME_CURRENT_VERSION;
    private boolean littleEndian = false; // by default we'll use big endian

    private int CLXN_MAX_SIZE = 0x0FFFFFFF;

    public static final String ERROR_BAD_LENGTH = "Invalid Collection Length";
    public static final String ERROR_NO_NULL = "String not null terminated";

    /**
     * Constructor for RmeSocket to initialize with InputStream from connection
     * 
     * @param stream
     */
    public RmeInputStream(InputStream stream) {
        super(stream, 2048);
    }

    /**
     * Constructor for RmeSocket to initialize with InputStream from connection
     * 
     * @param stream
     */
    public RmeInputStream(InputStream stream, int size) {
        super(stream, size);
    }

    /**
     * Sets the byte order flag for data on this stream
     * 
     * @param invert if true, use little endian (LSB first) order, if false, use
     *            big endian (MSB first) byte order.
     */
    public void setLittleEndian(boolean invert) {
        littleEndian = invert;
    }

    /**
     * Returns the rme version currently being used by this stream
     * 
     * @return rme version
     */
    public int getRmeVer() {
        return rmeProtVer;
    }

    /**
     * Sets the rme version to be used by this stream
     */
    public void setRmeVer(int rmeVersion) {
        rmeProtVer = rmeVersion;
    }

    /**
     * Compares the rme version passed in to the rme version being used by this
     * stream. Returns true if the stream rme version is >= the rme version
     * passed in. This is useful for operations to check before they send data
     * that is only required/allowed in later versions of rme.
     * 
     * @param rmeVer - an integer rme version to compare
     * @return true if rmeVer >= stream's current rme version
     */
    public boolean rmeProtAtLeast(int rmeVer) {
        return rmeProtVer >= rmeVer;
    }

    /**
     * Reads a <code>boolean</code> from this data input stream. This method
     * reads a single byte from the underlying input stream. A value of
     * <code>0</code> represents <code>false</code>. Any other value represents
     * <code>true</code>. This method blocks until either the byte is read, the
     * end of the stream is detected, or an exception is thrown.
     * 
     * @return the <code>boolean</code> value read.
     * @exception EOFException if this input stream has reached the end.
     * @exception IOException if an I/O error occurs.
     */
    public final boolean readBoolean() throws IOException {
        int ch = read();
        if (ch < 0) {
            throw new EOFException();
        }
        return (ch != 0);
    }

    /**
     * Reads a signed 8-bit value from this data input stream. This method reads
     * a byte from the underlying input stream. If the byte read is
     * <code>b</code>, where 0&nbsp;&lt;=&nbsp; <code>b</code>
     * &nbsp;&lt;=&nbsp;255, then the result is:
     * <ul>
     * <code>
     *     (byte)(b)
     * </code>
     * </ul>
     * <p>
     * This method blocks until either the byte is read, the end of the stream
     * is detected, or an exception is thrown.
     * 
     * @return the next byte of this input stream as a signed 8-bit
     *         <code>byte</code>.
     * @exception EOFException if this input stream has reached the end.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final byte readByte() throws IOException {
        int ch = read();
        if (ch < 0) {
            throw new EOFException();
        }
        return (byte) (ch);
    }

    /**
     * Reads an unsigned 8-bit number from this data input stream. This method
     * reads a byte from this data input stream's underlying input stream and
     * returns that byte. This method blocks until the byte is read, the end of
     * the stream is detected, or an exception is thrown.
     * 
     * @return the next byte of this input stream, interpreted as an unsigned
     *         8-bit number.
     * @exception EOFException if this input stream has reached the end.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final int readUnsignedByte() throws IOException {
        int ch = read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch;
    }

    /**
     * Reads a signed 16-bit number from this data input stream. The method
     * reads two bytes from the underlying input stream. If the two bytes read,
     * in order, are <code>b1</code> and <code>b2</code>, where each of the two
     * values is between <code>0</code> and <code>255</code>, inclusive, then
     * the result is equal to:
     * <ul>
     * <code>
     *     (short)((b1 &lt;&lt; 8) | b2)
     * </code>
     * </ul>
     * <p>
     * This method blocks until the two bytes are read, the end of the stream is
     * detected, or an exception is thrown.
     * 
     * @return the next two bytes of this input stream, interpreted as a signed
     *         16-bit number.
     * @exception EOFException if this input stream reaches the end before
     *                reading two bytes.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final short readShort() throws IOException {
        InputStream in = this.in;
        int ch1 = read();
        int ch2 = read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        if (littleEndian) {
            return (short) ((ch2 << 8) + (ch1 << 0));
        } else {
            return (short) ((ch1 << 8) + (ch2 << 0));
        }
    }

    /**
     * Reads an unsigned 16-bit number from this data input stream. This method
     * reads two bytes from the underlying input stream. If the bytes read, in
     * order, are <code>b1</code> and <code>b2</code>, where
     * <code>0&nbsp;&lt;=&nbsp;b1</code>,<code>b2&nbsp;&lt;=&nbsp;255</code>,
     * then the result is equal to:
     * <ul>
     * <code>
     *     (b1 &lt;&lt; 8) | b2
     * </code>
     * </ul>
     * <p>
     * This method blocks until the two bytes are read, the end of the stream is
     * detected, or an exception is thrown.
     * 
     * @return the next two bytes of this input stream, interpreted as an
     *         unsigned 16-bit integer.
     * @exception EOFException if this input stream reaches the end before
     *                reading two bytes.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final int readUnsignedShort() throws IOException {
        InputStream in = this.in;
        int ch1 = read();
        int ch2 = read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        if (littleEndian) {
            return (ch2 << 8) + (ch1 << 0);
        } else {
            return (ch1 << 8) + (ch2 << 0);
        }

    }

    /**
     * Reads a signed 32-bit integer from this data input stream. This method
     * reads four bytes from the underlying input stream. If the bytes read, in
     * order, are <code>b1</code>,<code>b2</code>,<code>b3</code>, and
     * <code>b4</code>, where 0&nbsp;&lt;=&nbsp; <code>b1</code>,
     * <code>b2</code>,<code>b3</code>,<code>b4</code> &nbsp;&lt;=&nbsp;255,
     * then the result is equal to:
     * <ul>
     * <code>
     *     (b1 &lt;&lt; 24) | (b2 &lt;&lt; 16) + (b3 &lt;&lt; 8) +b4
     * </code>
     * </ul>
     * <p>
     * This method blocks until the four bytes are read, the end of the stream
     * is detected, or an exception is thrown.
     * 
     * @return the next four bytes of this input stream, interpreted as an
     *         <code>int</code>.
     * @exception EOFException if this input stream reaches the end before
     *                reading four bytes.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final int readInt() throws IOException {
        InputStream in = this.in;
        int ch1 = read();
        int ch2 = read();
        int ch3 = read();
        int ch4 = read();
        if ((ch1 | ch2 | ch3 | ch4) < 0) {
            throw new EOFException();
        }
        if (littleEndian) {
            return ((ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0));
        } else {
            return ((ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0));
        }

    }

    /**
     * Reads a signed 64-bit integer from this data input stream. This method
     * reads eight bytes from the underlying input stream. If the bytes read, in
     * order, are <code>b1</code>,<code>b2</code>,<code>b3</code>,
     * <code>b4</code>,<code>b5</code>,<code>b6</code>, <code>b7</code>, and
     * <code>b8</code>, where
     * <ul>
     * <code>
     *     0 &lt;= b1, b2, b3, b4, b5, b6, b7, b8 &lt;= 255,
     * </code>
     * </ul>
     * <p>
     * then the result is equal to:
     * <p>
     * <blockquote>
     * 
     * <pre>
     * ((long) b1 &lt;&lt; 56) + ((long) b2 &lt;&lt; 48) + ((long) b3 &lt;&lt; 40) + ((long) b4 &lt;&lt; 32)
     *         + ((long) b5 &lt;&lt; 24) + (b6 &lt;&lt; 16) + (b7 &lt;&lt; 8) + b8
     * </pre>
     * 
     * </blockquote>
     * <p>
     * This method blocks until the eight bytes are read, the end of the stream
     * is detected, or an exception is thrown.
     * 
     * @return the next eight bytes of this input stream, interpreted as a
     *         <code>long</code>.
     * @exception EOFException if this input stream reaches the end before
     *                reading eight bytes.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final long readLong() throws IOException {
        InputStream in = this.in;
        if (littleEndian) {
            return ((long) (readInt() & 0xFFFFFFFFL) + (long) (readInt() << 32));
        } else {
            return ((long) (readInt()) << 32) + (readInt() & 0xFFFFFFFFL);
        }

    }
    
    
    /**
     * Reads a signed 32-bit integer blob from this data input stream. This method
     * reads four bytes from the underlying input stream. If the bytes read, in
     * order, are <code>b1</code>,<code>b2</code>,<code>b3</code>, and
     * <code>b4</code>, where 0&nbsp;&lt;=&nbsp; <code>b1</code>,
     * <code>b2</code>,<code>b3</code>,<code>b4</code> &nbsp;&lt;=&nbsp;255,
     * then the result is equal to:
     * <ul>
     * <code>
     *     (b1 &lt;&lt; 24) | (b2 &lt;&lt; 16) + (b3 &lt;&lt; 8) +b4
     * </code>
     * </ul>
     * <p>
     * This method blocks until the four bytes are read, the end of the stream
     * is detected, or an exception is thrown.
     * 
     * @return the next four bytes of this input stream, interpreted as an
     *         <code>int</code>.
     * @exception EOFException if this input stream reaches the end before
     *                reading four bytes.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final int readIntBlob() throws IOException {
        InputStream in = this.in;
        int ch1 = read();
        int ch2 = read();
        int ch3 = read();
        int ch4 = read();
        if ((ch1 | ch2 | ch3 | ch4) < 0) {
            throw new EOFException();
        }
        return ((ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0));
    }

    /**
     * Reads a signed 64-bit integer blob from this data input stream. This
     * method reads eight bytes from the underlying input stream. If the bytes
     * read, in order, are <code>b1</code>,<code>b2</code>,<code>b3</code>,
     * <code>b4</code>,<code>b5</code>,<code>b6</code>, <code>b7</code>, and
     * <code>b8</code>, where
     * <ul>
     * <code>
     *     0 &lt;= b1, b2, b3, b4, b5, b6, b7, b8 &lt;= 255,
     * </code>
     * </ul>
     * <p>
     * then the result is equal to:
     * <p>
     * <blockquote>
     * 
     * <pre>
     * ((long) b1 &lt;&lt; 56) + ((long) b2 &lt;&lt; 48) + ((long) b3 &lt;&lt; 40) + ((long) b4 &lt;&lt; 32)
     *         + ((long) b5 &lt;&lt; 24) + (b6 &lt;&lt; 16) + (b7 &lt;&lt; 8) + b8
     * </pre>
     * 
     * </blockquote>
     * <p>
     * This method blocks until the eight bytes are read, the end of the stream
     * is detected, or an exception is thrown.
     * 
     * @return the next eight bytes of this input stream, interpreted as a
     *         <code>long</code>.
     * @exception EOFException
     *                if this input stream reaches the end before reading eight
     *                bytes.
     * @exception IOException
     *                if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final long readLongBlob() throws IOException {
        return ((long) (readIntBlob()) << 32) + (readIntBlob() & 0xFFFFFFFFL);
    }

    /**
     * Reads a UUID blob from this data input stream. This method blocks until
     * the 128 bytes are read, the end of the stream is detected, or an
     * exception is thrown.
     * 
     * @return uuId
     * @exception EOFException
     *                if this input stream reaches the end before reading 128
     *                bytes.
     * @exception IOException
     *                if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final UUID readUUIDBlob() throws IOException {
        long mostSigBits;
        long leastSigBits;
        mostSigBits = readLong64Blob();
        leastSigBits = readLong64Blob();
        UUID uuId = new UUID(mostSigBits, leastSigBits);

        return uuId;
    }

    /**
     * Reads a signed 64-bit integer blob from this data input stream. This
     * method reads eight bytes from the underlying input stream. If the bytes
     * read, in order, are <code>b1</code>,<code>b2</code>,<code>b3</code>,
     * <code>b4</code>,<code>b5</code>,<code>b6</code>, <code>b7</code>, and
     * <code>b8</code>, where
     * <ul>
     * <code>
     *     0 &lt;= b1, b2, b3, b4, b5, b6, b7, b8 &lt;= 255,
     * </code>
     * </ul>
     * <p>
     * then the result is equal to:
     * <p>
     * <blockquote>
     * 
     * <pre>
     * ((long) b1 &lt;&lt; 56) + ((long) b2 &lt;&lt; 48) + ((long) b3 &lt;&lt; 40) + ((long) b4 &lt;&lt; 32)
     *         + ((long) b5 &lt;&lt; 24) + (b6 &lt;&lt; 16) + (b7 &lt;&lt; 8) + b8
     * </pre>
     * 
     * </blockquote>
     * <p>
     * This method blocks until the eight bytes are read, the end of the stream
     * is detected, or an exception is thrown.
     * 
     * @return the next eight bytes of this input stream, interpreted as a
     *         <code>long</code>.
     * @exception EOFException
     *                if this input stream reaches the end before reading eight
     *                bytes.
     * @exception IOException
     *                if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final long readLong64Blob() throws IOException {
        InputStream in = this.in;
        int ch1 = read();
        int ch2 = read();
        int ch3 = read();
        int ch4 = read();
        int ch5 = read();
        int ch6 = read();
        int ch7 = read();
        int ch8 = read();
        if ((ch1 | ch2 | ch3 | ch4 | ch5 | ch6 | ch7 | ch8) < 0) {
            throw new EOFException();
        }
        return (((long) ch1 << 56) + ((long) ch2 << 48) + ((long) ch3 << 40)
                + ((long) ch4 << 32) + ((long) ch5 << 24) + (ch6 << 16)
                + (ch7 << 8) + (ch8 << 0));

    }

    /**
     * Reads a signed 64-bit integer from this data input stream. This method
     * reads eight bytes from the underlying input stream. If the bytes read, in
     * order, are <code>b1</code>,<code>b2</code>,<code>b3</code>,
     * <code>b4</code>,<code>b5</code>,<code>b6</code>, <code>b7</code>, and
     * <code>b8</code>, where
     * <ul>
     * <code>
     *     0 &lt;= b1, b2, b3, b4, b5, b6, b7, b8 &lt;= 255,
     * </code>
     * </ul>
     * <p>
     * then the result is equal to:
     * <p>
     * <blockquote>
     * 
     * <pre>
     * ((long) b1 &lt;&lt; 56) + ((long) b2 &lt;&lt; 48) + ((long) b3 &lt;&lt; 40) + ((long) b4 &lt;&lt; 32)
     *         + ((long) b5 &lt;&lt; 24) + (b6 &lt;&lt; 16) + (b7 &lt;&lt; 8) + b8
     * </pre>
     * 
     * </blockquote>
     * <p>
     * This method blocks until the eight bytes are read, the end of the stream
     * is detected, or an exception is thrown.
     * 
     * @return the next eight bytes of this input stream, interpreted as a
     *         <code>long</code>.
     * @exception EOFException if this input stream reaches the end before
     *                reading eight bytes.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final long readLong64() throws IOException {
        InputStream in = this.in;
        int ch1 = read();
        int ch2 = read();
        int ch3 = read();
        int ch4 = read();
        int ch5 = read();
        int ch6 = read();
        int ch7 = read();
        int ch8 = read();
        if ((ch1 | ch2 | ch3 | ch4 | ch5 | ch6 | ch7 | ch8) < 0) {
            throw new EOFException();
        }
        if (littleEndian) {
            return (((long) ch8 << 56) + ((long) ch7 << 48)
                    + ((long) ch6 << 40) + ((long) ch5 << 32)
                    + ((long) ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0));
        } else {
            return (((long) ch1 << 56) + ((long) ch2 << 48)
                    + ((long) ch3 << 40) + ((long) ch4 << 32)
                    + ((long) ch5 << 24) + (ch6 << 16) + (ch7 << 8) + (ch8 << 0));
        }
    }

    /**
     * Reads a UUID from this data input stream. This method This method blocks
     * until the 128 bytes are read, the end of the stream is detected, or an
     * exception is thrown.
     * 
     * @return uuId
     * @exception EOFException if this input stream reaches the end before
     *                reading 128 bytes.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public final UUID readUUID() throws IOException {
        long mostSigBits;
        long leastSigBits;

        if (littleEndian) {
            leastSigBits = readLong64();
            mostSigBits = readLong64();
        } else {
            mostSigBits = readLong64();
            leastSigBits = readLong64();
        }
        UUID uuId = new UUID(mostSigBits, leastSigBits);

        return uuId;
    }

    /**
     * Reads an RME String from the input stream. This is a compound type
     * composed of an <code>int</code> which determines the string length
     * followed by the character bytes themselves. RME Strings are always null
     * terminated, and the string length read includes the null character.
     * Therefore an RME string of length 0 is the null string, where an RME
     * String of length 1 is the empty string "".
     * 
     * When storing the bytes into the Java String, the null character is
     * discarded.
     * 
     * The character encoding of the bytes in the string is assumed to be UTF-8,
     * and that encoding is used in the Byte-to-Char conversion when creating a
     * Java String. If the charset of the bytes is unknown, or is not in UTF-8,
     * then readStringBytes should be used instead.
     * 
     * @return String
     * @throws IOException if there is any error
     * @see RmeInputStream#readStringBytes()
     */
    public String readString() throws IOException {
        int len = readInt();
        if (len < 0 || len > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        if (len == 0) {
            return null;
        }
        if (len == 1) {
            if (read() != 0) {
                throw new ProtocolException(ERROR_NO_NULL);
            }
            return "";
        } else {
            byte[] stringBytes = new byte[len - 1];
            read(stringBytes, 0, len - 1);
            if (read() != 0) {
                throw new ProtocolException(ERROR_NO_NULL);
            }
            try {
                return new String(stringBytes, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return new String(stringBytes);
            }
        }
    }

    /**
     * Similar to readString, this method will also read an RME String from the
     * input stream. However, instead of converting the data into a java String
     * type (and performing a possibly incorrect Byte to Char conversion) it
     * will return a byte[] with the characters in the RME String. Note that
     * this function is different than readByteArray in that the null
     * termination character is discarded here.
     * 
     * @return byte [] of possible characters in unknown charset.
     * @throws IOException
     */
    public byte[] readStringBytes() throws IOException {
        int len = readInt();
        if (len < 0 || len > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        if (len == 0) {
            return null;
        }
        if (len == 1) {
            if (read() != 0) {
                throw new ProtocolException(ERROR_NO_NULL);
            }
            return new byte[0];
        } else {
            byte[] stringBytes = new byte[len - 1];
            int numRead = read(stringBytes, 0, len - 1);
            if (read() != 0) {
                throw new ProtocolException(ERROR_NO_NULL);
            }
            return stringBytes;
        }
    }

    /**
     * Reads an array of RME String objects, and returns a Java
     * <code>String []</code> with the data. The readString method is used to
     * read each individual RME String, so the characters are assumed to be in
     * UTF-8 format.
     * 
     * @return String []
     * @throws IOException if error
     * @see RmeInputStream#readString()
     */
    public String[] readStringArray() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        String[] retStr = new String[numElements];
        for (int i = 0; i < numElements; i++) {
            retStr[i] = readString();
        }
        return retStr;
    }

    /**
     * Reads an array of RME String objects which are not null terminated
     * 
     * @return String []
     * @throws IOException if error
     * @see RmeInputStream#readByteStringArray()
     */
    public String[] readByteStringArray() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        String[] retStr = new String[numElements];
        for (int i = 0; i < numElements; i++) {
            retStr[i] = readByteString();
        }
        return retStr;
    }
    
    /**
     * Reads an array of RME String objects, and returns a Java
     * <code>String []</code> with the data. The readStringBytes method is used
     * to read each individual RME String, so the characters are stored in raw.
     * 
     * @return String []
     * @throws IOException if error
     * @see RmeInputStream#readString()
     */
    public String[] readStringArrayInRaw() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        String[] retStr = new String[numElements];
        for (int i = 0; i < numElements; i++) {
            retStr[i] = new String(readStringBytes(), "8859_1");
        }
        return retStr;
    }

    /**
     * Reads an array of RME Integers from the input stream.
     * 
     * @return int [] holding the integers.
     * @throws IOException if error
     */
    public int[] readIntArray() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        int[] retStr = new int[numElements];
        for (int i = 0; i < numElements; i++) {
            retStr[i] = readInt();
        }
        return retStr;
    }

    /**
     * Reads an array of RME Integers from the input stream.
     * 
     * @return long [] holding the long.
     * @throws IOException if error
     */
    public long[] readLongArray() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        long[] retStr = new long[numElements];
        for (int i = 0; i < numElements; i++) {
            retStr[i] = readLong64();
        }
        return retStr;
    }

    /**
     * Reads an array of RME UUIDs from the input stream.
     * 
     * @return retStr -Array holding the UUID.
     * @throws IOException if error
     */
    public UUID[] readUUIDArray() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        UUID[] retStr = new UUID[numElements];
        for (int i = 0; i < numElements; i++) {
            retStr[i] = readUUID();
        }
        return retStr;
    }

    /**
     * Reads an array of RME Bytes from the input stream.
     * 
     * @return byte []
     * @throws IOException
     */
    public byte[] readByteArray() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        byte[] retStr = new byte[numElements];
        read(retStr, 0, numElements);
        return retStr;
    }

    /**
     * Reads an array of RME Bytes from the input stream. and returns a String
     * 
     * @return String
     * @throws IOException
     */
    public String readByteString() throws IOException {
        int numElements = readInt();
        if (numElements < 0 || numElements > CLXN_MAX_SIZE) {
            throw new ProtocolException(ERROR_BAD_LENGTH);
        }
        byte[] retStr = new byte[numElements];
        read(retStr, 0, numElements);
        return new String(retStr);
    }

    /**
     * This method is used by most operations to read a single log event off of
     * the wire, and place it into this operation's log event array
     * 
     * @throws IOException - on any socket error
     */
    public LogEvent[] readLogEvent() throws IOException {
        LogEvent[] logEvents = new LogEvent[0];
        boolean haveIt = readBoolean();
        if (haveIt) {
            logEvents = new LogEvent[1];
            logEvents[0] = new LogEvent(this);
        }
        return logEvents;
    }

    /**
     * This method is used by most operations to read an array of log events off
     * of the wire, and place it into this operation's log event array
     * 
     * @throws IOException - on any socket error
     */
    public LogEvent[] readLogEventArray() throws IOException {
        LogEvent[] logEvents = new LogEvent[0];
        int numLogEvents = readInt();
        if (numLogEvents > 0) {
            logEvents = new LogEvent[numLogEvents];
            for (int i = 0; i < numLogEvents; i++) {
                boolean haveIt = readBoolean();
                if (haveIt) {
                    logEvents[i] = new LogEvent(this);
                } else
                    logEvents[i] = null;
            }
        }
        return logEvents;
    }

    /**
     * debug method to print the remainder of the stream's data to stdout. Each
     * byte is printed in hex format and separated by a space. WARNING: this
     * method will block as it waits for more data to print.
     * 
     * @throws IOException on socket error
     */
    protected void debugReceive() throws IOException {
        // pull the bytes off the socket at least, and print
        int b = 0;
        while ((b = read()) != -1) {
            System.out.print(Integer.toHexString(b) + " ");
        }
    }

    /**
     * Fills the buffer with more data, taking into account shuffling and other
     * tricks for dealing with marks. Assumes that it is being called by a
     * synchronized method. This method also assumes that all data has already
     * been read in, hence pos > count.
     */
    private void fill() throws IOException {
        if (markpos < 0) {
            pos = 0; /* no mark: throw away the buffer */
        } else {
            if (pos >= buf.length) { /* no room left in buffer */
                if (markpos > 0) { /* can throw away early part of the buffer */
                    int sz = pos - markpos;
                    System.arraycopy(buf, markpos, buf, 0, sz);
                    pos = sz;
                    markpos = 0;
                } else if (buf.length >= marklimit) {
                    markpos = -1; /* buffer got too big, invalidate mark */
                    pos = 0; /* drop buffer contents */
                } else { /* grow buffer */
                    int nsz = pos * 2;
                    if (nsz > marklimit) {
                        nsz = marklimit;
                    }
                    byte nbuf[] = new byte[nsz];
                    System.arraycopy(buf, 0, nbuf, 0, pos);
                    buf = nbuf;
                }
            }
        }
        int n = in.read(buf, pos, buf.length - pos);

        count = n <= 0 ? pos : n + pos;
    }

    /**
     * Reads the next byte of data from this buffered input stream. The value
     * byte is returned as an <code>int</code> in the range <code>0</code> to
     * <code>255</code>. If no byte is available because the end of the stream
     * has been reached, the value <code>-1</code> is returned. This method
     * blocks until input data is available, the end of the stream is detected,
     * or an exception is thrown.
     * <p>
     * The <code>read</code> method of <code>BufferedInputStream</code> returns
     * the next byte of data from its buffer if the buffer is not empty.
     * Otherwise, it refills the buffer from the underlying input stream and
     * returns the next character, if the underlying stream has not returned an
     * end-of-stream indicator.
     * 
     * @return the next byte of data, or <code>-1</code> if the end of the
     *         stream is reached.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public int read() throws IOException {
        if (pos >= count) {
            fill();
            if (pos >= count) {
                return -1;
            }
        }
        return buf[pos++] & 0xff;
    }

    /**
     * Reads bytes into a portion of an array. This method will block until
     * <b>all requested </b> input is available, an I/O error occurs, or the end
     * of the stream is reached.
     * 
     * <p>
     * If this stream's buffer is not empty, bytes are copied from it into the
     * array argument. Otherwise, the buffer is refilled from the underlying
     * input stream and, unless the stream returns an end-of-stream indication,
     * the array argument is filled with characters from the newly-filled
     * buffer.
     * 
     * <p>
     * As an optimization, if the buffer is empty, the mark is not valid, and
     * <code>len</code> is at least as large as the buffer, then this method
     * will read directly from the underlying stream into the given array. Thus
     * redundant <code>BufferedInputStream</code> s will not copy data
     * unnecessarily.
     * 
     * @param b destination buffer.
     * @param off offset at which to start storing bytes.
     * @param len maximum number of bytes to read.
     * @return the number of bytes read, or <code>-1</code> if the end of the
     *         stream has been reached.
     * @exception IOException if an I/O error occurs.
     */
    public int read(byte b[], int off, int len) throws IOException {
        // one change.. if all data isn't there, then fill until it is.
        int avail = count - pos;
        int numRead = 0;
        while (avail < len) {
            if (avail > 0) {// if there is some, copy it, and adjust the values
                System.arraycopy(buf, pos, b, off, avail);
                pos += avail;
                numRead += avail;
                len -= avail;
                off += avail;
                avail = 0;
            }

            /*
             * If the requested length is larger than the buffer, and if there
             * is no mark/reset activity, do not bother to copy the bytes into
             * the local buffer. In this way buffered streams will cascade
             * harmlessly.
             */
            if (len >= buf.length && markpos < 0) {
                int lastRead = 0;
                while (lastRead != -1 && len > 0) {
                    lastRead = in.read(b, off, len);

                    numRead += lastRead;
                    len -= lastRead;
                    off += lastRead;
                }

                return numRead;
            }
            fill();
            avail = count - pos;
            if (avail <= 0) {
                return -1;
            }
        }
        numRead += len;
        System.arraycopy(buf, pos, b, off, len);
        pos += len;
        return numRead;
    }

    /**
     * Skips over and discards <code>n</code> bytes of data from the input
     * stream. The <code>skip</code> method may, for a variety of reasons, end
     * up skipping over some smaller number of bytes, possibly zero. The actual
     * number of bytes skipped is returned.
     * <p>
     * The <code>skip</code> method of <code>BufferedInputStream</code> compares
     * the number of bytes it has available in its buffer, <i>k </i>, where <i>k
     * </i>&nbsp;= <code>count&nbsp;- pos</code>, with <code>n</code>. If
     * <code>n</code> &nbsp;&le;&nbsp; <i>k </i>, then the <code>pos</code>
     * field is incremented by <code>n</code>. Otherwise, the <code>pos</code>
     * field is incremented to have the value <code>count</code>, and the
     * remaining bytes are skipped by calling the <code>skip</code> method on
     * the underlying input stream, supplying the argument <code>n&nbsp;-</code>
     * &nbsp; <i>k </i>.
     * 
     * @param n the number of bytes to be skipped.
     * @return the actual number of bytes skipped.
     * @exception IOException if an I/O error occurs.
     */
    public long skip(long n) throws IOException {
        if (n < 0) {
            return 0;
        }
        long avail = count - pos;

        if (avail >= n) {
            pos += n;
            return n;
        }

        pos += avail;
        return avail + in.skip(n - avail);
    }

    /**
     * Returns the number of bytes that can be read from this input stream
     * without blocking.
     * <p>
     * The <code>available</code> method of <code>BufferedInputStream</code>
     * returns the sum of the the number of bytes remaining to be read in the
     * buffer (<code>count&nbsp;- pos</code>) and the result of calling the
     * <code>available</code> method of the underlying input stream.
     * 
     * @return the number of bytes that can be read from this input stream
     *         without blocking.
     * @exception IOException if an I/O error occurs.
     * @see java.io.FilterInputStream#in
     */
    public int available() throws IOException {
        return (count - pos) + in.available();
    }

    /**
     * Marks the current position in this input stream. A subsequent call to the
     * <code>reset</code> method repositions the stream at the last marked
     * position so that subsequent reads re-read the same bytes.
     * <p>
     * The <code>readlimit</code> argument tells the input stream to allow that
     * many bytes to be read before the mark position gets invalidated.
     * 
     * @param readlimit the maximum limit of bytes that can be read before the
     *            mark position becomes invalid.
     * @see java.io.BufferedInputStream#reset()
     */
    public void mark(int readlimit) {
        marklimit = readlimit;
        markpos = pos;
    }

    /**
     * Repositions this stream to the position at the time the <code>mark</code>
     * method was last called on this input stream.
     * <p>
     * If the stream has not been marked, or if the mark has been invalidated,
     * an IOException is thrown. Stream marks are intended to be used in
     * situations where you need to read ahead a little to see what's in the
     * stream. Often this is most easily done by invoking some general parser.
     * If the stream is of the type handled by the parser, it just chugs along
     * happily. If the stream is not of that type, the parser should toss an
     * exception when it fails. If an exception gets tossed within readlimit
     * bytes, the parser will allow the outer code to reset the stream and to
     * try another parser.
     * 
     * @exception IOException if this stream has not been marked or if the mark
     *                has been invalidated.
     * @see java.io.BufferedInputStream#mark(int)
     */
    public void reset() throws IOException {
        if (markpos < 0) {
            throw new IOException("Resetting to invalid mark");
        }
        pos = markpos;
    }
}
