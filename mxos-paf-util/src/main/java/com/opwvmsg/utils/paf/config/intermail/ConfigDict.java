/*  
 *      Copyright 2005 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: 
 */
package com.opwvmsg.utils.paf.config.intermail;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.config.Impact;
import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;

/**
 * A representation of the actual config data.
 */
public class ConfigDict {

    TreeMap theDict = null;
    HashMap deltas = null;
    int size = 0;
    int changeSize = 0;

    Logger logger = Logger.getLogger(ConfigDict.class);

    /**
     * This constructor is used to create a blank config dict
     * 
     * @param size
     */
    public ConfigDict() {
        theDict = new TreeMap(new AsciiComparator());
    }

    /**
     * This constructor is used to create a ConfigDict from a RME stream
     * 
     * @param inStream
     * @throws IOException
     */
    public ConfigDict(RmeInputStream inStream) throws IOException {
        theDict = new TreeMap(new AsciiComparator());

        size = inStream.readInt();

        if (logger.isDebugEnabled()) {
            logger.debug("Reading Dict: " + size);
        }

        for (int i = 0; i < size; i++) {
            boolean haveIt = inStream.readBoolean();
            if (haveIt) {
                String key = inStream.readString();
                String value = inStream.readString();
                put(key, value);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Reading Deltas");
        }
        changeSize = inStream.readInt();
        deltas = new HashMap(changeSize);
        for (int i = 0; i < changeSize; i++) {
            String key = inStream.readString();
            int deltaV = inStream.readInt();
            if (logger.isDebugEnabled()) {
                logger.debug("Key: " + key + " ==> " + deltaV);
            }
            deltas.put(key, new Integer(deltaV));
        }
    }

    /**
     * Gets all of the keys in this config.db
     * 
     * @return Set of keys in this config.db
     */
    protected Set getAllKeys() {
        return theDict.keySet();
    }

    /**
     * Get the array of values associated with this key.
     * 
     * @param key the key name
     * @return values
     */
    protected String[] getValues(String key) {
        ConfigEntry entry = (ConfigEntry)theDict.get(key.toLowerCase());
        if (entry == null) {
            return null;
        }
        return entry.getValues();
    }

    /**
     * Gets the original key name as it appears in the config.db
     * 
     * @param key the key name (in any case)
     * @return the key name in the original case.
     */
    protected String getOriginalKey(String key) {
        ConfigEntry entry = (ConfigEntry)theDict.get(key.toLowerCase());
        if (entry == null) {
            return null;
        }
        return entry.getKey();
    }

    /**
     * Puts the value into this local dictionary. Assumes that this is a single
     * value, but per config.db standards, this value will be split into
     * multi-values on any newlines.
     * 
     * @param key the key name
     * @param value the value
     */
    protected void put(String key, String value) {
        ConfigEntry entry = new ConfigEntry(key, value);
        theDict.put(key.toLowerCase(), entry);
    }

    /**
     * Puts the values into this local dictionary.
     * 
     * @param key
     * @param values
     */
    protected void put(String key, String[] values) {
        ConfigEntry entry = new ConfigEntry(key, values);
        theDict.put(key.toLowerCase(), entry);
    }

    /**
     * Removes a key from the this dictionary.
     * 
     * @param key the key to remove.
     */
    protected void remove(String key) {
        theDict.remove(key.toLowerCase());
    }

    /**
     * Gets the number of keys in this config dictionary.
     * 
     * @return number of config keys.
     */
    public int getDictSize() {
        return theDict.size();
    }

    /**
     * Update the list of configured hosts.
     * 
     * @return the new set of configured hosts.
     */
    protected Set updateConfiguredHosts() {
        Set keys = theDict.keySet();
        Set hosts = new HashSet(53);
        Iterator iter = keys.iterator();
        while (iter.hasNext()) {
            String key = (String)iter.next();
            // key should always be of the form /host/app/key
            int slash = key.indexOf('/', 1);
            if (slash != -1) {
                String host = key.substring(1, slash);
                hosts.add(host);
            }
        }
        hosts.remove("*");
        return hosts;
    }

    /**
     * Update the config.db with the values in this dictionary.
     * 
     * @param cfg the IntermailConfig to update
     * @param incremental if true, this dictionary represents only a partial
     *            config.db of changes.
     */
    public void updateIntermailConfig(IntermailConfig cfg, boolean incremental) {
        if (incremental) {
            cfg.updateConfigDict(this, true, deltas);
        } else {
            cfg.updateConfigDict(this, false, null);
        }
    }

    /**
     * Loops through the key deltas, and returns a Map that matches each delta
     * value to it's impact.
     * 
     * @return a Map of deltas to impacts
     * @throws ConfigException on any error getting the config impacts
     */
    public Map assessImpacts() throws ConfigException {
        Map keyImpacts = Config.getKeyImpacts();
        if (keyImpacts == null) {
            return new HashMap(0);
        }
        HashMap impacts = new HashMap(deltas.size());
        Set s = deltas.keySet();
        String hostname = "*";
        String application = null;
        Iterator i = s.iterator();
        while (i.hasNext()) {
            String key = (String)i.next();
            int impact = -1;
            hostname = "*";
            application = null;
        
            if (keyImpacts != null) {
                key = key.trim();
                if (logger.isDebugEnabled()) {
                    logger.debug("Assess impact for key : " + key);
                }
                String results[] = key.split("/");
                if (results.length >= 4 ) {
                    hostname = results[1];
                    application = results[2];
                    key = results[3];
                } else if (results.length > 0) {
                    key = results[results.length - 1];
                }
                if (("*".equals(hostname) || Config.getDefaultHost().equals(hostname)) && !Config.getDefaultApp().equals(application)) {
                    Object impactObj = keyImpacts.get(key);
                    if (impactObj != null) {
                        impact = Integer.parseInt(impactObj.toString());
                    }
                }
            }

            switch (impact) {
                case (Config.CONFIG_IMPACT_TRIVIAL):
                    impacts.put(key, new Integer(Impact.IMPACT_TRIVIAL));
                    if (logger.isDebugEnabled()) {
                        logger.debug("Impact for hostname: " + hostname + " application: " + application 
                               + " key: " + key + " is: trival");
                    }
                    break;
                case (Config.CONFIG_IMPACT_RESTART):
                    impacts.put(key, new Integer(Impact.IMPACT_RESTART));
                    if (logger.isDebugEnabled()) {
                        logger.debug("Impact for hostname: " + hostname + " application: " + application 
                               + " key: " + key + " is: restart");
                    }
                    break;
                default:
                    if (logger.isDebugEnabled()) {
                        logger.debug("Impact for hostname: " + hostname + " application: " + application 
                               + " key: " + key + " is: no impact");
                    }
                  // no impact
            }
        }
        return impacts;
    }
    
    private static final class AsciiComparator implements Comparator {
        // the Collator class does not sort strings with "-" signs correctly,
        // so a simple Comparator here will take its place.
        
        public AsciiComparator() {            
        }
        
        public int compare(Object o1, Object o2) {
            String s1 = (String)o1;
            String s2 = (String)o2;
            if (s1 == null && s2 != null) {
                return -1;
            } else if (s2 == null && s1 != null) {
                return 1;                
            } else if (s1 == null && s2 == null) {
                return 0;
            }
            return s1.compareToIgnoreCase(s2);
        }
        
        public boolean equals (Object o1, Object o2) {
            return (compare(o1, o2) == 0);
        }
    }


}

