/*
 * Copyright (c) 2002-2005 Openwave Systems Inc.  All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/NoSuchProviderException.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config;

/**
 * An exception that occurs if the requested provider is not found.
 */
public class NoSuchProviderException extends ConfigException {

    /**
     * Create an instance.
     */
    public NoSuchProviderException() {
        super();
    }

    /**
     * Create an instance with the specified message.
     *
     * @param s the message
     */
    public NoSuchProviderException(String s) {
        super(s);
    }

    /**
     * Create an instance which wraps the given throwable.
     *
     * @param e the throwable
     */
    public NoSuchProviderException(Throwable e) {
        super(e);
    }

    /**
     * Create an instance with the specified message and which wraps
     * the given throwable.
     *
     * @param s the message
     * @param e the throwable
     */
    public NoSuchProviderException(String s, Throwable e) {
        super(s, e);
    }
}
