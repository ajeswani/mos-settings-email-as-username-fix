/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;

/**
 * This class encapsulates all of the MailboxInfo data of the ReadMailbox RME
 * 
 * @author mxos-dev
 */
public class MailboxInfo {
    private static final Logger logger = Logger.getLogger(MailboxInfo.class);

    // Attributes
    private long lastAccessDate;

    // Constants for Mailbox Info Attribute Types
    public static final byte ATTR_LAST_ACCESS_DATE_ID = (byte) 0x06;

    /**
     * Default Constructor. Reads the input Stream and constructs the
     * MailboxInfo.
     * 
     * @param inStream
     * @throws IOException
     */
    public MailboxInfo(RmeInputStream inStream) throws IOException {
        byte attrType = 0;
        attrType = inStream.readByte();
        switch (attrType) {
        case ATTR_LAST_ACCESS_DATE_ID:
            inStream.readInt(); // Last Access Date Length
            lastAccessDate = inStream.readLongBlob();
            break;
        default:
            logger.debug("Invalid Attribute Type " + attrType
                    + " received in the MSS_SL_MAILBOXINFO response.");
        }
    }

    /**
     * @return the lastAccessDate
     */
    public long getLastAccessDate() {
        return lastAccessDate;
    }

    /**
     * @param lastAccessDate
     *            the lastAccessDate to set
     */
    public void setLastAccessDate(long lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    /**
     * for debugging - output all of the mailboxInfo parameters
     * 
     * @param out
     *            a PrintStream to dump the data.
     */
    public void dump() {
        System.out.println("DUMPING MailboxInfo ");
        System.out.println("lastAccessDate: " + this.getLastAccessDate());
        System.out.println("==== DONE ==== ");
        System.out.println();
    }
}
