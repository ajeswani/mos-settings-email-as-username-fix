/*
 * Copyright (c) 2002-2005 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/Config.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config;

import com.opwvmsg.utils.paf.config.spi.ConfigProvider;
import com.opwvmsg.utils.paf.util.Service;

import java.util.Iterator;
import java.util.Map;

/**
 * An abstract class that provides access to configuration information.
 * <p/>
 * Configuration data may come from any one of a number of sources, such as
 * a properties file or a database. The choice of service provider determines
 * the source of the data.  Only a single provider may be in use at any one
 * time.  This class must be extended by each provider implementation.
 * <p/>
 * Configuration data is looked up using a combination of host, application, 
 * and key. The host and application may be specified at the time the data is
 * requested; otherwise, a default host and application are used. The
 * default host and application may be set for all instances of this class 
 * using static setter methods, or through the setup configuration passed 
 * to the {@link #setConfig(String,Map) setConfig} method. If not set, the
 * default host is "localhost" and the default application is "common".
 * <p/>
 * For each type of <code>get</code> method, there is one signature that 
 * takes a host, application, key triplet and one that takes just a key 
 * and uses default values for host and application.  Any method that 
 * accepts a host or application also falls back to the default if the 
 * value <code>null</code> is given.
 * <p/>
 * For each type of <code>get</code> method, there is also one signature that
 * takes a default result and one that does not.  If a default result is not
 * supplied and the key is not found in the configuration data, an exception 
 * occurs.
 * <p/>
 * This class is thread safe.
 *
 * @author Conrad Damon
 * @author Mark Abbott
 * @version $Revision: #1 $
 */

public abstract class Config {

    /**
     * The provider enables access to a Config object.
     */
    private static ConfigProvider provider = null;

    /**
     * A map of information needed to initialize the provider; for example, a
     * host or port for a database connection.  If the
     * <code>setup</code> map contains entries for "defaultHost" or
     * "defaultApp", the values are used to set those defaults for all
     * <code>Config</code> instances.
     */
    private static Map setup;

    /**
     * A Map containing the impact severity of various configuration keys. 
     * Impacts are optional and provider specific but may map to
     * severities such as "trivial" or "restart" indicating the
     * consequences that changes in the key's value have on a running
     * application.
     */
    private static Map keyImpacts = null;

    public static final int CONFIG_IMPACT_TRIVIAL = 0;
    public static final int CONFIG_IMPACT_RESTART = 1;

    /**
     * Default values for host and application.
     */
    private static String defaultHost = "localhost";
    private static String defaultApp = "common";

    public static final String SITE_MODE = "siteMode";
    public static final String SITE_NAME = "siteName";
    public static enum ClusterSiteMode {
        NORMAL,
        FAILOVER,
        FALLBACK,
        FALLBACKCOMPLETE,
        DOWN,
        DEFAULTMODE;

        public static ClusterSiteMode getByValue(String value) {
            for (ClusterSiteMode mode : values()) {
                if (mode.name().equalsIgnoreCase(value)) {
                    return mode;
                }
            }
            return DEFAULTMODE;
        }
    };

    /**
     * Gets an instance of this class using the provider and configuration data
     * specified by an earlier call to <code>setConfig</code>.
     *
     * @return An instance of the Config class.
     * @throws ConfigException If the provider is not found or there is a
     * problem getting to the configuration data source.
     */
    public static synchronized Config getInstance() throws ConfigException {
        try {
            if (provider != null) {
                return provider.getConfig(setup);
            } else {
                throw new ConfigException("No Config provider has been set");
            }
        } catch (ConfigException e) {
            throw new ConfigException("Could not create Config instance",
                    e.getCause() == null ? e : e.getCause());
        }
    }

    /**
     * Sets the provider type and any relevant configuration the provider may
     * require.  If the <code>setup</code> map contains entries for
     * "defaultHost" or "defaultApp", the values are used to set those defaults
     * for all <code>Config</code> instances.
     *
     * @param type The name of the provider.
     * @param setup A map of setup information needed by the provider.
     */
    public static synchronized void setConfig(String type, Map setup)
            throws NoSuchProviderException {

        Iterator i = Service.providers(ConfigProvider.class);
        while (i.hasNext()) {
            ConfigProvider provider = (ConfigProvider) i.next();
            if (provider.isType(type)) {
                Config.provider = provider;
                Config.setup = setup;

                String defaultHost = (String) setup.get("defaultHost");
                if (defaultHost != null) {
                    Config.setDefaultHost(defaultHost);
                }
                String defaultApp = (String) setup.get("defaultApp");
                if (defaultApp != null) {
                    Config.setDefaultApp(defaultApp);
                }

                break;
            }
        }
        if (provider == null) {
            throw new NoSuchProviderException("Provider " + type +
                    " not found");
        }
    }

    /**
     * Sets the default host.
     * <p/>
     *
     * @param host The default host.
     */
    public static synchronized void setDefaultHost(String host) {
        Config.defaultHost = host;
    }

    /**
     * Gets the default host.
     * <p/>
     *
     * @return The default host.
     */
    public static synchronized String getDefaultHost() {
        return Config.defaultHost;
    }

    /**
     * Sets the default application.
     * <p/>
     *
     * @param app The default application.
     */
    public static synchronized void setDefaultApp(String app) {
        Config.defaultApp = app;
    }

    /**
     * Gets the default application.
     * <p/>
     *
     * @return The default application.
     */
    public static synchronized String getDefaultApp() {
        return Config.defaultApp;
    }

    /**
     * Tests to see if the underlying provider is of the specified type
     * 
     * @param providerType
     * @return true if the provider is of the specified type, false otherwise
     */
    public static synchronized boolean isType(String providerType) {
        if (provider == null) {
            return false;
        }
        return provider.isType(providerType);
    }

    /**
     * Sets the severity level for configuration keys.
     * <p/>
     * Impacts are optional and provider specific but may map to
     * levels such as "trivial" or "restart", indicating the
     * consequences that changes in the key's value have on a running
     * application.
     * <p/>
     *
     * @param impacts Specifies the configuration key severity levels.
     */
    public static synchronized void setKeyImpacts(Map impacts) {
        Config.keyImpacts = impacts;
    }

    /**
     * Gets the severity level for all configuration keys.
     * <p/>
     * Impacts are optional and provider specific but may map to
     * levels such as "trivial" or "restart", indicating the
     * consequences that changes in the key's value have on a running
     * application.
     * <p/>
     *
     * @return The default application.
     */
    public static synchronized Map getKeyImpacts() {
        return Config.keyImpacts;
    }


    /**
     * Gets the severity level for a specific configuration key.
     * <p/>
     * Impacts are optional and provider specific but may map to
     * levels such as "trivial" or "restart", indicating the
     * consequences that changes in the key's value have on a running
     * application.
     * <p/>
     *
     * @param keyname The desired key.
     * @return The severity level for the specified key.
     */
    public static int getKeyImpact(String keyname) {
        int result = 0;
        if (keyImpacts != null) {
            Object impact = keyImpacts.get(keyname);
            if (impact != null) {
                result = Integer.parseInt(impact.toString());
            }
        }
        return result;
    }

    /**
     * Does nothing. Subclasses may define their own constructors. The caller
     * uses <code>getInstance</code> to invoke the constructor of the
     * appropriate provider.
     */
    protected Config() {
    }

    /**
     * Checks the configuration to determine if this host name exists in 
     * any of the keys.  Most providers probably don't implement this function, so
     * the default is to return true.
     * 
     * @param hostName the host to lookup
     * @return true if the host exists in at least one key, false otherwise
     */
    public boolean isAConfiguredHost(String hostName) {
        return true;
    }

    /**
     * Checks to see if the server is configured to run on the specified host. 
     * By default returns true, in case providers don't implement this.
     * 
     * @param hostName The name of the host
     * @param server The name of the server
     * @return true if the server is configured to run on the host, false
     *         otherwise
     */
    public boolean serverRunsOnHost(String hostName, String server) {
        return true;
    }

    /**
     * Gets the configuration value with the given host, application, and 
     * key, as a String. Leading and trailing white space is trimmed. 
     * If the value is empty, the empty string (rather than null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the
     * first value of a multi-valued entry is returned by this method.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The value of the configuration item with this key.
     * @throws ConfigException If the key does not exist.
     */
    public abstract String get(String host, String app, String key)
            throws ConfigException;

    /**
     * Gets the configuration value with the given host, application, 
     * and key, as a String. Leading and trailing white space is trimmed. 
     * If the value is empty, the empty string (rather than null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the
     * first value of a multi-valued entry is returned by this method.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public String get(String host, String app, String key, String def) {
        try {
            return get(host, app, key);
        } catch (ConfigException e) {
            return def;
        }
    }

    /**
     * Gets the configuration value with the given key, as a String. 
     * The current default host and application values are used. 
     * Leading and trailing white space is trimmed. If the value is empty, 
     * the empty string (rather than null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the
     * first value of a multi-valued entry is returned by this method.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @return The value of the configuration item with this key.
     * @throws ConfigException If the key does not exist.
     */
    public String get(String key) throws ConfigException {
        return get(defaultHost, defaultApp, key);
    }

    /**
     * Gets the configuration value with the given key, as a String. 
     * The current default host and application values are used. 
     * Leading and trailing white space is trimmed. If the value is empty, 
     * the empty string (rather than null) is returned.
     * <p/>
     * If a provider supports multi-valued entries, only the
     * first value of a multi-valued entry is returned by this method.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public String get(String key, String def) {
        return get(defaultHost, defaultApp, key, def);
    }

    /**
     * Gets the configuration value with the given host, application, 
     * and key, as a boolean. This method follows the Java convention 
     * whereby the string "true" evaluates to true, and anything else is false.
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return A value of true or false. Returns false if the value is empty.
     * @throws ConfigException If the key does not exist.
     */
    public abstract boolean getBoolean(String host, String app, String key)
            throws ConfigException;

    /**
     * Gets the configuration value with the given host, application, 
     * and key, as a boolean. This method follows the Java convention 
     * whereby the string "true" evaluates to true, and anything else is false.
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return A value of true or false. Returns false if the value is empty, 
     * or the provided default value if the key does not exist.
     */
    public boolean getBoolean(
            String host, String app, String key, boolean def) {
        try {
            return getBoolean(host, app, key);
        } catch (ConfigException e) {
            return def;
        }
    }

    /**
     * Gets the configuration value with the given key, as a boolean. The
     * current default host and application values are used. This method 
     * follows the Java convention whereby the string "true" evaluates 
     * to true, and anything else is false.
     *
     * @param key The name of a configuration key.
     * @return A value of true or false. Returns false if the value is empty.
     * @throws ConfigException If the key does not exist.
     */
    public boolean getBoolean(String key) throws ConfigException {
        return getBoolean(defaultHost, defaultApp, key);
    }

    /**
     * Gets the configuration value with the given key, as a boolean. The
     * current default host and application values are used. This method 
     * follows the Java convention whereby the string "true" evaluates to 
     * true, and anything else is false.
     *
     * @param key The name of a configuration key.
     * @param def The default value.
     * @return A value of true or false. Returns false if the value is empty, 
     * or the provided default value if the key does not exist.
     */
    public boolean getBoolean(String key, boolean def) {
        return getBoolean(defaultHost, defaultApp, key, def);
    }

    /**
     * Gets the configuration value with the given host, application, 
     * and key, as an integer.
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The integer value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the value 
     * cannot be parsed as an integer.
     */
    public abstract int getInt(String host, String app, String key)
            throws ConfigException;

    /**
     * Gets the configuration value with the given host, application, 
     * and key, as an integer.
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The integer value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public int getInt(String host, String app, String key, int def) {
        try {
            return getInt(host, app, key);
        } catch (ConfigException e) {
            return def;
        }
    }

    /**
     * Gets the configuration value with the given key, as an integer. The
     * current default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @return The integer value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the value
     * cannot be parsed as an integer.
     */
    public int getInt(String key) throws ConfigException {
        return getInt(defaultHost, defaultApp, key);
    }

    /**
     * Gets the configuration value with the given key, as an integer. The
     * current default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The integer value of the configuration item with this key, 
     * or the provided default value if the key does not exist.
     */
    public int getInt(String key, int def) {
        return getInt(defaultHost, defaultApp, key, def);
    }

    /**
     * Gets the configuration value with the given host, application, 
     * and key, as a float.
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The float value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the 
     * value cannot be parsed as a float.
     */
    public abstract float getFloat(String host, String app, String key)
            throws ConfigException;

    /**
     * Gets the configuration value with the given host, application, 
     * and key, as a float.
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The float value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public float getFloat(String host, String app, String key, float def) {
        try {
            return getFloat(host, app, key);
        } catch (ConfigException e) {
            return def;
        }
    }

    /**
     * Gets the configuration value with the given key, as a float. The current
     * default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @return The float value of the configuration item with this key.
     * @throws ConfigException If the key does not exist, or if the value 
     * cannot be parsed as a float.
     */
    public float getFloat(String key) throws ConfigException {
        return getFloat(defaultHost, defaultApp, key);
    }

    /**
     * Gets the configuration value with the given key, as a float. The current
     * default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The float value of the configuration item with this key, or the
     *         provided default value if the key does not exist.
     */
    public float getFloat(String key, float def) {
        return getFloat(defaultHost, defaultApp, key, def);
    }


    /**
     * Gets the multi-valued configuration value with the given host,
     * application, and key as a String array.
     * <p/>
     * If a provider doesn't support multi-valued entries or the
     * entry is not multi-valued, then the value is returned in a
     * String array of length one.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default value.
     * @return The array of values for the configuration item with this key, or
     *         the provided default value if the key does not exist
     * @deprecated Replaced by {@link #getArray(String,String,String,String[])}
     */
    public String[] getArray(String host, String app, String key, String def) {
        return getArray(host, app, key, new String[] { def });
    }


    /**
     * Gets the multi-valued configuration value with the given host,
     * application, and key as a String array.
     * <p/>
     * If a provider doesn't support multi-valued entries or the
     * entry is not multi-valued, the value is returned in a
     * String array of length one.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @param def The default values.
     * @return The array of values for the configuration item with this key, or
     *         the provided default value if the key does not exist.
     */
    public String[] getArray(
            String host, String app, String key, String[] def) {
        try {
            return getArray(host, app, key);
        } catch (ConfigException e) {
            return def;
        }
    }


    /**
     * Gets the multi-valued configuration value with the given host,
     * application, and key as a String array.
     * <p/>
     * If a provider doesn't support multi-valued entries or the
     * entry is not multi-valued, the value is returned in a
     * String array of length one.
     * <p/>
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return The array of values for the configuration item with this key
     * @throws ConfigException If the key does not exist.
     */
    public abstract String[] getArray(String host, String app, String key)
            throws ConfigException;


    /**
     * Gets the multi-valued configuration value with the given key,
     * as a String. 
     * The current default host and application values are used. Leading 
     * and trailing white space is trimmed. If the value is empty, the 
     * empty string (rather than null) is returned.
     * <p/>
     * If a provider doesn't support multi-valued entries or the
     * entry is not multi-valued, the value is returned in a
     * String array of length one.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @return The array of values of the configuration item with this key.
     * @throws ConfigException If the key does not exist.
     */
    public String[] getArray(String key) throws ConfigException {
        return getArray(defaultHost, defaultApp, key);
    }

    /**
     * Gets the multi-valued configuration value with the given key,
     * as a String array. The current default host and application are used.
     * Leading and trailing white space is trimmed. If the value is
     * empty, the empty string (rather than null) is returned.
     * <p/>
     * If a provider doesn't support multi-valued entries or the
     * entry is not multi-valued, the value is returned in a
     * String array of length one.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @param def The default value to use for this key.
     * @return The value of the configuration item with this key.
     * @deprecated Replaced by {@link #getArray(String,String[])}
     */
    public String[] getArray(String key, String def) {
        return getArray(defaultHost, defaultApp, key, new String[] { def });
    }


    /**
     * Gets the multi-valued configuration value with the given key,
     * as a String array. The current default host and application are used.
     * Leading and trailing white space is trimmed. If the value is
     * empty, the empty string (rather than null) is returned.
     * <p/>
     * If a provider doesn't support multi-valued entries or the
     * entry is not multi-valued, the value is returned in a
     * String array of length one.
     * <p/>
     *
     * @param key The name of this configuration item.
     * @param def The default values to use for this key.
     * @return The value of the configuration item with this key.
     */
    public String[] getArray(String key, String[] def) {
        return getArray(defaultHost, defaultApp, key, def);
    }


    /**
     * Returns a value of true if a if the given
     * host, application, and key combination exists.
     *
     * @param host The host associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param app The application associated with this configuration item, or
     * <code>null</code> to use the default.
     * @param key The name of this configuration item.
     * @return A value of <code>true</code> if the given key exists.
     */
    public abstract boolean contains(String host, String app, String key);

    /**
     * Returns a value of true if if the given key exists.
     * The current default host and application values are used.
     *
     * @param key The name of this configuration item.
     * @return A value of <code>true</code if the given key exists.
     */
    public boolean contains(String key) {
        return contains(defaultHost, defaultApp, key);
    }

    /**
     * Removes a cached value from the configuration cache, so it can
     * be retrieved from the configuration store next time the host,
     * application, and key combination is requested.
     *
     * @param host The host part of the cache key.
     * @param app The application part of the cache key.
     * @param key The name of the key.
     */
    public abstract void removeCachedValue(String host, String app, String key);

}
