/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MessageMetadata;
import com.opwvmsg.utils.paf.intermail.mail.AttributeDescription;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation will read a message from the MSS. It has a variety of options
 * to control how much of the message you will retrieve from the most basic of
 * meta data, to the headers, mime information, or the text itself.
 * 
 * @author mOS_dev
 */
public class ReadSortedMsgs extends AbstractMssOperation {

    // input
    private String name; // Mailbox Id
    private UUID folderUUID; // folder from which we want list of messages.
    private short keyType; // Fetch messages sorted based on particular index
                           // like To/From/CC/Subject
    private String sliceStart; // Represents start of the query range. A blank
                               // string means the smallest entry in the list.
    private String sliceEnd; // Represents end of the query range. A blank
                             // string means the largest entry in the list.
    private int sliceCount; // Number of messages starting from sliceStart.
    private int sliceReversed; // 0 for Ascending, 1 for Descending
    private short[] attrType; // List of attribute types to request for in reply
    private boolean conversationViewReq; // group the conversations
    private boolean includePopDeletedMsgs; // Include pop deleted messages

    // constants

    // KeyTypes: Specifies the Sort Order of the Messages that are to be
    // fetched.
    public static final short SORTED_BASED_ON_MESSAGE_UUID = 0;
    public static final short SORTED_BASED_ON_ATTACHMENTS = 4;
    public static final short SORTED_BASED_ON_RICHMAIL_FLAG = 5;
    public static final short SORTED_BASED_ON_ARRIVAL_TIME = 6;
    public static final short SORTED_BASED_ON_SIZE = 11;
    public static final short SORTED_BASED_ON_UID = 12;
    public static final short SORTED_BASED_ON_PRIORITY = 16;
    public static final short SORTED_BASED_ON_SUBJECT = 18;
    public static final short SORTED_BASED_ON_FROM_ADDRESS = 19;
    public static final short SORTED_BASED_ON_TO_FIELD = 20;
    public static final short SORTED_BASED_ON_CC_FIELD = 21;
    public static final short SORTED_BASED_ON_DATE = 22;
    public static final short SORTED_BASED_ON_CONVERSATION_HISTORY = 23;

    public static final short UUID_LENGTH = 36;

    public static final int SORT_ASCENDING = 0x00;
    public static final int SORT_DESCENDING = 0x01;

    // output
    private MessageMetadata[] messageMetadata;
    private LinkedHashMap<Integer, MessageMetadata[]> sortedMessages = null;
    private int numConversations;
    private int numEntries;

    // internal
    private String url;
    private String hostHeader;

    /**
     * Default Constructor
     * 
     * @param host The MSS hostname for the user.
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID folder from which we want list of messages.
     * @param keyType Fetch messages sorted based on particular index 0 -
     *            messageUUID, 4 - Attachments,5 - Richmail Flag,6 - arrival
     *            time,11 - size,12 - UID,16 - Priority,18- Subject,19 - From,20
     *            - To,21 - Cc,22 - Date,23 - Conversation History.
     * @param sliceStart Represents start of the query range. A blank string
     *            means the smallest entry in the list.
     * @param sliceEnd Represents end of the query range. A blank string means
     *            the largest entry in the list.
     * @param sliceCount Number of messages starting from sliceStart.
     * @param sliceReversed: 0 for Ascending, 1 for Descending
     * @param attrType: List of attribute types to request for in reply. If the
     *            vector is empty, MSS returns all supporting attributes. The
     *            attribute messageUUID is always included in the reply even
     *            though it is not specified. 0: messageUUID,1: bounce,2:
     *            msgDeliveredNDR,3: msgPrivate,4: hasAttachments,5: richmail
     *            flag,6: arrivalTime,7:
     *            msgExpirationSeconds,8:msgFirstSeenSeconds,9:
     *            timeFirstAccessed,10: timeLastAccessed,11: size,12: uid,13:
     *            msgFlags,14: keywords,15: msgType,16:priority,17:
     *            multipleMsgs,18: subject,19: from,20: to,21: cc,22: date,23:
     *            references
     * @param conversationViewReq: true - if conversation view is required false
     *            - if conversation view is not required
     * @param includePopDeletedMsgs true, to include special-deleted msgs in return
     *            - false, to exclude special-deleted msgs in return          
     */
    public ReadSortedMsgs(String host, String name, String realm,
            UUID folderUUID, short keyType, String sliceStart, String sliceEnd,
            int sliceCount, int sliceReversed, short[] attrType,
            boolean conversationViewReq, boolean includePopDeletedMsgs) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_RETRIEVESORTEDMSGS);
        this.folderUUID = folderUUID;
        this.keyType = keyType;
        this.sliceStart = sliceStart;
        this.sliceEnd = sliceEnd;
        this.sliceCount = sliceCount;
        this.sliceReversed = sliceReversed;
        this.attrType = attrType;
        this.conversationViewReq = conversationViewReq;
        this.includePopDeletedMsgs = includePopDeletedMsgs;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (folderUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "folderUUID", "ReadSortedMsgs" }));
        }
        if (sliceReversed < SORT_ASCENDING || sliceReversed > SORT_DESCENDING) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "sliceReversed", "ReadSortedMsgs" }));
        }
        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_SL_RETRIEVESORTEDMSGS", host,
                            "dataModel=" + rmeDataModel }));
        } else {
            callRme(MSS_SL_RETRIEVESORTEDMSGS);
        }
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_RETRIEVESORTEDMSGS));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeUUID(folderUUID);
        outStream.writeShort(keyType);

        if (keyType == SORTED_BASED_ON_MESSAGE_UUID && sliceStart != null
                && sliceStart.length() == UUID_LENGTH) {
            outStream.writeUUIDBlob(UUID.fromString(sliceStart));
        } else {
            outStream.writeString(sliceStart);
        }
        if (keyType == SORTED_BASED_ON_MESSAGE_UUID && sliceEnd != null
                && sliceEnd.length() == UUID_LENGTH) {
            outStream.writeUUIDBlob(UUID.fromString(sliceEnd));
        } else {
            outStream.writeString(sliceEnd);
        }
        outStream.writeInt(sliceCount);
        outStream.writeInt(sliceReversed);
        outStream.writeShortArray(attrType);
        outStream.writeBoolean(conversationViewReq);
        outStream.writeBoolean(includePopDeletedMsgs);
        outStream.flush();
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        // Read Attribute Descriptions Array
        int numAttrDesc = inStream.readInt();
        AttributeDescription[] attrDescriptions = new AttributeDescription[numAttrDesc];
        for (int i = 0; i < numAttrDesc; i++)
            attrDescriptions[i] = new AttributeDescription(inStream);

        numEntries = inStream.readInt();
        sortedMessages = new LinkedHashMap<Integer, MessageMetadata[]>(numEntries);
        for (int i = 0; i < numEntries; i++) {
            // Read Messages and its attributes.
            numConversations = inStream.readInt();
            messageMetadata = new MessageMetadata[numConversations];
            for (int j = 0; j < numConversations; j++) {
                messageMetadata[j] = new MessageMetadata(inStream,
                        attrDescriptions);
            }
            sortedMessages.put(i, messageMetadata);
        }
        // Read the Log Events
        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the sortedMessages
     */
    public LinkedHashMap<Integer, MessageMetadata[]> getSortedMessages() {
        return sortedMessages;
    }

}
