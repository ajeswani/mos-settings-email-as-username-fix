/*
 * Copyright (c) 2005 Openwave Systems Inc.  All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/intermail/IntermailPasswordProvider.java#1 $
 */

package com.opwvmsg.utils.paf.util.password.intermail;

import java.util.Map;

import com.opwvmsg.utils.paf.util.password.Password;
import com.opwvmsg.utils.paf.util.password.PasswordException;
import com.opwvmsg.utils.paf.util.password.spi.PasswordProvider;

/**
 * A provider for the password API that does the hashing locally.
 * <p>
 */
public class IntermailPasswordProvider implements PasswordProvider {

    /**
     * Gets a Password which implements the Intermail password types locally.
     * <p>
     * @param setup a map 
     * @return a Password
     * @throws PasswordException 
     */
    public Password getPassword(Map setup) throws PasswordException {
        return new IntermailPassword();
    }

    /**
     * Returns true when asked about the "intermail" provider type.
     * <p>
     * @param type the desired type of provider
     * @return true if the given type is "intermail", else false
     */
    public boolean isType(String type) {
        return type.equalsIgnoreCase("intermail");
    }
}
