/*
 * Copyright (c) 2013 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: 
 */

package com.opwvmsg.utils.paf.intermail.http;

import java.io.IOException;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;

/**
 * Implementation of HTTP Connection pool for RME.
 * 
 * @author mxos-dev
 */
public class HTTPConnectionPool implements IConnectionPool<HTTPConnection> {

    protected static Logger logger = Logger.getLogger(HTTPConnectionPool.class);

    protected GenericObjectPool<HTTPConnection> objPool;
    private String hostName = null;
    private int port;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public HTTPConnectionPool(final int capacity, final String host,
            final int port, final int timeout) {
        int maxConnectionPoolSize = capacity;
        this.hostName = host;
        this.port = port;
        objPool = new GenericObjectPool<HTTPConnection>(
                new HTTPConnectionFactory(host, port, timeout),
                maxConnectionPoolSize);
        objPool.setMaxIdle(-1);
        objPool.setMinIdle(-1);
        initializeJMXStats(maxConnectionPoolSize);
    }

    @Override
    public HTTPConnection borrowObject() throws IntermailException {
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            HTTPConnection obj = objPool.borrowObject();
            if (obj == null) {
                logger.error("Borrowed object is null.");
                throw new IntermailException(LogEvent.ERROR,
                        "Failed to borrow the HTTPConnection Object from Pool: ");
            }
            incrementJMXStats();
            return obj;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new IntermailException(LogEvent.ERROR,
                    "Failed to borrow the HTTPConnection Object from Pool: "
                            + e.getMessage());
        }
    }

    @Override
    public void returnObject(final HTTPConnection client)
            throws IntermailException {
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            client.cleanup();
        } catch (IllegalStateException e) {
            throw new IntermailException(LogEvent.ERROR,
                    "Response Object cleanup failed during HTTPConnection returnObject to Pool: "
                            + e.getMessage());
        } catch (IOException e) {
            throw new IntermailException(LogEvent.ERROR,
                    "Response Object cleanup failed during HTTPConnection returnObject to Pool: "
                            + e.getMessage());
        }
        try {
            objPool.returnObject((HTTPConnection) client);
            decrementJMXStats();
        } catch (Exception e) {
            logger.error("Error while returning the object.", e);
            throw new IntermailException(LogEvent.ERROR,
                    "Failed to return the HTTPConnection Object to Pool: "
                            + e.getMessage());
        }
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.HTTP.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_HTTP.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_HTTP.decrement();
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(final String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public GenericObjectPool<HTTPConnection> getObjPool() {
        return objPool;
    }

    public void setObjPool(GenericObjectPool<HTTPConnection> objPool) {
        this.objPool = objPool;
    }

}