/*
 * Copyright (c) 2004 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/filters/HttpLogFilter.java#1 $
 */
package com.opwvmsg.utils.paf.filters;

import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import com.opwvmsg.utils.paf.util.HeaderUtil;

/**
 * This filter logs HTTP request and response data.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class HttpLogFilter implements Filter {

    private static final Logger logger = Logger.getLogger(HttpLogFilter.class);

    private FilterConfig config;

    /**
     * Initializes.
     *
     * @param config the filter configuration
     */
    public void init(FilterConfig config) {
        this.config = config;
        logger.debug("Logging of HTTP traffic is enabled.");
    }

    /**
     * Cleans up when the filter is taken out of service.
     */
    public void destroy() {
        config = null;
    }

    /**
     * Returns the configuration.
     *
     * @return the filter configuration
     */
    public FilterConfig getFilterConfig() {
        return config;
    }

    /**
     * Filters the request.
     *
     * @param request the request
     * @param response the response
     * @param chain the filter chain
     *
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException {

        ServletResponse newResponse = response;
        LoggingResponse wrapper = null;
        if (logger.isDebugEnabled()) {
            wrapper = new LoggingResponse((HttpServletResponse) response);
            newResponse = wrapper;
        }

        chain.doFilter(request, newResponse);

        if (logger.isDebugEnabled()) {
            logData(request, wrapper);
        }
    }

    private void logData(ServletRequest req, LoggingResponse response)
            throws IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        StringBuffer data = new StringBuffer();
        data.append("Request/Response\n");
        data.append("=====================================================\n");
        data.append("connection from ");
        data.append(request.getRemoteAddr());
        data.append('\n');
        data.append(request.getMethod());
        data.append(' ');
        data.append(request.getRequestURL());
        String query = request.getQueryString();
        if (query != null) {
            data.append('?');
            data.append(query);
        }
        data.append('\n');
        data.append("--Headers--\n");
        Enumeration headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String header = (String) headers.nextElement();
            Enumeration values = request.getHeaders(header);
            while (values.hasMoreElements()) {
                data.append(header);
                data.append(": ");
                data.append(values.nextElement().toString());
                data.append('\n');
            }
        }
        data.append("--Parameters--\n");
        Enumeration params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String param = (String) params.nextElement();
            String[] values = request.getParameterValues(param);
            for (int i = 0; i < values.length; i++) {
                data.append(param);
                data.append(" = ");
                data.append(values[i]);
                data.append('\n');
            }
        }

        data.append("-----------------------------------------------------\n");
        data.append(response.getStatusLine());
        data.append('\n');
        String[] respHeaders = response.getHeaders();
        for (int i = 0; i < respHeaders.length; i++) {
            data.append(respHeaders[i]);
            data.append('\n');
        }
        String contentType = response.getContentType();
        data.append("Content-Type: ");
        data.append(contentType);
        data.append('\n');
        data.append("Content-Length: ");
        data.append(response.getContentLength());
        data.append("\n\n");
        if (contentType != null) {
            if (contentType.startsWith("text")) {
                data.append(response.getContent());
            } else {
                data.append("<binary content omitted>");
            }
        }
        data.append("\n=====================================================\n");
        logger.debug(data.toString());
    }

    private static class LoggingResponse extends HttpServletResponseWrapper {

        LoggingServletOutputStream stream;
        LoggingWriter writer;
        int length = -1;
        String type;
        List headers = new ArrayList();
        int status = -1;
        String message;
        String redirect;


        public LoggingResponse(HttpServletResponse response) {
            super(response);
        }


        public ServletOutputStream getOutputStream() throws IOException {
            ServletOutputStream stream = super.getOutputStream();
            this.stream = new LoggingServletOutputStream(stream);
            return this.stream;
        }

        public PrintWriter getWriter() throws IOException {
            PrintWriter writer = super.getWriter();
            this.writer = new LoggingWriter(writer);
            return this.writer;
        }

        public void setStatus(int code) {
            super.setStatus(code);
            this.status = code;
        }

        public void sendError(int code) throws IOException {
            super.sendError(code);
            this.status = code;
        }

        public void sendError(int code, String msg) throws IOException {
            super.sendError(code, msg);
            this.status = code;
            this.message = msg;
        }

        public void sendRedirect(String location) throws IOException {
            super.sendRedirect(location);
            this.redirect = location;
        }

        public String getStatusLine() {
            if (redirect != null) {
                return "Redirect to " + redirect;
            } else {
                StringBuffer buf = new StringBuffer();
                buf.append("Status: ");
                if (status >= 0) {
                    buf.append(status);
                } else {
                    buf.append("<unknown> (probably 200)");
                }
                if (message != null) {
                    buf.append(" ");
                    buf.append(message);
                }
                return buf.toString();
            }
        }

        public void addHeader(String name, String value) {
            super.addHeader(name, value);
            saveHeader(name, value);
        }

        public void addIntHeader(String name, int value) {
            super.addIntHeader(name, value);
            saveHeader(name, String.valueOf(value));
        }

        public void addDateHeader(String name, long value) {
            super.addDateHeader(name, value);
            String date = HeaderUtil.rfc1123Format.format(new Date(value));
            saveHeader(name, date);
        }

        public void setHeader(String name, String value) {
            super.setHeader(name, value);
            saveHeader(name, value);
        }

        public void setIntHeader(String name, int value) {
            super.setIntHeader(name, value);
            saveHeader(name, String.valueOf(value));
        }

        public void setDateHeader(String name, long value) {
            super.setDateHeader(name, value);
            String date = HeaderUtil.rfc1123Format.format(new Date(value));
            saveHeader(name, date);
        }

        public void addCookie(Cookie cookie) {
            super.addCookie(cookie);
            StringBuffer text = new StringBuffer();
            text.append(HeaderUtil.getCookieHeaderName(cookie));
            text.append('=');
            HeaderUtil.getCookieHeaderValue(cookie, text);
            saveHeader("Set-Cookie", text.toString());
        }

        public void setContentLength(int length) {
            super.setContentLength(length);
            this.length = length;
        }

        public int getContentLength() {
            if (length >= 0) {
                return length;
            } else {
                return getActualContentLength();
            }
        }

        public void setContentType(String type) {
            super.setContentType(type);
            this.type = type.trim();
        }

        public String getContentType() {
            return type;
        }

        public String getContent() throws IOException {
            if (writer != null) {
                return writer.getContent();
            }
            if (stream != null) {
                return stream.getContent(getCharacterEncoding());
            }
            return "";
        }

        public int getActualContentLength() {
            if (writer != null) {
                return writer.getContentLength();
            }
            if (stream != null) {
                return stream.getContentLength();
            }
            return -1;
        }

        private void saveHeader(String name, String value) {
            headers.add(name + ": " + value);
        }

        public String[] getHeaders() {
            String[] res = new String[headers.size()];
            return (String[]) headers.toArray(res);
        }
    }

    private static class LoggingServletOutputStream
            extends ServletOutputStream {

        private ServletOutputStream stream;

        private ByteArrayOutputStream content;


        public LoggingServletOutputStream(ServletOutputStream stream) {
            this.stream = stream;
            this.content = new ByteArrayOutputStream(8192);
        }

        public void write(int b) throws IOException {
            content.write(b);
            stream.write(b);
        }

        public String getContent(String enc)
                throws UnsupportedEncodingException {
            return content.toString(enc);
        }

        public int getContentLength() {
            return content.size();
        }
    }

    private static class LoggingWriter extends PrintWriter {

        private StringWriter content;


        public LoggingWriter(PrintWriter writer) {
            super(writer);
            this.content = new StringWriter(8192);
        }

        public void write(int c) {
            content.write(c);
            super.write(c);
        }

        public void write(char buf[], int off, int len) {
            content.write(buf, off, len);
            super.write(buf, off, len);
        }

        public void write(String s, int off, int len) {
            content.write(s, off, len);
            super.write(s, off, len);
        }

        public void write(String s) {
            content.write(s);
            super.write(s);
        }

        public String getContent() {
            return content.toString();
        }

        public int getContentLength() {
            return content.toString().length();
        }
    }
}
