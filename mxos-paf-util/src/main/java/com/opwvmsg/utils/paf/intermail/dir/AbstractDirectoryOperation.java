/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved. The copyright to
 * the computer software herein is the property of Openwave Systems, Inc. The
 * software may be used and/or copied only with the written permission of
 * Openwave Systems, Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:
 */
package com.opwvmsg.utils.paf.intermail.dir;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

public abstract class AbstractDirectoryOperation extends AbstractOperation {
    // RME ops currently implemented
    protected static final int DIR_P_GETUSERINFO = 1;
    protected static final int DIR_P_GETFWDINFO = 3;
    protected static final int DIR_P_GETALIASINFO = 15;

    // these are defined in the directory (and adjusted for the null terminator)
    protected static final int MAX_PASSWORD_LENGTH = 192;
    protected static final int MAX_USERNAME_LENGTH = 387;

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#connect(boolean)
     */
    protected void connect(boolean usePool) throws IntermailException {
        if (rmeSocket == null || !rmeSocket.isReady()) {
            rmeSocket = ConnectionManager.getRmeSock(usePool);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#releaseConnection()
     */
    protected void releaseConnection() throws IntermailException {
        if (rmeSocket != null) {
            ConnectionManager.putRmeSock(rmeSocket);
            rmeSocket = null;
        }
    }

    @Override
    protected void createHttpClient(boolean usePool, int port)
            throws IntermailException {
    }

    @Override
    protected void releaseHttpConnection() throws IntermailException {
        // TODO Auto-generated method stub
    }

}
