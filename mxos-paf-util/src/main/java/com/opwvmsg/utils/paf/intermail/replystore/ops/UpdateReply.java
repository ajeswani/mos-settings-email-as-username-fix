/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.replystore.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.replystore.Reply;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

public class UpdateReply extends AbstractMssOperation {

    // input
    private String mssId;
    private Reply reply;
    private int operation;

    // output
    // -- no output --

    // constants
    public static int SET_ENTRY = 0x01;
    public static int CLEAR_ENTRY = 0x02;

    // internal
    private String url;
    private String hostHeader;

    /**
     * For MSS_P_INITAUTOREPLY This operation will update a reply text. Note
     * that if this is the autoreply message (type 1) then we will assume that
     * the data should be saved in the charset specified by autoReplyCharset,
     * otherwise UTF-8 is used.
     * 
     * @param host the auto reply host
     * @param mssId the user's mssId
     * @param realm The mailRealm of the user
     * @param reply the Reply object to update. Since the only way to get a
     *            reply object is from ReadReply, this is assured of being an
     *            initialized reply field
     * @param rmeDataModel RME access pattern. CL or Leopard
     */
    public UpdateReply(String host, String mssId, String realm, Reply reply,
            final RmeDataModel rmeDataModel) {
        super(rmeDataModel);
        this.host = resolveClusterToMSS(host, mssId);
        this.mssId = mssId;
        this.reply = reply;
        this.hostHeader = getHostHeader(host, mssId, realm);
    }

    /**
     * For MSS_SL_OPERATEAUTOREPLY This operation will set or clear reply text.
     * Note that if this is the autoreply message (type 1) then we will assume
     * that the data should be saved in the charset specified by
     * autoReplyCharset, otherwise UTF-8 is used.
     * 
     * @param host the auto reply host
     * @param mssId the user's mssId
     * @param realm The mailRealm of the user
     * @param operation - can have the following values SET_ENTRY = 0x01
     *            CLEAR_ENTRY = 0x02
     * @param reply the Reply object to update. Since the only way to get a
     *            reply object is from ReadReply, this is assured of being an
     *            initialized reply field
     * @param rmeDataModel RME access pattern. CL or Leopard
     */
    public UpdateReply(String host, String mssId, String realm, int operation,
            Reply reply, final RmeDataModel rmeDataModel) {
        super(rmeDataModel);
        this.host = resolveClusterToMSS(host, mssId);
        this.mssId = mssId;
        this.operation = operation;
        this.reply = reply;
        this.hostHeader = getHostHeader(host, mssId, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || mssId == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (this.rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_INITAUTOREPLY);
        } else { // Leopard
            if ((operation < 1) || (operation > 2)) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "operation", "UpdateReply" }));
            }
            url = getMsUrl(host, mssId, MSS_SL_OPERATEAUTOREPLY);
            callRme(MSS_SL_OPERATEAUTOREPLY);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeStringArray(new String[] { mssId });
        reply.writeReply(outStream);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        int[] newVersions = inStream.readIntArray(); // versions
        if (newVersions != null && newVersions.length >= 1) {
            reply.setVersion(newVersions[0]);
        }

        logEvents = inStream.readLogEvent();
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_OPERATEAUTOREPLY));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);

        outStream.writeByte(operation);
        reply.writeSLReply(outStream);
        if (operation == SET_ENTRY) {
            outStream.writeInt(1); // migration
        } else {
            outStream.writeInt(2); // overrideLock
        }

        outStream.flush();
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        logEvents = inStream.readLogEvent();
        int version = inStream.readInt(); // version
        reply.setVersion(version);

    }

}