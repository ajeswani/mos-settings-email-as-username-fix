package com.opwvmsg.utils.paf.intermail.mail;

/**
 * Enum of the Attribute Ids of the Messages.
 * 
 * @author mOS-dev
 */
public enum AttributeId {
    attr_messageUUID((short) 0), attr_bounce((short) 1), attr_deliveredNDR(
            (short) 2), attr_private((short) 3), attr_hasAttachment((short) 4), attr_richMailFlag(
            (short) 5), attr_arrivalTime((short) 6), attr_expirationTime(
            (short) 7), attr_firstSeenTime((short) 8), attr_firstAccessedTime(
            (short) 9), attr_lastAccessedTime((short) 10), attr_size((short) 11), attr_uid(
            (short) 12), attr_msgFlags((short) 13), attr_keywords((short) 14), attr_type(
            (short) 15), attr_priority((short) 16), attr_multipleMsgs(
            (short) 17), attr_subject((short) 18), attr_from((short) 19), attr_to(
            (short) 20), attr_cc((short) 21), attr_date((short) 22), attr_references(
            (short) 23), attr_sender((short) 24), attr_bcc((short) 25), attr_replyTo(
            (short) 26), attr_inReplyTo((short) 27), attr_blobMessageId(
            (short) 28), attr_specialDeleted((short) 29), attr_folderUUID(
            (short) 30);

    private short type;

    private AttributeId(short typeStr) {
        this.type = typeStr;
    }

    public short valueOf() {
        return this.type;
    }

    public static AttributeId valueOf(final Short key) {
        for (final AttributeId id : AttributeId.values()) {
            if (id.valueOf() == key.shortValue()) {
                return id;
            }
        }
        return null;
    }
}