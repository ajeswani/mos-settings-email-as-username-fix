/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation will get the Imap UIDs and Message UUIDs of all the messages
 * in the folder.
 * 
 * @author mOS-Dev
 */
public class ImapListUids extends AbstractMssOperation {

    // input
    private String name;
    private UUID folderUUID;
    private boolean ignorePopDeleted;

    // output
    int[] imapUids; // IMAP UIDs
    UUID[] msgUUids; // Message UUIDs

    // internal
    private String url;
    private String hostHeader;

    /**
     * Constructor for ImapListUids
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID The folder from which the messages ids has to be
     *            retrieved
     * @param ignorePopDeleted False - list messages that are marked as special
     *            deleted. True - do not list the messages that are marked as
     *            special deleted.
     */
    public ImapListUids(String host, String name, String realm,
            UUID folderUUID, boolean ignorePopDeleted) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_IMAPLISTUIDS);
        this.folderUUID = folderUUID;
        this.ignorePopDeleted = ignorePopDeleted;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (folderUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "folderUUID", "ImapListUids" }));
        }

        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Rme.OpNotSupported",
                            new String[] { "MSS_SL_IMAPLISTUIDS", host,
                                    "dataModel=" + rmeDataModel }));
        } else {
            callRme(MSS_SL_IMAPLISTUIDS);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_IMAPLISTUIDS));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeUUID(folderUUID);
        outStream.writeBoolean(ignorePopDeleted);
        outStream.flush();
    }

    @Override
    protected void receiveHttpData() throws IOException {
        imapUids = inStream.readIntArray();// IMAP UIDs of all the messages in
                                           // the folder
        msgUUids = inStream.readUUIDArray(); // Message UUIDs of all the
                                             // messages in the folder
        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the imapUids
     */
    public int[] getImapUids() {
        return imapUids;
    }

    /**
     * @return the msgUUids
     */
    public UUID[] getMsgUUids() {
        return msgUUids;
    }
}
