/*
 *      Copyright (c) 2002-2006 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/Utilities.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.StringTokenizer;


/**
 * General purpose utility methods.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class Utilities {

    /**
     * WML escape sequence for $.
     */
    static private String WML_DOLLAR   = "$$";

    /**
     * HDML escape sequence for $.
     */
    static private String HDML_DOLLAR  = "&dol;";

    /**
     * Escape sequence for &.
     */
    static private String AMPERSAND    = "&amp;";

    /**
     * Escape sequence for <.
     */
    static private String LESS_THAN    = "&lt;";

    /**
     * Escape sequence for >.
     */
    static private String GREATER_THAN = "&gt;";

    /**
     * Escape sequence for ".
     */
    static private String QUOTE        = "&quot;";

    /**
     * Escape sequence for '.
     */
    static private String APOS         = "&#039;";

    /**
     * Escape sequence for non-breaking space.
     */
    static private String NBSP         = "&nbsp;";

    /**
     * Escape sequence for soft-hyphen.
     */
    static private String SOFT_HYPHEN  = "&shy;";


    /**
     * Convert a single character delimiter separated string of values to an
     * array of strings allowing empty tokens.
     * 
     * @param dsvString the DSV string
     * @param delimiter the delimiter
     * @return an array of Strings, zero-length if the input string was empty
     *
     * @throws IllegalArgumentException if the input string is null
     */
    public static String[] dsvToArray(String dsvString, char delimiter) {
        return dsvToArray(dsvString, delimiter, true);
    }

    /**
     * Convert a single character delimiter separated string of values to an
     * array of strings.
     * 
     * @param dsvString the DSV string
     * @param delimiter the delimiter
     * @param emptyTokensAllowed if true, empty tokens may be added to the list,
     *            if false, they will be ignored.
     * 
     * @return an array of Strings, zero-length if the input string was empty
     * 
     * @throws IllegalArgumentException if the input string is null
     */
    public static String[] dsvToArray(String dsvString, char[] delimiters,
                                      boolean emptyTokensAllowed) {
        if (dsvString == null) {
            throw new IllegalArgumentException("null string");
        }
        if (dsvString.length() == 0) {
            return new String[0];
        }
        List values = new ArrayList();
        char[] chars = dsvString.toCharArray();
        int start = 0;
        for (int i = 0; i < chars.length; i++) {
            for (int j = 0; j < delimiters.length; j++) {
                if (chars[i] == delimiters[j]) {
                    if (emptyTokensAllowed || (start != i)) {
                        values.add(dsvString.substring(start, i));
                    }
                    start = i + 1;
                }
            }
        }
        if (start < chars.length) {
            values.add(dsvString.substring(start));
        } else if (emptyTokensAllowed) {
            values.add("");
        }
        return (String[]) values.toArray(new String[values.size()]);
    }

    /**
     * Convert a single character delimiter separated string of values to an
     * array of strings.
     * 
     * @param dsvString the DSV string
     * @param delimiter the delimiter
     * @param emptyTokensAllowed if true, empty tokens may be added to the list,
     *            if false, they will be ignored.
     * @param quoteChars Anything surrounded by these characters will not be
     *            considered a delimeter
     * 
     * @return an array of Strings, zero-length if the input string was empty
     * 
     * @throws IllegalArgumentException if the input string is null
     */
    public static String[] dsvToArray(String dsvString, char[] delimiters,
                                      boolean emptyTokensAllowed, char []quoteChars) {
        if (dsvString == null) {
            throw new IllegalArgumentException("null string");
        }
        if (dsvString.length() == 0) {
            return new String[0];        
        }
        if (quoteChars == null) {
            quoteChars = new char[0];
        }
        
        int quoteCount = 0;
        boolean[] inQuoteState = new boolean[quoteChars.length];
        
        List values = new ArrayList();
        char[] chars = dsvString.toCharArray();
        int start = 0;
        for (int i = 0; i < chars.length; i++) {
            // adjust quote count
            boolean quoteFound = false;
            for (int q = 0; q < quoteChars.length; q++) {
                if (chars[i] == quoteChars[q]) {
                    quoteFound = true;
                    if (inQuoteState[q] == true) {
                        inQuoteState[q] = false;
                        quoteCount--;
                    } else {
                        inQuoteState[q] = true;
                        quoteCount++;                        
                    }
                    break;
                }
                
            }
            if (quoteFound) {
                continue;
            } else if (quoteCount == 0) {
                for (int j = 0; j < delimiters.length; j++) {
                    if (chars[i] == delimiters[j]) {
                        if (emptyTokensAllowed || (start != i)) {
                            values.add(dsvString.substring(start, i));
                        }
                        start = i + 1;
                    }
                }
            }
        }
        if (start < chars.length) {
            values.add(dsvString.substring(start));
        } else if (emptyTokensAllowed) {
            values.add("");
        }
        return (String[]) values.toArray(new String[values.size()]);
    }

    /**
     * Convert a single character delimiter separated string of values to an
     * array of strings.
     * 
     * @param dsvString the DSV string
     * @param delimiter the delimiter
     * @param emptyTokensAllowed if true, empty tokens may be added to the list,
     *            if false, they will be ignored.
     * 
     * @return an array of Strings, zero-length if the input string was empty
     * 
     * @throws IllegalArgumentException if the input string is null
     */
    public static String[] dsvToArray(String dsvString, char delimiter,
                                      boolean emptyTokensAllowed) {
        if (dsvString == null) {
            throw new IllegalArgumentException("null string");
        }
        if (dsvString.length() == 0) {
            return new String[0];
        }
        List values = new ArrayList();
        char[] chars = dsvString.toCharArray();
        int start = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == delimiter) {
                if (emptyTokensAllowed || (start != i)) {
                    values.add(dsvString.substring(start, i));
                }
                start = i + 1;
            }
        }
        if (start < chars.length) {
            values.add(dsvString.substring(start));
        } else if (emptyTokensAllowed) {
            values.add("");
        }
        return (String[]) values.toArray(new String[values.size()]);
    }

    /**
     * Convert a string delimiter separated string of values 
     * to an array of strings.
     *
     * @param dsvString the DSV string 
     * @param delimiter the delimiter.  If zero-length, the
     *    output array will have length one and contain the 
     *    entire input string.
     *
     * @return an array of Strings, zero-length if the input string
     *     was empty
     *
     * @throws IllegalArgumentException if the input string is null
     */
    public static String[] dsvToArray(String dsvString, String delimiter) {
        if (dsvString == null) {
            throw new IllegalArgumentException("null string");
        }
        if (dsvString.length() == 0) {
            return new String[0];
        }
        if (delimiter == null) {
            throw new IllegalArgumentException("null delimiter");
        }
        if (delimiter.length() == 0) {
            return new String[]{dsvString};
        }
        List values = new ArrayList();
        int start = 0;
        int skip = delimiter.length();
        while (start < dsvString.length()) {
            int i = dsvString.indexOf(delimiter, start);
            if (i >= 0) {
                values.add(dsvString.substring(start, i));
                start = i + skip;
            } else {
                break;
            }
        }
        if (start < dsvString.length()) {
            values.add(dsvString.substring(start));
        } else {
            values.add("");
        }
        return (String[]) values.toArray(new String[values.size()]);
    }

    /**
     * Convert a escapable-delimiter separated string of values to an array of
     * Strings.
     *
     * @param text the DSV string 
     * @param delimiter the delimiter
     * @param escapeChar if this character precedes the delimiter, it will be
     *             treated as an ordinary character and the escape character
     *             will not be included in the return value.  If the escape
     *             char precedes any other character, or comes at the end of
     *             the string, it will be treated as an ordinary character.
     *             If this character appears twice in a row, it will be treated
     *             as a single ordinary character.
     *
     * @return an array of Strings, zero-length if the input string
     *     was empty
     *
     * @throws IllegalArgumentException if the input string is null
     */
    public static String[] dsvToArray(String text, char delimiter,
                                      char escapeChar) {
        if (text == null) {
            throw new IllegalArgumentException("null string");
        }
        if (text.length() == 0) {
            return new String[0];
        }

        char[] chars = text.toCharArray();
        List values = new ArrayList();
        StringBuffer buff = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (ch == delimiter) {
                values.add(buff.toString());
                buff.setLength(0);
            } else if (ch == escapeChar && i < chars.length - 1 &&
                       (chars[i + 1] == delimiter ||
                        chars[i + 1] == escapeChar)) {
                buff.append(chars[i + 1]);
                i++;
            } else {
                buff.append(ch);
            }
        }
        values.add(buff.toString());

        return (String[]) values.toArray(new String[values.size()]);
    }

    /**
     * Convert an array of strings to a single character delimiter 
     * separated string of values.
     *
     * @param values the strings to be combined
     * @param delimiter the delimiter
     *
     * @return a DSV String, zero-length if the input array was empty
     *
     * @throws IllegalArgumentException if the input array was null
     */
    public static String arrayToDsv(String[] values, char delimiter) {
        return arrayToDsv(values, String.valueOf(delimiter));
    }

    /**
     * Convert an array of strings to a string delimiter separated 
     * string of values.
     *
     * @param values the strings to be combined
     * @param delimiter the delimiter
     *
     * @return a DSV String, zero-length if the input array was empty
     *
     * @throws IllegalArgumentException if the input array was null
     */
    public static String arrayToDsv(String[] values, String delimiter) {
        if (values == null) {
            throw new IllegalArgumentException("empty string array");
        }
        if (values.length == 0) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < values.length; i++) {
            if (i > 0) {
                buf.append(delimiter);
            }
            buf.append(values[i]);
        }
        return buf.toString();
    }

    /**
     * Convert an array of strings into an escaped-delimiter separated string
     * of values.
     *
     * @param values the strings to be combined
     * @param delimiter the delimiter
     * @param escapeChar every occurrence of the delimiter character or this 
     *             character will be preceded by this character.
     *
     * @return an array of Strings, zero-length if the input string
     *     was empty
     *
     * @throws IllegalArgumentException if the input string is null
     */
    public static String arrayToDsv(String[] values, char delimiter,
                                    char escapeChar) {
        
        if (values == null) {
            throw new IllegalArgumentException("empty string array");
        }
        if (values.length == 0) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < values.length; i++) {
            if (i > 0) {
                buf.append(delimiter);
            }
            for (int j = 0; j < values[i].length(); j++) {
                char ch = values[i].charAt(j);
                if (ch == delimiter || ch == escapeChar) {
                    buf.append(escapeChar);
                }
                buf.append(ch);
            }
        }
        return buf.toString();
    }

    /**
     * Convert a CSV string to an array of strings.
     *
     * @param csvString the CSV string 
     *
     * @return an array of Strings, zero-length if the input string
     *     was empty
     *
     * @throws IllegalArgumentException if the input string is null
     */
    public static String[] csvToArray(String csvString) {
        return dsvToArray(csvString, ',');
    }

    /**
     * Convert an array of strings to a CSV string.
     *
     * @param values the strings to be combined
     *
     * @return a CSV String, zero-length if the input array was empty
     *
     * @throws IllegalArgumentException if the input array was null
     */
    public static String arrayToCsv(String[] values) {
        return arrayToDsv(values, ',');
    }

    /**
     * Convert a locale specifier such as "en_US" to an instance
     * of a Locale.
     *
     * @param localeSpec the "_" delimited specifier String to convert
     *
     * @return the Locale
     *
     * @throws IllegalArgumentException if the input string is null or empty
     */
    public static Locale getLocale(String localeSpec) {
        return getLocale(localeSpec, "_");
    }

    /**
     * Convert a locale specifier such as "en-US" to an instance of a
     * Locale.
     *
     * @param localeSpec the specifier String to convert
     * @param delimiter the delimiter set used in the specifier
     *         (for example, "_", "-", or "_-")
     *
     * @return the Locale
     *
     * @throws IllegalArgumentException if the input string is null or empty
     */
    public static Locale getLocale(String localeSpec, String delimiter) {
        if (localeSpec == null || localeSpec.length() < 1) {
            throw new IllegalArgumentException("null or empty locale spec");
        }
        String language = "";
        String country = "";
        String variant = null;
        StringTokenizer tokenizer = new StringTokenizer(localeSpec, delimiter);
        if (tokenizer.hasMoreTokens()) {
            language = tokenizer.nextToken();
            if (tokenizer.hasMoreTokens()) {
                country = tokenizer.nextToken();
                if (tokenizer.hasMoreTokens()) {
                    variant = localeSpec.substring(
                            language.length() + 1 + country.length() + 1);
                }
            }
        }
        if (variant != null) {
            return new Locale(language, country, variant);
        } else {
            return new Locale(language, country);
        }
    }

    /**
     * Return a deep copy of the given Properties instance.  In other
     * words, extract all the entries including defaults from the
     * given Properties instance and add them to the Properties
     * instance to be returned.
     *
     * @return a copy of the given properties
     */
    public static Properties getProperties(Properties input) {
        Properties output = new Properties();
        if (input != null) {
            for (Enumeration e = input.propertyNames(); e.hasMoreElements(); ) {
                String name = (String)e.nextElement();
                output.setProperty(name, input.getProperty(name));
            }
        }
        return output;
    }

    /**
     * Replace unsafe characters in a string with their XML escape
     * sequences.  The characters &amp; &lt; &gt; &quot; &apos;
     * are replaced with their entity equivalents.  If HDML 
     * escaping is called for, then the dollar sign ($) is also escaped.  If
     * WML escaping is being done, non-breaking spaces and
     * soft hyphens are escaped in addition to all HDML and XML
     * escapes.
     *
     * @param text the input text
     * @param type the type of escaping to perform
     *
     * @return the escaped text
     */
    public static String escapeXml(String text, EscapeType type) {
        char[] chars = text.toCharArray();
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            switch (chars[i]) {
            case '&':
                result.append(AMPERSAND);
                break;
            case '<':
                result.append(LESS_THAN);
                break;
            case '>':
                result.append(GREATER_THAN);
                break;
            case '\"':
                result.append(QUOTE);
                break;
            case '\'':
                result.append(APOS);
                break;
            case '$':
                if (type == EscapeType.WML) {
                    result.append(WML_DOLLAR);
                } else if (type == EscapeType.HDML) {
                    result.append(HDML_DOLLAR);
                } else {
                    result.append(chars[i]);
                }
                break;
           case 173:
               if (type == EscapeType.WML) {
                    result.append(SOFT_HYPHEN);
               } else {
                    result.append(chars[i]);
               }
               break;
           case 160:
               if (type == EscapeType.WML || type == EscapeType.HDML) {
                    result.append(NBSP);
               } else {
                    result.append(chars[i]);
               }
               break;
            default:
                if (type == EscapeType.XML) {
                    char c = chars[i];
                    // <http://www.w3.org/TR/REC-xml/#NT-Char>
                    // Legal characters are tab, carriage return, line feed,
                    // and the legal characters of Unicode and ISO/IEC 10646.
                    // [2] Char ::= #x9 | #xA | #xD | [#x20-#xD7FF] |
                    //              [#xE000-#xFFFD] | [#x10000-#x10FFFF]                    
                    if (c == '\t' || c == '\n' || c == '\r' ||
                            ('\u0020' <= c && c <= '\uD7FF') ||
                            ('\uE000' <= c && c <= '\uFFFD')) {
                        result.append(chars[i]);
                    } else {
                        result.append('?');
                    }
                } else {
                    result.append(chars[i]);
                }
                break;
            }
        }
        return result.toString();
    }

    private static final String hex[] = { "0", "1", "2", "3", "4", "5", "6",
            "7", "8", "9", "a", "b", "c", "d", "e", "f" };

    /**
     * Transcode the given input hex string into a byte array.
     *
     * @param in a hex string
     * @return a byte array containing the transcoded hex from the given input
     */
    public static byte[] hexStringToByteArray(String in) {
        if (in == null) {
            return null;
        }
        int numBytes = in.length() / 2;
        byte[] out = new byte[numBytes];
        char[] oneByte = new char[2];
        for (int i = 0; i < numBytes; i++) {
            oneByte[0] = in.charAt(i * 2);
            oneByte[1] = in.charAt((i * 2) + 1);
            out[i] = (byte)(Integer.parseInt(new String(oneByte), 16));
        }
        return out;
    }

    /**
     * Converts a byte array into a string of hex characters representing the
     * value of each byte.
     * 
     * @param in
     * @return String of hex characters
     */
    public static String byteArrayToHexString(byte in[]) {
        byte byteValue = 0x00;
        int i = 0;
        if (in == null || in.length <= 0) {
            return null;
        }

        StringBuffer out = new StringBuffer(in.length * 2);

        while (i < in.length) {
            byteValue = (byte)(((in[i] & 0xF0) >>> 4) & 0x0F);
            out.append(hex[byteValue]);
            byteValue = (byte)(in[i] & 0x0F);
            out.append(hex[(int)byteValue]);
            i++;
        }
        return out.toString();
    }

    /**
     * Converts an array of strings into a single string
     *
     * @param values the strings to be combined
     * @return an array of Strings, zero-length if the input string
     *     was empty
     * @throws IllegalArgumentException if the input string is null
     */
    public static String arrayToString(String[] values) {
        if (values == null) {
            throw new IllegalArgumentException("empty string array");
        }
        if (values.length == 0) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < values.length; i++) {
            buf.append(values[i]);
        }
        return buf.toString();
    }

    /**
     * No instances.
     */
    private Utilities() {
    }
}
