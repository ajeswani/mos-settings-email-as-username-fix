/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/log/IntermailException.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.log;

import javax.mail.MessagingException;

import org.apache.log4j.Level;

/**
 * An Exception class that encapsulates an RME Log Event. This Exception is used
 * throughout the RME code for reporting errors.
 */
public class IntermailException extends MessagingException implements RmeException {

    private Level log4jLevel;
    private String formattedString;
    private LogEvent[] errors = null;

    /**
     * Constructor that takes a detail message (the formatted log string), and a
     * log severity that can be retrieved later as a log4j Level
     * 
     * @param severity intermail log severity for this exception
     * @param formattedString the formatted log message used as the detail
     *            message of this exception
     */
    public IntermailException(int severity, String formattedString) {
        super(formattedString);
        this.formattedString = formattedString;
        log4jLevel = LogEvent.getLog4jLevel(severity);
    }

    /**
     * Constructor that takes a detail message (the formatted log string), and a
     * log severity that can be retrieved later as a log4j Level.
     * In addition, the server side errors that caused this exception
     * are included.  Generally this constructor should be used for all
     * server (intermail) side errors that should generate an exception, 
     * while the other constructor is used for client (JavaRME) errors (i.e. 
     * parameter validation errors, etc..)
     * 
     * @param severity intermail log severity for this exception
     * @param formattedString the formatted log message used as the detail
     *            message of this exception
     */
    public IntermailException(int severity, String formattedString,
                              LogEvent[] serverErrors) {
        super(formattedString);
        this.formattedString = formattedString;
        log4jLevel = LogEvent.getLog4jLevel(severity);
        errors = serverErrors;
    }

    /**
     * Returns the formatted String text of the error message
     * 
     * @return the formatted error message
     */
    public String getFormattedString() {
        return formattedString;
    }
    
    /* (non-Javadoc)
     * @see com.openwave.intermail.log.RmeException#getLog4jLevel()
     */
    public Level getLog4jLevel() {
        return log4jLevel;
    }

    /* (non-Javadoc)
     * @see com.openwave.intermail.log.RmeException#hasLogEvents()
     */
    public boolean hasLogEvents() {
        return (errors != null && errors.length > 0);
    }
    
    /* (non-Javadoc)
     * @see com.openwave.intermail.log.RmeException#getLogEvents()
     */
    public LogEvent[] getLogEvents() {
        return errors;
    }

}