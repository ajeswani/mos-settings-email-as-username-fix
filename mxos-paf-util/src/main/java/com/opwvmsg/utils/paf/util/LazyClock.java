/*
 * Copyright (c) 2000-2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/LazyClock.java#1 $
 */

package com.opwvmsg.utils.paf.util;

/**
 * A lazy clock that is only accurate to within half a second.
 * <p>
 * This class was originally part of net.mobility.util
 *
 * @author Forrest Girouard
 */
public class LazyClock implements Clock {

    /**
     * Current time in seconds.
     */
    private static int seconds;

    private static LazyClock clock = new LazyClock();

    /**
     * Keep the time accurate to within a second.
     *
     * Create and start a thread that updates the cached time value
     * twice a second.
     */
    private LazyClock() {
        update();
        Thread updater = new Thread("LazyClock") {
            public void run() {
                while (!Thread.interrupted()) {
                    try {
                        Thread.sleep(500);
                        update();
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        };
        updater.setDaemon(true);
        updater.start();
    }

    /**
     * Updates the current time.
     */
    private synchronized void update() {
        seconds = (int)(System.currentTimeMillis() / 1000L);
    }

    /**
     * Get the current time in milliseconds.
     *
     * @return long time in milliseconds
     */
    public long currentTimeMillis() {
        return (long)(seconds * 1000L);
    }

    public static LazyClock getInstance() {
        return clock;
    }

} // end class LazyClock
