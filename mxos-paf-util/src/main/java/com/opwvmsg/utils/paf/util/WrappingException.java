/*
 *      Copyright (c) 2002-2004 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/WrappingException.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * This exception class allows exceptions to wrap another exception inside this
 * one.  This class is not necessary in Java 1.4 and should eventually be
 * deprecated.
 *
 * @deprecated extend Exception instead
 */
public class WrappingException extends Exception {

    /**
     * The wrapped Throwable
     *
     * @see #getCause
     */
    private Throwable cause = null;

    /**
     * Construct a new exception with no message
     */
    public WrappingException() {
        super();
    }

    /**
     * Construct a new exception with a message
     * @param message the exception message string.
     */
    public WrappingException(String message) {
        super(message);
    }

    /**
     * Construct an exception out of another exception
     * @param cause the wrapped exception
     */
    public WrappingException(Throwable cause) {
        super();
        initCause(cause);
    }

    /**
     * Construct an exception out of another exception with additional
     * information from the string
     *
     * @param message the exception message string
     * @param cause the wrapped exception
     */
    public WrappingException(String message, Throwable cause) {
        super(message);
        initCause(cause);
    }

    /**
     * Get the exception that caused this exception.
     * 
     * @return the cause of the exception or <code>null</code>
     */
    public Throwable getCause() {
        return cause;
    }

    /**
     * Sets the wrapped exception.
     *
     * @param cause the cause of the exception.
     * @throws IllegalArgumentException if <code>cause</code> is this
     * @throws IllegalStateException if a cause is already set
     */
    public Throwable initCause(Throwable cause) {
        if (cause == this) {
            throw new IllegalArgumentException("An Exception can't be its own cause");
        }
        if (this.cause != null) {
            throw new IllegalStateException("This Exception already has a cause");
        }
        this.cause = cause;
        return this;
    }

    /**
     * Returns the error message, including the message from the wrapped
     * exception if there is one.
     */
    public String getMessage() {
        if (cause == null) {
            return super.getMessage();
        } else {
            String superMessage = super.getMessage();
            StringBuffer message = new StringBuffer();
            if (superMessage != null) {
                message.append(superMessage);
                if (superMessage.endsWith(".")) {
                    message.append(" ");
                } else {
                    message.append(". ");
                }
            }
            message.append("Cause of the exception is ");
            message.append(cause.getMessage());
            return message.toString();
        }
    }
    
    /**
     * Prints the message and stack trace to System.err
     */
    public void printStackTrace() {
        printStackTrace(System.err);
    }

    /**
     * Prints the message and stack trace to the specified PrintStream
     *
     * @param ps          the print stream
     */
    public void printStackTrace(PrintStream ps) {
        if (cause == null) {
            super.printStackTrace(ps);
        } else {
            String superString = super.toString();
            String fullString = superString
                + (superString.endsWith(".") ? "" : ".")
                + " Cause of the exception is ";
            synchronized(ps) {
                ps.println(fullString);
                cause.printStackTrace(ps);
            }
        }
    }

    /**
     * Prints the message and stack trace to the specified PrintWriter
     *
     * @param pw          the print writer
     */
    public void printStackTrace(PrintWriter pw) {
        if (cause == null) {
            super.printStackTrace(pw);
        } else {
            String superString = super.toString();
            String fullString = superString
                + (superString.endsWith(".") ? "" : ".")
                + " Cause of the exception is ";
            synchronized(pw) {
                pw.println(fullString);
                cause.printStackTrace(pw);
            }
        }
    }

}
