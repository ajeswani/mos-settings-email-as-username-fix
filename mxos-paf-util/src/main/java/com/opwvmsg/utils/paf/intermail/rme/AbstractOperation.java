/*
 * H+ Copyright 2004 Software.com, Inc. All Rights Reserved. The copyright to
 * the computer software herein is the property of Software.com, Inc. The
 * software may be used and/or copied only with the written permission of
 * Software.com, Inc. or in accordance with the terms and conditions stipulated
 * in the agreement/contract under which the software has been supplied. $Id:
 * //paf
 * /mainline/util/src/main/java/com/openwave/intermail/rme/AbstractOperation
 * .java#2 $ H-
 */
package com.opwvmsg.utils.paf.intermail.rme;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ByteArrayEntity;

import com.opwvmsg.utils.paf.intermail.http.HTTPConnection;
import com.opwvmsg.utils.paf.intermail.http.HTTPConnectionManager;
import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;

/**
 * This class handles all of the details of the RME transaction. It is generic
 * to all operations, and defines server specific and operation specific methods
 * that will be implemented in the respective subclasses.
 */
public abstract class AbstractOperation {
    private static final Logger logger = Logger
            .getLogger(AbstractOperation.class);

    // RME Data bytes

    // The first byte in each remote operation.
    public final static byte RME_OP = 0x23; // what follows is operation
    public final static byte RME_CLOSE = 0x24; // terminate connection
    public final static byte RME_SYNC_REQ = 0x25; // request RMESYNCDATA
    public final static byte RME_SYNC_DATA = 0x26; // Noop for synchronization
    public final static byte RME_RESET_SOCK = 0x27; // Reset an RMESock to the
    // state
    // Synchronization bytes.
    public final static byte RME_SYNC_1 = (byte) 0x83;
    public final static byte RME_SYNC_7 = (byte) 0x84;

    // The RME Version of MSS
    public final static String MSS_RME_VERSION = "mssRmeVersion";
    // some choice RME rev's
    // older RME versions are not listed, since
    // this api will require at least RME version 145
    // to support the connection-less APIs. anything less
    // will return failure before you start.
    public final static int RME_CL_VER = 145;
    public final static int RME_EXPIRATION_TIME_MMS_VER = 146;
    public final static int RME_CL_OPERATIONS_OPTIONS_VER = 147;
    public final static int RME_INVENTORY_CONTROL_VER = 148;
    public final static int RME_DIRSERV_DUMP_VER = 149;
    public final static int RME_MS_DESC_MAILBOX_VERSIONED_VER = 150;
    public final static int RME_CREATE_MS_WITH_OPTIONS_VER = 151;
    public final static int RME_FLOOD_VER = 152;
    public final static int RME_IMCL_READ_BIN_MSG_VER = 153;
    public final static int RME_IMCL_READ_PARTIAL_MSG_VER = 154;
    public final static int RME_MAILBOX_AGING_VER = 155;
    public final static int RME_UNBIND_FOLDER_REPLY_VER = 156;
    public final static int RME_GETUSERINFO_WITH_DOMAIN_VER = 157;
    public final static int RME_READFOLDER_WINDOW_VER = 158;
    // This RME version is the latest as of 8/11/2005
    public final static int RME_PRESERVE_AUTO_REPLY_VER = 159;
    // ======================================================================//
    // Support for the Spool Based Queue Server
    // As we are not supporting the Spool Based Queue Server for the near
    // future, we are bumping this number to very high. So this gives us
    // the possibility of future RME upgrades.
    // the 160 was OMSpoolBasedQueueServer but it was bumped up to 999.
    public final static int RME_SPOOL_BASED_QUEUE_SERVER = 999;
    // Support for the Indexing Server
    public final static int RME_INDEXING_SERVER_VER = 161;
    // Support 4000-byte mailFolderQuota in MsClassOfService object.
    public final static int RME_4K_FOLDER_QUOTA_VER = 162;
    // Mx 8.0 - support large quotas, header summaries, and folder sort orders.
    public final static int RME_MX8_VER = 163;
    // Mx 8.1.2 - Support Remote Call Tracin
    public final static int RME_REMOTE_CALL_TRACING = 164;
    // Mx 9.0 - SL MSS, RME over HTTP
    public final static int RME_STATELESS = 175;

    public final static int RME_MIN_VER = RME_CL_VER;
    
    // These are our rme versions. If you want to raise these, you'll
    // have to make sure that *all* operations conform to the
    // updated rme protocol (which means it will have to support all
    // rme versions in between as well)
    // RME has two protocol types, i.e., regular RME and HTTP RME.
    // These two types have different protocol versions.
    public static int RME_CURRENT_VERSION = RME_MX8_VER;
    public static int RME_HTTP_CURRENT_VERSION = RME_STATELESS;
    
    // RME protocol types
    public enum RmeProtocolType {
        /**
         * <tt>REGULAR</tt> : Regular conventional RME that is stateful.
         */
        REGULAR,
        
        /**
         * <tt>HTTP</tt> : HttpRME that is stateless.
         * @since Email Mx 9.0
         */
        HTTP
    };
    protected RmeProtocolType rmeProtocolType = RmeProtocolType.REGULAR;
    
    // other consts - this is the rme auth, not the user auth
    public final static int RME_NOAUTH = 0;
    public final static int RME_UNIXA = 1;
    public final static int RME_UNIXB = 2;

    public final static int RME_OMIT_SENDING_CLIENT_INDEX = -3;
    public final static int RME_SEND_DEFAULT_CLIENT_INDEX = -1;

    public final static int RME_DEFAULT_CLIENT_INDEX = 0;

    public final static String HTTP_HOST_KEY = "Host";
    public final static String HTTP_CONTENT_TYPE_KEY = "Content-Type";
    public final static String HTTP_RMECLASS_KEY = "x-rme-class";
    public final static String HTTP_RMEOPERATION_KEY = "x-rme-operation";
    public final static String HTTP_RMEVERSION_KEY = "x-rme-version";

    // our main link to the outside world
    protected RmeSocket rmeSocket = null;
    protected RmeInputStream inStream = null;
    protected RmeOutputStream outStream = null;
    protected HTTPConnection httpClient = null;
    protected HttpPost postMethod = null;

    // most RME operations have a reply, any that dont should set this to false
    protected boolean hasReply = true;

    // log events can be created anywhere in the chain
    protected LogEvent[] logEvents = new LogEvent[0];

    /**
     * Get the array of LogEvents for this operation
     * 
     * @return array of LogEvents
     */
    public LogEvent[] getLogEvents() {
        return logEvents;
    }

    /**
     * Get the first LogEvent (if any)
     * 
     * @return first LogEvent in the array, if exists, or null.
     */
    public LogEvent getLogEvent() {
        if (logEvents.length > 0) {
            return logEvents[0];
        }
        return null;
    }

    /**
     * Get the number of LogEvents currently added to this operation
     * 
     * @return number of LogEvents.
     */
    public int getNumLogEvents() {
        return logEvents.length;
    }

    /**
     * This is a generic method to receive data from a server for all operation
     * ids. Generally most operations only expect one reply, so override the
     * simpler method that takes no arguments. If an operation could have two
     * replies, then it would override this method and switch based on the id
     * passed in to determine which reply it was receiving.
     * 
     * @param id - the id of the server's RME reply.
     * @throws IOException - on any socket error.
     */
    protected void receiveData(int id) throws IOException {
        // by default the rmeOps don't care about the id,
        // but if they do, they can override this method
        receiveData();
    }

    /**
     * Defined by operation specific subclasses, this method gets the operation
     * specfic reply data off of the RmeInputStream
     * 
     * @throws IOException on any error
     */
    protected abstract void receiveData() throws IOException;

    /**
     * Defined by operation specific subclasses, this method gets the operation
     * specfic reply data off of the RmeInputStream for HTTP RME
     * Implementations.
     * 
     * @throws IOException on any error
     */
    protected abstract void receiveHttpData() throws IOException;

    /**
     * Defined by operation specific subclasses, this method sends the operation
     * specfic request data to the RmeOutputStream
     * 
     * @throws IOException on any error
     */
    protected abstract void sendData() throws IOException;

    /**
     * Defined by operation specific subclasses for HTTP RME Implementation,
     * this method sends the operation specfic request data to the
     * RmeOutputStream
     * 
     * @throws IOException on any error
     */
    protected abstract void constructHttpData() throws IOException;

    /**
     * This abstract method is expected to initialize and open the connection to
     * the server, and set the rmeSocket variable. This method is intented to be
     * overridden by a server specific subclass.
     * 
     * @param usePool if true, will use a pool connection if available,
     *            otherwise will open a new connection
     * @throws IntermailException - if there is any error opening the socket or
     *             in the subsequent handshake.
     */
    protected abstract void connect(boolean usePool) throws IntermailException;

    /**
     * This abstract method is expected to initialize and open the connection to
     * the HTTP server, and set the httpClient variable. This method is intented
     * to be overridden by a server specific subclass.
     * 
     * @param usePool if true, will use a pool connection if available,
     *            otherwise will open a new connection
     * @throws IntermailException - if there is any error opening the socket or
     *             in the subsequent handshake.
     */
    protected abstract void createHttpClient(boolean usePool, int port)
            throws IntermailException;

    /**
     * This abstract method handles a connection when the operation is finished.
     * It is intented to either close the connection or place it back into a
     * pool. This method is overridden by server specific subclasses.
     * 
     * @throws IntermailException - if the is an error closing the socket.
     */
    protected abstract void releaseConnection() throws IntermailException;

    /**
     * This abstract method handles a connection when the operation is finished.
     * It is intented to either close the HTTP connection or place it back into
     * a HTTP Connection pool. This method is overridden by server specific
     * subclasses.
     * 
     * @throws IntermailException - if the is an error closing the socket.
     */
    protected abstract void releaseHttpConnection() throws IntermailException;

    /**
     * This abstract method is intended to be overriden by each individual
     * operation subclass. This is the public entry point to start the rme
     * operation. This method will check the arguments, and kick off the
     * client/server communication.
     * 
     * @throws IntermailException - on any error, either from the RME itself, or
     *             returned from the server's reply.
     */
    public abstract void execute() throws IntermailException;

    /**
     * This method actually does the work of the rme transaction. It is called
     * from the <code>execute</code> method after the arguments have been
     * checked. This particular method only takes an operation id, and assumes a
     * stateless, connection-less, RME operation
     * 
     * @param id - the ID of the operation to send.
     * @throws IntermailException - on any error.
     */
    protected void callRme(int id) throws IntermailException {
        callRme(id, RME_OMIT_SENDING_CLIENT_INDEX, false, true);
    }

    /**
     * This method actually does the work of the rme transaction. It is called
     * from the <code>execute</code> method after the arguments have been
     * checked.
     * 
     * @param id - the ID of the RME operation
     * @param clientIndex - the index of the server side client we're talking to
     * @param saveConnection - if true, the connection will not be released
     * @throws IntermailException - on any error.
     */
    protected void callRme(int id, int clientIndex, boolean saveConnection)
            throws IntermailException {
        if (rmeProtocolType == RmeProtocolType.HTTP) {
            callHttpRme();
        } else {
            callRme(id, clientIndex, saveConnection, true);
        }
    }

    protected void callRme(int id, int clientIndex, boolean saveConnection,
            boolean tryPool) throws IntermailException {

        LogEvent errorEvent = null;

        // clear any old log events;
        logEvents = new LogEvent[0];

        // this will set rmeSocket or die trying.
        connect(tryPool);

        String hostName = rmeSocket.getHost();
        int port = rmeSocket.getPort();

        try {
            inStream = (RmeInputStream) rmeSocket.getInputStream();
            outStream = (RmeOutputStream) rmeSocket.getOutputStream();
            beginOp(outStream, id, clientIndex);

            // operation specific data.
            sendData();
            endOp(outStream);

            // process reply
            if (hasReply) {
                processReply(clientIndex != RME_OMIT_SENDING_CLIENT_INDEX);
            }
            // right now This is optimized for cnxn less operations
            // any cnxn oriented ops must release the connection when
            // they're done.
            rmeSocket.setUsed();
            if (!saveConnection) {
                releaseConnection();
            }

            // if log events, throw exception
            if (getNumLogEvents() > 0) {
                // throw the first one.. additional ones are available upon
                // request!
                for (int i = 0; i < getNumLogEvents(); i++) {
                    if (logEvents[i] != null) {
                        throw logEvents[i].getIntermailException();
                    }
                }
                // I doubt we'll ever get to this case, but if so,
                // we need to report *something*
                throw new IntermailException(LogEvent.ERROR,
                        "No Exception Detail Found.");

            }
        } catch (SocketTimeoutException ste) {

            errorEvent = new LogEvent(LogEvent.URGENT, "Rme.ClientTimeout",
                    new String[] { hostName + ":" + port });
        } catch (IOException e) {
            // catch bum socket here and punt back to failover

            // attempt to get a new connection. If that fails,
            // the sock pool will automatically fail over, and
            // if it can't find any connection, then we fail.

            // only try again if this was an old connection from
            // the pool.. if this was a new connection, then fail
            if (tryPool && !rmeSocket.isNew()) {
                // close the socket permanently
                try {
                    rmeSocket.setReadyToClose();
                    releaseConnection();
                } catch (IntermailException ignore) {
                }
                callRme(id, clientIndex, saveConnection, false);
            } else {
                errorEvent = new LogEvent(LogEvent.URGENT,
                        "Rme.ServerClosedSocket", new String[] { hostName + ":"
                                + port });
            }

        } finally {
            if (errorEvent != null) {
                try {
                    // close the socket permanently
                    rmeSocket.setReadyToClose();
                    releaseConnection();
                } catch (IntermailException ioe) {
                    // ignore error trying to close
                }
                throw errorEvent.getIntermailException();
            }
        }
    }

    /**
     * This method is used for debugging RME bugs when decoding a server's
     * reply. All of the bytes from the socket will be dumped to stdout for
     * analysis. This method will usually block, since the server does not close
     * the connection.
     * 
     * @throws IOException - on error
     */
    protected void debugReceive() throws IOException {
        // pull the bytes off the socket, and print
        int b = 0;
        while ((b = inStream.read()) != -1) {
            String s = Integer.toHexString(b);
            if (s.length() > 1) {
                System.out.print(Integer.toHexString(b) + " ");
            } else {
                System.out.print("0" + Integer.toHexString(b) + " ");
            }
        }
    }

    /**
     * This method handles the server's reply.
     * 
     * @param getClientIndex - true if this is an RME operation, and a client
     *            index will be sent, false if this is SRME
     * @throws IOException - on any socket error.
     */
    private void processReply(boolean getClientIndex) throws IOException,
            IntermailException {
        // read the index, but we don't care since only one
        // client can have the wire at a time
        int ctrl = inStream.read();
        if (getClientIndex) {
            // we don't need this value, but need to read it
            // off the stream.
            inStream.readInt();
        }

        switch (ctrl) {
        case RME_OP:
            int id = inStream.readInt();
            receiveData(id);

            ctrl = inStream.read();
            if (ctrl != RME_SYNC_DATA) {
                handleRmeProtError();
            }
            // fall through
        case RME_SYNC_DATA:
            if (!readTrailer(inStream)) {
                handleRmeProtError();
            }
            break;
        case RME_CLOSE:
            throw new IOException(
                    "Server requested that this socket be closed.");
        case RME_SYNC_REQ:
            // report rme prot error, but don't close
            // really I don't think this is ever used anymore
            String hostName = rmeSocket.getHost();
            int port = rmeSocket.getPort();
            logger.log(Level.ERROR, LogEvent.formatLogString(
                    "Rme.ProtocolBadResponse", new String[] { hostName + ":"
                            + port }));
            endOp(outStream);
            break;
        case RME_RESET_SOCK:
            // noop for this client
            break;
        default:
            handleRmeProtError();
        }
    }

    /**
     * Writes the appropriate header bytes to begin an RME operation. This can
     * be used for either RME or SRME operations
     * 
     * @param id the request's operation id
     * @param clientIndex the client side index id Set this to
     *            AbstractOperation.RME_OMIT_SENDING_CLIENT_INDEX for SRME ops
     *            Set this to AbstractOperation.RME_SEND_DEFAULT_CLIENT_INDEX to
     *            use the default client
     * @throws IOException on a socket or connection error
     */
    public static void beginOp(RmeOutputStream outStream, int id,
            int clientIndex) throws IOException {
        outStream.write(RME_OP);
        if (clientIndex != AbstractOperation.RME_OMIT_SENDING_CLIENT_INDEX) {
            outStream.writeInt(clientIndex);
        }
        outStream.writeInt(id);
    }

    /**
     * Writes the appropriate sync bytes to end an RME request
     * 
     * @throws IOException on any IO error
     */
    public static void endOp(RmeOutputStream outStream) throws IOException {
        outStream.write(RME_SYNC_DATA);
        for (int i = 6; i > 0; i--)
            outStream.write(RME_SYNC_1);
        outStream.write(RME_SYNC_7);
        outStream.flush();
    }

    /**
     * Reads the RME trailer from the data stream. This trailer is a specific
     * set of bytes that ends each rme response.
     * 
     * @return true if trailer received OK false if trailer bytes are incorrect
     * @throws IOException on any socket related error
     */
    protected static boolean readTrailer(RmeInputStream inStream)
            throws IOException {
        int b = 0;
        for (int i = 0; i < 6; i++) {
            b = inStream.read();
            if ((byte) b != RME_SYNC_1) {
                return false;
            }
        }
        b = inStream.read();
        if ((byte) b != RME_SYNC_7) {
            return false;
        }
        return true;
    }

    /**
     * Handles a protocol error by closing the socket and raising a properly
     * filled in IntermailException
     * 
     * @throws IntermailException always
     */
    private void handleRmeProtError() throws IntermailException {
        String hostName = rmeSocket.getHost();
        int port = rmeSocket.getPort();
        try {
            rmeSocket.setReadyToClose();
            releaseConnection();
            // fall through to exception at end
        } catch (IntermailException ioe) {
        }
        throw new IntermailException(LogEvent.ERROR, LogEvent.formatLogString(
                "Rme.ProtocolBadResponse", new String[] { "hostName: "
                        + hostName + " port: " + port }));
    }

    /**
     * Make a HTTP Call over RME. If the MSS is down connects, executes the
     * failover scenarios
     * 
     * @throws IntermailException
     */
    protected void callHttpRme() throws IntermailException {
        HttpResponse response = null;
        String hostName = null;

        logEvents = new LogEvent[0];
        postMethod = new HttpPost();

        // Construct the OutputStream and construct the HTTPPost Header
        ByteArrayOutputStream os = null;
        BufferedOutputStream bufOutStream = null;
        os = new ByteArrayOutputStream();
        bufOutStream = new BufferedOutputStream(os);
        outStream = new RmeOutputStream(bufOutStream);
        outStream.setLittleEndian(true);
        outStream.setRmeVer(RME_HTTP_CURRENT_VERSION);

        try {
            constructHttpData();
        } catch (IOException e) {
            throw new IntermailException(LogEvent.ERROR,
                    "Error during creation of HTTP Output Stream"
                            + e.getMessage());
        }
        postMethod.setEntity(new ByteArrayEntity(os.toByteArray()));
        
        if (logger.isDebugEnabled())
            HTTPConnectionManager.printPoolUsageInfo();

        createHttpClient(true, 0);

        if (logger.isDebugEnabled())
            HTTPConnectionManager.printPoolUsageInfo();

        int startPort = httpClient.getPort();
        hostName = httpClient.getHost();
        int port = startPort;
        boolean ioExceptionOccured = false;

        do {
            ioExceptionOccured = false;
            try {
                postMethod.setURI(URI.create("http://" + httpClient.getHost()
                        + ":" + httpClient.getPort()));
                response = httpClient.executePost(postMethod);
            } catch (ClientProtocolException e) {
                if (httpClient != null) {
                    releaseHttpConnection();
                }
                throw new IntermailException(LogEvent.URGENT,
                        LogEvent.formatLogString("Rme.ProtoBadDataFmt",
                                new String[] { e.getMessage(), Integer.toString(port),
                                        "Bad Data format sent to MSS" }));
            } catch (IOException e) {
                if (httpClient != null) {
                    releaseHttpConnection();
                }
                ioExceptionOccured = true;
                logger.log(Level.ERROR, LogEvent.formatLogString(
                        "Nio.ConnServerFail",
                        new String[] { e.getMessage(), Integer.toString(port),
                                "Failing over to next mss port." }));
            }
            if (ioExceptionOccured == true) {
                createHttpClient(true, port);
                port = httpClient.getPort();
            }
        } while ((response == null) && (port != startPort));

        if (response == null) {
            if (httpClient != null) {
                releaseHttpConnection();
            }
            throw new IntermailException(LogEvent.URGENT,
                    LogEvent.formatLogString("Nio.ConnServerFail",
                            new String[] { hostName }));
        }

        int returnCode = response.getStatusLine().getStatusCode();
        if (returnCode == HttpStatus.SC_OK) {
            try {
                inStream = new RmeInputStream(
                        response.getEntity().getContent(), (int) response
                                .getEntity().getContentLength());
                inStream.setLittleEndian(true);
                receiveHttpData();
            } catch (IOException e) {
                throw new IntermailException(LogEvent.ERROR,
                        "Rme.ServerClosedSocket");
            } catch (Exception e) {
                throw new IntermailException(LogEvent.ERROR,
                        "Rme.ServerClosedSocket");
            } finally {
                if (httpClient != null) {
                    releaseHttpConnection();
                }
            }
            // if log events, throw exception
            if (getNumLogEvents() > 0) {
                for (int i = 0; i < getNumLogEvents(); i++) {
                    if (logEvents[i] != null) {
                        throw logEvents[i].getIntermailException();
                    }
                }
                throw new IntermailException(LogEvent.ERROR,
                        "No Exception Detail Found.");
            }

        } else {
            if (httpClient != null) {
                releaseHttpConnection();
            }
            throw new IntermailException(LogEvent.URGENT,
                    LogEvent.formatLogString("Rme.ProtoBadDataFmt",
                            new String[] { "Bad Data format sent to MSS." }));
        }
    }
}
