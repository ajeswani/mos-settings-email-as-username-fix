/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/extsrv/ImxArray.java#1 $
 *  H- */
package com.opwvmsg.utils.paf.intermail.extsrv;

import java.io.IOException;
import java.util.ArrayList;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class represends an array of ImxData objects. It is one of the RME data
 * types used by extension services.
 */
public class ImxArray extends ArrayList {

    /**
     * Constructs an ImxArray of the given size
     * 
     * @param size the size of the array.
     */
    public ImxArray(int size) {
        super(size);
    }

    /**
     * Constructs an ImxArray from an RmeInputStream
     * 
     * @param inStream the stream from which the structure is read
     * @throws IOException on any IO error from the stream.
     */
    public ImxArray(RmeInputStream inStream) throws IOException {
        super();
        int arraySize = inStream.readInt();
        ensureCapacity(arraySize);
        for (int i = 0; i < arraySize; i++) {
            ImxData data = new ImxData(inStream);
            add(data);
        }
    }

    /**
     * Overrides the ArrayList add method to ensure that only ImxData objects
     * are added.
     * 
     * @see java.util.Collection#add(java.lang.Object)
     */
    public boolean add(Object value) {
        if (value instanceof ImxData) {
            super.add(value);
            return true;
        }
        throw new ClassCastException();
    }

    /**
     * Add an int value to this Imx Array
     * 
     * @param value integer value
     * @return if this collection changed as a result of the call
     */
    public boolean add(int value) {
        return (super.add(new ImxData(value)));
    }

    /**
     * Add a boolean value to this Imx Array
     * 
     * @param value boolean value
     * @return if this collection changed as a result of the call
     */
    public boolean add(boolean value) {
        return (super.add(new ImxData(value)));
    }

    /**
     * Add a byte array value to this Imx Array
     * 
     * @param value byte array
     * @return if this collection changed as a result of the call
     */
    public boolean add(byte[] value) {
        return (super.add(new ImxData(value)));
    }

    /**
     * Add a String to this Imx Array Note that this string will be transmitted
     * as UTF-8 If this is not desirable, use the byte[] constructor instead.
     * 
     * @param value String value
     * @return if this collection changed as a result of the call
     */
    public boolean add(String value) {
        return (super.add(new ImxData(value)));
    }

    /**
     * Add an ImxArray value to this Imx Array
     * 
     * @param value an ImxArray to add
     * @return if this collection changed as a result of the call
     */
    public boolean add(ImxArray value) {
        return (super.add(new ImxData(value)));
    }

    /**
     * Add an ImxStruct to this Imx Array
     * 
     * @param value ImxStruct to add
     * @return if this collection changed as a result of the call
     */
    public boolean add(ImxStruct value) {
        return (super.add(new ImxData(value)));
    }

    /**
     * Get the ImxData object at the index as a boolean
     * 
     * @param index The element number to retrieve
     * @return boolean value
     * @throws ClassCastException if ImxData element is not a boolean
     */
    public boolean getBoolean(int index) {
        return ((ImxData) (super.get(index))).getBoolean();
    }

    /**
     * Get the ImxData object at the index as a boolean
     * 
     * @param index The element number to retrieve
     * @return boolean value
     * @throws ClassCastException if ImxData element is not a boolean
     */
    public int getInt(int index) {
        return ((ImxData) (super.get(index))).getInt();
    }

    /**
     * Get the ImxData object at the index as an integer
     * 
     * @param index The element number to retrieve
     * @return integer value
     * @throws ClassCastException if ImxData element is not an integer
     */
    public ImxStruct getStruct(int index) {
        return ((ImxData) (super.get(index))).getStruct();
    }

    /**
     * Get the ImxData object at the index as an ImxArray
     * 
     * @param index The element number to retrieve
     * @return ImxArray value
     * @throws ClassCastException if ImxData element is not an ImxArray
     */
    public ImxArray getArray(int index) {
        return ((ImxData) (super.get(index))).getArray();
    }

    /**
     * Get the ImxData object at the index as a byte array
     * 
     * @param index The element number to retrieve
     * @return byte array containing the ImxString
     * @throws ClassCastException if ImxData element is not an ImxString
     */
    public byte[] getStringBytes(int index) {
        return ((ImxData) (super.get(index))).getImxString();
    }

    /**
     * Get the ImxData object at the index as a Utf8 String
     * 
     * @param index The element number to retrieve
     * @return String value (ImxString encoded as Utf8)
     * @throws ClassCastException if ImxData element is not an ImxString
     */
    public String getUtf8String(int index) {
        return ((ImxData) (super.get(index))).getUtf8String();
    }

    /**
     * Write this ImxArray to the RME output stream
     * 
     * @param outStream the RME output stream
     * @throws IOException on any IO error
     */
    public void writeImxArray(RmeOutputStream outStream) throws IOException {
        int arraySize = size();
        outStream.writeInt(arraySize);
        for (int i = 0; i < arraySize; i++) {
            ImxData data = (ImxData) get(i);
            data.writeImxData(outStream);
        }
    }

}