/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will delete Mailbox store and autoreply for the user.
 * 
 * @author mOS-dev
 */
public class DeleteMailbox extends AbstractMssOperation {

    // input
    private String name;
    private boolean keepAutoReply;
    private boolean suppressMers;
    private boolean disableBlobHardDelete;

    // output
    MsRMId mssId; // MSS RM Id.

    // internal
    private String url;
    private String hostHeader;

    /**
     * Constructor for DeleteMailbox
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param suppressMers if true, suppress MERS events for this operation.
     * @param keepAutoReply if true, autoreply should not be deleted from MSS.
     * @param disableBlobHardDelete if true, disables blob hard delete.
     * @param rmeDataModel backend MSS data model.  CL or Leopard
     * 
     */
    public DeleteMailbox(String host, String name, String realm, boolean suppressMers,
            boolean keepAutoReply, boolean disableBlobHardDelete, final RmeDataModel rmeDataModel) {
        super(rmeDataModel);
        this.name = name;
        this.host = resolveClusterToMSS(host, name);
        this.suppressMers = suppressMers;
        this.keepAutoReply = keepAutoReply;
        this.disableBlobHardDelete = disableBlobHardDelete;
        if (rmeDataModel == RmeDataModel.Leopard) {
            url = getMsUrl(host, name, MSS_SL_DELETEMS);
            this.hostHeader = getHostHeader(host, name, realm);
        } else {
            url = getMsUrl(host, name, MSS_P_DELETEMS);
        }

    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        // check args
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_DELETEMS, true);
            rmeSocket.setReadyToClose();
            releaseConnection();
        } else {
            callRme(MSS_SL_DELETEMS);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // client Id
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        // ms url
        outStream.writeString(url);
        outStream.writeEmptyCos();
        outStream.writeBoolean(suppressMers);
        if (outStream
                .rmeProtAtLeast(AbstractOperation.RME_PRESERVE_AUTO_REPLY_VER)) {
            outStream.writeBoolean(keepAutoReply);
        } else {
            outStream.writeBoolean(false);
        }
        if (outStream.rmeProtAtLeast(AbstractOperation.RME_MX8_VER)) {
            outStream.writeBoolean(disableBlobHardDelete);
        } else {
            outStream.writeBoolean(false);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Need to read additional bytes from inStream
        inStream.readInt(); // We ignore this
        logEvents = inStream.readLogEvent();
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveHttpData() throws IOException {
        //debugReceive();
        logEvents = inStream.readLogEvent();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.opwvmsg.utils.paf.intermail.rme.AbstractOperation#constructHttpData()
     */
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_DELETEMS));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        outStream.writeString(url); // ms url
        outStream.writeEmptyCos(); // Empty Cos

        outStream.writeBoolean(keepAutoReply);
        outStream.writeBoolean(disableBlobHardDelete);
        outStream.flush();
    }

    /**
     * @return the mssId
     */
    public MsRMId getMssId() {
        return mssId;
    }

}
