/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * System, Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id:
 */
package com.opwvmsg.utils.paf.intermail.config;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.intermail.ConfigDict;
import com.opwvmsg.utils.paf.config.intermail.IntermailConfig;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;
import com.opwvmsg.utils.paf.intermail.rme.AbstractRequestHandler;

/**
 * This class handles the requests and updates from the config server.
 */
public class ConfigChangeListener extends AbstractRequestHandler {

    private static final Logger logger = Logger.getLogger(AbstractConfigOperation.class);

    /**
     * Initialize the request handler thread.
     * 
     * @param rmesock a connected and initialized socket that is ready to accept
     *            requests from the config server
     */
    public ConfigChangeListener(RmeSocket rmesock) {
        super("ConfigChangeListener", rmesock);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractRequestHandler#processRequest(int)
     */
    public void processRequest(int id) throws IOException, IntermailException,
            UnsupportedOperationException {

        switch (id) {
        case (AbstractConfigOperation.LOCKSERV_NOTIFY):
            handleLockServ();
            break;
        case (AbstractConfigOperation.UPDATE_NOTIFY):
            handleConfigUpdate();
            break;
        default:
            // There are no other known ops currently that the config server
            // will send, so this case shouldn't happen unless new conifg
            // features are added in the future.
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Handles the Lock Server request from the config server. This is mainly a
     * notification that another server is thinking about changing the config.db
     * This same operation is used to send revert notifications, too.
     * 
     * @throws IOException on any io error from the socket
     * @throws IntermailException on any rme logic error
     */
    protected void handleLockServ() throws IOException, IntermailException {
        String host = inStream.readString();
        int pid = inStream.readInt();
        String disposition = inStream.readString();
        if (logger.isInfoEnabled()) {
            logger.info("Got Config Server lock notification: Host=>"
                + host + " PID=>" + pid + " Disposition=>" + disposition);
        }
        IntermailConfig.setLockStatus (host, pid, disposition);
        finishRequest();
        // no reply needed.
    }

    /**
     * Handles a config update request from the config server. This request may
     * be just a request for impacts, or it may be a request to install a new
     * set of keys. When installing, the list of keys may be just a delta, or it
     * may be an entirely new config.db
     * 
     * @throws IOException on any socket error
     * @throws IntermailException on any rme logic error
     */
    protected void handleConfigUpdate() throws IOException, IntermailException {
        boolean assess = inStream.readBoolean();
        ConfigDict rmeDict = new ConfigDict(inStream);
        boolean incremental = inStream.readBoolean();

        Map impacts = null;
        String defaultHost = null;
        String defaultApp = null;
        if (assess) {
            // get impacts
            defaultHost = Config.getDefaultHost();
            defaultApp = Config.getDefaultApp();
            impacts = rmeDict.assessImpacts();
        } else {
            // if we're not assessing, we're installing.
            rmeDict.updateIntermailConfig(
                (IntermailConfig)Config.getInstance(), incremental);
        }
        finishRequest();

        AbstractOperation.beginOp(outStream,
            AbstractConfigOperation.IMPACT_REPLY,
            AbstractOperation.RME_OMIT_SENDING_CLIENT_INDEX);
        
        if (impacts != null && impacts.size() > 0) {
            outStream.writeInt(impacts.size());
            Set s = impacts.keySet();
            Iterator i = s.iterator();
            while (i.hasNext()) {
                String key = (String)i.next();
                int impactInt = ((Integer)impacts.get(key)).intValue();
                Impact impact = new Impact(key, defaultHost, defaultApp, null, 0,
                        0, impactInt);
                impact.writeImpactPtr(outStream);
            }
        } else {
            outStream.writeInt(0);
        }
        AbstractOperation.endOp(outStream);

    }

}
