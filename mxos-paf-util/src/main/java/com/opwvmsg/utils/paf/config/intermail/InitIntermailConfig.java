/*
 * Copyright (c) 2005 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/intermail/InitIntermailConfig.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config.intermail;

import java.util.Map;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.config.NoSuchProviderException;

/**
 * This servlet uses <code>init</code> to set up an intermail config provider
 * on application startup. On initiliazation, you can specify 5 parameters:
 * intermailDir, defaultHost, defaultApp, confServerhost, confServerPost If you
 * specify the intermailDir, then a local config.db will be read, and the values
 * will be pulled from there. If not, then the confServHost/Port will be used to
 * contact the config server, and a complete config.db will be downloaded.
 */
public class InitIntermailConfig extends HttpServlet {

    private static final Logger logger = Logger.getLogger(InitIntermailConfig.class);

    /**
     * Set up an IntermailConfig object.
     */
    public void init() throws ServletException {

        String intermailDir = getInitParameter(IntermailConfig.INTERMAIL);
        String defaultHost = getInitParameter(IntermailConfig.DEFAULT_HOST);
        String defaultApp = getInitParameter(IntermailConfig.DEFAULT_APP);
        String anacapaHost = getInitParameter(IntermailConfig.ANACAPA_HOST);
        String anacapaPort = getInitParameter(IntermailConfig.ANACAPA_PORT);
        String confHost = getInitParameter(IntermailConfig.CONF_HOST);
        String confPort = getInitParameter(IntermailConfig.CONF_PORT);
        String retryInterval = getInitParameter(IntermailConfig.RETRY_INTERVAL);
        String forceConfigDb = getInitParameter(IntermailConfig.FORCE_CONFIG_DB);
        String msgcatProperties = getInitParameter(IntermailConfig.MSGCAT_PROPERTIES);
        String localConfig = getInitParameter(IntermailConfig.LOCAL_CONFIG);

        if (logger.isDebugEnabled()) {
            logger.debug("Initializing Intermail config at: " + intermailDir
                    + " with defaults " + defaultHost + "/" + defaultApp);
        }
        Map setup = new HashMap();
        setup.put(IntermailConfig.INTERMAIL, intermailDir);
        setup.put(IntermailConfig.DEFAULT_HOST, defaultHost);
        setup.put(IntermailConfig.DEFAULT_APP, defaultApp);
        setup.put(IntermailConfig.ANACAPA_HOST, anacapaHost);
        setup.put(IntermailConfig.ANACAPA_PORT, anacapaPort);
        setup.put(IntermailConfig.CONF_HOST, confHost);
        setup.put(IntermailConfig.CONF_PORT, confPort);
        setup.put(IntermailConfig.RETRY_INTERVAL, retryInterval);
        setup.put(IntermailConfig.FORCE_CONFIG_DB, forceConfigDb);
        setup.put(IntermailConfig.LOCAL_CONFIG, localConfig);
        setup.put(IntermailConfig.MSGCAT_PROPERTIES, msgcatProperties);
        try {
            Config.setConfig(IntermailConfig.INTERMAIL_PROVIDER, setup);
            if (logger.isDebugEnabled()) {
                try {
                    Config config = Config.getInstance();
                } catch (ConfigException e) {
                    logger.debug("Config.getInstance()", e);
                }
            }
        } catch (NoSuchProviderException e) {
            throw new ServletException(e);
        }
    }
}
