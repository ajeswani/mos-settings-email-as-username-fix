/*
 * Copyright (c) 2001-2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/Clock.java#1 $
 */

package com.opwvmsg.utils.paf.util;

/**
 * A clock interface.
 * <p>
 * This class was originally part of net.mobility.util
 *
 * @author Forrest Girouard
 */
public interface Clock {

    /**
     * Get the current time in milliseconds.
     *
     * @return long time in milliseconds
     */
    public long currentTimeMillis();

} // end interface Clock
