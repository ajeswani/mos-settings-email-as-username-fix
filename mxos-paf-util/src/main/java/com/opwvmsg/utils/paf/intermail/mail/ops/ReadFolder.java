/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/ReadFolder.java#1 $
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.Folder;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation will read the basic data for an Intermail folder
 */
public class ReadFolder extends AbstractMssOperation {

    // input
    private String name;
    private String pathname;
    private int accessId;
    private int options;
    private int sort;
    private String matchStart;
    private String matchEnd;
    private boolean excludeMatch = true;
    private int matchRangePreFetch = 0;
    private int matchRangePostFetch = 0;
    private boolean matchIsAbsoluteOffset = true;

    // output
    private Folder folder;

    // internal
    private String[] pathnameArray;
    private String url;

    // public constants
    public static final int FOLDER_SORT_NATURAL = 0;
    public static final int FOLDER_SORT_ARRIVAL_ASCENDING = 1;
    public static final int FOLDER_SORT_ARRIVAL_DESCENDING = 2;
    public static final int FOLDER_SORT_SIZE_ASCENDING = 3;
    public static final int FOLDER_SORT_SIZE_DESCENDING = 4;

    public final static int READFOLDER_SHOW_PRIVATE = 0x01;
    public final static int READFOLDER_RECURSIVE = 0x02;
    public final static int READFOLDER_NOTSEEN = 0x04;
    public final static int READFOLDER_RECENT = 0x08;
    public final static int READFOLDER_SUPPRESSMERS = 0x10;
    public final static int READFOLDER_NOMESSAGEIDS = 0x20;
    public final static int READFOLDER_NOMESSAGEDETAILS = 0x40;
    public final static int READFOLDER_CLEAR_RECENT = 0x80;
    public final static int READFOLDER_SIMPLE_INVENTORY = 0x100;

    /**
     * Constructor for ReadFolder
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder to read
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param sort the sort method to use. One of <br>
     *            FOLDER_SORT_NATURAL <br>
     *            FOLDER_SORT_ARRIVAL_ASCENDING <br>
     *            FOLDER_SORT_ARRIVAL_DESCENDING <br>
     *            FOLDER_SORT_SIZE_ASCENDING <br>
     *            FOLDER_SORT_SIZE_DESCENDING <br>
     * @param options options for this operation. possible bits are: <br>
     *            READ_FOLDER_SHOW_PRIVATE - If private message handling is
     *            enabled, this must be set to true to retrieve private
     *            messages. <br>
     *            READ_FOLDER_RECURSIVE - Read subfolders of this folder, too <br>
     *            READ_FOLDER_NOTSEEN - Only retrieve unread message <br>
     *            READ_FOLDER_RECENT - Only retrieve recent messages. <br>
     *            READ_FOLDER_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     * @param matchStart window start parameter (based on current sort)
     * @param matchEnd window end parameter
     * @param matchRangePreFetch number of messages to fetch before matchSsart
     * @param matchRnagePostFetch number of message to fetch after matchEnd
     * @param excludeMatch exclude the exact matches in results
     * @param matchIsAbsoluteOffset if true, the match is an absolute offset if
     *            false, the match is a parameter of the current sort field
     */
    public ReadFolder(String host, String name, String pathname, int accessId,
            int sort, int options, String matchStart, String matchEnd,
            int matchRangePreFetch, int matchRangePostFetch,
            boolean excludeMatch, boolean matchIsAbsoluteOffset) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_CL_READFOLDER);
        this.pathname = pathname;
        this.sort = sort;
        pathnameArray = buildPathnameArray(pathname);
        this.accessId = accessId;
        this.options = options;
        this.matchStart = matchStart;
        this.matchEnd = matchEnd;
        this.matchRangePreFetch = matchRangePreFetch;
        this.matchRangePostFetch = matchRangePostFetch;
        this.excludeMatch = excludeMatch;
        this.matchIsAbsoluteOffset = matchIsAbsoluteOffset;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        // check args
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (pathname == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "pathname", "ReadFolder" }));
        }
        if (sort < FOLDER_SORT_NATURAL || sort > FOLDER_SORT_SIZE_DESCENDING) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "sortOrder", "ReadFolder" }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_CL_READFOLDER);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_P_CL_READFOLDER", host,
                            "dataModel=" + rmeDataModel }));
        }
    }

    /**
     * Gets the folder object returned from the MSS
     * 
     * @return folder
     */
    public Folder getFolder() {
        return folder;
    }

    // RME FUNCTIONS

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(pathnameArray); // Folder name
        outStream.writeInt(sort);
        outStream.writeInt(accessId); // end user or admin
        outStream.writeInt(options);
        if (outStream.getRmeVer() >= AbstractOperation.RME_READFOLDER_WINDOW_VER) {
            outStream.writeString(matchStart);
            outStream.writeString(matchEnd);
            outStream.writeInt(matchRangePreFetch);
            outStream.writeInt(matchRangePostFetch);
            outStream.writeBoolean(excludeMatch);
            outStream.writeBoolean(matchIsAbsoluteOffset);
        }
        if (outStream.getRmeVer() >= AbstractOperation.RME_MX8_VER) {
            // Currently not supported.
            outStream.writeString(""); // sortHeader
            outStream.writeString(""); // sorLocale
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        boolean readMsgIds = ((options & READFOLDER_NOMESSAGEIDS) == 0);
        folder = new Folder(inStream, readMsgIds);

        if (inStream.getRmeVer() >= AbstractOperation.RME_READFOLDER_WINDOW_VER) {
            folder.setMsgIdsOffset(inStream.readInt());
        }

        logEvents = inStream.readLogEvent();
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation is not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation is not supported
    }

}
