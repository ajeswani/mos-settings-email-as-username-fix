/*  
 *      Copyright 2005 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: 
 */
package com.opwvmsg.utils.paf.config.intermail;

import com.opwvmsg.utils.paf.util.Utilities;

/**
 * A specific config entry.  
 */
public class ConfigEntry {

    String key; // this is the key name as it appears in the file
    // this is mainly kept so that when we rewrite the file, we preserve capitalization.
    String[] values;

    /**
     * A config entry holds the values and original key name
     * for each key.  The ConfigDict stores the key name in 
     * lowercase for case insensitive lookups, so we need the 
     * original key name in order to write the config.db
     * @param keyIn original key name from config.db
     * @param valuesIn string [] of values
     */
    protected ConfigEntry(String keyIn, String [] valuesIn) {
        key = keyIn;
        values = valuesIn;
    }
        
        
    /**
     * This key is the same as above, but it takes a String
     * instead of a String [].  Any newlines in the string
     * mull be broken into multiple lines in the entry.
     * @param keyIn
     * @param valueIn
     */
    protected ConfigEntry(String keyIn, String valueIn) {
        key = keyIn;
        values = Utilities.dsvToArray(valueIn, '\n', true);        
    }
    
    /**
     * Gets the original key name for this entry
     * @return the key name as it appeared in config.db
     */
    String getKey() {
        return key;
    }

    /**
     * gets the values associated with this key
     * @return the string array of values
     */
    String[] getValues() {
        return values;
    }
    
}
