/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;

/**
 * This class encapsulates the data returned from an RME PopList object.
 */
public class PopListInfo {
    private UUID[] msgUUids; // Message UUID
    private int[] msgSizes; // Message Size
    String[] oldMsgUUIDs; // old migrated Msg IDs

    /**
     * Default Constructor. Reads the input Stream and constructs the
     * PopListInfo.
     * 
     * @param inStream
     * @throws IOException
     */
    public PopListInfo(RmeInputStream inStream) throws IOException {

        // Message UUID List of all the messages in the folder
        msgUUids = inStream.readUUIDArray();

        // Message size List of all the messages in the folder
        msgSizes = inStream.readIntArray();

    }

    /**
     * @return the msgUUids
     */
    public UUID[] getMsgUUids() {
        return msgUUids;
    }

    /**
     * @return the msgSizes
     */
    public int[] getMsgSizes() {
        return msgSizes;
    }

    /**
     * @return the oldMsgUUIDs
     */
    public String[] getOldMsgUUIDs() {
        return oldMsgUUIDs;
    }

    /**
     * setOldMsgUUIDs
     * 
     * @param inStream
     * @throws IOException
     */
    public void setOldMsgUUIDs(RmeInputStream inStream) throws IOException {
        this.oldMsgUUIDs = inStream.readByteStringArray();
    }

    /**
     * for debugging - output all of the PopListInfo parameters
     * 
     * @param out a PrintStream to dump the data.
     */
    public void dump() {
        System.out.println("DUMPING PopListInfo ");
        if (this.getMsgUUids() != null) {
            for (int i = 0; i < this.getMsgUUids().length; i++) {
                System.out.println("msgUUID: "
                        + this.getMsgUUids()[i].toString());
                System.out.println("msgSize: " + this.getMsgSizes()[i]);
                System.out.println("oldMsgUUID: " + this.getOldMsgUUIDs()[i]);
                System.out.println();
            }
        }
        System.out.println("==== DONE ==== ");
        System.out.println();
    }
}
