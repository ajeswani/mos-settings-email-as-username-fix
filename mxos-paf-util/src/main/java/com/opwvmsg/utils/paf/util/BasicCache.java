/*
 *      Copyright (c) 2002-2004 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/BasicCache.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This is a basic cache implementation.  Storage of objects is
 * permanent; they remain in the cache until they are removed
 * or the cache is cleared. Nulls are permitted as a key and 
 * as stored objects.  The methods are fully synchronized, so the
 * class is thread-safe.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class BasicCache implements Cache, Serializable {

    /**
     * The map that contains the cached objects.
     */
    private Map cache = new HashMap();

    /**
     * Create an instance of a <code>BasicCache</code> using the
     * content of the given parameters <code>Map</code> as the
     * initial contents.
     *
     * @param parameters a <code>Map</code> of the initial contents
     *        of the cache
     * @return an instnace of a <code>BasicCache</code> constructed
     *        using the given parameters as the initial contents
     */
    public static Cache getInstance(Map parameters) {
        if (parameters != null) {
            return new BasicCache(parameters);
        }
        return new BasicCache();
    }

    /**
     * Construct an empty cache.
     */
    public BasicCache() {
    }

    /**
     * Construct a cache with the given initial contents.
     *
     * @param initialContents initialize the cache with these contents
     */
    public BasicCache(Map initialContents) {
        putAll(initialContents);
    }

    /**
     * Get an object from the cache.
     *
     * @param key the key for the desired object
     *
     * @return the object, or <code>null</code> if the object is not
     *    in the cache
     */
    public synchronized Object get(Object key) {
        return cache.get(key);
    }

    /**
     * Get the contents of the cache.
     *
     * @return map cache contents as a map
     */
    public synchronized Map getMap() {
        return Collections.unmodifiableMap(cache);
    }

    /**
     * Put an object into the cache.
     *
     * @param key the key for the object
     * @param value the object
     *
     * @return the object previously stored in the cache under this
     *    key, or <code>null</code> if there was no such object
     */
    public synchronized Object put(Object key, Object value) {
        return cache.put(key, value);
    }

    /**
     * Remove an object from the cache.
     *
     * @param key the key for the object to remove
     *
     * @return the removed object, or <code>null</code> if there was
     *    no such object
     */
    public synchronized Object remove(Object key) {
        return cache.remove(key);
    }

    /**
     * Clear the contents of the cache.
     */
    public synchronized void clear() {
        cache.clear();
    }

    /**
     * Returns a string representation of this <code>Cache</code> object
     * in the format of a <code>Hashtable</code> string representation.
     *
     * @return a string representation of this cache
     */
    public synchronized String toString() {
        int max = cache.size() - 1;
        StringBuffer buf = new StringBuffer();
        Iterator it = cache.entrySet().iterator();

        buf.append("{");
        for (int i = 0; i <= max; i++) {
            Map.Entry e = (Map.Entry) (it.next());
            Object key = e.getKey();
            Object value = e.getValue();
            buf.append((key   == this ? "(this Map)" : key) + "=" + 
                       (value == this ? "(this Map)" : value));

            if (i < max) {
                buf.append(", ");
            }
        }
        buf.append("}");
        return buf.toString();
    }

    /**
     * Update the cache with the contents of the given map.
     *
     * @param map update cache contents with the contents of this map
     */
    private void putAll(Map map) {
        if (map != null && map.size() > 0) {
            Iterator keys = map.keySet().iterator();
            while (keys.hasNext()) {
                Object key = keys.next();
                put(key, map.get(key));
            }
        }
    }

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        try {
            putAll(getMap());
        } catch (UnsupportedOperationException e) {
        }
    }
}
