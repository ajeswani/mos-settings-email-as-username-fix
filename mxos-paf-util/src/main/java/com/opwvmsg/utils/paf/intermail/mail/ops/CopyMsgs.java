/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.Folder;
import com.opwvmsg.utils.paf.intermail.mail.MessageMetadata;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will copy messages from one folder to another. (and
 * possibly delete the original location).
 */
public class CopyMsgs extends AbstractMssOperation {

    // input
    private String name;
    private int[] uids;
    private String[] messageIds;
    protected int options;
    private int accessId;
    private String fromPathname;
    private String destPathname;

    private UUID srcFolderUUID; // Source folder UUID
    private UUID destFolderUUID;// Destination folder UUID
    private MessageMetadata[] messageMetadatas;// Array of MessageMetadata
    private boolean deleteFromSource;// Whether to move ( TRUE ) or copy message
                                     // ( FALSE )

    // output
    private Folder folder;
    private int folderUIDValidity;// folderUIDValidity of destination
    private int[] srcMovedUIDs;// Imap uid's for messages moved
    private int[] destNewUIDs;// Imap uid's of messages created at destination.

    // internal
    private String url;
    private String hostHeader;
    private String[] fromPathnameArray;
    private String[] destPathnameArray;

    // constants
    public static final int COPY_MSGS_FOLDER_IS_HINT = 0x01;
    public static final int COPY_MSGS_MULTIPLE_OK = 0x02;
    public static final int COPY_MSGS_GET_DEST_FOLDER = 0x04;
    public static final int COPY_MSGS_DELETE_ORIGINALS = 0x08;
    public static final int COPY_MSGS_SUPPRESS_MERS = 0x10;

    //Only for leapord RME model
    public static final int COPY_MSGS_DISABLE_NOTIFICATION = 0x1000;
    
    /**
     * Constructor for CopyMsgs
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param fromPathname The name of the folder to copy from
     * @param destPathname The name of the folder to copy to
     * @param uids an array of integer UIDs to copy
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options options for this operation. possible bits are: <br>
     *            COPY_MSGS_FOLDER_IS_HINT - message may not be in specified src
     *            folder <br>
     *            COPY_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            COPY_MSGS_GET_DEST_FOLDER - load and return the destination
     *            folder with the reply <br>
     *            COPY_MSGS_DELETE_ORIGINALS - after copy, delete original
     *            messages <br>
     *            COPY_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     */
    public CopyMsgs(String host, String name, String fromPathname,
            String destPathname, int[] uids, int accessId, int options) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.fromPathname = fromPathname;
        this.destPathname = destPathname;
        url = getMsUrl(host, name, MSS_P_CL_COPYMSGS);
        fromPathnameArray = buildPathnameArray(fromPathname);
        destPathnameArray = buildPathnameArray(destPathname);
        this.uids = uids;
        this.accessId = accessId;
        this.options = options;
    }

    /**
     * Constructor for CopyMsgs for Stateless RME
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param MessageMetadata array of messageMetadata's to be copied
     * @param srcFolderUUID Source folder UUID
     * @param destFolderUUID Destination folder UUID
     * @param deleteFromSourceWhether to move ( TRUE ) or copy message ( FALSE )
     * @param options options for this operation. possible bits are: <br>
     *            COPY_MSGS_DISABLE_NOTIFICATION - diable Notifications <br>
     */
    public CopyMsgs(String host, String name, String realm,
            MessageMetadata[] messageMetadatas, UUID srcFolderUUID,
            UUID destFolderUUID, boolean deleteFromSource, int options) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.url = getMsUrl(host, name, MSS_SL_COPYMESSAGES);
        this.messageMetadatas = messageMetadatas;
        this.srcFolderUUID = srcFolderUUID;
        this.destFolderUUID = destFolderUUID;
        this.deleteFromSource = deleteFromSource;
        this.options = options;
        this.hostHeader = getHostHeader(host, name, realm);

    }

    /**
     * Constructor for CopyMsgs
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param fromPathname The name of the folder to copy from
     * @param destPathname The name of the folder to copy to
     * @param uids an array of integer UIDs to copy
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options options for this operation. possible bits are: <br>
     *            COPY_MSGS_FOLDER_IS_HINT - message may not be in specified src
     *            folder <br>
     *            COPY_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            COPY_MSGS_GET_DEST_FOLDER - load and return the destination
     *            folder with the reply <br>
     *            COPY_MSGS_DELETE_ORIGINALS - after copy, delete original
     *            messages <br>
     *            COPY_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     */
    public CopyMsgs(String host, String name, String fromPathname,
            String destPathname, int accessId, int options, String[] messageIds) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.fromPathname = fromPathname;
        this.destPathname = destPathname;
        url = getMsUrl(host, name, MSS_P_CL_COPYMSGS);
        fromPathnameArray = buildPathnameArray(fromPathname);
        destPathnameArray = buildPathnameArray(destPathname);
        this.messageIds = messageIds;
        this.accessId = accessId;
        this.options = options;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            if (fromPathname == null || destPathname == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "pathname", "CopyMsgs" }));
            }
            if (uids == null) {
                uids = new int[0];
            }
            if (messageIds == null) {
                messageIds = new String[0];
            }
            callRme(MSS_P_CL_COPYMSGS);
        } else { // Leopard
            if (messageMetadatas == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "messageMetadata", "CopyMsgs" }));
            }
            if (MessageMetadata.validateMessageMetadatasSize(messageMetadatas) == false) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] {
                                        "messageMetadatas not of equal size",
                                        "CopyMsgs" }));
            }

            if (MessageMetadata.validateAttributes(messageMetadatas) == false) {
                throw new IntermailException(
                        LogEvent.ERROR,
                        LogEvent.formatLogString(
                                "Ms.BadRequestArg",
                                new String[] {
                                        "messageMetadatas do not contain same attributes",
                                        "CopyMsgs" }));
            }
            if (srcFolderUUID == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "srcFolderUUID", "CopyMsgs" }));
            }

            if (destFolderUUID == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "destFolderUUID", "CopyMsgs" }));
            }
            callRme(MSS_SL_COPYMESSAGES);
        }
    }

    /**
     * get the destination folder returned on the reply (if available). This
     * folder is only returned if the option was specified.
     * 
     * @return Folder destination Folder
     */
    public Folder getFolder() {
        return folder;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(fromPathnameArray);
        outStream.writeStringArray(destPathnameArray);
        // msg specs
        if (messageIds != null && messageIds.length > 0) {
            outStream.writeInt(messageIds.length);
            for (int i = 0; i < messageIds.length; i++) {
                outStream.write(1);
                outStream.writeInt(-1); // uids
                outStream.writeString(messageIds[i]); // msgids
            }
        } else {
            outStream.writeInt(uids.length);
            for (int i = 0; i < uids.length; i++) {
                outStream.write(1);
                outStream.writeInt(uids[i]);
                outStream.writeString(null);
            }
        }
        outStream.writeInt(accessId);
        outStream.writeInt(options);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // read Message
        boolean returningFolder = inStream.readBoolean();
        if (returningFolder) {
            folder = new Folder(inStream, false);
        }
        logEvents = inStream.readLogEventArray();
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_COPYMESSAGES));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        outStream.writeString(url);

        MessageMetadata.write(messageMetadatas, outStream);

        outStream.writeUUID(srcFolderUUID);
        outStream.writeUUID(destFolderUUID);
        outStream.writeBoolean(deleteFromSource);
        outStream.writeInt(options);
        outStream.flush();
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        folderUIDValidity = inStream.readInt();
        int noSrcMovedUID = inStream.readInt();
        srcMovedUIDs = new int[noSrcMovedUID];
        for (int i = 0; i < noSrcMovedUID; i++)
            srcMovedUIDs[i] = inStream.readInt();
        int noDestNewUIDs = inStream.readInt();
        destNewUIDs = new int[noDestNewUIDs];
        for (int i = 0; i < noDestNewUIDs; i++)
            destNewUIDs[i] = inStream.readInt();

        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the folderUIDValidity
     */
    public int getFolderUIDValidity() {
        return folderUIDValidity;
    }

    /**
     * @return the srcMovedUIDs
     */
    public int[] getSrcMovedUIDs() {
        return srcMovedUIDs;
    }

    /**
     * @return the destNewUIDs
     */
    public int[] getDestNewUIDs() {
        return destNewUIDs;
    }

}
