/* H+ 
 * 
 * Copyright 2004 Software.com, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of
 * Software.com, Inc. The software may be used and/or copied only with the
 * written permission of Software.com, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/rme/SocketPool.java#1 $ 
 * H- */
package com.opwvmsg.utils.paf.intermail.rme;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.util.BasicStackPool;
import com.opwvmsg.utils.paf.util.LazyClock;

/**
 * A pool specifically designed for RmeSocket objects. The pool also handles the
 * creation and closing of RmeSocket objects in order to keep track of the total
 * number of connections that a client has requested.
 */
public class SocketPool extends BasicStackPool {
    /**
     * The initial size of this pool
     */
    private int initialSize;

    /**
     * The maximum idle time for connections in the pool
     */
    private int maxIdleTime = -1;

    /**
     * A test whether this pool is diabled (not accepting new objects)
     */
    private boolean disabled = false;

    // hold on to information about the sockets in this pool.
    // used so that the pool can create a new object
    private String serverType = null;
    private String hostName;
    private int port;
    private int timeout;
    
    // This is the predicted number of connections the the pool
    // knows about. It may not be accurate (for instance if a
    // client drops a connection and doesn't return it to the pool.
    // But it is useful for logging and estimates.
    private int totalConnections = 0;
    private static LazyClock idleClock = LazyClock.getInstance();

    /* a logger for this class - mostly for debug logs */
    private static final Logger logger = Logger.getLogger(SocketPool.class);

    /**
     * Constructs a new, empty pool with the specified capacity. Sets hostname,
     * port, and timeout info for the RmeSockets
     * 
     * @param capacity the maximum number of items to be retained by this pool.
     *            For this pool, a capacity of 0 disables the pool, and -1 means
     *            infinite capacity.
     * @param minSize the initial number of connections to place in the poolm
     * @param idleTimeSec number of seconds to elapse before connections are
     *            considered to be too idle and need to be pruned.
     * @param host The hostname used in creating new RmeSockets
     * @param port the port number used in creating new RmeSockets
     * @param timeout The SoTimeout to set on new RmeSockets
     * @param serverTypeIn a String name of the server type that the RmeSockets
     *            in the pool will connect to. used for logging purposes
     */
    public SocketPool(int capacity, int initialSize, int idleTimeSec,
            String host, int port, int timeout, String serverType) {
        super(capacity == -1 ? 0 : capacity);
        if (capacity == -1) {
            disabled = true;
        }
        hostName = host;
        this.port = port;
        this.timeout = timeout;
        this.initialSize = initialSize;
        this.serverType = serverType;
        maxIdleTime = idleTimeSec * 1000;

        if (logger.isDebugEnabled()) {
            logger.debug ("Pool Created: Capacity: "+capacity+
                          ", InitialSize: "+initialSize+
                          ", IdleTimeSec: "+idleTimeSec+
                          ", Host: "+host+":"+port+
                          ", Timeout: "+timeout+
                          ", ServerType: "+serverType);
        }
        // initialize the minimum connections in the pool
        for (int i = 0; i < initialSize; i++) {
            try {
                putItem(createSocket());
            } catch (IntermailException ie) {
                logger.log(ie.getLog4jLevel(), ie.getFormattedString());
                // let all exceptions be handled when the
                // client goes to get the connection.
            }
        }
    }

    /**
     * Gets the host name used for the connections in this pool
     * 
     * @return String host name
     */
    public String getHost() {
        return hostName;
    }

    /**
     * Gets the port number used by the connections in this pool
     * 
     * @return int port number
     */
    public int getPort() {
        return port;
    }

    /**
     * Creates and returns a fresh socket based on the settings of this pool
     * 
     * @return new, initialized ExpiringRmeSocket
     * @throws IntermailException on any error
     */
    public ExpiringRmeSocket createSocket() throws IntermailException {
        ExpiringRmeSocket retSock = null;
        try {
            retSock = new ExpiringRmeSocket(hostName, port, timeout, maxIdleTime);
        } catch (UnknownHostException uhe) {
            throw new IntermailException(LogEvent.URGENT,
                                         LogEvent.formatLogString("Nio.ConnServerFail",
                                                                  new String[] {
                                                                          serverType,
                                                                          hostName,
                                                                          Integer.toString(port),
                                                                          uhe.getMessage() }));
        } catch (SocketTimeoutException ste) {
            throw new IntermailException(LogEvent.URGENT,
                                         LogEvent.formatLogString("Rme.ClientTimeout",
                                                                  new String[] { hostName
                                                                          + ":"
                                                                          + Integer.toString(port) }));
        } catch (IOException ie) {
            throw new IntermailException(LogEvent.URGENT,
                                         LogEvent.formatLogString("Nio.ConnServerFail",
                                                                  new String[] {
                                                                          serverType,
                                                                          hostName,
                                                                          Integer.toString(port),
                                                                          ie.getMessage() }));
        }

        synchronized (getLock()) {
            totalConnections++;
            if (logger.isDebugEnabled()) {
                logger.debug("Pool growth: " + hostName + ":" + port
                        + " ==> Pool size: " + size() + " Total connections: "
                        + totalConnections);
            }
        }
        return retSock;
    }

    /* (non-Javadoc)
     * @see com.openwave.paf.util.Pool#getItem()
     */
    public Object getItem() {
        Object item = null;
        // See if we need to prune any connections
        item = getItemForPruning();
        if (item != null) {
            return item;
        }
        // if not, return the last connection used.
        return super.getItem();
    }
    
    /**
     * Check the idle time of the socket on the bottom of the stack.
     * If it is stale, then return that socket, and mark it for closure
     * so that will not be placed back into the Pool when it completes.
     * 
     * @return Object item to use, then prune, or null if there are no
     *         stale items
     */
    private Object getItemForPruning() {
        Object item = null;       
        synchronized (getLock()) {
            if (size() > 0 && totalConnections > initialSize) {
                List items = getItems();
                ExpiringRmeSocket oldSocket = (ExpiringRmeSocket)items.get(0);                
                if (oldSocket.isExpired()) {
                    item = items.remove(0);
                    ((RmeSocket)item).setReadyToClose();
                }
            }
        }
        return item;
    }
    
    /**
     * Adds the specified item to this pool.
     * 
     * If the item cannot be added to this pool without exceeding the capacity
     * then the item is returned.
     * 
     * @param item an <code>RmeSocket</code> to be placed into this pool for
     *            later use.
     * 
     * @return RmeSocket the specified item if the capacity of the pool would be
     *         exceeded by adding it and <code>null</code> otherwise.
     */
    public Object putItem(Object item) {
        if (!(item instanceof ExpiringRmeSocket)) {
            // don't allow it in the pool
            return null;
        }
        ExpiringRmeSocket socket = (ExpiringRmeSocket) item;
        
        synchronized (getLock()) {
            if (!disabled && socket.isReady() && (!socket.readyToClose())
                    && socket.getHost().equals(hostName)) {
                socket = (ExpiringRmeSocket)putItemInternal(socket);
            }

            if (socket != null) {
                // couldn't put it in the pool, close the socket
                try {
                    socket.close();
                } catch (IOException e) {
                    logger.log(Level.ERROR,
                               LogEvent.formatLogString("Nio.CloseSockFail",
                                                        new String[] {
                                                                socket.getHost()
                                                                        + ":"
                                                                        + socket.getPort(),
                                                                e.getMessage() }));
                }
                if (socket.getHost().equals(hostName)) {
                    totalConnections--;
                    if (logger.isDebugEnabled()) {
                        logger.debug("Pool pruned: " + hostName + ":" + port
                                + " ==> Pool size: " + size()
                                + " Total Connections: " + totalConnections);
                    }

                    if (totalConnections < initialSize) {
                        // make a best effort to repopulate the
                        // connection. if it fails.. oh well.
                        try {
                            putItemInternal(createSocket());
                        } catch (IntermailException ie) {
                            logger.log(ie.getLog4jLevel(),
                                       ie.getFormattedString());
                        }
                    }
                }
            }

            return socket;
        }
    }
    
    /**
     * Put the item in the pool, and reset the idle timestamp.
     * 
     * @param item an ExpiringRmeSocket to add to the pool
     * @return the item if it was added to the pool
     */
    private Object putItemInternal(ExpiringRmeSocket item) {
        item.resetIdleTimestamp();
        // all items going into the pool are considered "used"
        // for purposes of failover
        item.setUsed();

        return (super.putItem(item));
    }

    /**
     * Removes all items from this pool.
     */
    public void clear() {
        synchronized (getLock()) {
            // close all current connections
            RmeSocket tmpSocket = (RmeSocket) getItem();
            while (tmpSocket != null) {
                try {
                    tmpSocket.close();
                    tmpSocket = (RmeSocket) getItem();
                } catch (IOException e) {
                }
            }
            //clear the pool
            super.clear();
            totalConnections = 0;
                        
            if (logger.isDebugEnabled()) {
                logger.debug("Pool cleared: " + hostName + ":" + port
                        + " ==> Pool size: " + size()
                        + " Total Connections: " + totalConnections);
            }

        }
    }
    
    /**
     * An ExpiringRmeSocket is an Rme Socket that may expire
     * after a certain time has passed without it being used.
     */
    public static final class ExpiringRmeSocket extends RmeSocket {
        private long idleTimeStart = 0L;
        private int maxIdleTime = -1;
        
        /**
         * Constructor for ExpiringRmeSocket
         * 
         * @see RmeSocket(String, int, int)
         */
        protected ExpiringRmeSocket(String host, int port, int timeout, int maxIdleTime) 
               throws UnknownHostException, IOException, IntermailException { 
            super(host, port, timeout);
            this.maxIdleTime = maxIdleTime;
            Config config = Config.getInstance();
            String tcpKeepAlive = config.get("enableTCPKeepalive", "default");
            // TCPKeepalive generates extra network traffic, which can have an impact on routers and firewalls.
            // See ITS 1230930 for more details.
            if (tcpKeepAlive != null && tcpKeepAlive.length() > 0) {
                if (tcpKeepAlive.equalsIgnoreCase("true")
                        || (tcpKeepAlive.equalsIgnoreCase("on"))) {
                    if (logger.isDebugEnabled()){
                        logger.debug("TCPKeepalive set as: " + tcpKeepAlive);
                    }
                    this.setKeepAlive(true);
                } else if (tcpKeepAlive.equalsIgnoreCase("false")
                        || (tcpKeepAlive.equalsIgnoreCase("off"))) {
                    if (logger.isDebugEnabled()){
                        logger.debug("TCPKeepalive set as: " + tcpKeepAlive);
                    }
                    this.setKeepAlive(false);
                }
            }
        }
        
        /**
         * Sets a timestamp to track the time this socket is idle
         * 
         * @param idle a long datestamp
         */
        public void resetIdleTimestamp() {
            idleTimeStart = idleClock.currentTimeMillis();
        }

        /**
         * Determine if this socket has been idle for longer than the
         * max idle time.
         * 
         * @return true if this socket is expired.
         */
        public boolean isExpired() {
            if (maxIdleTime == -1) {
                return false;
            }
            return ((idleClock.currentTimeMillis() - idleTimeStart) >                    
                    maxIdleTime);
        }
    }
}

