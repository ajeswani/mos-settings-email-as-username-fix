/*
 * Copyright (c) 2003 Openwave Systems Inc.  All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/spi/PasswordProvider.java#1 $
 */

package com.opwvmsg.utils.paf.util.password.spi;

import java.util.Map;

import com.opwvmsg.utils.paf.util.password.Password;
import com.opwvmsg.utils.paf.util.password.PasswordException;

/**
 * This interface defines the class that must be implemented
 * by all providers for the Password API.  The implementation
 * must indicate which type of provider it is through its definition
 * of the {@link #isType <code>isType</code>} method.
 *
 * @author Conrad Damon
 * @version $Revision: #1 $
 */
public interface PasswordProvider {

    /**
     * Gets a <code>Password</code> using the given setup information.
     * <p>
     * @param setup a map of setup information needed by the provider
     * @return an <code>Password</code>
     * @throws PasswordException for any error
     */
    Password getPassword(Map setup) throws PasswordException;

    /**
     * Test whether this provider is capable of providing the Password API 
     * for the specified provider type.
     * <p>
     * @param type the desired type of provider
     * @return <code>true</code> if the named type is handled by this provider,
     *         else <code>false</code>
     */
    boolean isType(String type);
}
