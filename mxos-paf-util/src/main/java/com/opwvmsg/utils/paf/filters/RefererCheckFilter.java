/*
 * Copyright (c) 2006-2007 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/filters/RefererCheckFilter.java#1 $
 */
package com.opwvmsg.utils.paf.filters;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 * The RefererCheckFilter blocks certain requests that originate from a foreign
 * domain.  These requests are identified by the "Referer" field in the request header.
 * If the user enters the URL manually, this field is set to null.  If the user clicked
 * on a link, the "Referer" field will identify the site where the user came from.
 *
 * The idea is that requests originating from outside servers are not to be trusted.  It
 * is possible to post an instruction to WebEdge or Rich Mail that will modify the state
 * of the user's mail content or application configuration.  All such instructions must
 * originate from within the trusted application.
 *
 * This filter allows configuration of acceptable methods (i.e. GET, POST, PUT), acceptable
 * paths (i.e. /do/login), and acceptable parameters (i.e. v, l).  Any request with a
 * referer that has any request component that does not match the allowed set will be
 * rejected.
 *
 * @author Paul Lovvik
 */
public class RefererCheckFilter implements Filter {
    private static final Logger logger = Logger.getLogger(RefererCheckFilter.class);
    private static final String ALLOWED_METHODS = "allowedMethods";
    private static final String ALLOWED_PATHS = "allowedPaths";
    private static final String ALLOWED_PARAMETERS = "allowedParameters";
    private static final String ALLOWED_HOSTS= "allowedHosts";

    /**
     * The set of request methods ("GET", "POST", or "PUT") which will
     * be allowed by this filter.
     */
    private HashMap allowedMethods = new HashMap();

    /**
     * The set of paths which will be allowed from foreign hosts that
     * will be allowed by this filter.
     */
    private HashMap allowedPaths = new HashMap();

    /**
     * The set of parameters which will be allowed from foreign hosts
     * that will be allowed by this filter.
     */
    private HashMap allowedParameters = new HashMap();

    /**
     * The set of parameters which will be allowed from foreign hosts
     * that will be allowed by this filter.
     */
    private HashMap allowedHosts = new HashMap();

    /**
     * Initializes this filter.  The possible parameters are:
     * <ul>
     * <li> allowedMethods - any combination of "GET", "POST", or "PUT" describing the
     * methods allowed which originate from foreign domains.</li>
     * <li> allowedPaths - a comma separated list of paths allowed from foreign domains.</li>
     * <li> allowedParameters - a comma separated list of parameters allowed from foreign domains.</li>
     * <li> allowedHosts - a comma separated list of hosts allowed from foreign domains.</li>
     * </ul>
     *
     * @param fconfig the FilterConfig used to access the configuration
     */
    public void init(FilterConfig fconfig) throws ServletException {
        String allowedMethods = fconfig.getInitParameter(ALLOWED_METHODS);
        String allowedPaths = fconfig.getInitParameter(ALLOWED_PATHS);
        String allowedParameters = fconfig.getInitParameter(ALLOWED_PARAMETERS);
        String allowedHosts = fconfig.getInitParameter(ALLOWED_HOSTS);
        if (allowedMethods != null) {
            StringTokenizer methods = new StringTokenizer(allowedMethods, ",");
            while (methods.hasMoreTokens()) {
                String method = methods.nextToken();
                if ("get".equalsIgnoreCase(method)) {
                    this.allowedMethods.put("GET", "GET");
                } else if ("post".equalsIgnoreCase(method)) {
                    this.allowedMethods.put("POST", "POST");
                } else if ("put".equalsIgnoreCase(method)) {
                    this.allowedMethods.put("PUT", "PUT");
                } else {
                    logger.error("refererCheck filter initialization has a bad method " + method +
                                 ".  Must be GET, POST, or PUT.");
                    throw new ServletException("Bad method in RefererCheckFilter initialization: " +
                                               method + ".  Must be any combination of GET, POST, or PUT");
                }
            }
        }
        if (allowedPaths != null) {
            StringTokenizer paths = new StringTokenizer(allowedPaths, ",");
            while (paths.hasMoreTokens()) {
                String path = paths.nextToken();
                this.allowedPaths.put(path, path);
            }
        }
        if (allowedParameters != null) {
            StringTokenizer parameters = new StringTokenizer(allowedParameters, ",");
            while (parameters.hasMoreTokens()) {
                String parameter = parameters.nextToken();
                this.allowedParameters.put(parameter, parameter);
            }
        }
        if (allowedHosts != null) {
            StringTokenizer hosts = new StringTokenizer(allowedHosts, ",");
            while (hosts.hasMoreTokens()) {
                String host = hosts.nextToken();
                this.allowedHosts.put(host, host);
            }
        }

        if (logger.isInfoEnabled()) {
            if (this.allowedMethods.size() > 0) {
                StringBuffer methods = new StringBuffer();
                Iterator i = this.allowedMethods.keySet().iterator();
                while (i.hasNext()) {
                    methods.append(" " + (String)i.next());
                }
                logger.info("Methods allowed: " + methods.toString());
            } else {
                logger.info("No methods allowed.");
            }

            if (this.allowedPaths.size() > 0) {
                StringBuffer paths = new StringBuffer();
                Iterator i = this.allowedPaths.keySet().iterator();
                while (i.hasNext()) {
                    paths.append(" " + (String)i.next());
                }
                logger.info("Paths allowed: " + paths.toString());
            } else {
                logger.info("No paths allowed");
            }

            if (this.allowedParameters.size() > 0) {
                StringBuffer parameters = new StringBuffer();
                Iterator i = this.allowedParameters.keySet().iterator();
                while (i.hasNext()) {
                    parameters.append(" " + (String)i.next());
                }
                logger.info("Parameters allowed: " + parameters.toString());
            } else {
                logger.info("No parameters allowed.");
            }
        }
    }

    public void destroy() {
    }

    /**
     * Check the referer.  If the referer is foreign, this filter compares the request method to the
     * list of allowed methods.  If the method is not allowed, the path is not allowed, or any
     * specified request parameters are not allowed, an exception is thrown.
     *
     * @param request the ServletRequest
     * @param response the ServletResponse
     * @param chain the FilterChain
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest)request;
        String referer = req.getHeader("Referer");
        if (referer != null) {
            /*
             * The referer must match the first part of the request URL.
             */
            URL sourceURL = new URL(referer);
            URL requestURL = new URL(req.getRequestURL().toString());
            String host = sourceURL.getHost();
            if ((host != null && host.equals(requestURL.getHost())) || this.allowedHosts.containsKey(host)) {
                /*
                 * All is well.
                 */
            } else {
                /*
                 * Referer doesn't match.
                 */
                String method = req.getMethod();
                if (!this.allowedMethods.containsKey(method)) {
                    String queryString = req.getQueryString();
                    logger.error("Method '" + method + "' from foreign host '" + host +
                                 "' is not allowed.  Attempted reference is " +
                                 req.getRequestURL() + (queryString == null ? "" : "?" + queryString));
                    ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN);
                    return;
                }
                String path = req.getRequestURI();
                if (!this.allowedPaths.containsKey(path)) {
                    String queryString = req.getQueryString();
                    logger.error("Path '" + path + "' from foreign host '" + host +
                                 "' is not allowed.  Attempted reference is " +
                                 req.getRequestURL() + (queryString == null ? "" : "?" + queryString));
                    ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN);
                    return;
                }
                String query = req.getQueryString();
                if (req.getQueryString() != null) {
                    Iterator parameters = getParameterNamesFromQuery(query);
                    while (parameters.hasNext()) {
                        String parameter = (String)parameters.next();
                        if (!this.allowedParameters.containsKey(parameter)) {
                            String queryString = req.getQueryString();
                            logger.error("Parameter '" + parameter + "' from foreign host '" + host +
                                         "' is not allowed.  Attempted reference is " +
                                         req.getRequestURL() + (queryString == null ? "" : "?" +
                                                                queryString));
                            ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN);
                            return;
                        }
                    }
                }
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Returns the names of all parameters from the specified query string.
     * Each parameter is a key=value pair, with each pair separated by the
     * "&" character.
     *
     * @praam queryString the string containing the query parameters
     *
     * @return an Iterator containing only the names of the parameters found in the string.
     */
    private Iterator getParameterNamesFromQuery(String queryString) {
        ArrayList parameterNames = new ArrayList();
        if (queryString != null) {
            StringTokenizer parameters = new StringTokenizer(queryString, "&");
            while (parameters.hasMoreTokens()) {
                String parameter = parameters.nextToken();
                int equalsIndex = parameter.indexOf("=");
                if (equalsIndex > 0) {
                    String name = parameter.substring(0, equalsIndex);
                    parameterNames.add(name);
                }
            }
        }
        return (parameterNames.iterator());
    }
}
