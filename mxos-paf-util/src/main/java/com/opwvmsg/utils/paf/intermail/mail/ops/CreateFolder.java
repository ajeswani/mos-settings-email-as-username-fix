/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.Folder;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will create a new folder.
 * 
 * @author mxos-dev
 */
public class CreateFolder extends AbstractMssOperation {

    // input
    private String name;
    private UUID parentFolderUUID;
    private String folderName;
    private long uidValidity;

    private String pathname;
    private int options;
    private int accessId;

    // output
    private UUID folderUUID;
    private Folder folder;

    // constants
    public static final int CREATE_FOLDER_SUPPRESS_MERS = 0x01;

    // internal
    private String url;
    private String hostHeader;
    private String[] pathnameArray;

    /**
     * Constructor for CreateFolder(Stateless RME Call)
     * 
     * @param host : The MSS hostname for the user
     * @param name : The MSS id of the user
     * @param realm The mailRealm of the user
     * @param parentFolderUUID : UUID of parent folder in which we want to
     *            create new folder
     * @param folderName : Name of the folder to be created.
     * @param uidValidity : UID Validity
     */
    public CreateFolder(String host, String name, String realm,
            UUID parentFolderUUID, String folderName, long uidValidity) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.parentFolderUUID = parentFolderUUID;
        this.folderName = folderName;
        url = getMsUrl(host, name, MSS_SL_CREATEFOLDER);
        this.uidValidity = uidValidity;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /**
     * Constructor for CreateFolder(Stateful RME Call)
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder to create
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options options for this operation. possible bits are: <br>
     *            CREATE_FOLDER_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     */
    public CreateFolder(String host, String name,
            String pathname, int accessId, int options) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.pathname = pathname;
        url = getMsUrl(host, name, MSS_P_CL_CREATEFOLDER);
        pathnameArray = buildPathnameArray(pathname);
        this.accessId = accessId;
        this.options = options;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            if (pathname == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "pathname", "CreateFolder" }));
            }
            callRme(MSS_P_CL_CREATEFOLDER);
        } else {
            if (parentFolderUUID == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "parentFolderUUID",
                                        "CreateFolder" }));
            }
            if (folderName == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "folderName", "CreateFolder" }));
            }
            callRme(MSS_SL_CREATEFOLDER);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(pathnameArray);
        outStream.writeInt(-1); // UID validity.
        outStream.writeInt(accessId);
        if (outStream.rmeProtAtLeast(RME_CL_OPERATIONS_OPTIONS_VER)) {
            outStream.writeInt(options);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        folder = new Folder(inStream, false);
        logEvents = inStream.readLogEvent();
    }

    /*
     * OutStream Format(Parameter Name:Parameter Type:Description):
     * clientId:clientId:ClientId msURL:msURL:msURL parentFolderUUID:int128:UUID
     * of parent folder in which we want to create new folder.
     * folderName:string:Name of the folder to be created. uidValidity:long:UID
     * Validity
     * @see
     * com.opwvmsg.utils.paf.intermail.rme.AbstractOperation#constructHttpData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_CREATEFOLDER));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        outStream.writeString(url);
        outStream.writeUUID(parentFolderUUID);
        outStream.writeString(folderName);
        // Temporary Fix(casting to int and writing int) as MSS is seeing this
        // as INT32
        outStream.writeInt((int) uidValidity);
        outStream.flush();
    }

    @Override
    protected void receiveHttpData() throws IOException {
        folderUUID = inStream.readUUID();
        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the folderUUID
     */
    public UUID getFolderUUID() {
        return folderUUID;
    }

    /**
     * @return the folder
     */
    public Folder getFolder() {
        return folder;
    }
}
