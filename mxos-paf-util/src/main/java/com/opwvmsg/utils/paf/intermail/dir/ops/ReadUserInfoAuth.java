/*
 * Copyright 2005-2006 Openwave Systems, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems, Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id:
 */
package com.opwvmsg.utils.paf.intermail.dir.ops;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.opwvmsg.utils.paf.intermail.dir.AbstractDirectoryOperation;
import com.opwvmsg.utils.paf.intermail.dir.Account;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;

public class ReadUserInfoAuth extends AbstractDirectoryOperation {

    // Auth constants
    public static final int SMTPQUERY = 'S';
    public static final int POPQUERY = 'P';
    public static final int MBOXQUERY = 'M';
    public static final int ALIASQUERY = 'A';

    // request vars
    String user;
    String pass;
    boolean doAuth;
    int queryType;
    boolean getAllCos;
    String[] cosAttrs;

    // reply vars
    Account account;
    boolean authOk = false;

    /**
     * This operation implements the DIR_P_GETUSERINFO RME operation. This
     * operation will look up the user in the directory, optionally authenticate
     * the user's password, and return a list of attributes.
     * 
     * @param username The user's SMTP address, alias, mailbox id pop address
     * @param password The user's password (required for authentication)
     * @param doAuthIn Authenticate user by checking password
     * @param queryTypeIn The type of address used in the username can be one of
     *            SMTPQUERY, POPQUERY, MAILBOXQUERY or ALIASQUERY
     * @param getAllCosIn if true, retrieve all cos values (and nothing else) if
     *            false, retrieve attrs specified in cosAttrsIn
     * @param cosAttrsIn user attributes to retrieve from the directory
     */
    public ReadUserInfoAuth(String username, String password, boolean doAuthIn,
            int queryTypeIn, boolean getAllCosIn, String[] cosAttrsIn) {
        user = username;
        pass = password;
        doAuth = doAuthIn;
        queryType = queryTypeIn;
        getAllCos = getAllCosIn;
        cosAttrs = cosAttrsIn;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    @Override
    public void execute() throws IntermailException {
        // check args
        if (pass == null && doAuth) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "password", "ReadUserInfoAuth" }));

        }

        try {
            if (pass != null
                    && pass.getBytes("UTF-8").length > MAX_PASSWORD_LENGTH) {
                throw new IntermailException(
                        LogEvent.ERROR,
                        LogEvent.formatLogString(
                                "Rme.ClientParmLengthError",
                                new String[] { "password", "ReadUserInfoAuth",
                                        String.valueOf((char) queryType), user }));
            }

            if (user != null
                    && user.getBytes("UTF-8").length > MAX_USERNAME_LENGTH) {
                throw new IntermailException(
                        LogEvent.ERROR,
                        LogEvent.formatLogString(
                                "Rme.ClientParmLengthError",
                                new String[] { "username", "ReadUserInfoAuth",
                                        String.valueOf((char) queryType), user }));
            }
        } catch (UnsupportedEncodingException ignore) { // never happen
        }

        if (queryType != SMTPQUERY && queryType != POPQUERY
                && queryType != MBOXQUERY && queryType != ALIASQUERY) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "queryType", "ReadUserInfoAuth" }));

        }
        callRme(DIR_P_GETUSERINFO);
    }

    /**
     * Tests to see if the authentication succeeded.
     * 
     * @return true if the user was authenticated properly, false otherwise if
     *         authentication was not requested, then this will return true if
     *         the user was retrieved successfully.
     */
    public boolean isAuthOk() {
        return authOk;
    }

    /**
     * Gets the Account object that was filled in by the RME response
     * 
     * @return the account
     */
    public Account getAccount() {
        return account;
    }

    // RME FUNCTIONS

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    @Override
    protected void sendData() throws IOException {
        // rme op supports multiple requests at once.. right now,
        // this op only does 1 at a time.
        outStream.writeInt(1); // 1 request
        outStream.writeBoolean(true); // have it
        outStream.writeString(user);
        if (pass != null) {
            outStream.writeString(pass);
        } else {
            outStream.writeString("");
        }
        outStream.writeInt(queryType); // POP/SMTP
        outStream.writeBoolean(doAuth);
        outStream.writeBoolean(getAllCos);// allcos
        outStream.writeStringArray(cosAttrs);// attrs
        outStream.writeString(""); // FM approve Senders Check - not used here
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    @Override
    protected void receiveData() throws IOException {

        // read the number of responses
        // This will always be 1, unless we change sendData above
        inStream.readInt();

        account = new Account(inStream);
        authOk = (account.getLookupStatus() == Account.MS_SUCCESS);

        logEvents = inStream.readLogEvent();
    }

	@Override
	protected void constructHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void receiveHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}
}
