/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved. The copyright to
 * the computer software herein is the property of Openwave Systems, Inc. The
 * software may be used and/or copied only with the written permission of
 * Openwave Systems, Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:
 */
package com.opwvmsg.utils.paf.intermail.config;

import java.io.IOException;
import java.net.UnknownHostException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.config.intermail.IntermailConfig;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This abstract class is the basis for all rme operations to the config server.
 */
public abstract class AbstractConfigOperation extends AbstractOperation {

    public static final int UPDATE = 1; // for imconfupdate
    public static final int UPDATE_REPLY = 2; // for UPDATE and
    // UPDATE_CONN
    public static final int UPDATE_CONN = 3; // for servers connecting

    public static final int UPDATE_NOTIFY = 4; // for notifying servers
    public static final int INSTALLDICT = 5; // for assess/install new
    // dictionary
    public static final int IMPACT_REPLY = 6; // for servers to reply
    // about impact
    public static final int INSTALLKEYS = 7; // assess/install keys
    public static final int START_LOCKSERV = 8; // make a copy of the
    // config dict,
    // and lock the server until finished
    public static final int END_LOCKSERV = 9; // unlock the server
    public static final int ABORT_LOCKSERV = 10; // restore config dict,
    // unlock server
    public static final int LOCKSERV_REPLY = 11; // acknowledge above
    // three ops
    public static final int LOCKSERV_NOTIFY = 12; // for notifying servers
    // of pkg install

    protected static String configServerHost = null;
    protected static int configServerPort = 0;

    protected static String configTimeStamp = null;
    protected static String oldTimeStamp = "Fri Dec 27 06:45:00 EST 1974";
    protected static String defaultHost = null;
    protected static String defaultApplication = null;

    private static boolean reInit = false;
    protected IntermailConfig config = null;

    private static final Logger logger = Logger
            .getLogger(AbstractConfigOperation.class);

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#connect(boolean)
     */
    protected void connect(boolean reuse) throws IntermailException {
        if (configServerHost == null) {
            initCfgOps(config);
        }

        // There is no pooling for config connections, therefore there is
        // no connection manager. We just initialize the new connection
        // directly.
        if (rmeSocket == null || !rmeSocket.isReady()) {
            try {
                rmeSocket = new RmeSocket(configServerHost, configServerPort,
                        -1);
                Config config = Config.getInstance();
                String tcpKeepAlive = config.get("enableTCPKeepalive",
                        "default");
                // TCPKeepalive generates extra network traffic, which can have
                // an impact on routers and firewalls.
                // See ITS 1230930 for more details.
                if (tcpKeepAlive != null && tcpKeepAlive.length() > 0) {
                    if (tcpKeepAlive.equalsIgnoreCase("true")
                            || (tcpKeepAlive.equalsIgnoreCase("on"))) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("TCPKeepalive set as: " + tcpKeepAlive);
                        }
                        rmeSocket.setKeepAlive(true);
                    } else if (tcpKeepAlive.equalsIgnoreCase("false")
                            || (tcpKeepAlive.equalsIgnoreCase("off"))) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("TCPKeepalive set as: " + tcpKeepAlive);
                        }
                        rmeSocket.setKeepAlive(false);
                    }
                }
            } catch (UnknownHostException e1) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString(
                                "Nio.CreateSocketFail",
                                new String[] {
                                        configServerHost + ":"
                                                + configServerPort,
                                        e1.getMessage() }));
            } catch (IOException e2) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString(
                                "Nio.CreateSocketFail",
                                new String[] {
                                        configServerHost + ":"
                                                + configServerPort,
                                        e2.getMessage() }));
            }

        }
        if (reInit) {
            // unset this value for next time
            configServerHost = null;
        }

    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#releaseConnection()
     */
    protected void releaseConnection() throws IntermailException {
        try {
            rmeSocket.close();
        } catch (IOException e) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Nio.CloseSockFail",
                            new String[] {
                                    configServerHost + ":" + configServerPort,
                                    e.getMessage() }));
        }
        rmeSocket = null;
    }

    /**
     * Initializes the config server connection parameters. This assumes that
     * the config data has already been loaded (i.e. via config.db)
     */
    protected synchronized void initCfgOps(IntermailConfig config) {
        if (configServerHost == null) {
            try {
                if (config == null) {
                    config = (IntermailConfig) Config.getInstance();
                }
                configServerHost = config.get("confServHost");
                configServerPort = config.getInt(configServerHost,
                        "imconfserv", "confServPort");
                configTimeStamp = config.get("*", "common", "confTimeStamp");
                defaultHost = Config.getDefaultHost();
                defaultApplication = Config.getDefaultApp();
                reInit = false;
            } catch (ConfigException ce) {
                logger.log(Level.ERROR,
                        "The confServHost and confServPort could not be found.");
                configServerHost = null;
            }
        }
    }

    /**
     * Initializes the config connection parameters with passed in values.
     * 
     * @param host the config server host name
     * @param port the config server port
     * @param defHost the default host name for our application
     * @param defPort the default application name
     */
    protected synchronized void initCfgOps(String host, int port,
            String defHost, String defPort) {

        configServerHost = host;
        configServerPort = port;
        configTimeStamp = oldTimeStamp;
        defaultHost = defHost;
        defaultApplication = defPort;
        reInit = true;
    }

    @Override
    protected void createHttpClient(boolean usePool, int port)
            throws IntermailException {
        // TODO Auto-generated method stub
    }

    @Override
    protected void releaseHttpConnection() throws IntermailException {
        // TODO Auto-generated method stub
    }
}
