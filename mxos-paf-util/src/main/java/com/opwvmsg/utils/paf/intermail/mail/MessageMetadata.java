/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;
import com.opwvmsg.utils.paf.intermail.mail.MessageAttribute;

/**
 * This class encapsulates all the receivedAttributes of a message returned by
 * MSS_SL_RETRIEVESORTEDMSGS, MSS_SL_GETMESSAGEATTRIBUTES and is also used for
 * encoding MessageMetadata for MSS_SL_COPYMESSAGES RME. This is generic method
 * and can be used by any RME
 * 
 * @author mOS-dev
 */
public class MessageMetadata {
    // Map of receivedAttributes returned
    private Map<Short, MessageAttribute> receivedAttributes = null;
    private Map<Short, MessageAttribute> setAttributes = null;

    /**
     * Default Constructor.
     */
    public MessageMetadata(int numAttrs) {
        setAttributes = new LinkedHashMap<Short, MessageAttribute>(numAttrs);
    }

    /**
     * Constructor. Reads this MessageMetadata object from an RME stream
     * 
     * @param inStream the RME input stream
     * @param attributeDescriptions List of receivedAttributes that shall be
     *            returned by MSS
     * @throws IOException on any IO error
     */
    public MessageMetadata(RmeInputStream inStream,
            AttributeDescription[] attributeDescriptions) throws IOException {
        receivedAttributes = new LinkedHashMap<Short, MessageAttribute>(
                attributeDescriptions.length);
        for (int j = 0; j < attributeDescriptions.length; j++) {
            MessageAttribute ma = new MessageAttribute(
                    attributeDescriptions[j].getDataType(), inStream);
            receivedAttributes.put(attributeDescriptions[j].getAttributeId(),
                    ma);
        }
    }

    /**
     * Generic method to get an attribute value. This method finds the target
     * attribute and retrieve the value. Specifying uninitialized or unknown
     * attribute type would cause an exception.
     * 
     * @param attributeId: Id of the attribute to be fetched.
     * @param dataType: Data type of the attribute to be fetched.
     * @return: Attribute.
     * @throws IOException
     */
    public Object getAttribute(AttributeId attributeId, short dataType)
            throws IOException {
        MessageAttribute messageAttribute = receivedAttributes.get(attributeId
                .valueOf());
        if (messageAttribute == null)
            return null;
        else if (messageAttribute.getDataType() != dataType)
            throw new IOException(
                    "Incorrect Data format received from MSS for attributeId : "
                            + attributeId.name());
        return messageAttribute.getValue();
    }

    /**
     * Generic method to set an attribute value.
     * 
     * @param attributeId: Id of the attribute to be fetched.
     * @param dataType: Data type of the attribute to be fetched.
     * @param value: sets the value of the attribute
     * @return: Attribute.
     * @throws IOException
     */
    private void setAttribute(Short attributeId, short dataType, Object value) {
        MessageAttribute messageAttribute = new MessageAttribute(dataType,
                value);
        if (messageAttribute != null) {
            setAttributes.put(attributeId, messageAttribute);
        }
    }

    /**
     * Generic method to write MessageMetadata. This method finds the target
     * attribute and retrieve the value.
     * 
     * @param messageMetadatas: Array of MessageMetadata.
     * @param outStream: OutStream to write MessageMetadata.
     * @return: void.
     * @throws: IOException
     */
    public static void write(MessageMetadata[] messageMetadatas,
            RmeOutputStream outStream) throws IOException {
        Map<Short, MessageAttribute> attributeDescription = messageMetadatas[0]
                .getSetAttributes();
        writeAttributeDescription(attributeDescription, outStream);
        outStream.writeInt(messageMetadatas.length);
        for (MessageMetadata messageMetadata : messageMetadatas) {
            Map<Short, MessageAttribute> outAttributes = messageMetadata
                    .getSetAttributes();
            Iterator<Short> itr = outAttributes.keySet().iterator();
            while (itr.hasNext()) {
                Short key = itr.next();
                AttributeId id = AttributeId.valueOf(key);
                MessageAttribute messageAttribute = outAttributes.get(id
                        .valueOf());
                messageAttribute.write(messageAttribute, outStream);
            }
        }
    }

    /**
     * Generic method write array of AttributeDescription to outStream
     * 
     * @param attributes: Map of attributeId and MessageAttributes
     * @param outStream: OutStream to write AttributeDescription.
     * @return: void.
     * @throws: IOException
     */
    private static void writeAttributeDescription(
            Map<Short, MessageAttribute> attributes, RmeOutputStream outStream)
            throws IOException {
        outStream.writeInt(attributes.size());
        Iterator<Short> itr = attributes.keySet().iterator();
        while (itr.hasNext()) {
            Short key = itr.next();
            AttributeId id = AttributeId.valueOf(key);
            MessageAttribute messageAttribute = attributes.get(id.valueOf());
            outStream.writeShort(id.valueOf());
            outStream.writeShort(messageAttribute.getDataType());
        }
    }

    /**
     * Generic method validate size of each MessageMetadata in array
     * 
     * @param messageMetadatas: Array of MessageMetadata
     * @return: Boolean - true or false.
     * @throws:
     */
    public static boolean validateMessageMetadatasSize(
            MessageMetadata[] messageMetadatas) {
        int messageMetadataSize = messageMetadatas[0].getSetAttributes().size();
        for (MessageMetadata messageMetadata : messageMetadatas) {
            if (messageMetadata.getSetAttributes().size() != messageMetadataSize) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generic method validate Attributes of each MessageMetadata in array. It
     * checks if the Attributes are same in each MessageMetadata
     * 
     * @param messageMetadatas: Array of MessageMetadata
     * @return: Boolean - true or false.
     * @throws:
     */
    public static boolean validateAttributes(MessageMetadata[] messageMetadatas) {
        MessageMetadata msgMetadata = messageMetadatas[0];
        for (MessageMetadata messageMetadata : messageMetadatas) {
            Iterator<Short> itr = msgMetadata.getSetAttributes().keySet()
                    .iterator();
            while (itr.hasNext()) {
                Short key = itr.next();
                if (!messageMetadata.getSetAttributes().containsKey(key)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Send the key set of the available receivedAttributes.
     * 
     * @return
     */
    public Set<Short> getAttributeKeys() {
        return receivedAttributes.keySet();
    }

    /**
     * Return the UID
     * 
     * @return
     * @throws IOException
     */
    public int getUid() throws IOException {
        return (Integer) getAttribute(AttributeId.attr_uid,
                MessageAttribute.ATTR_UNIT32);
    }

    /**
     * Returns the from address in the message
     * 
     * @return From Address
     * @throws IOException
     */
    public String getFrom() throws IOException {
        String fromAddress = new String((byte[]) getAttribute(
                AttributeId.attr_from, MessageAttribute.ATTR_TYPE_STRING));
        return fromAddress;
    }

    /**
     * Returns the Message UUIS
     * 
     * @return Message UUID
     * @throws IOException
     */
    public UUID getMessageUUID() throws IOException {
        return (UUID) getAttribute(AttributeId.attr_messageUUID,
                MessageAttribute.ATTR_UNIT128);
    }

    /**
     * Returns the Bounce Message Attribute of the Message.
     * 
     * @return Bounce Attribute of the Message
     * @throws IOException
     */
    public Boolean getBounce() throws IOException {
        return (((Byte) getAttribute(AttributeId.attr_bounce,
                MessageAttribute.ATTR_UNIT8)) != 0);
    }

    /**
     * Returns the Deliver NDR Attribute of the Message.
     * 
     * @return
     * @throws IOException
     */
    public Boolean getDeliveredNDR() throws IOException {
        return (((Byte) getAttribute(AttributeId.attr_deliveredNDR,
                MessageAttribute.ATTR_UNIT8)) != 0);
    }

    /**
     * Returns the Private Attribute Flag of the Message.
     * 
     * @return
     * @throws IOException
     */
    public Boolean getPrivate() throws IOException {
        return (((Byte) getAttribute(AttributeId.attr_private,
                MessageAttribute.ATTR_UNIT8)) != 0);
    }

    /**
     * Returns the attribute attachments flag of the message.
     * 
     * @return Attachment Flag of the Message.
     * @throws IOException
     */
    public Boolean getHasAttachment() throws IOException {
        return (((Byte) getAttribute(AttributeId.attr_hasAttachment,
                MessageAttribute.ATTR_UNIT8)) != 0);
    }

    /**
     * Returns the Richmail of the Message.
     * 
     * @return Rishmail Flag
     * @throws IOException
     */
    public int getRichMailFlag() throws IOException {
        return (Integer) getAttribute(AttributeId.attr_richMailFlag,
                MessageAttribute.ATTR_UNIT32);
    }

    /**
     * Returns the Arrival Time of the Message.
     * 
     * @return Arrival Time.
     * @throws IOException
     */
    public long getArrivalTime() throws IOException {
        return (Long) getAttribute(AttributeId.attr_arrivalTime,
                MessageAttribute.ATTR_UNIT64);
    }

    /**
     * Returns the sent date of the Message.
     * 
     * @return Sent Date.
     * @throws IOException
     */
    public long getSentDate() throws IOException {
        return (Long) getAttribute(AttributeId.attr_date,
                MessageAttribute.ATTR_UNIT64);
    }

    /**
     * Returns the Expiration Time of the Message.
     * 
     * @return Expiration Time.
     * @throws IOException
     */
    public long getExpirationTime() throws IOException {
        return (Long) getAttribute(AttributeId.attr_expirationTime,
                MessageAttribute.ATTR_UNIT64);
    }

    /**
     * Returns the First seen time of the Message.
     * 
     * @return First Seen Time.
     * @throws IOException
     */
    public long getFirstSeenTime() throws IOException {
        return (Long) getAttribute(AttributeId.attr_firstSeenTime,
                MessageAttribute.ATTR_UNIT64);
    }

    /**
     * Returns the First Accessed Time of the Message.
     * 
     * @return First Accessed Time
     * @throws IOException
     */
    public long getFirstAccessedTime() throws IOException {
        return (Long) getAttribute(AttributeId.attr_firstAccessedTime,
                MessageAttribute.ATTR_UNIT64);
    }

    /**
     * Returns the last Accessed Time of the Message.
     * 
     * @return Last Accessed Time.
     * @throws IOException
     */
    public long getLastAccessedTime() throws IOException {
        return (Long) getAttribute(AttributeId.attr_lastAccessedTime,
                MessageAttribute.ATTR_UNIT64);
    }

    /**
     * Returns the Size of the Message.
     * 
     * @return Size of the Message.
     * @throws IOException
     */
    public long getSize() throws IOException {
        return (Long) getAttribute(AttributeId.attr_size,
                MessageAttribute.ATTR_UNIT64);
    }

    /**
     * Returns the Message Flags of the Message.
     * 
     * @return Message Flags
     * @throws IOException
     */
    public boolean[] getMessageFlags() throws IOException {
        int flags = (Integer) getAttribute(AttributeId.attr_msgFlags,
                MessageAttribute.ATTR_UNIT32);
        return MsgFlags.getMsgFlags(flags);
    }

    /**
     * Returns the Subject Attribute of the Message.
     * 
     * @return Subject in the Message.
     * @throws IOException
     */
    public String getSubject() throws IOException {
        String value = System
                .getProperty(SystemProperty.msgSubjectBase64Encoded.name());
        if (Boolean.parseBoolean(value)) {
            byte[] subjectBytes = (byte[]) getAttribute(
                    AttributeId.attr_subject, MessageAttribute.ATTR_TYPE_STRING);
            Base64 base64 = new Base64();
            String encodedSubject = new String(base64.encode(subjectBytes));
            return encodedSubject;
        } else {
            return new String((byte[]) getAttribute(AttributeId.attr_subject,
                    MessageAttribute.ATTR_TYPE_STRING));
        }
    }

    /**
     * Returns the Keywords of the Message.
     * 
     * @return
     * @throws IOException
     */
    public String[] getKeywords() throws IOException {
        return (String[]) getAttribute(AttributeId.attr_keywords,
                MessageAttribute.ATTR_VECTOR_STRING);
    }

    /**
     * Returns the MsgType in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String getMsgType() throws IOException {
        String msgType = new String((byte[]) getAttribute(
                AttributeId.attr_type, MessageAttribute.ATTR_TYPE_STRING));
        return msgType;
    }

    /**
     * Returns Multiple Messages in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String getMultipleMessages() throws IOException {
        String multipleMessages = new String((byte[]) getAttribute(
                AttributeId.attr_multipleMsgs,
                MessageAttribute.ATTR_TYPE_STRING));
        return multipleMessages;
    }

    /**
     * Returns the Message Priority in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String getMsgPriority() throws IOException {
        String msgPriority = new String((byte[]) getAttribute(
                AttributeId.attr_priority, MessageAttribute.ATTR_TYPE_STRING));
        return msgPriority;
    }

    /**
     * Returns the To Addresses in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String[] getTo() throws IOException {
        return (String[]) getAttribute(AttributeId.attr_to,
                MessageAttribute.ATTR_VECTOR_STRING);
    }

    /**
     * Returns the CC Addresses in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String[] getCc() throws IOException {
        return (String[]) getAttribute(AttributeId.attr_cc,
                MessageAttribute.ATTR_VECTOR_STRING);
    }

    /**
     * Returns the References in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String getReferences() throws IOException {
        String references = new String((byte[]) getAttribute(
                AttributeId.attr_references, MessageAttribute.ATTR_TYPE_STRING));
        return references;
    }

    /**
     * Returns the Sender in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String getSender() throws IOException {
        String sender = new String((byte[]) getAttribute(
                AttributeId.attr_sender, MessageAttribute.ATTR_TYPE_STRING));
        return sender;
    }

    /**
     * Returns the Bcc in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String[] getBcc() throws IOException {
        return (String[]) getAttribute(AttributeId.attr_bcc,
                MessageAttribute.ATTR_VECTOR_STRING);
    }

    /**
     * Returns the ReplyTo in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String[] getReplyTo() throws IOException {
        return (String[]) getAttribute(AttributeId.attr_replyTo,
                MessageAttribute.ATTR_VECTOR_STRING);
    }

    /**
     * Returns the InReplyTo in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String getInReplyTo() throws IOException {
        String inReplyTo = new String((byte[]) getAttribute(
                AttributeId.attr_inReplyTo, MessageAttribute.ATTR_TYPE_STRING));
        return inReplyTo;
    }

    /**
     * Returns the BlobMessageId in the Message.
     * 
     * @return
     * @throws IOException
     */
    public String getBlobMessageId() throws IOException {
        String blobMessageId = new String((byte[]) getAttribute(
                AttributeId.attr_blobMessageId,
                MessageAttribute.ATTR_TYPE_STRING));
        return blobMessageId;
    }

    /**
     * Sets the UID
     * 
     */
    public void setUid(int value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_uid.ordinal()),
                MessageAttribute.ATTR_UNIT32, value);
    }

    /**
     * sets the from address in the message
     * 
     */
    public void setFrom(String value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_from.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Sets the Message UUID
     * 
     */
    public void setMessageUUID(UUID value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_messageUUID.ordinal()),
                MessageAttribute.ATTR_UNIT128, value);
    }

    /**
     * Sets the Bounce Message Attribute of the Message.
     * 
     */
    public void setBounce(boolean value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_bounce.ordinal()),
                MessageAttribute.ATTR_UNIT8, value);
    }

    /**
     * Sets the Deliver NDR Attribute of the Message.
     * 
     */
    public void setDeliveredNDR(boolean value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_deliveredNDR.ordinal()),
                MessageAttribute.ATTR_UNIT8, value);
    }

    /**
     * Sets the Private Attribute Flag of the Message.
     * 
     */
    public void setPrivate(boolean value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_private.ordinal()),
                MessageAttribute.ATTR_UNIT8, value);
    }

    /**
     * Sets the attribute attachments flag of the message.
     * 
     */
    public void setHasAttachment(boolean value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_hasAttachment.ordinal()),
                MessageAttribute.ATTR_UNIT8, value);
    }

    /**
     * Sets the Richmail of the Message.
     * 
     */
    public void setRichMailFlag(int value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_richMailFlag.ordinal()),
                MessageAttribute.ATTR_UNIT32, value);
    }

    /**
     * Sets the Arrival Time of the Message.
     * 
     */
    public void setArrivalTime(long value) throws IOException {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_arrivalTime.ordinal()),
                MessageAttribute.ATTR_UNIT64, value);
    }

    /**
     * Sets the Expiration Time of the Message.
     * 
     */
    public void setExpirationTime(long value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_expirationTime
                .ordinal()), MessageAttribute.ATTR_UNIT64, value);
    }

    /**
     * Sets the First seen time of the Message.
     * 
     */
    public void setFirstSeenTime(long value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_firstSeenTime.ordinal()),
                MessageAttribute.ATTR_UNIT64, value);
    }

    /**
     * Sets the First Accessed Time of the Message.
     * 
     */
    public void setFirstAccessedTime(long value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_firstAccessedTime
                .ordinal()), MessageAttribute.ATTR_UNIT64, value);
    }

    /**
     * Sets the last Accessed Time of the Message.
     * 
     */
    public void setLastAccessedTime(long value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_lastAccessedTime
                .ordinal()), MessageAttribute.ATTR_UNIT64, value);
    }

    /**
     * Sets the Size of the Message.
     * 
     */
    public void getSize(long value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_size.ordinal()),
                MessageAttribute.ATTR_UNIT64, value);
    }

    /**
     * Sets the Message Flags of the Message.
     * 
     */
    public void setMessageFlags(int value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_msgFlags.ordinal()),
                MessageAttribute.ATTR_UNIT32, value);
    }

    /**
     * Sets the Subject Attribute of the Message.
     * 
     */
    public void setSubject(String value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_subject.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Sets the Keywords of the Message.
     * 
     */
    public void setKeywords(String[] value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_keywords.ordinal()),
                MessageAttribute.ATTR_VECTOR_STRING, value);
    }

    /**
     * Sets the MsgType in the Message.
     * 
     */
    public void setMsgType(String value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_type.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Sets the Multiple Messages in the Message.
     * 
     * @return
     * @throws IOException
     */
    public void setMultipleMessages(String value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_multipleMsgs.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Set the Message Priority in the Message.
     * 
     * @return
     * @throws IOException
     */
    public void setMsgPriority(String value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_priority.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Sets the To Addresses in the Message.
     * 
     */
    public void setTo(String[] value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_to.ordinal()),
                MessageAttribute.ATTR_VECTOR_STRING, value);
    }

    /**
     * Sets the CC Addresses in the Message.
     * 
     */
    public void setCc(String[] value) throws IOException {
        setAttribute(Short.valueOf((short) AttributeId.attr_cc.ordinal()),
                MessageAttribute.ATTR_VECTOR_STRING, value);
    }

    /**
     * Sets the References in the Message.
     * 
     */
    public void setReferences(String value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_references.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Sets the Sender in the Message.
     * 
     */
    public void setSender(String value) throws IOException {
        setAttribute(Short.valueOf((short) AttributeId.attr_sender.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Sets the Bcc in the Message.
     * 
     */
    public void getBcc(String[] value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_bcc.ordinal()),
                MessageAttribute.ATTR_VECTOR_STRING, value);
    }

    /**
     * Sets the ReplyTo in the Message.
     * 
     */
    public void setReplyTo(String[] value) {
        setAttribute(Short.valueOf((short) AttributeId.attr_replyTo.ordinal()),
                MessageAttribute.ATTR_VECTOR_STRING, value);
    }

    /**
     * Sets the InReplyTo in the Message.
     * 
     */
    public void getInReplyTo(String value) {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_inReplyTo.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Sets the BlobMessageId in the Message.
     * 
     * @return
     * @throws IOException
     */
    public void setBlobMessageId(String value) throws IOException {
        setAttribute(
                Short.valueOf((short) AttributeId.attr_blobMessageId.ordinal()),
                MessageAttribute.ATTR_TYPE_STRING, value);
    }

    /**
     * Returns the PopDeleted Attribute of the Message.
     * 
     * @return PopDeleted Attribute of the Message
     * @throws IOException
     */
    public Boolean getPopDeleted() throws IOException {
        return (((Byte) getAttribute(AttributeId.attr_specialDeleted,
                MessageAttribute.ATTR_UNIT8)) != 0);
    }

    /**
     * Returns the Folder UUID
     * 
     * @return Folder UUID
     * @throws IOException
     */
    public UUID getFolderUUID() throws IOException {
        return (UUID) getAttribute(AttributeId.attr_folderUUID,
                MessageAttribute.ATTR_UNIT128);
    }

    /**
     * @return the setAttributes
     */
    public Map<Short, MessageAttribute> getSetAttributes() {
        return setAttributes;
    }

    /**
     * A debugging method to dump the contents of this object to a console
     * 
     * @throws IOException
     * 
     */
    public void dump() throws IOException {
        System.out.println("DUMPING MESSAGEMETADATA");
        System.out.println("===============");

        Iterator<Short> itr = receivedAttributes.keySet().iterator();
        while (itr.hasNext()) {
            Short key = itr.next();
            AttributeId id = AttributeId.valueOf(key);
            if (id == null) {
                System.out.println("Id == null!");
                break;
            }
            System.out.println("Attributes key " + id.name());
            switch (id) {
            case attr_messageUUID:
                System.out.println("msgUUID: "
                        + this.getMessageUUID().toString());
                break;
            case attr_bounce:
                System.out.println("bounce: " + this.getBounce());
                break;
            case attr_deliveredNDR:
                System.out
                        .println("msgDeliveredNDR: " + this.getDeliveredNDR());
                break;
            case attr_private:
                System.out.println("msgPrivate: " + this.getPrivate());
            case attr_hasAttachment:
                System.out.println("attachmentOrNot: "
                        + this.getHasAttachment());
                break;
            case attr_richMailFlag:
                System.out.println("rmFlag: " + this.getRichMailFlag());
                break;
            case attr_arrivalTime:
                System.out.println("arrivalTime: " + this.getArrivalTime());
                break;
            case attr_expirationTime:
                System.out.println("msgExpirationSeconds: "
                        + this.getExpirationTime());
                break;
            case attr_firstSeenTime:
                System.out.println("msgFirstSeenSeconds: "
                        + this.getFirstSeenTime());
                break;
            case attr_firstAccessedTime:
                System.out.println("timeFirstAccessed: "
                        + this.getFirstAccessedTime());
                break;
            case attr_lastAccessedTime:
                System.out.println("timeLastAccessed: "
                        + this.getLastAccessedTime());
                break;
            case attr_size:
                System.out.println("msgSize: " + this.getSize());
                break;
            case attr_uid:
                System.out.println("uid: " + this.getUid());
                break;
            case attr_msgFlags:
                System.out.println("msgFlags: "
                        + Arrays.toString(this.getMessageFlags()));
                break;
            case attr_keywords:
                System.out.println("keywords: "
                        + Arrays.toString(this.getKeywords()));
                break;
            case attr_type:
                System.out.println("msgType: " + this.getMsgType());
                break;
            case attr_priority:
                System.out.println("msgPriority: " + this.getMsgPriority());
                break;
            case attr_multipleMsgs:
                System.out.println("multipleMsgs: "
                        + this.getMultipleMessages());
                break;
            case attr_subject:
                System.out.println("subject: " + this.getSubject());
                break;
            case attr_from:
                System.out.println("from: " + this.getFrom());
                break;
            case attr_to:
                System.out.println("to: " + Arrays.toString(this.getTo()));
                break;
            case attr_cc:
                System.out.println("cc: " + Arrays.toString(this.getCc()));
                break;
            case attr_date:
                System.out.println("sentDate: " + this.getSentDate());
                break;
            case attr_references:
                System.out.println("references: " + this.getReferences());
                break;
            case attr_sender:
                System.out.println("sender: " + this.getSender());
                break;
            case attr_bcc:
                System.out.println("bcc: " + Arrays.toString(this.getBcc()));
                break;
            case attr_replyTo:
                System.out.println("replyTo: "
                        + Arrays.toString(this.getReplyTo()));
                break;
            case attr_inReplyTo:
                System.out.println("inReplyTo: " + this.getInReplyTo());
                break;
            case attr_blobMessageId:
                System.out.println("blobMessageId: " + this.getBlobMessageId());
                break;
            case attr_specialDeleted:
                System.out.println("popDeleted: " + this.getPopDeleted());
                break;
            case attr_folderUUID:
                System.out.println("folderUUID: " + this.getFolderUUID());
                break;
            default:
                break;
            }
        }
        System.out.println("==== DONE MESSAGEMETADATA =========");
        System.out.println();
    }
}