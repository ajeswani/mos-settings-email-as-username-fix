/*
 * BodyTagSupport.java
 *
 * Copyright (c) 2002 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/jsp/tagext/BodyTagSupport.java#1 $
 */

package com.opwvmsg.utils.paf.jsp.tagext;

import javax.servlet.jsp.JspTagException;


/**
 * An Openwave wrapper for the <code>BodyTagSupport</code> class.
 *
 * @author Mark Abbott
 * @author Bill Wohler
 * @version $Revision: #1 $
 */
public class BodyTagSupport extends javax.servlet.jsp.tagext.BodyTagSupport {
    /**
     * Tests whether the given attribute is set.
     *
     * @param attribute attribute to test
     *
     * @return <code>true</code> if the given attribute is non-null and
     * non-empty.
     */
    protected boolean isSet(String attribute) {
        return attribute != null && attribute.length() > 0;
    }

    /**
     * Maps scope attribute to PageContext scope values.
     *
     * @param scope variable scope.
     * @return PageContext scope.
     *
     * @throws JspTagException if the scope has an invalid value
     */
    protected int getVarScope(String scope) throws JspTagException {
        if (scope == null) {
            throw new JspTagException("invalid scope value: null");
        }
        Scope sc = Scope.forName(scope);
        if (sc == null) {
            throw new JspTagException("invalid scope value: " + scope);
        }
        return sc.getPageContextConstant();
    }
}
