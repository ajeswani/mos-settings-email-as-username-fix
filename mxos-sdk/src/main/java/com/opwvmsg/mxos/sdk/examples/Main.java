package com.opwvmsg.mxos.sdk.examples;

import java.util.List;
import java.util.Properties;

import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;

public class Main {
    public Main(String a) {}

    private static String cosId = "samplecos555";
    private static String domain = "openwave555.com";
    private static String email = "sampleuser555@" + domain;
    private static String alias = "sampleuser666@" + domain;
    private static String password = "test555";

    public static void main(String[] args) throws Exception {

        if (args == null || args.length < 1 || "".equals(args[0])) {
            System.out.println("Usage : Main <contextId>");
            System.out.println("Where <contextId> is : STUB | REST");
            System.exit(-1);
        }
        String contextId = "MXOS-SDK-2.0";
        Properties p = loadProperties(args[0]);
        IMxOSContext context = MxOSContextFactory.getInstance().getContext(
                contextId, p);

        // Delete existing Objects
        try {
            MailboxAPI.delete(context, email);
        } catch (Exception e) {
        }
        try {
            DomainAPI.delete(context, domain);
        } catch (Exception e) {
        }
        try {
            CosAPI.delete(context, cosId);
        } catch (Exception e) {
        }

        // Domain APIs
        DomainAPI.create(context, domain, "local");
        Domain domainObj = DomainAPI.read(context, domain);
        System.out.println("Domain : " + domainObj);
        // DomainAPI.delete(context, domain);

        // Cos APIs
        CosAPI.create(context, cosId);
        Base base = CosAPI.readBase(context, cosId);
        System.out.println("Cos Base " + base);
        CosAPI.updateBase(context, cosId, 1000);
        base = CosAPI.readBase(context, cosId);
        System.out.println("Cos Base " + base);
        // CosAPI.delete(context, cosId);

        // Mailbox APIs
        MailboxAPI.create(context, email, password, cosId);
        Base mailboxBase = MailboxAPI.readBase(context, email);
        System.out.println("Mailbox Base Obj :" + mailboxBase);
        MailboxAPI.updateBase(context, email, 15);
        mailboxBase = MailboxAPI.readBase(context, email);
        System.out.println("Updated Mailbox Base Obj :" + mailboxBase);
        MailboxAPI.addAllowedDomain(context, email, domain);
        List<String> allowedDomains = MailboxAPI.listAllowedDomains(context,
                email);
        System.out.println("Mailbox allowedDomains :"
                + allowedDomains.toString());
        MailboxAPI.addAlias(context, email, alias);
        List<String> aliases = MailboxAPI.listAliases(context, email);
        System.out.println("Mailbox Aliases :" + aliases.toString());
        // MailboxAPI.delete(context, email);

        // Folder APIs
        String folderName = "TestFolder";
        FolderAPI.readAll(context, email);
        FolderAPI.create(context, email, folderName);
        Folder folder = FolderAPI.read(context, email, folderName);
        System.out.println("Folder :" + folder);
        // FolderAPI.delete(context, email, folderName);

        // Message APIs
        String receivedFrom = "test@test.com";
        String message = "This is a sample test message";
        MessageAPI.create(context, email, folderName, receivedFrom, message);

        System.out.println("End of Example...!");
    }

    public static Properties loadProperties(String contextId) {
        // To get MxOS Context object
        Properties p = new Properties();
        // for STUB context
        if (contextId.equalsIgnoreCase("STUB")) {
            p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                    ContextEnum.STUB.name());
        } else if (contextId.equalsIgnoreCase("REST")) {
            // for REST context
            p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                    ContextEnum.REST.name());
            // mxos url
            p.setProperty(ContextProperty.MXOS_BASE_URL,
                    "http://localhost:8080/mxos");
            // no. of connections
            p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "50");
        } else {
            throw new RuntimeException("Invalid <ContextType>");
        }
        return p;
    }
}
