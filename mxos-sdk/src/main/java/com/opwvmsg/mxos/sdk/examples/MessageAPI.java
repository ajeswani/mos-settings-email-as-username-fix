/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.sdk.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;

/**
 * Utility class for Message APIs.
 * 
 * @author mxos-dev
 */
public class MessageAPI {

    /**
     * Create message.
     * 
     * @param context mxosContext
     * @param email email
     * @param folderName folderName
     * @param receivedFrom receivedFrom
     * @param message message
     * @throws MxOSException on any error
     */
    public static String create(final IMxOSContext context, final String email,
            final String folderName, final String receivedFrom,
            final String message) throws MxOSException {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(MailboxProperty.email.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.email.name()).add(email);
        inputParams.put(FolderProperty.folderName.name(),
                new ArrayList<String>());
        inputParams.get(FolderProperty.folderName.name()).add(folderName);
        inputParams.put(MxOSPOJOs.receivedFrom.name(), new ArrayList<String>());
        inputParams.get(MxOSPOJOs.receivedFrom.name()).add(receivedFrom);
        inputParams.put(MxOSPOJOs.message.name(), new ArrayList<String>());
        inputParams.get(MxOSPOJOs.message.name()).add(message);
        IMessageService messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        return messageService.create(inputParams);
    }
}
