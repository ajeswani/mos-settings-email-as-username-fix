/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sdk.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.domain.IDomainService;
/**
 * Utility class for Domain APIs.
 *
 * @author mxos-dev
 */
public class DomainAPI {

    /**
     * Create Domain.
     * @param context mxosContext
     * @param domain domain
     * @param type type
     * @throws MxOSException on any error
     */
    public static void create(final IMxOSContext context, final String domain,
            final String type) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(DomainProperty.domain.name(), new ArrayList<String>());
        requestMap.get(DomainProperty.domain.name()).add(domain);

        requestMap.put(DomainProperty.type.name(), new ArrayList<String>());
        requestMap.get(DomainProperty.type.name()).add(type);

        IDomainService domainService = (IDomainService) context
                .getService(ServiceEnum.DomainService.name());
        domainService.create(requestMap);
        System.out.println("Domain Created...\n");
    }


    /**
     * Read Domain.
     * @param context mxosContext
     * @param domain domain
     * @return Domain object
     * @throws MxOSException on any error
     */
    public static Domain read(final IMxOSContext context, final String domain)
            throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(DomainProperty.domain.name(), new ArrayList<String>());
        requestMap.get(DomainProperty.domain.name()).add(domain);

        IDomainService domainService = (IDomainService) context
                .getService(ServiceEnum.DomainService.name());
        Domain obj = domainService.read(requestMap);
        System.out.println("Domain Read...\n");
        return obj;
    }


    /**
     * Update Domain.
     * @param context mxosContext
     * @param domain domain
     * @param defaultMailbox defaultMailbox
     * @throws MxOSException on any error
     */
    public static void update(final IMxOSContext context, final String domain,
            final String defaultMailbox) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(DomainProperty.domain.name(), new ArrayList<String>());
        requestMap.get(DomainProperty.domain.name()).add(domain);

        requestMap.put(DomainProperty.defaultMailbox.name(),
                new ArrayList<String>());
        requestMap.get(DomainProperty.defaultMailbox.name()).add(
                defaultMailbox);

        IDomainService domainService = (IDomainService) context
                .getService(ServiceEnum.DomainService.name());
        domainService.update(requestMap);

        System.out.println("Domain Updated...\n");
    }


    /**
     * Delete Domain.
     * @param context mxosContext
     * @param domain domain
     * @throws MxOSException on any error
     */
    public static void delete(final IMxOSContext context, final String domain)
            throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(DomainProperty.domain.name(), new ArrayList<String>());
        requestMap.get(DomainProperty.domain.name()).add(domain);

        IDomainService domainService = (IDomainService) context
                .getService(ServiceEnum.DomainService.name());
        domainService.delete(requestMap);

        System.out.println("Domain Deleted...\n");
    }

}
