/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sdk.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedDomainService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
/**
 * Utility class for Mailbox APIs.
 *
 * @author mxos-dev
 */
public class MailboxAPI {

    /**
     * Create Mailbox.
     * @param context mxosContext
     * @param email email
     * @throws MxOSException on any error
     */
    public static void create(final IMxOSContext context, final String email,
            String password, String cosId) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        requestMap
                .put(MailboxProperty.password.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.password.name()).add(password);

        requestMap.put(MailboxProperty.cosId.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.cosId.name()).add(cosId);

        IMailboxService service = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());

        service.create(requestMap);

        System.out.println("Mainbox Created...\n");
    }

    /**
     * Read Mailbox Base.
     * @param context mxosContext
     * @param email email
     * @return Mailbox Base object
     * @throws MxOSException on any error
     */
    public static Base readBase(final IMxOSContext context, final String email)
            throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        IMailboxBaseService service = (IMailboxBaseService) context
                .getService(ServiceEnum.MailboxBaseService.name());

        Base obj = service.read(requestMap);

        System.out.println("Mainbox Base Read...\n");
        return obj;
    }

    /**
     * Update Mailbox Base.
     * @param context mxosContext
     * @param email email
     * @param maxNumAliases maxNumAliases
     * @throws MxOSException on any error
     */
    public static void updateBase(final IMxOSContext context,
            final String email, int maxNumAliases) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        requestMap.put(MailboxProperty.maxNumAliases.name(),
                new ArrayList<String>());
        requestMap.get(MailboxProperty.maxNumAliases.name()).add(
                String.valueOf(maxNumAliases));

        IMailboxBaseService service = (IMailboxBaseService) context
                .getService(ServiceEnum.MailboxBaseService.name());

        service.update(requestMap);

        System.out.println("Mainbox Base Updated...\n");
    }

    /**
     * Add Allowed Domain.
     * @param context mxosContext
     * @param email email
     * @param emailAlias emailAlias
     * @throws MxOSException on any error
     */
    public static void addAllowedDomain(final IMxOSContext context,
            final String email, String allowedDomain) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        requestMap.put(MailboxProperty.allowedDomain.name(),
                new ArrayList<String>());
        requestMap.get(MailboxProperty.allowedDomain.name()).add(
                String.valueOf(allowedDomain));

        IAllowedDomainService service = (IAllowedDomainService) context
                .getService(ServiceEnum.AllowedDomainService.name());
        service.create(requestMap);

        System.out.println("Mainbox Allowed Domain Added...\n");
    }

    /**
     * Add Email Alias.
     * @param context mxosContext
     * @param email email
     * @param emailAlias emailAlias
     * @throws MxOSException on any error
     */
    public static void addAlias(final IMxOSContext context,
            final String email, String emailAlias) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        requestMap.put(MailboxProperty.emailAlias.name(),
                new ArrayList<String>());
        requestMap.get(MailboxProperty.emailAlias.name()).add(
                String.valueOf(emailAlias));

        IEmailAliasService service = (IEmailAliasService) context
                .getService(ServiceEnum.MailAliasService.name());
        service.create(requestMap);

        System.out.println("Mainbox Alias Added...\n");
    }

    /**
     * Add Email Alias.
     * @param context mxosContext
     * @param email email
     * @return List of Aliases
     * @throws MxOSException on any error
     */
    public static List<String> listAllowedDomains(final IMxOSContext context,
            final String email) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        IEmailAliasService service = (IEmailAliasService) context
                .getService(ServiceEnum.MailAliasService.name());
        List<String> l = service.read(requestMap);

        System.out.println("Mainbox List Allowed Domains...\n");
        
        return l;
    }

    /**
     * Add Email Alias.
     * @param context mxosContext
     * @param email email
     * @return List of Aliases
     * @throws MxOSException on any error
     */
    public static List<String> listAliases(final IMxOSContext context,
            final String email) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        IEmailAliasService service = (IEmailAliasService) context
                .getService(ServiceEnum.MailAliasService.name());
        List<String> l = service.read(requestMap);
        System.out.println("Mainbox List Aliases...\n");        
        return l;
    }

    /**
     * Delete Mailbox.
     * @param context mxosContext
     * @param email email
     * @throws MxOSException on any error
     */
    public static void delete(final IMxOSContext context, final String email)
            throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        IMailboxService service = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());

        service.delete(requestMap);

        System.out.println("Mainbox Deleted...\n");
    }
}
