Openwave Messaging MxOS 2.0 - SDK Examples

 This document outlines the examples of mOS SDK.

* Downloads

 Download the below from http://home.openwave.com/~devbuild/repository/com/opwvmsg/mxos/mxos-sdk/\<version\>

 [[1]] mOS SDK (Zip file with all dependant jars) mxos-sdk-\<version\>.zip.
 
 [[2]] mOS SDK Example sources files mxos-sdk-\<version\>-sources.jar.
 
* Working with Examples

 [[1]] Extract the mOS Zip file to a local path
 
 [[2]] Keep all the jars into CLASSPATH
 
 [[3]] Run "com.opwvmsg.mxos.sdk.examples.Main \<Context\>"  where \<Context\> is STUB or REST
 
 [[4]] Refer mOS SDK Example sources files (mxos-sdk-\<version\>.zip) for more information.
 

* For maven users, add the below mxos-sdk dependancy in your pom.xml  
 
+-- 
        <dependency>
            <groupId>com.opwvmsg.mxos</groupId>
            <artifactId>mxos-sdk</artifactId>
            <version>${mxos.version}</version>
        </dependency> 
+--