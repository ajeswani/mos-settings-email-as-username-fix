/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail Receipt filter operations interface which will be exposed to the client.
 * This interface is responsible for doing mailReceipt filter related operations
 * (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface ICosMailReceiptFiltersService {

    /*
     * This operation is responsible for reading mailReceipt filters.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
	 *     Example: bs000123@openwave.com
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>firstName</b>
	 *     <i>Subscriber's first name.</i>
	 *     Type: String, Empty Allowed: No, Minimum Length: 1, Maximum Length: 255
	 *     Example: Fooooo
	 * </pre>
	 *  
	 * @return returns list of Filters of MailReceipt.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_CREATE - <i>Unable to perform Mailbox Create operation.</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
	/**
	 * 
	 * NOT SUPPORTED
	 * 
	 */
    Filters read(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
