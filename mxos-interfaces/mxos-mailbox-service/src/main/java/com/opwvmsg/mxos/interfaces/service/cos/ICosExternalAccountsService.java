/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox external account cos operations interface which will be exposed to
 * the client. This interface is responsible for doing mailbox external account
 * cos related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface ICosExternalAccountsService {

    /**
     * This operation is responsible for reading external account cos attrs.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * </pre>
	 *  
	 * @return returns ExternalAccounts POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_EXTERNAL_ACCOUNT_NOT_FOUND - <i>The given Cos External Account does not exists</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    ExternalAccounts read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating external account cos attrs.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 *  
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>externalMailAccountsAllowed</b>
	 *     <i>Option to allow external mail account</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>promptForExternalAccountSync</b>
	 *     <i>Option to prompt for external mail account</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_EXTERNAL_ACCOUNT_NOT_FOUND - <i>The given Cos External Account does not exists</i>
	 * COS_UNABLE_TO_UPDATE - <i>Unable to perform Cos Update operation</i>  
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
