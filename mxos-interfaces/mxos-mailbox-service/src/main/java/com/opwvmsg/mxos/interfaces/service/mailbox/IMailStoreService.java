/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail store operations interface which will be exposed to the client. This
 * interface is responsible for doing mail store related operations (like Read,
 * Update, etc.).
 *
 * @author mxos-dev
 */
public interface IMailStoreService {

    /**
     * This operation is responsible for reading mail store. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 *
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * </pre>
	 *  
	 * @return returns MailStore POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>
	 *
	 * <b>Error Codes:</b>
	 *     MBX_INVALID_LARGE_MAILBOX_PLATFORM_ENABLED - <i>Invalid largeMailboxPlatformEnabled.</i>
	 *     MBX_INVALID_MAX_MESSAGES - <i>Invalid max messages</i>
	 *     MBX_INVALID_MAX_STORAGE_SIZEKB - <i>Invalid max storage size</i>
	 *     MBX_INVALID_MOBILE_MAX_MESSAGES - <i>Invalid mobile max messages.</i>
         *     MBX_INVALID_MOBILE_MAX_STORAGE_SIZEKB - <i>Invalid mobile max storage size.</i>
         *     MBX_INVALID_MAX_MESSAGES_SOFT_LIMIT - <i>Invalid max messages soft limit.</i>
         *     MBX_INVALID_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Invalid max storage size soft limit.</i>
         *     MBX_INVALID_MOBILE_MAX_MESSAGES_SOFT_LIMIT - <i>Invalid mobile max messages soft limit.</i>
         *     MBX_INVALID_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Invalid mobile max storage size soft limit.</i>
         *     MBX_INVALID_QUOTA_WARNING_THRESHOLD - <i>Invalid quota warning threshold</i>
	 *     MBX_INVALID_QUOTA_BOUNCE_NOTIFY - <i>Invalid quota bounce notify</i>
	 *     MBX_INVALID_FOLDER_QUOTA - <i>Invalid folder quota</i>
	 *     MBX_UNABLE_TO_MAILSTORE_GET - <i>Unable to perform MailStore GET operation</i> 
	 *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    MailStore read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail store. It uses one or
     * more actions to do this activity.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 *
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>largeMailboxPlatformEnabled</b>
         *     <i>This attribute specifies whether mailbox is a large mailbox: No -(not a large Mailbox),
                     Yes - (large Mailbox), this attribute is currently supported in 8.4.3.x and not supported in 9.0</i>
         *     Type: String, ENUM ["no", "yes"]
         *     Example: yes
	 *
	 * <b>maxMessages</b>
	 *     <i>Max messages that can exist in mailbox</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 0
	 *     
	 * <b>maxStorageSizeKB</b>
	 *     <i>Mail quota</i>
	 *     Type: long, Minimum : 0, Maximum : 1073741823
	 *     Example: 0
	 *     
	 *  <b>maxMessagesSoftLimit</b>
         *     <i>When the messages reaches the max limit, the messages are deleted till under the softLimit </i>
         *     Type: integer, Minimum : 0, Maximum : 2147483647
         *     Example: 0
         *     
         * <b>maxStorageSizeKBSoftLimit</b>
         *     <i>When the size reaches the max limit, the messages are deleted till under the softLimit </i>
         *     Type: long, Minimum : 0, Maximum : 1073741823
         *     Example: 0
         *     
         *  <b>mobileMaxMessages</b>
         *     <i>Max messages that can exist in a mobile mailbox</i>
         *     Type: integer, Minimum : 0, Maximum : 2147483647
         *     Example: 0
         *     
         * <b>mobileMaxStorageSizeKB</b>
         *     <i>Mobile messages quota size</i>
         *     Type: long, Minimum : 0, Maximum : 1073741823
         *     Example: 0
         *     
         *  <b>mobileMaxMessagesSoftLimit</b>
         *     <i>When the messages reaches the max limit, the messages are deleted till under the softLimit </i>
         *     Type: integer, Minimum : 0, Maximum : 2147483647
         *     Example: 0
         *     
         * <b>mobilemaxStorageSizeKBSoftLimit</b>
         *     <i>When the size reaches the max limit, the messages are deleted till under the softLimit </i>
         *     Type: long, Minimum : 0, Maximum : 1073741823
         *     Example: 0         
	 *     
	 * <b>quotaWarningThreshold</b>
	 *     <i>Percentage of Mailbox Quota, triggers a warning for user</i>
	 *     Type: integer, Minimum : 0, Maximum : 100
	 *     Example: 1
	 *     
	 * <b>quotaBounceNotify</b>
	 *     <i>Specifies whether the MTA delivers a notification message to a message
                          store when a message is rejected because of quota constraints | yes/no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>folderQuota</b>
	 *     <i>Folder quota constraints - Single valued attribute</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: bs000123
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>
	 * <b>Error Codes:</b>
	 *
	 *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
	 *     MBX_INVALID_LARGE_MAILBOX_PLATFORM_ENABLED - <i>Invalid largeMailboxPlatformEnabled.</i>
	 *     MBX_UNABLE_TO_SET_LARGE_MAILBOX_PLATFORM_ENABLED - <i>Unable to set largeMailboxPlatformEnabled.</i>
	 *     MBX_INVALID_MAX_MESSAGES - <i>Invalid max messages</i>
	 *     MBX_UNABLE_TO_SET_MAX_MESSAGES - <i>Unable to set maxMessages</i>
	 *     MBX_INVALID_MAX_STORAGE_SIZEKB - <i>Invalid max storage size</i>
	 *     MBX_UNABLE_TO_SET_MAX_STORAGE_SIZEKB - <i>Unable to set maxStorageSizeKB</i>
	 *     MBX_INVALID_MOBILE_MAX_MESSAGES - <i>Invalid mobile max messages.</i>
         *     MBX_UNABLE_TO_SET_MOBILE_MAX_MESSAGES - <i>Unable to set mobileMaxMessages.</i>
         *     MBX_INVALID_MOBILE_MAX_STORAGE_SIZEKB - <i>Invalid mobile max storage size.</i>
         *     MBX_UNABLE_TO_SET_MOBILE_MAX_STORAGE_SIZEKB - <i>Unable to set mobileMaxStorageSizeKB.</i>
	 *     MBX_INVALID_MAX_MESSAGES_SOFT_LIMIT - <i>Invalid max messages soft limit.</i>
         *     MBX_UNABLE_TO_SET_MAX_MESSAGES_SOFT_LIMIT - <i>Unable to set maxMessagesSoftLimit.</i>
         *     MBX_INVALID_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Invalid max storage size soft limit.</i>
         *     MBX_UNABLE_TO_SET_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Unable to set maxStorageSizeKBSoftLimit.</i>
         *     MBX_INVALID_MOBILE_MAX_MESSAGES_SOFT_LIMIT - <i>Invalid mobile max messages soft limit.</i>
         *     MBX_UNABLE_TO_SET_MOBILE_MAX_MESSAGES_SOFT_LIMIT - <i>Unable to set mobileMaxMessagesSoftLimit.</i>
         *     MBX_INVALID_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Invalid mobile max storage size soft limit.</i>
         *     MBX_UNABLE_TO_SET_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Unable to set mobileMaxStorageSizeKBSoftLimit.</i>
	 *     MBX_INVALID_QUOTA_WARNING_THRESHOLD - <i>Invalid quota warning threshold</i>
	 *     MBX_UNABLE_TO_SET_QUOTA_WARNING_THRESHOLD - <i>Unable to set quotaWarningThreshold</i>
	 *     MBX_INVALID_QUOTA_BOUNCE_NOTIFY - <i>Invalid quota bounce notify</i>
	 *     MBX_UNABLE_TO_SET_QUOTA_BOUNCE_NOTIFY - <i>Unable to set quotaBounceNotify</i>
	 *     MBX_INVALID_FOLDER_QUOTA - <i>Invalid folder quota</i>
	 *     MBX_UNABLE_TO_SET_FOLDER_QUOTA - <i>Unable to set folderQuota</i>
	 * 
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
