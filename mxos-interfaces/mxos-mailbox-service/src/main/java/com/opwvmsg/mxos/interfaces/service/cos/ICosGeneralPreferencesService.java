/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to COS GeneralPreferences Object level operations like Read and
 * Update.
 *
 * @author mxos-dev
 */

public interface ICosGeneralPreferencesService {

    /**
     * This operation is responsible for reading COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 *  
	 * @return returns GeneralPreferences POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_NOT_FOUND - <i>The given cosId does not exist</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    GeneralPreferences read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>charset</b>
	 *     <i>Valid international Character Set</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 257
	 *     Example: Fooooo
	 *     
	 * <b>timezone</b>
	 *     <i>Valid timezone in unix format</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: 443464
	 *          
	 * <b>parentalControlEnabled</b>
	 *     <i>Specifies enable Parental Control or not</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>recycleBinEnabled</b>
	 *     <i>Indicates whether to places deleted messages into the Trash folder 
  				or permanently deletes them immediately</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 *  <b>preferredUserExperience</b>
	 *     <i>pecifies the preferred Web mail application</i>
	 *     Type: String, ENUM ["basic", "rich", "mobile", "tablet"] , Empty Allowed: No
	 *     Example: basic
	 *     
	 *  <b>preferredTheme</b>
	 *     <i>Specifies the subscriber-preferred application variant</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: theme1
	 *     
	 *   <b>webmailMTA</b>
	 *     <i>Specifies the name of the MTA the system uses for the subscriber's mail transport-related operations</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: EX001   
	 *    
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_SET_LOCALE - <i>Unable to perform set locale</i>
	 * MBX_INVALID_CHARSET - <i>Unable to perform set charset, May occur due to Invalid charset in request</i>
	 * MBX_INVALID_TIMEZONE - <i>Unable to perform set timezone, May occur due to Invalid timezone in request</i>
	 * MBX_INVALID_PARENTALCONTROL - <i>Unable to perform set parental control,Invalid parentalControl value, supports either yes or no</i>
	 * MBX_INVALID_PREFERRED_THEME - <i>Unable to perform set preferred Theme, May occur due to Invalid preferredTheme in request</i>
	 * MBX_INVALID_PREFERRED_USER_EXPERIENCE - <i>Unable to perform set preferred UserExperience, May occur due to Invalid preferred UserExperience in request</i>
	 * MBX_INVALID_RECYCLEBIN_ENABLED - <i>Unable to perform set recycleBinTheme, Invalid recycleBinTheme, supports either yes or no</i>
	 * MBX_INVALID_WEBMAILMTA - <i>Unable to perform set webmailMTA, May occur due to Invalid webmailMTA in request</i>
	 * MBX_INVALID_MAILPARENTAL_CONTROL - <i>Invalid data in Ldap for mailparentalcontrol</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
