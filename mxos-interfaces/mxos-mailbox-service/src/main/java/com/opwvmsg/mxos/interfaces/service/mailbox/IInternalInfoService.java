/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail internal info operations interface which will be exposed to the client.
 * This interface is responsible for doing mail internal info related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface IInternalInfoService {

    /**
     * This operation is responsible for reading mail internal info.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return MailInternalInfo POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBXMBX_UNABLE_TO_INTERNALINFO_GET - <i>Unable to perform InternalInfo GET operation.</i>
     * MBXMBX_UNABLE_TO_GET_MSG_EVENT_RECORDS - <i>Unable to perform MessageEventRecords GET operation.</i>
     * MBXMBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBXMBX_NOT_FOUND - <i>The given email does not exist.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    InternalInfo read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail internal info.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>webmailVersion</b>
     *     <i>Webmail Version.</i>
     *     Type: String
     *     Regex Pattern: ^[a-zA-Z0-9.]*$
     * <b>selfCareAccessEnabled</b>
     *     <i>Self Care Access Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>selfCareSSLAccessEnabled</b>
     *     <i>Self Care SSL Access Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>messageStoreHost</b>
     *     <i>Message Store Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255, EmptyAllowed: No
     *     Regex Pattern:^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>smtpProxyHost</b>
     *     <i>SMPT Proxy Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>popProxyHost</b>
     *     <i>POP Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>autoReplyHost</b>
     *     <i>Auto Reply Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>mailRealm</b>
     *     <i>Realm.</i>
     *     Type: String, Maximum Length: 255, EmptyAllowed: Yes
     * <b>nonEmptyMailRealm</b>
     *     <i>Realm.</i>
     *     Type: String, Maximum Length: 255, EmptyAllowed: No   
     * <b>imapProxyHost</b>
     *     <i>IMAP Proxy Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>imapProxyAuthenticationEnabled</b>
     *     <i>IMAP Proxy Authentication.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>remoteCallTracingEnabled</b>
     *     <i>Remote Call Tracing Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>interManagerAccessEnabled</b>
     *     <i>Enables account access to the InterManager for the Openwave Email application.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>interManagerSSLAccessEnabled</b>
     *     <i>Enables account access to the SSL-encrypted InterManager for the Openwave Email application.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>voiceMailAccessEnabled</b>
     *     <i>Specifies whether the subscriber account has access to voice message features from the web interface.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>faxMailAccessEnabled</b>
     *     <i>Specifies whether the subscriber account has access to fax features from the web interface.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>ldapUtilitiesAccessType</b>
     *     <i>Controls account access to standard LDAP services such as imldapsh, ldapadd, ldapmodify, and other LDAP utilities.</i>
     *     Type: String, Enum: "all", "trusted", "none"
     * <b>addressBookProvider</b>
     *     <i>Specifies the subscriber-preferred address book provider.</i>
     *     Type: String, Enum: "mss", "plaxo", "sun", "vox"
     * </pre>
     *     
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_FOUND - <i>The given email does not exist.</i>
     * MBX_INVALID_WEBMAIL_VERSION - <i>Invalid webmailVersion.</i>
     * MBX_INVALID_SELFCARE_ACCESS - <i>Invalid selfcare access.</i>
     * MBX_INVALID_SELFCARE_SSL_ACCESS - <i>Invalid selfcare SSL access.</i>
     * MBX_INVALID_MESSAGE_STORE_HOST - <i>Invalid message store host.</i>
     * MBX_INVALID_OLD_MESSAGE_STORE_HOST - <i>Invalid old message store host.</i>
     * MBX_INVALID_NEW_MESSAGE_STORE_HOST - <i>Invalid new message store host.</i>
     * MBX_INVALID_SMTP_PROXY_HOST - <i>Invalid smpt proxy host.</i>
     * MBX_INVALID_IMAP_PROXY_HOST - <i>Invalid imap proxy host.</i>
     * MBX_INVALID_POP_PROXY_HOST - <i>Invalid pop proxy host.</i>
     * MBX_INVALID_AUTOREPLY_HOST - <i>Invalid autoreply host.</i>
     * MBX_INVALID_MAIL_REALM - <i>Invalid mail realm.</i>
     * MBX_INVALID_IMAP_PROXY_AUTH - <i>Invalid imapProxyAuthenticationEnabled.</i>
     * MBX_INVALID_REMOTE_CALL_TRACING - <i>Invalid remoteCallTracingEnabled.</i>
     * MBX_INVALID_INTER_MANAGER_ACCESS - <i>Invalid interManagerAccessEnabled</i>
     * MBX_INVALID_INTER_MANAGER_SSL_ACCESS - <i>Invalid interManagerSSLAccessEnabled</i>
     * MBX_INVALID_VOICE_MAIL_ACCESS - <i>Invalid voiceMailAccessEnabled.</i>
     * MBX_INVALID_FAX_ACCESS - <i>Invalid faxAccessEnabled.</i>
     * MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE - <i>Invalid ldapUtilitiesAccessType.</i>
     * MBX_INVALID_ADDRESS_BOOK_PROVIDER - <i>Invalid addressBookProvider.</i>
     * MBX_UNABLE_TO_SET_WEBMAIL_VERSION - <i>Unable to set webmailVersion.</i>
     * MBX_UNABLE_TO_SET_SELFCARE_ACCESS - <i>Unable to set selfcare access.</i>
     * MBX_UNABLE_TO_SET_SELFCARE_SSL_ACCESS - <i>Unable to set selfcare access.</i>
     * MBX_UNABLE_TO_SET_MESSAGE_STORE_HOST - <i>Unable to set Message Store host.</i>
     * MBX_UNABLE_TO_SET_SMTP_PROXY_HOST - <i>Unable to set SMTP proxy host.</i>
     * MBX_UNABLE_TO_SET_IMAP_PROXY_HOST - <i>Unable to set IMAP proxy host.</i>
     * MBX_UNABLE_TO_SET_POP_PROXY_HOST - <i>Unable to set POP proxy host.</i>
     * MBX_UNABLE_TO_SET_AUTOREPLY_HOST - <i>Unable to set AutoReply host.</i>
     * MBX_UNABLE_TO_SET_MAIL_REALM - <i>Unable to set MailRealm.</i>
     * MBX_UNABLE_TO_SET_IMAP_PROXY_AUTH - <i>Unable to set imapProxyAuthenticationEnabled.</i>
     * MBX_UNABLE_TO_SET_REMOTE_CALL_TRACING - <i>Unable to set remote call tracing.</i>
     * MBX_UNABLE_TO_GET_MESSAGE_STORE_HOST - <i>Unable to get Message Store host.</i>
     * MBX_UNABLE_TO_SET_INTER_MANAGER_ACCESS - <i>Unable to set interManagerAccessEnabled.</i>
     * MBX_UNABLE_TO_SET_INTER_MANAGER_SSL_ACCESS - <i>Unable to set interManagerSSLAccessEnabled.</i>
     * MBX_UNABLE_TO_SET_VOICE_MAIL_ACCESS - <i>Unable to set voiceMailAccessEnabled.</i>
     * MBX_UNABLE_TO_SET_FAX_ACCESS - <i>Unable to set faxAccessEnabled.</i>
     * MBX_UNABLE_TO_SET_LDAP_UTILITIES_ACCESS_TYPE - <i>Unable to set ldapUtilitiesAccessType.</i>
     * MBX_UNABLE_TO_SET_ADDRESS_BOOK_PROVIDER - <i>Unable to set addressBookProvider.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
