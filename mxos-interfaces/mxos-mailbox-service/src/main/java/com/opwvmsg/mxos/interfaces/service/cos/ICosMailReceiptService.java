/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail receipt operations interface which will be exposed to the client. This
 * interface is responsible for doing mail receipt related operations (like
 * Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface ICosMailReceiptService {

    /**
     * This operation is responsible for reading mail receipt.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * </pre>
	 *  
	 * @return returns MailReceipt POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    MailReceipt read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail receipt.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>receiveMessages</b>
	 *     <i>Controls whether users receive messages</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *     
	 *  <b>forwardingEnabled</b>
	 *     <i>Enables mail forwading by MTA</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *     
	 *  <b>copyOnForward</b>
	 *     <i>Indicates local delivery on or off</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *          
	 * <b>filterHTMLContent</b>
	 *     <i>Whether the application filters (disables) HTML content and/or images when displaying received messages for security purposes</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *     
	 * <b>displayHeaders</b>
	 *     <i>Subscriber-preferred setting that indicates whether the application displays HTML-formatted messages (those containing a text/html message part) as HTML or plain text</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["html", "text"]
	 *     Example: text
	 *     
	 *  <b>webmailDisplayWidth</b>
	 *     <i>Message view width</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["0", "1", "2"]
	 *     Example: 1
	 *          
	 * <b>webmailDisplayFields</b>
	 *     <i>Subscriber-preferred mail-related fields, such as size, date, subject, that the web interface shows when displaying message lists.</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: 1
	 *     
	 * <b>maxMailsPerPage</b>
	 *     <i>Subscriber-preferred number of messages in a message list that the web interface displays on a page</i>
	 *     Type: integer, Minimum Value: 1, Maximum Value: 2147483647, Empty Allowed: No 
	 * 	   Example: 4
	 *     
	 *  <b>previewPaneEnabled</b>
	 *     <i>Specifies whether the email preview pane must be enabled</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *          
	 * <b>ackReturnReceiptReq</b>
	 *     <i>Subscriber-preferred option for how the application responds to return receipt requests from inbound messages</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["always", "ask", "never"]
	 *     Example: always
	 *     
	 *  <b>deliveryScope</b>
	 *     <i>Restricted to sending or receiving mail</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["local", "global"]
	 *     Example: local
	 *     
	 *  <b>autoReplyMode</b>
	 *     <i>Auto reply mode</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["none", "vacation", "reply", "echo"]
	 *     Example: none
	 *          
	 * <b>maxReceiveMessageSizeKB</b>
	 *     <i>Maximum receive message size</i>
	 *     Type: integer, Minimum Value: 0, Maximum Value: 2097151, Empty Allowed: No 
	 * 	   Example: 1000   
	 * 
	 * <b>mobileMaxReceiveMessageSizeKB</b>
         *     <i>Max size in KB of the message that can be received by Mobile.</i>
         *     Type: Integer, Minimum: 0, maximum: 2147483647  
	 *    
	 *  <b>maxNumForwardingAddresses</b>
	 *     <i>Maximum number of forwarding addresses allowed for the cos</i>
	 *     Type: integer, Minimum Val: 1, Maximum Val: 20, Empty Allowed: No
	 *     Example: 14
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_RECEIVE_MESSAGES - <i>Invalid revceive messages</i>
	 * MBX_UNABLE_TO_SET_RECEIVE_MESSAGES - <i>Unable to set receiveMessages</i>
	 * MBX_INVALID_FORWARDING_ENABLED - <i>Invalid forwarding enabled</i>
	 * MBX_UNABLE_TO_SET_FORWARDING_ENABLED - <i>Unable to set forwardingEnabled</i>
	 * MBX_INVALID_COPY_ON_FORWARD - <i>Invalid copyOnForward</i>
	 * MBX_UNABLE_TO_SET_COPY_ON_FORWARD - <i>Unable to set opyOnForward</i>
	 * MBX_INVALID_FILTER_HTML_CONTENT - <i>Invalid filter html content</i>
	 * MBX_UNABLE_TO_SET_FILTER_HTML_CONTENT - <i>Unable to set filterHTMLContent</i>
	 * MBX_INVALID_DISPLAY_HEADERS - <i>Invalid display headers</i>
	 * MBX_UNABLE_TO_SET_DISPLAY_HEADERS - <i>Unable to set displayHeaders</i>
	 * MBX_INVALID_WEBMAIL_DISPLAY_WIDTH - <i>Invalid webmail display width</i>
	 * MBX_UNABLE_TO_SET_WEBMAIL_DISPLAY_WIDTH - <i>Unable to set webmailDisplayWidth</i>
	 * MBX_INVALID_WEBMAIL_DISPLAY_FIELDS - <i>Invalid webmail display fields</i>
	 * MBX_UNABLE_TO_SET_WEBMAIL_DISPLAY_FIELDS - <i>Unable to set webmailDisplayFields</i>
	 * MBX_INVALID_MAX_MAILS_PER_PAGE - <i>Invalid max mails per page</i>
	 * MBX_UNABLE_TO_SET_MAX_MAILS_PER_PAGE - <i>Unable to set maxMailsPerPage</i>
	 * MBX_INVALID_PREVIEW_PANE_ENABLED - <i>Invalid preview pane enabled</i>
	 * MBX_UNABLE_TO_SET_PREVIEW_PANE_ENABLED - <i>Unable to set previewPaneEnabled</i>
	 * MBX_INVALID_ACK_RETURN_RECEIPT_REQ - <i>Invalid ack return receipt req</i>
	 * MBX_UNABLE_TO_SET_ACK_RETURN_RECEIPT_REQ - <i>Unable to set ackReturnReceiptReq</i>
	 * MBX_INVALID_DELIVERY_SCOPE - <i>Invalid delivery scope</i>
	 * MBX_UNABLE_TO_SET_DELIVERY_SCOPE - <i>Unable to set deliveryScope</i>
	 * MBX_INVALID_AUTO_REPLY_MODE - <i>Invalid auto reply mode</i>
	 * MBX_UNABLE_TO_SET_AUTO_REPLY_MODE - <i>Unable to set autoReplyMode</i>
	 * MBX_INVALID_AUTO_REPLY_MESSAGE - <i>Invalid auto reply message</i>
	 * MBX_INVALID_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Invalid max receive message size</i>
	 * MBX_UNABLE_TO_SET_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Unable to set maxReceiveMessageSizeKB</i>
	 * MBX_INVALID_MOBILE_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Invalid mobile max receive message size.</i>
         * MBX_UNABLE_TO_SET_MOBILE_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Unable to set mobileMaxReceiveMessageSizeKB.</i>
	 * MBX_INVALID_MAXNUM_OF_FWDADDRSS - <i>Invalid maximum number of forward addresses</i>
	 * MBX_UNABLE_TO_SET_MAXNUM_OF_FWDADDRSS - <i>Unable to set maximum allowed forward addresses</i>
	 * 
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
