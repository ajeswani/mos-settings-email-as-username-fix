/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox SMS Services operations interface which will be exposed to
 * the client. This interface is responsible for doing mailbox SMS Service
 * related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface ISmsServicesService {

    /**
     * This operation is responsible for reading SMS Services.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * </pre>
	 *  
	 * @return returns SmsServices object of Mailbox.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 * MBX_SMSSERVICES_GET_MISSING_PARAMS - <i>One or more mandatory parameters are missing for SmsServices GET operation</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    SmsServices read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating SMS Services.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>smsServicesAllowed</b>
	 *     <i>Allowed value - yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>smsServicesMsisdn</b>
	 *     <i>SMS Services MSISDN for the email account in international format
 				(should be between 10 to 15 length, start with(+) and only number supported</i>
	 *     Type: String
	 *     Regex Pattern: "^(({0,})|[+]{1}([0-9]{10,15}))$"
	 *     Example: +11234567890
	 *     
	 * <b>smsServicesMsisdnStatus</b>
	 *     <i>Subscriber's Msisdn Status</i>
	 *     Type: String, REGEX ["deactivated", "activated"]
	 *     Example: activated
	 *     
	 * <b>lastSmsServicesMsisdnStatusChangeDate</b>
	 *     <i>Subscriber's Status change date</i>
	 *     Type: Date
	 *     Format: "yyyy-MM-dd'T'HH:mm:ss'Z'"
	 *     Example: 2013-01-01'T'11:11:11'Z'
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_SMS_SERVICES_ALLOWED - <i>Invalid sms services allowed</i>
	 * MBX_UNABLE_TO_SET_SMS_SERVICES_ALLOWED - <i>Unable to set smsServicesAllowed</i>
	 * MBX_INVALID_SMS_SERVICES_MSISDN - <i>Invalid sms services msisdn</i>
	 * MBX_UNABLE_TO_SET_SMS_SERVICES_MSISDN - <i>Unable to set smsServicesMsisdn</i>
	 * MBX_INVALID_SMS_SERVICES_MSISDN_STATUS - <i>Invalid sms services msisdn status</i>
	 * MBX_UNABLE_TO_SET_SMS_SERVICES_MSISDN_STATUS - <i>Unable to set smsServicesMsisdnStatus</i> 
	 * MBX_INVALID_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE - <i>Invalid last sms services msisdn status change date</i>
	 * MBX_UNABLE_TO_SET_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE - <i>Unable to set lastSmsServicesMsisdnStatusChangeDate</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
