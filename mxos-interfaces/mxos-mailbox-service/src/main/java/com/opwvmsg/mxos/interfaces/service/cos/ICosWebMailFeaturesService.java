/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to COS for WebMailFeatures Object level operations
 * like Read, Update.
 *
 * @author mxos-dev
 */
public interface ICosWebMailFeaturesService {

    /**
     * This operation is responsible for reading WebMailFeatures
     * for a COS.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service.</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * </pre>
     *  
     * @return returns Base POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * COS_INVALID_COSID - <i>Invalid cosId.</i>
     * COS_UNABLE_TO_GET - <i>Unable to perform Cos GET operation.</i>
     * COS_NOT_FOUND - <i>The given cosId does not exist.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    WebMailFeatures read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating WebMailFeatures
     * attributes for a COS.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service.</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>businessFeaturesEnabled</b>
     *     <i>Authorization for business users or regular users.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>fullFeaturesEnabled</b>
     *     <i>This states if full feature is enabled or not.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>allowPasswordChange</b>
     *     <i>This attribute specifies whether subscribers have permission to change their WebEdge account password.</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * "COS_INVALID_COSID - <i>Invalid cosId.</i>
     * "COS_NOT_FOUND - <i>The given cosId does not exist.</i>
     * "MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED - <i>Unable to update businessFeaturesEnabled.</i>
     * "MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED - <i>Unable to update fullFeaturesEnabled.</i>
     * MBX_UNABLE_TO_SET_ALLOW_PASSWORD_CHANGE - <i>Unable to update allowPasswordChange</i>
     * MBX_INVALID_ALLOW_PASSWORD_CHANGE - <i>Invalid value for allowPasswordChange</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
