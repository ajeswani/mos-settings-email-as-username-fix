/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail send operations interface which will be exposed to the client. This
 * interface is responsible for doing mail send related operations (like Read,
 * Update, etc.).
 *
 * @author mxos-dev
 */
public interface IMailSendService {

    /**
     * This operation is responsible for reading mail send.
     *
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 *
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * </pre>
	 * 
	 * @return returns MailSend POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>
	 * <b>Error Codes:</b>
	 *
	 * MBX_UNABLE_TO_MAILSEND_GET - <i>Unable to perform MailSend GET operation</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    MailSend read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail send.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 *
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * <b>inputParams-Optional:</b>
	 *
	 * <b>fromAddress</b>
	 *     <i>Specifies an alternative email address for a subscriber. when present,
                this alternate address is used in from header of outgoing messages</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: bs000123
	 *     
	 * <b>futureDeliveryEnabled</b>
	 *     <i>Enables the delayed delivery feature for the current class of service (COS) or user - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>maxFutureDeliveryDaysAllowed</b>
	 *     <i>The maximum number of days that a user can choose to delay a message before it is sent</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 0
	 *     
	 * <b>maxFutureDeliveryMessagesAllowed</b>
	 *     <i>The maximum number of delayed delivery messages a user can have at one time</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 0
	 *     
	 * <b>alternateFromAddess</b>
	 *     <i>Specifies which of the subscriber-specific aliases the subscriber prefers to appear</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: "^({0,})|(([a-zA-Z0-9]+[._-]?){1,}@(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4})$"
	 *     Example: bs000123@openwave.com
	 *     
	 * <b>replyToAddress</b>
	 *     <i>Specifies an alternative email address for a subscriber</i>
	 *     Regex Pattern: "^({0,})|(([a-zA-Z0-9]+[._-]?){1,}@(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4})$"
	 *     Example: bs000123@openwave.com
	 *     
	 * <b>useRichTextEditor</b>
	 *     <i>Specifies the subscriber-preferred default editor for composing messages in the web interface - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>includeOrginalMailInReply</b>
	 *     <i>Include the original message with a reply message - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>originalMailSeperatorCharacter</b>
	 *     <i>Specifies the subscriber-preferred format of original message text when subscribers reply to a message</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: bs000123
	 *     
	 * <b>autoSaveSentMessages</b>
	 *     <i>Specifies whether copies of outgoing messages are saved - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>addSignatureForNewMails</b>
	 *     <i>Enables automatic insertion of signature text at the end of a newly composed message - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>addSignatureInReplyType</b>
	 *     <i>Specifies the subscriber-preferred option to place the personal signature</i>
	 *     Type: String, ENUM ["afterOriginalMessage", "beforeOriginalMessage"]
	 *     Example: afterOriginalMessage
	 *     
	 * <b>signature</b>
         *     <i>Signature text of the mail user</i>
         *     Type: String, MaxLength: 1000
         *     Example: Signature 1
         *     
	 * <b>autoSpellCheckEnabled</b>
	 *     <i>Specifies whether the application automatically performs a spell check on out bound messages - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>confirmPromptOnDelete</b>
	 *     <i>Specifies whether WebEdge prompts for confirmation before deleting a message - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>includeReturnReceiptReq</b>
	 *     <i>Specifies the subscriber-preferred option for whether the application includes a return receipt request in all out bound messages - yes|no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>maxSendMessageSizeKB</b>
	 *     <i>pecifies the maximum send message size</i>
	 *     Type: integer, Minimum : 0, Maximum : 2097151
	 *     Example: 0
	 *     
	 * <b>maxAttachmentSizeKB</b>
	 *     <i>pecifies the maximum attachment size</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 0
	 *     
	 * <b>maxAttachmentsInSession</b>
	 *     <i>Specifies the maximum number of  attachments as a total that a subscriber can hold during simultaneous message compositions</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 0
	 * 
	 * <b>maxAttachmentsToMessage</b>
         *     <i>Specifies the maximum number of files that can be attached to a single message using the WebEdge Web interface</i>
         *     Type: integer, Minimum : 0, Maximum : 2147483647
         *     Example: 0
         *     
	 * <b>maxCharactersPerPage</b>
	 *     <i>Specifies the maximum number of characters per page that the Web interface uses when displaying a message.</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 0
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>
	 * <b>Error Codes:</b>
	 *
	 * MBX_UNABLE_TO_MAILSEND_GET - <i>Unable to perform MailSend GET operation</i>
	 * MBX_INVALID_FROM_ADDRESS - <i>Invalid from address</i>
	 * MBX_UNABLE_TO_SET_FROM_ADDRESS - <i>Unable to perform set fromAddress</i>
	 * MBX_INVALID_FUTURE_DELIVERY_ENABLED - <i>Invalid future delivery enabled</i>
	 * MBX_UNABLE_TO_SET_FUTURE_DELIVERY_ENABLED - <i>Unable to perform set futureDeliveryEnabled</i>
	 * MBX_INVALID_FUTURE_DELIVERY_DAYS - <i>Invalid future delivery days</i>
	 * MBX_UNABLE_TO_SET_FUTURE_DELIVERY_DAYS - <i>Unable to perform set futureDeliveryDays</i>
	 * MBX_INVALID_MAX_FUTURE_DELIVERY_MESSAGES - <i>Invalid max future delivery messages</i>
	 * MBX_UNABLE_TO_SET_MAX_FUTURE_DELIVERY_MESSAGES - <i>Unable to perform set maxFutureDeliveryMessages</i>
	 * MBX_INVALID_ALTERNATE_FROM_ADDRESS - <i>Invalid alternate from address</i>
	 * MBX_UNABLE_TO_SET_ALTERNATE_FROM_ADDRESS - <i>Unable to perform set alternateFromAddress</i>
	 * MBX_INVALID_REPLY_TO_ADDRESS - <i>Invalid reply to address</i>
	 * MBX_UNABLE_TO_SET_REPLY_TO_ADDRESS - <i>Unable to perform set replyToAddress</i>
	 * MBX_INVALID_USE_RICH_TEXT_EDITOR - <i>Invalid use rich text editor</i>
	 * MBX_UNABLE_TO_SET_USE_RICH_TEXT_EDITOR - <i>Unable to perform set useRichTextEditor</i>
	 * MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY - <i>Invalid include original mail in reply</i>
	 * MBX_UNABLE_TO_SET_INCLUDE_ORIGINAL_MAIL_IN_REPLY - <i>Unable to perform set includeOriginalMailInReply</i>
	 * MBX_INVALID_ORIGINAL_MAIL_SEPERATOR_CHARACTER - <i>Invalid original mail seperator character</i>
	 * MBX_UNABLE_TO_SET_ORIGINAL_MAIL_SEPERATOR_CHARACTER - <i>Unable to perform set maxSendMessageSizeKB</i>
	 * MBX_INVALID_AUTO_SAVE_MESSAGES - <i>Invalid auto save messages</i>
	 * MBX_UNABLE_TO_SET_AUTO_SAVE_MESSAGES - <i>Unable to perform set autoSaveMessages</i>
	 * MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS - <i>Invalid add signature for new mails</i>
	 * MBX_UNABLE_TO_SET_ADD_SIGNATURE_FOR_NEW_MAILS - <i>Unable to perform set addSignatureForNewMails</i>
	 * MBX_INVALID_ADD_SIGNATURE_IN_REPLY - <i>Invalid add signature in reply</i>
	 * MBX_UNABLE_TO_SET_ADD_SIGNATURE_IN_REPLY - <i>Unable to perform set addSignatureInReply</i>
	 * MBX_INVALID_AUTO_SPELL_CHECK_ENABLED - <i>Invalid auto spell check enabled</i>
	 * MBX_UNABLE_TO_SET_AUTO_SPELL_CHECK_ENABLED - <i>Unable to perform set autoSpellCheckEnabled</i>
	 * MBX_INVALID_CONFIRM_PROMT_ON_DELETE - <i>Invalid confirn promt on delete</i>
	 * MBX_UNABLE_TO_SET_CONFIRM_PROMT_ON_DELETE - <i>Unable to perform set confirmPromtOnDelete</i>
	 * MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ - <i>Invalid inlcude return receipt req</i>
	 * MBX_UNABLE_TO_SET_INCLUDE_RETURN_RECEIPT_REQ - <i>Unable to perform set includeReturnReceiptReq</i>
	 * MBX_INVALID_MAX_SEND_MESSAGE_SIZE_KB - <i>Invalid max send message size kb</i>
	 * MBX_UNABLE_TO_SET_MAX_SEND_MESSAGE_SIZE_KB - <i>Unable to perform set maxSendMessageSizeKB</i>
	 * MBX_INVALID_MAX_ATTACHEMENT_SIZE_KB - <i>Invalid max attachement size kb</i>
	 * MBX_UNABLE_TO_SET_MAX_ATTACHEMENT_SIZE_KB - <i>Unable to perform set maxAttachmentSizeKB</i>
	 * MBX_INVALID_MAX_ATTACHMENTS_IN_SESSION - <i>Invalid max attachments in session</i>
	 * MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_IN_SESSION - <i>Unable to perform set maxAttachments in session</i>
	 * MBX_INVALID_MAX_ATTACHMENTS_TO_MESSAGE" - <i>Invalid max attachments to message.</i>
         * MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_TO_MESSAGE" - <i>Unable to perform set maxAttachmentsToMessage.</i>
	 * MBX_INVALID_MAX_CHARACTERS_PER_PAGE - <i>Invalid max characters per page</i>
	 * MBX_UNABLE_TO_SET_MAX_CHARACTERS_PER_PAGE - <i>Unable to perform set maxCharactersPerPage</i>
	 * MBX_UNABLE_TO_SET_SIGNATURE - <i>Unable to set signature</i>
     * MBX_INVALID_SIGNATURE - <i>Invalid Signature</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
