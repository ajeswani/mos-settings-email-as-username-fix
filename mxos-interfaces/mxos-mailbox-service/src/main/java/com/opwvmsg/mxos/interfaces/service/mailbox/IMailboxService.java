/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;
import com.opwvmsg.mxos.data.pojos.Mailbox;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox operations interface which will be exposed to the client. This
 * interface is responsible for doing mailbox related operations (like Create,
 * Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IMailboxService {

    /**
     * This operation is responsible for creating mailbox. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * 
     * <b>password</b>
     *     <i>Subscriber's password, By default, the allowed values are strings between 1 and 65 characters,
     *     containing letters, numbers and the following special characters "+", "/", "\", "=", "-".
     *     An operator can control the allowed characters by modifying the password validator configuration
     *     in $MXOS_HOME/var/mxos/config/mxos/schema/attributes/password</i>
     *     Type: String
     *     Regex Pattern: "[a-zA-Z0-9+/=-]{1,31}"
     *     Example: Pa$$W0rD#1234
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service.</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "([a-zA-Z0-9._,=]+)"
     *     Example: default
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>userName</b>
     *     <i>Subscriber's POP Address
     *     Special characters are not allowed in userName. If config parameter storeUserNameAsEmail is set to true,
     *     &#64;domain will be appended to userName. Otherwise it will be stored as it is.</i>
     *     Type: String, Empty Allowed: No, Minimum Length: 4, Maximum Length: 129
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*$"
     *     Example: bs000123
     * 
     * <b>firstName</b>
     *     <i>Subscriber's first name.</i>
     *     Type: String, Empty Allowed: No, Minimum Length: 1, Maximum Length: 255
     *     Example: Alex
     * 
     * <b>lastName</b>
     *     <i>Subscriber's last name.</i>
     *     Type: String, Empty Allowed: No, Minimum Length: 1, Maximum Length: 255
     *     Example: Bob
     * 
     * <b>mailboxId</b>
     *     <i>Subscriber mailbox's ID</i>
     *     Type: Long
     *     Example: 8545124
     * 
     * <b>status</b>
     *     <i>Subscriber status</i>
     *     Type: String
     *     Allowed Values: "active", "maintenance", "suspended", "locked", "deleted", "proxy"
     *     Example: active
     * 
     * <b>passwordStoreType</b>
     *     <i>Password Type</i>
     *     Type: String
     *     Allowed Values: "clear", "unix", "ssha1", "md5-po", "sha1", "custom1", "custom2", "custom3", "custom4", "custom5"
     *     Example: clear
     * 
     * <b>messageStoreHost</b>
     *     <i>Specifies the name of the host or service cluster</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})"
     *     Example: rwcvmx83c0100
     * 
     * <b>emailAlias</b>
     *     <i>Specifies an alternate email address, Multiple emailAliases in the request are allowed.
     *     should be valid RFC complaint email addresses.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*)@(({0,})|(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@bob.com
     * 
     * <b>lastStatusChangeDate</b>
     *     <i>Last status change date.</i>
     *     Type: Date, Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * 
     * <b>notificationMailSentStatus</b>
     *     <i>Notification sent status</i> allowed values 
     *     Type: String, Enum: "notSent", "sentForInactiveStatus", "sentBeforeSuspendedStatus", "sentForSuspendedStatus", "sentAfterSuspendedStatus", "sentBeforeDeletedStatus"
     * 
     * <b>groupName</b>
     *     <i>Complete group name associated with a groupOfNames object</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$
     *     
     * <b>type</b>
     *     <i>mailbox type</i> allowed values 
     *     Type: String, Enum: "normal", "groupMailbox"
     * 
     * <b>msisdn</b>
     *     <i>MSISDN for the email account in international format, followed by any number number 10 to 15 digits..</i>
     *     Type: String
     *     Regex Pattern: ^(({0,})|[+]{1}([0-9]{10,15}))$
     * 
     * <b>msisdnStatus</b>
     *     <i>Status of the MSISDN.</i>
     *     Type: String, Enum: "deactivated", "activated"    
     * 
     * <b>lastMsisdnStatusChangeDate</b>
     *     <i>Last Msisdn Status Change Date.</i>
     *     Type: Date, Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * 
     * <b>maxNumAliases</b>
     *     <i>Maximum number of aliases allowed for the user.</i>
     *     Type: Integer, Minimum: 1, maximum: 1000
     *     
     * <b>maxNumAllowedDomains</b>
     *     <i>Maximum number of allowed Domain for the user.</i>
     *     Type: Integer, Minimum: 1, maximum: 1000
     * 
     * <b>customFields</b>
     *     <i>Customer specific data - below are the only attributes supported currently.</i>
     *     Format : customFields=bgcFKey:testfkey1
     *     Note: customField's Keys are Case Sensitive, throws GEN_BAD_REQUEST exception if improperly formed.
     * 
     * <b>extensionAttributes</b>
     *     <i>Customer specific optional data - extensionsOptionalCoSAttributes</i>
     *     Note: The extensionsOptionalCoSAttributes is a Config DB key which stores the optional 
     *     attribute key for the ldap. The actual value will be stored in the LDAP using the keys.
     *     : and | characters are not supported as value in extensionAttributes.
     * 
     * <b>businessFeaturesEnabled</b>
     *     <i>Authorization for business users or regular users</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>fullFeaturesEnabled</b>
     *     <i>This states if full feature is enabled or not</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>lastFullFeaturesConnectionDate</b>
     *     <i>Date when the Full feature was accessed</i>
     *     Type: Date
     *     Format: "yyyy-MM-dd'T'HH:mm:ss'Z'"
     *     Example: 2013-01-01'T'11:11:11'Z'
     *     
     * <b>allowPasswordChange</b>
     *     <i>This attribute specifies whether subscribers have permission to change their WebEdge account password.</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     *     
     * <b>maxContacts</b>
     *     <i>Max number of address book entries for a user</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 200
     *     
     * <b>maxGroups</b>
     *     <i>Specifies the maximum number of address book groups that a subscriber may define. </i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 20
     *     
     * <b>maxContactsPerGroup</b>
     *     <i>Specifies the maximum number of email addresses allowed in a single address book entry.</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 20
     *     
     * <b>maxContactsPerPage</b>
     *     <i>Specifies the subscriber-preferred number of address book entries that the web interface displays on a page.</i>
     *     Type: integer, Minimum : 1, Maximum : 2147483647
     *     Example: 50
     *     
     * <b>createContactsFromOutgoingEmails</b>
     *     <i>Specifies the subscriber-preferred method by which the web and WAP interfaces collects recipient email addresses from outgoing messages and adds them to the personal address book.</i>
     *     Type: String, ENUM ["disabled", "forAllAddresses", "onlyForAddressesInToField"]
     *     Example: disabled
     * 
     * <b>passwordStoreType</b>
     *     <i>Setting of only passwordStoreType is not allowed; it can only be set along with password; 
     *     subscriber's password Type allowed values - clear, md5-po, sha1, ssha1, custom1</i>
     *     Type: String, Enum: "clear", "unix", "ssha1", "md5-po", "sha1", "custom1", "custom2", "custom3", "custom4", "custom5"
     * 
     * <b>passwordRecoveryAllowed</b>
     *     <i>Specified whether password recovery enabled, allowed value- yes or no</i>
     *     Type: String, EmptyAllowed: yes, Maximum Length: 255
     * 
     * <b>passwordRecoveryPreference</b>
     *     <i>Indicate password recovery - allowed value- none, email, SMS, All</i>
     *     Type: String, Enum: "none", "email", "sms", "all"
     * 
     * <b>passwordRecoveryMsisdn</b>
     *     <i>Valid msisdn number used for recovery, should be between 10 to 15 length, start with(+) and only number supported</i>
     *     Type: String,  
     *     Regex Pattern: "^(({0,})|[+]{1}([0-9]{10,15}))$"  
     * 
     * <b>passwordRecoveryMsisdnStatus</b>
     *     <i>Indicates whether msisdn recovery is activated or not, allowed values- activated or deactivated</i>
     *     Type: String, Enum: "deactivated", "activated"
     * 
     * <b>lastPasswordRecoveryMsisdnStatusChangeDate</b>
     *     <i>date in format yyyy-MM-dd'T'HH:mm:ss'Z'</i>
     *     Type: date, Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * 
     * <b>passwordRecoveryEmail</b>
     *     <i>Email used in password Recovery, valid email address of format userName@domain</i>
     * 
     * <b>passwordRecoveryEmailStatus</b>
     *     <i>Indicates whether email recovery is activated or not, allowed values- activated or deactivated</i>
     *     Type: String, Enum: "deactivated", "activated"
     * 
     * <b>maxFailedLoginAttempts</b>
     *     <i>Specified maximum number of failed login attempts for a user, allowed value- positive integer</i>
     *     Type: integer, Minimum: 0, maximum: 1000
     * 
     * <b>failedLoginAttempts</b>
     *     <i>Indicates total number of failed login attempts since last successful login</i>
     *     Type: integer, Minimum: 0, maximum: 1000
     *     
     * <b>maxFailedCaptchaLoginAttempts</b>
     *     <i>Specified maximum number of failed captcha login attempts for a user, allowed value- positive integer</i>
     *     Type: integer, Minimum: 0, maximum: 1000
     * 
     * <b>failedCaptchaLoginAttempts</b>
     *     <i>Indicates total number of failed captcha login attempts since last successful login</i>
     *     Type: integer, Minimum: 0, maximum: 1000         
     * 
     * <b>lastLoginAttemptDate</b>
     *     <i>date of last login attempt in format yyyy-MM-dd'T'HH:mm:ss'Z'</i>
     *     Type: date 
     *     Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     *     
     * <b>lastSuccessfulLoginDate</b>
     *     <i>Last successful login date in format yyyy-MM-dd'T'HH:mm:ss'Z'</i>
     *     Type: date 
     *     Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * 
     * <b>locale</b>
     *     <i>Valid supported locale.</i>
     *     Type: String, Maximum Length: 6
     * 
     * <b>charset</b>
     *     <i>Valid international Character Set.</i>
     *     Type: String, Maximum Length: 257
     * 
     * <b>parentalControlEnabled</b>
     *     <i>Enables parental control for an account, allowed value- yes or no.</i>
     *     Type: String
     * 
     * <b>preferredTheme</b>
     *     <i>Specifies the subscriber-preferred application variant.</i>
     *     Type: String, Maximum Length: 256
     * 
     * <b>preferredUserExperience</b>
     *     <i>Specifies the preferred Web mail application.</i>
     *     Type: String, Enum: "basic", "rich", "mobile", "tablet"
     * 
     * <b>recycleBinEnabled</b>
     *     <i>Indicates whether to places deleted messages into the Trash folder 
     *     or permanently deletes them immediately, allowed value - yes or no</i>
     *            Type: String
     * 
     * <b>timezone</b>
     *     <i>Valid timezone in unix format</i>
     *     Type: String, Maximum Length: 255
     * 
     * <b>userThemes</b>
     *     <i>Specifies application variant.</i>
     *     Type: String, Maximum Length: 256
     * 
     * <b>webmailMTA</b>
     *     <i>Specifies the name of the MTA the system uses for the subscriber mail transport-related operations, max-length=20</i>
     *     Type: String
     * 
     * <b>fromAddress</b>
     *     <i>Specifies an alternative email address for a subscriber. when present,
     *     this alternate address is used in from header of outgoing messages</i>
     *     Type: String, Empty Allowed: No, Maximum Length: 255
     *     Example: bs000123
     * 
     * <b>futureDeliveryEnabled</b>
     *     <i>Enables the delayed delivery feature for the current class of service (COS) or user - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>maxFutureDeliveryDaysAllowed</b>
     *     <i>The maximum number of days that a user can choose to delay a message before it is sent</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     * 
     * <b>maxFutureDeliveryMessagesAllowed</b>
     *     <i>The maximum number of delayed delivery messages a user can have at one time</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     * 
     * <b>alternateFromAddess</b>
     *     <i>Specifies which of the subscriber-specific aliases the subscriber prefers to appear</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^({0,})|(([a-zA-Z0-9]+[._-]?){1,}@(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4})$"
     *     Example: bs000123@openwave.com
     * 
     * <b>replyToAddress</b>
     *     <i>Specifies an alternative email address for a subscriber</i>
     *     Regex Pattern: "^({0,})|(([a-zA-Z0-9]+[._-]?){1,}@(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4})$"
     *     Example: bs000123@openwave.com
     * 
     * <b>useRichTextEditor</b>
     *     <i>Specifies the subscriber-preferred default editor for composing messages in the web interface - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>includeOrginalMailInReply</b>
     *     <i>Include the original message with a reply message - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>originalMailSeperatorCharacter</b>
     *     <i>Specifies the subscriber-preferred format of original message text when subscribers reply to a message</i>
     *     Type: String, Empty Allowed: No, Maximum Length: 255
     *     Example: bs000123
     * 
     * <b>autoSaveSentMessages</b>
     *     <i>Specifies whether copies of outgoing messages are saved - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>addSignatureForNewMails</b>
     *     <i>Enables automatic insertion of signature text at the end of a newly composed message - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>addSignatureInReplyType</b>
     *     <i>Specifies the subscriber-preferred option to place the personal signature</i>
     *     Type: String, ENUM ["afterOriginalMessage", "beforeOriginalMessage"]
     *     Example: afterOriginalMessage
     * 
     * <b>signature</b>
     *     <i>Signature text of the mail user</i>
     *     Type: String, MaxLength: 1000
     *     Example: Signature 1
     *     
     * <b>autoSpellCheckEnabled</b>
     *     <i>Specifies whether the application automatically performs a spell check on out bound messages - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>confirmPromptOnDelete</b>
     *     <i>Specifies whether WebEdge prompts for confirmation before deleting a message - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>includeReturnReceiptReq</b>
     *     <i>Specifies the subscriber-preferred option for whether the application includes a return receipt request in all out bound messages - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>numDelayedDeliveryMessagesPending</b>
     *     <i>The Number of messages awaiting future delivery for a user</i>
     *     Type: String, Maximum Length: 255
     *     Format - Time,MessageId
     *     Example: 2011-06-28T17:26:54Z,MessageId     
     * 
     * <b>maxSendMessageSizeKB</b>
     *     <i>pecifies the maximum send message size</i>
     *     Type: integer, Minimum : 0, Maximum : 2097151
     *     Example: 0
     * 
     * <b>maxAttachmentSizeKB</b>
     *     <i>pecifies the maximum attachment size</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     * 
     * <b>maxAttachmentsInSession</b>
     *     <i>Specifies the maximum number of  attachments as a total that a subscriber can hold during simultaneous message compositions</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     *     
     * <b>maxAttachmentsToMessage</b>
     *     <i>Specifies the maximum number of files that can be attached to a single message using the WebEdge Web interface</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0     
     * 
     * <b>maxCharactersPerPage</b>
     *     <i>Specifies the maximum number of characters per page that the Web interface uses when displaying a message.</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     * 
     * <b>mailSendBmiSpamAction</b>
     *     <i>Spam Action</i>
     *     Type: String, ENUM ["discard", "none", "folder deliver", "add header", "subject prefix"]
     *     Example: discard     *
     * 
     * <b>spamMessageIndicator</b>
     *     <i>X-Header or subject prefix if Spam action is Add Header or Subject Prefix</i>
     *     Type: String, Empty Allowed: No, Maximum Length: 255
     *     Example: fooo 
     * 
     * <b>mailSendCommtouchSpamAction</b>
     *     <i>Spam Action</i>
     *     Type: String, ENUM ["discard", "allow", "reject", "tag"]
     *     Example: discard 
     * 
     * <b>mailSendCommtouchVirusAction</b>
     *     <i>Virus Action</i>
     *     Type: String, ENUM ["discard", "allow", "reject", "tag"]
     *     Example: discard 
     * 
     * <b>mailSendMcAfeeVirusAction</b>
     *     <i>Virus Action</i>
     *     Type: String, ENUM ["allow", "clean", "discard", "reject", "repair"]
     *     Example: discard     *
     * 
     * <b>receiveMessages</b>
     *     <i>Controls whether users receive messages.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>forwardingEnabled</b>
     *     <i>Enables mail forwading by MTA.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>copyOnForward</b>
     *     <i>Indicates local delivery on or off</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>forwardingAddress</b>
     *     <i>Subscriber's local delivery email address to be deleted</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com  
     * 
     * <b>maxNumForwardingAddresses</b>
     *     <i>Maximum number of forwarding addresses allowed for the mailbox</i>
     *     Type: integer, Minimum Val: 1, Maximum Val: 20, Empty Allowed: No
     *     Example: 14
     * 
     * <b>filterHTMLContent</b>
     *     <i>Whether the application filters (disables) HTML content and/or images when displaying received messages for security purposes..</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>displayHeaders</b>
     *     <i>Subscriber-preferred setting that indicates whether the application displays HTML-formatted messages (those containing a text/html message part) as HTML or plain text.</i>
     *     Type: String, "html", "text"
     * 
     * <b>webmailDisplayWidth</b>
     *     <i>Message view width.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * 
     * <b>webmailDisplayFields</b>
     *     <i>Subscriber-preferred mail-related fields, such as size, date, subject, that the web interface shows when displaying message lists.</i>
     *     Type: String, Maximum Length: 225, EmptyAllowed: yes
     * 
     * <b>maxMailsPerPage</b>
     *     <i>Subscriber-preferred number of messages in a message list that the web interface displays on a page.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * 
     * <b>previewPaneEnabled</b>
     *     <i>Specifies whether the email preview pane must be enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>ackReturnReceiptReq</b>
     *     <i>Subscriber-preferred option for how the application responds to return receipt requests from inbound messages.</i>
     *     Type: String, Enum: "always", "ask", "never"
     * 
     * <b>deliveryScope</b>
     *     <i>Restricted to sending or receiving mail.</i>
     *     Type: String, Enum: "local", "global"
     * 
     * <b>addressForLocalDelivery</b>
     *     <i>Subscriber's email address for local delivery</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: 123@openwave.com
     * 
     * <b>autoReplyMode</b>
     *     <i>Auto reply mode.</i>
     *     Type: String, Enum: "none", "vacation", "reply", "echo"
     * 
     * <b>autoReplyMessage</b>
     *     <i>Auto reply message.</i>
     *     Type: String, Maximum Length: 225
     * 
     * <b>maxReceiveMessageSizeKB</b>
     *     <i>Maximum receive message size.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     *     
     * <b>mobileMaxReceiveMessageSizeKB</b>
     *     <i>Max size in KB of the message that can be received by Mobile.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647        
     * 
     * <b>senderBlockingAllowed</b>
     *     <i>Sender blocking is allowed or not allowed for the subscriber - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>senderBlockingEnabled</b>
     *     <i>Sender blocking is enabled or disabled for the subscriber - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>rejectAction</b>
     *     <i>Rejection action - drop|forward</i>
     *     Type: String, ENUM ["drop", "forward"]
     *     Example: drop
     * 
     * <b>rejectFolder</b>
     *     <i>Additional information associated with the reject action attribute</i>
     *     Type: String, Maximum Length: 255
     *     Example: ok
     * 
     * <b>allowedSender</b>
     *     <i>Subscriber's allowed sender email address to be added</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * 
     * <b>blockedSender</b>
     *     <i>Subscriber's blocked sender email address to be added</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * 
     * <b>blockSendersPABActive</b>
     *     <i>Sender blocking is active or not active for subscriber - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>blockSendersPABAccess</b>
     *     <i>Sender blocking is enabled or disabled for subscriber - yes|no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>mailReceiptBmiSpamAction</b>
     *     <i>Spam Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * 
     * <b>spamMessageIndicator</b>
     *     <i>X-Header or subject prefix if Spam action is Add Header or Subject Prefix.</i>
     *     Type: String, Empty Allowed: Yes, Maximum Length: 255
     * 
     * <b>spamfilterEnabled</b>
     *     <i>Spam Filter Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>spamPolicy</b>
     *     <i>Spam Policy Type.</i>
     *     Type: String, Enum: "mild", "moderate", "aggressive"
     * 
     * <b>mailReceiptCloudmarkSpamAction</b>
     *     <i>Spam Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * 
     * <b>cleanAction</b>
     *     <i>Clean Action.</i>
     *     Type: String, Enum: "accept", "delete", "quarantine", "tag", "header"
     * 
     * <b>suspectAction</b>
     *     <i>Suspect Action.</i>
     *     Type: String, Enum: "accept", "delete", "quarantine", "tag", "header"
     * 
     * <b>folderName</b>
     *     <i>Folder Name.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern:
     *      "^[a-z, A-Z, 0-9]*$"
     * 
     * <b>tagText</b>
     *     <i>Tag Text.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: 
     *      "^[\\w \\!,\\#-\\/,\\:-\\@,\\[-\\`,\\{-\\~]*$"
     * 
     * <b>headerText</b>
     *     <i>Header Text.</i>
     *     Type: String, Maximum Length: 255  
     *     Regex Pattern: 
     *      "^[\\w \\!,\\#-\\/,\\:-\\@,\\[-\\`,\\{-\\~]*$" 
     * 
     * <b>mailReceiptCommtouchSpamAction</b>
     *     <i>Spam Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * 
     * <b>mailReceiptCommtouchVirusAction</b>
     *     <i>Virus Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * 
     * <b>mailReceiptMcAfeeVirusAction</b>
     *     <i>Virus Action.</i>
     *     Type: String, Enum: "allow", "clean", "discard", "reject", "repair"
     * 
     * <b>sieveFilteringEnabled</b>
     *     <i>Sieve filtering is enabled or disabled for the subscriber.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>mtafilter</b>
     *     <i>MTA filter in mss.</i>
     *     Type: String, Maximum Length: 225, EmptyAllowed: yes
     * 
     * <b>rmfilter</b>
     *     <i>RM filter in mss.</i>
     *     Type: String, Maximum Length: 225, EmptyAllowed: yes
     * 
     * <b>sieveFilterBlockedSender</b>
     *     <i>Sieve blocked senders address to added</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: foo@bar.com
     * 
     * <b>blockedSenderAction</b>
     *     <i>Blocked sender action.</i>
     *     Type: String, Enum: "bounce", "reject", "sideline", "toss", "none"
     * 
     * <b>blockedSenderMessage</b>
     *     <i>Message associated with the denied sender action.</i>
     *     Type: String, Maximum Length: 225
     * 
     * <b>rejectBouncedMessage</b>
     *     <i>Reject bounced message enabled or disabled.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>largeMailboxPlatformEnabled</b>
     *     <i>This attribute specifies whether mailbox is a large mailbox: No -(not a large Mailbox),
                 Yes - (large Mailbox), this attribute is currently supported in 8.4.3.x and not supported in 9.0</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>maxMessages</b>
     *     <i>Max messages that can exist in mailbox</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     * 
     * <b>maxStorageSizeKB</b>
     *     <i>Mail quota</i>
     *     Type: long, Minimum : 0, Maximum : 1073741823
     *     Example: 0
     *     
     * <b>maxMessagesSoftLimit</b>
     *     <i>When the messages reaches the max limit, the messages are deleted till under the softLimit </i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     *     
     * <b>maxStorageSizeKBSoftLimit</b>
     *     <i>When the size reaches the max limit, the messages are deleted till under the softLimit </i>
     *     Type: long, Minimum : 0, Maximum : 1073741823
     *     Example: 0
     *     
     *  <b>mobileMaxMessages</b>
     *     <i>Max messages that can exist in a mobile mailbox</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     *     
     * <b>mobileMaxStorageSizeKB</b>
     *     <i>Mobile messages quota size</i>
     *     Type: long, Minimum : 0, Maximum : 1073741823
     *     Example: 0
     *     
     *  <b>mobileMaxMessagesSoftLimit</b>
     *     <i>When the messages reaches the max limit, the messages are deleted till under the softLimit </i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 0
     *     
     * <b>mobilemaxStorageSizeKBSoftLimit</b>
     *     <i>When the size reaches the max limit, the messages are deleted till under the softLimit </i>
     *     Type: long, Minimum : 0, Maximum : 1073741823
     *     Example: 0       
     * 
     * <b>quotaWarningThreshold</b>
     *     <i>Percentage of Mailbox Quota, triggers a warning for user</i>
     *     Type: integer, Minimum : 0, Maximum : 100
     *     Example: 1
     * 
     * <b>quotaBounceNotify</b>
     *     <i>Specifies whether the MTA delivers a notification message to a message
     *                           store when a message is rejected because of quota constraints | yes/no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>folderQuota</b>
     *     <i>Folder quota constraints - Single valued attribute</i>
     *     Type: String, Empty Allowed: No, Maximum Length: 255
     *     Example: bs000123
     * 
     * <b>socialNetworkIntegrationAllowed</b>
     *     <i>Social Network Integration Allowed</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>smsServicesAllowed</b>
     *     <i>Allowed value - yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>smsServicesMsisdn</b>
     *     <i>SMS Services MSISDN for the email account in international format
     *                                 (should be between 10 to 15 length, start with(+) and only number supported</i>
     *     Type: String
     *     Regex Pattern: "^(({0,})|[+]{1}([0-9]{10,15}))$"
     *     Example: +11234567890
     * 
     * <b>smsServicesMsisdnStatus</b>
     *     <i>Subscriber's Msisdn Status</i>
     *     Type: String, REGEX ["deactivated", "activated"]
     *     Example: activated
     * 
     * <b>lastSmsServicesMsisdnStatusChangeDate</b>
     *     <i>Subscriber's Status change date</i>
     *     Type: Date
     *     Format: "yyyy-MM-dd'T'HH:mm:ss'Z'"
     *     Example: 2013-01-01'T'11:11:11'Z'
     * 
     * <b>smsOnlineEnabled</b>
     *     <i>Allowed values- yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>internationalSMSAllowed</b>
     *     <i>Allowed values- yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>internationalSMSEnabled</b>
     *     <i>Allowed values- yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>maxSMSPerDay</b>
     *     <i>Max number of Online SMS Allowed per user</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 2
     * 
     * <b>concatenatedSMSAllowed</b>
     *     <i>Allowed values- yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>concatenatedSMSEnabled</b>
     *     <i>Allowed values- yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>maxConcatenatedSMSSegments</b>
     *     <i>Max number of concatenated SMS segments per SMS</i>
     *     Type: integer, Minimum : 0, Maximum : 256
     *     Example: 2
     * 
     * <b>maxPerCaptchaSMS</b>
     *     <i>Max number of messages before CAPTCHA is presented</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 2
     * 
     * <b>maxPerCaptchaDurationMins</b>
     *     <i>Max duration (minutes) before CAPTCHA is presented</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 2
     * 
     * <b>smsBasicNotificationsEnabled</b>
     *     <i>Allowed value- yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>smsAdvancedNotificationsEnabled</b>
     *     <i>Allowed value- yes or no</i>
     *     Type: String, ENUM ["no", "yes"]
     *     Example: yes
     * 
     * <b>webmailVersion</b>
     *     <i>Webmail Version.</i>
     *     Type: String
     *     Regex Pattern: ^[a-zA-Z0-9.]*$
     * 
     * <b>selfCareAccessEnabled</b>
     *     <i>Self Care Access Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>selfCareSSLAccessEnabled</b>
     *     <i>Self Care SSL Access Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>messageStoreHost</b>
     *     <i>Message Store Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255, EmptyAllowed: No
     *     Regex Pattern:^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * 
     * <b>smtpProxyHost, </b>
     *     <i>SMPT Proxy Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * 
     * <b>popProxyHost</b>
     *     <i>POP Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * 
     * <b>autoReplyHost</b>
     *     <i>Auto Reply Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * 
     * <b>mailRealm</b>
     *     <i>Realm.</i>
     *     Type: String, Maximum Length: 255, EmptyAllowed: No
     *     
     * <b>imapProxyHost</b>
     *     <i>IMAP Proxy Host Name. It is case-sensitive</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * 
     * <b>imapProxyAuthenticationEnabled</b>
     *     <i>IMAP Proxy Authentication.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>remoteCallTracingEnabled</b> 
     *     <i>Remote Call Tracing Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>interManagerAccessEnabled</b>
     *     <i>Enables account access to the InterManager for the Openwave Email application.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>interManagerSSLAccessEnabled</b>
     *     <i>Enables account access to the SSL-encrypted InterManager for the Openwave Email application.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>voiceMailAccessEnabled</b>
     *     <i>Specifies whether the subscriber account has access to voice message features from the web interface.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>faxMailAccessEnabled</b>
     *     <i>Specifies whether the subscriber account has access to fax features from the web interface.</i>
     *     Type: String, Enum: "no", "yes"
     * 
     * <b>ldapUtilitiesAccessType</b>
     *     <i>Controls account access to standard LDAP services such as imldapsh, ldapadd, ldapmodify, and other LDAP utilities.</i>
     *     Type: String, Enum: "all", "trusted", "none"
     * 
     * <b>addressBookProvider</b>
     *     <i>Specifies the subscriber-preferred address book provider.</i>
     *     Type: String, Enum: "mss", "plaxo", "sun", "vox"
     * 
     * </pre>
     * 
     * @return returns unique identifier for mailbox i.e. mailboxId.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     *     MBX_ALREADY_EXISTS - <i>The given Mailbox is already exists.</i>
     *     MBX_MAILBOX_ID_MISS_MATCH - <i>Mailbox Id in request and LDAP does not match. This error is applicable only if mailboxId is sent in the request and mailbox already exists on LDAP.</i>
     *     MBX_UNABLE_TO_CREATE - <i>Unable to perform Mailbox Create operation.</i>
     *     MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *     MBX_INVALID_PASSWORD - <i>Invalid password, check the password format.</i>
     *     MBX_INVALID_MAILBOXID - <i>Invalid mailboxid.</i>
     *     MBX_INVALID_MAILBOX_STATUS - <i>Invalid account status.</i>
     *     MBX_INVALID_COSID - <i>Invalid cosId.</i>
     *     MBX_INVALID_PASSWORD_TYPE - <i>Invalid password store type.</i>
     *     MBX_INVALID_MESSAGE_STORE_HOST - <i>Invalid message store host.</i>
     *     MBX_ALIAS_NOT_EXIST_IN_ALLOWED_DOMAINS - <i>Given alias does not belong to allowed domains.</i>
     *     MBX_ALIAS_REACHED_MAX_LIMIT - <i>Number of email aliases reached the maximum limit.</i>
     *     MBX_INVALID_ALIAS - <i>Invalid alias.</i>
     *     MBX_INVALID_FROM_ADDRESS - <i>Invalid from address.</i>
     *     MBX_UNABLE_TO_SET_FROM_ADDRESS - <i>Unable to perform set fromAddress.</i>
     *     MBX_INVALID_USERNAME - <i>Invalid username.</i>
     *     MBX_INVALID_MAILBOX_STATUS - <i>Invalid account status.</i>
     *     MBX_INVALID_MAILBOX_STATUS_DATE - <i>Invalid account status change date.</i>
     *     MBX_INVALID_GROUPNAME - <i>Invalid groupName.</i>
     *     MBX_INVALID_MAILBOX_TYPE - <i>Invalid account type.</i>
     *     MBX_INVALID_MSISDN - <i>Invalid MSISDN.</i>
     *     MBX_INVALID_MSISDN_STATUS - <i>Invalid MSISDN Status.</i>
     *     MBX_INVALID_MSISDN_STATUS_DATE - <i>Invalid MSISDN Status change date.</i>
     *     MBX_INVALID_MAXNUM_OF_ALIASES - <i>Invalid maximum number of email aliases.</i>
     *     MBX_UNABLE_TO_GET - <i>Unable to perform Mailbox GET operation.</i>
     *     MBX_UNABLE_TO_SEARCH - <i>Unable to perform Mailbox SEARCH operation.</i>
     *     MBX_UNABLE_TO_SET_USERNAME - <i>Unable to perform set username.</i>
     *     MBX_UNABLE_TO_SET_ACCOUNT_STATUS - <i>Unable to perform set account status.</i>
     *     MBX_UNABLE_TO_SET_ACCOUNT_STATUS_DATE - <i>Unable to perform set account status change date.</i>
     *     MBX_UNABLE_TO_SET_ACCOUNT_TYPE - <i>Unable to perform set account type.</i>
     *     MBX_UNABLE_TO_SET_MSISDN - <i>Unable to perform set account msisdn.</i>
     *     MBX_UNABLE_TO_SET_MSISDN_STATUS - <i>Unable to perform set account msisdn status.</i>     
     *     MBX_UNABLE_TO_SET_MSISDN_STATUS_DATE - <i>Unable to perform set account msisdn status change date.</i>
     *     MBX_UNABLE_TO_SET_MAX_NUM_ALIAS - <i>Unable to perform set maxNumAliases.</i>
     *     MBX_UNABLE_TO_SET_COSID - <i>Unable to perform set account COS id.</i>
     *     MBX_INVALID_MAX_ALLOWEDDOMAIN - <i>Invalid maxAllowedDomain value.</i>
     *     MBX_UNABLE_TO_SET_MAX_ALLOWEDDOMAIN - <i>Unable to perform set maxNumAllowedDomain.</i>
     *     MBX_UNABLE_TO_SET_CUSTOMFIELDS - <i>Unable to perform set CustomFields.</i>
     *     MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED - <i>Unable to set businessFeaturesEnabled</i>
     *     MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED - <i>Unable to set fullFeaturesEnabled</i>
     *     MBX_UNABLE_TO_SET_LAST_FULL_FEATURES_CONN_DATE - <i>Unable to set lastFullFeaturesConnectionDate</i>
     *     MBX_INVALID_LAST_FULL_FEATURES_CONN_DATE - <i>Invalid lastFullFeaturesConnectionDate</i>
     *     GEN_BAD_REQUEST - <i>email parameter  is missing from the request.</i>
     *     MBX_UNABLE_TO_SET_PASSWORD - <i>Unable to perform set password.</i>
     *     MBX_INVALID_PASSWORD - <i>Invalid password, check the password format.</i>
     *     MBX_INVALID_PASSWORD_STORETYPE - <i>Unable to perform set PasswordStoreType, 
     *     Invalid passwordStoreType parameter.</i>
     *     MBX_INVALID_PASSWORD_RECOVERY_ALLOWED - <i>Unable to perform set passwordRecoveryAllowed, 
     *     Invalid passwordRecoveryAllowed parameter.</i>
     *     MBX_INVALID_PASSWORD_RECOVERY_PREFERENCE - <i>Unable to perform set passwordRecoveryPreference, 
     *     Invalid passwordRecoveryPreference parameter.</i>
     *     MBX_INVALID_PASSWORD_RECOVERY_MSISDN - <i>Unable to perform set passwordRecoveryMsisdn, 
     *     Invalid passwordRecoveryMsisdn parameter.</i>
     *     MBX_INVALID_PASSWORD_RECOVERY_MSISDN_STATUS - <i>Unable to perform set passwordRecoveryMsisdnStatus 
     *     Invalid passwordRecoveryMsisdnStatus parameter.</i>
     *     MBX_INVALID_PASS_REC_MSISDN_STATUS_CHANGE_DATE - <i>Unable to perform set 
     *     passwordRecoveryMsisdnStatusChangeDate, Invalid passwordRecoveryMsisdnStatusChangeDate parameter.</i>
     *     MBX_INVALID_PASSWORD_RECOVERY_EMAIL - <i>Unable to perform set passwordRecoveryEmail,
     *     Invalid passwordRecoveryEmail paremeter.</i>
     *     MBX_INVALID_PASSWORD_RECOVERY_EMAIL_STATUS - <i>Unable to perform set passwordRecoveryEmailStatus,
     *     Invalid passwordRecoveryEmailStatus parameter</i>
     *     MBX_INVALID_MAX_FAILED_LOGIN_ATTEMPTS - <i>Unable to perform set maxFailedLoginAttempts,
     *     Invalid maxFailedLoginAttempts parameter.</i>
     *     MBX_INVALID_FAILED_LOGIN_ATTEMPTS - <i>Unable to perform set failedLoginAttempts,
     *     Invalid failedLoginAttempts parameter.</i>
     *     MBX_INVALID_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Invalid failedCaptchaLoginAttempts parameter.</i>
     *     MBX_INVALID_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Invalid maxFailedCaptchaLoginAttempts parameter.</i>
     *     MBX_UNABLE_TO_SET_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Unable to set failedCaptchaLoginAttempts.</i>
     *     MBX_UNABLE_TO_SET_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Unable to set maxFailedCaptchaLoginAttempts.</i> 
     *     MBX_INVALID_LAST_LOGIN_ATTEMPTS_DATE - <i>Unable to perform set lastLoginAttemptDate,
     *     Invalid lastLoginAttemptDate parameter.</i>
     *     MBX_INVALID_LAST_FAILED_LOGIN_DATE - <i>Invalid last failed login time.</i>
     *     MBX_UNABLE_TO_SET_LOCALE - <i>Unable to perform set locale.</i>
     *     MBX_INVALID_CHARSET - <i>Unable to perform set charset, May occur due to Invalid charset in request.</i>
     *     MBX_INVALID_TIMEZONE - <i>Unable to perform set timezone, May occur due to Invalid timezone in request.</i>
     *     MBX_INVALID_PARENTALCONTROL - <i>Unable to perform set parental control,Invalid parentalControl value, supports either yes or no.</i>
     *     MBX_INVALID_PREFERRED_THEME - <i>Unable to perform set preferred Theme, May occur due to Invalid preferredTheme in request.</i>
     *     MBX_INVALID_PREFERRED_USER_EXPERIENCE - <i>Unable to perform set preferred UserExperience, May occur due to Invalid preferred UserExperience in request.</i>
     *     MBX_INVALID_RECYCLEBIN_ENABLED - <i>Unable to perform set recycleBinTheme, Invalid recycleBinTheme, supports either yes or no.</i>
     *     MBX_INVALID_WEBMAILMTA - <i>Unable to perform set webmailMTA, May occur due to Invalid webmailMTA in request.</i>
     *     MBX_INVALID_FROM_ADDRESS - <i>Invalid from address</i>
     *     MBX_UNABLE_TO_SET_FROM_ADDRESS - <i>Unable to perform set fromAddress</i>
     *     MBX_INVALID_FUTURE_DELIVERY_ENABLED - <i>Invalid future delivery enabled</i>
     *     MBX_UNABLE_TO_SET_FUTURE_DELIVERY_ENABLED - <i>Unable to perform set futureDeliveryEnabled</i>
     *     MBX_INVALID_FUTURE_DELIVERY_DAYS - <i>Invalid future delivery days</i>
     *     MBX_UNABLE_TO_SET_FUTURE_DELIVERY_DAYS - <i>Unable to perform set futureDeliveryDays</i>
     *     MBX_INVALID_MAX_FUTURE_DELIVERY_MESSAGES - <i>Invalid max future delivery messages</i>
     *     MBX_UNABLE_TO_SET_MAX_FUTURE_DELIVERY_MESSAGES - <i>Unable to perform set maxFutureDeliveryMessages</i>
     *     MBX_INVALID_ALTERNATE_FROM_ADDRESS - <i>Invalid alternate from address</i>
     *     MBX_UNABLE_TO_SET_ALTERNATE_FROM_ADDRESS - <i>Unable to perform set alternateFromAddress</i>
     *     MBX_INVALID_REPLY_TO_ADDRESS - <i>Invalid reply to address</i>
     *     MBX_UNABLE_TO_SET_REPLY_TO_ADDRESS - <i>Unable to perform set replyToAddress</i>
     *     MBX_INVALID_USE_RICH_TEXT_EDITOR - <i>Invalid use rich text editor</i>
     *     MBX_UNABLE_TO_SET_USE_RICH_TEXT_EDITOR - <i>Unable to perform set useRichTextEditor</i>
     *     MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY - <i>Invalid include original mail in reply</i>
     *     MBX_UNABLE_TO_SET_INCLUDE_ORIGINAL_MAIL_IN_REPLY - <i>Unable to perform set includeOriginalMailInReply</i>
     *     MBX_INVALID_ORIGINAL_MAIL_SEPERATOR_CHARACTER - <i>Invalid original mail seperator character</i>
     *     MBX_UNABLE_TO_SET_ORIGINAL_MAIL_SEPERATOR_CHARACTER - <i>Unable to perform set maxSendMessageSizeKB</i>
     *     MBX_INVALID_AUTO_SAVE_MESSAGES - <i>Invalid auto save messages</i>
     *     MBX_UNABLE_TO_SET_AUTO_SAVE_MESSAGES - <i>Unable to perform set autoSaveMessages</i>
     *     MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS - <i>Invalid add signature for new mails</i>
     *     MBX_UNABLE_TO_SET_ADD_SIGNATURE_FOR_NEW_MAILS - <i>Unable to perform set addSignatureForNewMails</i>
     *     MBX_INVALID_ADD_SIGNATURE_IN_REPLY - <i>Invalid add signature in reply</i>
     *     MBX_UNABLE_TO_SET_ADD_SIGNATURE_IN_REPLY - <i>Unable to perform set addSignatureInReply</i>
     *     MBX_INVALID_AUTO_SPELL_CHECK_ENABLED - <i>Invalid auto spell check enabled</i>
     *     MBX_UNABLE_TO_SET_AUTO_SPELL_CHECK_ENABLED - <i>Unable to perform set autoSpellCheckEnabled</i>
     *     MBX_INVALID_CONFIRM_PROMT_ON_DELETE - <i>Invalid confirn promt on delete</i>
     *     MBX_UNABLE_TO_SET_CONFIRM_PROMT_ON_DELETE - <i>Unable to perform set confirmPromtOnDelete</i>
     *     MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ - <i>Invalid inlcude return receipt req</i>
     *     MBX_UNABLE_TO_SET_INCLUDE_RETURN_RECEIPT_REQ - <i>Unable to perform set includeReturnReceiptReq</i>
     *     MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING - <i>Invalid delayed delivery messages pending<i>
     *     MBX_INVALID_OLD_NUM_DELAYED_DELIVERY_MESSAGES_PENDING - <i>Invalid old delayed delivery messages pending.</i>
     *     MBX_INVALID_NEW_NUM_DELAYED_DELIVERY_MESSAGES_PENDING - <i>Invalid new delayed delivery messages pending.</i>      
     *     MBX_INVALID_MAX_SEND_MESSAGE_SIZE_KB - <i>Invalid max send message size kb</i>
     *     MBX_UNABLE_TO_SET_MAX_SEND_MESSAGE_SIZE_KB - <i>Unable to perform set maxSendMessageSizeKB</i>
     *     MBX_INVALID_MAX_ATTACHEMENT_SIZE_KB - <i>Invalid max attachement size kb</i>
     *     MBX_UNABLE_TO_SET_MAX_ATTACHEMENT_SIZE_KB - <i>Unable to perform set maxAttachmentSizeKB</i>
     *     MBX_INVALID_MAX_ATTACHMENTS_IN_SESSION - <i>Invalid max attachments in session</i>
     *     MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_IN_SESSION - <i>Unable to perform set maxAttachments in session</i>
     *     MBX_INVALID_MAX_ATTACHMENTS_TO_MESSAGE" - <i>Invalid max attachments to message.</i>
     *     MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_TO_MESSAGE" - <i>Unable to perform set maxAttachmentsToMessage.</i>
     *     MBX_INVALID_MAX_CHARACTERS_PER_PAGE - <i>Invalid max characters per page</i>
     *     MBX_UNABLE_TO_SET_MAX_CHARACTERS_PER_PAGE - <i>Unable to perform set maxCharactersPerPage</i>
     *     MBX_INVALID_RECEIVE_MESSAGES - <i>Invalid revceive messages.</i>
     *     MBX_UNABLE_TO_SET_RECEIVE_MESSAGES - <i>Unable to set receiveMessages.</i>
     *     MBX_INVALID_FORWARDING_ENABLED - <i>Invalid forwarding enabled.</i>
     *     MBX_UNABLE_TO_SET_FORWARDING_ENABLED - <i>Unable to set forwardingEnabled.</i>
     *     MBX_INVALID_COPY_ON_FORWARD - <i>Invalid copyOnForward.</i>
     *     MBX_UNABLE_TO_SET_COPY_ON_FORWARD - <i>Unable to set opyOnForward.</i>
     *     MBX_INVALID_FILTER_HTML_CONTENT - <i>Invalid filter html content.</i>
     *     MBX_UNABLE_TO_SET_FILTER_HTML_CONTENT - <i>Unable to set filterHTMLContent.</i>
     *     MBX_INVALID_DISPLAY_HEADERS - <i>Invalid display headers.</i>
     *     MBX_UNABLE_TO_SET_DISPLAY_HEADERS - <i>Unable to set displayHeaders.</i>
     *     MBX_INVALID_WEBMAIL_DISPLAY_WIDTH - <i>Invalid webmail display width.</i>
     *     MBX_UNABLE_TO_SET_WEBMAIL_DISPLAY_WIDTH - <i>Unable to set webmailDisplayWidth.</i>
     *     MBX_INVALID_WEBMAIL_DISPLAY_FIELDS - <i>Invalid webmail display fields.</i>
     *     MBX_UNABLE_TO_SET_WEBMAIL_DISPLAY_FIELDS - <i>Unable to set webmailDisplayFields.</i>
     *     MBX_INVALID_MAX_MAILS_PER_PAGE - <i>Invalid max mails per page.</i>
     *     MBX_UNABLE_TO_SET_MAX_MAILS_PER_PAGE - <i>Unable to set maxMailsPerPage.</i>
     *     MBX_INVALID_PREVIEW_PANE_ENABLED - <i>Invalid preview pane enabled.</i>
     *     MBX_UNABLE_TO_SET_PREVIEW_PANE_ENABLED - <i>Unable to set previewPaneEnabled.</i>
     *     MBX_INVALID_ACK_RETURN_RECEIPT_REQ - <i>Invalid ack return receipt req.</i>
     *     MBX_UNABLE_TO_SET_ACK_RETURN_RECEIPT_REQ - <i>Unable to set ackReturnReceiptReq.</i>
     *     MBX_INVALID_DELIVERY_SCOPE - <i>Invalid delivery scope.</i>
     *     MBX_UNABLE_TO_SET_DELIVERY_SCOPE - <i>Unable to set deliveryScope.</i>
     *     MBX_INVALID_AUTO_REPLY_MODE - <i>Invalid auto reply mode.</i>
     *     MBX_UNABLE_TO_SET_AUTO_REPLY_MODE - <i>Unable to set autoReplyMode.</i>
     *     MBX_INVALID_AUTO_REPLY_MESSAGE - <i>Invalid auto reply message.</i>
     *     MBX_INVALID_MAIL_REALM - <i>Invalid mailRealm.</i>
     *     MBX_UNABLE_TO_SET_MAIL_REALM - <i>Unable to set MailRealm.</i>
     *     MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE - <i>Unable to set autoReplyMessage.</i>
     *     MBX_INVALID_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Invalid max receive message size.</i>
     *     MBX_UNABLE_TO_SET_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Unable to set maxReceiveMessageSizeKB.</i>
     *     MBX_UNABLE_TO_SET_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Unable to set maxReceiveMessageSizeKB.</i>
     *     MBX_INVALID_MOBILE_MAX_RECEIVE_MESSAGE_SIZE_KB - <i>Invalid mobile max receive message size.</i>
     *     MBX_INVALID_LARGE_MAILBOX_PLATFORM_ENABLED - <i>Invalid largeMailboxPlatformEnabled.</i>
     *     MBX_UNABLE_TO_SET_LARGE_MAILBOX_PLATFORM_ENABLED - <i>Unable to set largeMailboxPlatformEnabled.</i>
     *     MBX_INVALID_MAX_MESSAGES - <i>Invalid max messages</i>
     *     MBX_UNABLE_TO_SET_MAX_MESSAGES - <i>Unable to set maxMessages</i>
     *     MBX_INVALID_MAX_STORAGE_SIZEKB - <i>Invalid max storage size</i>
     *     MBX_UNABLE_TO_SET_MAX_STORAGE_SIZEKB - <i>Unable to set maxStorageSizeKB</i>
     *     MBX_INVALID_MOBILE_MAX_MESSAGES - <i>Invalid mobile max messages.</i>
     *     MBX_UNABLE_TO_SET_MOBILE_MAX_MESSAGES - <i>Unable to set mobileMaxMessages.</i>
     *     MBX_INVALID_MOBILE_MAX_STORAGE_SIZEKB - <i>Invalid mobile max storage size.</i>
     *     MBX_UNABLE_TO_SET_MOBILE_MAX_STORAGE_SIZEKB - <i>Unable to set mobileMaxStorageSizeKB.</i>
     *     MBX_INVALID_MAX_MESSAGES_SOFT_LIMIT - <i>Invalid max messages soft limit.</i>
     *     MBX_UNABLE_TO_SET_MAX_MESSAGES_SOFT_LIMIT - <i>Unable to set maxMessagesSoftLimit.</i>
     *     MBX_INVALID_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Invalid max storage size soft limit.</i>
     *     MBX_UNABLE_TO_SET_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Unable to set maxStorageSizeKBSoftLimit.</i>
     *     MBX_INVALID_MOBILE_MAX_MESSAGES_SOFT_LIMIT - <i>Invalid mobile max messages soft limit.</i>
     *     MBX_UNABLE_TO_SET_MOBILE_MAX_MESSAGES_SOFT_LIMIT - <i>Unable to set mobileMaxMessagesSoftLimit.</i>
     *     MBX_INVALID_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Invalid mobile max storage size soft limit.</i>
     *     MBX_UNABLE_TO_SET_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT - <i>Unable to set mobileMaxStorageSizeKBSoftLimit.</i>
     *     MBX_INVALID_QUOTA_WARNING_THRESHOLD - <i>Invalid quota warning threshold</i>
     *     MBX_UNABLE_TO_SET_QUOTA_WARNING_THRESHOLD - <i>Unable to set quotaWarningThreshold</i>
     *     MBX_INVALID_QUOTA_BOUNCE_NOTIFY - <i>Invalid quota bounce notify</i>
     *     MBX_UNABLE_TO_SET_QUOTA_BOUNCE_NOTIFY - <i>Unable to set quotaBounceNotify</i>
     *     MBX_INVALID_FOLDER_QUOTA - <i>Invalid folder quota</i>
     *     MBX_UNABLE_TO_SET_FOLDER_QUOTA - <i>Unable to set folderQuota</i>
     *     MBX_INVALID_SNINTEGRATIONALLOWED - <i>Invalid SocialNetworkIntegrationAllowed value</i>
     *     MBX_INVALID_SMS_SERVICES_ALLOWED - <i>Invalid sms services allowed</i>
     *     MBX_UNABLE_TO_SET_SMS_SERVICES_ALLOWED - <i>Unable to set smsServicesAllowed</i>
     *     MBX_INVALID_SMS_SERVICES_MSISDN - <i>Invalid sms services msisdn</i>
     *     MBX_UNABLE_TO_SET_SMS_SERVICES_MSISDN - <i>Unable to set smsServicesMsisdn</i>
     *     MBX_INVALID_SMS_SERVICES_MSISDN_STATUS - <i>Invalid sms services msisdn status</i>
     *     MBX_UNABLE_TO_SET_SMS_SERVICES_MSISDN_STATUS - <i>Unable to set smsServicesMsisdnStatus</i> 
     *     MBX_INVALID_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE - <i>Invalid last sms services msisdn status change date</i>
     *     MBX_UNABLE_TO_SET_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE - <i>Unable to set lastSmsServicesMsisdnStatusChangeDate</i>
     *     MBX_INVALID_SMS_ONLINE_ENABLED - <i>Invalid sms online enabled</i>
     *     MBX_UNABLE_TO_SET_SMS_ONLINE_ENABLED - <i>Unable to set smsOnlineEnabled</i>
     *     MBX_INVALID_INTERNATIONAL_SMS_ALLOWED - <i>Invalid international sms allowed</i>
     *     MBX_UNABLE_TO_SET_INTERNATIONAL_SMS_ALLOWED - <i>Unable to set internationalSMSAllowed</i>
     *     MBX_INVALID_INTERNALTONAL_SMS_ENABLED - <i>Invalid international sms enabled</i>
     *     MBX_UNABLE_TO_SET_INTERNALTONAL_SMS_ENABLED - <i>Unable to set internationalSMSEnabled</i>
     *     MBX_INVALID_MAX_SMS_PER_DAY - <i>Invalid maximum sms per day</i>
     *     MBX_UNABLE_TO_SET_MAX_SMS_PER_DAY - <i>Unable to set maxSMSPerDay</i>
     *     MBX_INVALID_CONCATENATED_SMS_ALLOWED - <i>Invalid concatenated sms allowed</i>
     *     MBX_UNABLE_TO_SET_CONCATENATED_SMS_ALLOWED - <i>Unable to set concatenatedSMSAllowed</i>
     *     MBX_INVALID_CONCATENATED_SMS_ENABLED - <i>Invalid concatenated sms enabled</i>
     *     MBX_UNABLE_TO_SET_CONCATENATED_SMS_ENABLED - <i>Unable to set concatenatedSMSEnabled</i>
     *     MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS - <i>Invalid maximum concatenated sms segments</i>
     *     MBX_UNABLE_TO_SET_MAX_CONCATENATED_SMS_SEGMENTS - <i>Unable to set maxConcatenatedSMSSegments</i>
     *     MBX_INVALID_MAX_PER_CAPTCHA_SMS - <i>Invalid maximum per capcha sms</i>
     *     MBX_UNABLE_TO_SET_MAX_PER_CAPTCHA_SMS - <i>Unable to set maxPerCaptchaSMS</i>
     *     MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS - <i>Invalid maximum per captcha duration minutes</i>
     *     MBX_UNABLE_TO_SET_MAX_PER_CAPTCHA_DURATION_MINS - <i>Unable to set maxPerCaptchaDurationMins</i>
     *     MBX_INVALID_SMS_ADVANCED_NOTIFICATIONS_ENABLED - <i>Invalid sms advanced notifications enabled</i>
     *     MBX_UNABLE_TO_SET_SMS_ADVANCED_NOTIFICATIONS_ENABLED - <i>Unable to set smsAdvancedNotificationsEnabled</i>
     *     MBX_INVALID_SMS_BASIC_NOTICATIONS_ENABLED - <i>Invalid sms basic notifications enabled</i>
     *     MBX_UNABLE_TO_SET_SMS_BASIC_NOTICATIONS_ENABLED - <i>Unable to set smsBasicNotificationsEnabled</i>
     *     MBX_NOT_FOUND - <i>The given email does not exist.</i>
     *     MBX_INVALID_WEBMAIL_VERSION - <i>Invalid webmailVersion.</i>
     *     MBX_INVALID_SELFCARE_ACCESS - <i>Invalid selfcare access.</i>
     *     MBX_INVALID_SELFCARE_SSL_ACCESS - <i>Invalid selfcare SSL access.</i>
     *     MBX_INVALID_MESSAGE_STORE_HOST - <i>Invalid message store host.</i>
     *     MBX_INVALID_OLD_MESSAGE_STORE_HOST - <i>Invalid old message store host.</i>
     *     MBX_INVALID_NEW_MESSAGE_STORE_HOST - <i>Invalid new message store host.</i>
     *     MBX_INVALID_SMTP_PROXY_HOST - <i>Invalid smpt proxy host.</i>
     *     MBX_INVALID_IMAP_PROXY_HOST - <i>Invalid imap proxy host.</i>
     *     MBX_INVALID_POP_PROXY_HOST - <i>Invalid pop proxy host.</i>
     *     MBX_INVALID_AUTOREPLY_HOST - <i>Invalid autoreply host.</i>
     *     MBX_INVALID_IMAP_PROXY_AUTH - <i>Invalid imapProxyAuthenticationEnabled.</i>
     *     MBX_INVALID_REMOTE_CALL_TRACING - <i>Invalid remoteCallTracingEnabled.</i>
     *     MBX_INVALID_INTER_MANAGER_ACCESS - <i>Invalid interManagerAccessEnabled</i>
     *     MBX_INVALID_INTER_MANAGER_SSL_ACCESS - <i>Invalid interManagerSSLAccessEnabled</i>
     *     MBX_INVALID_VOICE_MAIL_ACCESS - <i>Invalid voiceMailAccessEnabled.</i>
     *     MBX_INVALID_FAX_ACCESS - <i>Invalid faxAccessEnabled.</i>
     *     MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE - <i>Invalid ldapUtilitiesAccessType.</i>
     *     MBX_INVALID_ADDRESS_BOOK_PROVIDER - <i>Invalid addressBookProvider.</i>
     *     MBX_UNABLE_TO_SET_WEBMAIL_VERSION - <i>Unable to set webmailVersion.</i>
     *     MBX_UNABLE_TO_SET_SELFCARE_ACCESS - <i>Unable to set selfcare access.</i>
     *     MBX_UNABLE_TO_SET_SELFCARE_SSL_ACCESS - <i>Unable to set selfcare access.</i>
     *     MBX_UNABLE_TO_SET_MESSAGE_STORE_HOST - <i>Unable to set Message Store host.</i>
     *     MBX_UNABLE_TO_SET_SMTP_PROXY_HOST - <i>Unable to set SMTP proxy host.</i>
     *     MBX_UNABLE_TO_SET_IMAP_PROXY_HOST - <i>Unable to set IMAP proxy host.</i>
     *     MBX_UNABLE_TO_SET_POP_PROXY_HOST - <i>Unable to set POP proxy host.</i>
     *     MBX_UNABLE_TO_SET_AUTOREPLY_HOST - <i>Unable to set AutoReply host.</i>
     *     MBX_UNABLE_TO_SET_IMAP_PROXY_AUTH - <i>Unable to set imapProxyAuthenticationEnabled.</i>
     *     MBX_UNABLE_TO_SET_REMOTE_CALL_TRACING - <i>Unable to set remote call tracing.</i>
     *     MBX_UNABLE_TO_GET_MESSAGE_STORE_HOST - <i>Unable to get Message Store host.</i>
     *     MBX_UNABLE_TO_SET_INTER_MANAGER_ACCESS - <i>Unable to set interManagerAccessEnabled.</i>
     *     MBX_UNABLE_TO_SET_INTER_MANAGER_SSL_ACCESS - <i>Unable to set interManagerSSLAccessEnabled.</i>
     *     MBX_UNABLE_TO_SET_VOICE_MAIL_ACCESS - <i>Unable to set voiceMailAccessEnabled.</i>
     *     MBX_UNABLE_TO_SET_FAX_ACCESS - <i>Unable to set faxAccessEnabled.</i>
     *     MBX_UNABLE_TO_SET_LDAP_UTILITIES_ACCESS_TYPE - <i>Unable to set ldapUtilitiesAccessType.</i>
     *     MBX_UNABLE_TO_SET_ADDRESS_BOOK_PROVIDER - <i>Unable to set addressBookProvider.</i>
     *     MBX_INVALID_MS_BMI_FILTERS_SPAM_ACTION - <i>Invalid spam action.</i>
     *     MBX_UNABLE_TO_SET_MS_BMI_FILTERS_SPAM_ACTION - <i>Unable to perform set spamAction.</i>
     *     MBX_INVALID_MS_BMI_FILTERS_SPAM_MESSAGE_INDICATOR - <i>Invalid spam message indicator.</i>
     *     MBX_UNABLE_TO_SET_MS_BMI_FILTERS_SPAM_MESSAGE_INDICATOR - <i>Unable to perform set spamMessageIndicator.</i>
     *     MBX_INVALID_CLOUDMARK_FILTERS_CLEAN_ACTION - <i>Invalid cloudmark filters clean action.</i>
     *     MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_CLEAN_ACTION - <i>Unable to set cleanAction.</i>
     *     MBX_INVALID_CLOUDMARK_FILTERS_SPAM_ACTION - <i>Invalid cloudmark filters spam action.</i>
     *     MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_ACTION - <i>Unable to set spamAction.</i>
     *     MBX_INVALID_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED - <i>Invalid cloudmark filters spam filtering enabled.</i>
     *     MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED - <i>Unable to set spamfilterEnabled.</i>
     *     MBX_INVALID_CLOUDMARK_FILTERS_SPAM_POLICY - <i>Invalid cloudmark filters spam policy.</i>
     *     MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_POLICY - <i>Unable to set spamPolicy.</i>
     *     MBX_INVALID_CLOUDMARK_FILTERS_SUSPECT_ACTION - <i>Invalid cloudmark filters spam action.</i>
     *     MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SUSPECT_ACTION - <i>Unable to set suspectAction.</i>
     *     MBX_INVALID_FOLDER_NAME - <i>Invalid folder name.</i>
     *     MBX_INVALID_HEADER_TEXT - <i>Invalid header text.</i>
     *     MBX_INVALID_TAG_TEXT - <i>Invalid tag text.</i> 
     *     FOLDER_NAME_REQUIRED <i>Folder name is required for action quarantine. </i>
     *     TAG_TEXT_REQUIRED <i>Tag text is required for action tag. </i>
     *     HEADER_TEXT_REQUIRED <i>message="Header text is required for action header. </i>
     *     MBX_INVALID_MS_COMMTOUCH_FILTERS_SPAM_ACTION - <i>Invalid spam action .</i>
     *     MBX_UNABLE_TO_SET_MS_COMMTOUCH_FILTERS_SPAM_ACTION - <i>Unable to perform set spamAction.</i>
     *     MBX_INVALID_MS_COMMTOUCH_FILTERS_VIRUS_ACTION - <i>Invalid virus action.</i>
     *     MBX_UNABLE_TO_SET_MS_COMMTOUCH_FILTERS_VIRUS_ACTION - <i>Unable to perform set virusAction.</i>
     *     MBX_INVALID_MS_MCAFEE_FILTERS_VIRUS_ACTION - <i>Invalid virus action.</i>
     *     MBX_UNABLE_TO_SET_MS_MCAFEE_FILTERS_VIRUS_ACTION - <i>Unable to perform set virusAction.</i>
     *     MBX_UNABLE_TO_SET_MS_BMI_FILTERS_SPAM_ACTION - <i>Unable to perform set spamAction.
     *     MBX_INVALID_MS_BMI_FILTERS_SPAM_MESSAGE_INDICATOR - <i>Invalid spam message indicator.
     *     MBX_UNABLE_TO_SET_MS_BMI_FILTERS_SPAM_MESSAGE_INDICATOR - <i>Unable to perform set spamMessageIndicator.
     *     MBX_INVALID_MR_COMMTOUCH_FILTERS_SPAM_ACTION - <i>Invalid spam action</i>
     *     MBX_UNABLE_TO_SET_MR_COMMTOUCH_FILTERS_SPAM_ACTION - <i>Unable to perform set spamAction</i>
     *     MBX_INVALID_MR_COMMTOUCH_FILTERS_VIRUS_ACTION - <i>Invalid virus action</i>
     *     MBX_UNABLE_TO_SET_MR_COMMTOUCH_FILTERS_VIRUS_ACTION - <i>Unable to perform set virusAction</i>
     *     MBX_INVALID_MS_MCAFEE_FILTERS_VIRUS_ACTION - <i>Invalid virus action</i>
     *     MBX_UNABLE_TO_SET_MS_MCAFEE_FILTERS_VIRUS_ACTION - <i>Unable to perform set virusAction</i>
     *     MBX_UNABLE_TO_SET_MAIL_NOTIFICATION_SENT_STATUS - <i>Unable to perform set notification sent status.</i>
     *     MBX_INVALID_MAIL_NOTIFICATION_SENT_STATUS - <i>Invalid notification sent status</i>
     *     MBX_UNABLE_TO_SET_LAST_SUCCESSFUL_LOGIN_DATE - <i>Unable to perform set successful login date.</i>
     *     MBX_INVALID_LAST_SUCCESSFUL_LOGIN_DATE - <i>Invalid date format for last successful login date.</i>
     *     MBX_UNABLE_TO_SET_SIGNATURE - <i>Unable to set signature</i>
     *     MBX_INVALID_SIGNATURE - <i>Invalid Signature</i>
     *     MBX_UNABLE_TO_SET_ALLOW_PASSWORD_CHANGE - <i>Unable to update allowPasswordChange</i>
     *     MBX_INVALID_ALLOW_PASSWORD_CHANGE - <i>Invalid value for allowPasswordChange</i>
     *     MBX_UNABLE_TO_SET_MAX_CONTACTS - <i>Unable to update maxContacts</i>
     *     MBX_UNABLE_TO_SET_MAX_GROUPS - <i>Unable to update maxGroups</i>
     *     MBX_UNABLE_TO_SET_MAX_CONTACTS_PER_GROUP - <i>Unable to update maxContactsPerGroup</i>
     *     MBX_UNABLE_TO_SET_MAX_CONTACTS_PER_PAGE - <i>Unable to update maxContactsPerPage</i>
     *     MBX_UNABLE_TO_SET_CREATE_CONTACTS_FROM_OUTGOING_EMAILS - <i>Unable to update createContactsFromOutgoingEmails</i>
     *     MBX_INVALID_MAX_CONTACTS - <i>Invalid value for maxContacts</i>
     *     MBX_INVALID_MAX_GROUPS - <i>Invalid value for maxGroups</i>
     *     MBX_INVALID_MAX_CONTACTS_PER_GROUP - <i>Invalid value for maxContactsPerGroup</i>
     *     MBX_INVALID_MAX_CONTACTS_PER_PAGE - <i>Invalid value for maxContactsPerPage</i>
     *     MBX_INVALID_CREATE_CONTACTS_FROM_OUTGOING_EMAILS - <i>Invalid value for createContactsFromOutgoingEmails</i>
     *     MBX_UNABLE_TO_SET_EXTENSION_ATTRIBUTES - <i>Unable to set Extension Cos Attributes.</i>
     *     MBX_INVALID_EXTENSION_ATTRIBUTES - <i>Invalid value for Extension Cos Attributes.</i>
     *     MBX_INVALID_MAXNUM_OF_FWDADDRSS - <i>Invalid maximum number of forward addresses</i>
     *     MBX_UNABLE_TO_SET_MAXNUM_OF_FWDADDRSS - <i>Unable to set maximum allowed forward addresses</i>
     *     MBX_CREATE_FOR_GROUP_ADMIN_NOT_SUPPORTED - <i>Create Mailbox for group admin is not supported.</i>
     *     MBX_GROUP_NOT_EXISTS - <i>Group does not exist.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    long create(final Map<String, List<String>> inputParams)
        throws MxOSException;
    
    /**
     * This operation is responsible for reading mailbox. It uses one or more
     * actions to do this activity.
     *
     * @param inputParams
     *     Parameters given by user.
     *
     * <pre>
     * <b>inputParams-Mandatory:</b>
     *
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * </pre>
     *
     * @return returns Mailbox POJO.
     * @throws MxOSException MxOSException.
     *
     * <pre>
     * <b>Error Codes:</b>
     *
     *     MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *     MBX_INVALID_MAILBOXID - <i>Invalid mailboxid.</i>
     * <pre>
     **/
    Mailbox read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading multiple mailboxes.
     * it takes multiple email address and provide the mailbox details
     * for all the email address.
     *
     * @param inputParams
     *     Parameters given by user.
     *
     * <pre>
     * <b>inputParams-Mandatory:</b>
     *
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * </pre>
     *
     * @return returns list of Mailbox POJO.
     * @throws MxOSException MxOSException.
     *
     * <pre>
     * <b>Error Codes:</b>
     *
     *     MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *     MBX_INVALID_MAILBOXID - <i>Invalid mailboxid.</i>
     *     MBX_NOT_FOUND - <i>The given email does not exist.</i>
     *     MBX_INVALID_MAX_ALLOWEDDOMAIN - <i>Invalid maxAllowedDomain value.</i>
     * <pre>
     **/
    List<Mailbox> list(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    /**
     * This operation is responsible for deleting mailbox. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     *
     * <b>inputParams-Optional:</b>
     *
     * <b>deleteOnlyOnMss</b>
     *     <i>To delete the mailbox only from Message Store Host.</i>
     *     Type: Boolean
     *     Enum : false, true
     *
     * <b>recreateOnMss</b>
     *     <i>To recreate an empty mailbox on Message Store Host.
     *     Note: this parameter is considered only if deleteOnlyOnMss is true.</i>
     *     Type: Boolean
     *     Enum : false, true
     *     
     * <b>suppressMers</b>
     *     <i>To suppress MERS events for this operation..
     *     Type: Boolean
     *     Enum : false, true
     *     
     * <b>keepAutoReply</b>
     *     <i>To prevent deletion of autoreply from MSS.
     *     Type: Boolean
     *     Enum : false, true
     *     
     * <b>disableBlobHardDelete</b>
     *     <i>To disable blob hard delete.
     *     Type: Boolean
     *     Enum : false, true
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_UNABLE_TO_DELETE - <i>Unable to perform Mailbox DELETE operation.</i>
     * MBX_UNABLE_TO_DELETE_GROUP_ADMIN - <i>Mailbox delete not allowed for group admin(HOH) account.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
