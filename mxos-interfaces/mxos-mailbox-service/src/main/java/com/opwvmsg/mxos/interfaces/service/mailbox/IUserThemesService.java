/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * User Themes Service operations interface which will be exposed to the client.
 * This interface is responsible for doing User Themes related operations (like
 * Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IUserThemesService {

    /**
     * NOT SUPPORTED
     */
    void create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * NOT SUPPORTED
     */
    List<String> read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * NOT SUPPORTED
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * NOT SUPPORTED
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
