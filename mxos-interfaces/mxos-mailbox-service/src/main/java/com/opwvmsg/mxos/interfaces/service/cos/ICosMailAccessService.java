/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail access COS operations interface which will be exposed to the client.
 * This interface is responsible for doing mail access related operations (like
 * Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface ICosMailAccessService {

    /**
     * This operation is responsible for reading mail access COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * </pre>
	 *  
	 * @return returns MailAccess POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_CREATE - <i>Unable to perform Mailbox Create operation.</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    MailAccess read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail access COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>popAccessType</b>
	 *     <i>Account access for standard POP service. Allowed values: all|trusted|none></i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["all", "trusted", "none"]
	 *     Example: all
	 *	  
	 * <b>popAuthenticationType</b>
	 *     <i>Authenticated POP service status of an account</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["0", "1", "2"]
	 *     Example: 1
	 *	  
	 * <b>popSSLAccessType</b>
	 *     <i>Account access for SSL-encrypted POP service</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["all", "trusted", "none"]
	 *     Example: all
	 *     
	 * <b>imapAccessType</b>
	 *     <i>Account access for standard IMAP service</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["all", "trusted", "none"]
	 *     Example: all
	 *	  
	 * <b>imapSSLAccessType</b>
	 *     <i>Account access for SSL-encrypted IMAP service</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["all", "trusted", "none"]
	 *     Example: all
	 *	  
	 * <b>smtpAccessEnabled</b>
	 *     <i>Account access for standard SMTP service</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *     
	 * <b>smtpAuthenticationEnabled</b>
	 *     <i>Authenticated SMTP service status of an account</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *	  
	 * <b>smtpSSLAccessEnabled</b>
	 *     <i>Account access for SSL-encrypted SMTP service</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *	  
	 * <b>webmailAccessType</b>
	 *     <i>Account access for WebEdge Web interface</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["all", "trusted", "none"]
	 *     Example: all
	 *     
	 *  <b>webmailSSLAccessType</b>
	 *     <i>Account access for SSL-encrypted WebEdge Web interface</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["all", "trusted", "none"]
	 *     Example: all
	 *	  
	 * <b>mobileMailAccessType</b>
	 *     <i>Specifies how users in the COS can use Web Engine servers to access messages</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["all", "trusted", "none"]
	 *     Example: all
	 *	 
	 * <b>mobileActiveSyncAllowed</b>
	 *     <i>Active Sync Allowed status for the account</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *     
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_NOT_FOUND - <i>The given Cos does not exists</i>
	 * MBX_UNABLE_TO_SET_POP_ACCESS - <i>Unable to perform set popAccessType</i>
	 * MBX_UNABLE_TO_SET_POP_AUTH_TYPE - <i>Unable to perform set popAuthenticationType</i>
	 * MBX_UNABLE_TO_SET_POP_SSL_ACCESS - <i>Unable to perform set popSSLAccessType</i>
	 * MBX_UNABLE_TO_SET_IMAP_ACCESS - <i>Unable to perform set imapAccessType</i>
	 * MBX_UNABLE_TO_SET_IMAP_SSL_ACCESS - <i>Unable to perform set imapSSLAccessType</i>
	 * MBX_UNABLE_TO_SET_SMTP_ACCESS_ENABLED - <i>Unable to perform set smtpAccessEnabled</i>
	 * MBX_UNABLE_TO_SET_SMTP_AUTH_ENABLED - <i>Unable to perform set smtpAuthenticationEnabled" />
	 * MBX_UNABLE_TO_SET_SMTP_SSL_ACCESS_ENABLED - <i>Unable to perform set smtpSSLAccessEnabled" />
	 * MBX_UNABLE_TO_SET_WEBMAIL_ACCESS - <i>Unable to perform set webMailAccessType</i>
	 * MBX_UNABLE_TO_SET_WEBMAIL_SSL_ACCESS - <i>Unable to perform set webMailSSLAccessType</i>
	 * MBX_UNABLE_TO_SET_MOBILE_MAIL_ACCESS - <i>Unable to perform set mobileMailAccessType</i>
	 * MBX_UNABLE_TO_SET_MOBILE_ACTIVE_SYNC_ALLOWED - <i>Unable to perform set mobileActiveSyncAllowed</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
