/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox MailReceipt BMI filter operations interface which will be exposed to
 * the client. This interface is responsible for doing mailReceipt BMI filter
 * related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface IMailReceiptBMIFiltersService {

    /**
     * This operation is responsible for reading mailReceipt BMI filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return BmiFilters Object of MailReceipt.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_MAILSEND_GET - <i>Unable to perform MailSend GET operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    BmiFilters read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mailReceipt BMI filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     *      
     * <b>spamAction</b>
     *     <i>Spam Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * <b>spamMessageIndicator</b>
     *     <i>X-Header or subject prefix if Spam action is Add Header or Subject Prefix.</i>
     *     Type: String, Empty Allowed: Yes, Maximum Length: 255
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *     MBX_INVALID_MS_BMI_FILTERS_SPAM_ACTION - <i>Invalid spam action.</i>
     *     MBX_UNABLE_TO_SET_MS_BMI_FILTERS_SPAM_ACTION - <i>Unable to perform set spamAction.</i>
     *     MBX_INVALID_MS_BMI_FILTERS_SPAM_MESSAGE_INDICATOR - <i>Invalid spam message indicator.</i>
     *     MBX_UNABLE_TO_SET_MS_BMI_FILTERS_SPAM_MESSAGE_INDICATOR - <i>Unable to perform set spamMessageIndicator.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
