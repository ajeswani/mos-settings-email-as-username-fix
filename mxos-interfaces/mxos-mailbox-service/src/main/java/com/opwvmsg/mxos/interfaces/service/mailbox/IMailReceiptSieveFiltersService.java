/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox mailReceipt sieve filter operations interface which will be exposed
 * to the client. This interface is responsible for doing mailReceipt sieve
 * filter related operations (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IMailReceiptSieveFiltersService {

    /**
     * This operation is responsible for reading mailReceipt sieve filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return SieveFilters Object of MailReceipt.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    SieveFilters read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mailReceipt sieve filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     *      
     * <b>sieveFilteringEnabled</b>
     *     <i>Sieve filtering is enabled or disabled for the subscriber.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>mtafilter</b>
     *     <i>MTA filter in mss.</i>
     *     Type: String, Maximum Length: 225, EmptyAllowed: yes
     * <b>rmfilter</b>
     *     <i>RM filter in mss.</i>
     *     Type: String, Maximum Length: 225, EmptyAllowed: yes
     * <b>blockedSenderAction</b>
     *     <i>Blocked sender action.</i>
     *     Type: String, Enum: "bounce", "reject", "sideline", "toss", "none"
     * <b>blockedSenderMessage</b>
     *     <i>Message associated with the denied sender action.</i>
     *     Type: String, Maximum Length: 225
     * <b>rejectBouncedMessage</b>
     *     <i>Reject bounced message enabled or disabled.</i>
     *     Type: String, Enum: "no", "yes"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *     MBX_INVALID_SIEVE_FILTERING_ENABLED - <i>Invalid sieve filtering enabled.</i>
     *     MBX_UNABLE_TO_SET_SIEVE_FILTERING_ENABLED - <i>Unable to set sieveFilteringEnabled.</i>
     *     MBX_INVALID_SIEVE_FILTERS_BLOCKED_SENDER_ACTION - <i>Invalid sieve filters blocked sender action.</i>
     *     MBX_UNABLE_TO_SET_SIEVE_FILTERS_BLOCKED_SENDER_ACTION - <i>Unable to set blockedSenderAction.</i>
     *     MBX_INVALID_SIEVE_FILTERS_BLOCKED_SENDER_MESSAGE - <i>Invalid sieve filters blocked sender message.</i>
     *     MBX_UNABLE_TO_SET_SIEVE_FILTERS_BLOCKED_SENDER_MESSAGE - <i>Unable to set blockedSenderMessage.</i>
     *     MBX_INVALID_SIEVE_FILTERS_MTA_FILTER - <i>Invalid MTA filter.</i>
     *     MBX_INVALID_SIEVE_FILTERS_RM_FILTER - <i>Invalid RM filter.</i>
     *     MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER - <i>Unable to set MTA filter.</i>
     *     MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER - <i>Unable to set RM filter.</i>
     *     MBX_INVALID_SIEVE_FILTERS_REJECT_BOUNCED_MESSAGE - <i>Invalid sieve filters reject bounced message.</i>
     *     MBX_UNABLE_TO_SET_SIEVE_FILTERS_REJECT_BOUNCED_MESSAGE - <i>Unable to set rejectBouncedMessage.</i>
     *      
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
