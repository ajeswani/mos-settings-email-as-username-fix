/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail external store operations interface which will be exposed to the client.
 * This interface is responsible for doing mail store related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface ICosExternalStoreService {

    /**
     * This operation is responsible for reading mail external store cos
     * attributes. It uses one or more actions to do this activity.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * </pre>
	 *  
	 * @return returns MailStore POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_CREATE - <i>Unable to perform Mailbox Create operation.</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    ExternalStore read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail external store cos
     * attributes. It uses one or more actions to do this activity.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 *  
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>externalStoreAccessAllowed</b>
	 *     <i>Specifies whether RichMail allows access to external store></i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>maxExternalStoreSizeMB</b>
	 *     <i>External storage size quota</i>
	 *     Type: long, Minimum Value: 0, Maximum Value: 1073741823, Empty Allowed: No
	 *     Example: 1
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_UNABLE_TO_GET - <i>Unable to perform Cos GET operation</i>
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_NOT_FOUND - <i>The given cosId does not exist</i>
	 * COS_UNABLE_TO_EXTERNALSTORE_GET - <i>Unable to perform Cos External Store GET operation</i>
	 * COS_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED - <i>Invalid external store access allowed</i>
	 * COS_INVALID_MAX_EXTERNAL_STORE_SIZEMB - <i>Invalid max external store size</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
