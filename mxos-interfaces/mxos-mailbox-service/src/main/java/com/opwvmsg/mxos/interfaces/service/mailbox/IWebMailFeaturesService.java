/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * WebMailFeatures interface which will be exposed to the client. This
 * interface is responsible for doing web mail features related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface IWebMailFeaturesService {

    /**
     * This operation is responsible for reading web mail features.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * </pre>
	 *  
	 * @return returns WebMailFeatures POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_WEBMAILFEATUES_GET - <i>Unable to perform WebMail Features GET operation</i>
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 * MBX_NOT_FOUND - <i>The given email does not exist</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    WebMailFeatures read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating web mail features.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>businessFeaturesEnabled</b>
	 *     <i>Authorization for business users or regular users</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>fullFeaturesEnabled</b>
	 *     <i>This states if full feature is enabled or not</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>Date when the Full feature was accessed</b>
	 *     <i>Subscriber's first name.</i>
	 *     Type: Date
	 *     Format: "yyyy-MM-dd'T'HH:mm:ss'Z'"
	 *     Example: 2013-01-01'T'11:11:11'Z'
	 *     
         * <b>allowPasswordChange</b>
         *     <i>This attribute specifies whether subscribers have permission to change their WebEdge account password.</i>
         *     Type: String, ENUM ["no", "yes"]
         *     Example: yes
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_NOT_FOUND - <i>The given email does not exist</i>
	 * MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED - <i>Unable to update businessFeaturesEnabled</i>
	 * MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED - <i>Unable to update fullFeaturesEnabled</i>
	 * MBX_UNABLE_TO_SET_LAST_FULL_FEATURES_CONN_DATE - <i>Unable to update lastFullFeaturesConnectionDate</i>
	 * MBX_INVALID_LAST_FULL_FEATURES_CONN_DATE - <i>Invalid lastFullFeaturesConnectionDate</i>
	 * MBX_UNABLE_TO_SET_ALLOW_PASSWORD_CHANGE - <i>Unable to update allowPasswordChange</i>
         * MBX_INVALID_ALLOW_PASSWORD_CHANGE - <i>Invalid value for allowPasswordChange</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
