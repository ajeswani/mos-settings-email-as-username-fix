/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Inteface to COS main object to perform Create and Delete of COS.
 *
 * @author mxos-dev
 */
public interface ICosService {

    /**
     * This operation is responsible for creating cos.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * COS_UNABLE_TO_CREATE - <i>Unable to perform Cos Create operation.</i>
     * COS_INVALID_COSID - <i>Invalid cosId.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting COS.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * COS_UNABLE_TO_DELETE - <i>Unable to perform Cos DELETE operation.</i>
     * COS_INVALID_COSID - <i>Invalid cosId.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
