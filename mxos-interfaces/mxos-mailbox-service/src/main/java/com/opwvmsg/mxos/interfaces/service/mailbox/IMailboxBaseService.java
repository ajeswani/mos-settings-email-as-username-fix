/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox operations interface which will be exposed to the client. This
 * interface is responsible for doing mailbox related operations (like Create,
 * Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IMailboxBaseService {

    /**
     * This operation is responsible for reading mailbox. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return Mailbox POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * MBX_UNABLE_TO_GET - <i>Unable to perform Mailbox GET operation.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_FOUND - <i>The given email does not exist.</i>
     * MBX_INVALID_MAX_ALLOWEDDOMAIN - <i>Invalid maxAllowedDomain value.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Base read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mailbox. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>userName</b>
     *     <i>Subscriber's POP Address.</i>
     *     Type: String, Maximum Length: 225, EmptyAllowed: yes
     * <b>status</b>
     *     <i>Subscriber status.</i>
     *     Type: String, Enum: "active", "maintenance", "suspended", "locked", "deleted", "proxy"
     * <b>firstName</b>
     *     <i>Subscriber's first name.</i> 
     *     Type: String, Minimum Length: 1, Maximum Length: 225, EmptyAllowed: no
     * <b>lastName</b>
     *     <i>Subscriber's last name.</i> 
     *     Type: String, Minimum Length: 1, Maximum Length: 225, EmptyAllowed: no
     * <b>lastStatusChangeDate</b>
     *     <i>Last status change date.</i>
     *     Type: Date, Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * <b>notificationMailSentStatus</b>
     *     <i>Notification sent status</i> allowed values 
     *     Type: String, Enum: "notSent", "sentForInactiveStatus", "sentBeforeSuspendedStatus", "sentForSuspendedStatus", "sentAfterSuspendedStatus", "sentBeforeDeletedStatus"
     * <b>msisdn</b>
     *     <i>MSISDN for the email account in international format, followed by any number number 10 to 15 digits..</i>
     *     Type: String
     *     Regex Pattern: ^(({0,})|[+]{1}([0-9]{10,15}))$
     * <b>lastMsisdnStatusChangeDate</b>
     *     <i>Last Msisdn Status Change Date.</i>
     *     Type: Date, Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * <b>msisdnStatus</b>
     *     <i>Status of the MSISDN.</i>
     *     Type: String, Enum: "deactivated", "activated"
     * <b>maxNumAliases</b>
     *     <i>Maximum number of aliases allowed for the user.</i>
     *     Type: Integer, Minimum: 1, maximum: 1000
     * <b>maxNumAllowedDomains</b>
     *     <i>Maximum number of allowed Domain for the user.</i>
     *     Type: Integer, Minimum: 1, maximum: 1000
     * <b>extensionAttributes</b>
     *     <i>Customer specific optional data - extensionsOptionalCoSAttributes</i>
     *     Note: The extensionsOptionalCoSAttributes is a Config DB key which stores the optional 
     *     attribute key for the ldap. The actual value will be stored in the LDAP using the keys.
     *     : and | characters are not supported as value in extensionAttributes. 
     * <b>cosId</b>
     *     <i>New cos Id to be used.</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: ^([a-zA-Z0-9._]+)$
     * <b>customFields</b>
     *     <i>Customer specific data - below are the only attributes supported currently.</i>
     *     Format : customFields=bgcFKey:testfkey1
     *     Note: customField's Keys are Case Sensitive, throws GEN_BAD_REQUEST exception if improperly formed.
     *
     * </pre>
     *    
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *     MBX_INVALID_USERNAME - <i>Invalid username.</i>
     *     MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *     MBX_INVALID_MAILBOX_STATUS - <i>Invalid account status.</i>
     *     MBX_INVALID_MAILBOX_STATUS_DATE - <i>Invalid account status change date.</i>
     *     MBX_INVALID_MAILBOX_TYPE - <i>Invalid account type.</i>
     *     MBX_INVALID_MSISDN - <i>Invalid MSISDN.</i>
     *     MBX_INVALID_MSISDN_STATUS - <i>Invalid MSISDN Status.</i>
     *     MBX_INVALID_MSISDN_STATUS_DATE - <i>Invalid MSISDN Status change date.</i>
     *     MBX_INVALID_MAXNUM_OF_ALIASES - <i>Invalid maximum number of email aliases.</i>
     *     MBX_INVALID_COSID - <i>Invalid cosId.</i>
     *     MBX_UNABLE_TO_GET - <i>Unable to perform Mailbox GET operation.</i>
     *     MBX_UNABLE_TO_UPDATE - <i>Unable to perform Mailbox POST operation.</i>
     *     MBX_UNABLE_TO_SEARCH - <i>Unable to perform Mailbox SEARCH operation.</i>
     *     MBX_UNABLE_TO_SET_USERNAME - <i>Unable to perform set username.</i>
     *     MBX_UNABLE_TO_SET_ACCOUNT_STATUS - <i>Unable to perform set account status.</i>
     *     MBX_UNABLE_TO_SET_ACCOUNT_STATUS_DATE - <i>Unable to perform set account status change date.</i>
     *     MBX_UNABLE_TO_SET_ACCOUNT_TYPE - <i>Unable to perform set account type.</i>
     *     MBX_UNABLE_TO_SET_MSISDN - <i>Unable to perform set account msisdn.</i>
     *     MBX_UNABLE_TO_SET_MSISDN_STATUS - <i>Unable to perform set account msisdn status.</i>     
     *     MBX_UNABLE_TO_SET_MSISDN_STATUS_DATE - <i>Unable to perform set account msisdn status change date.</i>
     *     MBX_UNABLE_TO_SET_MAX_NUM_ALIAS - <i>Unable to perform set maxNumAliases.</i>
     *     MBX_UNABLE_TO_SET_COSID - <i>Unable to perform set account COS id.</i>    
     *     "4" - <i>"Given domain is not a primary domain.</i>
     *     "27" - <i>"Invalid status, status is not one of the supported ones [open, locked, closed, inactive, pending, closing]</i>
     *     MBX_INVALID_MAX_ALLOWEDDOMAIN - <i>Invalid maxAllowedDomain value.</i>
     *     MBX_UNABLE_TO_SET_MAX_ALLOWEDDOMAIN - <i>Unable to perform set maxNumAllowedDomain.</i>
     *     MBX_UNABLE_TO_SET_CUSTOMFIELDS - <i>Unable to perform set CustomFields.</i>
     *     MBX_UNABLE_TO_SET_MAIL_NOTIFICATION_SENT_STATUS - <i>Unable to perform set notification sent status.</i>
     *     MBX_INVALID_MAIL_NOTIFICATION_SENT_STATUS - <i>Invalid notification sent status</i>
     *     MBX_UNABLE_TO_SET_EXTENSION_ATTRIBUTES - <i>Unable to set Extension Cos Attributes.</i>
     *     MBX_INVALID_EXTENSION_ATTRIBUTES - <i>Invalid value for Extension Cos Attributes.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for searching mailbox. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * <b>query</b>
     *     <i>query to search, can be any valid LDAP search query</i>
     *     For Search by email:
     *     http://mxosHost:mxosPort/mxos/mailbox/v2/base/list?query=(mail=<email>)
     *     
     *     To search by email "test*"
     *     from browser (UTF-8 charset): http://localhost:8080/mxos/mailbox/v2/base/list?query=%28mail%3dtest%2a%29

     *     For Search by email or alias:
     *     http://mxosHost:mxosPort/mxos/mailbox/v2/base/list?query=(|(mail=<email>)(mailalternateaddress=<alias>))
     * </pre>
     * 
     * @return List containing Mailbox POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * MBX_UNABLE_TO_SEARCH - <i>Unable to perform Mailbox SEARCH operation.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<Base> search(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
