/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Social Network Site interface which will be exposed to the client. This
 * interface is responsible for doing Social Network Site related operations
 * (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface ISocialNetworkSiteService {

    /**
     * This operation is responsible for creating Social Network Site.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * <b>socialNetworkSite</b>
	 *     <i>Linked socialNetwork sites for the account, allowed Value - RFC compliant domain</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}$",
	 *     Example: openwave.com
	 *     
	 *     
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>socialNetworkSiteAccessEnabled</b>
	 *     <i>Indicate whether the site access are enabled or not</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: no
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_PUT_SOCIALNETWORKSSITE - <i>Unable to perform SocialNetworksSite PUT operation</i>
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 * MBX_INVALID_SOCIALNETWORKSITE - <i>Invalid socialNetworkSite value</i>
	 * MBX_INVALID_SNSITEACCESSENABLED - <i>Invalid socialNetworkSiteAccessEnabled value</i> 
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading Social Network Site.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * </pre>
	 *  
	 * @return returns list of SocialNetworkSite objects.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 * MBX_INVALID_NETMAIL_SOCIALNETWORKSITE - <i>Invalid netMailSocialNetworkSite value in Ldap</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    List<SocialNetworkSite> read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating Social Network Site.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 *     
	 * <b>socialNetworkSiteAccessEnabled</b>
	 *     <i>ndicate whether the site access are enabled or not</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 *  MBX_UNABLE_TO_POST_SOCIALNETWORKSSITE - <i>Unable to perform SocialNetworksSite POST operation</i>
	 *  MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 *  MBX_INVALID_SOCIALNETWORKSITE - <i>Invalid socialNetworkSite value</i>
	 *  MBX_INVALID_SNSITEACCESSENABLED - <i>Invalid socialNetworkSiteAccessEnabled value</i> 
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting Social Network Site.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * <b>socialNetworkSite</b>
	 *     <i>Linked socialNetwork sites for the account, allowed Value - RFC compliant domain</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}$",
	 *     Example: openwave.com
	 *     
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 * MBX_UNABLE_TO_DELETE_SOCIALNETWORKSSITE - <i>Unable to perform SocialNetworksSite DELETE operation</i>
	 * MBX_SOCIALNETWORK_SNS_NOTFOUND - <i>Given socialNetworkSite is not present in Ldap</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
