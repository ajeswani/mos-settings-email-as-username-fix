/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Sender Blocking operations interface which will be exposed to the client.
 * This interface is responsible for doing Sender Blocking related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface ICosSenderBlockingService {

    /**
     * This operation is responsible for reading Sender Blocking.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * </pre>
     *  
     * @return returns SenderBlocking POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    SenderBlocking read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating Sender Blocking.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
      <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>senderBlockingAllowed</b>
     *     <i>Sender blocking is allowed or not allowed for the subscriber.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>senderBlockingEnabled</b>
     *     <i>Sender blocking is enabled or disabled for the subscriber.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>blockSendersPABActive</b>
     *     <i>Sender blocking is active or not active for subscriber.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>blockSendersPABAccess</b>
     *     <i>Sender blocking is enabled or disabled for subscriber.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_SENDER_BLOCKING_ALLOWED - <i>Invalid sender blocking allowed.</i>
     * MBX_UNABLE_TO_SET_SENDER_BLOCKING_ALLOWED - <i>Unable to set senderBlockingAllowed.</i>
     * MBX_INVALID_SENDER_BLOCKING_ENABLED - <i>Invalid sender blocked enabled.</i>
     * MBX_UNABLE_TO_SET_SENDER_BLOCKING_ENABLED - <i>Unable to set senderBlockingEnabled.</i>
     * MBX_INVALID_REJECT_ACTION - <i>Invalid reject action.</i>
     * MBX_INVALID_BLOCK_SENDERS_PAB_ACTIVE - <i>Invalid block senders pab active.</i>
     * MBX_UNABLE_TO_SET_BLOCK_SENDERS_PAB_ACTIVE - <i>Unable to set blockSendersPABActive.</i>
     * MBX_INVALID_BLOCK_SENDERS_PAB_ACCESS - <i>Invalid block senders pab access.</i>
     * MBX_UNABLE_TO_SET_BLOCK_SENDERS_PAB_ACCESS - <i>Unable to set blockSendersPABAccess.</i>

     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
