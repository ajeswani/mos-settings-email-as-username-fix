/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.captcha;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Captcha operations interface which will be exposed to the client. This
 * interface is responsible for validating captcha.
 * 
 * @author mxos-dev
 */

public interface ICaptchaService {

    /**
     * This operation is responsible for validating CAPTCHA
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>isValid</b>
     *     <i>The validity status of the user</i>
     *     Type: String
     *     Allowed Values: true|false
     *     Example: true
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * CPT_INVALID_CAPTCHA - <i>Invalid input for CAPTCHA status.</i>
     * CPT_UNABLE_TO_UPDATE - <i>Unable to update the captcha details.</i>
     * CPT_ATTEMPTS_ALLOWED - <i>Captcha failed with attempts allowed.</i>
     * CPT_ATTEMPTS_ALLOWED_LDAP_UPDATE_ERROR - <i>Captcha failed to update LDAP with attempts allowed.</i>
     * CPT_ATTEMPTS_EXCEEDED - <i>Captcha failed with attempts exceeded.</i>
     * CPT_ATTEMPTS_EXCEEDED_LDAP_UPDATE_ERROR - <i>Captcha failed to update LDAP with attempts exceeded.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void validateCaptcha(final Map<String, List<String>> inputParams)
            throws MxOSException;

}
