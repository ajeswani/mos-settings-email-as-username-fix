/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * MailAccess Allowed Ip interface which will be exposed to the client.
 * This interface is responsible for doing MailAccess Allowed Ip related
 * operations (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IMailAccessAllowedIpService {

    /**
     * This operation is responsible for creating MailAccess Allowed Ip.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>allowedIP</b>
     *     <i>Allowed IP in IPv4 or IPv6 format</i>
     *     Type: String, 
     *     Regex Pattern: "^((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|(([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4}$"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     *
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_CREATE - <i>Unable to perform AllowedIPs Create operation.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_FOUND - <i>The given email does not exist.</i>
     * MBX_INVALID_MAILACCESS_ALLOWED_IP - <i>Invalid allowedIP.</i>
     * MBX_MAILACCESS_ALLOWED_IPS_REACHED_MAX_LIMIT - <i>Allowed IPs reached the maximum limit.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading MailAccess Allowed Ip.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of forward addresses.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_FOUND - <i>The given email does not exist.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating MailAccess Allowed Ip.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>oldAllowedIP</b>
     *     <i>Old allowed IP in IPv4 or IPv6 format</i>
     *     Type: String, 
     *     Regex Pattern: "^((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|(([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4}$"
     * <b>newAllowedIP</b>
     *     <i>New allowed IP in IPv4 or IPv6 format</i>
     *     Type: String, 
     *     Regex Pattern: "^((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|(([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4}$"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_POST - <i>Unable to perform AllowedIPs POST operation.</i>
     * MBX_NOT_FOUND - <i>The given email does not exist.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_INVALID_OLD_MAILACCESS_ALLOWED_IP - <i>Invalid oldAllowedIP.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting MailAccess Allowed Ip.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>allowedIP</b>
     *     <i>Allowed IP in IPv4 or IPv6 format</i>
     *     Type: String, 
     *     Regex Pattern: "^((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|(([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4}$"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_DELETE - <i>Unable to perform AllowedIPs DELETE operation.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_FOUND - <i>The given email does not exist.</i>
     * MBX_INVALID_MAILACCESS_ALLOWED_IP - <i>Invalid allowedIP.</i>
     * MBX_MAILACCESS_ALLOWED_IP_NOT_EXIST - <i>Allowed IP does not exist.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
