/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox external account operations interface which will be exposed to the
 * client. This interface is responsible for doing mailbox external account
 * related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface IExternalAccountsService {

    /**
     * This operation is responsible for reading external account.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return ExternalAccounts.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_INVALID_EXTERNAL_ACCOUNTS_ALLOWED - <i>Invalid ExternalAccountsAllowed value in Ldap.</i>
     * MBX_INVALID_PROMPT_FOR_EXTERNAL_SYNC - <i>Invalid Prompt For External Sync value in Ldap.</i>
     * MBX_INVALID - <i>Invalid input of MailAccount.</i>
     * MBX_UNABLE_TO_GET_EXTERNALACCOUNTS - <i>Unable to get Externalaccounts</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    ExternalAccounts read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating external account.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>externalMailAccountsAllowed</b>
     *     <i>Allowed value - yes or no</i>
     *     Type: String, Enum: "no", "yes"
     * <b>promptForExternalAccountSync</b>
     *     <i>Allowed value - yes or no</i>
     *     Type: String, Enum: "no", "yes"
     * </pre>    
     *     
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EXTERNAL_ACCOUNTS_ALLOWED - <i>Invalid ExternalAccountsAllowed value in Ldap.</i>
     * MBX_INVALID_PROMPT_FOR_EXTERNAL_SYNC - <i>Invalid Prompt For External Sync value in Ldap.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_INVALID - <i>Invalid input of MailAccount.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
