/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to COS Credentials Object level operations like Read and Update.
 *
 * @author mxos-dev
 */
public interface ICosCredentialsService {
    
	/**
     * This operation is responsible for reading COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * </pre>
	 *  
	 * @return returns Credentials POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_INVALID_COSID - <i>Invalid cosId.</i>
	 * COS_NOT_FOUND - <i>The given cosId does not exist</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    Credentials read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>passwordRecoveryAllowed</b>
	 *     <i>Specified whether password recovery enabled</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>maxFailedLoginAttempts</b>
	 *     <i>Specified maximum number of failed login attempts for a user</i>
	 *     Type: integer, Minimum Val: 1, Maximum Val: 1000, Empty Allowed: No 
	 * 	   Example: 4
	 *     
	 * <b>maxFailedCaptchaLoginAttempts</b>
         *     <i>Specified maximum number of failed captcha login attempts for a user, allowed value- positive integer</i>
         *     Type: integer, Minimum: 0, maximum: 1000
         *         Example: 10
         *
         * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_NOT_FOUND - <i>The given cosId does not exist</i>
	 * MBX_INVALID_PASSWORD_RECOVERY_ALLOWED - <i>Unable to perform set passwordRecoveryAllowed, Invalid passwordRecoveryAllowed parameter</i>
	 * MBX_INVALID_MAX_FAILED_LOGIN_ATTEMPTS - <i>Unable to perform set maxFailedLoginAttempts, Invalid maxFailedLoginAttempts parameter</i>
	 * MBX_INVALID_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Invalid maxFailedCaptchaLoginAttempts parameter.</i>
         * MBX_UNABLE_TO_SET_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Unable to set maxFailedCaptchaLoginAttempts.</i> 
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
