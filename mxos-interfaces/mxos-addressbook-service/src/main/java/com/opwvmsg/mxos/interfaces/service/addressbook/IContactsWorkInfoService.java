/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Contact WorkInfo Object level operations like Read and Update.
 * 
 * @author mxos-dev
 */
public interface IContactsWorkInfoService {

    /**
     * This operation is responsible for reading WorkInfo.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @return returns WorkInfo POJO.
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_WORKINFO_UNABLE_TO_GET - <i>Error in getting Contacts Workinfo.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    WorkInfo read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating WorkInfo.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>company</b> 
     *     <i>name of the company of contact</i>
     *     Type: String, Maximum Length: 128
     *     Example: Openwave
     * <b>department</b> 
     *     <i>department of contact</i>
     *     Type: String, Maximum Length: 128
     *     Example: Messaging
     * <b>title</b> 
     *     <i>designation of contact</i>
     *     Type: String, Maximum Length: 64
     *     Example: Executive
     * <b>manager</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Shawn
     * <b>employeeId</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Janice
     * <b>webPage</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: openwave.com
     * <b>assistant</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Janice
     * <b>assistantPhone</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: 9383840543
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_COMPANY - <i>Invalid company.</i>
     * ABS_INVALID_URL - <i>Invalid web page.</i>
     * ABS_INVALID_DEPARTMENT - <i>Invalid department.</i>
     * ABS_INVALID_TITLE - <i>Invalid title.</i>
     * ABS_INVALID_MANAGER - <i>Invalid manager name.</i>
     * ABS_INVALID_ASSISTANT - <i>Invalid assistant.</i>
     * ABS_INVALID_ASSISTANT_PHONE - <i>Invalid assistant phone.</i>
     * ABS_INVALID_EMPLOYEE - <i>Invalid employee Id.</i>
     * ABS_WORKINFO_UNABLE_TO_UPDATE - <i>Error in getting Contacts Workinfo.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

}
