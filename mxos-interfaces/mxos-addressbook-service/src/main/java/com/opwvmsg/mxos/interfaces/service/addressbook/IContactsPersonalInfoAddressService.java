/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Contact PersonalInfo Address Object level operations like Read
 * and Update.
 * 
 * @author mxos-dev
 */
public interface IContactsPersonalInfoAddressService {

    /**
     * This operation is responsible for reading PersonalInfo Address.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b> 
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @return returns WorkInfo Address POJO.
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_CONTACTNAME_ADDRESS_UNABLE_TO_GET - <i>Error in getting Contacts Name.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    Address read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating PersonalInfo Address.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>street</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Gallant Street
     * <b>city</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Stanford
     * <b>stateOrProvince</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: California
     * <b>postalCode</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: 94305
     * <b>country</b> 
     *    <i>Allowed value - String</i>
     *    Type: String, Maximum Length: 64
     *    Example: United States of America
     * <b>personalInfoAddress</b> 
     *    <i>Allowed value - String</i>
     *    Type: String
     *    Example: San Jose, United States of America
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_PERSONALINFO_INVALID_STREET - <i>Invalid contact street.</i>
     * ABS_PERSONALINFO_INVALID_CITY - <i>Invalid contact city.</i>
     * ABS_PERSONALINFO_INVALID_STATE_OR_PROVINCE - <i>Invalid contact state or province.</i>
     * ABS_PERSONALINFO_INVALID_POSTAL_CODE - <i>Invalid contact postal code.</i>
     * ABS_PERSONALINFO_INVALID_COUNTRY - <i>Invalid contact country.</i>
     * ABS_PERSONALINFO_ADDRESS_UNABLE_TO_UPDATE - <i>Error in updating Contacts PersonalInfo Address.</i>
     * ABS_PERSONALINFO_INVALID_ADDRESS - <i>Invalid personalInfo Address.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

}
