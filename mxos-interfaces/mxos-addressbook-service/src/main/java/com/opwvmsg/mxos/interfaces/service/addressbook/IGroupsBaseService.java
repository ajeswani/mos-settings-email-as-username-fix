/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Group Base Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public interface IGroupsBaseService {

    /**
     * This operation is responsible for searching Group Base.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>query</b>
     *     <i>search query for groups base</i>
     *     Type: String, Maximum Length: 255
     *     Example: query= firstName==John && lastName!= Does || displayName ~ John && displayName !~ Does && middleName ^= ab && birthday > 01-01-2001 && anniversary $= cd
     * <b>sortKey</b>
     *     <i>key for sorting groups base list</i>
     *     Type: String, Maximum Length: 20
     *     Example: sortKey=lastName
     * <b>sortOrder</b>
     *     <i>sort order for sorting groups base list</i>
     *     Type: String, can be one of 'ascending' or 'descending'
     *     Example: sortOrder=ascending
     * <b>filter</b>
     *     <i>filter criteria for groups base list (filtering is for displayName attribute only)</i>
     *     Type: String, not empty, one of 'ALL', ""ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ", "OTHER"
     *     Example: filter=ABC
     * 
     * </pre>
     * 
     *            <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * ABS_GROUPBASE_LIST_UNABLE_TO_GET - <i>Error in getting Groups Base List.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * ABS_INVALID_SEARCH_QUERY - <i>Invalid search query.</i>
     * ABS_INVALID_SEARCH_TERM - <i>Invalid search term.</i>
     * ABS_INVALID_SORT_KEY - <i>Invalid sort key.</i>
     * ABS_INVALID_SORT_ORDER - <i>Invalid sort order.</i>
     * ABS_INVALID_FILTER - <i>Invalid filter..</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     * @return returns list containing Group Base POJO.
     * @throws MxOSException MxOSException.
     */
    List<GroupBase> list(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading Group Base.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupId</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * 
     *            <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_GROUP - <i>Invalid group id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * ABS_GROUPSBASE_UNABLE_TO_GET - <i>Error in getting Groups Base.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     * @return returns Group Base POJO.
     * @throws MxOSException MxOSException.
     */
    GroupBase read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating Group Base.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>group</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>isPrivate</b> 
     *     <i>Allowed Values - "yes", no"</i>
     *     Type: String
     *     Example: yes
     * <b>colorLabel</b> 
     *     <i>Any integer value which corresponds to color number(Can not be Empty)</i>
     *     Type: integer, Minimum Val: 1, Maximum Val: 10
     *     Example: 4
     * <b>categories</b> 
     *     <i>String containing comma separated categories. Order is preserved.</i>
     *     Type: String, Maximum Length: 128
     *     Example: Business
     * <b>groupName</b> 
     *     <i>Name of the group as text value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>groupDescription</b> 
     *     <i>Description of the group as text value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_GROUP - <i>Invalid group id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * ABS_GROUPSBASE_UNABLE_TO_UPDATE - <i>Error in updating Contacts Base.</i>
     * ABS_INVALID_IS_PRIVATE - <i>Invalid contact base private flag.</i> 
     * ABS_INVALID_COLOR_LABEL - <i>Invalid contact base color label.</i> 
     * ABS_INVALID_CATEGORIES - <i>Invalid contact base categories.</i> 
     * ABS_INVALID_GROUP_DESCRIPTION - <i>Invalid group description.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * ABS_INVALID_GROUP_NAME - <i>Invalid group name.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     * 
     * @throws MxOSException MxOSException.
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

}
