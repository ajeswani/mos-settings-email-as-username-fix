/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.SharedGroup;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Subscriptions Groups Object level operations like Create, Read
 * and Delete.
 * 
 * @author mxos-dev
 */
public interface ISubscriptionsSharedGroupsService {

    /**
     * This operation is responsible for creating subscriptions groups. It uses
     * one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @return returns unique identifier for group i.e. groupId.
     * @throws MxOSException MxOSException.
     */
    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting subscriptions groups. It uses
     * one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting all subscriptions groups. It
     * uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading subscriptions groups.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @return returns Notification POJO.
     * @throws MxOSException MxOSException.
     */
    SharedGroup read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading all the subscriptions groups.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @return returns Notification POJO.
     * @throws MxOSException MxOSException.
     */
    List<SharedGroup> readAll(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
