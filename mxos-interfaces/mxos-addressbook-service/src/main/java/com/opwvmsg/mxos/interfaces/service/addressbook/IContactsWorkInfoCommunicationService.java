/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Contact WorkInfo Communication Object level operations like Read
 * and Update.
 * 
 * @author mxos-dev
 */
public interface IContactsWorkInfoCommunicationService {

    /**
     * This operation is responsible for reading WorkInfo Communication.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     *  * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @return returns WorkInfo Communication POJO.
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_WORKINFO_COMMUNICATION_UNABLE_TO_GET - <i>Error in getting Contacts Workinfo Communication.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    Communication read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating WorkInfo Communication.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc    
     * <b>phone1</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4221
     * <b>phone2</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4216
     * <b>mobile</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4290
     * <b>fax</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4217
     * <b>email</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: user@openwave.com
     * <b>imAddress</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: imuser@openwave.com
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_WORKINFO_INVALID_PHONE1 - <i>Invalid contact phone1.</i>
     * ABS_WORKINFO_INVALID_PHONE2 - <i>Invalid contact phone2.</i>
     * ABS_WORKINFO_INVALID_MOBILE - <i>Invalid contact mobile.</i>
     * ABS_WORKINFO_INVALID_FAX - <i>Invalid contact fax.</i>
     * ABS_WORKINFO_INVALID_EMAIL - <i>Invalid contact email.</i>
     * ABS_WORKINFO_INVALID_IM_ADDRESS - <i>Invalid contact instant messenger address.</i>
     * ABS_WORKINFO_COMMUNICATION_UNABLE_TO_UPDATE - <i>Error in updating Contacts Workinfo Communication.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

}
