/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Subscription;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Contact PersonalInfo Subscriptions Object level operations like
 * Create, Read, Update and Delete.
 * 
 * @author mxos-dev
 */
public interface IContactsPersonalInfoSubscriptionsService {

    /**
     * This operation is responsible for creating a contact subscriptions. It
     * uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @return returns unique identifier for contact i.e. userId.
     * @throws MxOSException MxOSException.
     */
    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting contact subscription. It uses
     * one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting all contact subscriptions. It
     * uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading contact subscription.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @return returns Base POJO.
     * @throws MxOSException MxOSException.
     */
    Subscription read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading all the contact subscription.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @return returns Base POJO.
     * @throws MxOSException MxOSException.
     */
    List<Subscription> readAll(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating contact subscription.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
