/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Contact Name Object level operations like Read and Update.
 * 
 * @author mxos-dev
 */
public interface IContactsNameService {

    /**
     * This operation is responsible for reading Contact Name.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @return returns Name POJO.
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_CONTACTNAME_ADDRESS_UNABLE_TO_GET - <i>Error in getting Contacts Name.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    Name read(final Map<String, List<String>> inputParams) throws MxOSException;

    /**
     * This operation is responsible for updating Contact Name.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>firstName</b> 
     *     <i>First name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: John
     * <b>middleName</b> 
     *     <i>Middle name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: William
     * <b>lastName</b> 
     *     <i>Last name as string value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Jackson
     * <b>nickName</b> 
     *     <i>Nick name as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: John
     * <b>displayName</b>
     *     <i>Display name for a contact</i>
     *     Type: String, Maximum Length: 128
     *     Example: John
     * <b>prefix</b> 
     *     <i>Prefix as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: Mr
     * <b>suffix</b> 
     *     <i>Suffix as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: Sr.
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_FIRST_NAME - <i>Invalid contact first name.</i>
     * ABS_INVALID_MIDDLE_NAME - <i>Invalid contact middle name.</i>
     * ABS_INVALID_LAST_NAME - <i>Invalid contact last name.</i>
     * ABS_INVALID_NICK_NAME - <i>Invalid contact nick name.</i>
     * ABS_INVALID_DISPLAY_NAME - <i>Invalid contact display name.</i>
     * ABS_INVALID_PREFIX - <i>Invalid contact prefix.</i>
     * ABS_INVALID_SUFFIX - <i>Invalid contact suffix.</i>
     * ABS_CONTACTNAME_ADDRESS_UNABLE_TO_UPDATE - <i>Error in updating Contacts Name.</i>
     * 
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

}
