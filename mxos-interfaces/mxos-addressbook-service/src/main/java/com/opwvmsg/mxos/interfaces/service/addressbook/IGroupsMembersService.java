/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Groups Members Object level operations like Create, Read and
 * Delete.
 * 
 * @author mxos-dev
 */
public interface IGroupsMembersService {

    /**
     * This operation is responsible for creating group members. It uses one or
     * more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupId</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * <b>memberId</b>
     *     <i>Member Id, which is actually a contact Id in the addressBook</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7 
     *     
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @return returns group Id.
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_GROUP - <i>Invalid group id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_MEMBER_ID - <i>Invalid member id.</i>  
     * ABS_GROUPS_MEMBERS_UNABLE_TO_PUT - <i>Error in creating Group members.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * ABS_GROUPS_MEMBER_NOT_FOUND - <i>Group member not found error.</i>
     * ABS_GROUPS_MEMBER_ALREADY_EXISTS - <i>Group member already exists error.</i> 
     * ABS_GROUPS_MEMBER_DOES_NOT_EXIST - <i>Group member does not exists error.</i> 
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     ** @return returns unique identifier for address book i.e. userId.
     * @throws MxOSException MxOSException.
     */
    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting group members. It uses one or
     * more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupId</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     * <b>memberId</b>
     *     <i>Member Id, which is actually a contact Id in the addressBook</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7 
     *     
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_GROUP - <i>Invalid group id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_MEMBER_ID - <i>Invalid member id.</i>  
     * ABS_GROUPS_MEMBERS_UNABLE_TO_DELETE - <i>Error in creating Group members.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * ABS_GROUPS_MEMBER_NOT_FOUND - <i>Group member not found error.</i>
     * ABS_GROUPS_MEMBER_DOES_NOT_EXIST - <i>Group member does not exists error.</i> 
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     *             *
     * @throws MxOSException MxOSException.
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting all group members. It uses one
     * or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupId</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7    
     *     
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_GROUP - <i>Invalid group id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_GROUPS_MEMBERS_UNABLE_TO_DELETE - <i>Error in creating Group members.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading group member.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupId</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7
     * <b>memberId</b>
     *     <i>Member Id, which is actually a contact Id in the addressBook</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7     
     *     
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_GROUP - <i>Invalid group id.</i>
     * ABS_INVALID_MEMBER_ID - <i>Invalid group member id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_GROUPS_MEMBERS_UNABLE_TO_GET - <i>Error in getting Group members.</i>
     * ABS_GROUPS_MEMBER_DOES_NOT_EXIST - <i>Group members does not exist error.</i> 
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * @return returns Group Member POJO.
     * @throws MxOSException MxOSException.
     */
    Member read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading all the group member.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupId</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7
     *     
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_GROUP - <i>Invalid group id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_GROUPS_MEMBERS_UNABLE_TO_GET - <i>Error in getting Group members.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * @return returns list of all the Group members.
     * @throws MxOSException MxOSException.
     */
    List<Member> readAll(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
