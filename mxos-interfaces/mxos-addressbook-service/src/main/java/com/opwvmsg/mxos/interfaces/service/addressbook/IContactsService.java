/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Contact Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public interface IContactsService {

    /**
     * This operation is responsible for creating a Contact. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com    
     * 
     * <b>inputParams - Optional</b>
     * <b>displayName</b>
     *     <i>Display name for a contact</i>
     *     Type: String, Maximum Length: 128
     *     Example: John
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc    
     * <b>folder</b> 
     *     <i>Open Xchange folder where the user should be created, default is private folder (22)</i>
     *     Type: integer, Minimum Val:0, Maximum Val:2147483647
     *     Example: 37
     * <b>notes</b> 
     *     <i>Notes for the contact as text value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>pager</b> 
     *     <i>Pager value which gets stored in custom fields</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>yomiFirstName</b> 
     *     <i>yomiFirstName value which gets stored in custom fields. To support Furigana fields </i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>yomiLastName</b> 
     *     <i>yomiLastName value which gets stored in custom fields. To support Furigana fields</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>yomiCompany</b> 
     *     <i>yomiCompany value which gets stored in custom fields. To support Furigana fields</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>email3</b> 
     *     <i>email3 value which gets stored in custom fields.</i>     *    
     *     Example: email3@test.com
     * <b>fileName</b> 
     *     <i>fileName value which gets stored in custom fields.</i>     *    
     *     Example: fileName    
     * <b>firstName</b> 
     *     <i>First name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: John
     * <b>middleName</b> 
     *     <i>Middle name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: William
     * <b>lastName</b> 
     *     <i>Last name as string value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Jackson
     * <b>nickName</b> 
     *     <i>Nick name as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: John
     * <b>prefix</b> 
     *     <i>Prefix as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: Mr
     * <b>suffix</b> 
     *     <i>Suffix as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: Sr.
     * <b>isPrivate</b> 
     *     <i>Allowed Values - "yes", no"</i>
     *     Type: String
     *     Example: yes
     * <b>colorLabel</b> 
     *     <i>Any integer value which corresponds to color number(Can not be Empty)</i>
     *     Type: integer, Minimum Val: 1, Maximum Val: 10
     *     Example: 4
     * <b>categories</b> 
     *     <i>String containing comma separated categories. Order is preserved.</i>
     *     Type: String, Maximum Length: 128
     *     Example: Business
     * <b>notes</b> 
     *     <i>Notes for the contact as text value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>maritalStatus</b> 
     *     <i>Married/Single/Divorcee</i>
     *     Type: String, Either of Married/Single/Divorcee
     *     Example: Single
     * <b>spouseName</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Julie
     * <b>personalInfoStreet</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Gallant Street
     * <b>personalInfoCity</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Stanford
     * <b>personalInfoStateOrProvince</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: California
     * <b>personalInfoPostalCode</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: 94305
     * <b>personalInfoCountry</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: United States of America
     * <b>personalInfoPhone1</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20 
     *     Example: 1-760-987-1234
     * <b>personalInfoPhone2</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-760-987-5678
     * <b>personalInfoMobile</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example:  1-760-987-3476
     * <b>personalInfoFax</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-760-987-2873
     * <b>personalInfoEmail</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: test@openwave.com
     * <b>personalInfoImAddress</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: testim@openwave.com
     * <b>companyName</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Openwave
     * <b>department</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Messaging
     * <b>title</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Executive
     * <b>manager</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Shawn
     * <b>webPage</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: openwave.com
     * <b>assistant</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Janice
     * <b>assistantPhone</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: 9383840543
     * <b>employeeId</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: OM043
     * <b>workInfoStreet</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Harper Street
     * <b>workInfoCity</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Cody
     * <b>workInfoStateOrProvince</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Wyoming
     * <b>workInfoPostalCode</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: 82414
     * <b>workInfoCountry</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: USA
     * <b>workInfoPhone1</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-422
     * <b>workInfoPhone2</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4216
     * <b>workInfoMobile</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4290
     * <b>workInfoFax</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4217
     * <b>workInfoEmail</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: user@openwave.com
     * <b>workInfoImAddress</b> 
     *     <i>Allowed value - email address format</i>
     *     Type: String, Maximum Length: 128
     *     Example: imuser@openwave.com
     * 
     * </pre>
     * 
     * @return returns unique identifier for contact i.e. userId.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_DISPLAY_NAME - <i>Invalid contact display name.</i>
     * ABS_INVALID_FOLDER - <i>Invalid contact folder.</i>
     * ABS_INVALID_FIRST_NAME - <i>Invalid contact first name.</i>
     * ABS_INVALID_MIDDLE_NAME - <i>Invalid contact middle name.</i>
     * ABS_INVALID_LAST_NAME - <i>Invalid contact last name.</i>
     * ABS_INVALID_NICK_NAME - <i>Invalid contact nick name.</i>
     * ABS_INVALID_PREFIX - <i>Invalid contact prefix.</i>
     * ABS_INVALID_SUFFIX - <i>Invalid contact suffix.</i>
     * ABS_INVALID_IS_PRIVATE - <i>Invalid contact base private flag.</i>
     * ABS_INVALID_COLOR_LABEL - <i>Invalid contact base color label.</i>
     * ABS_INVALID_CATEGORIES - <i>Invalid contact base categories.</i>
     * ABS_INVALID_NOTES - <i>Invalid contact base notes.</i>
     * ABS_INVALID_MARITAL_STATUS - <i>Invalid contact marital status.</i>
     * ABS_INVALID_SPOUSE_NAME - <i>Invalid contact spouse name.</i>
     * ABS_PERSONALINFO_INVALID_STREET - <i>Invalid contact street.</i>
     * ABS_PERSONALINFO_INVALID_CITY - <i>Invalid contact city.</i>
     * ABS_PERSONALINFO_INVALID_STATE_OR_PROVINCE - <i>Invalid contact state or province.</i>
     * ABS_PERSONALINFO_INVALID_POSTAL_CODE - <i>Invalid contact postal code.</i>
     * ABS_PERSONALINFO_INVALID_COUNTRY - <i>Invalid contact country.</i>
     * ABS_PERSONALINFO_INVALID_PHONE1 - <i>Invalid contact phone1.</i>
     * ABS_PERSONALINFO_INVALID_PHONE2 - <i>Invalid contact phone2.</i>
     * ABS_PERSONALINFO_INVALID_MOBILE - <i>Invalid contact mobile.</i>
     * ABS_PERSONALINFO_INVALID_FAX - <i>Invalid contact fax.</i>
     * ABS_PERSONALINFO_INVALID_EMAIL - <i>Invalid contact email.</i>
     * ABS_PERSONALINFO_INVALID_IM_ADDRESS - <i>Invalid contact instant messenger address.</i>
     * ABS_INVALID_COMPANY - <i>Invalid company.</i>
     * ABS_INVALID_URL - <i>Invalid web page.</i>
     * ABS_INVALID_DEPARTMENT - <i>Invalid department.</i>
     * ABS_INVALID_TITLE - <i>Invalid title.</i>
     * ABS_INVALID_MANAGER - <i>Invalid manager name.</i>
     * ABS_INVALID_ASSISTANT - <i>Invalid assistant.</i>
     * ABS_INVALID_ASSISTANT_PHONE - <i>Invalid assistant phone.</i>
     * ABS_INVALID_EMPLOYEE - <i>Invalid employee Id.</i>
     * ABS_WORKINFO_INVALID_STREET - <i>Invalid contact street.</i>
     * ABS_WORKINFO_INVALID_CITY - <i>Invalid contact city.</i>
     * ABS_WORKINFO_INVALID_STATE_OR_PROVINCE - <i>Invalid contact state or province.</i>
     * ABS_WORKINFO_INVALID_POSTAL_CODE - <i>Invalid contact postal code.</i>
     * ABS_WORKINFO_INVALID_COUNTRY - <i>Invalid contact country.</i>
     * ABS_WORKINFO_INVALID_PHONE1 - <i>Invalid contact phone1.</i>
     * ABS_WORKINFO_INVALID_PHONE2 - <i>Invalid contact phone2.</i>
     * ABS_WORKINFO_INVALID_MOBILE - <i>Invalid contact mobile.</i>
     * ABS_WORKINFO_INVALID_FAX - <i>Invalid contact fax.</i>
     * ABS_WORKINFO_INVALID_EMAIL - <i>Invalid contact email.</i>
     * ABS_WORKINFO_INVALID_IM_ADDRESS - <i>Invalid contact instant messenger address.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for creating multiple Contacts. It uses one
     * or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>fileName</b>
     *     <i>Filename containing the below input parameters (one per line)</i>
     *     Type: String, Maximum Length: 256
     *     Example: file:///c:/import_contacts.txt    
     * 
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com    
     * 
     * <b>inputParams - Optional</b>
     * <b>displayName</b>
     *     <i>Display name for a contact</i>
     *     Type: String, Maximum Length: 128
     *     Example: John
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc    
     * <b>folder</b> 
     *     <i>Open Xchange folder where the user should be created, default is private folder (22)</i>
     *     Type: integer, Minimum Val:0, Maximum Val:2147483647
     *     Example: 37
     * <b>firstName</b> 
     *     <i>First name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: John
     * <b>middleName</b> 
     *     <i>Middle name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: William
     * <b>lastName</b> 
     *     <i>Last name as string value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Jackson
     * <b>nickName</b> 
     *     <i>Nick name as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: John
     * <b>prefix</b> 
     *     <i>Prefix as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: Mr
     * <b>suffix</b> 
     *     <i>Suffix as string value</i>
     *     Type: String, Maximum Length: 64
     *     Example: Sr.
     * <b>isPrivate</b> 
     *     <i>Allowed Values - "yes", no"</i>
     *     Type: String
     *     Example: yes
     * <b>colorLabel</b> 
     *     <i>Any integer value which corresponds to color number(Can not be Empty)</i>
     *     Type: integer, Minimum Val: 1, Maximum Val: 10
     *     Example: 4
     * <b>categories</b> 
     *     <i>String containing comma separated categories. Order is preserved.</i>
     *     Type: String, Maximum Length: 128
     *     Example: Business
     * <b>notes</b> 
     *     <i>Notes for the contact as text value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     * <b>maritalStatus</b> 
     *     <i>Married/Single/Divorcee</i>
     *     Type: String, Either of Married/Single/Divorcee
     *     Example: Single
     * <b>spouseName</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Julie
     * <b>personalInfoStreet</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Gallant Street
     * <b>personalInfoCity</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Stanford
     * <b>personalInfoStateOrProvince</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: California
     * <b>personalInfoPostalCode</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: 94305
     * <b>personalInfoCountry</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: United States of America
     * <b>personalInfoPhone1</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20 
     *     Example: 1-760-987-1234
     * <b>personalInfoPhone2</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-760-987-5678
     * <b>personalInfoMobile</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example:  1-760-987-3476
     * <b>personalInfoFax</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-760-987-2873
     * <b>personalInfoEmail</b> 
     *     <i>Allowed value - email address format</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"  
     *     Example: test@openwave.com
     * <b>personalInfoImAddress</b> 
     *     <i>Allowed value - email address format</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"  
     *     Example: testim@openwave.com
     * <b>companyName</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Openwave
     * <b>department</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Messaging
     * <b>title</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Executive
     * <b>manager</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Shawn
     * <b>webPage</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: openwave.com
     * <b>assistant</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Janice
     * <b>assistantPhone</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: 9383840543
     * <b>employeeId</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: OM043
     * <b>workInfoStreet</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 128
     *     Example: Harper Street
     * <b>workInfoCity</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Cody
     * <b>workInfoStateOrProvince</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: Wyoming
     * <b>workInfoPostalCode</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: 82414
     * <b>workInfoCountry</b> 
     *     <i>Allowed value - String</i>
     *     Type: String, Maximum Length: 64
     *     Example: USA
     * <b>workInfoPhone1</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-422
     * <b>workInfoPhone2</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4216
     * <b>workInfoMobile</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4290
     * <b>workInfoFax</b> 
     *     <i>Allowed value - phone numbers as integers</i>
     *     Type: String, Maximum Length: 20
     *     Example: 1-307-587-4217
     * <b>workInfoEmail</b> 
     *     <i>Allowed value - email address format</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"  
     *     Example: user@openwave.com
     * <b>workInfoImAddress</b> 
     *     <i>Allowed value - email address format</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"  
     *     Example: imuser@openwave.com
     * 
     * </pre>
     * 
     * @return returns unique identifier for contact i.e. userId.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_DISPLAY_NAME - <i>Invalid contact display name.</i>
     * ABS_INVALID_FOLDER - <i>Invalid contact folder.</i>
     * ABS_INVALID_FIRST_NAME - <i>Invalid contact first name.</i>
     * ABS_INVALID_MIDDLE_NAME - <i>Invalid contact middle name.</i>
     * ABS_INVALID_LAST_NAME - <i>Invalid contact last name.</i>
     * ABS_INVALID_NICK_NAME - <i>Invalid contact nick name.</i>
     * ABS_INVALID_PREFIX - <i>Invalid contact prefix.</i>
     * ABS_INVALID_SUFFIX - <i>Invalid contact suffix.</i>
     * ABS_INVALID_IS_PRIVATE - <i>Invalid contact base private flag.</i>
     * ABS_INVALID_COLOR_LABEL - <i>Invalid contact base color label.</i>
     * ABS_INVALID_CATEGORIES - <i>Invalid contact base categories.</i>
     * ABS_INVALID_NOTES - <i>Invalid contact base notes.</i>
     * ABS_INVALID_MARITAL_STATUS - <i>Invalid contact marital status.</i>
     * ABS_INVALID_SPOUSE_NAME - <i>Invalid contact spouse name.</i>
     * ABS_PERSONALINFO_INVALID_STREET - <i>Invalid contact street.</i>
     * ABS_PERSONALINFO_INVALID_CITY - <i>Invalid contact city.</i>
     * ABS_PERSONALINFO_INVALID_STATE_OR_PROVINCE - <i>Invalid contact state or province.</i>
     * ABS_PERSONALINFO_INVALID_POSTAL_CODE - <i>Invalid contact postal code.</i>
     * ABS_PERSONALINFO_INVALID_COUNTRY - <i>Invalid contact country.</i>
     * ABS_PERSONALINFO_INVALID_PHONE1 - <i>Invalid contact phone1.</i>
     * ABS_PERSONALINFO_INVALID_PHONE2 - <i>Invalid contact phone2.</i>
     * ABS_PERSONALINFO_INVALID_MOBILE - <i>Invalid contact mobile.</i>
     * ABS_PERSONALINFO_INVALID_FAX - <i>Invalid contact fax.</i>
     * ABS_PERSONALINFO_INVALID_EMAIL - <i>Invalid contact email.</i>
     * ABS_PERSONALINFO_INVALID_IM_ADDRESS - <i>Invalid contact instant messenger address.</i>
     * ABS_INVALID_COMPANY - <i>Invalid company.</i>
     * ABS_INVALID_URL - <i>Invalid web page.</i>
     * ABS_INVALID_DEPARTMENT - <i>Invalid department.</i>
     * ABS_INVALID_TITLE - <i>Invalid title.</i>
     * ABS_INVALID_MANAGER - <i>Invalid manager name.</i>
     * ABS_INVALID_ASSISTANT - <i>Invalid assistant.</i>
     * ABS_INVALID_ASSISTANT_PHONE - <i>Invalid assistant phone.</i>
     * ABS_INVALID_EMPLOYEE - <i>Invalid employee Id.</i>
     * ABS_WORKINFO_INVALID_STREET - <i>Invalid contact street.</i>
     * ABS_WORKINFO_INVALID_CITY - <i>Invalid contact city.</i>
     * ABS_WORKINFO_INVALID_STATE_OR_PROVINCE - <i>Invalid contact state or province.</i>
     * ABS_WORKINFO_INVALID_POSTAL_CODE - <i>Invalid contact postal code.</i>
     * ABS_WORKINFO_INVALID_COUNTRY - <i>Invalid contact country.</i>
     * ABS_WORKINFO_INVALID_PHONE1 - <i>Invalid contact phone1.</i>
     * ABS_WORKINFO_INVALID_PHONE2 - <i>Invalid contact phone2.</i>
     * ABS_WORKINFO_INVALID_MOBILE - <i>Invalid contact mobile.</i>
     * ABS_WORKINFO_INVALID_FAX - <i>Invalid contact fax.</i>
     * ABS_WORKINFO_INVALID_EMAIL - <i>Invalid contact email.</i>
     * ABS_WORKINFO_INVALID_IM_ADDRESS - <i>Invalid contact instant messenger address.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<Long> createMultiple(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for list of all Contacts.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>query</b>
     *     <i>search query for contacts</i>
     *     Type: String, Maximum Length: 255
     *     Example: query= firstName==John && lastName!= Does || displayName ~ John && displayName !~ Does && middleName ^= ab && birthday > 01-01-2001 && anniversary $= cd
     * <b>sortKey</b>
     *     <i>key for sorting contacts list</i>
     *     Type: String, Maximum Length: 20
     *     Example: sortKey=lastName
     * <b>sortOrder</b>
     *     <i>sort order for sorting contacts list</i>
     *     Type: String, can be one of 'ascending' or 'descending'
     *     Example: sortOrder=ascending
     * <b>filter</b>
     *     <i>filter criteria for contacts list (filtering is for displayName attribute only)</i>
     *     Type: String, not empty, one of 'ALL', ""ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ", "OTHER"
     *     Example: filter=ABC
     * 
     * </pre>
     * 
     * @return returns list of Contact POJO with all its sub-pojo's.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * ABS_CONTACTLIST_UNABLE_TO_GET - <i>Error in getting Contacts.</i>
     * ABS_PROVIDER_CONNECTION_ERROR - <i>Provider connection error.</i>
     * ABS_INVALID_SEARCH_QUERY - <i>Invalid search query.</i>
     * ABS_INVALID_SEARCH_TERM - <i>Invalid search term.</i>
     * ABS_INVALID_SORT_KEY - <i>Invalid sort key.</i>
     * ABS_INVALID_SORT_ORDER - <i>Invalid sort order.</i>
     * ABS_INVALID_FILTER - <i>Invalid filter..</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    List<Contact> list(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting contact. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com 
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting all contacts. It uses one or
     * more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>To be supported in future release</b>
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for moving all contacts to a folder. It
     * uses one or more actions to do this activity. This operation is only
     * specific for Open-Xchange backend.
     * 
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com 
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_CONTACTS_MULTIPLE_UNABLE_TO_MOVE - <i>Error in moving multiple Contacts.</i>
     * ABS_INVALID_MULTIPLE_MOVE_CONTACTS - <i>Invalid contact ids to move.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void moveMultiple(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
