/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.notify;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Notify subscriptions operations interface which will be exposed to the client. 
 * This interface is responsible for doing notify subscriptions related operations 
 * (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface ISubscriptionsService {

    /**
     * This operation is responsible to register (login) to notification server
     * for the given topic.It uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * 
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>topic</b>
     *     <i>Topic for which subscriptions are required. This can be mailboxId
     *     and folderId or any other arbitrarily formated String. IMAP creates a topic and subscriptions
     *     associated with it. Topic acts as key for multiple values(subscriptions).</i>
     *     Type: String, Maximum Length: 255
     *     Sample Format:- &lt;folder-id&gt;.&lt;notification-class&gt;.&lt;notification-type&gt; 
     *     Where:- folder-id=folder-UUID, notification-class="mss",notification-type: "message",  
     *     "add", "remove", or "flagschanged".
     *     Example: - imap.any.7001.33852bcf-2987-3644-898a-c9a45510dc96
     *     
     * <b>subscription</b>
     *     <i>Specifies the subscription/callback-URL to be stored against for the topic. 
     *     This can be arbitrary string/url. IMAP server may use this to store the clientId 
     *     and callback URL.</i>
     *     Type: String, Maximum Length: 1024
     *     Sample Format:- http://&lt;imap-host-name&gt;:&lt;listener-port&gt;/&lt;mailbox-id&gt;/&lt;folder-id&gt;/&lt;client-UUID&gt;
     *     Example:- http://192.168.0.104:6701/7001/33852bcf-2987-3644-898a-c9a45510dc96/37477412-7c63-11e2-a46b-9c0aaaaed21a
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     * 
     * NFT_INVALID_TOPIC - <i>Invalid topic format.</i>
     * NFT_INVALID_SUBSCRIPTION - <i>Invalid subscription format.</i>
     * NFT_UNABLE_TO_CREATE_SUBSCRIPTION - <i>Unable to perform Notify Subscription PUT operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible to get all the subscriptions from notification 
     * server for the given topic. It uses one or more actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * 
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>topic</b>
     *     <i>Topic for which subscriptions are required. This can be mailboxId
     *     and folderId or any other arbitrarily formated String. IMAP creates a topic and subscriptions
     *     associated with it. Topic acts as key for multiple values(subscriptions).</i>
     *     Type: String, Maximum Length: 255
     *     Sample Format:- &lt;folder-id&gt;.&lt;notification-class&gt;.&lt;notification-type&gt; 
     *     Where:- folder-id=folder-UUID, notification-class="mss",notification-type: "message",  
     *     "add", "remove", or "flagschanged".
     *     Example: - imap.any.7001.33852bcf-2987-3644-898a-c9a45510dc96
     * </pre>
     * 
     * @return List<String>
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     * 
     * NFT_INVALID_TOPIC - <i>Invalid topic format.</i>
     * NFT_UNABLE_TO_READ_SUBSCRIPTION - <i>Unable to perform Notify Subscription GET operation.</i>
     * NTF_SUBSCRIPTION_NOT_FOUND - <i>Given subscription not found for the topic.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is not supported 
     * 
     * @param inputParams
     *     Parameters given by user.
     */
    void update(final Map<String, List<String>> inputParams)
    throws MxOSException;
    
    
    /**
     * This operation is responsible to delete the subscription from the topic.
     * This operation can be used by IMAP server when the client unregisters or 
     * logs out. The subscription value should be the same used during registration 
     * It uses one or more actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * 
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>topic</b>
     *     <i>Topic for which subscriptions are required. This can be mailboxId
     *     and folderId or any other arbitrarily formated String. IMAP creates a topic and subscriptions
     *     associated with it. Topic acts as key for multiple values(subscriptions).</i>
     *     Type: String, Maximum Length: 255
     *     Sample Format:- &lt;folder-id&gt;.&lt;notification-class&gt;.&lt;notification-type&gt; 
     *     Where:- folder-id=folder-UUID, notification-class="mss",notification-type: "message",  
     *     "add", "remove", or "flagschanged".
     *     Example: - imap.any.7001.33852bcf-2987-3644-898a-c9a45510dc96
     *     
     * <b>subscription</b>
     *     <i>Specifies the subscription/callback-URL to be stored against for the topic. 
     *     This can be arbitrary string/url. IMAP server may use this to store the clientId 
     *     and callback URL.</i>
     *     Type: String, Maximum Length: 1024
     *     Sample Format:- http://&lt;imap-host-name&gt;:&lt;listener-port&gt;/&lt;mailbox-id&gt;/&lt;folder-id&gt;/&lt;client-UUID&gt;
     *     Example:- http://192.168.0.104:6701/7001/33852bcf-2987-3644-898a-c9a45510dc96/37477412-7c63-11e2-a46b-9c0aaaaed21a    
     * </pre> 
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     * 
     * NFT_INVALID_TOPIC - <i>Invalid topic format.</i>
     * NFT_INVALID_SUBSCRIPTION - <i>Invalid subscription format.</i>
     * NTF_SUBSCRIPTION_NOT_FOUND - <i>Given subscription not found for the topic.</i>
     * NFT_UNABLE_TO_DELETE_SUBSCRIPTION - <i>Unable to perform Notify Subscription DELETE operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
