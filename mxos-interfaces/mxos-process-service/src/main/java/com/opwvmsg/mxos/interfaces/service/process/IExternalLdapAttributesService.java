/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.process;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
/**
 * External Ldap Attributes Retrieve Service interface which will be exposed to the client.
 * This interface is responsible for retrieving external ldap attributes
 *
 * @author mxos-dev
 */
public interface IExternalLdapAttributesService {

    /**
     * This operation is responsible for retrieving ldap Attributes from external ldap server.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>filterValue</b>
     *     <i>filterValue value for which attributes will be retrieved</i>
     *     Type: Non empty Charset, Maximum Length: 257
     *     Example: 123344556
     * </pre>
     * 
     * @return Map of Attributes name and values.
     * @throws MxOSException MxOSException.
     */
    Map<String,List<String>> readAttributes(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
