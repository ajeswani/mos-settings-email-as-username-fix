/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.process;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Log to ExternalEntity Service interface which will be exposed to the client.
 * This interface is responsible for sending log to MAA Interface
 *
 * @author mxos-dev
 */
public interface ILogToExternalEntityService {

    /**
     * This operation is responsible for sending log to MAA Interface.
     * It uses one or more actions to do this activity.
     *
     * @param inputParams Parameters given by user.
     * @throws MxOSException if any.
     */
    void process(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
