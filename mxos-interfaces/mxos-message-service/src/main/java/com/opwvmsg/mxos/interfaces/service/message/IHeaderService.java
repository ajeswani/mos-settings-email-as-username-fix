/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Message Header operations interface which will be exposed to the client. This
 * interface is responsible for doing message header related operations (like
 * Create, Read, Update, Delete, etc.).
 * 
 * @author mxos-dev
 */
public interface IHeaderService {

    /**
     * This operation is responsible for read message header of given message.
     * 
     * @param inputParams Parameters given by user.
     * @return returns list of messages.
     * @throws MxOSException MxOSException.
     */
    Header read(Map<String, List<String>> inputParams) throws MxOSException;
}
