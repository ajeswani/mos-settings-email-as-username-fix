/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Message Meta Data interface which will be exposed to the client. This
 * interface is responsible for getting message meta data.
 * 
 * @author mxos-dev
 */
public interface IMetadataService {

    /**
     * This operation is responsible for reading message meta data.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name of the folder where the message resides</i>
     *     Type: String, Maximum Length: 255
     *     Example: openwave
     * <b>messageId</b>
     *     <i>Message Id of the message to be retrieved. This is unique for the system. max-length(excluding angular brackets)=255</i>
     *     Type: String, Maximum Length: 257
     *     Example: openwave123
     *     
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     *  
     * </pre>
     * 
     * @return returns MetaData POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     *     MBX_INVALID_EMAIL - <i>Invalid email</i>
     *     FLD_INVALID_FOLDERNAME - <i>Invalid folder name</i>
     *     MSG_INVALID_MESSAGE_ID - <i>Invalid message Id</i>
     *     MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value</i>
     *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
     *     MSG_UNABLE_TO_GET_MESSAGE_META_DATA - <i>Unable to get the message meta data</i>
     *     MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Metadata read(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    /**
     * This operation is responsible for reading multiple message meta data.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>folderName</b>
     *      <i>Folder Name of the folder where the message resides</i>
     *      Type: String, Maximum Length: 255
     * <b>messageId</b>
     *      <i>One or more MessageIds of the message to be retrieved. This is unique for the system. max-length(excluding angular brackets)=255.</i>
     *      Type: String, Maximum Length: 257
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for RME version 163</i>
     *        Type: String; Enum: false, true; EmptyAllowed: no
     * </pre>
     * 
     * @return MAP <messageId, MetaData POJO>.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     *      MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MSG_UNABLE_TO_GET_MESSAGE_META_DATA - <i>Unable to get the message metadata.</i>
     *      MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Map<String, Metadata> readMulti(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    /**
     * This operation is responsible for updating message meta data.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name of the folder where the message resides</i>
     *     Type: String, Maximum Length: 255
     *     Example: openwave
     * <b>messageId</b>
     *     <i>Message Id of the message to be retrieved. This is unique for the system. max-length(excluding angular brackets)=255</i>
     *     Type: String, Maximum Length: 257
     *     Example: openwave123
     *  
     *  
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagSeen</b>
     *     <i>Specifies whether the flag is seen, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagAns</b>
     *     <i>Specifies whether the flag is answer, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagFlagged</b>
     *     <i>Specifies whether the flag is flagged, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagDel</b>
     *     <i>Specifies whether the flag is delete, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false3
     * <b>flagRecent</b>
     *     <i>Specifies whether the the flag is recent, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagDraft</b>
     *     <i>Specifies whether the the flag is draft, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>keywords</b>
     *     <i>Keywords</i>
     *     Type: String, Maximum Length: 255; EmptyAllowed: no    
     *     
     * </pre>
     * 
     * @return returns nothing.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     *     MBX_NOT_FOUND - <i>The given email does not exist</i>
     *     FLD_INVALID_FOLDERNAME - <i>Invalid folder name</i>
     *     MSG_INVALID_MESSAGE_ID - <i>Invalid message Id</i>
     *     MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value</i>
     *     MSG_INVALID_FLAG_SEEN - <i>Invalid flag seen value</i>
     *     MSG_INVALID_FLAG_ANS - <i>Invalid flag answer value</i>
     *     MSG_INVALID_FLAG_FLAGGED - <i>Invalid flag flagged value</i>
     *     MSG_INVALID_FLAG_DEL - <i>Invalid flag delete value</i>
     *     MSG_INVALID_FLAG_RECENT - <i>Invalid flag recent value</i> 
     *     MSG_INVALID_FLAG_DRAFT - <i>Invalid flag draft value</i>
     *     MSG_INVALID_KEYWORDS - <i>Invalid Keywords</i>
     *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
     *     MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     *     MSG_UNABLE_TO_PERFORM_GET - <i>Unable to perform Get Message operation.(For RME version 163 only)</i>
     *     MSG_UNABLE_TO_PERFORM_UPDATE - <i>Unable to perform Update Message operation.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating the same flags for all messages.
     * 
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name of the folder where the message resides</i>
     *     Type: String, Maximum Length: 255
     *     Example: openwave
     * <b>messageId</b>
     *      <i>One or more MessageIDs which need to be updated</i>
     *      Type: String, Maximum Length: 257     
     *  
     *  
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagSeen</b>
     *     <i>Specifies whether the flag is seen, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagAns</b>
     *     <i>Specifies whether the flag is answer, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagFlagged</b>
     *     <i>Specifies whether the flag is flagged, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagDel</b>
     *     <i>Specifies whether the flag is delete, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false3
     * <b>flagRecent</b>
     *     <i>Specifies whether the the flag is recent, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>flagDraft</b>
     *     <i>Specifies whether the the flag is draft, allowed values are true/false</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>popDeletedFlag</b>
     *     <i>Pop deleted flag, allowed values are true/false. Only for RME version 175 and higher</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Sets the message as popDeleted.
     *     false - Sets the message as normal.     
     * <b>keywords</b>
     *     <i>Keywords</i>
     *     Type: String, Maximum Length: 255; EmptyAllowed: no    
     *     
     * </pre>
     * 
     * @return String.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     *     MBX_NOT_FOUND - <i>The given email does not exist</i>
     *     FLD_INVALID_FOLDERNAME - <i>Invalid folder name</i>
     *     MSG_INVALID_MESSAGE_ID - <i>Invalid message Id</i>
     *     MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value</i>
     *     MSG_INVALID_FLAG_SEEN - <i>Invalid flag seen value</i>
     *     MSG_INVALID_FLAG_ANS - <i>Invalid flag answer value</i>
     *     MSG_INVALID_FLAG_FLAGGED - <i>Invalid flag flagged value</i>
     *     MSG_INVALID_FLAG_DEL - <i>Invalid flag delete value</i>
     *     MSG_INVALID_FLAG_RECENT - <i>Invalid flag recent value</i> 
     *     MSG_INVALID_FLAG_DRAFT - <i>Invalid flag draft value</i>
     *     MSG_INVALID_KEYWORDS - <i>Invalid Keywords</i>
     *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
     *     MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     *     MSG_UNABLE_TO_PERFORM_GET - <i>Unable to perform Get Message operation.(For RME version 163 only)</i>
     *     MSG_UNABLE_TO_PERFORM_UPDATE - <i>Unable to perform Update Message operation.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void updateMulti(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for searching messages inside given folder.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name of the folder where the message resides</i>
     *     Type: String, Maximum Length: 255
     *     Example: openwave
     * 
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for RME Version 163</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>popDeletedFlag</b>
     *     <i>Pop deleted flag, allowed values are true/false. Only for RME version 175 and higher</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - The messages that are marked as pop deleted are returned as deleted messages.
     *     false - The messages that are marked as pop deleted are not returned as deleted.
     * <b>sortKey</b>
     *     <i>Fetch messages sorted based on particular index like MessageId/To/From/CC/Subject.</i>
     *     <i>If popDeletedFlag is present in request then allowed value for sortKey is messageId only. i.e. sortKey=messageId.</i>
     *     <i>For RME v163 sortKey would be ignored.</i>
     *     Type: String, Enum: {messageId, attachments, richMailFlag, size, uid, priority, subject, from, to, cc}; EmptyAllowed: no
     *     Example: messageId
     * <b>sortOrder</b>
     *     <i>Order of sorting result messages.</i>
     *     <i>For RME v163 sortOrder would be ignored</i>
     *     Type: String, Enum: {ascending, descending},  default=ascending (driven through configuration).
     * </pre>
     * 
     * 
     * @return returns List of MetaData.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * 
     * <b>Error Codes:</b>
     *     MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *     MSG_INVALID_MESSAGE_QUERY - <i>Invalid message query</i>
     *     MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     *     MSG_INVALID_POP_DELETE_FLAG - <i>Invalid Pop Deleted Flag.</i>
     *     MSG_INVALID_SORT_KEY - <i>Invalid sort key.</i>
     *     MSG_INVALID_SORT_ORDER - <i>Invalid sort order.</i>
     *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
     *     MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     *     MSG_UNABLE_TO_SEARCH_MESSAGE_META_DATA - <i>Unable to search the message meta data.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Map<String, Metadata> list(
            final Map<String, List<String>> inputParams) throws MxOSException;

    /**
     * This operation is responsible for retrieving MessageUUIDs and UIDs for Message Metadatas inside given folder.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name of the folder where the message resides</i>
     *     Type: String, Maximum Length: 255
     *     Example: openwave
     * 
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for RME version 163</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     Example: false
     * <b>popDeletedFlag</b>
     *     <i>Pop deleted flag, allowed values are true/false. Only for RME version 175 and higher</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Do not list the messages that are marked as pop deleted.
     *     false - List messages that are marked as pop deleted.
     * <b>mailboxId</b>
     *      <i>Mailbox Id of the user. It's a mandatory parameter if messageStoreHost is present</i>
     *      Type: Long
     * <b>messageStoreHost</b>
     *      <i>Mailbox Message Store host of the user. It's a mandatory parameter if mailboxId is present</i>
     *      Type: String, Maximum Length: 255
     * </pre>
     * 
     * 
     * @return Map of Metadata objects.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     *     MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *     FLD_INVALID_FOLDERNAME - <i>Invalid folder name</i>
     *     FLD_NOT_FOUND - <i>Given folder NOT found.</i>
     *     MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     *     MSG_INVALID_POP_DELETE_FLAG - <i>Invalid Pop Deleted Flag.</i>
     *     MBX_INVALID_MAILBOXID - <i>Invalid mailboxid.</i>
     *     MSG_INVALID_MESSAGE_STORE_HOST - <i>Invalid Message Store Host.</i>
     *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
     *     MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     *     MSG_MISSING_MAILBOXID_OR_MESSAGESTOREHOST_PARAM - <i>Unable to perform operation, messageStoreHost and mailboxId both are mandatory parameters if either is present.</i>
     *     MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST - <i>Unable to get the messages meta data uids list.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Map<String, Metadata> listUIDs(
            final Map<String, List<String>> inputParams) throws MxOSException;
    
    /**
     * This operation is responsible for searching sorted messages inside given folder. For search 
     * based on UID for RME v163 sliceStart,sliceEnd,sliceCount parameters are mandatory along 
     * with other mandatory parameters. sliceStart must be equal to sliceEnd. Hence, sliceCount 
     * must be equal to 1.Also, sliceStart,sliceEnd must be valid UIDs.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: bs000123@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name of the folder where the message resides</i>
     *     Type: String, Maximum Length: 255
     *     Example: openwave
     * <b>sortKey</b>
     *     <i>Fetch messages sorted based on particular index like To/From/CC/Subject.</i>
     *     <i>In search based on UID for RME v163 sorting can be done using uid only. i.e. sortKey=uid</i>
     *     Type: String, Enum: {messageId, attachments, richMailFlag, arrTime, size, uid, priority, subject, from, to, cc, date, threadId}; EmptyAllowed: no
     *     Example: messageId
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>sortOrder</b>
     *     <i>Order of sorting result messages.</i>
     *     <i>In search based on UID for RME v163 sortOrder would be ignored since search</i>
     *     <i>can be done for 1 UID only at a time</i>
     *     Type: String, Enum: {ascending, descending},  default=ascending (driven through configuration).
     * <b>sliceStart</b>
     *     <i>Represents start of the query range. A blank string means the smallest entry in the list.</i>
     *     <i>In search based on UID for RME v163 sliceStart must be a valid UID.</i>
     *     Type: String
     * <b>sliceEnd</b>
     *     <i>Represents end of the query range. A blank string means the largest entry in the list.</i>
     *     <i>In search based on UID for RME v163 sliceEnd must be a valid UID.</i>
     *     Type: String
     * <b>sliceCount</b>
     *     <i>Number of messages starting from sliceStart.</i>
     *     <i>In search based on UID for RME v163 pass sliceCount = 1 since at present</i>
     *     <i>single UID search is supported.</i>
     *     Type: String, Enum: {ascending, descending},  Default driven through configuration.  NOTE: To retrieve all the messages in given folder, please pass sliceCount = 2147483647 and sliceStart=""

     * </pre>
     * 
     * 
     * @return returns Map of Map of MetaData where key==MessageId and value==CorrespondingMetadata.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     *     MBX_INVALID_EMAIL - <i>Invalid email.</i>
     *     FLD_INVALID_FOLDERNAME - <i>Invalid folder name.</i>
     *     MSG_INVALID_SORT_KEY - <i>Invalid sort key.</i>
     *     MSG_INVALID_SLICE_START - <i>Invalid slice start.</i>
     *     MSG_INVALID_SLICE_END - <i>Invalid slice end.</i>
     *     MSG_INVALID_SLICE_COUNT - <i>Invalid slice count.</i>
     *     MSG_INVALID_SORT_ORDER - <i>Invalid sort order.</i>
     *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *     MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     *     MSG_UNABLE_TO_SEARCH_MESSAGE_META_DATA - <i>Unable to search the message meta data.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */

    Map<String, Map<String, Metadata>> search(final Map<String, List<String>> inputParams,
            SearchTerm searchTerm) throws MxOSException;
}
