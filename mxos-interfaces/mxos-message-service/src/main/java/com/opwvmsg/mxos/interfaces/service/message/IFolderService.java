/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Folder operations interface which will be exposed to the client. This
 * interface is responsible for doing folder related operations (like Create,
 * Read, Update, Delete, etc.). Note: Currently default system
 * folders(INBOX(case insensitive), SentMail and Trash(case sensitive) cannot be
 * created, deleted and updated only we can update the uidValidity(for
 * stateless) of these folders.
 * 
 * @author mxos-dev
 */
public interface IFolderService {

    /**
     * This operation is responsible for creating folder. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>folderName</b>
     *     <i>Name of the folder to be created, folder name is case sensitive.</i>
     *     Type: String, folderNameLength: 255, folderDepth: 30
     *     Example: TEST
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"
     *     
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false" 
     * 
     * <b>uidValidity</b>
     *     <i>uidValidity of a folder and this attribute is only allowed for stateless.</i>
     *     Type: long
     * </pre>
     * 
     * @return in case of stateless rme returns folderUUID of newly created
     *         folder while in case of stateful rme returns nothing.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     * MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     * FLD_INVALID_SUPRESS_MERS - <i>Invalid supressMers value.</i>
     * FLD_INVALID_UID_VALIDITY - <i>Invalid uidValidity value.</i>
     * FLD_UNABLE_TO_PERFORM_CREATE - <i>Unable to perform Create Folder operation.</i>
     * FLD_ALREADY_EXISTS - <i>Given folder already exists.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    String create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading folder. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>folderName</b>
     *     <i>Name of the folder to be created, folder name is case sensitive</i>
     *     Type: String, folderNameLength: 255, folderDepth: 30
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"
     *     
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"
     * </pre>
     * 
     * @return returns Folder POJO.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     * FLD_UNABLE_TO_PERFORM_GET - <i>Unable to perform Get Folder operation.</i>
     * FLD_NOT_FOUND - <i>Given folder NOT found.</i>
     * MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     * FLD_INVALID_SUPRESS_MERS - <i>Invalid supressMers value.</i> 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Folder read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating folder. It uses one or more
     * actions to do this activity. In case of stateless folderUUID(folderId) of
     * the folder to be renamed will get updated.
     * 
     * * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>folderName</b>
     *     <i>Name of the folder to be created, folder name is case sensitive.</i>
     *     Type: String, folderNameLength: 255, folderDepth: 30
     * 
     * <b>toFolderName</b>
     *     <i>New folder name, folder name is case sensitive</i>
     *     Type: String, Maximum Length: 255
     *     
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"    
     * <b>uidValidity</b>
     *     <i>uidValidity of a folder and this attribute is only allowed for stateless.</i>
     *     Type: long
     * <b>folderSubscribed</b>
     *     <i>folderSubscribed indicates if the folder is subscribed, allowed values are true/false and this attribute is only allowed for stateless</i>
     *     Type: String, Enum: "true", "false"
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     * FLD_INVALID_TO_FOLDERNAME - <i>Invalid toFolderName value.</i>
     * MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     * FLD_INVALID_SUPRESS_MERS - <i>Invalid supressMers value.</i>
     * FLD_INVALID_UID_VALIDITY - <i>Invalid uidValidity value.</i>
     * FLD_UNABLE_TO_PERFORM_UPDATE - <i>Unable to perform Update Folder operation.</i>
     * FLD_NOT_FOUND - <i>Given folder NOT found.</i>
     * FLD_ALREADY_EXISTS - <i>Given folder already exists.</i>
     * FLD_INVALID_FOLDER_SUBSCRIBED - <i>Invalid folderSubscribed value.</i>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting folder. It uses one or more
     * actions to do this activity.
     * 
     * * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>folderName</b>
     *     <i>Name of the folder to be created, folder name is case sensitive.</i>
     *     Type: String, folderNameLength: 255, folderDepth: 30
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"
     *     
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"        
     * 
     * <b>force</b>
     *     <i>If true, delete folder even if it's locked by a user.</i>
     *     Type: String, Enum: "true", "false"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     * MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     * FLD_INVALID_SUPRESS_MERS - <i>Invalid supressMers value.</i>
     * MSG_INVALID_FORCE - <i>Invalid force value.</i>
     * FLD_UNABLE_TO_PERFORM_DELETE - <i>Unable to perform Delete Folder operation.</i>
     * FLD_NOT_FOUND - <i>Given folder NOT found.</i>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    
    /**
     * This operation is responsible for deleting all folders. 
     * Note: This operation will clean up all the folders and messages and creates system folders back by deleting and recreating mailbox in mss.
     * 
     * * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * MBX_UNABLE_TO_DELETE - <i>Unable to perform Mailbox Delete operation.</i>
     * MBX_UNABLE_TO_CREATE - <i>Unable to perform Mailbox Delete operation.</i>
     * </pre>
     */
    void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading all the folders in mailbox. It
     * uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"
     *     
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation. Applicable for stateful only.</i>
     *     Type: String, Enum: "true", "false"
     * </pre>
     * 
     * @return returns list containing Folder POJO.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     * FLD_INVALID_SUPRESS_MERS - <i>Invalid supressMers value.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<Folder> list(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
