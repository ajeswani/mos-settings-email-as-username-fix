/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * Enum of all the Services.
 *
 * @author mxos-dev
 */
public enum ServiceEnum implements DataMap.Property {

    //AddressBook
    AddressBookService,
    AddressBookBaseService,
    ContactsService,
    ContactsBaseService,
    ContactsImageService,
    ContactsNameService,
    ContactsPersonalInfoService,
    ContactsPersonalInfoCommunicationService,
    ContactsPersonalInfoAddressService,
    ContactsPersonalInfoEventsService,
    ContactsWorkInfoService,
    ContactsWorkInfoCommunicationService,
    ContactsWorkInfoAddressService,
    GroupsService,
    GroupsBaseService,
    GroupsMemberService,
    GroupsExternalMemberService,

    ExternalLoginService,
    
    //COS
    CosService,
    CosBaseService,
    CosGeneralPreferencesService,
    CosCredentialsService,
    CosMailStoreService,
    CosExternalStoreService,
    CosWebMailFeaturesService,
    CosWebMailFeaturesAddressBookService,
    CosInternalInfoService,
    CosMessageEventRecordsService,
    CosMailSendService,
    CosSmsServicesService,
    CosSmsOnlineService,
    CosMailAccessService,
    CosExternalAccountService,
    CosSocialNetworksService,
    CosMailReceiptService,
    CosMailReceiptFiltersService,
    CosMailReceiptSieveFiltersService,
    CosMailReceiptCloudmarkFiltersService,
    CosSenderBlockingService,

    // Domain
    DomainService,

    //Mailbox
    MailboxService,
    MailboxesService,
    MailboxBaseService,
    AllowedDomainService,
    MailAliasService,

    WebMailFeaturesService,
    WebMailFeaturesAddressBookService,
    CredentialService,
    GeneralPreferenceService,

    MailSendService,
    MailSendFiltersService,
    MailSendBMIFiltersService,
    MailSendCommtouchFiltersService,
    MailSendMcAfeeFiltersService,
    MailSendDeliveryMessagesPendingService,
    MailSendSignatureService,
    
    MailReceiptService,
    MailForwardService,
    AddressForLocalDeliveryService,
    MailReceiptBMIFiltersService,
    MailReceiptCommtouchFiltersService,
    MailReceiptCloudmarkFiltersService,
    MailReceiptFiltersService,
    MailReceiptMcAfeeFiltersService,
    SenderBlockingService,
    MailReceiptSieveFiltersService,
    AllowedSendersListService,
    BlockedSendersListService,
    SieveBlockedSendersService,
    //Admin Approved/Blocked senders list service
    AdminBlockedSendersListService,
    AdminApprovedSendersListService,

    MailAccessService,
    MailAccessAllowedIpService,

    MailStoreService,
    ExternalStoreService,
    GroupAdminAllocationsService,

    SocialNetworksService,
    SocialNetworkSiteService,

    ExternalAccountService,
    ExternalMailAccountService,

    SmsServicesService,
    SmsOnlineService,
    SmsNotificationsService,

    VoiceMailService,
    InternalInfoService,
    MessageEventRecordsService,

    AuthenticateService,
    AuthenticateAndGetMailboxService,
    CaptchaService,
    AdminControlService,
    MssLinkInfoService,
    
    // SAML Service
    SamlService,
    
    // Message Services
    FolderService,
    MessageService,
    MetadataService,
    HeaderService,
    BodyService,
    
    // Process
    SendSmsService,
    AuthorizeMsisdnService,
    DepositeMailService,
    LogToExternalEntityService,
    MsisdnDeactivationService,
    MsisdnReactivationService,
    MsisdnSwapService,
    SendMailService,
    ExternalLdapAttributesService,

    // Logging
    LoggingMailboxService,
    LoggingMailboxBaseService,
    LoggingCredentialsService,
    LoggingEmailAliasService,
    LoggingMailForwardService,
    LoggingMailReceiptService,

    //Notufy Services
    NotifyService,
    SubscriptionsService,
    
    //Tasks
    TasksService,
    TasksBaseService,
    TasksRecurrenceService,
    TasksParticipantService,
    TasksExternalParticipantService,
    TasksDetailsService,
    TasksAttachmentService,
    
    // Enums added for Backend Stats
    // LDAP Stats
    ActionGetMailboxTime,
    ActionSearchMailboxBaseTime,
    ActionAuthenticateTime,
    ActionAuthenticateReadMailboxTime,
    LdapObjAcquireTime,
    LdapSearchObjAcquireTime,
    LDAPGetMailboxTime,
    LDAPSearchMailboxTime,
    LDAPGetCosTime,
    LDAPGetCosCacheTime
}
