/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.common;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to External Login operations.
 * For example, we should login to OX for AddressBook and Tasks.
 * 
 * @author mxos-dev
 */
public interface IExternalLoginService {

    /**
     * This operation is responsible for logging in. It uses one or more actions
     * to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>password</b>
     *     <i>Password for user in LDAP store</i>
     *     Type: String
     *     Regex Pattern: "[a-zA-Z0-9+/=-]{1,64}"
     *     Example: Pa$$W0rD#1234
     * 
     * <b>Optional Parameters not Allowed</b>
     * </pre>
     * 
     * @return returns unique identifier for address book i.e. userId.
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_PASSWORD - <i>Invalid password.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    ExternalSession login(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for logging in. It uses one or more actions
     * to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * <b>Optional Parameters not Allowed</b>
     * </pre>
     * 
     * @return returns unique identifier for address book i.e. userId.
     * @throws MxOSException MxOSException.
     * 
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_PASSWORD - <i>Invalid password.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void logout(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
