/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * The MxOSServiceLoader class is used to load the MxOS services from
 * configuration mxos-services.xml.
 *
 * @author mxos-dev
 */
public class ServiceLoader {
    private static Logger logger = Logger.getLogger(ServiceLoader.class);

    /*
     * Possible contexts - stub, backend, rest. Sample mxos-services.xml
     * <contexts> <context name="Stub"> <service id="mailboxService"
     * className="com...StubMailboxService"/> ... </context> ... </contexts>
     */
    private static final String CONTEXT = "context";
    private static final String SERVICE = "service";
    private static final String ID = "id";
    private static final String CLASS_NAME = "className";

    /**
     * Default constructor.
     */
    protected ServiceLoader() {
    }

    /**
     * Parse XML by using local file.
     *
     * @param context name of context to load
     * @return IMxOSContext context Object
     * @throws MxOSException if any
     */
    public static IMxOSContext loadContext(
            DataMap.Property context) throws MxOSException {
        // TODO: The mxos-contexts.xml name should take from configuration
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        IMxOSContext contextObj = null;
        String fileName = "/mxos-contexts.xml";
        InputStream is = null;
        try {
            XMLInputFactory xmlif = XMLInputFactory.newInstance();
            XMLStreamReader xmlr = null;
            is = ServiceLoader.class.getResourceAsStream(fileName);
            xmlr = xmlif.createXMLStreamReader(fileName, is);
            String klass = parseContext(xmlr, context);
            // load class
            Object obj = getInstance(klass);
            if (obj != null && obj instanceof IMxOSContext) {
                contextObj = (IMxOSContext) obj;
            }
        } catch (Exception e) {
            logger.error(
                    "Error while loading mxos-contexts.xml", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Error while loading mxos-contexts.xml");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return contextObj;
    }
    /**
     * Parse XML by using local file, which loads all the services.
     *
     * @param context services to load - based on the config property loadServices.
     * @return Map of service objects
     * @throws MxOSException if any
     */
    public static Map<String, Object> loadServices(
            final DataMap.Property context) throws MxOSException {
        final ServiceObjectEnum[] services;
        if (System.getProperties().containsKey(
                SystemProperty.loadServices.name())) {
            String loadServices = System
                    .getProperty(SystemProperty.loadServices.name());
            String[] servicesInConf = loadServices.split(",");
            services = new ServiceObjectEnum[servicesInConf.length];
            for (int i = 0; i < servicesInConf.length; i++) {
                services[i] = ServiceObjectEnum.valueOf(servicesInConf[i]);
            }
        } else {
            services = ServiceObjectEnum.values();
        }
        return loadServices(context, services);
    }

    /**
     * Parse XML by using local file, which loads only the given services.
     *
     * @param context services to load - based on the config property loadServices.
     * @param services to be loaded
     * @return Map of service objects
     * @throws MxOSException if any
     */
    public static Map<String, Object> loadServices(
            final DataMap.Property context, ServiceObjectEnum[] services) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        if (services == null || services.length == 0) {
            services = ServiceObjectEnum.values();
        }
        // TODO: The mxos-services.xml name should take from configuration
        Map<String, Object> servicesMap = new HashMap<String, Object>();
        for (ServiceObjectEnum service : services) {
            InputStream is = null;
            // file name as <ServiceObjectEnum>-Services.xml
            String fileName = new StringBuffer().append("/")
                    .append(service.name()).append("-Services.xml").toString();
            try {
                XMLInputFactory xmlif = XMLInputFactory.newInstance();
                XMLStreamReader xmlr = null;
                is = ServiceLoader.class.getResourceAsStream(fileName);
                xmlr = xmlif.createXMLStreamReader(fileName, is);
                parseServices(xmlr, context, servicesMap);
            } catch (XMLStreamException e) {
                // TODO Auto-generated catch block
                logger.error(
                        "XMLStreamException while loading ".concat(fileName), e);
            } catch (Exception e) {
                logger.error("Error while loading ".concat(fileName), e);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                        "Error while loading " + fileName);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return servicesMap;
    }

    /**
     * Parse the XML reader.
     *
     * @param xmlr XMLReader
     * @param context name of context to load.
     * @throws MxOSException in case of any error while parsing rules
     */
    private static String parseContext(
            final XMLStreamReader xmlr,
            final DataMap.Property context) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String k = null;
        int eventType = 0;
    LOOP:
        try {
            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                    case XMLEvent.END_DOCUMENT:
                        break LOOP;
                    case XMLEvent.START_ELEMENT:
                        String name = xmlr.getLocalName();
                        if (CONTEXT.equals(name)) {
                            String contextId = xmlr.getAttributeValue(null, ID);
                            if (contextId != null && contextId.equalsIgnoreCase(
                                    context.name())) {
                                k = xmlr.getAttributeValue(null, CLASS_NAME);
                                break LOOP;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            logger.error(
                    "Exception encountered while parsing XML reader to load context",
                    e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return k;
    }

    /**
     * Parse the XML reader.
     *
     * @param xmlr XMLReader
     * @param context name of context to load.
     * @param servieMap servieMap map
     * @throws MxOSException in case of any error while parsing rules
     */
    private static boolean parseServices(
            final XMLStreamReader xmlr,
            final DataMap.Property context,
            final Map<String, Object> servieMap) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        boolean ret = true;
        int eventType = 0;
        boolean contextFound = false;

    LOOP:
        try {
            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                    case XMLEvent.END_DOCUMENT:
                        break LOOP;
                    case XMLEvent.START_ELEMENT:
                        String name = xmlr.getLocalName();
                        if (CONTEXT.equals(name)) {
                            // Exit for other context
                            // Need not to load other context services
                            if (contextFound) {
                                break LOOP;
                            }
                            String contextId = xmlr.getAttributeValue(null, ID);
                            if (contextId != null && contextId.equalsIgnoreCase(
                                    context.name())) {
                                contextFound = true;
                            }
                        } else if (SERVICE.equals(name) && contextFound) {
                            String serviceId = xmlr.getAttributeValue(null, ID);
                            String k = xmlr.getAttributeValue(null, CLASS_NAME);
                            // Load the service class with reflection
                            // load class
                            Object service = getInstance(k);
                            if (service != null) {
                                servieMap.put(serviceId, service);
                            }
                            // Register the service object to serviceMap
                            servieMap.put(serviceId, service);
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            logger.error(
                    "Exception encountered while parsing XML reader to load services",
                    e);
            ret = false;
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return ret;
    }

    private static Object getInstance(String klass) throws Exception {
        if (klass != null && !klass.equals("")) {
            // Used reflection to load the respective MxOS Context
            // It should be fine to use reflection, as it is executed
            // only once while starting the application
            logger.info("Loading Class = ".concat(klass));
            Class<?> c = Class.forName(klass);
            if (c != null) {
                return c.newInstance();
            }
        }
        throw new Exception("Unable to create instance of : " + klass);
    }
}
