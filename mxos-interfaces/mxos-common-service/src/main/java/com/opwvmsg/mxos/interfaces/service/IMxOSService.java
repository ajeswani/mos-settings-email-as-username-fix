/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Abstract MxOS Service.
 * 
 * @author mxos-dev
 */
public interface IMxOSService {

    <T> T setupRetryStrategy(T service, RetryStrategy retryStrategy)
            throws MxOSException;

}