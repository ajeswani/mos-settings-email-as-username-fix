/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Class contains All Message Error Keys.
 *
 * @author mxos-dev
 */
public enum MessageError {
    // Message params errors
    MSG_INVALID_MESSAGE_ID,
    MSG_INVALID_MESSAGE_TYPE,
    MSG_INVALID_MESSAGE_FROM,
    MSG_INVALID_MESSAGE_TO,
    MSG_INVALID_MESSAGE_HEADER_SUMMERY,
    MSG_INVALID_MESSAGE_BODY,
    INVALID_MESSAGE_SEARCH_QUERY,
    MSG_INVALID_SORT_ORDER,
    

    // Message APIs errors
    MSG_CREATE_MISSING_PARAMS,
    MSG_COPY_MISSING_PARAMS,
    MSG_GET_ALL_MISSING_PARAMS,
    MSG_GET_MISSING_PARAMS,
    MSG_GET_BODY_MISSING_PARAMS,
    MSG_UPDATE_MISSING_PARAMS,
    MSG_DELETE_MISSING_PARAMS,
    MSG_SEARCH_MISSING_PARAMS,
    MSG_INVALID_DATA,
    MSG_MISSING_MAILBOXID_OR_MESSAGESTOREHOST_PARAM,

    MSG_UNABLE_TO_PERFORM_CREATE,
    MSG_UNABLE_TO_PERFORM_GET,
    MSG_UNABLE_TO_PERFORM_UPDATE,
    MSG_UNABLE_TO_PERFORM_DELETE,
    MSG_UNABLE_TO_PERFORM_DELETE_ALL,
    MSG_UNABLE_TO_PERFORM_SEARCH,
    MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME,
    MSG_UNABLE_TO_PERFORM_COPY,    
    MSG_UNABLE_TO_PERFORM_COPY_ALL,
    MSG_ERROR_MOVE_SOURCE_DEST_CANNOT_BE_SAME,
    MSG_UNABLE_TO_PERFORM_MOVE,
    MSG_UNABLE_TO_PERFORM_STORE_MSG_FLAGS,
    MSG_UNABLE_TO_PERFORM_MOVE_ALL,
    // Message CRUD errors
    MSG_ALREADY_EXISTS,
    MSG_NOT_FOUND,
    MSG_INVALID_FORCE,
    MSG_INVALID_IS_ADMIN,
    MSG_UNABLE_TO_GET_MESSAGE_META_DATA,
    MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST,
    MSG_UNABLE_TO_SEARCH_MESSAGE_META_DATA,
    MSG_INVALID_FLAG_SEEN,
    MSG_INVALID_FLAG_ANS,
    MSG_INVALID_FLAG_FLAGGED,
    MSG_INVALID_FLAG_DEL,
    MSG_INVALID_FLAG_RES,
    MSG_INVALID_FLAG_RECENT,
    MSG_INVALID_FLAG_DRAFT,
    MSG_UNABLE_TO_GET_MESSAGE_SUMMARY,
    MSG_UNABLE_TO_GET_LAST_ACCESS_TIME,
    MSG_INVALID_MESSAGE_STORE_HOST,
    MSG_INVALID_ARRIVAL_TIME,
    MSG_INVALID_EXPIRY_TIME,
    MSG_INVALID_USER_FLAGS,
    MSG_UNABLE_TO_GET_MESSAGE_BODY,
    MSG_INVALID_SORT_KEY,
    MSG_UNABLE_TO_UPDATE_ROLLBACK_SUCCESS,
    MSG_UNABLE_TO_UPDATE_ROLLBACK_FAILED,
    MBX_QUOTA_LIMIT_ERROR
}

