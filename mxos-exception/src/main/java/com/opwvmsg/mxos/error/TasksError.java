/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Class contains All Tasks Error Keys.
 *
 * @author mxos-dev
 */
public enum TasksError {
    // OX Login/Logout API
    // OX Session Validation Errors
    TSK_INVALID_ENTITY,
    TSK_INVALID_USERNAME,
    TSK_INVALID_PASSWORD,
    TSK_INVALID_SESSION,
    TSK_INVALID_COOKIE,
    TSK_INVALID_FOLDER_ID,
    TSK_UNABLE_TO_LOGIN,
    TSK_UNABLE_TO_LOGOUT,

    // Tasks Errors
    TSK_TASKBASE_UNABLE_TO_CREATE,
    TSK_INVALID_NAME,
    TSK_INVALID_NOTES,
    
    TSK_INVALID_MULTIPLE_TASKS,
    TSK_INVALID_TASKID,
    TSK_TASKBASE_UNABLE_TO_GET,
    TSK_TASK_NOT_FOUND,
    
    TSK_INVALID_SORT_KEY,
    TSK_INVALID_SEARCH_TERM,
    TSK_UNABLE_TO_LISTTASKS,

    TSK_UNABLE_TO_TASKDELETE,
    TSK_UNABLE_TO_TASKDELETEALL,
}
