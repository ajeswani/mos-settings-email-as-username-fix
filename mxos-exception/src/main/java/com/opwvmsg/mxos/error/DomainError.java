/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Class contains All MxOS Error Keys.
 *
 * @author mxos-dev
 */
public enum DomainError {

    // Domain related Error keys.
    DMN_INVALID_NAME,
    DMN_INVALID_TYPE,
    DMN_INVALID_RELAYHOST,
    DMN_INVALID_REWRITTENDOMAIN,
    DMN_INVALID_DEFAULT_MAILBOX,
    DMN_INVALID_CUSTOM_FEILDS,
    // Domain API Errors
    DMN_UNABLE_TO_PERFORM_CREATE,
    DMN_UNABLE_TO_PERFORM_GET,
    DMN_UNABLE_TO_PERFORM_UPDATE,
    DMN_UNABLE_TO_PERFORM_DELETE,
    DMN_UNABLE_TO_PERFORM_SEARCH,
    DMN_MISSING_RELAYHOST,
    DMN_MISSING_ALTERNATEDOMAIN,
    // Domain CRUD Errors
    DMN_ALREADY_EXISTS,
    DMN_NOT_FOUND,
    DMN_DEFAUT_MAILBOX_NOT_FOUND

}

