/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Class contains All MxOS Error Keys.
 *
 * @author mxos-dev
 */
public enum ErrorCode {
    // Common Errors
    GEN_REQUEST_FORBIDDEN,
    GEN_BAD_REQUEST,
    GEN_INTERNAL_ERROR,
    GEN_METHOD_NOT_ALLOWED,
    // Invalid params
    GEN_INVALID_DATA,
    // Invalid Search params
    GEN_INVALID_SEARCH_QUERY,
    GEN_INVALID_SORT_QUERY,
    GEN_INVALID_SEARCH_MAXTIME_OUT,
    GEN_INVALID_SEARCH_MAXROWS,

    // LDAP Errors
    LDP_NAME_ALREADY_BOUND,
    LDP_INVALID_NAME,
    LDP_BAD_GRAMMAR,
    LDP_ATTRIBUTE_IN_USE,
    LDP_UNABLE_MODIFY_ATTRIBUTE,
    LDP_INVALID_ATTRIBUTE_VALUE,
    LDP_SCHEMA_VIOLATION,
    LDP_NO_SUCH_ATTRIBUTE,
    LDP_NAMING_ERROR,
    LDP_INVALID_DATA_ERROR,
    LDP_CONNECTION_ERROR,
    LDP_AUTHENTICATION_ERROR,
    LDP_SEARCH_TIMEOUT_ERROR,

    // MSS Error
    MSS_CONNECTION_ERROR,

    // POSIX related Error keys.
    PSX_CONNECTION_ERROR,

    // mOS(MxOS) related Error keys.
    MXS_GENERAL_ERROR,
    MXS_INPUT_ERROR,
    MXS_OUTPUT_ERROR,
    MXS_CONNECTION_ERROR,
    
    // AddressBook Open-Xchange error.
    ABS_CONNECTION_ERROR,
    
    // RMI Open-Xchange error.
    RMI_CONNECTION_ERROR,
    
    // MySQL Open-Xchange error.
    OXS_CONNECTION_ERROR,
    
    // Tasks Open-Xchange error.
    TASKS_CONNECTION_ERROR,
    
    // SDK Retry error.
    SDK_RETRY_ERROR
}
