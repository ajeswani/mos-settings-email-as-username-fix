/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Class contains All Folder Error Keys.
 *
 * @author mxos-dev
 */
public enum FolderError {
    // Folder Params errors
    FLD_INVALID_FOLDERID,
    FLD_INVALID_FOLDERNAME,
    FLD_INVALID_SRC_FOLDERNAME,
    FLD_INVALID_TO_FOLDERNAME,
    FLD_INVALID_PARENT_FOLDERID,
    FLD_INVALID_PARENT_FOLDERNAME,
    FLD_INVALID_UID_VALIDITY,
    // Folder APIs errors
    FLD_CREATE_MISSING_PARAMS,
    FLD_GET_MISSING_PARAMS,
    FLD_UPDATE_MISSING_PARAMS,
    FLD_DELETE_MISSING_PARAMS,
    FLD_SEARCH_MISSING_PARAMS,
    FLD_UNABLE_TO_PERFORM_CREATE,
    FLD_UNABLE_TO_PERFORM_GET,
    FLD_UNABLE_TO_PERFORM_UPDATE,
    FLD_UNABLE_TO_PERFORM_DELETE,
    FLD_UNABLE_TO_PERFORM_SEARCH,
    FLD_UNABLE_TO_PERFORM_LIST,
    // Folder CRUD errors
    FLD_ALREADY_EXISTS,
    FLD_NOT_FOUND
}

