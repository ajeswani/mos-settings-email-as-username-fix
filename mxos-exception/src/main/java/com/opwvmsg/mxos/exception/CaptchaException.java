/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.exception;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * Captcha Exception that can occur.
 *
 * @author mxos-dev
 */

public class CaptchaException extends Exception {

    private static final long serialVersionUID = -80008000L;
    private String code;
    private String requestParams;
    private String operationType;
    private String shortMessage;
    private String longMessage;

    /**
     * Default constructor with ErrorCode and Message.
     *
     * @param code - error code
     * @param message - error message
     */
    public CaptchaException(final String code, final String message) {
        super(message);
        this.code = code;
        this.shortMessage = message;
    }

    /**
     * Default constructor with ErrorCode and Exception.
     *
     * @param code - error code
     * @param t - original Throwable
     */
    public CaptchaException(final String code, final Throwable t) {
        super((t != null && t.getCause() != null) ? t.getCause().getMessage()
                : t.getMessage());
        this.code = code;
        this.shortMessage = (t != null && t.getCause() != null) ? t.getCause().getMessage()
                : t.getMessage();
    }

    /**
     * Default constructor with ErrorCode.
     *
     * @param code - error code
     */
    public CaptchaException(final String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the requestParams
     */
    public String getRequestParams() {
        return requestParams;
    }

    /**
     * @param requestParams the requestParams to set
     */
    public void setRequestParams(String requestParams) {
        this.requestParams = requestParams;
    }

    /**
     * @return the operationType
     */
    public String getOperationType() {
        return operationType;
    }

    /**
     * @param operationType the operationType to set
     */
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    /**
     * @return the shortMessage
     */
    public String getShortMessage() {
        return shortMessage;
    }

    /**
     * @param shortMessage the shortMessage to set
     */
    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    /**
     * @return the longMessage
     */
    public String getLongMessage() {
        return longMessage;
    }

    /**
     * @param longMessage the longMessage to set
     */
    public void setLongMessage(String longMessage) {
        this.longMessage = longMessage;
    }

    /**
     * Method to get String representation of this Error.
     *
     * @return String - error message
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("{").append("\"code\":\"")
                .append(this.code).append("\"");
        if (this.requestParams != null) {
            sb.append(",\"requestParams\":\"").append(
                    StringEscapeUtils.escapeJava(this.requestParams))
                    .append("\"");
        }
        if (this.operationType != null) {
            sb.append(",\"operationType\":\"").append(
                    StringEscapeUtils.escapeJava(this.operationType))
                    .append("\"");
        }
        if (this.shortMessage != null) {
            sb.append(",\"shortMessage\":\"").append(
                    StringEscapeUtils.escapeJava(this.shortMessage))
                    .append("\"");
        }
        if (this.longMessage != null) {
            sb.append(",\"longMessage\":\"").append(
                    StringEscapeUtils.escapeJava(this.longMessage))
                    .append("\"");
        }
        sb.append("}");

        return sb.toString();
    }
}
