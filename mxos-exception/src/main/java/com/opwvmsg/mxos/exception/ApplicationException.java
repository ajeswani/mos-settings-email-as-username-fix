/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.exception;

/**
 * An Exception that can occur for any unknown errors.
 *
 * @author mxos-dev
 */
public class ApplicationException extends MxOSException {
    private static final long serialVersionUID = -80008001L;

    /**
     * Default constructor with ErrorCode.
     *
     * @param errorCode - error code
     */
    public ApplicationException(final String errorCode) {
        super(errorCode);
    }

    /**
     * Default constructor with ErrorCode and Message.
     *
     * @param errorCode - error code
     * @param message - error message
     */
    public ApplicationException(final String errorCode, final String message) {
        super(errorCode, message);
    }

    /**
     * Default constructor with ErrorCode and Exception.
     *
     * @param errorCode - error code
     * @param t - original Throwable
     */
    public ApplicationException(final String errorCode, final Throwable t) {
        super(errorCode, t);
    }
}
