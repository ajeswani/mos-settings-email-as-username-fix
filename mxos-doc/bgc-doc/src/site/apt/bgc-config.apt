 ------------------------------------------------------------------------------
 BGC MxOS Configuration
 ------------------------------------------------------------------------------

BGC MxOS Config Parameters


 For MxOS generic configuration details please refer to
 

  * {{{../config.html}Generic MxOS Config Parameters}}


* BGC related custom configurations

** Following configurations are required for BGC in mxos.properties

** deploymentMode=bgc

 Deployment mode for Belgacom

** ldapCosObjectClasses=top,adminPolicy,mailuserprefs,msguserprefs,netmailuser,maillocaluserprefs, 
bgcuserprefs,smsUserPrefs,senderscontrol

 Default COS objectclasses

** loggingGateway=maa

 Default Logging Gateway
   
** maaEnabled=true

 Enable of Mail API Adapter

** maaPoolInitSize=50

 Initial size of MAA connection pool.

** maaPoolMaxSize=50

 Maximum size of MAA connection pool.

** maaTimeoutMS=10000

 Determines the timeout in milliseconds until a MAA connection is established.

* Configuration keys in gateway-config.xml

** snmpAgentPath=/config/snmp

 SNMP Agent default path

+--
Example:
<SMSGateways>
	<SendSMSGateways>
		<SendSMSGateway type="smsonline" Provider="com.opwvmsg.mxos.sms.custom.gateway.VampSendSMSGateway" >
			<URL>http://10.67.254.99:9350/vamp/interface/submit</URL>
			<Auth>nmpsmsodev</Auth>
			<Source>nmp_smsonline</Source>
			<Product>SMSOnline</Product>
			<ServiceCode>SMSOnlineSC</ServiceCode>
			<ContentType>text</ContentType>
			<SendSmsTempErrorList>454,455</SendSmsTempErrorList>
			<SendSmsTimeout>5000</SendSmsTimeout>
		</SendSMSGateway >
		<SendSMSGateway type="advancednotification" Provider="com.opwvmsg.mxos.sms.custom.gateway.FBISendSMSGateway" >
			<URL>http://10.67.254.97:80/fbi/sms</URL>
			<Auth>nmpadvnotif</Auth>
			<Source>nmp_advance_notif</Source>
			<Product>Mail_Advance_Notif</Product>
			<ServiceCode>Mail_AdvanceNotification</ServiceCode>
			<ContentType>text</ContentType>
			<SendSmsTempErrorList>454,455</SendSmsTempErrorList>
			<SendSmsTimeout>5000</SendSmsTimeout>
		</SendSMSGateway >
		<SendSMSGateway type="passwordrecoverysms" Provider="com.opwvmsg.mxos.sms.custom.gateway.MMGSendSMSGateway" >
			<SendSMSHost>rwcvrm50q0424.openwave.com</SendSMSHost>
			<SendSMSPort>16025</SendSMSPort>
			<SendSMSMaxConnections>10</SendSMSMaxConnections>
			<SendSmsTempErrorList>554</SendSmsTempErrorList>
			<SendSmsSnmpList>512</SendSmsSnmpList>
			<SendSmsTimeoutSnmpSend>true</SendSmsTimeoutSnmpSend>
		</SendSMSGateway>
		<SendSMSGateway type="basicnotification" Provider="com.opwvmsg.mxos.sms.custom.gateway.MMGSendSMSGateway" >
			<SendSMSHost>rwcvrm50q0424.openwave.com</SendSMSHost>
			<SendSMSPort>16025</SendSMSPort>
			<SendSMSMaxConnections>10</SendSMSMaxConnections>
			<SendSmsTempErrorList>554</SendSmsTempErrorList>
			<SendSmsSnmpList>512</SendSmsSnmpList>
			<SendSmsTimeoutSnmpSend>true</SendSmsTimeoutSnmpSend>
		</SendSMSGateway>
	</SendSMSGateways>
	<SendMailGateways>
		<SendMailGateway type="mta" Provider="com.opwvmsg.mxos.sms.custom.gateway.MTASendMailGateway" >
			<SendMailHost>localhost</SendMailHost>
			<SendMailPort>25</SendMailPort>
			<SendMailMaxConnections>10</SendMailMaxConnections>
		</SendMailGateway>
	</SendMailGateways>
	<LogToExternalEntityGateways>
		<LogToExternalEntityGateway type="maa" Provider="com.opwvmsg.mxos.sms.custom.gateway.MaaLogToExternalEntityGateway" >
			<LoggingURL>http://localhost:8080/mailadapter/log</LoggingURL>
			<AppLogin>opwv</AppLogin>
			<AppPassword>opwv</AppPassword>
			<LoggingTimeout>5000</LoggingTimeout>
		</LogToExternalEntityGateway>
	</LogToExternalEntityGateways>
	<AuthGateways>
		<AuthGateway type="smsonline" Provider="com.opwvmsg.mxos.sms.custom.gateway.SdupAuthorizationGateway">
			<Endpoint-URL>http://10.67.254.98:30080/SdupWs/services/SdupReaderService?wsdl</Endpoint-URL>
			<NameSpaceUri>http://reader.service.sdup.mobile.belgacom.be</NameSpaceUri>
			<ServiceName>SdupReaderService</ServiceName>
			<ClientApp>NewMailPlatform</ClientApp>
			<ccOffset>0,2</ccOffset>
			<ndcOffset>2,5</ndcOffset>
			<hlrOffset>5,7</hlrOffset>
			<regionOffset>7,9</regionOffset>
			<doGetUP>false</doGetUP>
			<ServiceProvider>BEMO,BEGA,BONE</ServiceProvider>
			<ServiceProviderType>BEMO,MVNO</ServiceProviderType>
			<Status>AB</Status>
			<SMSMoBarring>false</SMSMoBarring>
			<AuthTempErrorList>91,1000</AuthTempErrorList>
		</AuthGateway>
		<AuthGateway type="basicnotification" Provider="com.opwvmsg.mxos.sms.custom.gateway.SdupAuthorizationGateway">
			<Endpoint-URL>http://10.67.254.98:30080/SdupWs/services/SdupReaderService?wsdl</Endpoint-URL>
			<NameSpaceUri>http://reader.service.sdup.mobile.belgacom.be</NameSpaceUri>
			<ServiceName>SdupReaderService</ServiceName>
			<ClientApp>NewMailPlatform</ClientApp>
			<ccOffset>0,2</ccOffset>
			<ndcOffset>2,5</ndcOffset>
			<hlrOffset>5,7</hlrOffset>
			<regionOffset>7,9</regionOffset>
			<doGetUP>false</doGetUP>
			<ServiceProvider>BEMO,BEGA,BONE</ServiceProvider>
			<ServiceProviderType>BEMO,MVNO</ServiceProviderType>
			<Status>NA</Status>
			<SMSMoBarring>NA</SMSMoBarring>
			<AuthTempErrorList>91,1000</AuthTempErrorList>
		</AuthGateway>
		<AuthGateway type="advancednotification" Provider="com.opwvmsg.mxos.sms.custom.gateway.SdupAuthorizationGateway">
			<Endpoint-URL>http://10.67.254.98:30080/SdupWs/services/SdupReaderService?wsdl</Endpoint-URL>
			<NameSpaceUri>http://reader.service.sdup.mobile.belgacom.be</NameSpaceUri>
			<ServiceName>SdupReaderService</ServiceName>
			<ClientApp>NewMailPlatform</ClientApp>
			<ccOffset>0,2</ccOffset>
			<ndcOffset>2,5</ndcOffset>
			<hlrOffset>5,7</hlrOffset>
			<regionOffset>7,9</regionOffset>
			<doGetUP>false</doGetUP>
			<ServiceProvider>BEMO,BEGA,BONE</ServiceProvider>
			<ServiceProviderType>BEMO,MVNO</ServiceProviderType>
			<Status>AB</Status>
			<SMSMoBarring>false</SMSMoBarring>
			<AuthTempErrorList>91,1000</AuthTempErrorList>
		</AuthGateway>
		<AuthGateway type="passwordrecoverysms" Provider="com.opwvmsg.mxos.sms.custom.gateway.SdupAuthorizationGateway">
			<Endpoint-URL>http://10.67.254.98:30080/SdupWs/services/SdupReaderService?wsdl</Endpoint-URL>
			<NameSpaceUri>http://reader.service.sdup.mobile.belgacom.be</NameSpaceUri>
			<ServiceName>SdupReaderService</ServiceName>
			<ClientApp>NewMailPlatform</ClientApp>
			<ccOffset>0,2</ccOffset>
			<ndcOffset>2,5</ndcOffset>
			<hlrOffset>5,7</hlrOffset>
			<regionOffset>7,9</regionOffset>
			<doGetUP>false</doGetUP>
			<ServiceProvider>BEMO,BEGA,BONE</ServiceProvider>
			<ServiceProviderType>BEMO,MVNO</ServiceProviderType>
			<Status>NA</Status>
			<SMSMoBarring>NA</SMSMoBarring>
			<AuthTempErrorList>91,1000</AuthTempErrorList>
		</AuthGateway>
		<AuthGateway type="passwordrecoverymsisdn" Provider="com.opwvmsg.mxos.sms.custom.gateway.SdupAuthorizationGateway">
			<Endpoint-URL>http://10.67.254.98:30080/SdupWs/services/SdupReaderService?wsdl</Endpoint-URL>
			<NameSpaceUri>http://reader.service.sdup.mobile.belgacom.be</NameSpaceUri>
			<ServiceName>SdupReaderService</ServiceName>
			<ClientApp>NewMailPlatform</ClientApp>
			<ccOffset>0,2</ccOffset>
			<ndcOffset>2,5</ndcOffset>
			<hlrOffset>5,7</hlrOffset>
			<regionOffset>7,9</regionOffset>
			<doGetUP>false</doGetUP>
			<ServiceProvider>BEMO,BEGA,BONE</ServiceProvider>
			<ServiceProviderType>BEMO,MVNO</ServiceProviderType>
			<Status>NA</Status>
			<SMSMoBarring>NA</SMSMoBarring>
			<AuthTempErrorList>91,1000</AuthTempErrorList>
		</AuthGateway>
		<AuthGateway type="smsservicesmsisdn" Provider="com.opwvmsg.mxos.sms.custom.gateway.SdupAuthorizationGateway">
			<Endpoint-URL>http://10.67.254.98:30080/SdupWs/services/SdupReaderService?wsdl</Endpoint-URL>
			<NameSpaceUri>http://reader.service.sdup.mobile.belgacom.be</NameSpaceUri>
			<ServiceName>SdupReaderService</ServiceName>
			<ClientApp>NewMailPlatform</ClientApp>
			<ccOffset>0,2</ccOffset>
			<ndcOffset>2,5</ndcOffset>
			<hlrOffset>5,7</hlrOffset>
			<regionOffset>7,9</regionOffset>
			<doGetUP>false</doGetUP>
			<ServiceProvider>BEMO,BEGA,BONE</ServiceProvider>
			<ServiceProviderType>BEMO,MVNO</ServiceProviderType>
			<Status>NA</Status>
			<SMSMoBarring>NA</SMSMoBarring>
			<AuthTempErrorList>91,1000</AuthTempErrorList>
		</AuthGateway>
	</AuthGateways >
</SMSGateways>
+--
 
** Following existing MxOS 1.0 BGC configurations in config.db are used by MxOS 2.0

** /*/mxos/mailApiAdaptorUrl: []

 Mail API Adaptor URL
 
** /*/mxos/MAAappLogin : []

 Mail API Adaptor Login parameter value

** /*/mxos/MAAappPassword : []

 Mail API Adaptor Password parameter value

* Default attributes to be added during Create APIs.

** Create Domain API :

  Add the below Belgacom specific attributes in default-domain.properties.
  
+--
        # BGC Custom attributes for 
        bgcDomainMigrated: 0
+--

** Create Cos API :

  Add the below Belgacom specific attributes in default-cos.properties.
  
+--
        # BGC Custom attributes for Cos
        bgcAntiSpamAllowed: 0
        bgcAntiVirusAllowed: 0
        bgcForwardAllowed: 1
        bgcSmsNotificationsAllowed: 0
        bgcAutoReplyAllowed: 1
+--

** Create Mailbox API :

  Add the below Belgacom specific attributes in default-mailbox.properties.
  
+--
        # BGC Custom attributes for Mailbox
        bgcForwardingFailedCount: 0
        bgcMailSentForChangedStatus: 0
        # below attributes are required only for BGC (status)
        bgcMailboxMigrated: 0
        bgcEndofContract: 0
        bgcPending: 0
+--
