 ------------------------------------------------------------------------------
 BGC MAA Logging APIs
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

BGC MAA Logging APIs
 
 Logging operations interface which will be exposed to the client. This interface is responsible for doing logging related operations (like Create,
 Read, Update, Delete, etc.).

* Note: Use "custom" as "true" to access Logging APIs, while creating the IMxOSContext. Sample below.

+--
        final Properties p = new Properties();
        p.setProperty(MxOSContextFactory.MXOS_CONTEXT_ID,
                ContextEnum.REST.name());
        // MxOS 2.0 url
        p.setProperty("mxosBaseUrl", "http://localhost:8081/mxos");
        p.setProperty("mxosMaxConnections", "10");
        // To use MAA Logging APIs (via MxOS 2.0 Logging APIs)
        // true - To use logging APIs
        // false - To use direct APIs  (Default value - false)
        p.setProperty("custom", "true");
        // Get context object
        final IMxOSContext context = MxOSContextFactory.getContext(p);

+--

* MAA Logging APIs

* newAccount2 - To create mailbox via MAA 

 * {{{./logging/newAccount2.html}IMailboxService.create(�)}}
 
    This operation is responsible for creating mailbox via Maa.
  
* changeAccountAddAlias1 - To add new emailAlias via MAA

 * {{{./logging/changeAccountAddAlias1.html}IEmailAliasService.create(�)}}
 
    This operation is responsible for adding a new emailAlias via Maa.

* changeAccountUpdateAlias1 - To update an existing emailAlias via MAA

 * {{{./logging/changeAccountUpdateAlias1.html}IEmailAliasService.update(�)}}
 
    This operation is responsible for updating an existing emailAlias via Maa. 

* changeAccountDelAlias1 - To delete emailAlias via MAA

 * {{{./logging/changeAccountDelAlias1.html}IEmailAliasService.delete(�)}}
 
    This operation is responsible for deleting an emailAlias via Maa.

* changeAccountOptionsPref1 - To update bgcOptionsPref via MAA

 * {{{./logging/changeAccountOptionsPref1.html}IMailboxBaseService.update(�)}}
 
    This operation is responsible for updating options pref via Maa.

* changeAccountPassword1 - To update password via MAA

 * {{{./logging/changeAccountPassword1.html}IMailboxBaseService.update(�)}}
 
    This operation is responsible for updating the password via Maa. 

* changeAccountStatus1 - To update status via MAA

 * {{{./logging/changeAccountStatus1.html}IMailboxBaseService.update(�)}}
 
    This operation is responsible for updating the status via Maa. 

* changeAccountForward2 - To create or update forwardingAddress via MAA

 * {{{./logging/changeAccountForward2.html}IMailForwardService.create(�)}}
 
    This operation is responsible for creating or updating mail forwarding address via Maa.

* outOfOfficeSetReply - To Update autoReplyMessage via MAA

 * {{{./logging/outOfOfficeSetReply.html}IMailReceiptService.update(�)}}
 
    This operation is responsible for updating the autoReplyMessage via Maa.
 
* delAccount1 - To delete mailbox via MAA

 * {{{./logging/delAccount1.html}IMailboxService.delete(�)}}
 
    This operation is responsible for deleting mailbox via Maa.
                     