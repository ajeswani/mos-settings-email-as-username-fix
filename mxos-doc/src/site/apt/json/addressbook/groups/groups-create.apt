 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Groups - create API
 
 To Create a new Group

* API Description

 API to Create Group

** Invoking using REST Context in SDK - Return GroupId

 *  long IGroupsService.create(final Map<String, List<String>> inputParams) throws MxOSException;
 
** Invoking using REST Context in SDK - Return list of GroupIds

 *  long IGroupsService.createMultiple(final Map<String, List<String>> inputParams) throws MxOSException;
 
** Invoking using REST URL

 To create an individual group 

 * URL - PUT http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/groups

 To create multiple contacts at a time, upload a file with content-disposition name "file" (as input parameter "groupsList") to 

 * URL - POST http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/groups 

 The format of the file should be similar to the request made in URL query-

 individual parameter=value with each parameter separated by '&' and each group details on a new line and should be sent as multi-part data

 For example, file named groups.txt contains the following entries -

 groupName=abc
 groupName=ghi

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * groupName=<Group Name as text> 

** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** Optional Parameters
 
  * folderId=<folder where the user should be created>

** REST URL Response

 * Success Response - HTTP 200 with Group Id returned as long value.

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_INVALID_FOLDER" message="Invalid contact folder." />
    <error code="ABS_GROUP_UNABLE_TO_CREATE" message="Error in creating group." />
    <error code="ABS_INVALID_GROUP_NAME" message="Invalid group name." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 