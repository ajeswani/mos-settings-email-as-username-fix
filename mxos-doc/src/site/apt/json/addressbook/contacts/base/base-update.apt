 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Contacts Base - update API
 
 To Update AddressBook contact base attributes

* API Description

 Updates Contact Base attributes such as isPrivate, colorLabel, notes etc

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/\{contactId\}/base

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * contactId=<Contact Id, which is returned after calling create contact API>

** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** Optional Parameters (Any one is required)
  
  * isPrivate=<Allowed Values - "yes", no">
  
  * colorLabel=<Any integer value which corresponds to color number>
  
  * categories=<String containing comma separated categories. Order is preserved.>
  
  * notes=<Notes for the contact as text value>
  
  * pager=<Allowed value - phone numbers as integers>
 
  * yomiFirstName=<Allowed value - String Furigana field>
 
  * yomiLastName=<Allowed value - String Furigana field>

  * yomiCompany=<Allowed value - String Furigana field>
 
  * email3=<Allowed value - String>
  
  * fileName=<Allowed value - String>
  
  * uid=<Allowed value - String>  
  
  * otherPhone=<Other telephone number  Allowed value - String>
   
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_CONTACTBASE_UNABLE_TO_UPDATE" message="Error in updating Contacts Base." />
    <error code="ABS_INVALID_CREATED" message="Invalid contact created date." />
    <error code="ABS_INVALID_UPDATED" message="Invalid contact updated date." />
    <error code="ABS_INVALID_IS_PRIVATE" message="Invalid contact base private flag." />
    <error code="ABS_INVALID_COLOR_LABEL" message="Invalid contact base color label." />
    <error code="ABS_INVALID_CATEGORIES" message="Invalid contact base categories." />
    <error code="ABS_INVALID_NOTES" message="Invalid contact base notes." />
    <error code="ABS_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 