 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Workinfo - update API
 
 To Update Workinfo attributes

* API Description

 Updates WorkInfo attributes

** Invoking using SDK

 * void IContactsWorkInfoService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/\{contactId\}/workInfo
 
** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * contactId=<contact Id, which is returned after calling create contact API>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>
 
** Optional Parameters (Any one is required)

 * companyName= <name of the company of contact>
 
 * department= <department of contact>
 
 * employeeId= <Employee Id>
 
 * manager= <Manager of contact>
 
 * title= <designation of contact>
 
 * url= <company web page>
 
 * assistant= <Assistant of contact>
 
 * assistantPhone= <Assistant phone>
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_WORKINFO_UNABLE_TO_UPDATE" message="Error in updating Contacts Workinfo." />
    <error code="ABS_INVALID_COMPANY" message="Invalid company." />
    <error code="ABS_INVALID_DEPARTMENT" message="Invalid department." />
    <error code="ABS_INVALID_EMPLOYEE" message="Invalid employee Id." />
    <error code="ABS_INVALID_MANAGER" message="Invalid manager name." />
    <error code="ABS_INVALID_TITLE" message="Invalid title." />
    <error code="ABS_INVALID_URL" message="Invalid web page." />
    <error code="ABS_INVALID_ASSISTANT" message="Invalid assistant." />
    <error code="ABS_INVALID_ASSISTANT_PHONE" message="Invalid assistant phone." />
    <error code="ABS_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 
