 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Workinfo - Read API
 
 This API will be used to get WorkInfo POJO

* API Description

 API to read WorkInfo 

** Invoking using SDK - Returns WorkInfo POJO

 * WorkInfo IContactsWorkInfoService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns Workinfo JSON Object

 * URL - GET http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/\{contactId\}/workInfo

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * contactId=<contact Id, which is returned after calling create contact API>
 
** Mandatory Parameters (Only when using Open Xchange backend)
 
 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
 {
  "companyName": "5",
  "department": "16",
  "title": "",
  "manager": "",
  "assistant": "64",
  "assistantPhone": "14",
  "employeeId": "",
  "webPage": "15",
  - "address": {
    "street": "Street1",
    "city": "City1",
    "stateOrProvince": "State1",
    "postalCode": "12",
    "country": "Countr1"
  },
  - "communication": {
    "phone1": "123",
    "phone2": "123",
    "mobile": "Y3",
    "fax": "123",
    "email": "Y5@gh.com",
    "imAddress": ""
    "voip": "sip://test.com"
  }
 }
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_WORKINFO_UNABLE_TO_GET" message="Error in getting Contacts Workinfo." />
    <error code="ABS_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
