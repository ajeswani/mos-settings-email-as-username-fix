 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Workinfo Communication - update API
 
 To Update Workinfo Communication attributes

* API Description

 API to update WorkInfo Communication

** Invoking using SDK

 * void IContactsWorkInfoCommunicationService.update(final Map<String, List<String>> inputParams) throws MxOSException 

** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/\{contactId\}/workInfo/communication

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * contactId=<contact Id, which is returned after calling create contact API>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>
 
** Optional Parameters (Any one is required)

 * phone1=<landline number with country code>
 
 * phone2=<landline number with country code>
 
 * mobile=<mobile number with country code>
 
 * fax=<fax number of work>
 
 * email=<email address of work, allowed value valid email format>
 
 * imAddress=<IM address of work, allowed value String>
 
 * voip=<VOIP address of work>

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_WORKINFO_COMMUNICATION_UNABLE_TO_UPDATE" message="Error in updating Contacts Workinfo Communication." />
    <error code="ABS_WORKINFO_INVALID_PHONE1" message="Invalid contact phone1." />
    <error code="ABS_WORKINFO_INVALID_PHONE2" message="Invalid contact phone2." />
    <error code="ABS_WORKINFO_INVALID_MOBILE" message="Invalid contact mobile." />
    <error code="ABS_WORKINFO_INVALID_FAX" message="Invalid contact fax." />
    <error code="ABS_WORKINFO_INVALID_EMAIL" message="Invalid contact email." />
    <error code="ABS_WORKINFO_INVALID_IM_ADDRESS" message="Invalid contact instant messenger address." />
    <error code="ABS_WORKINFO_INVALID_VOIP" message="Invalid contact voip." />
    <error code="ABS_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 
