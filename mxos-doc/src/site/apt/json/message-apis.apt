 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Message APIs
 
 Message operations interface which will be exposed to the client. This interface is responsible for doing message related operations (like Create,
 Read, Update, Delete, etc.).

 
* createMessage - To create a message in a folder

 * {{{./message/message-create.html}IMessageService.create(�)}}
 
    This operation is responsible for creating message. It uses one or more actions to do this activity.

* readMessage - To get message by using messageId in a folder

 * {{{./message/message-get.html}IMessageService.read(�)}}
 
    This operation is responsible to getting message. It uses one or more actions to do this activity.

* deleteMessage - To delete message by using messageId in a folder

 * {{{./message/message-delete.html}IMessageService.delete(�)}}
 
    This operation is responsible for deleting a message. It uses one or more actions to do this activity.

* deleteMultipleMessage - To delete multiple messages in a folder

 * {{{./message/message-delete-multi.html}IMessageService.deleteMulti(�)}}

    This operation is responsible for deleting one or more messages from given folder. It uses one or more actions to do this activity.

* deleteAllMessage - To delete all messages in a folder

 * {{{./message/message-delete-all.html}IMessageService.deleteAll(�)}}

    This operation is responsible for deleting all messages from given folder. It uses one or more actions to do this activity.

* copyMessage - To copy message by using messageId from source folder to destination folder

 * {{{./message/message-copy.html}IMessageService.copy(�)}}
 
    This operation is responsible for copying message from source folder to destination folder. It uses one or more actions to do this activity.

* copyMultipleMessages - Copies multiple messages from one folder and its sub folders to another folder.

 * {{{./message/message-copy-multi.html}IMessageService.copyMulti(�)}}
 
    This operation is responsible for copying one or more messages from source folder to destination folder. It uses one or more actions to do this activity.

* copyAllMessages - To copy all the messages from source folder to destination folder

 * {{{./message/message-copy-all.html}IMessageService.copyAll(�)}}
 
    This operation is responsible for copying all the messages from source folder to destination folder. It uses one or more actions to do this activity.

* moveMessage - To move message by using messageId from source folder to destination folder

 * {{{./message/message-move.html}IMessageService.move(�)}}
 
    This operation is responsible for moving message from source folder to destination folder. It uses one or more actions to do this activity.

* moveMultipleMessages - To move multiple messages from source folder to destination folder

 * {{{./message/message-move-multi.html}IMessageService.moveMulti(�)}}
 
    This operation is responsible for moving one or more messages from source folder to destination folder. It uses one or more actions to do this activity.

* moveAllMessages - To move all the messages from source folder to destination folder

 * {{{./message/message-move-all.html}IMessageService.moveAll(�)}}
 
    This operation is responsible for moving all the messages from source folder to destination folder. It uses one or more actions to do this activity.

* readMetaData - read messages metadata of a particular message

 * {{{./message/metaData/message-getmeta.html}IMetadataService.read(�)}}
 
   This operation is responsible for read message metadata of a particular message. It uses one or more actions to do this activity.
   
* readMultiMetaData - read multiple message meta data in particular folder

 * {{{./message/metaData/message-getmeta-multi.html}IMetadataService.readMulti(�)}}
 
   This operation is responsible for read multiple message meta data in particular folder. It uses one or more actions to do this activity.
   
* listMetaData - list messages meta data in particular folder

 * {{{./message/metaData/message-listmeta.html}IMetadataService.list(�)}}
 
   This operation is responsible for list all the message meta data in particular folder. It uses one or more actions to do this activity.

* listUidsMetaData - list message UUIDs and UIDs of messages metadata in particular folder

 * {{{./message/metaData/message-uidslist-meta.html}IMetadataService.listUIDs(�)}}
 
   This operation is responsible for list all the message UUIDs and UIDs of messages metadata in particular folder. It uses one or more actions to do this activity.   

* searchSortedMetaData - search ordered messages meta data in particular folder

 * {{{./message/metaData/message-searchsorted-meta.html}IMetadataService.search(�)}}
 
   This operation is responsible to search metadata of all messages, API to support Keytype, sliceStart, sliceEnd, sliceCount and sliceReversed.    
   
* updateMetaData - update message metadata of a particular message

 * {{{./message/metaData/message-updatemeta.html}IMetadataService.update(�)}}
 
   This operation is responsible for update message metadata of a particular message. It uses one or more actions to do this activity.
   
* updateMultipleMessagesMetaData - update multiple messages meta data in particular folder

 * {{{./message/metaData/message-updatemeta-multi.html}IMetadataService.updateMulti(�)}}
 
   This operation is responsible for updating the same flags for all messages in particular folder. It uses one or more actions to do this activity.   

* Header - To get message header by using messageId in a folder

 * {{{./message/message-getheader.html}IHeaderService.read(�)}}
 
    This operation is responsible to get message header. It uses one or more actions to do this activity.
    
* Body - To get message body by using messageId in a folder

 * {{{./message/message-getbody.html}IBodyService.read(�)}}
 
    This operation is responsible to get message body. It uses one or more actions to do this activity.

* update - To update message body by using messageId in a folder

 * {{{./message/message-updatebody.html}IBodyService.update(�)}}
 
    This operation is responsible to update message body. It uses one or more actions to do this activity.
