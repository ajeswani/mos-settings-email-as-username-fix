 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------
 
Address Book APIs

* Login API
 
 * {{{./addressbook/login-user.html}IExternalLoginSerive.login()}}
 
    This operation is responsible for logging in.

* Logout API
 
 * {{{./addressbook/logout-user.html}IExternalLoginSerive.logout()}}
 
    This operation is responsible for logging out.
 
* AddressBook APIs
     
 * {{{./addressbook/addressbook-delete.html}IAddressBookService.delete()}}
 
    This operation is responsible for deleting address book for a user.
 
* Contact APIs
 
 * {{{./addressbook/contacts/contacts-create.html}IAddressBook.createContact()}}
 
    This operation is responsible for creating contact.
     
 * {{{./addressbook/contacts/contacts-delete.html}IAddressBook.deleteContact()}}
 
    This operation is responsible for deleting contact.
  
* Contacts Base APIs
 
  Provides Contacts Base Object level operations like Read, Update.
 
 * {{{./addressbook/contacts/base/base-read.html}IAddressBook.readContactBase()}}
 
    This operation is responsible for reading Contacts Base Object.
 
 * {{{./addressbook/contacts/base/base-readall.html}IAddressBook.readAllContactBase()}}
 
    This operation is responsible for reading all Contacts Base Object.
    
 * {{{./addressbook/contacts/base/base-update.html}IAddressBook.updateContactBase()}}
  
    This operation is responsible for updating Contacts Base Object.
      
* Contacts Name APIs
  
  Provides Contacts Name Object level operations like Read, Update.
  
 * {{{./addressbook/contacts/name/name-read.html}IAddressBook.readContactName()}}
 
    This operation is responsible for reading Contacts Name object.
 
 * {{{./addressbook/contacts/name/name-update.html}IAddressBook.updateContactName()}} 

    This operation is responsible for updating Contacts Name object.

* Contacts Image APIs
 
  Provides Contacts Image Object level operations like Read and Update. 
 
 * {{{./addressbook/contacts/contactsImage/contactsImage-read.html}IAddressBook.readContactsImage()}}
 
    This operation is responsible for reading Contacts Image Object. 
 
 * {{{./addressbook/contacts/contactsImage/contactsImage-update.html}IAddressBook.updateContactsImage()}}
 
    This operation is responsible for updating Contacts Image Object.         

* Contacts PersonalInfo APIs
 
  Provides Contacts PersonalInfo Object level operations like Read and Update.
 
 * {{{./addressbook/contacts/personalInfo/personalInfo-read.html}IAddressBook.readContactsPersonalInfo()}}
 
    This operation is responsible for reading Contacts PersonalInfo Object. 
 
 * {{{./addressbook/contacts/personalInfo/personalInfo-update.html}IAddressBook.updateContactsPersonalInfo()}}
 
    This operation is responsible for updating Contacts PersonalInfo Object.

* Contacts PersonalInfo/address APIs
 
  Provides Contacts PersonalInfo Address Object level operations like Read and Update.
 
 * {{{./addressbook/contacts/personalInfo/address/address-read.html}IAddressBook.readContactsPersonalInfoAddress()}}
 
    This operation is responsible for reading Contacts PersonalInfo Address Object. 
 
 * {{{./addressbook/contacts/personalInfo/address/address-update.html}IAddressBook.updateContactsPersonalInfoAddress()}}
 
    This operation is responsible for updating Contacts PersonalInfo Address Object.
        
* Contacts PersonalInfo/communication APIs
 
  Provides Contacts PersonalInfo Communication Object level operations like Read and Update.
 
 * {{{./addressbook/contacts/personalInfo/communication/communication-read.html}IAddressBook.readContactsPersonalInfoCommunication()}}
 
    This operation is responsible for reading Contacts PersonalInfo Communication Object. 
 
 * {{{./addressbook/contacts/personalInfo/communication/communication-update.html}IAddressBook.updateContactsPersonalInfoCommunication()}}
 
    This operation is responsible for updating Contacts PersonalInfo Communication Object.    
 
* Contacts PersonalInfo/events APIs
 
  Provides Contacts PersonalInfo Event Object level operations like Read and Update.
 
 * {{{./addressbook/contacts/personalInfo/events/events-read.html}IAddressBook.readContactPersonalInfoEvents()}}
 
     This operation is responsible for reading Contacts PersonalInfo Event Object. 
 
 * {{{./addressbook/contacts/personalInfo/events/events-update.html}IAddressBook.updateContactsPersonalInfoEvent()}}
 
    This operation is responsible for updating Contacts PersonalInfo Event Object.
    
* Contacts WorkInfo APIs
 
  Provides Contacts WorkInfo Object level operations like Read and Update. 
 
 * {{{./addressbook/contacts/workInfo/workinfo-read.html}IAddressBook.readContactsWorkInfo()}}
 
    This operation is responsible for reading Contacts WorkInfo Object. 
 
 * {{{./addressbook/contacts/workInfo/workinfo-update.html}IAddressBook.updateContactsWorkInfo()}}
 
    This operation is responsible for updating Contacts WorkInfo Object.
    
* Contacts WorkInfo/address APIs
 
  Provides Contacts WorkInfo Address Object level operations like Read and Update. 
 
 * {{{./addressbook/contacts/workInfo/address/address-read.html}IAddressBook.readContactsWorkInfoAddress)}}
 
    This operation is responsible for reading Contacts WorkInfo Address Object. 
 
 * {{{./addressbook/contacts/workInfo/address/address-update.html}IAddressBook.updateContactsWorkInfoAddress()}}
 
    This operation is responsible for updating Contacts WorkInfo Address Object.
    
* Contacts WorkInfo/communication APIs
 
  Provides Contacts WorkInfo Communication Object level operations like Read and Update. 
 
 * {{{./addressbook/contacts/workInfo/communication/communication-read.html}IAddressBook.readContactsWorkInfoCommunication()}}
 
    This operation is responsible for reading Contacts WorkInfo Communication Object. 
 
 * {{{./addressbook/contacts/workInfo/communication/communication-update.html}IAddressBook.updateContactsWorkInfoCommunication()}}
 
    This operation is responsible for updating Contacts WorkInfo Communication Object.    

* Group APIs             
 
 * {{{./addressbook/groups/groups-create.html}IAddressBook.createGroup()}}
 
    This operation is responsible for creating a Group.
    
 * {{{./addressbook/groups/groups-delete.html}IAddressBook.deleteGroup()}}
 
    This operation is responsible for deleting a Group.

* Group Base APIs
     
 * {{{./addressbook/groups/base/base-read.html}IAddressBook.readGroupBase()}}
 
    This operation is responsible for creating Group Base Object.
    
 * {{{./addressbook/groups/base/base-update.html}IAddressBook.updateGroupBase()}}
 
    This operation is responsible for updating Group Base Object.

* Group Members APIs
    
 * {{{./addressbook/groups/members/members-create.html}IAddressBook.createMember()}}
 
    This operation is responsible for creating Group Member Object.
    
 * {{{./addressbook/groups/members/members-read.html}IAddressBook.readMember()}}
 
    This operation is responsible for reading Group Member Object.
    
 * {{{./addressbook/groups/members/members-delete.html}IAddressBook.deleteMember()}}
 
    This operation is responsible for deleting Group Member Object.
    
* Group ExternalMembers APIs
    
 * {{{./addressbook/groups/externalMembers/externalMembers-create.html}IAddressBook.createExternalMember()}}
 
    This operation is responsible for creating Group External Member Object.
    
 * {{{./addressbook/groups/externalMembers/externalMembers-read.html}IAddressBook.readExternalMember()}}
 
    This operation is responsible for reading Group External Member Object.
    
 * {{{./addressbook/groups/externalMembers/externalMembers-delete.html}IAddressBook.deleteExternalMember()}}
 
    This operation is responsible for deleting Group External Member Object.
	

			 