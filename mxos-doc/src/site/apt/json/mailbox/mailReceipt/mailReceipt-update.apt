 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailReceipt - update API
 
 To update MailReceipt attributes

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://<host>:<port>/mxos/mailbox/v2/{email}/mailReceipt

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
** Optional Parameters

 * receiveMessages=<Controls whether users receive messages - yes|no>
 
 * forwardingEnabled=<Enables mail forwading by MTA - yes|no>
 
 * copyOnForward=<Indicates local delivery on or off - yes|no>
 
 * filterHTMLContent=<Whether the application filters (disables) HTML content and/or images when displaying received messages for security purposes.>
 
 * displayHeaders=<Subscriber-preferred setting that indicates whether the application displays HTML-formatted messages (those containing a text/html message part) as HTML or plain text.>
 
 * webmailDisplayWidth=<Message view width.>
 
 * webmailDisplayFields=<Subscriber-preferred mail-related fields, such as size, date, subject, that the web interface shows when displaying message lists.>
 
 * maxMailsPerPage=<Subscriber-preferred number of messages in a message list that the web interface displays on a page.>
 
 * previewPaneEnabled=<Specifies whether the email preview pane must be enabled - yes|no>
 
 * ackReturnReceiptReq=<Subscriber-preferred option for how the application responds to return receipt requests from inbound messages - always|ask|never>
 
 * deliveryScope=<Restricted to sending or receiving mail - local|global>
 
 * autoReplyMode=<Auto reply mode - none|vacation|reply|echo>
 
 * autoReplyMessage=<Auto reply message>
 
 * maxReceiveMessageSizeKB=<Maximum receive message size>
 
 * mobileMaxReceiveMessageSizeKB=<Max size in KB of the message that can be received by Mobile>
 
 * maxNumForwardingAddresses=<Maximum number of forwarding addresses allowed for the mailbox>
 
 ** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_RECEIVE_MESSAGES" message="Invalid revceive messages." />
    <error code="MBX_UNABLE_TO_SET_RECEIVE_MESSAGES" message="Unable to set receiveMessages." />
    <error code="MBX_INVALID_FORWARDING_ENABLED" message="Invalid forwarding enabled." />
    <error code="MBX_UNABLE_TO_SET_FORWARDING_ENABLED" message="Unable to set forwardingEnabled." />
    <error code="MBX_INVALID_COPY_ON_FORWARD" message="Invalid copyOnForward." />
    <error code="MBX_UNABLE_TO_SET_COPY_ON_FORWARD" message="Unable to set opyOnForward." />
    <error code="MBX_INVALID_FILTER_HTML_CONTENT" message="Invalid filter html content." />
    <error code="MBX_UNABLE_TO_SET_FILTER_HTML_CONTENT" message="Unable to set filterHTMLContent." />
    <error code="MBX_INVALID_DISPLAY_HEADERS" message="Invalid display headers." />
    <error code="MBX_UNABLE_TO_SET_DISPLAY_HEADERS" message="Unable to set displayHeaders." />
    <error code="MBX_INVALID_WEBMAIL_DISPLAY_WIDTH" message="Invalid webmail display width." />
    <error code="MBX_UNABLE_TO_SET_WEBMAIL_DISPLAY_WIDTH" message="Unable to set webmailDisplayWidth." />
    <error code="MBX_INVALID_WEBMAIL_DISPLAY_FIELDS" message="Invalid webmail display fields." />
    <error code="MBX_UNABLE_TO_SET_WEBMAIL_DISPLAY_FIELDS" message="Unable to set webmailDisplayFields." />
    <error code="MBX_INVALID_MAX_MAILS_PER_PAGE" message="Invalid max mails per page." />
    <error code="MBX_UNABLE_TO_SET_MAX_MAILS_PER_PAGE" message="Unable to set maxMailsPerPage." />
    <error code="MBX_INVALID_PREVIEW_PANE_ENABLED" message="Invalid preview pane enabled." />
    <error code="MBX_UNABLE_TO_SET_PREVIEW_PANE_ENABLED" message="Unable to set previewPaneEnabled." />
    <error code="MBX_INVALID_ACK_RETURN_RECEIPT_REQ" message="Invalid ack return receipt req." />
    <error code="MBX_UNABLE_TO_SET_ACK_RETURN_RECEIPT_REQ" message="Unable to set ackReturnReceiptReq." />
    <error code="MBX_INVALID_DELIVERY_SCOPE" message="Invalid delivery scope." />
    <error code="MBX_UNABLE_TO_SET_DELIVERY_SCOPE" message="Unable to set deliveryScope." />
    <error code="MBX_INVALID_AUTO_REPLY_MODE" message="Invalid auto reply mode." />
    <error code="MBX_UNABLE_TO_SET_AUTO_REPLY_MODE" message="Unable to set autoReplyMode." />
    <error code="MBX_INVALID_AUTO_REPLY_MESSAGE" message="Invalid auto reply message." />
    <error code="MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE" message="Unable to set autoReplyMessage." />
    <error code="MBX_INVALID_MAX_RECEIVE_MESSAGE_SIZE_KB" message="Invalid max receive message size." />
    <error code="MBX_INVALID_MOBILE_MAX_RECEIVE_MESSAGE_SIZE_KB" message="Invalid mobile max receive message size." />
    <error code="MBX_UNABLE_TO_SET_MOBILE_MAX_RECEIVE_MESSAGE_SIZE_KB" message="Unable to set mobileMaxReceiveMessageSizeKB." />
    <error code="MBX_UNABLE_TO_SET_MAX_RECEIVE_MESSAGE_SIZE_KB" message="Unable to set maxReceiveMessageSizeKB." />
    <error code="MBX_INVALID_MAXNUM_OF_FWDADDRSS" message="Invalid maximum allowed forward addresses." />
    <error code="MBX_UNABLE_TO_SET_MAXNUM_OF_FWDADDRSS" message="Unable to set maximum allowed forward addresses." />

+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 