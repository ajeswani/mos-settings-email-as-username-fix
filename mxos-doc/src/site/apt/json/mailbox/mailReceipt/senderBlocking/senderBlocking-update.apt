 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailReceipt SenderBlocking - update API
 
 To update SenderBlocking attributes

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://<host>:<port>/mxos/mailbox/v2/{email}/mailReceipt/senderBloking

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

** Optional Parameters

 * senderBlockingAllowed=<Sender blocking is allowed or not allowed for the subscriber - yes|no>
 
 * senderBlockingEnabled=<Sender blocking is enabled or disabled for the subscriber - yes|no>
 
 * rejectAction=<Rejection action - drop|forward>
 
 * rejectFolder=<Additional information associated with the reject action attribute>
 
 * blockSendersPABActive=<Sender blocking is active or not active for subscriber - yes|no>

 * blockSendersPABAccess=<Sender blocking is enabled or disabled for subscriber - yes|no>
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_SENDER_BLOCKING_ALLOWED" message="Invalid sender blocking allowed." />
    <error code="MBX_UNABLE_TO_SET_SENDER_BLOCKING_ALLOWED" message="Unable to set senderBlockingAllowed." />
    <error code="MBX_INVALID_SENDER_BLOCKING_ENABLED" message="Invalid sender blocked enabled." />
    <error code="MBX_UNABLE_TO_SET_SENDER_BLOCKING_ENABLED" message="Unable to set senderBlockingEnabled." />
    <error code="MBX_INVALID_REJECT_ACTION" message="Invalid reject action." />
    <error code="MBX_UNABLE_TO_SET_REJECT_ACTION" message="Unable to set rejectAction." />
    <error code="MBX_INVALID_REJECT_FOLDER" message="Invalid reject folder." />
    <error code="MBX_UNABLE_TO_SET_REJECT_FOLDER" message="Unable to set rejectFolder." />
    <error code="MBX_INVALID_BLOCK_SENDERS_PAB_ACTIVE" message="Invalid block senders pab active." />
    <error code="MBX_UNABLE_TO_SET_BLOCK_SENDERS_PAB_ACTIVE" message="Unable to set blockSendersPABActive." />
    <error code="MBX_INVALID_BLOCK_SENDERS_PAB_ACCESS" message="Invalid block senders pab access." />
    <error code="MBX_UNABLE_TO_SET_BLOCK_SENDERS_PAB_ACCESS" message="Unable to set blockSendersPABAccess." />

+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 