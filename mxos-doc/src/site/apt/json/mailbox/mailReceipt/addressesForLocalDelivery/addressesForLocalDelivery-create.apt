 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailReceipt addressesForLocalDelivery - create API
 
 To create MailReceipt addressesForLocalDelivery

* API Description

** Invoking using SDK

 *  void create(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL
 
 * For Single address for local delivery   
  URL - PUT https://<host>:<port>/mxos/mailbox/v2/{email}/mailReceipt/addressesForLocalDelivery/{addressForLocalDelivery}

 * For Multiple addresses for local delivery
  URL - PUT https://<host>:<port>/mxos/mailbox/v2/{email}/mailReceipt/addressesForLocalDelivery
 
** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

 * addressForLocalDelivery=<Subscriber's address for local delivery>
   Example: addressForLocalDelivery=foo@bar.com&addressForLocalDelivery=bar@foo.com&addressForLocalDelivery=xyz@bar.com
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_UNABLE_TO_LOCALDELIVERY_ADDRESS_CREATE" message="Unable to perform LocalDelivery Address Create operation." />
    <error code="MBX_LOCAL_DELIVERY_ADDRESS_REACHED_MAX_LIMIT" message="Number of local delivery addresses reached the maximum limit." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 