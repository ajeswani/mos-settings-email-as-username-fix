 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailReceipt Filters - read API
 
 To read MailReceipt Filters attributes

* API Description

** Invoking using SDK

 *  void read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET https://<host>:<port>/mxos/mailbox/v2/\{email\}/mailReceipt/filters

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
"sieveFilters": {
    "sieveFilteringEnabled": "yes",
    "mtaFilter": "if header 'from' contains-nocase 'ma000001' charset 'UTF-8' {fileinto 'Junk Mail';keep;stop;}",
    "rmFilter": "8XPkKnPK4qGXzeGi;version=1.0;filter=New rule1;enabled=true;forward=;folder=;delete=true;deliver=;match=any;condition:from,contains,aaa;charset=UTF-8",
    "blockedSenders": [
                   "barblcoked12@bar.com",
                    "barblcoked@foo.com"
    ],
    "blockedSenderAction": "bounce",
    "blockedSenderMessage": "blocked",
    "rejectBouncedMessage": "yes"
},
"bmiFilters": {
    "spamAction": "Discard",
    "spamMessageIndicator": "get free offer"
},
"commtouchFilters": {
    "spamAction": "Tag",
    "virusAction": "Tag"
},
"cloudmarkFilters": {
    "spamfilterEnabled": "yes",
    "spamPolicy": "mild",
    "spamAction": "delete",
    "cleanAction": "delete",
    "suspectAction": "delete"
},
"mcAfeeFilters": {
    "virusAction": "clean"
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_UNABLE_TO_MAILRECEIPT_GET" message="Unable to perform MailReceipt GET operation." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 