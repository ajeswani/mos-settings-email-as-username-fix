 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailReceipt Filters CloudmarkFilters - update API
 
 To update MailReceipt Filters CloudmarkFilters attributes

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://<host>:<port>/mxos/mailbox/v2/{email}/mailReceipt/filters/cloudmarkFilters

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

** Optional Parameters

 * spamfilterEnabled=<Spam Filter Enabled - 0-No | 1-yes>

 * spamPolicy=<Spam Policy Type - mild|moderate|aggressive>

 * spamAction=<Spam Action - accept|delete|quarantine|tag|header>

 * cleanAction=<Clean Action - accept|delete|quarantine|tag|header>

 * suspectAction=<Suspect Action - accept|delete|quarantine|tag|header>
 
 Note: If the values of spamAction, cleanAction and suspectAction is one of quarantine|tag|header, we need additional data
 
 * folderName=<folder name, if spamAction, cleanAction and suspectAction is quarantine>
 
 * tagText=<tag text, if spamAction, cleanAction and suspectAction is tag>
 
 * headerText=<header text, if spamAction, cleanAction and suspectAction is header>   
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_CLOUDMARK_FILTERS_CLEAN_ACTION" message="Invalid cloudmark filters clean action." />
    <error code="MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_CLEAN_ACTION" message="Unable to set cleanAction." />
    <error code="MBX_INVALID_CLOUDMARK_FILTERS_SPAM_ACTION" message="Invalid cloudmark filters spam action." />
    <error code="MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_ACTION" message="Unable to set spamAction." />
    <error code="MBX_INVALID_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED" message="Invalid cloudmark filters spam filtering enabled." />
    <error code="MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED" message="Unable to set spamfilterEnabled." />
    <error code="MBX_INVALID_CLOUDMARK_FILTERS_SPAM_POLICY" message="Invalid cloudmark filters spam policy." />
    <error code="MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_POLICY" message="Unable to set spamPolicy." />
    <error code="MBX_INVALID_CLOUDMARK_FILTERS_SUSPECT_ACTION" message="Invalid cloudmark filters spam action." />
    <error code="MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SUSPECT_ACTION" message="Unable to set suspectAction." />
    <error code="MBX_INVALID_FOLDER_NAME" message="Invalid folder name." />
    <error code="MBX_INVALID_HEADER_TEXT" message="Invalid header text." />
    <error code="MBX_INVALID_TAG_TEXT" message="Invalid tag text." />
    <error code="FOLDER_NAME_REQUIRED" message="Folder name is required for action quarantine" />
    <error code="TAG_TEXT_REQUIRED" message="Tag text is required for action tag." />
    <error code="HEADER_TEXT_REQUIRED" message="Header text is required for action header." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 