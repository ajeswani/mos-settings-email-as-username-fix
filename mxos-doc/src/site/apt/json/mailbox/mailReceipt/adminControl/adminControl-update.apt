 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailReceipt AdminControl - update API
 
 To update AdminControl attributes. Update AdminControl operation is allowed only for group admin mailbox.

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://<host>:<port>/mxos/mailbox/v2/{email}/mailReceipt/adminControl

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

** Optional Parameters

 * adminRejectAction=<Admin rejection action - drop|forward>
 
 * adminRejectInfo=<Additional information associated with the admin reject action attribute>

 * adminDefaultDisposition=<Specifies the default action taken on messages sent to submailboxes that are from senders who are on neither the approved nor blocked list of senders - accept|reject>

 Note: Either of the optional attribute needs to be present while updating the AdminControl attributes.
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_ADMIN_REJECT_ACTION" message="Invalid admin reject action." />
    <error code="MBX_UNABLE_TO_SET_ADMIN_REJECT_ACTION" message="Unable to set adminRejectAction." />
    <error code="MBX_INVALID_ADMIN_REJECT_INFO" message="Invalid admin reject info." />
    <error code="MBX_UNABLE_TO_SET_ADMIN_REJECT_INFO" message="Unable to set adminRejectInfo." />
    <error code="MBX_INVALID_ADMIN_DEFAULT_DISPOSITION" message="Invalid admin default disposition." />
    <error code="MBX_UNABLE_TO_SET_ADMIN_DEFAULT_DISPOSITION" message="Unable to set adminDefaultDisposition." />
    <error code="MBX_NOT_GROUP_ADMIN_ACCOUNT" message="Mailbox is not a group admin account." />
    <error code="MBX_NOT_GROUP_ACCOUNT" message="Mailbox is not a group account." />
    <error code="GROUP_MAILBOX_FEATURE_IS_DISABLED" message="Group Mailbox feature is disabled." />        
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 