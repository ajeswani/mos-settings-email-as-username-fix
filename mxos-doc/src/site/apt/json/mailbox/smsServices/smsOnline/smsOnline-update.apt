 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Mailbox smsOnline - update API
 
 To Update Mailbox smsOnline attributes

* API Description

** Invoking using SDK - Returns void

 *  void ISmsOnlineService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/smsServices/smsOnline

** Mandatory Parameters

 * email=<Subscriber's email address >
 
** Optional Parameters (Any one is required)

 * smsOnlineEnabled= <Allowed values- yes or no>
 
 * internationalSMSAllowed= <Allowed values- yes or no>
 
 * internationalSMSEnabled= <Allowed values- yes or no>
 
 * maxSMSPerDay= <Max number of Online SMS Allowed per user, Allowed values- 0 to 2147483647>
 
 * concatenatedSMSAllowed= <Allowed values- yes or no>
 
 * concatenatedSMSEnabled= <Allowed values- yes or no>
 
 * maxConcatenatedSMSSegments= <Max number of concatenated SMS segments per SMS, Allowed value - 0 to 256>
 
 * maxPerCaptchaSMS= <Max number of messages before CAPTCHA is presented, Allowed values- 0 to 2147483647>
 
 * maxPerCaptchaDurationMins= <Max duration (minutes) before CAPTCHA is presented, Allowed values- 0 to 2147483647>


** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * Error code, when any required parameter is missing from the request other than email.
    <error code="MBX_SMSSERVICES_UPDATE_MISSING_PARAMS" message="One or more mandatory parameters 
		are missing for SmsServices POST operation." />
 * Error code, if smsOnlineEnabled  is Invalid
    <error code="MBX_INVALID_SMS_ONLINE_ENABLED" message="Invalid sms online enabled." />
 * Error code, if update operation on smsOnlineEnabled is unsuccessful
    <error code="MBX_UNABLE_TO_SET_SMS_ONLINE_ENABLED" message="Unable to set smsOnlineEnabled." />
 * Error code, if internationalSMSAllowed  is Invalid
    <error code="MBX_INVALID_INTERNATIONAL_SMS_ALLOWED" message="Invalid international sms allowed." />
 * Error code, if update operation on internationalSMSAllowed is unsuccessful
    <error code="MBX_UNABLE_TO_SET_INTERNATIONAL_SMS_ALLOWED" message="Unable to set internationalSMSAllowed." />
 * Error code, if internationalSMSEnabled  is Invalid
    <error code="MBX_INVALID_INTERNALTONAL_SMS_ENABLED" message="Invalid international sms enabled." />
 * Error code, if update operation on internationalSMSEnabled is unsuccessful
    <error code="MBX_UNABLE_TO_SET_INTERNALTONAL_SMS_ENABLED" message="Unable to set internationalSMSEnabled." />
 * Error code, if maxSMSPerDay  is Invalid
    <error code="MBX_INVALID_MAX_SMS_PER_DAY" message="Invalid maximum sms per day." />
 * Error code, if update operation on maxSMSPerDay is unsuccessful
    <error code="MBX_UNABLE_TO_SET_MAX_SMS_PER_DAY" message="Unable to set maxSMSPerDay." />
 * Error code, if concatenatedSMSAllowed  is Invalid
	<error code="MBX_INVALID_CONCATENATED_SMS_ALLOWED" message="Invalid concatenated sms allowed." />
 * Error code, if update operation on concatenatedSMSAllowed is unsuccessful
    <error code="MBX_UNABLE_TO_SET_CONCATENATED_SMS_ALLOWED" message="Unable to set concatenatedSMSAllowed." />
 * Error code, if concatenatedSMSEnabled  is Invalid
    <error code="MBX_INVALID_CONCATENATED_SMS_ENABLED" message="Invalid concatenated sms enabled." />
 * Error code, if update operation on concatenatedSMSEnabled is unsuccessful
    <error code="MBX_UNABLE_TO_SET_CONCATENATED_SMS_ENABLED" message="Unable to set concatenatedSMSEnabled." />
 * Error code, if maxConcatenatedSMSSegments  is Invalid
    <error code="MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS" message="Invalid maximum concatenated sms segments." />
 * Error code, if update operation on maxConcatenatedSMSSegments is unsuccessful
    <error code="MBX_UNABLE_TO_SET_MAX_CONCATENATED_SMS_SEGMENTS" message="Unable to set maxConcatenatedSMSSegments." />
 * Error code, if maxPerCaptchaSMS  is Invalid
    <error code="MBX_INVALID_MAX_PER_CAPTCHA_SMS" message="Invalid maximum per capcha sms." />
 * Error code, if update operation on maxPerCaptchaSMS is unsuccessful
    <error code="MBX_UNABLE_TO_SET_MAX_PER_CAPTCHA_SMS" message="Unable to set maxPerCaptchaSMS." />
 * Error code, if maxPerCaptchaSMS  is Invalid
    <error code="MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS" message="Invalid maximum per captcha duration minutes." />
 * Error code, if update operation on maxPerCaptchaDurationMins is unsuccessful
    <error code="MBX_UNABLE_TO_SET_MAX_PER_CAPTCHA_DURATION_MINS" message="Unable to set maxPerCaptchaDurationMins." />

+--

*** Common errors 

 * {{{../../../errors/common-errors.html} Common errors}}

 