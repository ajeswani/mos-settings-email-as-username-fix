 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailAccess - update API
 
 To Update MailAccess attributes

* API Description

** Invoking using SDK

 *  void IMailAccessService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailAccess

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
** Optional Parameters (Any one is required)

 * popAccessType=<Account access for standard POP service. Allowed values: all|trusted|none>

 * popSSLAccessType=<Account access for SSL-encrypted POP service. Allowed values: all|trusted|none>

 * popAuthenticationType=<Authenticated POP service status of an account. Allowed values: 0|1|2>

 * imapAccessType=<Account access for standard IMAP service. Allowed values: all|trusted|none>

 * imapSSLAccessType=<Account access for SSL-encrypted IMAP service. Allowed values: all|trusted|none>

 * smtpAccessEnabled=<Account access for standard SMTP service. Allowed values: Yes|No>

 * smtpSSLAccessEnabled=<Account access for SSL-encrypted SMTP service. Allowed values: Yes|No>

 * smtpAuthenticationEnabled=<Authenticated SMTP service status of an account. Allowed values: Yes|No>
 
 * webMailAccessType=<Account access for WebEdge Web interface. Allowed values: all|trusted|none>

 * webMailSSLAccessType=<Account access for SSL-encrypted WebEdge Web interface. Allowed values: all|trusted|none>

 * mobileMailAccessType=<Specifies how users in the COS can use Web Engine servers to access messages. Allowed values: all|trusted|none>

 * mobileActiveSyncAllowed=<Active Sync Allowed status for the account. Allowed values: Yes|No>

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    // Email format is not valid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    // Email does not exist.
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
    <error code="MBX_UNABLE_TO_SET_POP_ACCESS" message="Unable to perform set popAccessType." />
    <error code="MBX_UNABLE_TO_SET_POP_SSL_ACCESS" message="Unable to perform set popSSLAccessType." />
    <error code="MBX_UNABLE_TO_SET_POP_AUTH_TYPE" message="Unable to perform set popAuthenticationType." />
    <error code="MBX_UNABLE_TO_SET_IMAP_ACCESS" message="Unable to perform set imapAccessType." />
    <error code="MBX_UNABLE_TO_SET_IMAP_SSL_ACCESS" message="Unable to perform set imapSSLAccessType." />
    <error code="MBX_UNABLE_TO_SET_SMTP_ACCESS_ENABLED" message="Unable to perform set smtpAccessEnabled." />
    <error code="MBX_UNABLE_TO_SET_SMTP_SSL_ACCESS_ENABLED" message="Unable to perform set smtpSSLAccessEnabled" />
    <error code="MBX_UNABLE_TO_SET_SMTP_AUTH_ENABLED" message="Unable to perform set smtpAuthenticationEnabled" />
    <error code="MBX_UNABLE_TO_SET_WEBMAIL_ACCESS" message="Unable to perform set webMailAccessType." /> 
    <error code="MBX_UNABLE_TO_SET_WEBMAIL_SSL_ACCESS" message="Unable to perform set webMailSSLAccessType." />
    <error code="MBX_UNABLE_TO_SET_MOBILE_MAIL_ACCESS" message="Unable to perform set mobileMailAccessType." />
    <error code="MBX_UNABLE_TO_SET_MOBILE_ACTIVE_SYNC_ALLOWED" message="Unable to perform set mobileActiveSyncAllowed." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
