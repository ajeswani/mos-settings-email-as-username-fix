 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MessageEventRecords - read API
 
 To read MessageEventRecords POJO

* API Description

** Invoking using SDK - Returns MessageEventRecords POJO

 * MessageEventRecords IMessageEventRecordsService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns MessageEventRecords JSON

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/{email}/internalInfo/messageEventRecords

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
    "eventRecordingEnabled": "Never",
    "maxHeaderLength": 0,
    "maxFilesSizeKB": 0
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    // This error usually comes server is not able to get the MessageEventRecords object.
    <error code="MBX_UNABLE_TO_GET_MSG_EVENT_RECORDS" message="Unable to perform MessageEventRecords GET operation." />
    // Email format is not valid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    // Email does not exist.
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
+--

*** Common errors 

 * {{{../../../errors/common-errors.html} Common errors}}
