 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Mailbox ExternalStore - Read API
 
 This API will be used to get ExternalStore POJO

* API Description

** Invoking using SDK - Returns Mailbox ExternalStore POJO

 * ExternalStore IExternalStoreService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns ExternalStore JSON Object

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailStore/externalStore

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
    "maxExternalStoreSizeMB":"<External storage size quota>",
    "externalStoreAccessAllowed":"<Specifies whether RichMail allows access to external store | yes/no>"
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * Error code for missing required parameters other than email
    <error code="MBX_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED" message="Invalid externalStoreAccessAllowed." />
    <error code="MBX_INVALID_MAX_EXTERNAL_STORE_SIZEMB" message="Invalid maxExternalStoreSizeMB." />
 * Error code, if ldap get operation is not successful, due to some external problem
    <error code="MBX_UNABLE_TO_EXTERNALSTORE_GET" message="Unable to perform External Store GET operation." /> 
 * Error code, if email parameter  is missing from the request.   
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters."/>
 
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
