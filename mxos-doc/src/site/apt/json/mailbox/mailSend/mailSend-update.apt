 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailSend - update API
 
 To update MailSend attributes

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://<host>:<port>/mxos/mailbox/v2/{email}/mailSend

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
** Optional Parameters

 * fromAddress=<Specifies an alternative email address for a subscriber. when present,
                this alternate address is used in from header of outgoing messages. maxLength = 255>

 * futureDeliveryEnabled=<Enables the delayed delivery feature for the current class of service (COS) or user - yes|no>

 * maxFutureDeliveryDaysAllowed=<The maximum number of days that a user can choose to delay a message before it is sent.>

 * maxFutureDeliveryMessagesAllowed=<The maximum number of delayed delivery messages a user can have at one time.>

 * alternateFromAddess=<Specifies which of the subscriber-specific aliases the subscriber prefers to appear.>

 * replyToAddress=<Specifies an alternative email address for a subscriber.>

 * useRichTextEditor=<Specifies the subscriber-preferred default editor for composing messages in the web interface - yes|no>

 * includeOrginalMailInReply=<Include the original message with a reply message - yes|no>

 * originalMailSeperatorCharacter=<Specifies the subscriber-preferred format of original message text when subscribers reply to a message.>

 * autoSaveSentMessages=<Specifies whether copies of outgoing messages are saved - yes|no>

 * addSignatureForNewMails=<Enables automatic insertion of signature text at the end of a newly composed message - yes|no>

 * addSignatureInReplyType=<Specifies the subscriber-preferred option to place the personal signature.>
 
 * signature=<Signature text of the mail user.>

 * autoSpellCheckEnabled=<Specifies whether the application automatically performs a spell check on out bound messages - yes|no>

 * confirmPromptOnDelete=<Specifies whether WebEdge prompts for confirmation before deleting a message - yes|no>

 * includeReturnReceiptReq=<Specifies the subscriber-preferred option for whether the application includes a return receipt request in all out bound messages - yes|no>

 * maxSendMessageSizeKB=<pecifies the maximum send message size>

 * maxAttachmentSizeKB=<pecifies the maximum attachment size>

 * maxAttachmentsInSession=<Specifies the maximum number of  attachments as a total that a subscriber can hold during simultaneous message compositions.>

 * maxAttachmentsToMessage=<Specifies the maximum number of files that can be attached to a single message using the WebEdge Web interface.>

 * maxCharactersPerPage=<Specifies the maximum number of characters per page that the Web interface uses when displaying a message.>

 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_UNABLE_TO_MAILSEND_GET" message="Unable to perform MailSend GET operation." />
    <error code="MBX_INVALID_FROM_ADDRESS" message="Invalid from address." />
    <error code="MBX_UNABLE_TO_SET_FROM_ADDRESS" message="Unable to perform set fromAddress." />
    <error code="MBX_INVALID_FUTURE_DELIVERY_ENABLED" message="Invalid future delivery enabled." />
    <error code="MBX_UNABLE_TO_SET_FUTURE_DELIVERY_ENABLED" message="Unable to perform set futureDeliveryEnabled." />
    <error code="MBX_INVALID_FUTURE_DELIVERY_DAYS" message="Invalid future delivery days." />
    <error code="MBX_UNABLE_TO_SET_FUTURE_DELIVERY_DAYS" message="Unable to perform set futureDeliveryDays." />
    <error code="MBX_INVALID_MAX_FUTURE_DELIVERY_MESSAGES" message="Invalid max future delivery messages." />
    <error code="MBX_UNABLE_TO_SET_MAX_FUTURE_DELIVERY_MESSAGES" message="Unable to perform set maxFutureDeliveryMessages" />
    <error code="MBX_INVALID_ALTERNATE_FROM_ADDRESS" message="Invalid alternate from address." />
    <error code="MBX_UNABLE_TO_SET_ALTERNATE_FROM_ADDRESS" message="Unable to perform set alternateFromAddress." />
    <error code="MBX_INVALID_REPLY_TO_ADDRESS" message="Invalid reply to address." />
    <error code="MBX_UNABLE_TO_SET_REPLY_TO_ADDRESS" message="Unable to perform set replyToAddress" />
    <error code="MBX_INVALID_USE_RICH_TEXT_EDITOR" message="Invalid use rich text editor." />
    <error code="MBX_UNABLE_TO_SET_USE_RICH_TEXT_EDITOR" message="Unable to perform set useRichTextEditor" />
    <error code="MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY" message="Invalid include original mail in reply." />
    <error code="MBX_UNABLE_TO_SET_INCLUDE_ORIGINAL_MAIL_IN_REPLY" message="Unable to perform set includeOriginalMailInReply." />
    <error code="MBX_INVALID_ORIGINAL_MAIL_SEPERATOR_CHARACTER" message="Invalid original mail seperator character." />
    <error code="MBX_UNABLE_TO_SET_ORIGINAL_MAIL_SEPERATOR_CHARACTER" message="Unable to perform set maxSendMessageSizeKB." />
    <error code="MBX_INVALID_AUTO_SAVE_MESSAGES" message="Invalid auto save messages." />
    <error code="MBX_UNABLE_TO_SET_AUTO_SAVE_MESSAGES" message="Unable to perform set autoSaveMessages." />
    <error code="MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS" message="Invalid add signature for new mails." />
    <error code="MBX_UNABLE_TO_SET_ADD_SIGNATURE_FOR_NEW_MAILS" message="Unable to perform set addSignatureForNewMails." />
    <error code="MBX_INVALID_ADD_SIGNATURE_IN_REPLY" message="Invalid add signature in reply." />
    <error code="MBX_UNABLE_TO_SET_ADD_SIGNATURE_IN_REPLY" message="Unable to perform set addSignatureInReply." />
    <error code="MBX_INVALID_AUTO_SPELL_CHECK_ENABLED" message="Invalid auto spell check enabled." />
    <error code="MBX_UNABLE_TO_SET_AUTO_SPELL_CHECK_ENABLED" message="Unable to perform set autoSpellCheckEnabled." />
    <error code="MBX_INVALID_CONFIRM_PROMT_ON_DELETE" message="Invalid confirn promt on delete." />
    <error code="MBX_UNABLE_TO_SET_CONFIRM_PROMT_ON_DELETE" message="Unable to perform set confirmPromtOnDelete." />
    <error code="MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ" message="Invalid inlcude return receipt req." />
    <error code="MBX_UNABLE_TO_SET_INCLUDE_RETURN_RECEIPT_REQ" message="Unable to perform set includeReturnReceiptReq." />
    <error code="MBX_INVALID_MAX_SEND_MESSAGE_SIZE_KB" message="Invalid max send message size kb." />
    <error code="MBX_UNABLE_TO_SET_MAX_SEND_MESSAGE_SIZE_KB" message="Unable to perform set maxSendMessageSizeKB." />
    <error code="MBX_INVALID_MAX_ATTACHEMENT_SIZE_KB" message="Invalid max attachement size kb." />
    <error code="MBX_UNABLE_TO_SET_MAX_ATTACHEMENT_SIZE_KB" message="Unable to perform set maxAttachmentSizeKB." />
    <error code="MBX_INVALID_MAX_ATTACHMENTS_IN_SESSION" message="Invalid max attachments in session." />
    <error code="MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_IN_SESSION" message="Unable to perform set maxAttachments in session." />
    <error code="MBX_INVALID_MAX_ATTACHMENTS_TO_MESSAGE" message="Invalid max attachments to message." />
    <error code="MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_TO_MESSAGE" message="Unable to perform set maxAttachmentsToMessage." />
    <error code="MBX_INVALID_MAX_CHARACTERS_PER_PAGE" message="Invalid max characters per page." />
    <error code="MBX_UNABLE_TO_SET_MAX_CHARACTERS_PER_PAGE" message="Unable to perform set maxCharactersPerPage." />
    <error code="MBX_UNABLE_TO_SET_SIGNATURE" message="Unable to set signature." />
    <error code="MBX_INVALID_SIGNATURE" message="Invalid Signature." />
    
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 