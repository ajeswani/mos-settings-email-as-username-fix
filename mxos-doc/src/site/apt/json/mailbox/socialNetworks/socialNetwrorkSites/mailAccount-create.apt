 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailAccounts - create API
 
 To Create MailAccount attributes

* API Description

** Invoking using REST Context in SDK - Return void

 *  IExternalMailAccountService.create(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - PUT http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\/externalAccounts/mailAccounts/\{accountId\}}
 
** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
 * accountId=<AccountId of the mailAccount, allowed Value - String> 

** Optional Parameters

 * 	accountName=< Specifies the text description of the account associated with the external server, allowed Value - String>
 * 	accountType=< Specifies the type of external mail server, such as POP, POP SSL, or IMAP, allowed Value - String>
 *  serverAddress=< Specifies the name of the external mail server that contains the account, allowed Value - String>
 *  serverPort=< Specifies the port number of the external mail server, allowed Value - Integer>
 *  timeoutSecs=< Specifies the subscriber-preferred amount of time, in seconds, that the application waits for a response from the external mail server, allowed Value - Integer>
 *  emailAddress=< Specifies the SMTP address of the external mail server  account, allowed Value - String>
 *	accountUserName=< Specifies the name of the account on the external mail server, allowed Value - String>
 *	accountPassword=< Specifies the password, in the format identified by the password encryption type, for the external mail server account, allowed Value - String>
 *	displayImage=< Specifies a number corresponding to the subscriber-preferred image that the interface displays to indicate that the message was fetched from a particular external account, allowed Value - String>
 *	fetchMailsEnabled=< Specifies, using a boolean value, whether mail in the remote account is downloaded (true) or viewed (false).(Rich Mail only), allowed Value - String>
 *	autoFetchMails=< Specifies the subscriber-preferred option as to whether (true) or not (false), allowed Value - String>
 *	fetchedMailsFolderName=< Specifies the subscriber-preferred folder in which messages from the external mail server account are placed after being downloaded, allowed Value - String>
 *	leaveEmailsOnServer=< Specifies the subscriber-preferred option of whether messages in the external mail server account are deleted (true) or saved (false) after they are retrieved., allowed Value - String>
 *	fromAddressInReply=< Specifies the subscriber-preferred email address placed in the From message header when replying to messages retrieved from the external mail, allowed Value - String>
 *	mailSendEnabled=< Specifies, using a boolean value, whether (true) or not (false) outgoing mail is enabled from this account.(Rich Mail only), allowed Value - String>
 *	mailSendSMTPAddress=< Specifies the hostname of the external SMTP server.(Rich Mail only), allowed Value - String>
 *	mailSendSMTPPort=< Specifies the port number of external SMTP server.(Rich Mail only), allowed Value - String>
 *	smtpSSLEnabled=< Specifies whether the external SMTP server uses the SSL protocol (SMTPS) or not (SMTP).(Rich Mail only), allowed Value - String>
  
** REST URL Response

 * Success Response - HTTP 200 OK.

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
  	<error code="MBX_UNABLE_TO_SET_MAIL_ACCOUNTS" message="Unable to set MailAccounts" />
    <error code="MBX_INVALID_ACCOUNT_ID" message="Invalid Account Id of MailAccount" />
    <error code="MBX_MAIL_ACCOUNT_NOT_FOUND" message="The given MailAccount does not exist" />
    <error code="MBX_UNABLE_TO_PUT_MAIL_ACCOUNT" message="Unable to put MailAccounts" />
    <error code="MBX_INVALID_ACCOUNT_NAME" message="Invalid Account Name" />
    <error code="MBX_INVALID_ACCOUNT_TYPE" message="Invalid Account Type" />
    <error code="MBX_INVALID_SERVER_ADDRESS" message="Invalid Server Address" />
    <error code="MBX_INVALID_SERVER_PORT" message="Invalid Server Port" />
    <error code="MBX_INVALID_TIMEOUT_SEC" message="Invalid TimeOut in Seconds" />
    <error code="MBX_INVALID_EMAIL_ADDRESS" message="Invalid Email Address" />
    <error code="MBX_INVALID_ACCOUNT_USERNAME" message="Invalid Account Username" />
    <error code="MBX_INVALID_ACCOUNT_PASSWORD" message="Invalid Account Password" />
    <error code="MBX_INVALID_DISPLAY_IMAGE" message="Invalid Display Image" />
    <error code="MBX_INVALID_FETCH_MAILS_ENABLED" message="Invalid Fetch Mails Enabled" />
    <error code="MBX_INVALID_AUTO_FETCH_MAILS" message="Invalid Auto Fetch Mails" />
    <error code="MBX_INVALID_FETCHED_MAILS_FOLDER_NAME" message="Invalid Fetched Folders Name" />
    <error code="MBX_INVALID_LEAVE_EMAIL_ON_SERVER" message="Invalid Leave Email on Server" />
    <error code="MBX_INVALID_FROM_ADDRESS_IN_REPLY" message="Invalid From Address in Reply" />
    <error code="MBX_INVALID_MAIL_SEND_ENABLED" message="Invalid Mail Send Enabled" />
    <error code="MBX_INVALID_MAIL_SEND_SMTP_ADDRESS" message="Invalid Mail Send SMTP Address" />
    <error code="MBX_INVALID_MAIL_SEND_SMTP_PORT" message="Invalid Mail Send SMTP Port" />
    <error code="MBX_INVALID_SMTP_SSL_ENABLED" message="Invalid SMTP SSL Enabled" />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 