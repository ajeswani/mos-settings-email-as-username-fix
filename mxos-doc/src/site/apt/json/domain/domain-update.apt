 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Domain - Update API
 
 To Update Domain object

* API Description

  Update Domain API is to Update attributes of Domain Object.

** Invoking using REST Context in SDK - Return nothing on success, otherwise MxOSException

 *  IDomainService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/domain/v2/\{domain\}

** Mandatory Parameters

 * domain=<Complete domain name associated with a mailDomain object>

** POST Request Mandatory HTTP Body Parameters
 
 * type=<Domain type value can be "local/nonauth/rewrite">

** POST Request Optional HTTP Body Parameters (Any one is required)
 
 * For type=nonauth mailRelayHost is required
 
    relayHost=<Another mail host that accepts messages for a nonauthoritative domain if the primary host does not recognize the recipient>
 
 * For type=rewrite alternateDomain is required

    alternateDomain=<Specifies the new domain name for a recipientís email address>
 
 * To Update default mailbox for domain
  
    defaultMailbox=<Email account within a mail domain that is to receive all mail sent to nonexistent addresses within that domain>

** Response Format

*** Success Response Format: 

 * HTTP 200 without body
 
*** Failure Response Format: 

 * HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body as follows

+--
{
   "code" : <Error code>
   "requestParams" : <Request parameters>
   "operationType" : <Operation type - PUT/GET/POST/DELETE
   "shortMessage" : <Short description of the Error>
   "longMessage" : <Exception details of the Error>
}
+--
  
** Error Codes

*** Parameter type validation errors

 * Error: DMN_INVALID_NAME, Message: Invalid domain, not RFC compliant, maxLength supported is 255.
 
 * Error: DMN_INVALID_TYPE, Message: Invalid type, maxLength supported is 20.
 
 * Error: DMN_INVALID_RELAYHOST, Message: Invalid mailRelayHost, maxLength supported is 255.
 
 * Error: DMN_INVALID_DEFAULT_MAILBOX, Message: Invalid defaultMailbox, not RFC compliant, please refer attribute email.
 
 * Error: DMN_INVALID_CUSTOM_FEILDS, Message: Invalid Domain related customFields, maxLength supported is 255.

*** Operation related errors
 
 * Error: GEN_BAD_REQUEST, Message:Bad request, please check the request and parameters.

*** Business logic related errors
  
 * Error: DMN_MISSING_ALTERNATEDOMAIN, Message: The param alternateDomain is mandatory for REWRITE domain type.
 
 * Error: DMN_MISSING_RELAYHOST, Message: Missing mailRelayHost attribute. Should be present for type=Non-Authoritative.
 
*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

** Example

    {{{./domain-update-resp.html}http://mxos.openwave.com:8080/mxos/domain/v2/openwave.com}} 
 
*** POST Request Mandatory HTTP Body Parameters
 
 * domain=openwave.com
 
 * type=Local
 
*** POST Request Optional HTTP Body Parameters (Any one is required)
 
 * defaultMailbox=openwave.com

 (OR)

*** POST Request Mandatory HTTP Body Parameters
 
 * domain=openwave.com
 
 * type=Non-Authoritative
  
*** POST Request Optional HTTP Body Parameters (Any one is required)
 
 * defaultMailbox=openwave.com

 * mailRelayHost=relay.openwave.com 
 
 (OR)

*** POST Request Mandatory HTTP Body Parameters
 
 * domain=openwave.com
 
 * type=Rewrite
 
*** POST Request Optional HTTP Body Parameters (Any one is required)
 
 * defaultMailbox=openwave.com
  
 * alternateDomain=test.com

 