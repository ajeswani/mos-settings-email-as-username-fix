 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Domain - Create API
 
 To Create Domain attributes

* API Description

  Create Domain API is to create a Domain Object.

** Invoking using REST Context in SDK - Return nothing on success, otherwise MxOSException

 *  IDomainService.create(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - PUT http://mxosHost:mxosPort/mxos/domain/v2/\{domain\}

** Mandatory Parameters

 * domain=<Complete domain name associated with a mailDomain object>

** PUT Request Mandatory HTTP Body Parameters

*** For type = local
  
 * type=<To create domain type as Local, value should be "local">

*** For type = nonauth
  
 * type=<To create domain type as Non-authoritative value should be "nonauth">

 * relayHost=<Another mail host that accepts messages for a nonauthoritative domain if the primary host does not recognize the recipient>
 
*** For type = rewrite

 * type=<To create domain type as Rewrite value should be "rewrite">

 * alternateDomain=<Specifies the new domain name for a recipientís email address>
 
** PUT Request Optional HTTP Body Parameters
  
 * defaultMailbox=<Email account within a mail domain that is to receive all mail sent to nonexistent addresses within that domain>

 * customFields=<Domain specific custom fields>
 
** Response Format

*** Success Response Format: 

 * HTTP 200 with the empty body
 
*** Failure Response Format: 

 * HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body as follows

+--
{
   "code" : <Error code>
   "requestParams" : <Request parameters>
   "operationType" : <Operation type - PUT/GET/POST/DELETE
   "shortMessage" : <Short description of the Error>
   "longMessage" : <Exception details of the Error>
}
+--

** Error Codes

*** API specific errors

+--    

 * Error: DMN_INVALID_NAME, Message: Invalid domain, not RFC compliant, maxLength supported is 255.
 
 * Error: DMN_INVALID_TYPE, Message: Invalid type, maxLength supported is 20.
 
 * Error: DMN_INVALID_RELAYHOST, Message: Invalid mailRelayHost, maxLength supported is 255.
 
 * Error: DMN_INVALID_DEFAULT_MAILBOX, Message: Invalid defaultMailbox, not RFC compliant, please refer attribute email.
 
 * Error: DMN_INVALID_CUSTOM_FEILDS, Message: Invalid Domain related customFields, maxLength supported is 255.

+--
 
*** Operation related errors

+--

 * Error: GEN_BAD_REQUEST, Message:Bad request, please check the request and parameters.
 
+--
 
*** Business logic related errors
 
+--
 
 * Error: DMN_ALREADY_EXISTS, Message: The given domain already exists.
 
 * Error: DMN_MISSING_ALTERNATEDOMAIN, Message: The param alternateDomain is mandatory for REWRITE domain type.
 
 * Error: DMN_MISSING_RELAYHOST, Message: Missing mailRelayHost attribute. Should be present for type=nonauth.
 
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

** Example

    {{{./domain-add-resp.html}http://mxos.openwave.com:8080/mxos/domain/v2/openwave.com}}
 
*** PUT Request Mandatory HTTP Body Parameters
 
 * domain=openwave.com
 
 * type=rewrite
 
 * alternateDomain=test.com
 
  (or) 

 * domain=openwave.com
 
 * type=nonauth
 
 * mailRelayHost=relay.openwave.com

   (or) 

 * domain=openwave.com
 
 * type=local
 
*** PUT Request Optional HTTP Body Parameters
 
 * defaultMailbox=user1@openwave.com

 * customFields="Domain custom fileds"

 