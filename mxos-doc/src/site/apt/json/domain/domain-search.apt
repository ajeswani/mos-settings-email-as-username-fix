 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Domain - Search API
 
 To Search Domain Objects

* API Description

  Search Domains API is to Search Domain Objects for the given search criteria.

** Invoking using REST Context in SDK - Return List of Domain objects on success, otherwise MxOSException

 *  IDomainService.search(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/domain/v2/search?query="LDAP Search Query"

** Mandatory Parameters

  * query="LDAP Search Query"

** Response Format

*** Success Response Format: 

+--
[
    {
        "domain": <new Domain thats been created>,
        "type": <type of Domain | Local/Non-authoritative/Rewrite>,
        "relayHost":<Another mail host that accepts messages for a nonauthoritative domain if the primary host does not recognize the recipient>,
        "alternateDomain":<Specifies the new domain name for a recipientís email address>,
        "defaultMailbox": <default mailbox for domain>,
        "customFields": <Custom Fields to hold fields at a per domain level>
    }
]
+--

*** Failure Response Format: 

 * HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body as follows

+--
{
   "code" : <Error code>
   "requestParams" : <Request parameters>
   "operationType" : <Operation type - PUT/GET/POST/DELETE
   "shortMessage" : <Short description of the Error>
   "longMessage" : <Exception details of the Error>
}
+--

** Error Codes
 
*** Operation related errors

 * Error: GEN_BAD_REQUEST, Message: Bad request, please check the request and parameters..

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

** Example

*** GET Request Mandatory HTTP Query Parameters

 * query=domainname=openwave*
 
    {{{./domain-search-resp.html}http://mxos.openwave.com:8081/mxos/domain/v2/list?query=domainname=openwave*}} 

