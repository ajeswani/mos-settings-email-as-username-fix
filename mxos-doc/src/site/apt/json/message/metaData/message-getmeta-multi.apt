 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MessageMetaData service - read multiple messages meta data API of a particular message
 
 To read multiple message's meta data in particular folder

* API Description

** Invoking using SDK - Returns Map<String, MetadataPOJO>

 * Map<String, Metadata> IMetadataService.readMulti(Map<String, List<String>> inputParams) throws MxOSException 

** Invoking using REST URL - Returns MessageMetaData JSON Object

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/metadata?messageId={messageId1}&messageId={messagId2}

** GET Request Mandatory HTTP Query Parameters

 * email=<Subscriber's email id>
 
 * folderName=<Folder Name of the folder where the message resides>
 
 * messageId=<Message Id of the message to be retrieved. This is unique for the system. max-length(excluding angular brackets)=255>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false  � Only for RME Version 163.>

 * popDeletedFlag=<Pop deleted flag, allowed values are true/false - Only for RME version 175 and higher>
       false - The messages that are marked as pop deleted are not returned as deleted.
       true  - The messages that are marked as pop deleted are returned as deleted messages.
	
** Response Format

*** Success Response Format: 

+--
{
    uid: 1000,
    type: "",
    flagRecent: false,
    flagSeen: true,
    flagUnread: false,
    flagAns: true,
    flagFlagged: false,
    flagDel: true,
    flagBounce: false,
    flagPriv: false,
    flagRichMail: 0,
    flagDraft: false,
    popDeletedFlag: false,
    keywords: [0],
    references: "",
    hasAttachments: false,
    deliverNDR: 0,
    sentDate: 140566898223072,
    arrivalTime: 1370863987,
    lastAccessedTime: 1371033350,
    expireOn: 0,
    size: 263,
    from: "test1@openwave.com",
    to: [3],
    0:  "testTo@openwave.com",
    -
    cc: [3],
    0:  "testcc@openwave.com",
    -
    bcc: [0],
    replyTo: [0],
    inReplyTo: "",
    blobMessageId: "<12345>",
    subject: "welcome message"
}
+--

*** Failure Response Format: 

 * HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body as follows

** Error Codes

*** API specific errors

+--
* Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email." />
* Error code, if folder name is bad formatted or Invalid.
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folder name." />
* Error code, if message id is bad formatted or Invalid.
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid message Id." />
* Error code, if isAdmmin is bad formatted or Invalid.
    <error code="MSG_INVALID_IS_ADMIN" message="Invalid isAdmin value." />
* Error code, if invalid attributes are provided.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
* Error code, if could not be able to read the message metadata.
    <error code="MSG_UNABLE_TO_GET_MESSAGE_META_DATA" message="Unable to get the message metadata." />
* Error code, if unable to get the mss link info of the user.
    <error code="MBX_UNABLE_TO_MSSLINKINFO_GET" message="Unable to perform MssLinkInfo GET operation." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 