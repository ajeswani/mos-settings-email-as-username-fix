 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Metadata service - Search Message Metadata in a folder 

* API Description

 Metadata Search API supports sortKey, query, sliceStart, sliceEnd, sliceCount and sliceReversed.
 For search based on UID for RME v163 sliceStart, sliceEnd, sliceCount parameters are
 mandatory along with other mandatory parameters. sliceStart must be equal to sliceEnd.
 Hence, sliceCount must be equal to 1. Also, sliceStart and sliceEnd must be valid UIDs.

** Invoking using SDK - Returns Map\<String, Map\<String, Metadata\>\>

 * Map\<String, Map\<String, Metadata\>\> IMetadataService.search(
        final Map\<String, List\<String\>\> inputParams, SearchTerm searchTerm) 
        throws MxOSException
 
** Invoking using REST URL - Returns Map of MessageMetaData JSON Objects with message id as key

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}
             /messages/metadata/search?sortKey=messageId&query=\<Query String\>

** Mandatory Parameters

 * email=<Subscriber's email id>
 
 * folderName=<Folder Name which contains messages to be retrieved>
 
 * sortKey=<Fetch messages sorted based on particular index like to/from/cc/subject. 
   For RME v163 sorting can be done using uid only. i.e. sortKey=uid. 
   Type: String Enum values=\{messageId, hasAttachments, richMailFlag, arrivalTime, size,
   uid, priority, subject, from, to, cc, sentDate, threadId\}.>
 
** Optional Parameters

 * sortOrder=<String Enum values=\{ascending, descending\}, default=ascending
   (driven through configuration). In search based on UID for RME v163 sortOrder
   would be ignored since search can be done for 1 UID only at a time.> 

 * sliceStart=<String, Represents start of the query range. A blank string
   means the smallest entry in the list. In search based on UID for RME v163 
   sliceStart must be a valid UID.>
 
 * sliceEnd=<String, Represents end of the query range. A blank string means 
   the largest entry in the list. In search based on UID for RME v163 sliceEnd 
   must be a valid UID.>
 
 * sliceCount=<String, Number of messages starting from sliceStart. Default driven
   through configuration.  NOTE: To retrieve all the messages in given folder, 
   please pass sliceCount = 2147483647 and sliceStart="". In search based on UID
   for RME v163 pass sliceCount = 1 since at present single UID search is supported.>
   
 * conversationViewReq=<Boolean, values=\{true, false\}, default=false
   Specifies whether to retrieve conversation view or not.
   NOTE: This attribute is applicable only for RME v175 and greater.>

 * query=<Query string> is as follows
 
+--
  Query String format - query=<SearchTerm>      
   
  Simple SearchTerm = (<Key><Operator><Value>)  --> All the records whose key should match with the given value.
     
  Complex AND SearchTerm = (&(SearchTerm1)...(SearchTermN))  
    - Returns the records that match all the SearchTerms 1 to N. Minimum 2 Simple SearchTerms required to use AND SearchTerms.

  Complex OR SearchTerm = (|(SearchTerm1)...(SearchTermN))
    - Returns the records that match one of the SearchTerms 1 to N. Minimum 2 Simple SearchTerms required to use OR SearchTerms.
   
  Complex NOT SearchTerm = (!(SearchTerm))
    - Returns the records that does not match one the SearchTerm.

  Supported Keys: to, from, cc and subject
   
  Supported Operators:   
       == -> Equals
       ~  -> Contains 
 
  Supported Conditions:   
       && -> AND Condition 
       || -> OR Condition
       ! --> NOT Condition
   
  Examples:
    (&(to=foo@gmail.com)(from=bar@gmail.com))
            -- All the messages whose to address is foo@gmail.com and from address is bar@gmail.com

    (|(to=foo@gmail.com)(from=bar@gmail.com))
            -- All the messages whose to is foo@gmail.com or from is bar@gmail.com

    (|(&(to=foo@gmail.com)(subject~test))(cc=some@gmail.com))
            -- All the message whose to is foo@gmail.com and subject contains "test", or cc is some@gmail.com   
+--

** REST URL Response

 * Success Response - HTTP 200 with the following (example) JSON response in the body

+--
{
    "0":{
	    8f672a58-d1c1-11e2-9466-90691e411970: {
	    uid: 1000,
	    folderId: 8f672a58-d1c1-11e2-9466-90691e4111111,
	    type: "",
	    flagRecent: false,
	    flagSeen: true,
	    flagUnread: false,
	    flagAns: true,
	    flagFlagged: false,
	    flagDel: true,
	    flagBounce: false,
	    flagPriv: false,
	    flagRichMail: 0,
	    flagDraft: false,
	    popDeletedFlag: false,
	    keywords: [0],
	    references: "",
	    hasAttachments: false,
	    deliverNDR: 0,
	    sentDate: 140566898223072,
	    arrivalTime: 1370863987,
	    lastAccessedTime: 1371033350,
	    expireOn: 0,
	    size: 263,
	    from: "test1@openwave.com",
	    to: [3],
	    0:  "testTo@openwave.com",
	    -
	    cc: [3],
	    0:  "testcc@openwave.com",
	    -
	    bcc: [0],
	    replyTo: [0],
	    inReplyTo: "",
	    blobMessageId: "<12345>",
	    subject: "welcome message",
	    },
	    ...
	    ...
	},
	"1":{
	    2beb1e8e-d1c2-11e2-b526-ba4239c3e030: {
	    uid: 1001,
	    folderId: 8f672a58-d1c1-11e2-9466-90691e4111111,
	    type: "",
	    flagRecent: false,
	    flagSeen: true,
	    flagUnread: false,
	    flagAns: true,
	    flagFlagged: false,
	    flagDel: true,
	    flagBounce: false,
	    flagPriv: false,
	    flagRichMail: 0,
	    flagDraft: false,
	    popDeletedFlag: false,
	    keywords: [0],
	    references: "",
	    hasAttachments: false,
	    deliverNDR: 0,
	    sentDate: 140566898223072,
	    arrivalTime: 1370864249,
	    lastAccessedTime: 1371021557,
	    expireOn: 0,
	    size: 263,
	    from: "test1@openwave.com",
	    to: [3],
	    0:  "testTo@openwave.com",
	    -
	    cc: [3],
	    0:  "testcc@openwave.com",
	    -
	    bcc: [0],
	    replyTo: [0],
	    inReplyTo: "",
	    blobMessageId: "<12345>",
	    subject: "welcome message",
	    },
	    2beb1e8e-d1c2-11e2-b526-ba4239c3e031: {
	    uid: 1001,
	    folderId: 8f672a58-d1c1-11e2-9466-90691e4111111,
	    type: "",
	    flagRecent: false,
	    flagSeen: true,
	    flagUnread: false,
	    flagAns: true,
	    flagFlagged: false,
	    flagDel: true,
	    flagBounce: false,
	    flagPriv: false,
	    flagRichMail: 0,
	    flagDraft: false,
	    popDeletedFlag: false,
	    keywords: [0],
	    references: "",
	    hasAttachments: false,
	    deliverNDR: 0,
	    sentDate: 140566898223072,
	    arrivalTime: 1370864249,
	    lastAccessedTime: 1371021557,
	    expireOn: 0,
	    size: 263,
	    from: "test1@openwave.com",
	    to: [3],
	    0:  "testTo@openwave.com",
	    -
	    cc: [3],
	    0:  "testcc@openwave.com",
	    -
	    bcc: [0],
	    replyTo: [0],
	    inReplyTo: "",
	    blobMessageId: "<12345>",
	    subject: "welcome message",
	    },
	    ...
	    ...
	}
}
+--

 Note: In success response if priority is present it is represented as received from MSS(example:"normal", "urgent", "non-urgent").
 
 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_EMAIL" message="Invalid email." />
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folder name." />
    <error code="INVALID_MESSAGE_SEARCH_QUERY"  message="Invalid query value." />
    <error code="MSG_INVALID_SORT_KEY" message="Invalid sort key." />
    <error code="MSG_INVALID_SLICE_START" message="Invalid slice start." />
    <error code="MSG_INVALID_SLICE_END" message="Invalid slice end." />
    <error code="MSG_INVALID_SLICE_COUNT" message="Invalid slice count." />
    <error code="MSG_INVALID_SORT_ORDER" message="Invalid sort order." />
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
    <error code="MBX_UNABLE_TO_MSSLINKINFO_GET" message="Unable to perform MssLinkInfo GET operation." />
    <error code="MSG_UNABLE_TO_SEARCH_MESSAGE_META_DATA" message="Unable to search the message meta data.." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}
