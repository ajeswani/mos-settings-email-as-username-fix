 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MessageFlags - update API
 
 To Update message flags attributes

* API Description

** Invoking using SDK

 *  void IMessageFlagsService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/\{messageId\}/messageSummary/messageMetaData/flags

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

 * folderName=<Folder Name of the folder which contains the message>

 * messageId=<Message ID which has to be deleted. max-length(excluding angular brackets)=255>
 
** Optional Parameters (Any one is required)

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false>

 * flagSeen=<Message is seen. Allowed values: true|false>

 * flagAns=<Message is answered. Allowed values: true|false>

 * flagFlagged=<Message is flagged. Allowed values: true|false>

 * flagDel=<Message is deleted. Allowed values: true|false>

 * flagRes=<Message is restored. Allowed values: true|false>

 * flagDraft=<Message is in draft state. Allowed values: true|false>

** Limitations
 
 * flagRecent and flagUnread are not supported in update API.
  
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    // Email format is not valid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    // Email does not exist.
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
    <error code="MSS_CONNECTION_ERROR" message="MSS error - Communication error." />
    <error code="MSG_INVALID_IS_ADMIN" message="Invalid isAdmin value." />
    <error code="MSG_INVALID_FLAG_SEEN" message="Invalid flagSeen." />
    <error code="MSG_INVALID_FLAG_ANS" message="Invalid flagAns." />
    <error code="MSG_INVALID_FLAG_FLAGGED" message="Invalid flagFlagged." />
    <error code="MSG_INVALID_FLAG_DEL" message="Invalid flagDel." />
    <error code="MSG_INVALID_FLAG_RES" message="Invalid flagRes." />
    <error code="MSG_INVALID_FLAG_DRAFT" message="Invalid flagDraft." />
    <error code="MSG_UNABLE_TO_PERFORM_GET" message="Unable to perform Get Message operation." />
    <error code="MSG_UNABLE_TO_PERFORM_UPDATE" message="Unable to perform Update Message operation." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
