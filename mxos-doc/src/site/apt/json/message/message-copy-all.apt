 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Message service - copy all messages API
 
 To Copy all messages from source folder to destination folder.

* API Description

** Invoking using SDK - Returns void

 * Base IMessageService.copyAll(final Map<String, List<String>> inputParams) throws MxOSException

** Invoking using REST URL - Copy all the messages from source folder to destination folder.

 * URL - PUT http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{srcFolderName\}/messages/copy/\{toFolderName\}

** Mandatory Parameters

 * email=<Subscriber's email id>
 
 * srcFolderName=<Folder Name which contains the messages to be copied>
 
 * toFolderName=<Folder Name to which the messages to be copied>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false � Only for RME version 163>

 * optionFolderIsHint=<Specifies that the message may not be in specified src folder, allowed values are true/false � Only for RME version 163>

 * optionMultipleOk=<Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false � Only for RME version 163>

 * optionSupressMers=<Specifies to suppress MERS events for this operation, allowed values are true/false � Only for RME version 163>
 
 * disableNotificationFlag=<Disable notification flag, allowed values are true/false - Only for RME version 175 and higher>
    false - Send notification to the IMAP client
    true - Do not send notification to the IMAP client  
 
 Note: Copy all messages API does not support flags and uid as request parameters.

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body.

** Error Codes

*** API specific errors

+--
 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." /> 
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_SRC_FOLDERNAME" message="Invalid source folderName value." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_DEST_FOLDERNAME" message="Invalid destination folderName value." />
 * Error code, if deleteNotificationFlag is bad formatted or Invalid.
    <error code="MSG_INVALID_DISABLE_NOTIFICATION_FLAG" message="Invalid Disable Notification Flag." />   
 * Error code, when unable to perform move operation.   
    <error code="MSG_UNABLE_TO_PERFORM_COPY_ALL" message="Unable to perform copy all messages operation." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}