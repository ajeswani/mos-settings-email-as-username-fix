 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Body service - Message Body API
 
 To update a message body in a folder

* Update Message Body Description

** Invoking using SDK - Returns void

 * void IBodyService.update(final Map<String, List<String>> inputParams) throws MxOSException

** Invoking using REST URL with folder name and messageId 

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/\{messageId\}/body 

** POST Request Mandatory HTTP Query Parameters
 
 * email=<Subscriber's email id>
 
 (AND)
 
 * folderName=<Folder Name of the folder where the message resides>

 (AND)
  
 * messageId=<Message ID (for RME version 175) or IMAP uid (for RME version 163) for which body information has to be updated.>

** REST URL Response

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body
 
 * Success Response - HTTP 200 without body

** Error Codes

*** API specific errors

+--
 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
 * Error code, if folderName is Invalid.  
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
 * Error code, if messageid is Invalid.  
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
 * Error code, if any required parameter is missing.
	<error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />    
 * Error code, if messageId not found.   
    <error code="MSG_NOT_FOUND" message="Given message(id) NOT found." />
 * Error code, when unable to perform update Message operation
 	<error code="MSG_UNABLE_TO_PERFORM_UPDATE" message="Unable to perform update Message operation." />
 * Error code, when unable to perform Update Message operation and rollback failed
 	<error code="MSG_UNABLE_TO_UPDATE_ROLLBACK_FAILED" message="Unable to perform Update Message operation and rollback failed." />
 * Error code, when unable to perform Update Message operation and rollback success
 	<error code="MSG_UNABLE_TO_UPDATE_ROLLBACK_SUCCESS" message="Unable to perform Update Message operation and rollback success." /> 	
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 