 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

getMessage API
 
 Get Message details

* API Description

** Invoking using SDK - Returns void

 * Base IMessageService.read(final Map<String, List<String>> inputParams) throws MxOSException

** Invoking using REST URL - Read message with message Id in a particular folder.

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/\{messageId\}

** GET Request Mandatory HTTP Query Parameters
 
 * email=<Subscriber's email id>
 
 (AND)
 
 * folderName=<Folder Name of the folder where the message resides>

 (AND)
  
 * messageId=<Message ID which is to be read. max-length(excluding angular brackets)=255>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false � Only for RME version 163>

 * offset=<Offset for reading the message � Only for RME version 163>

 * lenght=<Length of the message to be read � Only for RME version 163>

 * popDeletedFlag=<Pop deleted flag, allowed values are true/false - Only for RME version 175 and higher>
       false - The messages that are marked as pop deleted are not returned as deleted.
       true  - The messages that are marked as pop deleted are returned as deleted messages.

** REST URL Response

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body.

 * Success Response Format: 

+--
{
    "metadata":{
    "uid":1001,
    "type":"",
    "flagRecent":true,
    "flagSeen":false,
    "flagAns":false,
    "flagFlagged":false,
    "flagDel":false,
    "flagDraft":false,
    "popDeletedFlag": false,,
    "keywords":"",
    "hasAttachments":false,
    "deliverNDR":0,
    "arrivalTime":1368802115,
    "lastAccessedTime":1368711776,
    "expireOn":0,
    "size":45,
    "from":"test2@test.com",
    to: "Foo <foo@openwave.com>"
    cc: "CC <cc@openwave.com>"
    bcc: "BCC <bcc@openwave.com>"
    replyTo: "Foo <foo@openwave.com>"
    inReplyTo: ""
    blobMessageId: ""
    "subject":""
    },
    "header":{
    "headerBlob":"to:test@test.com\r\nfrom:test2@test.com\r\n\r\n"
    },
    "body":{
    "messageBlob":"to:test@test.com\r\nfrom:test2@test.com\r\n\r\ntest"
    }
}
+--

*** Failure Response Format: 

 * HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body as follows

+--
{
   "code" : <Error code>
   "requestParams" : <Request parameters>
   "operationType" : <Operation type - PUT/GET/POST/DELETE
   "shortMessage" : <Short description of the Error>
   "longMessage" : <Exception details of the Error>
}
+--

** Error Codes

*** API specific errors

+--
 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
 * Error code, if folderName is Invalid.  
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
 * Error code, if messageid is Invalid.  
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />    
 * Error code, when unable to perform Search Message operation
     <error code="MSG_UNABLE_TO_PERFORM_SEARCH" message="Unable to perform Search Message operation." />
 * Error code, if offset is Invalid
     <error code="MSG_INVALID_OFFSET" message="Invalid message offset value." />
 * Error code, if lenght is Invalid
    <error code="MSG_INVALID_LENGTH" message="Invalid message length value." />
 * Error code, if unable to find the given folder
     <error code="FLD_NOT_FOUND" message="Given folder NOT found." />  
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 