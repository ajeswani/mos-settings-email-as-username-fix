 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Message service - delete API
 
 To delete a particular message.

* API Description

** Invoking using SDK - Returns void

 * Base IMessageService.delete(final Map<String, List<String>> inputParams) throws MxOSException

** Invoking using REST URL - Delete message with message Id in a particular folder.

 * URL - DELETE http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/\{messageId\}

** Mandatory Parameters

 * email=<Subscriber's email id>
 
 * folderName=<Folder Name which contains messages to be retrieved>

 * messageId=<Message ID which has to be deleted. max-length(excluding angular brackets)=255>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false � Only for RME version 163>

 * optionFolderIsHint=<Specifies that the message may not be in specified src folder, allowed values are true/false � Only for RME version 163>

 * optionMultipleOk=<Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false � Only for RME version 163>

 * optionSupressMers=<Specifies to suppress MERS events for this operation, allowed values are true/false � Only for RME version 163>

 * popDeletedFlag=<Pop deleted flag, allowed values are true/false - Only for RME version 175 and higher>
    false - means remove the message from the mss backend as well.
    true  - means mark the message as special pop deleted.
    
 * disableNotificationFlag=<Disable notification flag, allowed values are true/false - Only for RME version 175 and higher>
    false - Send notification to the IMAP client
    true - Do not send notification to the IMAP client  

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body.

** Error Codes

*** API specific errors

+--
 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." /> 
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
 * Error code, if messageId is Invalid.
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
 * Error code, if popDeletedFlag is Invalid.
    <error code="MSG_INVALID_POP_DELETE_FLAG" message="Invalid Pop Deleted Flag." />
 * Error code, if deleteNotificationFlag is bad formatted or Invalid.
    <error code="MSG_INVALID_DISABLE_NOTIFICATION_FLAG" message="Invalid Disable Notification Flag." />   
 * Error code, when unable to perform delete operation.   
    <error code="MSG_UNABLE_TO_PERFORM_DELETE" message="Unable to perform Delete Message operation." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}