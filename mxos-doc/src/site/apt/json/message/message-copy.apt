 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Message service - copy message API
 
 To copy a particular message from source folder to destination folder.

* API Description

** Invoking using SDK - Returns void

 * Base IMessageService.copy(final Map<String, List<String>> inputParams) throws MxOSException

** Invoking using REST URL - Copy message with message Id from source folder to destination folder.

 * URL - PUT http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{srcFolderName\}/messages/\{messageId\}/copy/\{toFolderName\}

** Mandatory Parameters

 * email=<Subscriber's email id>
 
 * srcFolderName=<Folder Name which contains the message to be copied>
 
 * toFolderName=<Folder Name to which the message to be copied>

 * messageId=<Message ID which has to be copied. max-length(excluding angular brackets)=255>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false � Only for RME version 163>

 * optionFolderIsHint=<Specifies that the message may not be in specified src folder, allowed values are true/false � Only for RME version 163>

 * optionMultipleOk=<Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false � Only for RME version 163>

 * optionSupressMers=<Specifies to suppress MERS events for this operation, allowed values are true/false � Only for RME version 163>

 * mailboxId=<Mailbox Id of the user. Allowed values: Long>

 * messageStoreHost=<Mailbox Message Store host of the user>
 
 * realm=<Mailbox mail realm of the user. e.g. "dns://mmsc.east.jsmail.jp">

 * flagSeen=<Flag that indicates message is seen. Empty value is not allowed. Allowed values: true|false � Only for RME version 175 and higher>

 * flagAns=<Flag that indicates message is answered. Empty value is not allowed. Allowed values: true|false � Only for RME version 175 and higher>

 * flagFlagged=<Flag that indicates message is flagged. Empty value is not allowed. Allowed values: true|false � Only for RME version 175 and higher>

 * flagDel=<Flag that indicates message is deleted. Empty value is not allowed. Allowed values: true|false � Only for RME version 175 and higher>

 * flagRecent=<Flag that indicates message is recent. Empty value is not allowed. Allowed values: true|false � Only for RME version 175 and higher>

 * flagDraft=<Flag that indicates message is in draft state. Empty value is not allowed. Allowed values: true|false � Only for RME version 175 and higher>

 * uid=<UID of the message to be created with. Allowed Values: Integer. Min: 0, Max: 2147483647 � Only for RME version 175 and higher>
 
 * disableNotificationFlag=<Disable notification flag, allowed values are true/false - Only for RME version 175 and higher>
    false - Send notification to the IMAP client
    true - Do not send notification to the IMAP client  

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body.

** Error Codes

*** API specific errors

+--
 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." /> 
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_SRC_FOLDERNAME" message="Invalid source folderName value." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_DEST_FOLDERNAME" message="Invalid destination folderName value." />
 * Error code, if messageId is Invalid.
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
 * Error code, if isAdmin is bad formatted or Invalid.
    <error code="MSG_INVALID_IS_ADMIN" message="Invalid isAdmin value." />
 * Error code, if flagSeen is bad formatted or Invalid.
    <error code="MSG_INVALID_FLAG_SEEN" message="Invalid flagSeen." />
 * Error code, if flagAns is bad formatted or Invalid.
    <error code="MSG_INVALID_FLAG_ANS" message="Invalid flagAns." />
 * Error code, if flagFlagged is bad formatted or Invalid.
    <error code="MSG_INVALID_FLAG_FLAGGED" message="Invalid flagFlagged." />
 * Error code, if flagDel is bad formatted or Invalid.
    <error code="MSG_INVALID_FLAG_DEL" message="Invalid flagDel." />
 * Error code, if flagRecent is bad formatted or Invalid.
    <error code="MSG_INVALID_FLAG_RECENT" message="Invalid flagRecent." />
 * Error code, if flagDraft is bad formatted or Invalid.
    <error code="MSG_INVALID_FLAG_DRAFT" message="Invalid flagDraft." />
 * Error code, if uid is bad formatted or Invalid.
    <error code="MSG_INVALID_UID" message="Invalid Uid." />
 * Error code, if deleteNotificationFlag is bad formatted or Invalid.
    <error code="MSG_INVALID_DISABLE_NOTIFICATION_FLAG" message="Invalid Disable Notification Flag." />   
 * Error code, when unable to perform move operation.   
    <error code="MSG_UNABLE_TO_PERFORM_COPY" message="Unable to perform copy message operation." />
 * Error code, when source and destination folders are same.
    <error code="MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME" message="Unable to copy messages, source and destination folders cannot be same." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}