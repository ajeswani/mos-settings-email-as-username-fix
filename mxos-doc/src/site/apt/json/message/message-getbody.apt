 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Body service - Message Body API
 
 To get a message body in a folder

* Get Message Body Description

** Invoking using SDK - Returns Body

 * Body IBodyService.read(final Map<String, List<String>> inputParams) throws MxOSException

** Invoking using REST URL with folder name and messageId - Returns Body JSON Object which contain entire message header blob and body blob information.

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/\{messageId\}/body 

** GET Request Mandatory HTTP Query Parameters
 
 * email=<Subscriber's email id>
 
 (AND)
 
 * folderName=<Folder Name of the folder where the message resides>

 (AND)
  
 * messageId=<Message ID or message UUID for which body information has to be fetched. max-length(excluding angular brackets)=255>

** REST URL Response

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body
 
 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
    "messageBlob": "<Message header blob and body blob>"
}
+--

** Error Codes

*** API specific errors

+--
 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
 * Error code, if folderName is Invalid.  
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
 * Error code, if messageid is Invalid.  
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
 * Error code, if any required parameter is missing.
	<error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />    
 * Error code, if messageId not found.   
    <error code="MSG_NOT_FOUND" message="Given message(id) NOT found." />
 * Error code, when unable to perform Search Message operation
 	<error code="MSG_UNABLE_TO_PERFORM_GET" message="Unable to perform Get Message operation." />

+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 