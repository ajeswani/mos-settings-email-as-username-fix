 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Tasks - logout API
 
 To Logout to Tasks backend store

* API Description

 API to Logout

** Invoking using REST Context in SDK

 *  void IExternalLoginService.logout(final Map<String, List<String>> inputParams) throws MxOSException;
 
** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/task/v2/\{userId\}/logout

** Mandatory Parameters

 * entity=<Entity to be used to get OX Session logout. Allowed Values: addressBook, tasks>

 * userId=<Subscriber's email address or emailAlias without domain>
 
 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** Sample Code
 
+--
    public void logoutOX() throws Exception {
        IExternalLoginService service = (IExternalLoginService) context
                .getService(ServiceEnum.ExternalLoginService.name());
        // then call the service methods as below
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(ExternalProperty.entity.name(), Arrays.asList(Entity.TASKS.toString()));
        requestMap.put(AddressBookProperty.userId.name(), Arrays.asList(userId));
        requestMap.put(AddressBookProperty.sessionId.name(), Arrays.asList(session.getSessionId()));
        requestMap.put(AddressBookProperty.cookieString.name(), Arrays.asList(session.getCookieString()));
        service.logout(requestMap);
    }
+--
 
** REST URL Response

 * Success Response - HTTP 200.

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="TSK_INVALID_USERNAME" message="Invalid username." />
    <error code="TSK_UNABLE_TO_LOGOUT" message="Error in logging out." />
    <error code="TSK_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 