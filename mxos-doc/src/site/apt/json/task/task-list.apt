 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Tasks - list API
 
 To list all Task for a user

* API Description

 List Task API will list all tasks for a user 

** Invoking using SDK - Returns void

 *  void ITasksService.list(final Map <String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/task/v2/\{userId\}/tasks/list

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias without domain>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** Optional Parameters

 * folderId=<Open Xchange folder where the task exists, default is private folder (22)>  

** REST URL Response

  * Success Response - HTTP 200 with Task Object as body

+--
 [
	    taskBase:{		
			taskId : "10",	
			folderId : 23,	
			name: "mOS sprint release progress report"	
			owner : "test1@openwave.com",	
			priority: "low",	
			status: "in progress",	
			isPrivate: "yes",	
			colorLabel: "green",	
			categories: "Release tasks",	
			notes: "some notes about the task",	
			createdDate: "2013-07-06T17:26:54Z",	
			updatedDate: "2013-07-06T17:26:54Z",	
			startDate: "2013-07-15T17:26:54Z",	
			dueDate: "2011-07-29T17:26:54Z",	
			completedDate: "2011-07-29T17:26:54Z",	
			reminderDate: "60",	
			progress: 66
	    },
	    taskBase:{		
			taskId : "11",	
			folderId : 23,	
			name: "mOS sprint release progress report2"	
			owner : "test2@openwave.com",	
			priority: "high",	
			status: "in progress",	
			isPrivate: "yes",	
			colorLabel: "green",	
			categories: "Release tasks",	
			notes: "some notes about the task",	
			createdDate: "2013-07-06T17:26:54Z",	
			updatedDate: "2013-07-06T17:26:54Z",	
			startDate: "2013-07-15T17:26:54Z",	
			dueDate: "2011-07-29T17:26:54Z",	
			completedDate: "2011-07-29T17:26:54Z",	
			reminderDate: "60",	
			progress: 66
	    }
]  
    
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="TSK_INVALID_USERNAME" message="Invalid username." />
    <error code="TSK_INVALID_SESSION" message="Invalid session." />
    <error code="TSK_INVALID_COOKIE" message="Invalid cookie." />
    <error code="TSK_INVALID_TASKID" message="Invalid Task Id." />
    <error code="TSK_INVALID_FOLDER_ID" message="Invalid folder Id" />
    <error code="TSK_UNABLE_TO_LISTTASKS" message="Unable to list task."/> 
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 