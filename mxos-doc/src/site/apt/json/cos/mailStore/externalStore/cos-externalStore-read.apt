 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

COS ExternalStore - read API
 
 To read COS ExternalStore attributes

* API Description

** Invoking using SDK

 *  void read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET https://\<host\>/mxos/cos/v2/\{cosId\}/mailStore/externalStore

** Mandatory Parameters

 * cosId= <Specifies Distinguished name of the class of service, maxLength=30>
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
     [
         {
             "externalStoreAccessAllowed": "yes",
             "maxExternalStoreSizeMB": 50
         }
     ]  
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="COS_UNABLE_TO_GET" message="Unable to perform Cos GET operation." />
    <error code="COS_INVALID_COSID" message="Invalid cosId." />
    <error code="COS_NOT_FOUND" message="The given cosId does not exist." />
    <error code="COS_UNABLE_TO_EXTERNALSTORE_GET" message="Unable to perform Cos External Store GET operation." />
    <error code="COS_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED" message="Invalid external store access allowed." />
    <error code="COS_INVALID_MAX_EXTERNAL_STORE_SIZEMB" message="Invalid max external store size." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
