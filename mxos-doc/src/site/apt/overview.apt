 ------------------------------------------------------------------------------
 Openwave Mxos - Overview
 ------------------------------------------------------------------------------
 Openwave
 ------------------------------------------------------------------------------

Openwave Messaging MxOS - Overview

 Mx Open Service is a system, which can be used for provisioning of COS, Domain, Mailbox, Folder, Message, Contacts, Groups and Process APIs. It uses various Databases/Stores in bachend to store the above data and provides single entry points (APIs) to the end client application/system.
 
MxOS Contexts

[./images/mxos-sdk.png]

* STUB Context

 For getting familiar with the APIs for integration.
 
 * Target - Any Client who just wanted to familiar with MxOS APIs.

 * No Dependency

 * No Connectivity Problems
 
 * Required Parameters:

+--
        contextId
            - Name of the context

        contextType
            - Context type as STUB
+--

 * Optional Parameters:

+--
        loadServices
            - Services to be support
            - com.opwvmsg.mxos.interfaces.service.ServiceObjectEnum can be used to get these names.
            - Allowed Value Mailbox,Message,AddressBook,Process,Notify
+--

* REST Context

 For interacting via HTTP REST interface
 
 * Target - Any Client who wanted connect MxOS APIs via REST Services.

 * Simple and Self explanatory REST APIs
 
 * The connection objects towards mxos would be created on demand

 * Uniform APIs for different services

 * APIs include CRUD operation APIs, search APIs, Specialized function APIs

 * Supports API versioning. Giving flexibility and choice to FEP for upgrade.

 * API documentation as part of the release. No separate documentation required.
 
 * Required Parameters:

+--
        contextId
            - Name of the context

        contextType
            - Context type as REST
+--

 * Optional Parameters:

+--
        loadServices
            - Services to be support
            - com.opwvmsg.mxos.interfaces.service.ServiceObjectEnum can be used to get these names.
            - Allowed Value Mailbox,Message,AddressBook,Process,Notify

        mxosBaseUrl
            - Base MxOS URL
            - Absent of this param, the default value http://mxos-host:8080/mxos

        mxosMaxConnections
            - Max Number of Connections to MxOS
            - The connection objects towards mxos would be created on demand
            - Absent of this param, the default value is 10
            
        mxosConnectionTimeout
            - Connection Timeout
            - Default value as per system level configuration
            
        mxosReadTimeout
            - Read Timeout
            - Default value as per system level configuration
+--

* Sample Code:

+--
        /**
         * Method to make call(s) to MxOS.
         */
        public void callToMxOS() {
            IMxOSContext context = null;
            try {
                // Load the MxOS Context
                context = loadContext();
                ...
                ...
                // Read Base Object
                final String email = "testuser@openwave.com";
                Base base = readBase(context, email);
                ...
                // Add email Alias
                final String alias = "testalias@openwave.com");
                addEmailAlias(context, email, alias);
                ...
            } catch (MxOSException e) {
                ...
            } finaly {
                // Close MxOS Connections
                if(context != null) {
                    try {
                        context.destroy();
                    } catch(MxOSException e) {
                        ...
                    }
                }
            }
        }
        /**
         * Method to read Mailbox Base object.
         * 
         * @param context IMxOSContext object
         * @param email Mailbox email
         * @return Mailbox's Base object
         */
        public Base readBase(final IMxOSContext context, final String email) {
            // Get Service Object
            try {
                final IMailboxBaseService baseService = (IMailboxBaseService) context
                        .getService(ServiceEnum.MailboxBaseService.name());
                // Read Base Object
                final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
                // Add email to request
                requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
                requestMap.get(MailboxProperty.email.name()).add(email);
                // Read Base call
                final Base base = baseService.read(requestMap);
                return base;
            } catch(MxOSException e) {
                ...
            }
        }
        /**
         * Method to add an alias to Mailbox.
         * 
         * @param context IMxOSContext object
         * @param email Mailbox email
         * @param alias Mailbox alias
         * @return Mailbox's Base object
         */
        public void addEmailAlias(final IMxOSContext context,
            final String email, final String alias) {
            // Get Service Object
            try {
                // Get Service Object
                final IEmailAliasService aliasService = (IEmailAliasService) context
                        .getService(ServiceEnum.MailAliasService.name());
                // Add email to request
                final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
                requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
                requestMap.get(MailboxProperty.email.name()).add(email);
                // Add alias to request
                requestMap.put(MailboxProperty.emailAlias.name(), new ArrayList<String>());
                requestMap.get(MailboxProperty.emailAlias.name()).add(alias);
                // Create Alias call
                aliasService.create(requestMap);
            } catch(MxOSException e) {
                ...
            }
        }
+--

* Loading STUB Context

+--
        public IMxOSContext loadContext() {
            try {
                // Add below property for STUB Context
                final Properties p = new Properties(); 
                p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                        ContextEnum.STUB.name());
                // Add "loadServices" services to be loadded (Optional)
                // Absent of this param would load all the available services
                // com.opwvmsg.mxos.interfaces.service.ServiceObjectEnum can be used to get these names.
                p.setProperty(ContextProperty.LOAD_SERVICES, "Mailbox,Message,AddressBook");
                // Get context object
                return MxOSContextFactory.getInstance().getContext("MXOS-STUB-DEMO", p);
            } catch (Exception e) {
                ...
            }
        }
+--

* Loading REST Context

+--
        public IMxOSContext loadContext() {
            try {
                // Add below property for REST Context
                final Properties p = new Properties();
                // "contextType" - Context TYPE as REST
                p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                        ContextEnum.REST.name());
                // "loadServices" - services to be loadded (Optional)
                // Absent of this param would load all the available services
                // com.opwvmsg.mxos.interfaces.service.ServiceObjectEnum can be used to get these names.
                p.setProperty(ContextProperty.LOAD_SERVICES, "Mailbox,Message,AddressBook");
                // "mxosBaseUrl" - MxOS 2.0 Base URL
                // Absent of this param would use URL "http://mxos-host:8081/mxos" as default
                p.setProperty(ContextProperty.MXOS_BASE_URL, "http://localhost:8081/mxos");
                // "mxosMaxConnections" - Number of Max Connections to be open
                // Allowed any possitive integer
                // Absent of this would load default 10 Max Connections.
                p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "50");
                // "mxosConnectionTimeout" - MxOS Connection Timeout (Optional)
                // Connect timeout interval property, in milliseconds.
                // The value MUST be an possitive of Integer. Otherwise, takes system default settings.
                // If the property is absent then it would take the system level default value.
                // A value of 0 is equivalent to an interval of infinity
                p.setProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT, "120000");
                // "mxosReadTimeout" - MxOS Read Timeout (Optional)
                // Read timeout interval property, in milliseconds.
                // The value MUST be an possitive of Integer. Otherwise, takes system default settings.
                // If the property is absent then it would take the system level default value.
                // A value of zero 0 is equivalent to an interval of infinity
                p.setProperty(ContextProperty.MXOS_READ_TIMEOUT, "60000");
                // "custom" - is required only if there is any support of custom logic in SDK.
                // To use MAA Logging APIs (via MxOS 2.0 Logging APIs)
                // true - To use logging APIs
                // false - To use direct APIs
                // Default value - false
                p.setProperty(ContextProperty.CUSTOM, "true");
                // Get context object
                return MxOSContextFactory.getInstance().getContext("MXOS-REST-DEMO", p);
            } catch (Exception e) {
                ...
            }
        }

+--

* Important Notes:

+--
        1. Handling the IMxOSContext instance object is upto the Client Application.

        2. It is recommended to reuse the context instance object for any number of subsequent calls to MxOS.
+--
