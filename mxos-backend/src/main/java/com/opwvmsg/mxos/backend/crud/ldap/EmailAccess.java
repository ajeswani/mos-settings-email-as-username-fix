/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/data/MailAccessEnum.java#1 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * @author Aricent
 * 
 */
public enum EmailAccess implements DataMap.Property {
    Yes("yes"), No("no");

    private static final Map<String, EmailAccess> BOOLEAN_MAIL_ACCESS_MAP = new HashMap<String, EmailAccess>();

    private String mode;

    static {
        // Used for MailAccess Enabled
        BOOLEAN_MAIL_ACCESS_MAP.put("all", Yes);
        BOOLEAN_MAIL_ACCESS_MAP.put("trusted", Yes);
        BOOLEAN_MAIL_ACCESS_MAP.put("none", No);
    }

    private EmailAccess(String mode) {
        this.mode = mode;
    }

    /**
     * 
     * @param str
     *            mode
     * @return mode enum
     * @throws InvalidRequestException
     *             if enum not found for given data
     */
    public static EmailAccess getEnum(String str)
            throws InvalidRequestException {
        if (str == null) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(),
                    "Invalid data supplied");
        } else {
            return BOOLEAN_MAIL_ACCESS_MAP.get(str);
        }
    }

    /**
     * 
     * @param str
     *            mode
     * @return mode enum
     * @throws InvalidRequestException
     *             if enum not found for given data
     */
    public static String getString(String str) throws InvalidRequestException {
        if (str == null) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(),
                    "Invalid data supplied");
        } else {
            if (BOOLEAN_MAIL_ACCESS_MAP.get(str) != null) {
                return BOOLEAN_MAIL_ACCESS_MAP.get(str).getMode();
            } else {
                throw new InvalidRequestException(
                        ErrorCode.GEN_INVALID_DATA.name(),
                        "Invalid data supplied");
            }
        }
    }

    /**
     * 
     * @param str
     *            mode
     * @return Mode Enum
     * @throws InvalidRequestException
     *             if enum not found for given data
     */
    public static String getModeKey(String str) throws InvalidRequestException {
        if (str == null) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(),
                    "Invalid data supplied");
        }
        for (Entry<String, EmailAccess> entry : BOOLEAN_MAIL_ACCESS_MAP
                .entrySet()) {
            if (entry.getValue().getMode().equals(str)) {
                return entry.getKey();
            }
        }
        throw new InvalidRequestException(
                ErrorCode.GEN_INVALID_DATA.name(),
                "Invalid data supplied");
    }

    /**
     * Returns the corresponding {@link String}.
     * 
     * @return password hash type
     */
    public String getMode() {
        return mode;
    }
}
