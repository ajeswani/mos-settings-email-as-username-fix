/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.message;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * mailbox related activities e.g create mailbox, delete mailbox, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class BackendMessageService implements
        com.opwvmsg.mxos.interfaces.service.message.IMessageService {

    private static Logger logger = Logger
            .getLogger(BackendMessageService.class);

    /**
     * Constructor.
     * 
     * @param metaObjectPool Object pool to talk to metadata.
     * @param blobCRUD Object pool to talk to blobdata.
     * @param serviceProperties Service properties like soft deleted, etc.
     */
    public BackendMessageService() {
        logger.info("BackendMessageService created...");
    }

    @Override
    public String create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.PUT);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.PUT, t0,
                    StatStatus.pass);
            return (String) mxosRequestState.getDbPojoMap().getPropertyAsObject(
                    MessageProperty.messageId);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Message read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.GET);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.GET, t0,
                    StatStatus.pass);
            return (Message) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.message);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }
    @Override
    public Message readByUID(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.GETUID);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.GETUID, t0,
                    StatStatus.pass);
            return (Message) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageBody);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.GETUID, t0,
                    StatStatus.fail);
            throw e;
        }
    }
    @Override
    public void updateByUID(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.POSTUID);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.POSTUID, t0,
                    StatStatus.pass);
            return;
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.POSTUID, t0,
                    StatStatus.fail);
            throw e;
        }
    }
    @Override
    public void delete(Map<String, List<String>> inputParams)
        throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.DELETE);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.DELETE, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.DELETE, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
        throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.DELETEALL);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.DELETEALL, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.DELETEALL, t0,
                    StatStatus.fail);
            throw e;
        }
    }
    @Override
    public void copy(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.COPY);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.COPY, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.COPY, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void copyAll(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.COPYALL);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.COPYALL, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.COPYALL, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void move(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.MOVE);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.MOVE, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.MOVE, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void moveAll(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.MOVEALL);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.MOVEALL, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.MOVEALL, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.DELETEMULTI);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.DELETEMULTI, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.DELETEMULTI, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void copyMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.COPYMULTI);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.COPYMULTI, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.COPYMULTI, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void moveMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MessageService, Operation.MOVEMULTI);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MessageService, Operation.MOVEMULTI, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MessageService, Operation.MOVEMULTI, t0,
                    StatStatus.fail);
            throw e;
        }
    }

}
