/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.logging;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 *  Action class to update alias via MAA.
 * @author Aricent
 */
public class UpdateAliasMaaLoggingAction extends MaaLoggingAction {
    private static Logger logger = Logger
            .getLogger(UpdateAliasMaaLoggingAction.class);

    /**
     * Action to make call to MAA.
     *
     * @param model
     *            model.
     * @throws Exception
     *             exception
     */
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Start : UpdateAliasMaaLoggingAction");
        }
        String userName;
        String domain;
        try {
            Map<String, String> params = new HashMap<String, String>();
            
            String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            userName = ActionUtils.getUsernameFromEmail(email);
            domain = ActionUtils.getDomainFromEmail(email);
            params.put(ARG1, userName);
            params.put(ARG2, domain);

            if ((requestState.getInputParams()
                    .containsKey(MailboxProperty.oldEmailAlias.name()))
                    && (requestState.getInputParams()
                            .get(MailboxProperty.oldEmailAlias.name()).get(0)
                            .length() != 0)) {
                String oldAlias = requestState.getInputParams()
                        .get(MailboxProperty.oldEmailAlias.name()).get(0);
                String oldAliasUser  = ActionUtils.getUsernameFromEmail(oldAlias);
                String oldAliasDomain = ActionUtils.getDomainFromEmail(oldAlias);
                params.put(ARG3, oldAliasUser);
                params.put(ARG4, oldAliasDomain);
            }

            if ((requestState.getInputParams()
                    .containsKey(MailboxProperty.newEmailAlias.name()))
                    && (requestState.getInputParams()
                            .get(MailboxProperty.newEmailAlias.name()).get(0)
                            .length() != 0)) {
                String newAlias = requestState.getInputParams()
                        .get(MailboxProperty.newEmailAlias.name()).get(0);
                String newAliasUser  = ActionUtils.getUsernameFromEmail(newAlias);
                String newAliasDomain = ActionUtils.getDomainFromEmail(newAlias);
                params.put(ARG5, newAliasUser);
                params.put(ARG6, newAliasDomain);
            }

            LoggingResponseBean maaResponse = callMaa(
                    UPDATE_ALIAS_LOGGING_SUB_URL, params);
            // Analyze logging response
            analyzeLoggingResponse(maaResponse);
        } catch(MxOSException me) {
            logger.error(me);
            throw me;
        } catch (Exception e) {
            logger.error(e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End : UpdateAliasMaaLoggingAction");
        }
        return;
    }
}
