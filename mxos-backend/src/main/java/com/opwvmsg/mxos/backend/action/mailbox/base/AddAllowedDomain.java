/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.domain.DomainProvisionHelper;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to add allowed domain.
 *
 * @author mxos-dev
 */
public class AddAllowedDomain implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(AddAllowedDomain.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddAllowedDomain action start."));
        }
        List<String> newAllowedDomains = requestState.getInputParams()
                .get(MailboxProperty.allowedDomain.name());
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool = MxOSApp.getInstance()
                    .getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();

            final List<String> allowedDomainsList;
            final Base base = (Base) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.base);
            /*
             * If we are calling this action as part of mailbox create then no
             * need to do getMailbox.
             */
            if (ActionUtils.isCreateMailboxOperation(requestState
                    .getOperationName())) {
                if (null == base || null == base.getMaxNumAllowedDomains()) {
                    throw new ApplicationException(
                            MailboxError.MBX_ALLOWEDDOMAINS_UNABLE_TO_CREATE
                                    .name());
                }
                allowedDomainsList = new ArrayList<String>();
            } else {
                if (null != base) {
                    if (null != base.getAllowedDomains()) {
                        allowedDomainsList = base.getAllowedDomains();
                    } else {
                        allowedDomainsList = new ArrayList<String>();
                    }
                } else {
                    throw new NotFoundException(
                            MailboxError.MBX_NOT_FOUND.name());
                }
            }
            // throw error if reached maximum limit
            if ((allowedDomainsList.size() + newAllowedDomains.size()) > base
                    .getMaxNumAllowedDomains()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_ALLOWEDDOMAINS_REACHED_MAX_LIMIT
                                .name());
            }
            for (String newAllowedDomain : newAllowedDomains) {
                final Domain domain = DomainProvisionHelper.read(
                        mailboxCRUD, newAllowedDomain);
                if (domain == null) {
                    throw new NotFoundException(
                            DomainError.DMN_NOT_FOUND.name());
                }
                if (!allowedDomainsList
                        .contains(newAllowedDomain.toLowerCase())) {
                    allowedDomainsList.add(newAllowedDomain.toLowerCase());
                }
            }
            String[] allowedDomainsArray = allowedDomainsList
                    .toArray(new String[allowedDomainsList.size()]);

            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.allowedDomain,
                            allowedDomainsArray);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while add allowed domain.", e);
            throw new ApplicationException(
                    MailboxError.MBX_ALLOWEDDOMAINS_UNABLE_TO_CREATE.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddAllowedDomain action end."));
        }
    }
}
