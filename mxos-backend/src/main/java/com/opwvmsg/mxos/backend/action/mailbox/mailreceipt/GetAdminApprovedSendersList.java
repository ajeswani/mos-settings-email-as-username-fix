/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

public class GetAdminApprovedSendersList implements MxOSBaseAction {

    private static Logger logger = Logger
            .getLogger(GetAdminApprovedSendersList.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "GetAdminApprovedSendersList action start."));
            }

            ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;

            try {
                mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                final String dn = requestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.adminRealmDn);
                if (!requestState.getOperationType().name()
                        .equals(Operation.GET.name())) {
                    if (dn == null || dn.length() == 0) {
                        if (logger.isInfoEnabled()) {
                            logger.info("admin realm dn is not valid");
                        }
                        throw new InvalidRequestException(
                                MailboxError.MBX_NOT_GROUP_ADMIN_ACCOUNT.name(),
                                "email not valid since could not retrieve valid admin realm dn");
                    }
                }
                final String email = requestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0);
                final List<String> adminApprovedSendersList = mailboxCRUD
                        .readAdminApprovedSendersList(email);
                requestState.getDbPojoMap().setProperty(
                        MxOSPOJOs.adminApprovedSendersList,
                        adminApprovedSendersList);
            } catch (final MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error("Error in getting admin approved senders list.", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_GET
                                .name(),
                        e);
            } catch (final Throwable t) {
                logger.error(
                        "Internal Error in getting admin approved senders list.",
                        t);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "GetAdminApprovedSendersList action end."));
            }
        } else {
            logger.error("Error while calling getadminapprovedsenderslist, group mailbox feature is disabled.");
            throw new InvalidRequestException(
                    MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
        }

    }
}
