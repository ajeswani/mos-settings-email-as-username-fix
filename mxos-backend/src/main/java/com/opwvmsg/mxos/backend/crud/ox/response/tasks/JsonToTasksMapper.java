/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;
import com.opwvmsg.mxos.task.pojos.TaskBase.Priority;
import com.opwvmsg.mxos.task.pojos.TaskBase.Status;

public class JsonToTasksMapper {
    
    public static final String UTC_TZ = "UTC";

    public static String mapFromTaskBase(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromTasksBaseFields(params, map);

        if (folder != null) {
            map.put(OXTasksProperty.folder_id.name(), folder);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    private static void mapFromTasksBaseFields(Map<String, String> attributes,
            Map<String, String> map) throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));

        if (attributes.get(OXTasksProperty.id.name()) != null) {
            map.put(OXTasksProperty.id.name(),
                    attributes.get(OXTasksProperty.id.name()));
        }
        if (attributes.get(OXTasksProperty.title.name()) != null) {
            map.put(OXTasksProperty.title.name(),
                    attributes.get(OXTasksProperty.title.name()));
        }
        if (attributes.get(OXTasksProperty.organizer.name()) != null) {
            map.put(OXTasksProperty.organizer.name(),
                    attributes.get(OXTasksProperty.organizer.name()));
        }
        if (attributes.get(OXTasksProperty.priority.name()) != null) {
            Priority priority = Priority.fromValue(attributes
                    .get(OXTasksProperty.priority.name()));
            map.put(OXTasksProperty.priority.name(),
                    String.valueOf(priority.ordinal() + 1));
        }
        if (attributes.get(OXTasksProperty.status.name()) != null) {
            Status status = Status.fromValue(attributes
                    .get(OXTasksProperty.status.name()));
            map.put(OXTasksProperty.status.name(),
                    String.valueOf(status.ordinal() + 1));
        }
        if (attributes.get(OXTasksProperty.private_flag.name()) != null) {
            Boolean privateFlag = false;
            if (attributes.get(OXTasksProperty.private_flag.name())
                    .equalsIgnoreCase(BooleanType.YES.name())) {
                privateFlag = true;
            }
            if (attributes.get(OXTasksProperty.private_flag.name())
                    .equalsIgnoreCase(BooleanType.NO.name())) {
                privateFlag = false;
            }
            map.put(OXTasksProperty.private_flag.name(), privateFlag.toString());
        }
        if (attributes.get(OXTasksProperty.color_label.name()) != null) {
            map.put(OXTasksProperty.color_label.name(),
                    attributes.get(OXTasksProperty.color_label.name()));
        }
        if (attributes.get(OXTasksProperty.categories.name()) != null) {
            map.put(OXTasksProperty.categories.name(),
                    attributes.get(OXTasksProperty.categories.name()));
        }
        if (attributes.get(OXTasksProperty.note.name()) != null) {
            map.put(OXTasksProperty.note.name(),
                    attributes.get(OXTasksProperty.note.name()));
        }
        if (attributes.get(OXTasksProperty.start_date.name()) != null) {
            final Date date = simpleDateFormat.parse(attributes
                    .get(OXTasksProperty.start_date.name()));
            map.put(OXTasksProperty.start_date.name(),
                    Long.toString(date.getTime()));
        }
        if (attributes.get(OXTasksProperty.end_date.name()) != null) {
            final Date date = simpleDateFormat.parse(attributes
                    .get(OXTasksProperty.end_date.name()));
            map.put(OXTasksProperty.end_date.name(),
                    Long.toString(date.getTime()));
        }
        if (attributes.get(OXTasksProperty.alarm.name()) != null) {
            final Date date = simpleDateFormat.parse(attributes
                    .get(OXTasksProperty.alarm.name()));
            map.put(OXTasksProperty.alarm.name(),
                    Long.toString(date.getTime()));
        }
        if (attributes.get(OXTasksProperty.percent_completed.name()) != null) {
            map.put(OXTasksProperty.percent_completed.name(),
                    attributes.get(OXTasksProperty.percent_completed.name()));
        }
    }

    public static TaskBase mapToTaskBase(JsonNode root) {
        TaskBase taskBase = new TaskBase();
        mapToTaskBaseFields(root, taskBase);
        return taskBase;
    }

    private static void mapToTaskBaseFields(JsonNode root, TaskBase taskBase) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));
        if (root.path(OXTasksProperty.id.name()).isInt()) {
            Integer id = root.path(OXTasksProperty.id.name()).asInt();
            taskBase.setTaskId(String.valueOf(id));
        }
        if (root.path(OXTasksProperty.folder_id.name()).isTextual()) {
            taskBase.setFolderId(root.path(OXTasksProperty.folder_id.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.title.name()).isTextual()) {
            taskBase.setName(root.path(OXTasksProperty.title.name()).asText());
        }
        if (root.path(OXTasksProperty.priority.name()).isInt()) {
            taskBase.setPriority(Priority.fromOrdinal(root.path(
                    OXTasksProperty.priority.name()).asInt() - 1));
        }
        if (root.path(OXTasksProperty.status.name()).isInt()) {
            taskBase.setStatus(Status.fromOrdinal(root.path(
                    OXTasksProperty.status.name()).asInt() - 1));
        }
        if (root.path(OXTasksProperty.private_flag.name()).isBoolean()) {
            Boolean isPrivate = root.path(OXTasksProperty.private_flag.name())
                    .asBoolean();
            taskBase.setIsPrivate(isPrivate ? BooleanType.YES : BooleanType.NO);
        }
        if (root.path(OXTasksProperty.color_label.name()).isInt()) {
            taskBase.setColorLabel(root
                    .path(OXTasksProperty.color_label.name()).asInt());
        }
        if (root.path(OXTasksProperty.categories.name()).isTextual()) {
            taskBase.setCategories(root.path(OXTasksProperty.categories.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.note.name()).isTextual()) {
            taskBase.setNotes(root.path(OXTasksProperty.note.name()).asText());
        }
        if (root.path(OXTasksProperty.creation_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.creation_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setCreatedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.last_modified.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.last_modified.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setUpdatedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.start_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.start_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setStartDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.end_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.end_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setDueDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.date_completed.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.date_completed.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setCompletedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.alarm.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.alarm.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setReminderDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.percent_completed.name()).isInt()) {
            taskBase.setProgress(root.path(
                    OXTasksProperty.percent_completed.name()).asInt());
        }
    }

    private static void columnsToTaskBaseFields(JsonNode root, TaskBase taskBase) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));
        Iterator<JsonNode> tempNodeIter = root.getElements();
        JsonNode node = tempNodeIter.next();
        if (node.isInt()) {
            // Set TaskId
            taskBase.setTaskId(String.valueOf(node.asInt()));
        }
        node = tempNodeIter.next();
        if (node.isInt()) {
            // Set folderId
            taskBase.setFolderId(String.valueOf(node.asInt()));
        }
        node = tempNodeIter.next();
        if (node.isTextual()) {
            // Set Name
            taskBase.setName(String.valueOf(node.asText()));
        }
        node = tempNodeIter.next();
        if (node.isInt()) {
            // Set Priority
            taskBase.setPriority(Priority.fromOrdinal(node.asInt() - 1));
        }
        /*
        if (root.path(OXTasksProperty.status.name()).isInt()) {
            taskBase.setStatus(Status.fromOrdinal(root.path(
                    OXTasksProperty.status.name()).asInt() - 1));
        }
        if (root.path(OXTasksProperty.private_flag.name()).isBoolean()) {
            Boolean isPrivate = root.path(OXTasksProperty.private_flag.name())
                    .asBoolean();
            taskBase.setIsPrivate(isPrivate ? BooleanType.YES : BooleanType.NO);
        }
        if (root.path(OXTasksProperty.color_label.name()).isInt()) {
            taskBase.setColorLabel(root
                    .path(OXTasksProperty.color_label.name()).asInt());
        }
        if (root.path(OXTasksProperty.categories.name()).isTextual()) {
            taskBase.setCategories(root.path(OXTasksProperty.categories.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.note.name()).isTextual()) {
            taskBase.setNotes(root.path(OXTasksProperty.note.name()).asText());
        }
        if (root.path(OXTasksProperty.creation_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.creation_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setCreatedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.last_modified.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.last_modified.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setUpdatedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.start_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.start_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setStartDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.end_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.end_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setDueDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.date_completed.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.date_completed.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setCompletedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.alarm.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.alarm.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setReminderDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.percent_completed.name()).isInt()) {
            taskBase.setProgress(root.path(
                    OXTasksProperty.percent_completed.name()).asInt());
        }*/
    }
    public static List<String> mapToAllTaskIds(JsonNode root)
            throws TasksException {

        List<String> tasksList = new ArrayList<String>();

        Iterator<JsonNode> arrayIter = root.getElements();

        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();

            Iterator<JsonNode> tempNodeIter = tempArrayElement.getElements();
            JsonNode isTaskNode = tempNodeIter.next();
            if (isTaskNode.isInt()) {
                // Set TaskId
                tasksList.add(String.valueOf(isTaskNode.asInt()));
            }
        }
        return tasksList;
    }
    
    public static String mapFromTaskIds(List<String> taskIds,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        List<Map<String, String>> tasksList = new ArrayList<Map<String, String>>();
        Map<String, String> task; 
        for (String taskId : taskIds) {
            task = new HashMap<String, String>();
            task.put(OXTasksProperty.folder.name(), folder);
            task.put(OXTasksProperty.id.name(), taskId);
            tasksList.add(task);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(tasksList);
    } 

    public static List<Task> mapToAllTasks(JsonNode root)
            throws TasksException {

        List<Task> tasksList = new ArrayList<Task>();
        Iterator<JsonNode> arrayIter = root.getElements();

        while (arrayIter.hasNext()) {
            Task task = new Task();
            TaskBase base = new TaskBase();
            task.setTaskBase(base);
            
            columnsToTaskBaseFields(arrayIter.next(), base);
            
            tasksList.add(task);
        }
        return tasksList;
    }

}
