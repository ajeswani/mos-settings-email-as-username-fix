/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.service.logging;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
/**
 * Mailbox logging service exposed to client which is responsible for doing basic
 * mailbox related logging activities.
 *
 * @author mxos-dev
 */
public class BackendLoggingMailboxService implements IMailboxService {

    private static Logger logger = Logger
    .getLogger(BackendLoggingMailboxService.class);
    
    /**
     * Default Constructor.
     */
    public BackendLoggingMailboxService() {
        logger.info("BackendLoggingMailboxService created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
        throws MxOSException {
        long t0 = Stats.startTimer();
        final MxOSRequestState mxosRequestState = new MxOSRequestState(
                inputParams, ServiceEnum.LoggingMailboxService, Operation.PUT);
        if (inputParams == null || inputParams.size() <= 1) {
            ExceptionUtils.createInvalidRequestException(mxosRequestState);
        }
        ActionUtils.setBackendState(mxosRequestState);
        BackendServiceUtils.validateAndExecuteService(mxosRequestState);

        Stats.stopTimer(ServiceEnum.LoggingMailboxService, Operation.PUT, t0,
                StatStatus.pass);
        Base base = (Base) mxosRequestState.getDbPojoMap().getPropertyAsObject(
                MxOSPOJOs.base);
        return (Long) base.getMailboxId();
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
        throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.LoggingMailboxService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.LoggingMailboxService,
                    Operation.DELETE, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.LoggingMailboxService,
                    Operation.DELETE, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Mailbox read(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public List<Mailbox> list(Map<String, List<String>> inputParams)
			throws MxOSException {
		// TODO Auto-generated method stub
		return null;
	}
}
