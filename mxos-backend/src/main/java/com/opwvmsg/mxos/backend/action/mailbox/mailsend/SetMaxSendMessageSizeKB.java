/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set maximum send message size kb.
 *
 * @author mxos-dev
 */
public class SetMaxSendMessageSizeKB implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetMaxSendMessageSizeKB.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetMaxSendMessageSizeKB action start."));
        }
        final String maxSendMessageSizeKB = requestState.getInputParams()
                .get(MailboxProperty.maxSendMessageSizeKB.name()).get(0);

        try {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.maxSendMessageSizeKB,
                            maxSendMessageSizeKB);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set max send messages size.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_MAX_SEND_MESSAGE_SIZE_KB
                            .name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetMaxSendMessageSizeKB action end."));
        }
    }
}
