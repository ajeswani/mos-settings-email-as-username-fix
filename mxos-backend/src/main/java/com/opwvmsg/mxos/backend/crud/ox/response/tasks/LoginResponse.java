package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.crud.ox.response.JsonToSessionMapper;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.sun.jersey.api.client.ClientResponse;

public class LoginResponse extends Response {

    public ExternalSession getSession(ClientResponse resp) throws TasksException {
        JsonNode root = getTree(resp);
        String sessionId = "";
        if (root == null
                || root.path(OXTasksProperty.session.name()).isMissingNode()) {
            return null;
        } else {
            sessionId = root.path(OXTasksProperty.session.name())
                    .getTextValue();
        }

        ExternalSession session = JsonToSessionMapper.mapToSession(resp, sessionId);

        return session;
    }
}
