/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType;
import com.opwvmsg.mxos.data.enums.MxosEnums.CreateContactsFromOutgoingEmails;
import com.opwvmsg.mxos.data.enums.MxosEnums.IntAccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.Status;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailReceipt.AckReturnReceiptReq;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.data.pojos.McAfeeFilters;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.DeploymentMode;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.util.password.HashType;
import com.opwvmsg.utils.paf.util.password.Password;
import com.opwvmsg.utils.paf.util.password.PasswordException;

/**
 * Util class of controllers.
 * 
 * @author mxos-dev
 * 
 */
public final class LDAPUtils {
    private static Logger logger = Logger.getLogger(LDAPUtils.class);

    /**
     * Default private constructor.
     */
    private LDAPUtils() {

    }

    public static String[] mailboxAttributes;
    public static String[] domainAttributes;
    public static String[] groupMailboxAttributes;
    public static String[] groupAdminMailboxAttributes;
    public static String[] mailboxConfiguredAttributes;
    public static List<String> mailboxCustomAttributes;
    public static List<String> domainCustomAttributes;

    public static EnumMap<MailboxProperty, LDAPMailboxProperty> JSONToLDAP_Mailbox = new EnumMap<MailboxProperty, LDAPMailboxProperty>(
            MailboxProperty.class);
    public static EnumMap<MailboxProperty, LDAPGroupAdminMailboxProperty> JSONToLDAP_GroupAdminMailbox = new EnumMap<MailboxProperty, LDAPGroupAdminMailboxProperty>(
            MailboxProperty.class);
    public static EnumMap<DomainProperty, LDAPDomainProperty> JSONToLDAP_Domain = new EnumMap<DomainProperty, LDAPDomainProperty>(
            DomainProperty.class);
    public static EnumMap<LDAPDomainProperty, DomainProperty> LDAPToJSON_Domain = new EnumMap<LDAPDomainProperty, DomainProperty>(
            LDAPDomainProperty.class);

    static {
        // TODO: Eventually read this map from config/property file.
        JSONToLDAP_Mailbox.put(MailboxProperty.userName,
                LDAPMailboxProperty.maillogin);
        JSONToLDAP_Mailbox.put(MailboxProperty.domain,
                LDAPMailboxProperty.domainname);
        JSONToLDAP_Mailbox.put(MailboxProperty.firstName,
                LDAPMailboxProperty.cn);
        JSONToLDAP_Mailbox.put(MailboxProperty.lastName,
                LDAPMailboxProperty.sn);
        JSONToLDAP_Mailbox.put(MailboxProperty.email, LDAPMailboxProperty.mail);
        JSONToLDAP_Mailbox.put(MailboxProperty.mailboxId,
                LDAPMailboxProperty.mailboxid);
        JSONToLDAP_Mailbox.put(MailboxProperty.type,
                LDAPMailboxProperty.mailfamilymailbox);
        JSONToLDAP_Mailbox.put(MailboxProperty.msisdn,
                LDAPMailboxProperty.smsCredentialMSISDN);
        JSONToLDAP_Mailbox.put(MailboxProperty.msisdnStatus,
                LDAPMailboxProperty.smsCredentialMSISDNStatus);
        JSONToLDAP_Mailbox.put(MailboxProperty.cosId,
                LDAPMailboxProperty.adminpolicydn);
        JSONToLDAP_Mailbox.put(MailboxProperty.businessFeaturesEnabled,
                LDAPMailboxProperty.netmailrmcos);
        JSONToLDAP_Mailbox.put(MailboxProperty.fullFeaturesEnabled,
                LDAPMailboxProperty.netmailFullFeaturesEnabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.lastFullFeaturesConnectionDate,
                LDAPMailboxProperty.netmailLastFullFeaturesConnectionDate);
        JSONToLDAP_Mailbox.put(MailboxProperty.autoReplyMode,
                LDAPMailboxProperty.mailautoreplymode);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxNumAliases,
                LDAPMailboxProperty.mailmaxalternateaddresses);
        JSONToLDAP_Mailbox.put(MailboxProperty.status,
                LDAPMailboxProperty.mailboxstatus);
        JSONToLDAP_Mailbox.put(MailboxProperty.lastStatusChangeDate,
                LDAPMailboxProperty.mailStatusChangeDate);
        JSONToLDAP_Mailbox.put(MailboxProperty.notificationMailSentStatus,
                LDAPMailboxProperty.mailNotificationStatus);
        
        JSONToLDAP_Mailbox.put(MailboxProperty.allowedDomain,
                LDAPMailboxProperty.mailallowedaliasdomains);
        JSONToLDAP_Mailbox.put(MailboxProperty.emailAlias,
                LDAPMailboxProperty.mailalternateaddress);
        JSONToLDAP_Mailbox.put(MailboxProperty.forwardingAddress,
                LDAPMailboxProperty.mailforwardingaddress);
        JSONToLDAP_Mailbox.put(MailboxProperty.copyOnForward,
                LDAPMailboxProperty.maildeliveryoption);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxNumAllowedDomains,
                LDAPMailboxProperty.mailmaxallowedaliasdomains);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxSendMessageSizeKB,
                LDAPMailboxProperty.mailquotasendermaxmsgkb);
        JSONToLDAP_Mailbox.put(MailboxProperty.receiveMessages,
                LDAPMailboxProperty.maildeliveryaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.forwardingEnabled,
                LDAPMailboxProperty.maildeliveryaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockedSenders,
                LDAPMailboxProperty.maildeniedsender);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockedSenderAction,
                LDAPMailboxProperty.maildeniedsenderaction);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockedSenderMessage,
                LDAPMailboxProperty.maildeniedsendermessage);
        JSONToLDAP_Mailbox.put(MailboxProperty.filteringEnabled,
                LDAPMailboxProperty.mailmtafilterperuser);
        JSONToLDAP_Mailbox.put(MailboxProperty.rejectBouncedMessage,
                LDAPMailboxProperty.mailmtafilter);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockSendersPABActive,
                LDAPMailboxProperty.mailblocksenderspabactive);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockSendersPABAccess,
                LDAPMailboxProperty.mailblocksenderspabaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.deliveryScope,
                LDAPMailboxProperty.maildeliveryscope);
        JSONToLDAP_Mailbox.put(MailboxProperty.addressesForLocalDelivery,
                LDAPMailboxProperty.mailscoperestrictedaddress);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxReceiveMessageSizeKB,
                LDAPMailboxProperty.mailquotamaxmsgkb);
        /* Check this name-value out---> */JSONToLDAP_Mailbox.put(
                MailboxProperty.autoReplyMessage,
                LDAPMailboxProperty.autoreplymessage);
        JSONToLDAP_Mailbox.put(MailboxProperty.allowedIP,
                LDAPMailboxProperty.mailallowtheseips);
        JSONToLDAP_Mailbox.put(MailboxProperty.popAccessType,
                LDAPMailboxProperty.mailpopaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.popAuthenticationType,
                LDAPMailboxProperty.mailapopaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.popSSLAccessType,
                LDAPMailboxProperty.mailpopsslaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.imapAccessType,
                LDAPMailboxProperty.mailimapaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.imapSSLAccessType,
                LDAPMailboxProperty.mailimapsslaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.smtpAccessEnabled,
                LDAPMailboxProperty.mailsmtpaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.smtpSSLAccessEnabled,
                LDAPMailboxProperty.mailsmtpsslaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.smtpAuthenticationEnabled,
                LDAPMailboxProperty.mailsmtpauth);
        JSONToLDAP_Mailbox.put(MailboxProperty.webmailAccessType,
                LDAPMailboxProperty.mailwebmailaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.webmailSSLAccessType,
                LDAPMailboxProperty.mailwebmailsslaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.mobileMailAccessType,
                LDAPMailboxProperty.mailmobilemailaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.mobileActiveSyncAllowed,
                LDAPMailboxProperty.netmailrmactivesyncenabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxMessages,
                LDAPMailboxProperty.mailquotamaxmsgs);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxStorageSizeKB,
                LDAPMailboxProperty.mailquotatotkb);
        
        JSONToLDAP_Mailbox.put(MailboxProperty.largeMailboxPlatformEnabled,
                LDAPMailboxProperty.maillargemailboxplatform);
        JSONToLDAP_Mailbox.put(MailboxProperty.mobileMaxMessages,
                LDAPMailboxProperty.altmailQuotaMaxMsgs);
        JSONToLDAP_Mailbox.put(MailboxProperty.mobileMaxStorageSizeKB,
                LDAPMailboxProperty.altmailQuotaTotKB);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxMessagesSoftLimit,
                LDAPMailboxProperty.mailsoftquotamaxmsgs);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxStorageSizeKBSoftLimit,
                LDAPMailboxProperty.mailsoftquotatotkb);
        JSONToLDAP_Mailbox.put(MailboxProperty.mobileMaxMessagesSoftLimit,
                LDAPMailboxProperty.altmailsoftquotamaxmsgs);
        JSONToLDAP_Mailbox.put(MailboxProperty.mobileMaxStorageSizeKBSoftLimit,
                LDAPMailboxProperty.altmailsoftquotatotkb);
        JSONToLDAP_Mailbox.put(MailboxProperty.quotaWarningThreshold,
                LDAPMailboxProperty.mailquotathreshold);
        JSONToLDAP_Mailbox.put(MailboxProperty.quotaBounceNotify,
                LDAPMailboxProperty.mailquotabouncenotify);
        JSONToLDAP_Mailbox.put(MailboxProperty.folderQuota,
                LDAPMailboxProperty.mailfolderquota);        
        // MailStore GroupAdminAllocations
        JSONToLDAP_GroupAdminMailbox.put(MailboxProperty.maxAdminUsers,
                LDAPGroupAdminMailboxProperty.adminmaxusers);
        JSONToLDAP_GroupAdminMailbox.put(MailboxProperty.maxAdminStorageSizeKB,
                LDAPGroupAdminMailboxProperty.adminmaxstoragekb);        
        JSONToLDAP_Mailbox.put(MailboxProperty.externalStoreAccessAllowed,
                LDAPMailboxProperty.netmailInfoStoreAccessAllowed);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxExternalStoreSizeMB,
                LDAPMailboxProperty.netmailInfoStoreQuotaMB);
        // General Preferences
        JSONToLDAP_Mailbox.put(MailboxProperty.charset,
                LDAPMailboxProperty.msgcharset);
        JSONToLDAP_Mailbox.put(MailboxProperty.locale,
                LDAPMailboxProperty.netmaillocale);
        JSONToLDAP_Mailbox.put(MailboxProperty.timezone,
                LDAPMailboxProperty.msgtimezone);
        JSONToLDAP_Mailbox.put(MailboxProperty.parentalControlEnabled,
                LDAPMailboxProperty.mailparentalcontrol);
        JSONToLDAP_Mailbox.put(MailboxProperty.recycleBinEnabled,
                LDAPMailboxProperty.netmailusetrash);
        JSONToLDAP_Mailbox.put(MailboxProperty.preferredUserExperience,
                LDAPMailboxProperty.netmailwebapp);
        JSONToLDAP_Mailbox.put(MailboxProperty.userThemes,
                LDAPMailboxProperty.uservariantsavailable);
        JSONToLDAP_Mailbox.put(MailboxProperty.preferredTheme,
                LDAPMailboxProperty.netmailvariant);
        JSONToLDAP_Mailbox.put(MailboxProperty.webmailMTA,
                LDAPMailboxProperty.netmailmta);
        // Crdentials
        JSONToLDAP_Mailbox.put(MailboxProperty.password,
                LDAPMailboxProperty.mailpassword);
        JSONToLDAP_Mailbox.put(MailboxProperty.passwordStoreType,
                LDAPMailboxProperty.mailpasswordtype);
        JSONToLDAP_Mailbox.put(MailboxProperty.passwordRecoveryAllowed,
                LDAPMailboxProperty.mailpasswordrecovery);
        JSONToLDAP_Mailbox.put(MailboxProperty.passwordRecoveryPreference,
                LDAPMailboxProperty.mailpasswordrecoverypreference);
        JSONToLDAP_Mailbox.put(MailboxProperty.passwordRecoveryMsisdn,
                LDAPMailboxProperty.smspasswordrecoverymsisdn);
        JSONToLDAP_Mailbox.put(MailboxProperty.passwordRecoveryMsisdnStatus,
                LDAPMailboxProperty.smspasswordrecoverymsisdnstatus);
        JSONToLDAP_Mailbox.put(MailboxProperty.passwordRecoveryEmail,
                LDAPMailboxProperty.mailpasswordrecoveryemail);
        JSONToLDAP_Mailbox.put(MailboxProperty.passwordRecoveryEmailStatus,
                LDAPMailboxProperty.mailpasswordrecoveryemailstatus);
        JSONToLDAP_Mailbox.put(
                MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate,
                LDAPMailboxProperty.smspasswordrecoverymsisdnstatuschangedate);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxFailedLoginAttempts,
                LDAPMailboxProperty.mailMaxFailedLoginAttempts);
        JSONToLDAP_Mailbox.put(MailboxProperty.lastSuccessfulLoginDate,
                LDAPMailboxProperty.mailLastSuccessfulLoginDate );
        
        JSONToLDAP_Mailbox.put(MailboxProperty.failedLoginAttempts,
                LDAPMailboxProperty.mailFailedLoginAttempts);
        JSONToLDAP_Mailbox.put(MailboxProperty.lastLoginAttemptDate,
                LDAPMailboxProperty.mailLastLoginAttemptDate);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxFailedCaptchaLoginAttempts,
                LDAPMailboxProperty.mailMaxFailedCaptchaLoginAttempts);
        JSONToLDAP_Mailbox.put(MailboxProperty.failedCaptchaLoginAttempts,
                LDAPMailboxProperty.mailFailedCaptchaLoginAttempts);
        // MailSend
        JSONToLDAP_Mailbox.put(MailboxProperty.fromAddress,
                LDAPMailboxProperty.mailfrom);
        JSONToLDAP_Mailbox.put(MailboxProperty.futureDeliveryEnabled,
                LDAPMailboxProperty.mailfuturedeliveryenabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxFutureDeliveryDaysAllowed,
                LDAPMailboxProperty.mailfuturedeliverymax);
        JSONToLDAP_Mailbox.put(
                MailboxProperty.maxFutureDeliveryMessagesAllowed,
                LDAPMailboxProperty.mailfuturedeliveryquota);
        JSONToLDAP_Mailbox.put(MailboxProperty.alternateFromAddress,
                LDAPMailboxProperty.netmailactivealias);
        JSONToLDAP_Mailbox.put(MailboxProperty.replyToAddress,
                LDAPMailboxProperty.mailreplyto);
        JSONToLDAP_Mailbox.put(MailboxProperty.useRichTextEditor,
                LDAPMailboxProperty.netmaildefaulteditor);
        JSONToLDAP_Mailbox.put(MailboxProperty.includeOrginalMailInReply,
                LDAPMailboxProperty.mailincludeoriginal);
        JSONToLDAP_Mailbox.put(MailboxProperty.originalMailSeperatorCharacter,
                LDAPMailboxProperty.netmailreplyformat);
        JSONToLDAP_Mailbox.put(MailboxProperty.autoSaveSentMessages,
                LDAPMailboxProperty.mailautosave);
        JSONToLDAP_Mailbox.put(MailboxProperty.addSignatureForNewMails,
                LDAPMailboxProperty.mailwebmailusesignature);
        JSONToLDAP_Mailbox.put(MailboxProperty.addSignatureInReplyType,
                LDAPMailboxProperty.netmailsignaturebeforereply);
        JSONToLDAP_Mailbox.put(MailboxProperty.autoSpellCheckEnabled,
                LDAPMailboxProperty.netmailautospellcheck);
        JSONToLDAP_Mailbox.put(MailboxProperty.confirmPromptOnDelete,
                LDAPMailboxProperty.mailwebmailconfirmdelete);
        JSONToLDAP_Mailbox.put(MailboxProperty.includeReturnReceiptReq,
                LDAPMailboxProperty.netmailrequestreturnreceipt);
        JSONToLDAP_Mailbox.put(
                MailboxProperty.numDelayedDeliveryMessagesPending,
                LDAPMailboxProperty.mailFutureDeliveryPending);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxSendMessageSizeKB,
                LDAPMailboxProperty.mailquotasendermaxmsgkb);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxAttachmentSizeKB,
                LDAPMailboxProperty.mailwebmailattachsizelimit);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxAttachmentsInSession,
                LDAPMailboxProperty.mailwebmailattachlimit);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxAttachmentsToMessage,
                LDAPMailboxProperty.mailwebmailmsgattachlimit );
        JSONToLDAP_Mailbox.put(MailboxProperty.maxCharactersPerPage,
                LDAPMailboxProperty.mailwebdisplay);
        // MailSend BmiFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.msSpamAction,
                LDAPMailboxProperty.mailbmimailwalloutboundspamactionverb);
        JSONToLDAP_Mailbox.put(MailboxProperty.msSpamMessageIndicator,
                LDAPMailboxProperty.mailbmimailwalloutboundactioninfo);
        // MailSend CommtouchFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.msCommtouchSpamAction,
                LDAPMailboxProperty.mailcommtouchoutboundspamaction);
        JSONToLDAP_Mailbox.put(MailboxProperty.msCommtouchVirusAction,
                LDAPMailboxProperty.mailcommtouchoutboundvirusaction);
        // MailSend McAfeeFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.msMcAfeeVirusAction,
                LDAPMailboxProperty.mailmcafeeoutboundvirusaction);

        // MailReceipt
        JSONToLDAP_Mailbox.put(MailboxProperty.receiveMessages,
                LDAPMailboxProperty.maildeliveryaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.forwardingEnabled,
                LDAPMailboxProperty.mailforwarding);
        JSONToLDAP_Mailbox.put(MailboxProperty.copyOnForward,
                LDAPMailboxProperty.maildeliveryoption);
        JSONToLDAP_Mailbox.put(MailboxProperty.forwardingAddress,
                LDAPMailboxProperty.mailforwardingaddress);
        JSONToLDAP_Mailbox.put(MailboxProperty.filterHTMLContent,
                LDAPMailboxProperty.netmailmsgviewprivacyfilter);
        JSONToLDAP_Mailbox.put(MailboxProperty.displayHeaders,
                LDAPMailboxProperty.netmailpreferredmsgview);
        JSONToLDAP_Mailbox.put(MailboxProperty.webmailDisplayWidth,
                LDAPMailboxProperty.netmailweblinewidth);
        JSONToLDAP_Mailbox.put(MailboxProperty.webmailDisplayFields,
                LDAPMailboxProperty.netmailmsglistcols);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxMailsPerPage,
                LDAPMailboxProperty.netmailmsgsperpage);
        JSONToLDAP_Mailbox.put(MailboxProperty.previewPaneEnabled,
                LDAPMailboxProperty.netmailEnablePreview);
        JSONToLDAP_Mailbox.put(MailboxProperty.ackReturnReceiptReq,
                LDAPMailboxProperty.netmailsendreturnreceipt);
        JSONToLDAP_Mailbox.put(MailboxProperty.deliveryScope,
                LDAPMailboxProperty.maildeliveryscope);
        JSONToLDAP_Mailbox.put(MailboxProperty.addressesForLocalDelivery,
                LDAPMailboxProperty.mailscoperestrictedaddress);
        JSONToLDAP_Mailbox.put(MailboxProperty.autoReplyMode,
                LDAPMailboxProperty.mailautoreplymode);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxReceiveMessageSizeKB,
                LDAPMailboxProperty.mailquotamaxmsgkb);
        JSONToLDAP_Mailbox.put(MailboxProperty.mobileMaxReceiveMessageSizeKB,
                LDAPMailboxProperty.altmailQuotaMaxMsgKB);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxNumForwardingAddresses,
                LDAPMailboxProperty.mailmaxforwardingaddresses);
        // MailReceipt AdminControl
        JSONToLDAP_GroupAdminMailbox.put(MailboxProperty.adminRejectAction,
                LDAPGroupAdminMailboxProperty.adminparentalrejectaction);
        JSONToLDAP_GroupAdminMailbox.put(MailboxProperty.adminRejectInfo,
                LDAPGroupAdminMailboxProperty.adminparentalrejectinfo);
        JSONToLDAP_GroupAdminMailbox.put(MailboxProperty.adminDefaultDisposition,
                LDAPGroupAdminMailboxProperty.adminparentaldefaultdisposition);
        JSONToLDAP_GroupAdminMailbox.put(MailboxProperty.adminApprovedSendersList,
                LDAPGroupAdminMailboxProperty.adminapprovedsenderslist);  
        JSONToLDAP_GroupAdminMailbox.put(MailboxProperty.adminBlockedSendersList,
                LDAPGroupAdminMailboxProperty.adminblockedsenderslist);  
        // MailReceipt SenderBlocking
        JSONToLDAP_Mailbox.put(MailboxProperty.senderBlockingAllowed,
                LDAPMailboxProperty.mailblocksendersaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.senderBlockingEnabled,
                LDAPMailboxProperty.mailblocksendersactive);
        JSONToLDAP_Mailbox.put(MailboxProperty.rejectAction,
                LDAPMailboxProperty.mailrejectaction);
        JSONToLDAP_Mailbox.put(MailboxProperty.rejectFolder,
                LDAPMailboxProperty.mailrejectInfo);
        JSONToLDAP_Mailbox.put(MailboxProperty.allowedSendersList,
                LDAPMailboxProperty.mailapprovedsenderslist);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockedSendersList,
                LDAPMailboxProperty.mailblockedsenderslist);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockSendersPABActive,
                LDAPMailboxProperty.mailblocksenderspabactive);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockSendersPABAccess,
                LDAPMailboxProperty.mailblocksenderspabaccess);
        // MailReceipt SieveFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.sieveFilteringEnabled,
                LDAPMailboxProperty.mailmtafilterperuser);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockedSenders,
                LDAPMailboxProperty.maildeniedsender);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockedSenderAction,
                LDAPMailboxProperty.maildeniedsenderaction);
        JSONToLDAP_Mailbox.put(MailboxProperty.blockedSenderMessage,
                LDAPMailboxProperty.maildeniedsendermessage);
        JSONToLDAP_Mailbox.put(MailboxProperty.rejectBouncedMessage,
                LDAPMailboxProperty.mailmtafilter);
        // MailReceipt BmiFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.bmiSpamAction,
                LDAPMailboxProperty.mailbmimailwallspamactionverb);
        JSONToLDAP_Mailbox.put(MailboxProperty.spamMessageIndicator,
                LDAPMailboxProperty.mailbmimailwallactioninfo);
        // MailReceipt CommtouchFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.commtouchSpamAction,
                LDAPMailboxProperty.mailcommtouchinboundspamaction);
        JSONToLDAP_Mailbox.put(MailboxProperty.commtouchVirusAction,
                LDAPMailboxProperty.mailcommtouchinboundvirusaction);
        // MailReceipt CloudmarkFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.spamfilterEnabled,
                LDAPMailboxProperty.asfenabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.spamPolicy,
                LDAPMailboxProperty.asfthresholdpolicy);
        JSONToLDAP_Mailbox.put(MailboxProperty.cloudmarkSpamAction,
                LDAPMailboxProperty.asfdeliveryactiondirty);
        JSONToLDAP_Mailbox.put(MailboxProperty.cleanAction,
                LDAPMailboxProperty.asfdeliveryactionclean);
        JSONToLDAP_Mailbox.put(MailboxProperty.suspectAction,
                LDAPMailboxProperty.asfdeliveryactionsuspect);
        // MailReceipt McAfeeFilters
        JSONToLDAP_Mailbox.put(MailboxProperty.mcAfeeVirusAction,
                LDAPMailboxProperty.mailmcafeeinboundvirusaction);

        // SmsServices
        JSONToLDAP_Mailbox.put(MailboxProperty.smsServicesAllowed,
                LDAPMailboxProperty.smsServicesAllowed);
        JSONToLDAP_Mailbox.put(MailboxProperty.smsServicesMsisdn,
                LDAPMailboxProperty.smsServicesMSISDN);
        JSONToLDAP_Mailbox.put(MailboxProperty.smsServicesMsisdnStatus,
                LDAPMailboxProperty.smsServicesMSISDNStatus);
        JSONToLDAP_Mailbox.put(
                MailboxProperty.lastSmsServicesMsisdnStatusChangeDate,
                LDAPMailboxProperty.smsServicesMSISDNStatusChangeDate);
        // SmsServices SmsOnline
        JSONToLDAP_Mailbox.put(MailboxProperty.smsOnlineEnabled,
                LDAPMailboxProperty.smsOnline);
        JSONToLDAP_Mailbox.put(MailboxProperty.internationalSMSAllowed,
                LDAPMailboxProperty.smsOnlineInternationalAllowed);
        JSONToLDAP_Mailbox.put(MailboxProperty.internationalSMSEnabled,
                LDAPMailboxProperty.smsOnlineInternationalEnabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxSMSPerDay,
                LDAPMailboxProperty.smsOnlineMaxSMSPerDay);
        JSONToLDAP_Mailbox.put(MailboxProperty.concatenatedSMSAllowed,
                LDAPMailboxProperty.smsOnlineConcatenatedSMSAllowed);
        JSONToLDAP_Mailbox.put(MailboxProperty.concatenatedSMSEnabled,
                LDAPMailboxProperty.smsOnlineConcatenatedSMSEnabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxConcatenatedSMSSegments,
                LDAPMailboxProperty.smsOnlineMaxSMSSegments);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxPerCaptchaSMS,
                LDAPMailboxProperty.smsCaptchaMessagesThreshold);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxPerCaptchaDurationMins,
                LDAPMailboxProperty.smsCaptchaDurationThreshold);
        // SmsServices SmsNotifications
        JSONToLDAP_Mailbox.put(MailboxProperty.smsBasicNotificationsEnabled,
                LDAPMailboxProperty.smsBasicNotifications);
        JSONToLDAP_Mailbox.put(MailboxProperty.smsAdvancedNotificationsEnabled,
                LDAPMailboxProperty.smsAdvancedNotifications);
        // InternalInfo
        JSONToLDAP_Mailbox.put(MailboxProperty.webmailVersion,
                LDAPMailboxProperty.netmailcurrentver);
        JSONToLDAP_Mailbox.put(MailboxProperty.selfCareAccessEnabled,
                LDAPMailboxProperty.mailselfcare);
        JSONToLDAP_Mailbox.put(MailboxProperty.selfCareSSLAccessEnabled,
                LDAPMailboxProperty.mailselfcaressl);
        JSONToLDAP_Mailbox.put(MailboxProperty.messageStoreHost,
                LDAPMailboxProperty.mailmessagestore);
        JSONToLDAP_Mailbox.put(MailboxProperty.smtpProxyHost,
                LDAPMailboxProperty.mailsmtprelayhost);
        JSONToLDAP_Mailbox.put(MailboxProperty.imapProxyHost,
                LDAPMailboxProperty.mailimapproxyhost);
        JSONToLDAP_Mailbox.put(MailboxProperty.popProxyHost,
                LDAPMailboxProperty.mailpopproxyhost);
        JSONToLDAP_Mailbox.put(MailboxProperty.imapProxyAuthenticationEnabled,
                LDAPMailboxProperty.imapproxyauthenticate);
        JSONToLDAP_Mailbox.put(MailboxProperty.remoteCallTracingEnabled,
                LDAPMailboxProperty.remotecalltracingenabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.autoReplyHost,
                LDAPMailboxProperty.mailautoreplyhost);
        JSONToLDAP_Mailbox.put(MailboxProperty.realm,
                LDAPMailboxProperty.mailRealm);
        JSONToLDAP_Mailbox.put(MailboxProperty.interManagerAccessEnabled,
                LDAPMailboxProperty.mailintermanager);
        JSONToLDAP_Mailbox.put(MailboxProperty.interManagerSSLAccessEnabled,
                LDAPMailboxProperty.mailintermanagerssl);
        JSONToLDAP_Mailbox.put(MailboxProperty.voiceMailAccessEnabled,
                LDAPMailboxProperty.netmailenablevoice);
        JSONToLDAP_Mailbox.put(MailboxProperty.faxAccessEnabled,
                LDAPMailboxProperty.netmailenablefax);
        JSONToLDAP_Mailbox.put(MailboxProperty.ldapUtilitiesAccessType,
                LDAPMailboxProperty.mailldapaccess);
        JSONToLDAP_Mailbox.put(MailboxProperty.addressBookProvider,
                LDAPMailboxProperty.addressbookprovider);
        JSONToLDAP_Mailbox.put(MailboxProperty.eventRecordingEnabled,
                LDAPMailboxProperty.mersenabled);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxHeaderLength,
                LDAPMailboxProperty.mersmaxheaderlength);
        JSONToLDAP_Mailbox.put(MailboxProperty.maxFilesSizeKB,
                LDAPMailboxProperty.mersfilesizekb);

        // SocialNetworks
        JSONToLDAP_Mailbox.put(MailboxProperty.socialNetworkIntegrationAllowed,
                LDAPMailboxProperty.netmailsocialnetworkintegrationallowed);
        JSONToLDAP_Mailbox.put(MailboxProperty.socialNetworkSite,
                LDAPMailboxProperty.netmailSocialNetworkSite);

        // ExternalAccounts
        JSONToLDAP_Mailbox.put(MailboxProperty.externalMailAccountsAllowed,
                LDAPMailboxProperty.netmailexternalaccounts);
        JSONToLDAP_Mailbox.put(MailboxProperty.promptForExternalAccountSync,
                LDAPMailboxProperty.netmailpopemailacctselect);
        JSONToLDAP_Mailbox.put(MailboxProperty.mailAccount,
                LDAPMailboxProperty.netmailpopacctdetails);

        JSONToLDAP_Domain.put(DomainProperty.domain,
                LDAPDomainProperty.domainname);
        JSONToLDAP_Domain.put(DomainProperty.relayHost,
                LDAPDomainProperty.mailrelayhost);
        JSONToLDAP_Domain.put(DomainProperty.alternateDomain,
                LDAPDomainProperty.mailrewritedomain);
        JSONToLDAP_Domain.put(DomainProperty.defaultMailbox,
                LDAPDomainProperty.mailwildcardaccount);
        JSONToLDAP_Domain.put(DomainProperty.type,
                LDAPDomainProperty.domaintype);

        LDAPToJSON_Domain.put(LDAPDomainProperty.domainname,
                DomainProperty.domain);
        LDAPToJSON_Domain.put(LDAPDomainProperty.mailrelayhost,
                DomainProperty.relayHost);
        LDAPToJSON_Domain.put(LDAPDomainProperty.mailrewritedomain,
                DomainProperty.alternateDomain);
        LDAPToJSON_Domain.put(LDAPDomainProperty.mailwildcardaccount,
                DomainProperty.defaultMailbox);
        LDAPToJSON_Domain.put(LDAPDomainProperty.domaintype,
                DomainProperty.type);

        getMailboxAttributes();
        getDomainAttributes();
        getGroupMailboxAttributes();
        getGroupAdminMailboxAttributes();
        getMailboxConfiguredAttributes();
    }

    @SuppressWarnings("rawtypes")
    public static void getMailboxAttributes() {
        try {
            Class clazz = LDAPMailboxProperty.class;
            Class<Enum> mailboxPropertyEnum = clazz;

            final List<String> mailboxAttributesList = new ArrayList<String>();
            for (Enum prop : mailboxPropertyEnum.getEnumConstants()) {
                mailboxAttributesList.add(prop.name());
            }
            // add custom attributes
            mailboxAttributesList.addAll(getMailboxCustomAttributes());
            // add extension attributes
            mailboxAttributesList.addAll(getExtensionAttributes());
            mailboxAttributes = mailboxAttributesList
                    .toArray(new String[mailboxAttributesList.size()]);
        } catch (Exception e) {
            logger.error("Error while get mailbox attributes.", e);
        }
    }

    @SuppressWarnings("rawtypes")
    public static void getGroupMailboxAttributes() {
        try {
            Class clazz = LDAPGroupMailboxProperty.class;
            Class<Enum> groupMailboxPropertyEnum = clazz;

            final List<String> groupMailboxAttributesList = new ArrayList<String>();
            for (Enum prop : groupMailboxPropertyEnum.getEnumConstants()) {
                groupMailboxAttributesList.add(prop.name());
            }
            groupMailboxAttributes = groupMailboxAttributesList
                    .toArray(new String[groupMailboxAttributesList.size()]);
        } catch (Exception e) {
            logger.error("Error while get mailbox attributes.", e);
        }
    }
    
    @SuppressWarnings("rawtypes")
    public static void getGroupAdminMailboxAttributes() {
        try {
            Class clazz = LDAPGroupAdminMailboxProperty.class;
            Class<Enum> groupAdminMailboxPropertyEnum = clazz;

            final List<String> groupAdminMailboxAttributesList = new ArrayList<String>();
            for (Enum prop : groupAdminMailboxPropertyEnum.getEnumConstants()) {
                groupAdminMailboxAttributesList.add(prop.name());
            }
            groupAdminMailboxAttributes = groupAdminMailboxAttributesList
                    .toArray(new String[groupAdminMailboxAttributesList.size()]);
        } catch (Exception e) {
            logger.error("Error while get mailbox attributes.", e);
        }
    }

    @SuppressWarnings("rawtypes")
    public static void getDomainAttributes() {
        try {
            Class clazz = LDAPDomainProperty.class;
            Class<Enum> mailboxPropertyEnum = clazz;

            final List<String> domainAttributesList = new ArrayList<String>();
            for (Enum prop : mailboxPropertyEnum.getEnumConstants()) {
                domainAttributesList.add(prop.name());
            }
            // add custom attributes
            domainAttributesList.addAll(getDomainCustomAttributes());
            domainAttributes = domainAttributesList
                    .toArray(new String[domainAttributesList.size()]);
        } catch (Exception e) {
            logger.error("Error while get domain attributes.", e);
        }
    }
    
    private static List<String> getExtensionAttributes() {
        try {
            final String[] extensionAttributes = MxOSConfig
                    .getExtensionsOptionalCosAttributes();
            final List<String> mailboxAttributesList = new ArrayList<String>();
            for (String extensionAttribute : extensionAttributes) {
                mailboxAttributesList.add(extensionAttribute.trim());
            }
            return mailboxAttributesList;
        } catch (Exception nEx) {
            logger.error("Error while get Extension Cos Attributes.", nEx);
        }
        return null;
    }

    public static void getMailboxConfiguredAttributes() {
        if (MxOSConfig.getMailboxReturnLdapAttributes() != null
                && !MxOSConfig.getMailboxReturnLdapAttributes().equals("")) {
            String[] configuredAttributes = MxOSConfig
                    .getMailboxReturnLdapAttributes()
                    .split(MxOSConstants.COMMA);
			try {
				final List<String> mailboxConfiguredAttributesList = new ArrayList<String>();
				for (String confuguredAttribute : configuredAttributes) {
					String mp = confuguredAttribute;
					try {
						mp = JSONToLDAP_Mailbox.get(
								MailboxProperty.valueOf(confuguredAttribute))
								.name();
					} catch (Exception e) {
						// This error occurs for all custom attributes
						logger.debug(new StringBuffer(
								"Loading the custom attribute :").append(mp));
					}
					mailboxConfiguredAttributesList.add(mp);
					if (mp.equals(MailboxProperty.extensionAttributes
							.toString())) {
						addExtensionAttributes(mailboxConfiguredAttributesList);
					}
				}
				if (!mailboxConfiguredAttributesList
						.contains(LDAPMailboxProperty.adminpolicydn.name())) {
					mailboxConfiguredAttributesList.add(JSONToLDAP_Mailbox.get(
							MailboxProperty.cosId).name());
				}
				// Below code is added for BGC to avoid the error while reading
				// mailbox MACRO API.
				if (MxOSConfig.getDeploymentMode() == DeploymentMode.bgc) {
					logger.info("Adding mandatory LDAP attributes for BGC users.");
					if (!mailboxConfiguredAttributesList
							.contains(LDAPMailboxProperty.mailboxstatus.name())) {
						mailboxConfiguredAttributesList
								.add(LDAPMailboxProperty.mailboxstatus.name());
					}
					if (!mailboxConfiguredAttributesList
							.contains(LDAPMailboxCustomProperty.bgcMailboxMigrated
									.name())) {
						mailboxConfiguredAttributesList
								.add(LDAPMailboxCustomProperty.bgcMailboxMigrated
										.name());
					}
					if (!mailboxConfiguredAttributesList
							.contains(LDAPMailboxCustomProperty.bgcEndOfContract
									.name())) {
						mailboxConfiguredAttributesList
								.add(LDAPMailboxCustomProperty.bgcEndOfContract
										.name());
					}
					if (!mailboxConfiguredAttributesList
							.contains(LDAPMailboxCustomProperty.bgcPending
									.name())) {
						mailboxConfiguredAttributesList
								.add(LDAPMailboxCustomProperty.bgcPending
										.name());
					}
				}
				mailboxConfiguredAttributes = mailboxConfiguredAttributesList
						.toArray(new String[mailboxConfiguredAttributesList
								.size()]);
			} catch (Exception e) {
                logger.error("Error while get configured mailbox attributes.",
                        e);
            }
        }
    }

    /**
     * 
     * @param mailboxConfiguredAttributesList
     */
    private static void addExtensionAttributes(
            final List<String> mailboxConfiguredAttributesList) {
        String[] extAttr = MxOSConfig.getExtensionsOptionalCosAttributes();
        if (null != extAttr && extAttr.length > 0) {
            for (String key : extAttr) {
                mailboxConfiguredAttributesList.add(key);
            }
        }
    }

    /**
     * Sets custom mailbox attributes by enum.
     * 
     * @param clazz enum which implements DataMap.Property interface
     */
    @SuppressWarnings("rawtypes")
    public static List<String> getMailboxCustomAttributes() {
        try {
            Class clazz = Class.forName(MxOSConfig
                    .getMailboxCustomFieldsClass());
            if (!clazz.isEnum()) {
                throw new IllegalArgumentException(clazz.getName()
                        + " must be enum");
            }
            Class<Enum> customMailboxPropertyEnum = clazz;
            mailboxCustomAttributes = new ArrayList<String>();
            for (Enum prop : customMailboxPropertyEnum.getEnumConstants()) {
                mailboxCustomAttributes.add(prop.name());
            }
            return mailboxCustomAttributes;
        } catch (Exception e) {
            logger.error("Error while get mailbox custom attributes.", e);
            throw new IllegalArgumentException(new StringBuffer(
                    "*/mxos/mailboxCustomFieldsClass must implement ").append(
                    DataMap.Property.class.getName()).toString());
        }
    }

    /**
     * Sets custom Domain attributes by enum.
     * 
     * @param clazz enum which implements DataMap.Property interface
     */
    @SuppressWarnings("rawtypes")
    public static List<String> getDomainCustomAttributes() {
        try {
            Class clazz = Class
                    .forName(MxOSConfig.getDomainCustomFieldsClass());
            if (!clazz.isEnum()) {
                throw new IllegalArgumentException(clazz.getName()
                        + " must be enum");
            }
            Class<Enum> customMailboxPropertyEnum = clazz;
            domainCustomAttributes = new ArrayList<String>();
            for (Enum prop : customMailboxPropertyEnum.getEnumConstants()) {
                domainCustomAttributes.add(prop.name());
            }
            return domainCustomAttributes;
        } catch (Exception e) {
            logger.error("Error while get domain custom attributes.", e);
            throw new IllegalArgumentException(new StringBuffer(
                    "*/mxos/domainCustomFieldsClass must implement ").append(
                    DataMap.Property.class.getName()).toString());
        }
    }

    /**
     * Method to get the encrypted password.
     * 
     * @param pwdProvider Password Provider
     * @param type Password type
     * @param password Password
     * @return encodedPassword
     * @throws PasswordException PasswordException
     */
    public static final String getEncryptedPassword(final Password pwdProvider,
            final HashType type, final String password)
            throws PasswordException {

        String encodedPassword = "";

        if (pwdProvider == null || type == null || password == null) {
            throw new PasswordException("Method parameters cannot be null");
        } else {
            encodedPassword = pwdProvider.hashPassword(password, type);
        }
        return encodedPassword;
    }

    /**
     * Contructs the dn string for ldap bind opearation.
     * 
     * @param username username
     * @param domain domain
     * @return dn dn
     */
    public static final String getConstructedDn(final String username,
            final String domain) {
        StringBuilder dn = new StringBuilder();
        StringBuilder dc = new StringBuilder("");
        if (domain != null && !domain.equals("")) {
            String[] tempdc = domain.split("\\.");
            if (tempdc != null && tempdc.length > 0) {
                for (int i = 0; i < tempdc.length; i++) {
                    if (i == (tempdc.length - 1)) {
                        dc.append("dc=" + tempdc[i]);
                    } else {
                        dc.append("dc=" + tempdc[i] + MxOSConstants.COMMA);
                    }
                }
            }
        }
        dn.append("mail=");
        dn.append(username);
        dn.append(MxOSConstants.AT_THE_RATE);
        dn.append(domain);
        dn.append(MxOSConstants.COMMA);
        dn.append(dc);
        return dn.toString();
    }

    /**
     * Get groupDn using groupName.
     *
     * @param groupName
     * @return
     */
    public static final String getGroupDnFromGroupName(final String groupName){
        StringBuilder dn = new StringBuilder();
        String[] groupNames = groupName.split(",");
        for (int i=1; i<groupNames.length; i++){
            if (i == (groupNames.length - 1)) {
                dn.append(groupNames[i].trim());
            } else {
                dn.append(groupNames[i].trim() + MxOSConstants.COMMA);
            }
        }
        return dn.toString();
    }

    /**
     * Get groupName from groupDn.
     *
     * @param groupDn
     * @return
     */
    public static String getGroupName(final String groupDn) {
        final String[] dc = groupDn.split(",");
        String fulldc = "";
        for (int i = 0; i < dc.length; i++) {
            if (fulldc == "") {
                fulldc = dc[i].substring(dc[i].indexOf("=") + 1);
            } else {
                fulldc = fulldc + "." + dc[i].substring(dc[i].indexOf("=") + 1);
            }
        }
        return fulldc;
    }
    
    /**
     * Get groupDn using scope.
     *
     * @param scope
     * @return
     */
    public static String getScopeSearchQuery(final String scope) {
        final String[] dc = scope.split("\\.");
        String fulldc = "";
        for (int i = 0; i < dc.length; i++) {
            if (fulldc == "") {
                fulldc = "ou=" + dc[i];
            } else {
                fulldc = fulldc + "," + "dc=" + dc[i];
            }
        }
        return fulldc;
    }
        
    /**
     * Get groupAdmin using groupName.
     *
     * @param groupName
     * @return
     */
    public static final String getGroupAdmin(final String groupName) {
        String groupAdmin = null;
        if (groupName != null && !groupName.equals("")) {
            if (groupName.contains(",ou=")) {
                String groupAdminLocal = groupName.substring(0,
                        groupName.indexOf(",ou="));
                if (groupAdminLocal.contains(MxOSConstants.EQUALS)) {
                    groupAdmin = groupAdminLocal.substring(groupAdminLocal
                            .indexOf(MxOSConstants.EQUALS) + 1);
                }
            }
        }
        return groupAdmin;
    }
    
    public static String getDomainSearchQuery(final String domain) {
        final String[] dc = domain.split("\\.");
        String fulldc = "";
        for (int i = 0; i < dc.length; i++) {
            if (fulldc == "") {
                fulldc = "dc=" + dc[i];
            } else {
                fulldc = fulldc + MxOSConstants.COMMA + "dc=" + dc[i];
            }
        }
        return fulldc;
    }

    /**
     * Contructs the sn string for ldap bind opearation.
     * 
     * @param username username
     * @param domain domain
     * @return sn sn
     */
    public static final String getXn(final String userName, final String domain) {
        final StringBuilder sn = new StringBuilder();
        if (userName != null && !userName.equals("") && domain != null
                && !domain.equals("")) {
            sn.append(userName);
            sn.append(MxOSConstants.AT_THE_RATE);
            sn.append(domain);
        }
        return sn.toString();
    }

    /**
     * Contructs the dn string for ldap unbind opearation.
     * 
     * @param email email
     * @return dn dn
     */
    public static final String getDnForDelete(final String email) {
        StringBuilder dn = new StringBuilder();
        final StringBuilder dc = new StringBuilder("");
        if (email != null && !email.equals("")) {
            String[] temp = email.split(MxOSConstants.AT_THE_RATE);
            if (temp.length > 0) {
                String[] tempdc = temp[1].split("\\.");
                if (tempdc != null && tempdc.length > 0) {
                    for (int i = 0; i < tempdc.length; i++) {
                        if (i == (tempdc.length - 1)) {
                            dc.append("dc=").append(tempdc[i]);
                        } else {
                            dc.append("dc=").append(tempdc[i])
                                    .append(MxOSConstants.COMMA);
                        }
                    }
                }
            }
        }
        dn.append("mail=");
        dn.append(email);
        dn.append(MxOSConstants.COMMA);
        dn.append(dc);
        return dn.toString();
    }

    /**
     * Get family mailbox dn to create family mailbox.
     *
     * @param email
     * @param groupName
     * @return
     * @throws InvalidRequestException
     */
    public static final String getDnForGroupMailbox(
            Map<String, List<String>> inputParams, String groupName)
            throws InvalidRequestException {
        if (inputParams.containsKey(MailboxProperty.email.name())) {
            final String email = inputParams.get(MailboxProperty.email.name())
                    .get(0);
            StringBuilder dn = new StringBuilder();
            final StringBuilder dc = new StringBuilder("");
            if (groupName != null && !groupName.equals("")) {
                final String[] tempdc = groupName.split("\\.");
                if (tempdc != null && tempdc.length > 0) {
                    for (int i = 0; i < tempdc.length; i++) {
                        if (i == (tempdc.length - 1)) {
                            dc.append("dc=").append(tempdc[i]);
                        } else {
                            dc.append("ou=").append(tempdc[i])
                                    .append(MxOSConstants.COMMA);
                        }
                    }
                }
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_GROUPNAME.name(),
                        "Invalid Group name");
            }
            dn.append("mail=");
            dn.append(email);
            dn.append(MxOSConstants.COMMA);
            dn.append(dc);
            return dn.toString();
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_EMAIL.name(),
                    "Mailbox attribute 'email' does not exist.");
        }

    }

    /**
     *
     * @param groupName
     * @return
     * @throws InvalidRequestException
     */
    public static final String getOrganizationalUnitForGroup(String groupName)
            throws InvalidRequestException {
        StringBuilder dn = new StringBuilder();
        final StringBuilder dc = new StringBuilder("");
		if (groupName != null && !groupName.equals("")) {
			final String[] tempdc = groupName.split("\\.");
			if (tempdc != null && tempdc.length > 0) {
				for (int i = 0; i < tempdc.length; i++) {
					if (i == 0) {
						dc.append("ou=").append(tempdc[i]);
					} else {
						dc.append(MxOSConstants.COMMA).append("dc=")
								.append(tempdc[i]);
					}
				}
			}
		} else {
			throw new InvalidRequestException(
					MailboxError.MBX_INVALID_GROUPNAME.name(),
					"Invalied Mailbox Data");
		}
        dn.append(dc);
        return dn.toString();
    }

    /**
     * Utility method to split the given string and return an array.
     * 
     * @param value value
     * @return array array
     */
    public static final String[] getArrayFromEmail(final String value) {
        if (value == null) {
            return null;
        }
        final String[] array = value.split(MxOSConstants.AT_THE_RATE);
        return array;
    }

    /**
     * Utility method to split the given string and return an array.
     * 
     * @param value value
     * @param regex regex
     * @return array array
     */
    public static final String[] getArray(final String value, final String regex) {
        if (value == null) {
            return null;
        }
        final String[] array = value.split(regex);
        return array;
    }

    /**
     * Method to validate the given data against given reg exp.
     * 
     * @param exp regexp
     * @param data data
     * @return boolean true - if matches, else false
     */
    public static final boolean isSearchMatches(String exp, final String data) {
        if (exp.contains("*")) {
            exp = exp.replace("*", ".*");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exp : " + exp + ", data = " + data);
        }
        final Pattern pattern = Pattern.compile(exp);
        final Matcher matcher = pattern.matcher(data);
        final boolean result = matcher.matches();
        if (logger.isDebugEnabled()) {
            logger.debug("Match Result = " + result);
        }
        return result;
    }

    public static String getMailboxDn(
            final Map<String, List<String>> inputParams)
            throws InvalidRequestException {
        if (inputParams.containsKey(MailboxProperty.email.name())) {
            final String email = inputParams.get(MailboxProperty.email.name())
                    .get(0);
            return getMailboxDn(email);
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_EMAIL.name(),
                    "Mailbox attribute 'email' does not exist.");
        }
    }

    /**
     * Contructs the dn string for ldap bind opearation.
     * 
     * @param mailboxData mailboxData
     * @return dn dn
     */
    public static String getMailboxDn(final String email)
            throws InvalidRequestException {
        StringBuilder dn = new StringBuilder();
        final String domain = ActionUtils.getDomainFromEmail(email);
        final StringBuilder dc = new StringBuilder("");
        if (domain != null && !domain.equals("")) {
            final String[] tempdc = domain.split("\\.");
            if (tempdc != null && tempdc.length > 0) {
                for (int i = 0; i < tempdc.length; i++) {
                    if (i == (tempdc.length - 1)) {
                        dc.append("dc=").append(tempdc[i]);
                    } else {
                        dc.append("dc=").append(tempdc[i])
                                .append(MxOSConstants.COMMA);
                    }
                }
            }
        }
        dn.append("mail=");
        dn.append(email);
        dn.append(MxOSConstants.COMMA);
        dn.append(dc);

        if (dn.toString().equals("")) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_EMAIL.name(),
                    "Invalied Mailbox Data");
        }
        return dn.toString();
    }

    public static String getCosDn(final Map<String, List<String>> inputParams)
            throws InvalidRequestException {
        if (inputParams.containsKey(MailboxProperty.cosId.name())) {
            final String cosId = inputParams.get(MailboxProperty.cosId.name())
                    .get(0);
            return getCosDn(cosId.toLowerCase());
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_COSID.name(),
                    "Cos does not exist.");
        }
    }

    public static String getCosDn(final String cosId)
            throws InvalidRequestException {
        if (cosId != null && !cosId.equals("")) {
            String cosDn = MxOSConfig.getLdapCosBaseDn();
            return cosDn.replace("{" + MailboxProperty.cosId.name() + "}",
                    cosId);
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_COSID.name(),
                    "Cos does not exist.");
        }
    }

    /**
     * Escape a DN.  This inserts a \ before any of the following characters in
     * a string:<br> = , # + " \ &lt; &gt; ;
     *
     * @param str the unescaped DN
     * @return the escaped DN
     */
    public static String escapeDn(String str) {
        StringBuffer result = new StringBuffer(str.length() * 2);
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch == '=' || ch == ',' || ch == '#' || ch == '+' ||
                    ch == '\"' || ch == '\\' || ch == '<' || ch == '>' ||
                    ch == ';') {

                result.append('\\');
            }
            result.append(ch);
        }
        return result.toString();
    }

    /**
     * Unescape a DN.  This removes \ in front of any character in a string.
     *
     * @param str the escaped DN
     * @return the unescaped DN
     */
    public static String unescapeDn(String str) {
        StringBuffer result = new StringBuffer(str.length());
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch == '\\') {
                i++;
                if (i < str.length()) {
                    ch = str.charAt(i);
                } else {
                    break;
                }
            }
            result.append(ch);
        }
        return result.toString();
    }

    /**
     * Get Parent dn for given dn
     *
     * @param dn
     * @return
     */
    public static String getParentDn(String dn) {
        int start = 0;
        while (start < dn.length()) {
            int index = dn.indexOf(MxOSConstants.COMMA, start);
            if (index == -1) {
                return "";
            } else if (index == 0 || index == dn.length()) {
                return null;
            } else if (dn.charAt(index - 1) == '\\') {
                start = index + 1;
            } else {
                return dn.substring(index + 1);
            }
        }
        return null;
    }
    
    public static String[] getSearchCriteria(
            final Map<String, List<String>> inputParams)
            throws InvalidRequestException {
        String[] args = {};

        if (inputParams.containsKey(MailboxProperty.email.name())) {
            final String email = inputParams.get(MailboxProperty.email.name())
                    .get(0);
            args = new String[] {
                    JSONToLDAP_Mailbox.get(MailboxProperty.email).name()
                            .toLowerCase(),
                    email,
                    JSONToLDAP_Mailbox.get(MailboxProperty.emailAlias).name()
                            .toLowerCase(), email };
        } else if (inputParams.containsKey(MailboxProperty.mailboxId.name())) {
            final Long mailboxId = Long.parseLong(inputParams.get(
                    MailboxProperty.mailboxId.name()).get(0));
            args = new String[] {
                    MailboxProperty.mailboxId.name().toLowerCase(),
                    "" + mailboxId };
        } else if (inputParams.containsKey(DomainProperty.domain.name())) {
            /*
             * TODO: For now search on domain is limited only to one criteria
             * like. "domainName = *" If we need to define search on multiple
             * conditions then we need to modify this API in future.
             */
            final String domainName = inputParams.get(
                    DomainProperty.domain.name()).get(0);
            args = new String[] {
                    JSONToLDAP_Domain.get(DomainProperty.domain).name()
                            .toLowerCase(), domainName };
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_EMAIL.name(), "Invalid reqeust");
        }
        return args;
    }

    public static void populateMailbox(final Mailbox mailbox,
            final Attributes attributes) throws Exception {
        if (attributes != null && mailbox != null) {
            populateBase(mailbox.getBase(), attributes);
            populateWebMailFeatures(mailbox.getWebMailFeatures(), attributes);
            populateCredentials(mailbox.getCredentials(), attributes);
            populateGeneralPreferences(mailbox.getGeneralPreferences(),
                    attributes);
            populateMailSend(mailbox.getMailSend(), attributes);
            populateMailReceipt(mailbox.getMailReceipt(), attributes);
            populateMailAccess(mailbox.getMailAccess(), attributes);
            populateMailStore(mailbox.getMailStore(), attributes);
            populateInternalInfo(mailbox.getInternalInfo(), attributes);
            populateCustomData(mailbox.getBase(), attributes);
            populateSmsServices(mailbox.getSmsServices(), attributes);
            populateSocialNetworks(mailbox.getSocialNetworks(), attributes);
            populateExternalAccounts(mailbox.getExternalAccounts(), attributes);
        }
    }

    public static void populateBase(final Base base, final Attributes attributes)
            throws Exception {
        Attribute attr = attributes.get(LDAPMailboxProperty.mail.name());
        if (attr != null) {
            base.setDomain(ActionUtils
                    .getDomainFromEmail(attr.get().toString()));
            base.setEmail((String) attr.get());
        }
        attr = attributes.get(LDAPMailboxProperty.maillogin.name());
        if (attr != null) {
            String userName = attr.get().toString();
            String returnUserNameWithDomain = System
                    .getProperty(SystemProperty.returnUserNameWithDomain.name());
            // if storeUserNameAsEmail is true, userName must have domain.
            if (java.lang.Boolean.valueOf(returnUserNameWithDomain)) {
                if (!userName.contains(MxOSConstants.AT_THE_RATE)) {
                    userName += (MxOSConstants.AT_THE_RATE + base.getDomain());
                }
            } else {
                if (userName.contains(MxOSConstants.AT_THE_RATE)) {
                    userName = ActionUtils.getArrayFromEmail(userName)[0];
                }
            }
            base.setUserName(userName);
        }
        attr = attributes.get(LDAPMailboxProperty.cn.name());
        if (attr != null) {
            base.setFirstName(attr.get().toString());
        }
        attr = attributes.get(LDAPMailboxProperty.sn.name());
        if (attr != null) {
            base.setLastName(attr.get().toString());
        }
        attr = attributes.get(LDAPMailboxProperty.mailboxid.name());
        if (attr != null) {
            base.setMailboxId(Long.parseLong((String) attr.get()));
        }
        // Status
        attr = attributes.get(LDAPMailboxProperty.mailboxstatus.name());
        if (attr != null) {
            DeploymentMode mode = MxOSConfig.getDeploymentMode();
            if (DeploymentMode.bgc.equals(mode)) {
                // for BGC
                populateBGCStatus(base, attributes);
            } else {
                // for Generic
                populateGenericStatus(base, attributes);
            }
        }
        // lastStatusChangeDate
        attr = attributes.get(LDAPMailboxProperty.mailStatusChangeDate.name());
        if (attr != null) {
            base.setLastStatusChangeDate(populateDate(attr.get().toString()));
        }
        //notificationMailSentStatus
        attr = attributes
                .get(LDAPMailboxProperty.mailNotificationStatus.name());
        if (attr != null) {
            int iAttr = Integer.parseInt(attr.get().toString());
            base.setNotificationMailSentStatus(MxosEnums.NotificationMailSentStatus
                    .fromOrdinal(iAttr));
        }
        // msisdn
        attr = attributes.get(LDAPMailboxProperty.smsCredentialMSISDN.name());
        if (attr != null) {
            base.setMsisdn(attr.get().toString());
        }
        // msisdnStatus
        attr = attributes.get(LDAPMailboxProperty.smsCredentialMSISDNStatus
                .name());
        if (attr != null) {
            if (((String) attr.get()).equalsIgnoreCase("1")) {
                base.setMsisdnStatus(MxosEnums.MsisdnStatus.ACTIVATED);
            } else {
                base.setMsisdnStatus(MxosEnums.MsisdnStatus.DEACTIVATED);
            }
        }
        // lastMsisdnStatusChangeDate
        attr = attributes
                .get(LDAPMailboxProperty.smsCredentialMSISDNStatusChangeDate
                        .name());
        if (attr != null) {
            base.setLastMsisdnStatusChangeDate(populateDate(attr.get()
                    .toString()));
        }
        attr = attributes.get(LDAPMailboxProperty.mailallowedaliasdomains
                .name());
        if (attr != null) {
            final NamingEnumeration<? extends Object> valueEnum = attr.getAll();
            List<String> values = new ArrayList<String>();
            while (valueEnum.hasMore()) {
                values.add((String) valueEnum.next());
            }
            valueEnum.close();
            base.setAllowedDomains(values);
        }
        attr = attributes.get(LDAPMailboxProperty.mailmaxallowedaliasdomains
                .name());
        if (attr != null) {
            int maxNumAllowedDomain = -1;
            try {
                maxNumAllowedDomain = Integer.parseInt(attr.get().toString());
            } catch (Exception e) {
                logger.error("Error while populate base.", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_MAX_ALLOWEDDOMAIN.name());
            }
            if (maxNumAllowedDomain > -1) {
                base.setMaxNumAllowedDomains(maxNumAllowedDomain);
            }
        }
        attr = attributes.get(LDAPMailboxProperty.mailmaxalternateaddresses
                .name());
        if (attr != null) {
            base.setMaxNumAliases(Integer.parseInt(attr.get().toString()));
        }
        attr = attributes.get(LDAPMailboxProperty.mailalternateaddress.name());
        if (attr != null) {
            final NamingEnumeration<? extends Object> valueEnum = attr.getAll();
            List<String> values = new ArrayList<String>();
            while (valueEnum.hasMore()) {
                values.add((String) valueEnum.next());
            }
            valueEnum.close();
            base.setEmailAliases(values);
        }
        attr = attributes.get(LDAPMailboxProperty.adminpolicydn.name());
        if (attr != null) {
            String cosId = (String) attr.get();
            if (cosId != null) {
                if (base.getCosId() == null) {
                    base.setCosId(cosId.split("[=]")[1].split("[,]")[0]);
                }
            }
        }
        
        populateCustomData(base, attributes);
        populateExtensionsAttributes(base, attributes);
    }
    
    /**
     * @throws NamingException 
     * 
     */
    public static void populateExtensionsAttributes(final Base base,
            final Attributes attributes) throws NamingException {
        StringBuffer extAttributesStr = null;
        String[] extAttributes = MxOSConfig
                .getExtensionsOptionalCosAttributes();
        if (null != extAttributes && extAttributes.length > 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Count of extension attributes: "
                        + extAttributes.length);
            }
            extAttributesStr = new StringBuffer();
            for (String key : extAttributes) {
                Attribute attr = attributes.get(key);
                if (attr != null) {
                    extAttributesStr.append(key).append(":")
                            .append(attr.get().toString()).append("|");
                }
            }
        }
        if (null != extAttributesStr && extAttributesStr.length() > 0) {
            // to cut down the last pipe symbol in the string data.
            extAttributesStr.setLength(extAttributesStr.length() - 1);
            base.setExtensionAttributes(extAttributesStr.toString());
        }
    }

    public static void populateCustomData(final Base base,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            String type = MailboxType.NORMAL.getType();

            Attribute attr = attributes
                    .get(LDAPMailboxCustomProperty.bgcForwardOnly.name());
            if (attr != null) {
                final String bgcForwardOnly = (String) attr.get();
                type = getType(bgcForwardOnly);
                base.setType(MxosEnums.Type.fromValue(type));
            } else if (attributes.get(LDAPMailboxProperty.mailfamilymailbox
                    .name()) != null) {
                int mailboxtype = Integer.parseInt(attributes
                        .get(LDAPMailboxProperty.mailfamilymailbox.name())
                        .get().toString());
                if (mailboxtype == 1) {
                    base.setType(MxosEnums.Type.GROUP_ADMIN);
                    if (attributes.get(LDAPGroupAdminMailboxProperty.member
                            .name()) != null) {
                        final NamingEnumeration<? extends Object> valueEnum = attributes
                                .get(LDAPGroupAdminMailboxProperty.member
                                        .name()).getAll();
                        while (valueEnum.hasMore()) {
                            String temp = (String) valueEnum.next();
                            base.setGroupName(getGroupName(getGroupDnFromGroupName(temp)));
                        }
                        valueEnum.close();
                    }

                } else if (mailboxtype == 2) {
                    base.setType(MxosEnums.Type.GROUP_MAILBOX);
                    List<String> groupAdmin = new ArrayList<String>();
                    if (attributes.get(LDAPGroupAdminMailboxProperty.member
                            .name()) != null) {
                        final NamingEnumeration<? extends Object> valueEnum = attributes
                                .get(LDAPGroupAdminMailboxProperty.member
                                        .name()).getAll();
                        while (valueEnum.hasMore()) {
                            String temp = (String) valueEnum.next();
                            groupAdmin.add(getGroupAdmin(temp));
                            base.setGroupName(getGroupName(getGroupDnFromGroupName(temp)));
                        }
                        valueEnum.close();
                        base.setGroupAdmin(groupAdmin);
                    }
                }

            } else {
                attr = attributes.get(LDAPMailboxProperty.maildeliveryoption
                        .name());
                if (attr != null) {
                    final String mailDeliveryOption = (String) attr.get();
                    if (mailDeliveryOption
                            .equalsIgnoreCase(MailboxType.MAILINGLISTSUBSCRIBER
                                    .getValue())) {
                        type = MailboxType.MAILINGLISTSUBSCRIBER.getType();
                    } else if (mailDeliveryOption
                            .equalsIgnoreCase(MailboxType.MAILINGLIST
                                    .getValue())) {
                        type = MailboxType.MAILINGLIST.getType();
                    }
                }
                base.setType(MxosEnums.Type.fromValue(type));
            }

            final StringBuffer customData = new StringBuffer();
            for (String key : mailboxCustomAttributes) {
                // add this attribute data to customData if LDAP attribute key
                // does not exists in MailboxProperty enums.
                attr = attributes.get(key);
                if (attr != null) {
                    customData.append(key).append(":")
                            .append(attr.get().toString()).append("|");
                }
            }
            final int len = customData.length();
            if (len > 0) {
                customData.replace(len - 1, len, "");
                base.setCustomFields(customData.toString());
            }
        }
    }

    /**
     * Method will return the mailbox after setting the type.
     * 
     * @param bgcForwardOnly bgcForwardOnly
     * @return String type
     */
    public static String getType(String bgcForwardOnly) {
        final String FORWARD_ONLY = "forwardOnly";
        final String NORMAL = "normal";
        if (bgcForwardOnly != null) {
            if (bgcForwardOnly.equals("1")) {
                return FORWARD_ONLY;
            } else {
                return NORMAL;
            }
        } else {
            return NORMAL;
        }
    }

    public static void populateGenericStatus(final Base base,
            final Attributes attributes) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Start of populateGenericStatus");
        }
        Attribute attr = attributes.get(LDAPMailboxProperty.mailboxstatus
                .name());
        if (attr != null) {
            GenericAccountStatus ldapStatus = GenericAccountStatus
                    .getEnumByKey((String) attr.get());
            MxosEnums.Status status = null;
            try {
                status = Status.fromValue(ldapStatus.name());
            } catch (IllegalArgumentException e) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_MAILBOX_STATUS.name()
                                + status.name());
            }

            if (ldapStatus != null) {
                base.setStatus(status);
            }
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MAILBOX_STATUS.name());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End of populateGenericStatus");
        }
    }

    public static void populateBGCStatus(final Base base,
            final Attributes attributes) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Start of populateBGCStatus");
        }
        Attribute attr = attributes.get(LDAPMailboxProperty.mailboxstatus
                .name());
        if (attr != null) {
            GenericAccountStatus ldapStatus = GenericAccountStatus.getEnumByKey((String) attr
                    .get());
            attr = attributes.get(LDAPMailboxCustomProperty.bgcMailboxMigrated
                    .name());
            String bgcMailboxMigrated = null;
            if (attr != null) {
                bgcMailboxMigrated = (String) attr.get();
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_BGCMAILBOXMIGRATED.name());
            }
            attr = attributes.get(LDAPMailboxCustomProperty.bgcEndOfContract
                    .name());
            String eoc = null;
            if (attr != null) {
                eoc = (String) attr.get();
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_BGCENDOFCONTRACT.name());
            }
            attr = attributes.get(LDAPMailboxCustomProperty.bgcPending.name());
            String pending = null;
            if (attr != null) {
                pending = (String) attr.get();
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_BGCPENDING.name());
            }
            MxosEnums.Status status = null;

			switch (ldapStatus) {
			case active:
				if (bgcMailboxMigrated != null
						&& bgcMailboxMigrated.equals("1")) {
					if (eoc != null && eoc.equals("0")) {
						status = Status.OPEN;
					} else {
						status = Status.CLOSING;
					}
				} else {
					status = Status.ACTIVE;
				}
				break;
			case maintenance:
				status = Status.MAINTENANCE;
				break;
			case locked:
				status = Status.LOCKED;
				break;
			case deleted:
				if (pending != null && pending.equals("0")) {
					status = Status.CLOSED;
				} else {
					status = Status.PENDING;
				}
				break;
			case suspended:
				status = Status.INACTIVE;
				break;
			case proxy:
				status = Status.PROXY;
				break;
			default:
				logger.info("Reached default case while getting mailbox status.");
				throw new InvalidRequestException(
						MailboxError.MBX_INVALID_MAILBOX_STATUS.name() + status);
			}
            if (ldapStatus != null) {
                base.setStatus(status);
            }
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MAILBOX_STATUS.name());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End of populateBGCStatus");
        }
    }

    /**
     * Populate WebMailFeatures Data into POJO.
     * 
     * @param dataMap dataMap
     * @throws Exception Exception
     */
    public static void populateWebMailFeatures(final WebMailFeatures wmf,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes.get(LDAPMailboxProperty.netmailrmcos
                    .name());
            if (attr != null) {
                BooleanType type = BooleanType.getByBusinessFeature(attr.get()
                        .toString());
                wmf.setBusinessFeaturesEnabled(MxosEnums.BooleanType
                        .fromValue(type.name()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.netmailFullFeaturesEnabled.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                wmf.setFullFeaturesEnabled(MxosEnums.BooleanType.fromValue(type
                        .name()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.netmailLastFullFeaturesConnectionDate
                            .name());
            if (attr != null) {
                Date ldapDate = null;
                String jsonDate = "";
                try {
                    ldapDate = new SimpleDateFormat(
                            MxOSConstants.LDAP_DATE_FORMAT).parse(attr.get()
                            .toString());
                } catch (Exception e) {
                    logger.error("Error while populate webmail features.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_LAST_FULL_FEATURES_CONN_DATE
                                    .name());
                }
                if (ldapDate != null) {
                    jsonDate = new SimpleDateFormat(
                            System.getProperty(SystemProperty.userDateFormat
                                    .name())).format(ldapDate);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_LAST_FULL_FEATURES_CONN_DATE
                                    .name());
                }
                wmf.setLastFullFeaturesConnectionDate(jsonDate);
            }
            attr = attributes.get(LDAPMailboxProperty.mailwebmailchangepassword
                    .name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                wmf.setAllowPasswordChange(MxosEnums.BooleanType.fromValue(type
                        .name()));
            }
            AddressBookFeatures ab = new AddressBookFeatures();
            LDAPUtils.populateWebMailFeaturesAddressBook(ab, attributes);
            wmf.setAddressBookFeatures(ab);
        }
    }
    
    
    /**
     * 
     * Populate WebMailFeaturesAddressBook Data into POJO.
     * 
     * @param AddressBook
     * @param attributes
     * @throws Exception Exception
     */
    public static void populateWebMailFeaturesAddressBook(final AddressBookFeatures ab,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailWebMailAddressBookLimit.name());
            if (attr != null) {
                ab.setMaxContacts(Integer.parseInt(attr.get().toString()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.netmailAddressbookContactListMax
                            .name());
            if (attr != null) {
                ab.setMaxGroups(Integer.parseInt(attr.get().toString()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.mailWebMailAddressBookListLimit
                            .name());
            if (attr != null) {
                ab.setMaxContactsPerGroup(Integer.parseInt(attr.get()
                        .toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.netMailAbEntriesPerPage
                    .name());
            if (attr != null) {
                ab.setMaxContactsPerPage(Integer
                        .parseInt(attr.get().toString()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.netMailAutoCollect.name());
            if (attr != null) {
                if (attr.get().toString().equalsIgnoreCase("none"))
                    ab.setCreateContactsFromOutgoingEmails(
                            CreateContactsFromOutgoingEmails.DISABLED);
                else if (attr.get().toString().equalsIgnoreCase("all"))
                    ab.setCreateContactsFromOutgoingEmails(
                            CreateContactsFromOutgoingEmails.FOR_ALL_ADDRESSES);
                else if (attr.get().toString().equalsIgnoreCase("to"))
                    ab.setCreateContactsFromOutgoingEmails(
                            CreateContactsFromOutgoingEmails.ONLY_FOR_ADDRESSES_IN_TO_FIELD);
            }
        }
    }

    /**
     * Method to populate Credentials object.
     * 
     * @param cred - Credentials
     * @param attributes - attributes
     * @throws Exception - on any exception
     */
    public static void populateCredentials(final Credentials cred,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailpasswordtype.name());
            if (attr != null) {
                String passwordType = attr.get().toString();
                PasswordType pType = PasswordType.getTypeWithKey(passwordType);
                if (cred.getPasswordStoreType() == null) {
                    cred.setPasswordStoreType(MxosEnums.PasswordStoreType.fromValue(pType.getText()));
                }
            }
            attr = attributes.get(LDAPMailboxProperty.mailLastLoginAttemptDate
                    .name());
            if (attr != null) {
                Date ldapDateFormat = null;
                String jsonDateFormat = "";
                try {
                    ldapDateFormat = new SimpleDateFormat(
                            MxOSConstants.LDAP_DATE_FORMAT).parse(attr.get()
                            .toString());
                } catch (Exception e) {
                    logger.error("Error while populating credentials.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_LAST_LOGIN_ATTEMPTS_DATE
                                    .name());
                }
                if (ldapDateFormat != null) {
                    jsonDateFormat = new SimpleDateFormat(
                            System.getProperty(SystemProperty.userDateFormat
                                    .name())).format(ldapDateFormat);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_LAST_LOGIN_ATTEMPTS_DATE
                                    .name());
                }
                cred.setLastLoginAttemptDate(jsonDateFormat);
            }
            attr = attributes.get(LDAPMailboxProperty.mailFailedLoginAttempts
                    .name());
            if (attr != null) {
                Integer failedLoginAttempts = Integer.parseInt(attr.get()
                        .toString());
                cred.setFailedLoginAttempts(failedLoginAttempts);
            }
            attr = attributes
                    .get(LDAPMailboxProperty.mailMaxFailedLoginAttempts.name());
            if (attr != null) {
                Integer maxFailedLoginAttempts = Integer.parseInt(attr.get()
                        .toString());
                cred.setMaxFailedLoginAttempts(maxFailedLoginAttempts);
            }
            attr = attributes.get(LDAPMailboxProperty.mailFailedCaptchaLoginAttempts
                    .name());
            if (attr != null) {
                Integer failedCaptchaLoginAttempts = Integer.parseInt(attr.get()
                        .toString());
                cred.setFailedCaptchaLoginAttempts(failedCaptchaLoginAttempts);
            }
            attr = attributes
                    .get(LDAPMailboxProperty.mailMaxFailedCaptchaLoginAttempts.name());
            if (attr != null) {
                Integer maxFailedCaptchaLoginAttempts = Integer.parseInt(attr.get()
                        .toString());
                cred.setMaxFailedCaptchaLoginAttempts(maxFailedCaptchaLoginAttempts);
            }
            attr = attributes.get(LDAPMailboxProperty.mailpassword.name());
            if (attr != null) {
                String rawPassword = attr.get().toString();
                Password passwordProvider = MxOSApp.getInstance()
                        .getPasswordProvider();
                PasswordType pType = PasswordType.getTypeWithValue(cred
                        .getPasswordStoreType().toString());
                String ripePassword = PasswordUtil.dencryptPassword(
                        passwordProvider, pType, rawPassword);
                cred.setPassword(ripePassword);
            }
            attr = attributes.get(LDAPMailboxProperty.mailpasswordrecovery
                    .name());
            if (attr != null) {
               if (attr.get()
                        .toString()
                        .equals(Integer.toString(MxosEnums.BooleanType.YES
                                .ordinal()))) {
                    cred.setPasswordRecoveryAllowed(MxosEnums.BooleanType.YES);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer.toString(MxosEnums.BooleanType.NO
                                .ordinal()))) {
                    cred.setPasswordRecoveryAllowed(MxosEnums.BooleanType.NO);
                } else {
                    new InvalidRequestException(
                            MailboxError.MBX_INVALID_MAILPASSWORD_RECOVERY
                                    .name());
                }
            }
            attr = attributes
                    .get(LDAPMailboxProperty.mailpasswordrecoverypreference
                            .name());
            if (attr != null) {
                if (attr.get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.PasswordRecoveryPreference.NONE
                                        .ordinal()))) {
                    cred.setPasswordRecoveryPreference(MxosEnums.PasswordRecoveryPreference.NONE);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.PasswordRecoveryPreference.EMAIL
                                        .ordinal()))) {
                    cred.setPasswordRecoveryPreference(MxosEnums.PasswordRecoveryPreference.EMAIL);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.PasswordRecoveryPreference.SMS
                                        .ordinal()))) {
                    cred.setPasswordRecoveryPreference(MxosEnums.PasswordRecoveryPreference.SMS);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.PasswordRecoveryPreference.ALL
                                        .ordinal()))) {
                    cred.setPasswordRecoveryPreference(MxosEnums.PasswordRecoveryPreference.ALL);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_MAILPASSWORD_RECOVERY_PREF
                                    .name());
                }
            }
            attr = attributes.get(LDAPMailboxProperty.smspasswordrecoverymsisdn
                    .name());
            if (attr != null) {
                cred.setPasswordRecoveryMsisdn(attr.get().toString());
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smspasswordrecoverymsisdnstatus
                            .name());
            if (attr != null) {
               if (attr.get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.MsisdnStatus.ACTIVATED
                                        .ordinal()))) {
                    cred.setPasswordRecoveryMsisdnStatus(MxosEnums.MsisdnStatus.ACTIVATED);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.MsisdnStatus.DEACTIVATED
                                        .ordinal()))) {
                    cred.setPasswordRecoveryMsisdnStatus(MxosEnums.MsisdnStatus.DEACTIVATED);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_SMSPASSWORD_RECOVERY_MSISDN_STATUS
                                    .name());
                }
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smspasswordrecoverymsisdnstatuschangedate
                            .name());
            if (attr != null) {
               Date ldapDateFormat = null;
                String jsonDateFormat = "";
                try {
                    ldapDateFormat = new SimpleDateFormat(
                            MxOSConstants.LDAP_DATE_FORMAT).parse(attr.get()
                            .toString());
                } catch (Exception e) {
                    logger.error("Error while populate credentials.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_PASS_REC_MSISDN_STATUS_CHANGE_DATE
                                    .name());
                }
                if (ldapDateFormat != null) {
                    jsonDateFormat = new SimpleDateFormat(
                            System.getProperty(SystemProperty.userDateFormat
                                    .name())).format(ldapDateFormat);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_PASS_REC_MSISDN_STATUS_CHANGE_DATE
                                    .name());
                }
                cred.setLastPasswordRecoveryMsisdnStatusChangeDate(jsonDateFormat);
            }
            attr = attributes.get(LDAPMailboxProperty.mailpasswordrecoveryemail
                    .name());
            if (attr != null) {
                cred.setPasswordRecoveryEmail(attr.get().toString());
            }
            attr = attributes
                    .get(LDAPMailboxProperty.mailpasswordrecoveryemailstatus
                            .name());
            if (attr != null) {
                if (attr.get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.MsisdnStatus.ACTIVATED
                                        .ordinal()))) {
                    cred.setPasswordRecoveryEmailStatus(MxosEnums.MsisdnStatus.ACTIVATED);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer
                                .toString(MxosEnums.MsisdnStatus.DEACTIVATED
                                        .ordinal()))) {
                    cred.setPasswordRecoveryEmailStatus(MxosEnums.MsisdnStatus.DEACTIVATED);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_MAILPASSWORD_RECOVERY_EMAIL_STATUS
                                    .name());
                }
            }
            //lastSuccessfulLoginDate
            attr = attributes
                    .get(LDAPMailboxProperty.mailLastSuccessfulLoginDate.name());
            if (attr != null) {
                Date ldapDateFormat = null;
                String jsonDateFormat = "";
                try {
                    ldapDateFormat = new SimpleDateFormat(
                            MxOSConstants.LDAP_DATE_FORMAT).parse(attr.get()
                            .toString());
                } catch (Exception e) {
                    logger.error("Error while populate credentials.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_LAST_SUCCESSFUL_LOGIN_DATE.name());
                }
                if (ldapDateFormat != null) {
                    jsonDateFormat = new SimpleDateFormat(
                            System.getProperty(SystemProperty.userDateFormat
                                    .name())).format(ldapDateFormat);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_LAST_SUCCESSFUL_LOGIN_DATE.name());
                }
                cred.setLastSuccessfulLoginDate(jsonDateFormat);

            }   
        }
    }

    /**
     * Method to populate raw Credentials object for Authentication purposes.
     * 
     * @param cred - Credentials
     * @param attributes - attributes
     * @throws Exception - on any exception
     */
    public static void populateRawCredentials(final Credentials cred,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailpasswordtype.name());
            if (attr != null) {
                String passwordType = attr.get().toString();
                PasswordType pType = PasswordType.getTypeWithKey(passwordType);
                if (cred.getPasswordStoreType() == null) {
                    cred.setPasswordStoreType(MxosEnums.PasswordStoreType.fromValue(pType.getText()));
                }
            }
            attr = attributes.get(LDAPMailboxProperty.mailpassword.name());
            if (attr != null) {
                String rawPassword = attr.get().toString();
                cred.setPassword(rawPassword);
            }            
        }
    }
    
    /**
     * Method to populate General Preferences object.
     * 
     * @param gp - GeneralPreferences
     * @param attributes - attributes
     * @throws Exception - on any exception
     */
    public static void populateGeneralPreferences(final GeneralPreferences gp,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes.get(LDAPMailboxProperty.msgcharset
                    .name());
            if (attr != null) {
                gp.setCharset(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.netmaillocale.name());
            if (attr != null) {
                gp.setLocale(attr.get().toString().replace('-', '_'));
            }
            attr = attributes.get(LDAPMailboxProperty.msgtimezone.name());
            if (attr != null) {
                gp.setTimezone(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.mailparentalcontrol
                    .name());
            if (attr != null) {
                if (attr.get()
                        .toString()
                        .equals(Integer.toString(MxosEnums.BooleanType.YES
                                .ordinal()))) {
                    gp.setParentalControlEnabled(MxosEnums.BooleanType.YES);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer.toString(MxosEnums.BooleanType.NO
                                .ordinal()))) {
                    gp.setParentalControlEnabled(MxosEnums.BooleanType.NO);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_MAILPARENTAL_CONTROL
                                    .name());
                }
            }
            attr = attributes.get(LDAPMailboxProperty.netmailusetrash.name());
            if (attr != null) {
                if (attr.get()
                        .toString()
                        .equals(Integer.toString(MxosEnums.BooleanType.YES
                                .ordinal()))) {
                    gp.setRecycleBinEnabled(MxosEnums.BooleanType.YES);
                } else if (attr
                        .get()
                        .toString()
                        .equals(Integer.toString(MxosEnums.BooleanType.NO
                                .ordinal()))) {
                    gp.setRecycleBinEnabled(MxosEnums.BooleanType.NO);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_NETMAIL_USETRASH.name());
                }
            }
            attr = attributes.get(LDAPMailboxProperty.netmailwebapp.name());
            if (attr != null) {
                if (attr.get()
                        .toString()
                        .equalsIgnoreCase(
                                MxosEnums.PreferredUserExperience.BASIC.name())) {
                    gp.setPreferredUserExperience(MxosEnums.PreferredUserExperience.BASIC);
                } else if (attr
                        .get()
                        .toString()
                        .equalsIgnoreCase(
                                MxosEnums.PreferredUserExperience.RICH.name())) {
                    gp.setPreferredUserExperience(MxosEnums.PreferredUserExperience.RICH);
                } else if (attr
                        .get()
                        .toString()
                        .equalsIgnoreCase(
                                MxosEnums.PreferredUserExperience.MOBILE.name())) {
                    gp.setPreferredUserExperience(MxosEnums.PreferredUserExperience.MOBILE);
                } else if (attr
                        .get()
                        .toString()
                        .equalsIgnoreCase(
                                MxosEnums.PreferredUserExperience.TABLET.name())) {
                    gp.setPreferredUserExperience(MxosEnums.PreferredUserExperience.TABLET);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_NETMAIL_WEBAPP.name());
                }
            }
            attr = attributes.get(LDAPMailboxProperty.uservariantsavailable
                    .name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                while (valueEnum.hasMore()) {
                    values.add((String) valueEnum.next());
                }
                valueEnum.close();
                gp.setUserThemesAvailable(values);
            }
            attr = attributes.get(LDAPMailboxProperty.netmailvariant.name());
            if (attr != null) {
                gp.setPreferredTheme(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.netmailmta.name());
            if (attr != null) {
                gp.setWebmailMTA(attr.get().toString());
            }
        }
    }

    /**
     * Populate MailSend Data into POJO.
     * 
     * @param dataMap dataMap
     * @return MailSend MailSend
     * @throws Exception Exception
     */
    public static void populateMailSend(final MailSend ms,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            // MailSend attributes
            // fromAddress
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailfrom.name());
            if (attr != null) {
                ms.setFromAddress(attr.get().toString());
            }
            // futureDeliveryEnabled
            attr = attributes.get(LDAPMailboxProperty.mailfuturedeliveryenabled
                    .name());
            if (attr != null) {
                ms.setFutureDeliveryEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // maxFutureDeliveryDaysAllowed
            attr = attributes.get(LDAPMailboxProperty.mailfuturedeliverymax
                    .name());
            if (attr != null) {
                ms.setMaxFutureDeliveryDaysAllowed(Integer.parseInt(attr.get()
                        .toString()));
            }
            // maxFutureDeliveryMessagesAllowed
            attr = attributes.get(LDAPMailboxProperty.mailfuturedeliveryquota
                    .name());
            if (attr != null) {
                ms.setMaxFutureDeliveryMessagesAllowed(Integer.parseInt(attr
                        .get().toString()));
            }
            // alternateFromAddeess
            attr = attributes
                    .get(LDAPMailboxProperty.netmailactivealias.name());
            if (attr != null) {
                ms.setAlternateFromAddress(attr.get().toString());
            }
            // replyToAddress
            attr = attributes.get(LDAPMailboxProperty.mailreplyto.name());
            if (attr != null) {
                ms.setReplyToAddress(attr.get().toString());
            }
            // useRichTextEditor
            attr = attributes.get(LDAPMailboxProperty.netmaildefaulteditor
                    .name());
            if (attr != null) {
                ms.setUseRichTextEditor(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // includeOrginalMailInReply
            attr = attributes.get(LDAPMailboxProperty.mailincludeoriginal
                    .name());
            if (attr != null) {
                ms.setIncludeOrginalMailInReply(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // originalMailSeperatorCharacter
            attr = attributes
                    .get(LDAPMailboxProperty.netmailreplyformat.name());
            if (attr != null) {
                // The below code is just a hack, need to handle this at frame
                // work level
                String value = attr.get().toString();
                if (value.equals("0")) {
                    ms.setOriginalMailSeperatorCharacter("=");
                } else if (value.equals("1")) {
                    ms.setOriginalMailSeperatorCharacter(">");
                } else if (value.equals("2")) {
                    ms.setOriginalMailSeperatorCharacter("none");
                }
            }
            // autoSaveSentMessages
            attr = attributes.get(LDAPMailboxProperty.mailautosave.name());
            if (attr != null) {
                ms.setAutoSaveSentMessages(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // addSignatureForNewMails
            attr = attributes.get(LDAPMailboxProperty.mailwebmailusesignature
                    .name());
            if (attr != null) {
                ms.setAddSignatureForNewMails(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // addSignatureInReplyType
            attr = attributes
                    .get(LDAPMailboxProperty.netmailsignaturebeforereply.name());
            if (attr != null) {
                ms.setAddSignatureInReplyType(MxosEnums.SignatureInReplyType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // autoSpellCheckEnabled
            attr = attributes.get(LDAPMailboxProperty.netmailautospellcheck
                    .name());
            if (attr != null) {
                ms.setAutoSpellCheckEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // confirmPromptOnDelete
            attr = attributes.get(LDAPMailboxProperty.mailwebmailconfirmdelete
                    .name());
            if (attr != null) {
                ms.setConfirmPromptOnDelete(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // includeReturnReceiptReq
            attr = attributes
                    .get(LDAPMailboxProperty.netmailrequestreturnreceipt.name());
            if (attr != null) {
                ms.setIncludeReturnReceiptReq(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // numDelayedDeliveryMessagesPending
            attr = attributes.get(LDAPMailboxProperty.mailFutureDeliveryPending
                    .name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                String timeInUserFormat = "";
                String messageId = "";
                while (valueEnum.hasMore()) {
                    String value = (String) valueEnum.next();
                    String[] timeMessageID = value.split(MxOSConstants.COMMA);
                    timeInUserFormat = populateDateFromEpoch(timeMessageID[0]);
                    messageId = timeMessageID[1];
                    StringBuffer sb = new StringBuffer();
                    sb.append(timeInUserFormat);
                    sb.append(MxOSConstants.COMMA);
                    sb.append(messageId);
                    values.add(sb.toString());
                }
                valueEnum.close();
                ms.setNumDelayedDeliveryMessagesPending(values);
            }
            // maxSendMessageSizeKB
            attr = attributes.get(LDAPMailboxProperty.mailquotasendermaxmsgkb
                    .name());
            if (attr != null) {
                ms.setMaxSendMessageSizeKB(Integer.parseInt(attr.get()
                        .toString()));
            }
            // maxAttachmentSizeKB
            attr = attributes
                    .get(LDAPMailboxProperty.mailwebmailattachsizelimit.name());
            if (attr != null) {
                ms.setMaxAttachmentSizeKB(Integer.parseInt(attr.get()
                        .toString()));
            }
            // maxAttachments
            attr = attributes.get(LDAPMailboxProperty.mailwebmailattachlimit
                    .name());
            if (attr != null) {
                ms.setMaxAttachmentsInSession(Integer.parseInt(attr.get().toString()));
            }
            // maxAttachmentsToMessage
            attr = attributes.get(LDAPMailboxProperty.mailwebmailmsgattachlimit
                    .name()); 
            if (attr != null) {
                ms.setMaxAttachmentsToMessage(Integer.parseInt(attr.get().toString()));
            }
            // maxCharactersPerPage
            attr = attributes.get(LDAPMailboxProperty.mailwebdisplay.name());
            if (attr != null) {
                ms.setMaxCharactersPerPage(Integer.parseInt(attr.get()
                        .toString()));
            }
            // Filters
            Filters filters = new Filters();
            // BMIFilters
            BmiFilters bmiFilters = new BmiFilters();
            // spamAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailbmimailwalloutboundspamactionverb
                            .name());
            if (attr != null) {
                bmiFilters.setSpamAction(MxosEnums.BmiSpamType.fromValue(attr
                        .get().toString()));
            }
            // spamMessageIndicator
            attr = attributes
                    .get(LDAPMailboxProperty.mailbmimailwalloutboundactioninfo
                            .name());
            if (attr != null) {
                bmiFilters.setSpamMessageIndicator(attr.get().toString());
            }
            filters.setBmiFilters(bmiFilters);
            // CommtouchFilters
            CommtouchFilters commtouchFilters = new CommtouchFilters();
            // spamAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailcommtouchoutboundspamaction
                            .name());
            if (attr != null) {
                commtouchFilters.setSpamAction(MxosEnums.CommtouchActionType
                        .fromValue(attr.get().toString()));
            }
            // virusAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailcommtouchoutboundvirusaction
                            .name());
            if (attr != null) {
                commtouchFilters.setVirusAction(MxosEnums.CommtouchActionType
                        .fromValue(attr.get().toString()));
            }
            filters.setCommtouchFilters(commtouchFilters);

            // McAfeeFilters
            McAfeeFilters mcAfeeFilters = new McAfeeFilters();
            // virusAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailmcafeeoutboundvirusaction
                            .name());
            if (attr != null) {
                mcAfeeFilters.setVirusAction(MxosEnums.McAfeeVirusActionType
                        .fromValue(attr.get().toString()));
            }
            filters.setMcAfeeFilters(mcAfeeFilters);

            ms.setFilters(filters);
        }
    }

    /**
     * Populate MailReceipt Data into POJO.
     * 
     * @param dataMap dataMap
     * @return MailReceipt MailReceipt
     * @throws Exception Exception
     */
    public static void populateMailReceipt(final MailReceipt mr,
            final Attributes attributes) throws Exception {

        if (attributes != null) {
            // receiveMessages
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.maildeliveryaccess.name());
            if (attr != null) {
                mr.setReceiveMessages(MxosEnums.BooleanType.fromOrdinal(Integer
                        .parseInt(attr.get().toString())));
            }
            // forwardingEnabled
            attr = attributes.get(LDAPMailboxProperty.mailforwarding.name());
            if (attr != null) {
                mr.setForwardingEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // copyOnForward
            attr = attributes
                    .get(LDAPMailboxProperty.maildeliveryoption.name());
            if (attr != null) {
                mr.setCopyOnForward(MxosEnums.BooleanType.fromValue(MailForward
                        .getString(attr.get().toString())));
            }
            // forwardingAddress
            attr = attributes.get(LDAPMailboxProperty.mailforwardingaddress
                    .name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                while (valueEnum.hasMore()) {
                    values.add((String) valueEnum.next());
                }
                valueEnum.close();
                mr.setForwardingAddress(values);
            }
            // filterHTMLContent
            attr = attributes
                    .get(LDAPMailboxProperty.netmailmsgviewprivacyfilter.name());
            if (attr != null) {
                mr.setFilterHTMLContent(MxosEnums.BooleanType
                        .fromValue(FilterHTMLContentType.getEnum(
                                attr.get().toString()).name()));
            }
            // displayHeaders
            attr = attributes.get(LDAPMailboxProperty.netmailpreferredmsgview
                    .name());
            if (attr != null) {
                mr.setDisplayHeaders(MxosEnums.DisplayHeadersType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // webmailDisplayWidth
            attr = attributes.get(LDAPMailboxProperty.netmailweblinewidth
                    .name());
            if (attr != null) {
                mr.setWebmailDisplayWidth(Integer.parseInt(attr.get().toString()));
            }
            // webmailDisplayFields
            attr = attributes
                    .get(LDAPMailboxProperty.netmailmsglistcols.name());
            if (attr != null) {
                mr.setWebmailDisplayFields(attr.get().toString());
            }
            // maxMailsPerPage
            attr = attributes
                    .get(LDAPMailboxProperty.netmailmsgsperpage.name());
            if (attr != null) {
                mr.setMaxMailsPerPage(Integer.parseInt(attr.get().toString()));
            }
            // previewPaneEnabled
            attr = attributes.get(LDAPMailboxProperty.netmailEnablePreview
                    .name());
            if (attr != null) {
                mr.setPreviewPaneEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // ackReturnReceiptReq
            attr = attributes.get(LDAPMailboxProperty.netmailsendreturnreceipt
                    .name());
            if (attr != null) {
                mr.setAckReturnReceiptReq(AckReturnReceiptReq.fromValue(attr
                        .get().toString()));
            }
            // deliveryScope
            attr = attributes.get(LDAPMailboxProperty.maildeliveryscope.name());
            if (attr != null) {
                mr.setDeliveryScope(MxosEnums.DeliveryScope.fromValue(attr
                        .get().toString()));
            }
            // addressesForLocalDelivery
            attr = attributes
                    .get(LDAPMailboxProperty.mailscoperestrictedaddress.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                while (valueEnum.hasMore()) {
                    values.add((String) valueEnum.next());
                }
                valueEnum.close();
                mr.setAddressesForLocalDelivery(values);
            }
            // autoReplyMode
            attr = attributes.get(LDAPMailboxProperty.mailautoreplymode.name());
            if (attr != null) {
                mr.setAutoReplyMode(MxosEnums.AutoReplyMode
                        .fromValue(AutoReplyMode.getAutoReplyModeValue(attr
                                .get().toString())));
            }

            // maxReceiveMessageSizeKB
            attr = attributes.get(LDAPMailboxProperty.mailquotamaxmsgkb.name());
            if (attr != null) {
                mr.setMaxReceiveMessageSizeKB(Integer.parseInt(attr.get()
                        .toString()));
            }
            // mobileMaxReceiveMessageSizeKB
            attr = attributes.get(LDAPMailboxProperty.altmailQuotaMaxMsgKB.name());
            if (attr != null) {
                mr.setMobileMaxReceiveMessageSizeKB(Integer.parseInt(attr.get()
                        .toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.mailmaxforwardingaddresses.name());
            if (attr != null) {
                String val = attr.get().toString() ;
                mr.setMaxNumForwardingAddresses(Integer.valueOf(val)) ;
            }
            // senderBlocking
            SenderBlocking senderBlocking = new SenderBlocking();
            // senderBlockingAllowed
            attr = attributes.get(LDAPMailboxProperty.mailblocksendersaccess
                    .name());
            if (attr != null) {
                senderBlocking.setSenderBlockingAllowed(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // senderBlockingEnabled
            attr = attributes.get(LDAPMailboxProperty.mailblocksendersactive
                    .name());
            if (attr != null) {
                senderBlocking.setSenderBlockingEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // rejectAction
            attr = attributes.get(LDAPMailboxProperty.mailrejectaction.name());
            if (attr != null) {
                senderBlocking.setRejectAction(MxosEnums.RejectActionType
                        .fromValue(attr.get().toString()));
            }
            // rejectFolder
            attr = attributes.get(LDAPMailboxProperty.mailrejectInfo.name());
            if (attr != null) {
                senderBlocking.setRejectFolder(attr.get().toString());
            }
            // allowedSendersList
            attr = attributes.get(LDAPMailboxProperty.mailapprovedsenderslist
                    .name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                while (valueEnum.hasMore()) {
                    values.add((String) valueEnum.next());
                }
                valueEnum.close();
                senderBlocking.setAllowedSendersList(values);
            }
            // blockedSendersList
            attr = attributes.get(LDAPMailboxProperty.mailblockedsenderslist
                    .name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                while (valueEnum.hasMore()) {
                    values.add((String) valueEnum.next());
                }
                valueEnum.close();
                senderBlocking.setBlockedSendersList(values);
            }
            // blockSendersPABActive
            attr = attributes.get(LDAPMailboxProperty.mailblocksenderspabactive
                    .name());
            if (attr != null) {
                senderBlocking.setBlockSendersPABActive(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // blockSendersPABAccess
            attr = attributes.get(LDAPMailboxProperty.mailblocksenderspabaccess
                    .name());
            if (attr != null) {
                senderBlocking.setBlockSendersPABAccess(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            mr.setSenderBlocking(senderBlocking);

            // Filters
            Filters filters = new Filters();
            // SieveFilters
            SieveFilters sieveFilters = new SieveFilters();
            // sieveFilteringEnabled
            attr = attributes.get(LDAPMailboxProperty.mailmtafilterperuser
                    .name());
            if (attr != null) {
                sieveFilters.setSieveFilteringEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            // blockedSenders
            attr = attributes.get(LDAPMailboxProperty.maildeniedsender.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                while (valueEnum.hasMore()) {
                    values.add((String) valueEnum.next());
                }
                valueEnum.close();
                sieveFilters.setBlockedSenders(values);
            }
            // blockedSenderAction
            attr = attributes.get(LDAPMailboxProperty.maildeniedsenderaction
                    .name());
            if (attr != null) {
                sieveFilters
                        .setBlockedSenderAction(MxosEnums.BlockedSenderAction
                                .fromValue(attr.get().toString()));
            }
            // blockedSenderMessage
            attr = attributes.get(LDAPMailboxProperty.maildeniedsendermessage
                    .name());
            if (attr != null) {
                sieveFilters.setBlockedSenderMessage(attr.get().toString());
            }
            // rejectBouncedMessage
            attr = attributes.get(LDAPMailboxProperty.mailmtafilter.name());
            if (attr != null) {
                sieveFilters.setRejectBouncedMessage(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            filters.setSieveFilters(sieveFilters);
            // BMIFilters
            BmiFilters bmiFilters = new BmiFilters();
            // spamAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailbmimailwallspamactionverb
                            .name());
            if (attr != null) {
                bmiFilters.setSpamAction(MxosEnums.BmiSpamType.fromValue(attr
                        .get().toString()));
            }
            // spamMessageIndicator
            attr = attributes.get(LDAPMailboxProperty.mailbmimailwallactioninfo
                    .name());
            if (attr != null) {
                bmiFilters.setSpamMessageIndicator(attr.get().toString());
            }
            filters.setBmiFilters(bmiFilters);
            // CommtouchFilters
            CommtouchFilters commtouchFilters = new CommtouchFilters();
            // spamAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailcommtouchinboundspamaction
                            .name());
            if (attr != null) {
                commtouchFilters.setSpamAction(MxosEnums.CommtouchActionType
                        .fromValue(attr.get().toString()));
            }
            // virusAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailcommtouchinboundvirusaction
                            .name());
            if (attr != null) {
                commtouchFilters.setVirusAction(MxosEnums.CommtouchActionType
                        .fromValue(attr.get().toString()));
            }
            filters.setCommtouchFilters(commtouchFilters);
            // CloudmarkFilters
            CloudmarkFilters cloudmarkFilters = new CloudmarkFilters();
            // spamfilterEnabled
            attr = attributes.get(LDAPMailboxProperty.asfenabled.name());
            if (attr != null) {
                if (attr.get() != null) {
                    if (attr.get().toString()
                            .equalsIgnoreCase(MxOSConstants.TRUE))
                        cloudmarkFilters
                                .setSpamfilterEnabled(MxosEnums.BooleanType
                                        .fromValue(MxOSConstants.YES));
                    else
                        cloudmarkFilters
                                .setSpamfilterEnabled(MxosEnums.BooleanType
                                        .fromValue(MxOSConstants.NO));
                }
            }
            // spamPolicy
            attr = attributes
                    .get(LDAPMailboxProperty.asfthresholdpolicy.name());
            if (attr != null) {
                cloudmarkFilters.setSpamPolicy(MxosEnums.SpamPolicy
                        .fromValue(attr.get().toString()));
            }
            // spamAction
            attr = attributes.get(LDAPMailboxProperty.asfdeliveryactiondirty
                    .name());
            if (attr != null) {
                cloudmarkFilters.setSpamAction((attr.get().toString()));
            }
            // cleanAction
            attr = attributes.get(LDAPMailboxProperty.asfdeliveryactionclean
                    .name());
            if (attr != null) {
                cloudmarkFilters.setCleanAction((attr.get().toString()));
            }
            // suspectAction
            attr = attributes.get(LDAPMailboxProperty.asfdeliveryactionsuspect
                    .name());
            if (attr != null) {
                cloudmarkFilters.setSuspectAction((attr.get().toString()));
            }
            filters.setCloudmarkFilters(cloudmarkFilters);
            // McAfeeFilters
            McAfeeFilters mcAfeeFilters = new McAfeeFilters();
            // virusAction
            attr = attributes
                    .get(LDAPMailboxProperty.mailmcafeeinboundvirusaction
                            .name());
            if (attr != null) {
                mcAfeeFilters.setVirusAction(MxosEnums.McAfeeVirusActionType
                        .fromValue(attr.get().toString()));
            }
            filters.setMcAfeeFilters(mcAfeeFilters);

            mr.setFilters(filters);

            // adminControl
            AdminControl adminControl = new AdminControl();
            attr = attributes.get(LDAPMailboxProperty.mailfamilymailbox.name());
            if (attr != null && MxOSConfig.isGroupMailboxEnabled()) {
                populateAdminControl(adminControl, attributes);
            }
            mr.setAdminControl(adminControl);
        }
    }

    /**
     * Populate ForwardingAddresses Data into POJO.
     * 
     * @param dataMap dataMap
     * @return list of Forwarding Addresses
     * @throws Exception Exception
     */
    public static void populateForwardingAddresses(final List<String> list,
            final Attributes attributes) throws Exception {

        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailforwardingaddress.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                while (valueEnum.hasMore()) {
                    list.add((String) valueEnum.next());
                }
                valueEnum.close();
            }
        }
    }

    /**
     * Populate AddressesForLocalDelivery Data into POJO.
     * 
     * @param dataMap dataMap
     * @return list of AddressesForLocalDelivery
     * @throws Exception Exception
     */
    public static void populateAddressesForLocalDelivery(
            final List<String> list, final Attributes attributes)
            throws Exception {

        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailscoperestrictedaddress.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                while (valueEnum.hasMore()) {
                    list.add((String) valueEnum.next());
                }
                valueEnum.close();
            }
        }
    }

    /**
     * Populate AllowedSendersList Data into POJO.
     * 
     * @param dataMap dataMap
     * @return list of AllowedSenders
     * @throws Exception Exception
     */
    public static void populateAllowedSendersList(final List<String> list,
            final Attributes attributes) throws Exception {

        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailapprovedsenderslist.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                while (valueEnum.hasMore()) {
                    list.add((String) valueEnum.next());
                }
                valueEnum.close();
            }
        }
    }

    /**
     * Populate BlockedSendersList Data into POJO.
     * 
     * @param dataMap dataMap
     * @return list of BlockedSenders
     * @throws Exception Exception
     */
    public static void populateBlockedSendersList(final List<String> list,
            final Attributes attributes) throws Exception {

        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailblockedsenderslist.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                while (valueEnum.hasMore()) {
                    list.add((String) valueEnum.next());
                }
                valueEnum.close();
            }
        }
    }

    /**
     * Populate SieveFilters BlockedSendersList Data into POJO.
     * 
     * @param dataMap dataMap
     * @return list of SieveFilters BlockedSenders
     * @throws Exception Exception
     */
    public static void populateSieveFiltersBlockedSendersList(
            final List<String> list, final Attributes attributes)
            throws Exception {

        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.maildeniedsender.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                while (valueEnum.hasMore()) {
                    list.add((String) valueEnum.next());
                }
                valueEnum.close();
            }
        }
    }

    /**
     * Populate MailAccess Data into POJO.
     * 
     * @param dataMap dataMap
     * @return MailAccess MailAccess
     * @throws Exception Exception
     */
    public static void populateMailAccess(final MailAccess ma,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes.get(LDAPMailboxProperty.mailpopaccess
                    .name());
            if (attr != null) {
                ma.setPopAccessType(AccessType.fromValue(attr.get().toString()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailallowtheseips.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<String> values = new ArrayList<String>();
                while (valueEnum.hasMore()) {
                    values.add((String) valueEnum.next());
                }
                valueEnum.close();
                ma.setAllowedIPs(values);
            }

            attr = attributes.get(LDAPMailboxProperty.mailapopaccess.name());
            if (attr != null) {
                ma.setPopAuthenticationType(IntAccessType.fromValue(attr.get()
                        .toString()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailpopsslaccess.name());
            if (attr != null) {
                ma.setPopSSLAccessType(AccessType.fromValue(attr.get()
                        .toString()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailimapaccess.name());
            if (attr != null) {
                ma.setImapAccessType(AccessType
                        .fromValue(attr.get().toString()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailimapsslaccess.name());
            if (attr != null) {
                ma.setImapSSLAccessType(AccessType.fromValue(attr.get()
                        .toString()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailsmtpaccess.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                ma.setSmtpAccessEnabled(MxosEnums.BooleanType.fromValue(type
                        .name()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailsmtpsslaccess.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                ma.setSmtpSSLAccessEnabled(MxosEnums.BooleanType.fromValue(type
                        .name()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailsmtpauth.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                ma.setSmtpAuthenticationEnabled(MxosEnums.BooleanType
                        .fromValue(type.name()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailwebmailaccess.name());
            if (attr != null) {
                ma.setWebmailAccessType(AccessType.fromValue(attr.get()
                        .toString()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailwebmailsslaccess
                    .name());
            if (attr != null) {
                ma.setWebmailSSLAccessType(AccessType.fromValue(attr.get()
                        .toString()));
            }

            attr = attributes.get(LDAPMailboxProperty.mailmobilemailaccess
                    .name());
            if (attr != null) {
                ma.setMobileMailAccessType(AccessType.fromValue(attr.get()
                        .toString()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.netmailrmactivesyncenabled.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByBoolValue(attr.get()
                        .toString());
                ma.setMobileActiveSyncAllowed(MxosEnums.BooleanType
                        .fromValue(type.getValue()));
            }
        }
    }

    /**
     * Populate MailStore Data into POJO.
     * 
     * @param dataMap dataMap
     * @return MailStore MailStore
     * @throws Exception Exception
     */
    public static void populateMailStore(final MailStore ms,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailquotamaxmsgs.name());
            if (attr != null) {
                ms.setMaxMessages(Integer.parseInt(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.maillargemailboxplatform
                    .name());
            if (attr != null) {
                ms.setLargeMailboxPlatformEnabled(Boolean.getString(attr.get()
                        .toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.mailquotatotkb.name());
            if (attr != null) {
                ms.setMaxStorageSizeKB(Long.parseLong(attr.get().toString()));
            }
           
            attr = attributes.get(LDAPMailboxProperty.mailsoftquotamaxmsgs.name());
            if (attr != null) {
                ms.setMaxMessagesSoftLimit(Integer.parseInt(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.mailsoftquotatotkb .name());
            if (attr != null) {
                ms.setMaxStorageSizeKBSoftLimit(Long.parseLong(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.altmailQuotaMaxMsgs.name());
            if (attr != null) {
                ms.setMobileMaxMessages(Integer.parseInt(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.altmailQuotaTotKB.name());
            if (attr != null) {
                ms.setMobileMaxStorageSizeKB(Long.parseLong(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.altmailsoftquotamaxmsgs.name());
            if (attr != null) {
                ms.setMobileMaxMessagesSoftLimit(Integer.parseInt(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.altmailsoftquotatotkb .name());
            if (attr != null) {
                ms.setMobileMaxStorageSizeKBSoftLimit(Long.parseLong(attr.get().toString()));
            }
            
            attr = attributes
                    .get(LDAPMailboxProperty.mailquotathreshold.name());
            if (attr != null) {
                ms.setQuotaWarningThreshold(Integer.parseInt(attr.get()
                        .toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.mailquotabouncenotify
                    .name());
            if (attr != null) {
                ms.setQuotaBounceNotify(Boolean
                        .getString(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.mailfolderquota.name());
            if (attr != null) {
                ms.setFolderQuota(attr.get().toString());
            }
            ExternalStore es = new ExternalStore();
            attr = attributes
                    .get(LDAPMailboxProperty.netmailInfoStoreAccessAllowed
                            .name());
            if (attr != null) {
                es.setExternalStoreAccessAllowed(Boolean.getString(attr.get()
                        .toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.netmailInfoStoreQuotaMB
                    .name());
            if (attr != null) {
                es.setMaxExternalStoreSizeMB(Integer.parseInt(attr.get()
                        .toString()));
            }
            ms.setExternalStore(es);

            GroupAdminAllocations groupAdminAllocations = new GroupAdminAllocations();
            attr = attributes.get(LDAPMailboxProperty.mailfamilymailbox.name());
            if (attr != null && MxOSConfig.isGroupMailboxEnabled()) {
                String mailFamilyMailbox = (String) attr.get();
                if (mailFamilyMailbox.equals(MailboxType.GROUPADMIN.getValue())) {
                    populateGroupAdminAllocations(groupAdminAllocations,
                            attributes);
                }
            }
            ms.setGroupAdminAllocations(groupAdminAllocations);
        }
    }

    /**
     * Populate InternalInfo Data into POJO.
     * 
     * @param dataMap dataMap
     * @return InternalInfo InternalInfo
     * @throws Exception Exception
     */
    public static void populateInternalInfo(final InternalInfo info,
            final Attributes attributes) throws Exception {
        info.setVersion(MxOSConfig.getMssVersion());
        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.netmailcurrentver.name());
            if (attr != null) {
                info.setWebmailVersion(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.mailselfcare.name());
            if (attr != null) {
                info.setSelfCareAccessEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes.get(LDAPMailboxProperty.mailselfcaressl.name());
            if (attr != null) {
                info.setSelfCareSSLAccessEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes.get(LDAPMailboxProperty.mailmessagestore.name());
            if (attr != null) {
                List<String> values = new ArrayList<String>();
                values.add(attr.get().toString());
                info.setMessageStoreHosts(values);
            }
            // mailSMTPRelayHost
            attr = attributes.get(LDAPMailboxProperty.mailsmtprelayhost.name());
            if (attr != null) {
                info.setSmtpProxyHost(attr.get().toString());
            }
            // mailIMAPProxyHost
            attr = attributes.get(LDAPMailboxProperty.mailimapproxyhost.name());
            if (attr != null) {
                info.setImapProxyHost(attr.get().toString());
            }
            // mailPOPProxyHost
            attr = attributes.get(LDAPMailboxProperty.mailpopproxyhost.name());
            if (attr != null) {
                info.setPopProxyHost(attr.get().toString());
            }
            // imapProxyAuthenticationEnabled
            attr = attributes.get(LDAPMailboxProperty.imapproxyauthenticate
                    .name());
            if (attr != null) {
                BooleanType type = BooleanType.getByBoolValue(attr.get()
                        .toString());
                info.setImapProxyAuthenticationEnabled(MxosEnums.BooleanType
                        .fromValue(type.getValue()));
            }
            // remoteCallTracingEnabled
            attr = attributes.get(LDAPMailboxProperty.remotecalltracingenabled
                    .name());
            if (attr != null) {
                if (attr != null) {
                    if (attr.get().toString().equalsIgnoreCase("TRUE")) {
                        info.setRemoteCallTracingEnabled(MxosEnums.BooleanType.YES);
                    } else {
                        info.setRemoteCallTracingEnabled(MxosEnums.BooleanType.NO);
                    }
                }
            }
            attr = attributes.get(LDAPMailboxProperty.mailautoreplyhost.name());
            if (attr != null) {
                info.setAutoReplyHost(attr.get().toString());
            }
            //mailRealm
            attr = attributes.get(LDAPMailboxProperty.mailRealm.name());
            if (attr != null) {
                info.setRealm(attr.get().toString());
            }
            // interManagerAccessEnabled
            attr = attributes.get(LDAPMailboxProperty.mailintermanager.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                info.setInterManagerAccessEnabled(MxosEnums.BooleanType
                        .fromValue(type.getValue()));
            }
            // interManagerSSLAccessEnabled
            attr = attributes.get(LDAPMailboxProperty.mailintermanagerssl
                    .name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                info.setInterManagerSSLAccessEnabled(MxosEnums.BooleanType
                        .fromValue(type.getValue()));
            }
            // voiceMailAccessEnabled
            attr = attributes
                    .get(LDAPMailboxProperty.netmailenablevoice.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                info.setVoiceMailAccessEnabled(MxosEnums.BooleanType
                        .fromValue(type.getValue()));
            }
            // faxAccessEnabled
            attr = attributes.get(LDAPMailboxProperty.netmailenablefax.name());
            if (attr != null) {
                BooleanType type = BooleanType.getByOrdinal(attr.get()
                        .toString());
                info.setFaxAccessEnabled(MxosEnums.BooleanType.fromValue(type
                        .getValue()));
            }
            // ldapUtilitiesAccessType
            attr = attributes.get(LDAPMailboxProperty.mailldapaccess.name());
            if (attr != null) {
                info.setLdapUtilitiesAccessType(AccessType.fromValue(attr.get()
                        .toString()));
            }
            // addressBookProvider
            attr = attributes.get(LDAPMailboxProperty.addressbookprovider
                    .name());
            if (attr != null) {
                info.setAddressBookProvider(AddressBookProviderType
                        .fromValue(attr.get().toString()));
            }
            populateMessageEventRecords(info.getMessageEventRecords(),
                    attributes);
        }
    }

    /**
     * Populate MssLinkInfo Data into POJO.
     * 
     * @param dataMap dataMap
     * @return MssLinkInfo MssLinkInfo
     * @throws Exception Exception
     */
    public static void populateMSSLinkInfo(final MssLinkInfo info,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes.get(LDAPMailboxProperty.mailboxid
                    .name());
            if (attr != null) {
                info.setMailboxId(Long.parseLong(attr.get().toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.mailmessagestore.name());
            if (attr != null) {
                List<String> values = new ArrayList<String>();
                values.add(attr.get().toString());
                info.setMessageStoreHosts(values);
            }
            attr = attributes.get(LDAPMailboxProperty.mailautoreplyhost.name());
            if (attr != null) {
                info.setAutoReplyHost(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.mailRealm.name());
            if (attr != null) {
                String realm = attr.get().toString();
                realm = realm.replaceAll("/", "");
                info.setRealm(realm);
            }
        }
    }


    /**
     * Populate MssLinkInfo Data into POJO from Mailbox Object
     * 
     * @param mailbox
     * @return MssLinkInfo
     * @throws Exception
     */
    public static MssLinkInfo populateMSSLinkInfoFromMailbox(Mailbox mailbox)
            throws Exception {

        if (null == mailbox) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Empty mailbox object received");
        }

        if (null == mailbox.getBase()) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Empty mailbox.getBase object received");
        }

        if (null == mailbox.getInternalInfo()) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Empty mailbox.getInternalInfo object received");
        }

        MssLinkInfo mssLinkInfo = new MssLinkInfo();

        mssLinkInfo.setMailboxId(mailbox.getBase().getMailboxId());
        mssLinkInfo.setMessageStoreHosts(mailbox.getInternalInfo()
                .getMessageStoreHosts());
        mssLinkInfo.setAutoReplyHost(mailbox.getInternalInfo()
                .getAutoReplyHost());
        mssLinkInfo.setRealm(mailbox.getInternalInfo().getRealm());

        return mssLinkInfo;
    }

    /**
     * Populate MessageEventRecords Data into POJO.
     * 
     * @param MessageEventRecords msgEventRecords
     * @param dataMap dataMap
     * @throws Exception Exception
     */
    public static void populateMessageEventRecords(
            final MessageEventRecords msgEventRecords,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes.get(LDAPMailboxProperty.mersenabled
                    .name());
            if (attr != null) {
                EventRecordingType ert = EventRecordingType
                        .getByShortValue(attr.get().toString());
                msgEventRecords
                        .setEventRecordingEnabled(MxosEnums.EventRecordingType
                                .fromValue(ert.getValue()));
            }
            attr = attributes.get(LDAPMailboxProperty.mersmaxheaderlength
                    .name());
            if (attr != null) {
                msgEventRecords.setMaxHeaderLength(Integer.parseInt(attr.get()
                        .toString()));
            }
            attr = attributes.get(LDAPMailboxProperty.mersfilesizekb.name());
            if (attr != null) {
                msgEventRecords.setMaxFilesSizeKB(Integer.parseInt(attr.get()
                        .toString()));
            }
        }
    }

    /**
     * Populate SmsServices Data into POJO.
     * 
     * @param dataMap dataMap
     * @return SmsServices SmsServices
     * @throws Exception Exception
     */
    public static void populateSmsServices(final SmsServices smsServices,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            // SmsServices
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.smsServicesAllowed.name());
            if (attr != null) {
                smsServices.setSmsServicesAllowed(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes.get(LDAPMailboxProperty.smsServicesMSISDN.name());
            if (attr != null) {
                smsServices.setSmsServicesMsisdn(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.smsServicesMSISDNStatus
                    .name());
            if (attr != null) {
                smsServices
                        .setSmsServicesMsisdnStatus(MxosEnums.SmsServicesMsisdnStatusType
                                .fromOrdinal(Integer.parseInt(attr.get()
                                        .toString())));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smsServicesMSISDNStatusChangeDate
                            .name());
            if (attr != null) {
                String date = populateDate(attr.get().toString());
                smsServices.setLastSmsServicesMsisdnStatusChangeDate(date);
            }

            // SmsOnline
            SmsOnline smsOnline = new SmsOnline();

            attr = attributes.get(LDAPMailboxProperty.smsOnline.name());
            if (attr != null) {
                smsOnline.setSmsOnlineEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smsOnlineInternationalAllowed
                            .name());
            if (attr != null) {
                smsOnline.setInternationalSMSAllowed(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smsOnlineInternationalEnabled
                            .name());
            if (attr != null) {
                smsOnline.setInternationalSMSEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes.get(LDAPMailboxProperty.smsOnlineMaxSMSPerDay
                    .name());
            if (attr != null) {
                smsOnline.setMaxSMSPerDay(Integer.parseInt(attr.get()
                        .toString()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smsOnlineConcatenatedSMSAllowed
                            .name());
            if (attr != null) {
                smsOnline.setConcatenatedSMSAllowed(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smsOnlineConcatenatedSMSEnabled
                            .name());
            if (attr != null) {
                smsOnline.setConcatenatedSMSEnabled(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }
            attr = attributes.get(LDAPMailboxProperty.smsOnlineMaxSMSSegments
                    .name());
            if (attr != null) {
                smsOnline.setMaxConcatenatedSMSSegments(Integer.parseInt(attr
                        .get().toString()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smsCaptchaMessagesThreshold.name());
            if (attr != null) {
                smsOnline.setMaxPerCaptchaSMS(Integer.parseInt(attr.get()
                        .toString()));
            }
            attr = attributes
                    .get(LDAPMailboxProperty.smsCaptchaDurationThreshold.name());
            if (attr != null) {
                smsOnline.setMaxPerCaptchaDurationMins(Integer.parseInt(attr
                        .get().toString()));
            }
            smsServices.setSmsOnline(smsOnline);

            // SmsNotifications
            SmsNotifications smsNotifications = new SmsNotifications();

            attr = attributes.get(LDAPMailboxProperty.smsBasicNotifications
                    .name());
            if (attr != null) {
                smsNotifications
                        .setSmsBasicNotificationsEnabled(MxosEnums.BooleanType
                                .fromOrdinal(Integer.parseInt(attr.get()
                                        .toString())));
            }
            attr = attributes.get(LDAPMailboxProperty.smsAdvancedNotifications
                    .name());
            if (attr != null) {
                smsNotifications
                        .setSmsAdvancedNotificationsEnabled(MxosEnums.BooleanType
                                .fromOrdinal(Integer.parseInt(attr.get()
                                        .toString())));
            }
            smsServices.setSmsNotifications(smsNotifications);
        }
    }

    /**
     * Populate SocialNetworks Data into POJO.
     * 
     * @param Attribute attribute
     * @param SocialNetworks sn
     * @throws Exception Exception
     */
    public static void populateSocialNetworks(final SocialNetworks sn,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            // SmsServices
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.netmailsocialnetworkintegrationallowed
                            .name());
            if (attr != null) {
                int snIntegrationAllowedInt = -1;
                try {
                    snIntegrationAllowedInt = Integer.parseInt(attr.get()
                            .toString());
                } catch (Exception e) {
                    logger.error("Error while populate social networks.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_SNINTEGRATIONALLOWED
                                    .name());
                }
                if (snIntegrationAllowedInt > -1) {
                    sn.setSocialNetworkIntegrationAllowed(MxosEnums.BooleanType
                            .fromOrdinal(snIntegrationAllowedInt));
                }
            }
            attr = attributes.get(LDAPMailboxProperty.netmailSocialNetworkSite
                    .name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                List<SocialNetworkSite> snsList = new ArrayList<SocialNetworkSite>();
                while (valueEnum.hasMore()) {
                    String entry = (String) valueEnum.next();
                    if (entry.contains(":")) {
                        SocialNetworkSite sns = new SocialNetworkSite();
                        String[] values = entry.split(":");
                        sns.setSocialNetworkSite(values[0]);
                        if (values.length < 2) {
                            throw new InvalidRequestException(
                                    MailboxError.MBX_INVALID_NETMAIL_SOCIALNETWORKSITE
                                            .name());
                        }
                        if (values[1].equalsIgnoreCase(MxosEnums.BooleanType.NO
                                .name())
                                || values[1]
                                        .equalsIgnoreCase(MxosEnums.BooleanType.YES
                                                .name())) {
                            sns.setSocialNetworkSiteAccessEnabled(values[1]);
                        } else {
                            throw new InvalidRequestException(
                                    MailboxError.MBX_INVALID_NETMAIL_SOCIALNETWORKSITE
                                            .name());
                        }
                        snsList.add(sns);
                    } else {
                        throw new InvalidRequestException(
                                MailboxError.MBX_INVALID_NETMAIL_SOCIALNETWORKSITE
                                        .name());
                    }
                }
                sn.setSocialNetworkSites(snsList);
            }
        }
    }

    /**
     * Populate External Account Data into POJO.
     * 
     * @param Attribute attribute
     * @param ExternalAccounts ea
     * @throws Exception Exception
     */
    public static void populateExternalAccounts(final ExternalAccounts ea,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            // External Accounts
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.netmailexternalaccounts.name());
            if (attr != null) {
                ea.setExternalMailAccountsAllowed(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }

            attr = attributes.get(LDAPMailboxProperty.netmailpopemailacctselect
                    .name());
            if (attr != null) {
                ea.setPromptForExternalAccountSync(MxosEnums.BooleanType
                        .fromOrdinal(Integer.parseInt(attr.get().toString())));
            }

            attr = attributes.get(LDAPMailboxProperty.netmailpopacctdetails
                    .name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();

                List<MailAccount> mailAcctList = new ArrayList<MailAccount>();

                while (valueEnum.hasMore()) {
                    String entry = (String) valueEnum.next();
                    mailAcctList.add(getMailAccounts(entry));
                }
                ea.setMailAccounts(mailAcctList);
            }
        }
    }

    /**
     * Populate MailAccount Data.
     * 
     * @param String entry
     * @throws Exception Exception
     */
    public static MailAccount getMailAccounts(String entry) throws Exception {

        MailAccount ma = new MailAccount();
        if (entry.contains("|")) {
            String[] value = entry.split("[|]");

            for (int i = 0; i < value.length; i++) {

                switch (i) {
                case 0:
                    if (!"".equals(value[0]))
                        ma.setAccountId(Integer.valueOf(value[0]));
                    break;
                case 1:
                    if (!"".equals(value[1]))
                        ma.setAccountName(value[1]);
                    break;
                case 2:
                    if (!"".equals(value[2]))
                        ma.setAccountType(MxosEnums.AccountType
                                .fromValue(value[2]));
                    break;
                case 3:
                    if (!"".equals(value[3]))
                        ma.setServerAddress(value[3]);
                    break;
                case 4:
                    if (!"".equals(value[4]))
                        ma.setServerPort(Integer.valueOf(value[4]));
                    break;
                case 5:
                    if (!"".equals(value[5]))
                        ma.setTimeoutSecs(Integer.valueOf(value[5]));
                    break;
                case 6:
                    if (!"".equals(value[6]))
                        ma.setEmailAddress(value[6]);
                    break;
                case 7:
                    if (!"".equals(value[7]))
                        ma.setAccountUserName(value[7]);
                    break;
                case 8:
                    if (!"".equals(value[8]))
                        ma.setAccountPassword(value[8]);
                    break;
                case 9:
                    if (!"".equals(value[9]))
                        ma.setDisplayImage(value[9]);
                    break;
                case 10:
                    if (!"".equals(value[10]))
                        ma.setFetchMailsEnabled(MxosEnums.BooleanType
                                .fromValue(value[10]));
                    break;
                case 11:
                    if (!"".equals(value[11]))
                        ma.setAutoFetchMails(MxosEnums.BooleanType
                                .fromValue(value[11]));
                    break;
                case 12:
                    if (!"".equals(value[12]))
                        ma.setFetchedMailsFolderName(value[12]);
                    break;
                case 13:
                    if (!"".equals(value[13]))
                        ma.setLeaveEmailsOnServer(MxosEnums.BooleanType
                                .fromValue(value[13]));
                    break;
                case 14:
                    if (!"".equals(value[14]))
                        ma.setFromAddressInReply(value[14]);
                    break;
                case 15:
                    if (!"".equals(value[15]))
                        ma.setMailSendEnabled(MxosEnums.BooleanType
                                .fromValue(value[15]));
                    break;
                case 16:
                    if (!"".equals(value[16]))
                        ma.setMailSendSMTPAddress(value[16]);
                    break;
                case 17:
                    if (!"".equals(value[17]))
                        ma.setMailSendSMTPPort(Integer.valueOf(value[17]));
                    break;
                case 18:
                    if (!"".equals(value[18]))
                        ma.setSmtpSSLEnabled(MxosEnums.BooleanType
                                .fromValue(value[18]));
                    break;
                case 19:
                    if (!"".equals(value[19]))
                        ma.setUseSmtpLogin(MxosEnums.BooleanType
                                .fromValue(value[19]));
                    break;
                case 20:
                    if (!"".equals(value[20]))
                        ma.setSmtpLogin(value[20]);
                    break;
                case 21:
                    if (!"".equals(value[21]))
                        ma.setSmtpPassword(value[21]);
                    break;
                case 22:
                    if (!"".equals(value[22]))
                        ma.setSendFolderName(value[22]);
                    break;
                case 23:
                    if (!"".equals(value[23]))
                        ma.setDraftsFolderName(value[23]);
                    break;
                case 24:
                    if (!"".equals(value[24]))
                        ma.setTrashFolderName(value[24]);
                    break;
                case 25:
                    if (!"".equals(value[25]))
                        ma.setSpamFolderName(value[25]);
                    break;
                }
            }
            return ma;
        } else {
            return null;
        }
    }
    public static Attributes buildDomainParentAttributes(final String dc) {
        final Attributes attributes = new BasicAttributes();
        final BasicAttribute ocattr = new BasicAttribute("objectclass");
        final String[] objectclasses = MxOSConfig
                .getParentDomainObjectClasses();
        for (final String oc : objectclasses) {
            ocattr.add(oc);
        }
        attributes.put(ocattr);
        attributes.put("dc", dc);
        return attributes;
    }

    public static Attributes buildDomainAttributes(Attributes attributes,
            String dc) {
        BasicAttribute ocattr = new BasicAttribute("objectclass");
        String[] objectclasses = MxOSConfig.getChildDomainObjectClasses();
        for (String oc : objectclasses) {
            ocattr.add(oc);
        }
        attributes.put(ocattr);
        attributes.put("dc", dc);
        attributes.put("o", dc);
        return attributes;
    }

    public static String getLDAPCosId(final String value) {
        final StringBuffer buffer = new StringBuffer();
        buffer.append(LDAPConstants.CN);
        buffer.append(MxOSConstants.EQUALS);
        buffer.append(value);
        if (MxOSConfig.getLdapCosBaseDn().contains(MxOSConstants.COMMA))
            buffer.append(MxOSConfig.getLdapCosBaseDn().substring(
                    MxOSConfig.getLdapCosBaseDn().indexOf(MxOSConstants.COMMA)));
        else
            buffer.append(MxOSConfig.getLdapCosBaseDn());
        return buffer.toString();
    }

    /**
     * Method to covert the date from ldap to required format.
     * 
     * @param str date string
     * @return String converted date
     * @throws MxOSException
     */
    public static String populateDate(String str)
            throws InvalidRequestException {
        Date date = null;
        String convertedDate = null;
        try {
            date = (new SimpleDateFormat(MxOSConstants.LDAP_DATE_FORMAT))
                    .parse(str);
            convertedDate = new SimpleDateFormat(
                    System.getProperty(SystemProperty.userDateFormat.name()))
                    .format(date);
        } catch (ParseException e) {
            logger.warn("Error while populate date.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_DATE_FORMAT.name(), e);
        }
        return convertedDate;
    }

    /**
     * Method to covert the user date to ldap date format.
     * 
     * @param str date string
     * @return String converted date
     * @throws MxOSException
     */
    public static String getLdapDate(String str) throws InvalidRequestException {
        return getLdapDate(str, new Date());
    }

    /**
     * Method to covert the user date to ldap date format.
     * 
     * @param str date string
     * @param default date date
     * @return String converted date
     * @throws MxOSException
     */
    public static String getLdapDate(String str, Date detault)
            throws InvalidRequestException {
        Date date = null;
        String convertedDate = "";
        try {
            if (str != null && !str.equals("")) {
                date = (new SimpleDateFormat(
                        System.getProperty(SystemProperty.userDateFormat.name())))
                        .parse(str);
            } else {
                date = detault;
            }
            if (null != date) {
                convertedDate = new SimpleDateFormat(
                        MxOSConstants.LDAP_DATE_FORMAT).format(date);
            }
        } catch (ParseException e) {
            logger.warn("Error while get ldap date.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_DATE_FORMAT.name(), e);
        }
        return convertedDate;
    }

   /**
     * Method to covert the user date to epoch time format.
     * 
     * @param str date string
     * @return String convertedEpochTime
     * @throws MxOSException
     */
    public static String getEpochTime(String str)
            throws InvalidRequestException {
        Date date = null;
        String convertedEpochTime = "";
        try {
            if (str != null && !str.equals("")) {
                SimpleDateFormat sdf = new SimpleDateFormat(
                        System.getProperty(SystemProperty.userDateFormat.name()));
                sdf.setLenient(false);
                date = sdf.parse(str);
            }
            if (null != date) {
                convertedEpochTime = Long.toString(date.getTime() / 1000);
            }
        } catch (ParseException e) {
            logger.error(
                    "Error while converting user date format to unix epoch time.",
                    e);
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                            .name());
        }
        return convertedEpochTime;
    }

    /**
     * Method to covert the epoch time to user date format.
     * 
     * @param str date string
     * @return String converted date
     * @throws MxOSException
     */
    public static String populateDateFromEpoch(String str)
            throws InvalidRequestException {
        Date date = null;
        String convertedDate = null;
        try {
            date = new Date(Long.parseLong(str) * 1000);
            convertedDate = new SimpleDateFormat(
                    System.getProperty(SystemProperty.userDateFormat.name()))
                    .format(date);
        } catch (Exception e) {
            logger.error(
                    "Error while populateing user date from unix epoch time.",
                    e);
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                            .name());
        }
        return convertedDate;
    }

    /**
     * Returns numDelayedDeliveryMessagesPending in required date format.
     * 
     * @param delayedDeliveryMessagesPendingArray String[]
     * @return String[]
     */
    public static final String[] getFormattedDateArrayForDeliveryMessagesPending(
            final String[] delayedDeliveryMessagesPendingArray)
            throws InvalidRequestException {
        try {
            String[] strArray = null;
            if (delayedDeliveryMessagesPendingArray != null
                    && delayedDeliveryMessagesPendingArray.length > 0) {
                strArray = new String[delayedDeliveryMessagesPendingArray.length];
                int index = 0;
                String timeInLdapFormat = "";
                String messageId = "";
                for (String value : delayedDeliveryMessagesPendingArray) {
                    final String[] timeMessageID = value
                            .split(MxOSConstants.COMMA);
                    if (timeMessageID.length > 1
                            && (timeMessageID[0] != null && !timeMessageID[0]
                                    .equals(""))
                            && (timeMessageID[1] != null && !timeMessageID[1]
                                    .equals(""))) {
                        timeInLdapFormat = LDAPUtils
                                .getEpochTime(timeMessageID[0]);
                        messageId = timeMessageID[1];
                        StringBuffer sb = new StringBuffer();
                        sb.append(timeInLdapFormat);
                        sb.append(MxOSConstants.COMMA);
                        sb.append(messageId);
                        strArray[index] = sb.toString();
                        index++;
                    } else {
                        throw new InvalidRequestException(
                                MailboxError.MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                                        .name());
                    }
                }
            } else {
                strArray = new String[0];
            }
            return strArray;
        } catch (Exception e) {
            logger.error(
                    "Error in formating date from numDelayedDeliveryMessagesPending.",
                    e);
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                            .name());
        }
    }
    
    /**
     * Populate AdminControl Data into POJO.
     * 
     * @param dataMap dataMap
     * @return AdminControl AdminControl
     * @throws Exception Exception
     */
    public static void populateAdminControl(final AdminControl adminControl,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailfamilymailbox.name());
            if (attr != null) {
                String mailFamilyMailbox = (String) attr.get();
                if (mailFamilyMailbox.equals(MailboxType.GROUPADMIN.getValue())
                        || mailFamilyMailbox.equals(MailboxType.GROUPMAILBOX
                                .getValue())) {
                    // adminParentalDefaultDisposition
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminparentaldefaultdisposition
                                    .name());
                    if (attr != null) {
                        adminControl
                                .setAdminDefaultDisposition(MxosEnums.AdminDefaultDispositionAction
                                        .fromValue(attr.get().toString()));
                    }
                    // adminParentalRejectAction
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminparentalrejectaction
                                    .name());
                    if (attr != null) {
                        adminControl
                                .setAdminRejectAction(MxosEnums.AdminRejectActionFlag
                                        .fromValue(attr.get().toString()));
                    }
                    // adminParentalRejectInfo
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminparentalrejectinfo
                                    .name());
                    if (attr != null) {
                        adminControl.setAdminRejectInfo(attr.get().toString());
                    }
                    // adminApprovedSendersList
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminapprovedsenderslist
                                    .name());
                    if (attr != null) {
                        final NamingEnumeration<? extends Object> valueEnum = attr
                                .getAll();
                        List<String> values = new ArrayList<String>();
                        while (valueEnum.hasMore()) {
                            values.add((String) valueEnum.next());
                        }
                        valueEnum.close();
                        adminControl.setAdminApprovedSendersList(values);
                    }
                    // adminBlockedSendersList
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminblockedsenderslist
                                    .name());
                    if (attr != null) {
                        final NamingEnumeration<? extends Object> valueEnum = attr
                                .getAll();
                        List<String> values = new ArrayList<String>();
                        while (valueEnum.hasMore()) {
                            values.add((String) valueEnum.next());
                        }
                        valueEnum.close();
                        adminControl.setAdminBlockedSendersList(values);
                    }
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_NOT_GROUP_ADMIN_ACCOUNT.name());
                }
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_NOT_GROUP_ACCOUNT.name());
            }
        }
    }

    /**
     * Populate adminBlockedSendersList.
     * 
     * @param attributes ldap attributes from which data has to be populated
     * @param list of list of strings to be updated with admin blocked senders
     *         data
     * @throws Exception Exception
     */
    public static void populateAdminBlockedSendersList(final List<String> list,
            final Attributes attributes) throws Exception {

        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPGroupAdminMailboxProperty.adminblockedsenderslist.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                while (valueEnum.hasMore()) {
                    list.add((String) valueEnum.next());
                }
                valueEnum.close();
            }
        }
    }
    
    /**
     * Populate adminApprovedSendersList
     * 
     * @param attributes ldap attributes from which data has to be populated
     * @param list of list of strings to be updated with admin approved senders
     *         data
     * @throws Exception Exception
     */
    public static void populateAdminApprovedSendersList(final List<String> list,
            final Attributes attributes) throws Exception {

        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPGroupAdminMailboxProperty.adminapprovedsenderslist.name());
            if (attr != null) {
                final NamingEnumeration<? extends Object> valueEnum = attr
                        .getAll();
                while (valueEnum.hasMore()) {
                    list.add((String) valueEnum.next());
                }
                valueEnum.close();
            }
        }
    }

    /**
     * Populate GroupAdminAllocations Data into POJO.
     * 
     * @param dataMap dataMap
     * @return GroupAdminAllocations
     * @throws Exception Exception
     */
    public static void populateGroupAdminAllocations(
            final GroupAdminAllocations groupAdminAllocations,
            final Attributes attributes) throws Exception {
        if (attributes != null) {
            Attribute attr = attributes
                    .get(LDAPMailboxProperty.mailfamilymailbox.name());
            if (attr != null) {
                String mailFamilyMailbox = (String) attr.get();
                if (mailFamilyMailbox.equals(MailboxType.GROUPADMIN.getValue())) {
                    // maxAdminStorageSizeKB
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminmaxstoragekb
                                    .name());
                    if (attr != null) {
                        groupAdminAllocations.setMaxAdminStorageSizeKB(Long
                                .parseLong(attr.get().toString()));
                    }
                    // maxAdminUsers
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminmaxusers
                                    .name());
                    if (attr != null) {
                        groupAdminAllocations.setMaxAdminUsers(Long
                                .parseLong(attr.get().toString()));
                    }
                    // adminNumUsers
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminnumusers
                                    .name());
                    if (attr != null) {
                        groupAdminAllocations.setAdminNumUsers(Long
                                .parseLong(attr.get().toString()));
                    }
                    // adminUsedStorageSizeKB
                    attr = attributes
                            .get(LDAPGroupAdminMailboxProperty.adminusedstoragekb
                                    .name());
                    if (attr != null) {
                        groupAdminAllocations.setAdminUsedStorageSizeKB(Long
                                .parseLong(attr.get().toString()));
                    }
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_NOT_GROUP_ADMIN_ACCOUNT.name());
                }
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_NOT_GROUP_ACCOUNT.name());
            }
        }
    }

    public static Map<String, Map<String, List<String>>> getDefaultHOHMap(
            Map<String, String> placeHoldersMap) throws MxOSException {
        logger.info("Loading default-group-HOH.properties");
        Map<String, Map<String, List<String>>> defaultHOHProperties
            = new LinkedHashMap<String, Map<String, List<String>>>();
        // Load properties from ${MXOS_HOME}/config/default-mailbox.properties
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(
                    System.getProperty(MxOSConstants.MXOS_HOME)
                            + "/config/default-group-HOH.properties"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line= line.trim();
                if (line.length() == 0 || line.charAt(0) == '#') {
                    continue;
                }
                int delim = line.indexOf(':');
                String key = line.substring(0, delim);
                String value = line.substring(delim + 2); // gets past ': '
                String[] attribute = key.split("\\.");
                // Replace the placeHolders
                Iterator<String> placeHoldersItr = placeHoldersMap.keySet()
                        .iterator();
                while (placeHoldersItr.hasNext()) {
                    String placeHolder = placeHoldersItr.next();
                    value = value.replace(placeHolder,
                            placeHoldersMap.get(placeHolder));
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Attribute : "
                            + key + ", value : " + value);
                }
                if (attribute.length != 2) {
                    throw new IOException(
                            "Error while loading default-group-HOH.properties"
                                    + ", please check the configuration at line : "
                                    + line);
                } else {
                    DefaultHOHPropertyKey[] enumsArray = DefaultHOHPropertyKey
                            .values();
                    for (DefaultHOHPropertyKey defaultHOHPropertyKey : enumsArray) {
                        if (attribute[0].equals(defaultHOHPropertyKey
                                .getValue())) {
                            Map<String, List<String>> map = defaultHOHProperties
                                    .get(defaultHOHPropertyKey.name());
                            if (map != null) {
                                List<String> list = map.get(attribute[1]);
                                if (list != null) {
                                    list.add(value);
                                    map.put(attribute[1], list);
                                } else {
                                    list = new ArrayList<String>();
                                    list.add(value);
                                    map.put(attribute[1], list);
                                }
                            } else {
                                map = new HashMap<String, List<String>>();
                                List<String> list = new ArrayList<String>();
                                list.add(value);
                                map.put(attribute[1], list);
                            }
                            defaultHOHProperties.put(
                                    defaultHOHPropertyKey.name(), map);
                        }
                    }
                }
            }
            logger.info("Loaded default-group-HOH.properties");
        } catch (FileNotFoundException e) {
            logger.warn("default-group-HOH.properties file not found.");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } catch (IOException e) {
            logger.warn("Error while loading default-group-HOH.properties.");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } catch (Exception e) {
            logger.error("Error while loading default-group-HOH.properties", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        return defaultHOHProperties;
    }
    
    /**
     * Read Ldif file and add or update the entries in LDAP
     * 
     * @param file
     * @throws Exception
     */
    public static Attributes readLdif(String fileName) throws Exception {
        Attributes attrs = new BasicAttributes();
        Attribute objectClassAttr = null;
        Attribute aciruleAttr = null;
        Attribute adminAllowedAdminPolicyDNAttr = null;

        BufferedReader bufferedReader = null;
        try {
            String line;
            bufferedReader = new BufferedReader(new FileReader(
                    System.getProperty(MxOSConstants.MXOS_HOME)
                            + "/config/hohLdifs/" + fileName));
            while ((line = bufferedReader.readLine()) != null) {
                if (line.length() == 0 || line.charAt(0) == '#') {
                    continue;
                }
                int delim = line.indexOf(':');
                String key = line.substring(0, delim);
                String value = line.substring(delim + 2); // gets past ': '
                logger.info("Line: " + line);
                logger.info("Attribute : " + key + " ,value : " + value);

                if (key.equalsIgnoreCase("objectclass")) {
                    if (objectClassAttr != null) {
                        objectClassAttr.add(value);
                    } else {
                        objectClassAttr = new BasicAttribute("objectclass");
                        objectClassAttr.add(value);
                    }
                } else if (key.equalsIgnoreCase("acirule")) {
                    if (aciruleAttr != null) {
                        aciruleAttr.add(value);
                    } else {
                        aciruleAttr = new BasicAttribute("acirule");
                        aciruleAttr.add(value);
                    }
                } else if (key.equalsIgnoreCase("adminAllowedAdminPolicyDN")) {
                    if (adminAllowedAdminPolicyDNAttr != null) {
                        adminAllowedAdminPolicyDNAttr.add(value);
                    } else {
                        adminAllowedAdminPolicyDNAttr = new BasicAttribute(
                                "adminAllowedAdminPolicyDN");
                        adminAllowedAdminPolicyDNAttr.add(value);

                    }
                } else {
                    Attribute basicAttr = new BasicAttribute(key);
                    basicAttr.add(value);
                    attrs.put(basicAttr);
                }
            }
            // Add object class attributes
            if (objectClassAttr != null) {
                attrs.put(objectClassAttr);
            }
            // Add acirule attributes
            if (aciruleAttr != null) {
                attrs.put(aciruleAttr);
            }
            // Add adminAllowedAdminPolicyDN
            if (adminAllowedAdminPolicyDNAttr != null) {
                attrs.put(adminAllowedAdminPolicyDNAttr);
            }

        } catch (IOException e) {
            logger.error(e);
            throw e;
        } finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();
            } catch (IOException ioe) {
                logger.error(ioe);
                throw ioe;
            }
        }
        return attrs;
    }
}
