/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMssLinkInfoService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * MssLinkInfoService exposed to client which is responsible for doing basic
 * Mailbox related activities e.g read MssLinkInfo via backend of MxOS.
 * 
 * @author mxos-dev
 */
public class BackendMssLinkInfoService implements IMssLinkInfoService {
    private static Logger logger = Logger
            .getLogger(BackendMssLinkInfoService.class);

    /**
     * Default Constructor.
     */
    public BackendMssLinkInfoService() {
        logger.info("BackendMssLinkInfoService created...");
    }

    @Override
    public MssLinkInfo read(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MssLinkInfoService, Operation.GET);

            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MssLinkInfoService, Operation.GET, t0,
                    StatStatus.pass);
            return (MssLinkInfo) mxosRequestState.getDbPojoMap().getPropertyAsObject(
                    MxOSPOJOs.mssLinkInfo);
        } catch (final MxOSException e) {
            Stats.stopTimer(ServiceEnum.MssLinkInfoService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }

    }
}
