/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaConnectionPool.java#2 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;

import javax.naming.directory.DirContext;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Implementation of LDAP CRUD connection pool.
 * 
 * @author mxos-dev
 */
public class LdapConnectionPool implements ICRUDPool<IMailboxCRUD> {
    protected static Logger logger = Logger.getLogger(LdapConnectionPool.class);

    protected GenericObjectPool<LdapMailboxCRUD> objPool;
    protected Hashtable<String, String> env;
    protected int maxSize = 10;
    protected int activeHostIndex = 0;
    private LdapCRUDFactory ldapCRUDFactory;
    protected int numHosts = 0;
    protected String[] hosts;
    protected String[] urls;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public LdapConnectionPool() throws MxOSException {
        env = new Hashtable<String, String>();
        env = getConfigParams();
        this.maxSize = MxOSConfig.getLdapPoolMaxSize();
        loadPool(env, maxSize);
        initializeJMXStats(maxSize);
        logger.info("Loaded LdapConnectionPool...");
    }

    /**
     * Method to load Pool.
     */
    protected void loadPool(Hashtable<String, String> env, int maxSize) {
        ldapCRUDFactory = new LdapCRUDFactory();
        ldapCRUDFactory.setEnv(env);
        objPool = new GenericObjectPool<LdapMailboxCRUD>(ldapCRUDFactory,
                maxSize);
        objPool.setMaxIdle(-1);
        objPool.setTestOnBorrow(true);
    }

    @Override
    public void resetPool() throws MxOSException {
        if (null != objPool) {
            objPool.clear();
            logger.info("# Clearing the pools..");
        }
    }


    @Override
    public synchronized IMailboxCRUD borrowObject() throws MxOSException {
        IMailboxCRUD obj = null;
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());

        if (!MxOSConfig.isLdapServerAMaster(env.get(MxOSConfig.PROVIDER_HOST))) {
            loadLdapHosts();
            getLdapUrl();
            ldapCRUDFactory.setEnv(env);
        }
        for (int i = 0; i < numHosts; i++) {
            try {
                logger.info("# Trying Host: " + i + ": "
                        + env.get(MxOSConfig.PROVIDER_HOST) + ", url: "
                        + env.get(DirContext.PROVIDER_URL));
                obj = objPool.borrowObject();
                logger.info("# Borrow object success on Host: " + i + ": "
                        + env.get(DirContext.PROVIDER_URL));
            } catch (Exception e) {
                logger.warn("# Error in borrowing object..");
                getLdapUrl();
                ldapCRUDFactory.setEnv(env);
            }
            if (obj != null) {
                break;
            }
            try {
                Thread.sleep(MxOSConfig.getLdapConnRetryInterval());
            } catch (InterruptedException e) {
                logger.warn("# Thread Interrupted.." + e.getMessage());
                throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                        "Thread Interrupted.." + e.getMessage());
            }
        }
        if (obj == null) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        incrementJMXStats();
        return obj;
    }

    @Override
    public void returnObject(IMailboxCRUD provisionCRUD) throws MxOSException {
        try {
            objPool.returnObject((LdapMailboxCRUD) provisionCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while return object object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.LDAP.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_LDAP.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_LDAP.decrement();
    }

    /**
     * Reads the Configuration Parameters and store in the env.
     * 
     * @return
     */
    protected Hashtable<String, String> getConfigParams() {

        String defaultHost = MxOSConfig.getDefaultHost();
        String defaultApp = MxOSConfig.getDefaultApp();
        String defaultDirServer = MxOSConfig.getDefaultDirectoryServer();

        String userDn = MxOSConfig.getString(defaultHost, defaultDirServer,
                MxOSConfig.Keys.DEFAULT_BIND_DN,
                MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_DN));

        String password = MxOSConfig.getString(defaultHost, defaultDirServer,
                MxOSConfig.Keys.DEFAULT_BIND_PASSWORD,
                MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_PASSWORD));

        loadLdapHosts();
        getLdapUrl();

        env.put(DirContext.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(DirContext.SECURITY_AUTHENTICATION, "simple");
        env.put(DirContext.SECURITY_PRINCIPAL, userDn);
        env.put(DirContext.SECURITY_CREDENTIALS, password);
        System.setProperty("com.sun.jndi.ldap.connect.pool", "false");
        if (logger.isDebugEnabled()) {
            logger.debug("ldap Read/Connect timeout:"+MxOSConfig.getLdapConnectTimeout());
        }
        env.put("com.sun.jndi.ldap.read.timeout",String.valueOf(MxOSConfig.getLdapConnectTimeout()));
        env.put("com.sun.jndi.ldap.connect.timeout",String.valueOf(MxOSConfig.getLdapConnectTimeout()));
        return env;
    }

    /**
     * Reads the LDAP Hosts information from Configuration
     */
    protected void loadLdapHosts() {
        String ldap = "ldap://";
        hosts = MxOSConfig.getLdapServers();
        if (numHosts == 0) {
            numHosts = hosts.length;
        }
        urls = new String[hosts.length];
        for (int i = 0; i < hosts.length; i++) {
            if (!hosts[i].startsWith(ldap)) {
                urls[i] = ldap + hosts[i];
            }
        }
    }

    /**
     * Gets the LDAP URL of the active Host
     */
    protected void getLdapUrl() {
        if(logger.isDebugEnabled()) {
            logger.debug("LdapConnectionPool:getLdapUrl");
            logger.debug("activeHostIndex value is : " + activeHostIndex);
            logger.debug("Ldap url is : " + urls[activeHostIndex]);
        }
        env.put(DirContext.PROVIDER_URL, urls[activeHostIndex]);
        env.put(MxOSConfig.PROVIDER_HOST,
                hosts[activeHostIndex].split(MxOSConstants.COLON)[0]);
        activeHostIndex++;
        if (activeHostIndex >= hosts.length) {
            activeHostIndex = 0 ;
        }       
    }
}
