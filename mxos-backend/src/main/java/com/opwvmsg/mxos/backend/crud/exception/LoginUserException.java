package com.opwvmsg.mxos.backend.crud.exception;

public class LoginUserException extends ComponentException {

    private static final long serialVersionUID = 8634793376715923619L;

    public LoginUserException(Exception e) {
        super(e);
        if (e instanceof ComponentException) {
            ComponentException ce = (ComponentException) e;
           this.setCategory(ce.getCategory());
           this.setCode(ce.getCode());
           this.setErrorId(ce.getErrorId());
        }
    }

    public LoginUserException(String message) {
        super(message);
    }

    public LoginUserException(String message, Exception e) {
        super(message, e);
    }

    public LoginUserException(String message, String category, String code,
            String errorId) {
        super(message);
    }

    public LoginUserException(String message, String category, String code,
            String errorId, Exception e) {
        super(message, e);
    }
}
