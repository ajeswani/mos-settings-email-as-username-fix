/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.we.utils;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.ox.addressbook.OXAddressBookUtil;
import com.opwvmsg.mxos.backend.crud.we.WEAddressBookUtil;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

public class Term implements Visitor {

    public enum Operator implements EnumConverter {
        MATCH("=="), NOT_MATCH("!="), CONTAIN("~"), NOT_CONTAIN("!~"), LESS_THAN(
                "<"), NOT_LESS_THAN(">="), GREATER_THAN(">"), NOT_GREATER_THAN(
                "<="), STARTS_WITH("^="), NOT_STARTS_WITH("!^="), ENDS_WITH(
                "$="), NOT_ENDS_WITH("!$=");

        private final String value;

        Operator(String value) {
            this.value = value;
        }

        @Override
        public String convert() {
            return value;
        }
    }

    private String operand;
    private Operator operator;
    private String value;

    public String getOperand() {
        return operand;
    }

    private static Logger logger = Logger.getLogger(Term.class);

    @Override
    public void parse(String input) throws AddressBookException {
        String[] operands = null;
        if (input.indexOf(Term.Operator.MATCH.convert()) > 0) {
            operands = StringUtils.split(input, Term.Operator.MATCH.convert());
            operator = Term.Operator.MATCH;
        } else if (input.indexOf(Term.Operator.NOT_MATCH.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.NOT_MATCH.convert());
            operator = Term.Operator.NOT_MATCH;
        } else if (input.indexOf(Term.Operator.NOT_CONTAIN.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.NOT_CONTAIN.convert());
            operator = Term.Operator.NOT_CONTAIN;
        } else if (input.indexOf(Term.Operator.CONTAIN.convert()) > 0) {
            operands = StringUtils
                    .split(input, Term.Operator.CONTAIN.convert());
            operator = Term.Operator.CONTAIN;
        } else if (input.indexOf(Term.Operator.NOT_LESS_THAN.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.NOT_LESS_THAN.convert());
            operator = Term.Operator.NOT_LESS_THAN;
        } else if (input.indexOf(Term.Operator.LESS_THAN.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.LESS_THAN.convert());
            operator = Term.Operator.LESS_THAN;
        } else if (input.indexOf(Term.Operator.NOT_GREATER_THAN.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.NOT_GREATER_THAN.convert());
            operator = Term.Operator.NOT_GREATER_THAN;
        } else if (input.indexOf(Term.Operator.GREATER_THAN.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.GREATER_THAN.convert());
            operator = Term.Operator.GREATER_THAN;
        } else if (input.indexOf(Term.Operator.STARTS_WITH.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.STARTS_WITH.convert());
            operator = Term.Operator.STARTS_WITH;
        } else if (input.indexOf(Term.Operator.NOT_STARTS_WITH.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.NOT_STARTS_WITH.convert());
            operator = Term.Operator.NOT_STARTS_WITH;
        } else if (input.indexOf(Term.Operator.ENDS_WITH.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.ENDS_WITH.convert());
            operator = Term.Operator.ENDS_WITH;
        } else if (input.indexOf(Term.Operator.NOT_ENDS_WITH.convert()) > 0) {
            operands = StringUtils.split(input,
                    Term.Operator.NOT_ENDS_WITH.convert());
            operator = Term.Operator.NOT_ENDS_WITH;
        }
        if (operands == null) {
            logger.error("Exception occurred while parsing input : " + input);
            throw new AddressBookException(
                    AddressBookError.ABS_INVALID_SEARCH_TERM.name(),
                    ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                    AddressBookError.ABS_INVALID_SEARCH_TERM.name(), "");
        }
        for (int i = 0; i < operands.length; i++) {
            operands[i] = StringUtils.trim(operands[i]);
        }
        try {
            if (MxOSConfig.getAddressBookBackend().equals(
                    AddressBookDBTypes.ox.name())) {
                operand = OXAddressBookUtil.JSONToOX_AddressBook.get(
                        AddressBookProperty.valueOf(operands[0])).toString();
            } else {
                operand = WEAddressBookUtil.JSONToWE_AddressBook.get(
                        AddressBookProperty.valueOf(operands[0])).toString();
            }
        } catch (IllegalArgumentException ile) {
            logger.error("Exception occurred invalid search term : "
                    + operands[0]);
            throw new AddressBookException(
                    AddressBookError.ABS_INVALID_SEARCH_TERM.name(),
                    ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                    AddressBookError.ABS_INVALID_SEARCH_TERM.name(), "");
        }
        value = operands[1];
    }

    @Override
    public boolean visit(Map<String, String> valueMap) {
        switch (operator) {
        case MATCH:
            if (valueMap.containsKey(operand)
                    && valueMap.get(operand).equals(value))
                return true;
            break;
        case NOT_MATCH:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .equals(value)))
                return true;
            break;
        case CONTAIN:
            if (valueMap.containsKey(operand)
                    && valueMap.get(operand).contains(value))
                return true;
            break;
        case NOT_CONTAIN:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .contains(value)))
                return true;
        case LESS_THAN:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .compareTo(value) < 0))
                return true;
        case NOT_LESS_THAN:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .compareTo(value) < 0))
                return true;
        case GREATER_THAN:
            if ((valueMap.containsKey(operand) && valueMap.get(operand)
                    .compareTo(value) > 0))
                return true;
        case NOT_GREATER_THAN:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .compareTo(value) > 0))
                return true;
        case STARTS_WITH:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .startsWith(value)))
                return true;
        case NOT_STARTS_WITH:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .startsWith(value)))
                return true;
        case ENDS_WITH:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .endsWith(value)))
                return true;
        case NOT_ENDS_WITH:
            if (!(valueMap.containsKey(operand) && valueMap.get(operand)
                    .endsWith(value)))
                return true;
        }
        return false;
    }

    public void setOperand(String operand) {
        this.operand = operand;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
