/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.we;

import org.apache.commons.pool.BasePoolableObjectFactory;

/**
 * Connection pool factory to talk over PAF. This is required by Apache object
 * pool.
 * 
 * @author mxos-dev
 */
class WEAddressBookFactory extends BasePoolableObjectFactory<WEAddressBookCRUD> {

    /**
     * Constructor.
     * 
     */
    public WEAddressBookFactory() {
    }

    @Override
    public void destroyObject(WEAddressBookCRUD restCRUD) throws Exception {
    }

    @Override
    public WEAddressBookCRUD makeObject() throws Exception {
        return new WEAddressBookCRUD();
    }
}
