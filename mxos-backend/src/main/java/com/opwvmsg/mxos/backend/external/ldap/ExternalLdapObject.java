/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.external.ldap;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.TimeLimitExceededException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;

/**
 * This class is for retrieving ldap attributes from external ldap server
 * 
 * @author Amit Baxi
 */
public class ExternalLdapObject {

    private static Logger logger = Logger.getLogger(ExternalLdapObject.class);
    private final DirContext ldapContext;
    private boolean status = false;

    /**
     * Default Constructor.
     * 
     * @param env env
     * @throws Exception Exception in case of any error on connection.
     */
    public ExternalLdapObject(final Hashtable<String, String> env)
            throws Exception {
        try {
            // Create one initial context (Get connection from pool)
            ldapContext = new InitialDirContext(env);
            if (ldapContext == null) {
                this.status = false;
                logger.error("Problem with External LDAP Connection.");
                throw new Exception("Problem with External LDAP Connection.");
            }
            this.status = true;
        } catch (final NamingException e) {
            ConnectionErrorStats.LDAP.increment();
            logger.error("Error while initializing External LDAP Pool.", e);
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * Close transport connection resource.
     */
    public boolean isConnected() {
        return this.status;
    }

    /**
     * Method to close the ldap connection.
     * 
     * @throws MxOSException
     */
    public void close() throws MxOSException {
        try {
            if (ldapContext != null) {
                ldapContext.close();
                this.status = false;
            }
        } catch (NamingException e) {
            logger.error("Error while closing External LDAP object.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    private static String getFormatFilter(final String filterValue) {
        String searchFilter = ExternalLDAPUtils.getInstance().getSearchFilter();
        return searchFilter.replaceAll("\\{filterValue\\}", filterValue);
    }

    public Attributes retrieveLdapAttributes(String filterValue)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing LDAP retrieve for filterValue - "
                    + filterValue);
        }
        String searchFilter = getFormatFilter(filterValue);
        ExternalLDAPUtils ldapUtils = ExternalLDAPUtils.getInstance();
        Attributes attrs = null;
        try {
            final int maxTimeOut = ldapUtils.getMaxSearchTimeout();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                    ldapUtils.getAttributeList(), true, false);
            NamingEnumeration<SearchResult> results = null;
            results = executeLDAPSearch(ldapUtils.getBaseDn(), searchFilter, sc);
            if (results != null && results.hasMore()) {
                final SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    attrs = searchResult.getAttributes();
                }
            }
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP retrieveLdapAttributes().", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP retrieveLdapAttributes().", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while LDAP retrieveLdapAttributes().", e);
            throw new ApplicationException(ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (Exception e) {
            logger.error("Error in LDAP retrieveLdapAttributes().", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
        return attrs;
    }

    private NamingEnumeration<SearchResult> executeLDAPSearch(
            final String ldapBaseDn, final String formattedLdapFilter,
            final SearchControls sc) throws MxOSException, NamingException {
        try {
            return ldapContext.search(ldapBaseDn, formattedLdapFilter, sc);
        } catch (final AuthenticationException e) {
            logger.error("Error in External LDAP search.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Time out error in External LDAP search.", e);
            ConnectionErrorStats.LDAPSEARCH.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in External LDAP search.", e);
            ConnectionErrorStats.LDAPSEARCH.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while External LDAP search", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error in External LDAP search.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }
}
