/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import javax.naming.directory.Attributes;

/**
 * Holds the Cos Attributes and its fetch Counter.
 * 
 * @author mxos-dev
 */
public class CosAttributes {

    // Holds the attempt number
    int noOfFetches;

    // Holds the attributes of the cache
    Attributes attributes;

    /**
     * Default constructor.
     */
    public CosAttributes() {
        attributes = null;
        noOfFetches = 0;
    }

    /**
     * Default constructor.
     */
    public CosAttributes(Attributes attributes) {
        noOfFetches = 0;
        this.attributes = attributes;
    }

    /**
     * @return the noOfFetches
     */
    public int getNoOfFetches() {
        return noOfFetches;
    }

    /**
     * @return the attributes
     */
    public Attributes getAttributes() {
        return attributes;
    }

    /**
     * @return the attributes
     */
    public int incrementAndGetFetchCount() {
        noOfFetches++;
        return noOfFetches;
    }
}