/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/data/BooleanEnum.java#1 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * @author mxos-dev
 * 
 */
public enum EventRecordingType implements DataMap.Property {
    ALWAYS,
    TRUE,
    FALSE,
    NEVER;

    private String shortValue;
    private EventRecordingType() {
        shortValue = name().substring(0, 1);
    }

    public static EventRecordingType getByValue(String value) throws InvalidRequestException {
        EventRecordingType returnVal = null;
        for (EventRecordingType type : values()) {
            if (type.name().equalsIgnoreCase(value)) {
                returnVal = type;
                break;
            }
        }
        if (null == returnVal) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return returnVal;
    }

    public static EventRecordingType getByShortValue(String value) throws InvalidRequestException {
        EventRecordingType returnVal = null;
        for (EventRecordingType type : values()) {
            if (type.shortValue.equalsIgnoreCase(value)) {
                returnVal = type;
                break;
            }
        }
        if (null == returnVal) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return returnVal;
    }

    public String getValue() {
        return name().toLowerCase();
    }

    public String getShortValue() {
        return shortValue.toLowerCase();
    }

}
