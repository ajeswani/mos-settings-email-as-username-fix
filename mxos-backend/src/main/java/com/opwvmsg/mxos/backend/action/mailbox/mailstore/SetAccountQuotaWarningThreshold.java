/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailstore;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set dataMap quota.
 *
 * @author mxos-dev
 */
public class SetAccountQuotaWarningThreshold implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetAccountQuotaWarningThreshold.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetAccountQuotaWarningThreshold action start."));
        }
        try {
            final String newQuotaWarningThreshold =
                requestState.getInputParams()
                            .get(MailboxProperty.quotaWarningThreshold.name())
                            .get(0);

            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.quotaWarningThreshold,
                            newQuotaWarningThreshold);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set account quota warning threshold.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_QUOTA_WARNING_THRESHOLD.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetAccountQuotaWarningThreshold action end."));
        }
    }
}
