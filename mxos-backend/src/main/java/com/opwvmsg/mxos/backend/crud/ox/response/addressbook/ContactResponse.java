/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.ox.response.addressbook;

import java.util.List;

import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.sun.jersey.api.client.ClientResponse;

public class ContactResponse extends Response {

    /*
     * Get all ContactBase.
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public List<ContactBase> getAllContactBase(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<ContactBase> contactBase = JsonToAddressBookMapper
                .mapToAllContactBase(root);

        return contactBase;
    }

    /*
     * Get all Contacts.
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public List<Contact> getAllContacts(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<Contact> contacts = JsonToAddressBookMapper.mapToAllContacts(root);

        return contacts;
    }

    /*
     * Get all GroupBase.
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public List<GroupBase> getAllGroupBase(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<GroupBase> groupBase = JsonToAddressBookMapper
                .mapToAllGroupBase(root);

        return groupBase;
    }

    /*
     * Get all GroupBase.
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public List<ExternalMember> getAllGroupExternalMembers(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<ExternalMember> memberList = JsonToAddressBookMapper
                .mapToAllExternalGroupMembers(root);

        return memberList;
    }

    /*
     * Get all GroupBase.
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public List<Member> getAllGroupMembers(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<Member> memberList = JsonToAddressBookMapper
                .mapToAllGroupMembers(root);

        return memberList;
    }

    /**
     * Get a Contact.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public Contact getContact(ClientResponse resp) throws AddressBookException {
        JsonNode root = getTree(resp);

        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        Contact contact = new Contact();
        contact.setName(getContactName(resp));
        return contact;
    }

    /**
     * Method to get Contact Image.
     * 
     * @param resp ClientResponse
     * 
     * @return Image
     * @throws AddressBookException
     */
    public byte[] getContactActualImage(ClientResponse resp)
            throws AddressBookException {
        byte[] imageBytes = resp.getEntity(byte[].class);
        if (imageBytes != null && imageBytes.length > 0) {
            return imageBytes;
        } else {
            throw new AddressBookException(
                    AddressBookError.ABS_CONTACTS_IMAGE_NOT_FOUND.name(),
                    ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                    ExceptionUtils.OX_CONTACT_IMAGE_NOT_FOUND_ERROR_CODE, "");
        }
    }

    /**
     * Get a ContactBase.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public ContactBase getContactBase(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        ContactBase contactBase = JsonToAddressBookMapper
                .mapToContactBase(root);

        return contactBase;
    }

    /**
     * Get a ContactId.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public ContactBase getContactId(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        ContactBase contactBase = JsonToAddressBookMapper
                .mapToContactBase(root);

        return contactBase;
    }

    /**
     * Method to get Contact Image.
     * 
     * @param resp ClientResponse
     * 
     * @return Image
     * @throws AddressBookException
     */
    public Image getContactImage(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        return JsonToAddressBookMapper.mapToContactImage(root);
    }

    /**
     * Get a ContactName.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public Name getContactName(ClientResponse resp) throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        return JsonToAddressBookMapper.mapToContactName(root);
    }

    /**
     * Get a Contact PersonalInfo.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public PersonalInfo getContactsPersonalInfo(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        return JsonToAddressBookMapper.mapToContactPersonalInfo(root);
    }

    /**
     * Get a Contact PersonalInfo Address.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public Address getContactsPersonalInfoAddress(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        return JsonToAddressBookMapper.mapToContactPersonalInfoAddress(root);
    }

    /**
     * Get a Contact PersonalInfo Communication.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public Communication getContactsPersonalInfoCommunication(
            ClientResponse resp) throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        return JsonToAddressBookMapper
                .mapToContactPersonalInfoCommunication(root);
    }

    /**
     * Method to get Contact PersonalInfo Events.
     * 
     * @param resp ClientResponse
     * 
     * @return Image
     * @throws AddressBookException
     */
    public List<Event> getContactsPersonalInfoEvents(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        return JsonToAddressBookMapper.mapToContactsPersonalInfoEvents(root);
    }

    /**
     * Method to get ContactsWorkInfo.
     * 
     * @param resp ClientResponse
     * 
     * @return WorkInfo
     * @throws AddressBookException
     */
    public WorkInfo getContactsWorkInfo(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        return JsonToAddressBookMapper.mapToContactWorkInfo(root);
    }

    /**
     * Method to get ContactsWorkInfoAddress.
     * 
     * @param resp ClientResponse
     * 
     * @return Address
     * @throws AddressBookException
     */
    public Address getContactsWorkInfoAddress(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        return JsonToAddressBookMapper.mapToContactWorkInfoAddress(root);
    }

    /**
     * Method to get ContactsWorkInfoCommunication.
     * 
     * @param resp ClientResponse
     * 
     * @return Communication
     * @throws AddressBookException
     */
    public Communication getContactsWorkInfoCommunication(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        return JsonToAddressBookMapper.mapToContactWorkInfoCommunication(root);
    }

    /**
     * Get Folder Id.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public Integer getFolderId(ClientResponse resp) throws AddressBookException {
        JsonNode root = getTree(resp);

        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        return root.asInt();
    }

    /**
     * Get a GroupBase.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public GroupBase getGroupBase(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        GroupBase groupBase = JsonToAddressBookMapper.mapToGroupBase(root);

        return groupBase;
    }

    /**
     * Get a GroupBase.
     * 
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public GroupBase getGroupsBase(ClientResponse resp)
            throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        GroupBase groupBase = JsonToAddressBookMapper.mapToGroupBase(root);

        return groupBase;
    }

    /*
     * Get all Contacts.
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public void getMultipleContactId(ClientResponse resp,
            List<String> contactIdList) throws AddressBookException {
        JsonNode root = getTree(resp);
        if (root == null || root.getElements() == null) {
            return;
        }

        JsonToAddressBookMapper.mapToMultipleContactsAndGroups(root,
                contactIdList);

    }

    /*
     * Get all Contacts.
     * @param resp resp.
     * @throws AddressBookException in case any error.
     */
    public void getMultipleGroupId(ClientResponse resp, List<String> groupIdList)
            throws AddressBookException {
        getMultipleContactId(resp, groupIdList);
    }
}
