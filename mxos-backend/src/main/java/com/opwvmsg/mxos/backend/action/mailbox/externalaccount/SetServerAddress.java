/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.externalaccount;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set accountName attribute of MailAccounts.
 * 
 * @author mxos-dev
 * 
 */

public class SetServerAddress implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetServerAddress.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetServerAddress action start."));
        }

        if (requestState.getInputParams().get(
                MailboxProperty.serverAddress.name()) != null) {
            String serverAddress = requestState.getInputParams()
                    .get(MailboxProperty.serverAddress.name()).get(0);

            MailAccount ma = (MailAccount) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailAccount);
            if (null != serverAddress) {
                ma.setServerAddress(serverAddress.toLowerCase());
            } else
                ma.setServerAddress(null);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailAccount, ma);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetServerAddress action end."));
        }
    }
}
