/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailaccess;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.misc.MxOSSystemConfigUtil;

/**
 * Action class to add allowedIP to MailAccess object.
 *
 * @author mxos-dev
 */
public class AddMailAccessAllowedIPs implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(AddMailAccessAllowedIPs.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddMailAccessAllowedIPs action start."));
        }
        final List<String> newAllowedIPs = requestState.getInputParams()
                .get(MailboxProperty.allowedIP.name());
        final List<String> allowedIPList;
        final MailAccess mailAccess;

        try {
            if (ActionUtils.isCreateMailboxOperation(requestState
                    .getOperationName())) {
                allowedIPList = new ArrayList<String>();
            } else {
                mailAccess = (MailAccess) requestState.getDbPojoMap()
                        .getPropertyAsObject(MxOSPOJOs.mailAccess);
                if(null != mailAccess.getAllowedIPs()) {
                    allowedIPList = mailAccess.getAllowedIPs();
                } else {
                    allowedIPList = new ArrayList<String>();
                }
            }
            if ((allowedIPList.size() + newAllowedIPs.size()) > MxOSSystemConfigUtil
                    .getInstance().getMaxAllowedIps()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_MAILACCESS_ALLOWED_IPS_REACHED_MAX_LIMIT
                                .name());
            }
            for (String newAllowedIP : newAllowedIPs) {
                if (!allowedIPList.contains(newAllowedIP.toLowerCase())) {
                    allowedIPList.add(newAllowedIP.toLowerCase());
                }
            }
            String[] allowedIPArray = allowedIPList
                    .toArray(new String[allowedIPList.size()]);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.allowedIP,
                            allowedIPArray);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while add mail access allowed IPs.", e);
            throw new ApplicationException(
                    MailboxError.MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_CREATE
                            .name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddMailAccessAllowedIPs action end."));
        }
    }
}
