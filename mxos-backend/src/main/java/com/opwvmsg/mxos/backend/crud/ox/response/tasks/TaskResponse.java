/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import java.util.List;

import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;
import com.sun.jersey.api.client.ClientResponse;

public class TaskResponse extends Response {

    /**
     * Get a TaskBase.
     * 
     * @param resp resp.
     * @throws TasksException in case any error.
     */
    public TaskBase getTaskBase(ClientResponse resp) throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        TaskBase taskBase = JsonToTasksMapper.mapToTaskBase(root);

        return taskBase;
    }
    


    /*
     * Get all Tasks.
     * @param resp resp.
     * @throws TasksException in case any error.
     */
    public List<String> getAllTaskIds(ClientResponse resp)
            throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<String> tasks = JsonToTasksMapper.mapToAllTaskIds(root);

        return tasks;
    }
    


    /*
     * Get all Tasks.
     * @param resp resp.
     * @throws TasksException in case any error.
     */
    public List<Task> getAllTasks(ClientResponse resp)
            throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<Task> tasks = JsonToTasksMapper.mapToAllTasks(root);

        return tasks;
    }
}
