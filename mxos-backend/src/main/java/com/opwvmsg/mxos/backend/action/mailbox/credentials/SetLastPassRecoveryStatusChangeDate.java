/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update password.
 * 
 * @author mxos-dev
 */
public class SetLastPassRecoveryStatusChangeDate implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetLastPassRecoveryStatusChangeDate.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetLastPassRecoveryStatusChangeDate action start.");
        }

        String strInputDate = requestState
                .getInputParams()
                .get(MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate
                        .name()).get(0);
        Date inputDate;
        try {
            inputDate = new SimpleDateFormat(
                    System.getProperty(SystemProperty.userDateFormat.name()))
                    .parse(strInputDate);
        } catch (Exception e) {
            logger.error("Error while set last password recovery status changed date.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASS_REC_MSISDN_STATUS_CHANGE_DATE
                            .name(),
                    e);
        }

        if (inputDate != null) {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(
                            requestState,
                            MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate,
                            inputDate);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetLastPassRecoveryStatusChangeDate action end."));
        }
    }

}
