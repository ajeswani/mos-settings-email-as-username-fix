/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.gp;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class SetPreferredUserExperience implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetPreferredUserExperience.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetPreferredUserExperience action start."));
        }
        final String userExp = requestState.getInputParams()
                .get(MailboxProperty.preferredUserExperience.name()).get(0);
        if (userExp != null) {
            if (userExp.equalsIgnoreCase(MxosEnums.PreferredUserExperience.BASIC.name())
                    || userExp.equalsIgnoreCase(MxosEnums.PreferredUserExperience.RICH.name())
                    || userExp.equalsIgnoreCase(MxosEnums.PreferredUserExperience.MOBILE.name())
                    || userExp.equalsIgnoreCase(MxosEnums.PreferredUserExperience.TABLET.name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.preferredUserExperience,
                                userExp);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PREFERRED_USER_EXPERIENCE
                                .name());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetPreferredUserExperience action end."));
        }
    }
}
