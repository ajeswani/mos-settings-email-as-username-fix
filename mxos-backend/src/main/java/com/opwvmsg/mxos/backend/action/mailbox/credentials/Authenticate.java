/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.ldap.PasswordType;
import com.opwvmsg.mxos.backend.crud.ldap.PasswordUtil;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.intermail.mail.AccessInfo;
import com.opwvmsg.utils.paf.util.password.Password;

/**
 * Action class to authenticate the user.
 * 
 * @author mxos-dev
 */
public class Authenticate implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(Authenticate.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Authenticate action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        Credentials credentials = null;
        long failedLoginAttempts = 0;
        int maxFailedLoginAttempts = 0;
        long lastFailedLoginAttemptTime = 0;
        String email = null;
        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            credentials = mailboxCRUD
                    .searchCredentialsForEmailAndUsername(requestState); 
            if (requestState.getInputParams().get(MailboxProperty.email.name()) != null)
            email = requestState.getInputParams().get(MailboxProperty.email.name()).get(0);            
            mailboxCRUD.commit();
            AccessInfo updateAccessInfo = new AccessInfo();
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            if (updateMSSAfterAuthenticationEnabled()) {
                try {
                    metaCRUD.readMailBoxAccessInfo(requestState);
                } catch (final ApplicationException e) {
                    logger.error("Error while reading mailbox access info for "
                            + email);
                    if (!e.getCode().equalsIgnoreCase(
                            MailboxError.MBX_NOT_FOUND.name())) {
                        throw e;
                    }
                    logger.warn("Mailbox NOT found in MSS for " + email);
                }
            }
            AccessInfo accessInfo = (AccessInfo) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.accessInfo);
            if (null != accessInfo) {
                failedLoginAttempts = accessInfo.getNumFailedLoginAttempts();
                lastFailedLoginAttemptTime = accessInfo.getLastFailedLoginTime();
            }
            if (null != credentials.getMaxFailedLoginAttempts()) {
                maxFailedLoginAttempts = (int)credentials.getMaxFailedLoginAttempts();
            }

            if (isBadPasswordWindowEnabled()
                    && !checkBadPasswordWindow(lastFailedLoginAttemptTime)) {
                failedLoginAttempts = 0;
            } else {
                if (isBadPasswordDelayEnabled()) {
                    checkBadPasswordDelay(failedLoginAttempts,
                            maxFailedLoginAttempts, lastFailedLoginAttemptTime);
                }
            }
            if (isCheckLockedAccountEnabled()) {
                authLockedAccount(requestState, failedLoginAttempts,
                        maxFailedLoginAttempts, metaCRUD, updateAccessInfo);
            }
            authenticateUser(requestState, credentials, failedLoginAttempts,
                    metaCRUD, updateAccessInfo);
            metaCRUD.commit();

        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while getting credentails.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning CRUD pool:"
                                    + e.getMessage());
                }
            }
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Meta CRUD pool:" + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Authenticate action end."));
        }
    }

    /**
     * check if badPasswordWindow is still applicable.
     * 
     * @param long lastFailedLoginAttemptTime
     * @return boolean
     */
    private boolean checkBadPasswordWindow(long lastFailedLoginAttemptTime)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "checkBadPasswordWindow method start."));
        }
        int badPasswordWindow = MxOSConfig.getBadPasswordWindow();
        boolean isbpwApplicable = true;
        long currentTime = System.currentTimeMillis();
        // checking if badPasswordWindow is still applicable.
        if ((currentTime - lastFailedLoginAttemptTime) > badPasswordWindow
                * MxOSConstants.MIN_TO_MILLISEC)
            isbpwApplicable = false;

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("checkBadPasswordWindow method end."));
        }
        return isbpwApplicable;

    }

    /**
     * check if badPasswordDelay is applicable; if yes then delay
     * mOS thread appropriately.
     * 
     * @param Integer failedLoginAttempts
     * @param Integer maxFailedLoginAttempts
     * @param long lastFailedLoginAttemptTime
     */
    private void checkBadPasswordDelay(long failedLoginAttempts,
            int maxFailedLoginAttempts, long lastFailedLoginAttemptTime)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("checkBadPasswordDelay method start."));
        }
        int badPasswordDelay = MxOSConfig.getBadPasswordDelay();
        long currentTime = System.currentTimeMillis();
        //check if re-authenticate attempt has been delayed appropriately; if not then delay mOS thread appropriately 
        if ((failedLoginAttempts != 0)
                && (currentTime - lastFailedLoginAttemptTime) < (badPasswordDelay * failedLoginAttempts)
                        * MxOSConstants.SEC_TO_MILLISEC) {
            try {
                Thread.sleep(badPasswordDelay * failedLoginAttempts
                        * MxOSConstants.SEC_TO_MILLISEC);
            } catch (InterruptedException e) {
                logger.error("Error while implementing the bad password delay",
                        e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("checkBadPasswordDelay method end."));
        }
    }

    /**
     * check if account is locked and authenticate if applicable. 
     * 
     * @param MxOSRequestState requestState 
     * @param Integer failedLoginAttempts
     * @param Integer maxFailedLoginAttempts
     * @param IMetaCRUD metaCRUD
     * @param AccessInfo updateAccessInfo
     */
    private void authLockedAccount(final MxOSRequestState requestState,
            long failedLoginAttempts, int maxFailedLoginAttempts,
            IMetaCRUD metaCRUD, AccessInfo updateAccessInfo) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authLockedAccount method start."));
        }
        // check if account is locked and authenticate if isAuthenticateLockedAccountEnabled is true.
        if (maxFailedLoginAttempts != 0) {
            if (failedLoginAttempts >= maxFailedLoginAttempts
                    && !isAuthenticateLockedAccountEnabled()) {
                final long lastLoginTime = System.currentTimeMillis();
                if (updateMSSAfterAuthenticationEnabled()) {
                updateAccessInfo.setLastLoginTime(lastLoginTime);
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.accessInfo, updateAccessInfo);
                metaCRUD.updateMailBoxAccessInfo(requestState);
                }
                throw new AuthorizationException(
                        MailboxError.MBX_ACCOUNT_LOCKED.name());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authLockedAccount method end."));
        }
    }

    /**
     * check if badPasswordDelay is applicable; if yes then delay
     * mOS thread appropriately.
     * 
     * @param MxOSRequestState requestState 
     * @param Integer failedLoginAttempts
     * @param Credentials credentials
     * @param IMetaCRUD metaCRUD
     * @param AccessInfo updateAccessInfo
     */
    private void authenticateUser(final MxOSRequestState requestState,
            Credentials credentials, long failedLoginAttempts,
            IMetaCRUD metaCRUD, AccessInfo updateAccessInfo) throws Exception {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authenticateUser method start."));
        }

        final Password passwordProvider = MxOSApp.getInstance()
                .getPasswordProvider();
        final String givenPassword = requestState.getInputParams()
                .get(MailboxProperty.password.name()).get(0);
        PasswordType pType = null;
        if (null != credentials.getPasswordStoreType()) {
            pType = PasswordType.getTypeWithValue(credentials
                    .getPasswordStoreType().toString());
        }
        String dbPassword = null;
        if (null != credentials.getPassword()) {
            dbPassword = credentials.getPassword();
        }
        boolean isAuthorized = false;
        if (null != pType && null != dbPassword) {
            isAuthorized = PasswordUtil.isAuthorised(passwordProvider, pType,
                    dbPassword, givenPassword);
        }
        final long lastLoginTime = System.currentTimeMillis();

        if (!isAuthorized) {
            failedLoginAttempts++;
            if (updateMSSAfterAuthenticationEnabled()) {
                updateAccessInfo.setLastFailedLoginTime(lastLoginTime);
                updateAccessInfo.setNumFailedLoginAttempts(failedLoginAttempts);
                updateAccessInfo.setLastLoginTime(lastLoginTime);
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.accessInfo,
                        updateAccessInfo);
                metaCRUD.updateMailBoxAccessInfo(requestState);
            }
            throw new AuthorizationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name());

        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Authentication was successful.");
            }
            failedLoginAttempts = 0;
            if (updateMSSAfterAuthenticationEnabled()) {
                updateAccessInfo.setLastSuccessfulLoginTime(lastLoginTime);
                updateAccessInfo.setNumFailedLoginAttempts(failedLoginAttempts);
                updateAccessInfo.setLastLoginTime(lastLoginTime);
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.accessInfo,
                        updateAccessInfo);
                metaCRUD.updateMailBoxAccessInfo(requestState);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authenticateUser method end."));
        }
    }

    private boolean isAuthenticateLockedAccountEnabled() {
        String unlockAccount = System
                .getProperty(SystemProperty.resetFailedLoginAttemptsOnAuthSuccessOfLockedAccount
                        .name());
        return Boolean.valueOf(unlockAccount);
    }

    private boolean isCheckLockedAccountEnabled() {
        String checkLockedAccountEnabled = System
                .getProperty(SystemProperty.checkLockedAccountEnabled.name());
        return Boolean.valueOf(checkLockedAccountEnabled);
    }

    private boolean isBadPasswordWindowEnabled() {
        String badPasswordWindowEnabled = System
                .getProperty(SystemProperty.badPasswordWindowEnabled.name());
        return Boolean.valueOf(badPasswordWindowEnabled);
    }

    private boolean isBadPasswordDelayEnabled() {
        String badPasswordDelayEnabled = System
                .getProperty(SystemProperty.badPasswordDelayEnabled.name());
        return Boolean.valueOf(badPasswordDelayEnabled);
    }

    private boolean updateMSSAfterAuthenticationEnabled() {
        String updateMSSAfterAuthenticationEnabled = System
                .getProperty(SystemProperty.updateMSSAfterAuthenticationEnabled
                        .name());
        return Boolean.valueOf(updateMSSAfterAuthenticationEnabled);
    }
}