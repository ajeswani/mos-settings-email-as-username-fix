/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.saml;

import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.SamlError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.saml.service.SAMLService;
import com.opwvmsg.mxos.saml.service.SAMLConfig;

/**
 * Action to decode SAML Assertion
 * 
 * @author
 * 
 */
public class DecodeSamlAssertion implements MxOSBaseAction {

    private static Logger logger = Logger.getLogger(DecodeSamlAssertion.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DecodeSamlAssertion action start.");
        }
        try {
            String samlAssertion = requestState.getInputParams()
                    .get(MxOSConstants.SAML_ASSERTION).get(0);
            SAMLService samlService = SAMLConfig.getInstance(System
                    .getProperty(SystemProperty.samlProvider.name()));
            Map<String, String> samlAttributes = samlService
                    .decodeSAMLAssertion(samlAssertion);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.samlAttributes,
                    samlAttributes);
        } catch (MxOSException ex) {
            logger.error("Unable to decode SAML Assertion");
            throw ex;
        } catch (Exception ex) {
            logger.error("Unable to decode SAML Assertion " + ex);
            throw new ApplicationException(
                    SamlError.SAML_UNABLE_TO_DECODE_SAML_ASSERTION.name(), ex);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("DecodeSamlAssertion action end.");
        }
    }
}
