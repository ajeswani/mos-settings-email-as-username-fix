/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update sieve filters blocked senders.
 *
 * @author mxos-dev
 */
public class UpdateSieveBlockedSenders implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(UpdateSieveBlockedSenders.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateSieveBlockedSenders action start."));
        }
        try {
            final String oldSieveFilterBlockedSender = requestState
                    .getInputParams()
                    .get(MailboxProperty.oldBlockedSender.name())
                    .get(0);
            final String newSieveFilterBlockedSender = requestState
                    .getInputParams()
                    .get(MailboxProperty.newBlockedSender.name())
                    .get(0);

            List<String> blockedSendersList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.sieveFiltersBlockedSendersList);

            if (blockedSendersList == null) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_SIEVE_BLOCKED_SENDER
                                .name());
            }
            if (!blockedSendersList
                    .contains(oldSieveFilterBlockedSender.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError
                        .MBX_INVALID_OLD_SIEVE_BLOCKED_SENDER_NOT_EXIST
                                .name());
            }
            if (blockedSendersList
                    .remove(oldSieveFilterBlockedSender.toLowerCase())) {
                blockedSendersList.add(newSieveFilterBlockedSender
                        .toLowerCase());
                final String[] sieveFilterBlockedSendersArray
                = blockedSendersList
                        .toArray(new String[blockedSendersList.size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.blockedSenders,
                                sieveFilterBlockedSendersArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_SIEVE_BLOCKED_SENDER
                                .name());
            }

        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while update sieve blocked senders.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SIEVE_BLOCKED_SENDERS_POST
                            .name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateSieveBlockedSenders action end."));
        }
    }
}
