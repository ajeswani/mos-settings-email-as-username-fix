/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.CaptchaException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to update mailbox.
 * 
 * @author mxos-devs
 */
public class UpdateMailbox implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateMailbox.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateMailbox action start."));
        }
        if (requestState == null || requestState.getBackendState() == null
                || requestState.getBackendState().size() == 0) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "Invalid request cannot update..."));
            }
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name());
        }
        ICRUDPool<IMailboxCRUD> provCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;

        int failedCaptchaAttempts = 0;
        int maxFailedCaptchaAttempts = -1;
        String captchaStatus = null;
        // Getting the CAPTCHA details if the service name is CaptchaService
        if (requestState.getServiceName().toString()
                .equals(ServiceEnum.CaptchaService.name())) {
            captchaStatus = requestState.getInputParams()
                    .get(MailboxProperty.isValid.name()).get(0);
            // if CAPTCHA validity is false/true, then get the details of
            // failure cases and the count of it.
            final Credentials credentials = (Credentials) requestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.credentials);
            try {
                failedCaptchaAttempts = credentials
                        .getFailedCaptchaLoginAttempts();
                maxFailedCaptchaAttempts = credentials
                        .getMaxFailedCaptchaLoginAttempts();
            } catch (NullPointerException npEx) {
                logger.error("mandatory value in credentials are null", npEx);
                // assuming the failedCaptchaAttempts value as 0.
                // assuming the maxFailedCaptchaAttempts value as -1.
            } finally {
                if ("false".equalsIgnoreCase(captchaStatus)) {
                    // Incrementing the attempt count, as the same is
                    // incremented along with the mailbox update. the
                    // increment is required here because the initial
                    // credentials fetched have the previous value and the
                    // incremented value is required for logic calculation.
                    ++failedCaptchaAttempts;
                }
            }
        }
        try {
            final IBackendState backendState = requestState.getBackendState()
                    .get(MailboxDBTypes.ldap.name());

            final IBackendState metaBackendState = requestState
                    .getBackendState().get(MetaDBTypes.mss.name());

            // Throw exception only if the operation does not update signatures
            // as update signature now done via multiple Signatures APIs
            if (backendState.getSize() == 0
                    && metaBackendState.getSize() == 0
                    && requestState.getServiceName() != ServiceEnum.MailSendSignatureService) {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }

            if (backendState.getSize() != 0) {
                // Update Provisioning Data
                provCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = provCRUDPool.borrowObject();
                MailboxProvisionHelper.update(mailboxCRUD, requestState);
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer(
                            "UpdateMailbox LDAP update done..!"));
                }
                mailboxCRUD.commit();
            }

            if (metaBackendState.getSize() != 0) {
                // Update Meta Data
                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();
                metaCRUD.updateMailbox(requestState);
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer(
                            "UpdateMailbox MSS update done..!"));
                }
                metaCRUD.commit();
            }
            if (null != captchaStatus
                    && "false".equalsIgnoreCase(captchaStatus)) {
                // Throws the error in case of CAPTCHA is not valid.
                if (failedCaptchaAttempts < maxFailedCaptchaAttempts) {
                    logger.error("CAPTCHA is not valid - Not exceeded the attempts");
                    throw new CaptchaException(
                            MailboxError.CPT_ATTEMPTS_ALLOWED.name());
                } else {
                    logger.error("CAPTCHA is not valid - Exceeded the attempts");
                    throw new CaptchaException(
                            MailboxError.CPT_ATTEMPTS_EXCEEDED.name());
                }
            }
        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (requestState.getServiceName().toString()
                    .equals(ServiceEnum.CaptchaService.name())) {
                // Throws the error if LDAP error occurs while updating CAPTCHA
                // details.
                if (failedCaptchaAttempts < maxFailedCaptchaAttempts) {
                    logger.error("CAPTCHA is not valid - Not exceeded the attempts with LDAP error");
                    throw new MxOSException(
                            MailboxError.CPT_ATTEMPTS_ALLOWED_LDAP_UPDATE_ERROR
                                    .name());
                } else {
                    logger.error("CAPTCHA is not valid - Exceeded the attempts with LDAP error");
                    throw new MxOSException(
                            MailboxError.CPT_ATTEMPTS_EXCEEDED_LDAP_UPDATE_ERROR
                                    .name());
                }
            }
            throw e;
        } catch (final CaptchaException e) {
            logger.error("Error while validating captcha ", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new MxOSException(e.getCode(), e);
        } catch (final Exception e) {
            logger.error("Error while update mailbox.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } catch (final Throwable t) {
            logger.error("Error while update mailbox.", t);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
        } finally {
            if (provCRUDPool != null && mailboxCRUD != null) {
                try {
                    provCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Meta CRUD pool:" + e.getMessage());
                }
            }
        }

        // Update in OX if backend is set to OX
        String appSuiteIntegrated = System
                .getProperty(SystemProperty.appSuiteIntegrated.name());
        if (appSuiteIntegrated != null
                && appSuiteIntegrated.trim().equalsIgnoreCase(
                        MxOSConstants.TRUE)) {
            if (MxOSConfig.getAddressBookBackend().equals(
                    AddressBookDBTypes.ox.name())) {
                ICRUDPool<ISettingsCRUD> oxSettingsCRUDPool = null;
                ISettingsCRUD oxSettingsCRUD = null;

                try {
                    // call mysql update methods
                    oxSettingsCRUDPool = MxOSApp.getInstance()
                            .getOXSettingsMySQLCRUD();

                    oxSettingsCRUD = oxSettingsCRUDPool.borrowObject();
                    oxSettingsCRUD.updateSetting(requestState);
                    if (logger.isDebugEnabled()) {
                        logger.debug(new StringBuffer(
                                "UpdateMailbox MySQL update done..!"));
                    }
                    oxSettingsCRUD.commit();

                } catch (final MxOSException e) {
                    if (oxSettingsCRUD != null) {
                        oxSettingsCRUD.rollback();
                    }
                    throw e;
                } catch (Exception e) {
                    logger.error("Error while update mailbox in OX.", e);
                    if (oxSettingsCRUD != null) {
                        oxSettingsCRUD.rollback();
                    }
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                } finally {
                    try {
                        if (oxSettingsCRUDPool != null
                                && oxSettingsCRUD != null) {
                            oxSettingsCRUDPool.returnObject(oxSettingsCRUD);
                        }
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                    }
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateMailbox action end."));
        }
    }
}
