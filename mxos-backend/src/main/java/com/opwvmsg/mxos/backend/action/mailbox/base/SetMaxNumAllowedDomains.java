/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

/**
 * Action class to set Maximum allowed number of Aliases.
 * 
 * @author mxos-dev
 */
public class SetMaxNumAllowedDomains implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetMaxNumAllowedDomains.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetMaxNumAllowedDomains action start."));
        }
        try {
            final String maxNumAllowedDomain = requestState.getInputParams()
                    .get(MailboxProperty.maxNumAllowedDomains.name()).get(0);

            if (!requestState.getOperationName().contains(
                    ServiceEnum.CosBaseService.name())) {
                final Base base = (Base) requestState.getDbPojoMap()
                        .getPropertyAsObject(MxOSPOJOs.base);
                try {
                    int maxNumAllowedDomainInt = 1;
                    if (maxNumAllowedDomain != null
                            && !maxNumAllowedDomain.equals("")) {
                        maxNumAllowedDomainInt = Integer
                                .parseInt(maxNumAllowedDomain);
                    }
                    final List<String> allowedDomainList = base
                            .getAllowedDomains();
                    if (allowedDomainList != null
                            && !maxNumAllowedDomain.equals("")) {
                        if (allowedDomainList.size() > maxNumAllowedDomainInt) {
                            throw new InvalidRequestException(
                                    MailboxError.MBX_UNABLE_TO_SET_MAX_ALLOWEDDOMAIN
                                            .name());
                        }
                    }
                } catch (final NumberFormatException e) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_MAX_ALLOWEDDOMAIN.name(),
                            e);
                }
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.maxNumAllowedDomains,
                            maxNumAllowedDomain);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while set max number allowed domains.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_MAX_ALLOWEDDOMAIN.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMaxNumAllowedDomains action end."));
        }
    }
}
