/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums.Status;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to create mailbox.
 * 
 * @author mxos-dev
 */
public class CreateMailbox implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateMailbox.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateMailbox action start."));
        }

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        // Mandatory params
        final String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        boolean mailboxExistsInMss = false;

        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();

            // Create in LDAP
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            MailboxProvisionHelper.create(mailboxCRUD, requestState);
            String status = requestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.status);
            boolean mssMailbox = true;
            // if status is Proxy
            // Check for the config param createMssMailboxOnProxyStatus
            if (status != null && status.equalsIgnoreCase(Status.PROXY.name())) {
                mssMailbox = Boolean.parseBoolean(MxOSConfig
                        .getCreateMssMailboxOnProxyStatus());
            }
            if (mssMailbox) {
                // Create in Metadata
                requestState.getAdditionalParams().setProperty(
                        MailboxProperty.systemfolders,
                        MxOSConfig.getSystemFolders());

                ActionUtils.readMSSLinkInfo(requestState, mailboxCRUD);
                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();

                try {
                    MailboxMetaServiceHelper.create(metaCRUD, requestState);
                } catch (final MxOSException e) {
                    if (e.getCode() != null
                            && e.getCode().equals(
                                    MailboxError.MBX_ALREADY_EXISTS.name())) {
                        mailboxExistsInMss = true;
                    }
                    throw e;
                }
                metaCRUD.commit();
            } else {
                logger.info("Create MSS Mailbox Disabled on Proxy status for email: "
                        + email);
            }
            mailboxCRUD.commit();
        } catch (final MxOSException e) {
            // If mailbox already exists on LDAP and and mailboxId exists in
            // request parameter try creating it on MSS
            if (!mailboxExistsInMss
                    && e.getCode() != null
                    && e.getCode().equals(
                            MailboxError.MBX_ALREADY_EXISTS.name())
                    && requestState.getInputParams().containsKey(
                            MailboxProperty.mailboxId.name())) {
            	logger.info("Creating Mailbox in MSS.");  
                createMailboxInMSS(mailboxCRUD, metaCRUDPool, metaCRUD,
                        requestState);
            } else {
                if (metaCRUD != null) {
                    metaCRUD.rollback();
                }
                if (mailboxCRUD != null) {
                    mailboxCRUD.rollback();
                }
                throw e;
            }
        } catch (final Exception e) {
            logger.error("Error while create mailbox.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                }
                if (metaCRUDPool != null && metaCRUD != null) {
                    metaCRUDPool.returnObject(metaCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        // Create in OX if backend is set to OX
        Boolean appSuiteIntegrated = Boolean.parseBoolean(System
                .getProperty(SystemProperty.appSuiteIntegrated.name()));
        Boolean oxCreateContextEnabled = Boolean.parseBoolean(System
                .getProperty(SystemProperty.oxCreateContextEnabled.name()));
        if (appSuiteIntegrated && oxCreateContextEnabled) {
            if (MxOSConfig.getAddressBookBackend().equals(
                    AddressBookDBTypes.ox.name())) {
                ICRUDPool<IRMIMailboxCRUD> rmiMailboxCRUDPool = null;
                IRMIMailboxCRUD rmiMailboxCRUD = null;

                try {
                    rmiMailboxCRUDPool = MxOSApp.getInstance()
                            .getRMIMailboxCRUD();

                    rmiMailboxCRUD = rmiMailboxCRUDPool.borrowObject();
                    rmiMailboxCRUD.createMailbox(requestState);
                    logger.info(new StringBuilder("Mailbox [").append(email)
                            .append("] created in OX backend."));
                } catch (final MxOSException e) {
                    logger.error("Error while create mailbox in OX.", e);
                    if (rmiMailboxCRUD != null) {
                        rmiMailboxCRUD.rollback();
                        logger.info("Rollback success for creating the mailbox in OX");
                    }
                    throw e;
                } catch (Exception e) {
                    logger.error("Error while create mailbox in OX.", e);
                    if (rmiMailboxCRUD != null) {
                        rmiMailboxCRUD.rollback();
                        logger.info("Rollback success for creating the mailbox in OX");
                    }
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                } finally {
                    try {
                        if (rmiMailboxCRUDPool != null
                                && rmiMailboxCRUD != null) {
                            rmiMailboxCRUDPool.returnObject(rmiMailboxCRUD);
                        }
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                    }
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateMailbox action end."));
        }
    }

    /**
     * Create mailbox only in MSS.
     * 
     * @param mailboxCRUD mailboxCRUD
     * @param metaCRUDPool metaCRUDPool
     * @param metaCRUD metaCRUD
     * @param requestState requestState
     * @throws MxOSException mssMxosException in case of any errors.
     */
    private void createMailboxInMSS(IMailboxCRUD mailboxCRUD,
            ICRUDPool<IMetaCRUD> metaCRUDPool, IMetaCRUD metaCRUD,
            final MxOSRequestState requestState) throws MxOSException {
        try {
            MssLinkInfo info = ActionUtils.readMSSLinkInfo(requestState,
                    mailboxCRUD);
            if (info != null) {
                Long mailboxId = info.getMailboxId();
                final Base base = mailboxCRUD.readMailboxBase(requestState
                        .getInputParams().get(MailboxProperty.email.name())
                        .get(0));
                if (logger.isDebugEnabled()) {
                    logger.debug("MailboxId in MssLinkInfo : " + mailboxId);
                    logger.debug("MailboxId on LDAP : " + base.getMailboxId());
                }
                if (!mailboxId.equals(base.getMailboxId())) {
                    logger.error("Error Mailbox Id"
                            + " in request and LDAP does not match"
                            + " while mailbox creation on mss.");
                    throw new ApplicationException(
                            MailboxError.MBX_MAILBOX_ID_MISS_MATCH.name());
                } else {
                    if (metaCRUDPool == null && metaCRUD == null) {
                        metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                        metaCRUD = metaCRUDPool.borrowObject();
                    }
                    logger.info("Mailbox already exist on LDAP"
                            + ", trying to create mailbox on MSS.");
                    MailboxMetaServiceHelper.create(metaCRUD, requestState);
                    metaCRUD.commit();
                    logger.info("Mailbox already exist on LDAP"
                            + ", mailbox creation on MSS success.");
                }
            }
        } catch (final MxOSException mssMxosException) {
            throw mssMxosException;
        }
    }
}
