/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.folder;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
/**
 * Action class to create mailbox in mss.
 * 
 * @author mxos-dev
 */
public class CreateMailboxInMSS implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateMailboxInMSS.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateMailboxInMSS action start."));
        }

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {

            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();

            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            metaCRUD.createMailbox(requestState);
            metaCRUD.commit();

            logger.info("Create Mailbox SUCCESS of: " + email);
        } catch (final MxOSException e) {
            logger.error("Error while creating mailbox.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while creating mailbox.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } finally {
            try {
                if (metaCRUDPool != null && metaCRUD != null) {
                    metaCRUDPool.returnObject(metaCRUD);
                }

                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                }

            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateMailboxInMSS action end."));
        }
    }
}
