/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPUtils;
import com.opwvmsg.mxos.backend.crud.ldap.PasswordType;
import com.opwvmsg.mxos.backend.crud.ldap.PasswordUtil;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.intermail.mail.AccessInfo;
import com.opwvmsg.utils.paf.util.password.Password;
import com.opwvmsg.mxos.data.pojos.Mailbox;

/**
 * Action class to authenticate and unlock the user if required.
 * 
 * @author mxos-dev
 */
public class AdvancedAuthenticate implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(AdvancedAuthenticate.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AdvancedAuthenticate action start."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        Credentials credentials = null;
        long failedLoginAttempts = 0;
        int maxFailedLoginAttempts = 0;
        long lastFailedLoginAttemptTime = 0;
        String email = null;
        AccessInfo accessInfo = null;
        AccessInfo updateAccessInfo = null;

        Mailbox mailbox = (Mailbox) requestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mailbox);

        if (null == mailbox) {
            logger.error("Empty mailbox object received as an AuthenticateAndUnlock action");
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Empty mailbox received as an AuthenticateAndUnlock action");
        }

        try {

            // Get the credentials Object
            credentials = mailbox.getCredentials();
            if (null == credentials) {
                logger.error("Credentials object is null in the mailbox");
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(),
                        "Credentials object is null in the mailbox");
            }

            boolean unlockMailbox = false;
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.unlockMailbox.name())) {
                unlockMailbox = Boolean.parseBoolean(requestState
                        .getInputParams()
                        .get(MailboxProperty.unlockMailbox.name()).get(0));
            }

            // If both the features are enabled, disable the advanced
            // Authetication.
            boolean isAdvancedAutheticationEnabled = isAdvancedAuthenticationEnabled();
            if (unlockMailbox && isBadPasswordWindowEnabled()) {
                isAdvancedAutheticationEnabled = false;
            }

            if (isAdvancedAutheticationEnabled) {
                // If Advanced Authetication is enabled, get all the required
                // information from MSS.

                // Prepare the MssLinkInfo Object to get MSS Meta CRUD Pool.
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.mssLinkInfo,
                        LDAPUtils.populateMSSLinkInfoFromMailbox(mailbox));

                if (requestState.getInputParams().get(
                        MailboxProperty.email.name()) != null)
                    email = requestState.getInputParams()
                            .get(MailboxProperty.email.name()).get(0);

                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();
                updateAccessInfo = new AccessInfo();

                try {
                    // Read the mailAccess Info
                    metaCRUD.readMailBoxAccessInfo(requestState);
                } catch (final ApplicationException e) {
                    logger.error("Error while reading mailbox access info for "
                            + email);
                    if (!e.getCode().equalsIgnoreCase(
                            MailboxError.MBX_NOT_FOUND.name())) {
                        throw e;
                    }
                    logger.warn("Mailbox NOT found in MSS for " + email);
                }

                accessInfo = (AccessInfo) requestState.getDbPojoMap()
                        .getPropertyAsObject(MxOSPOJOs.accessInfo);
                if (null == accessInfo) {
                    logger.error("Failed to get the AccessInfo");
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Failed to get the AccessInfo");
                }

                failedLoginAttempts = accessInfo.getNumFailedLoginAttempts();
                lastFailedLoginAttemptTime = accessInfo
                        .getLastFailedLoginTime();

                if (null != credentials.getMaxFailedLoginAttempts()) {
                    maxFailedLoginAttempts = (int) credentials
                            .getMaxFailedLoginAttempts();
                }

                if (isBadPasswordWindowEnabled()) {
                    if (!checkBadPasswordWindow(lastFailedLoginAttemptTime)) {
                        failedLoginAttempts = 0;
                    } else if (isBadPasswordDelayEnabled()) {
                        checkBadPasswordDelay(failedLoginAttempts,
                                maxFailedLoginAttempts,
                                lastFailedLoginAttemptTime);
                    }
                } else {
                    // If the password delay feature is disabled, perform the
                    // below flow.
                    if (!checkAutheticationLockTimeout(lastFailedLoginAttemptTime)) {
                        // If the last Failed login time has exceeded the Authenticate Lock Timeout
                        failedLoginAttempts = 0;
                        lastFailedLoginAttemptTime = 0;
                    } else if (failedLoginAttempts >= maxFailedLoginAttempts) {
                        // If the last Failed login time has not exceeded but
                        // noof attempts has increased, reject the login if
                        // unlockMailbox(appSuite login) is false.
                        // If unlockMailbox(appSuite login) is true, allow the
                        // user to authenticate.
                        if (unlockMailbox == false) {
                            throw new AuthorizationException(
                                    MailboxError.MBX_ACCOUNT_LOCKED.name());
                        }
                    }
                }

                if (isCheckLockedAccountEnabled()) {
                    authLockedAccount(requestState, failedLoginAttempts,
                            maxFailedLoginAttempts, metaCRUD, updateAccessInfo);
                }
            }

            authenticateUser(requestState, credentials, failedLoginAttempts,
                    metaCRUD, updateAccessInfo, isAdvancedAutheticationEnabled);

            if (isAdvancedAutheticationEnabled)
                metaCRUD.commit();

        } catch (final MxOSException e) {
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while getting credentails.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name(), e);
        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Meta CRUD pool:" + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AdvancedAuthenticate action end."));
        }
    }

    /**
     * check if authentication lock timeout has happened.
     * 
     * @param long lastFailedLoginAttemptTime
     * @return boolean
     */
    private boolean checkAutheticationLockTimeout(
            final long lastFailedLoginAttemptTime) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "checkAutheticationLockTimeout method start."));
        }
        final int authTimeoutWindow = MxOSConfig.getAutheticationLockTimeout();
        boolean authenticateTimeout = true;
        final long currentTime = System.currentTimeMillis();
        // checking if badPasswordWindow is still applicable.
        if ((currentTime - lastFailedLoginAttemptTime) > authTimeoutWindow
                * MxOSConstants.MIN_TO_MILLISEC)
            authenticateTimeout = false;
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "checkAutheticationLockTimeout method end."));
        }
        return authenticateTimeout;
    }

    /**
     * check if badPasswordWindow is still applicable.
     * 
     * @param long lastFailedLoginAttemptTime
     * @return boolean
     */
    private boolean checkBadPasswordWindow(long lastFailedLoginAttemptTime)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "checkBadPasswordWindow method start."));
        }
        int badPasswordWindow = MxOSConfig.getBadPasswordWindow();
        boolean isbpwApplicable = true;
        long currentTime = System.currentTimeMillis();
        // checking if badPasswordWindow is still applicable.
        if ((currentTime - lastFailedLoginAttemptTime) > badPasswordWindow
                * MxOSConstants.MIN_TO_MILLISEC)
            isbpwApplicable = false;

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("checkBadPasswordWindow method end."));
        }
        return isbpwApplicable;

    }

    /**
     * check if badPasswordDelay is applicable; if yes then delay mOS thread
     * appropriately.
     * 
     * @param Integer failedLoginAttempts
     * @param Integer maxFailedLoginAttempts
     * @param long lastFailedLoginAttemptTime
     */
    private void checkBadPasswordDelay(long failedLoginAttempts,
            int maxFailedLoginAttempts, long lastFailedLoginAttemptTime)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("checkBadPasswordDelay method start."));
        }
        int badPasswordDelay = MxOSConfig.getBadPasswordDelay();
        long currentTime = System.currentTimeMillis();
        // check if re-authenticate attempt has been delayed appropriately; if
        // not then delay mOS thread appropriately
        if ((failedLoginAttempts != 0)
                && (currentTime - lastFailedLoginAttemptTime) < (badPasswordDelay * failedLoginAttempts)
                        * MxOSConstants.SEC_TO_MILLISEC) {
            try {
                Thread.sleep(badPasswordDelay * failedLoginAttempts
                        * MxOSConstants.SEC_TO_MILLISEC);
            } catch (InterruptedException e) {
                logger.error("Error while implementing the bad password delay",
                        e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("checkBadPasswordDelay method end."));
        }
    }

    /**
     * check if account is locked and authenticate if applicable.
     * 
     * @param MxOSRequestState requestState
     * @param Integer failedLoginAttempts
     * @param Integer maxFailedLoginAttempts
     * @param IMetaCRUD metaCRUD
     * @param AccessInfo updateAccessInfo
     */
    private void authLockedAccount(final MxOSRequestState requestState,
            long failedLoginAttempts, int maxFailedLoginAttempts,
            IMetaCRUD metaCRUD, AccessInfo updateAccessInfo)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authLockedAccount method start."));
        }
        // check if account is locked and authenticate if
        // isAuthenticateLockedAccountEnabled is true.
        if (maxFailedLoginAttempts != 0) {
            if (failedLoginAttempts >= maxFailedLoginAttempts
                    && !isAuthenticateLockedAccountEnabled()) {
                final long lastLoginTime = System.currentTimeMillis();
                if (isAdvancedAuthenticationEnabled()) {
                    updateAccessInfo.setLastLoginTime(lastLoginTime);
                    requestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.accessInfo, updateAccessInfo);
                    metaCRUD.updateMailBoxAccessInfo(requestState);
                }
                throw new AuthorizationException(
                        MailboxError.MBX_ACCOUNT_LOCKED.name());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authLockedAccount method end."));
        }
    }

    /**
     * check if badPasswordDelay is applicable; if yes then delay mOS thread
     * appropriately.
     * 
     * @param MxOSRequestState requestState
     * @param Integer failedLoginAttempts
     * @param Credentials credentials
     * @param IMetaCRUD metaCRUD
     * @param AccessInfo updateAccessInfo
     */
    private void authenticateUser(final MxOSRequestState requestState,
            Credentials credentials, long failedLoginAttempts,
            IMetaCRUD metaCRUD, AccessInfo updateAccessInfo,
            final boolean isAdvancedAuthenticationEnabled) throws Exception {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authenticateUser method start."));
        }

        final Password passwordProvider = MxOSApp.getInstance()
                .getPasswordProvider();
        final String givenPassword = requestState.getInputParams()
                .get(MailboxProperty.password.name()).get(0);

        PasswordType pType = null;
        if (null != credentials.getPasswordStoreType()) {
            pType = PasswordType.getTypeWithValue(credentials
                    .getPasswordStoreType().toString());
        }
        String dbPassword = null;
        if (null != credentials.getPassword()) {
            dbPassword = credentials.getPassword();
        }
        boolean isAuthorized = false;
        if (null != pType && null != dbPassword) {
            isAuthorized = PasswordUtil.isAuthorised(passwordProvider, pType,
                    dbPassword, givenPassword);
        }
        final long lastLoginTime = System.currentTimeMillis();

        if (!isAuthorized) {
            if (isAdvancedAuthenticationEnabled) {
                failedLoginAttempts++;
                updateAccessInfo.setLastFailedLoginTime(lastLoginTime);
                updateAccessInfo.setNumFailedLoginAttempts(failedLoginAttempts);
                updateAccessInfo.setLastLoginTime(lastLoginTime);
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.accessInfo,
                        updateAccessInfo);
                metaCRUD.updateMailBoxAccessInfo(requestState);
            }
            throw new AuthorizationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name());

        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Authentication was successful.");
            }
            if (isAdvancedAuthenticationEnabled) {
                failedLoginAttempts = 0;
                updateAccessInfo.setLastSuccessfulLoginTime(lastLoginTime);
                updateAccessInfo.setNumFailedLoginAttempts(failedLoginAttempts);
                updateAccessInfo.setLastLoginTime(lastLoginTime);
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.accessInfo,
                        updateAccessInfo);
                metaCRUD.updateMailBoxAccessInfo(requestState);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("authenticateUser method end."));
        }
    }

    private boolean isAuthenticateLockedAccountEnabled() {
        String unlockAccount = System
                .getProperty(SystemProperty.resetFailedLoginAttemptsOnAuthSuccessOfLockedAccount
                        .name());
        return Boolean.valueOf(unlockAccount);
    }

    private boolean isCheckLockedAccountEnabled() {
        String checkLockedAccountEnabled = System
                .getProperty(SystemProperty.checkLockedAccountEnabled.name());
        return Boolean.valueOf(checkLockedAccountEnabled);
    }

    private boolean isBadPasswordWindowEnabled() {
        String badPasswordWindowEnabled = System
                .getProperty(SystemProperty.badPasswordWindowEnabled.name());
        return Boolean.valueOf(badPasswordWindowEnabled);
    }

    private boolean isBadPasswordDelayEnabled() {
        String badPasswordDelayEnabled = System
                .getProperty(SystemProperty.badPasswordDelayEnabled.name());
        return Boolean.valueOf(badPasswordDelayEnabled);
    }

    private boolean isAdvancedAuthenticationEnabled() {
        String advancedAutheticationEnabled = System
                .getProperty(SystemProperty.advancedAuthenticationEnabled.name());
        return Boolean.valueOf(advancedAutheticationEnabled);
    }
}