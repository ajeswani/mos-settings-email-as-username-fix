/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set mailSend bmi filters spam action.
 *
 * @author mxos-dev
 */
public class SetMSBMIFiltersSpamAction implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetMSBMIFiltersSpamAction.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetMSBMIFiltersSpamAction action start."));
        }
        String spamAction = null;
        if (null != requestState.getInputParams().get(
                MailboxProperty.spamAction.name())) {
            spamAction = requestState.getInputParams()
                    .get(MailboxProperty.spamAction.name()).get(0);
        } else if (null != requestState.getInputParams().get(
                MailboxProperty.mailSendBmiSpamAction.name())) {
            spamAction = requestState.getInputParams()
                    .get(MailboxProperty.mailSendBmiSpamAction.name())
                    .get(0);
        }

        try {
            String type = null;
            if (spamAction != null && !spamAction.equals("")) {
                type = MxosEnums.BmiSpamType.fromValue(spamAction).toString();
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.msSpamAction,
                            type);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set MS BMI filters spam action.", e);
            throw new InvalidRequestException(
                    MailboxError
                    .MBX_UNABLE_TO_SET_MS_BMI_FILTERS_SPAM_ACTION
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMSBMIFiltersSpamAction action end."));
        }
    }
}
