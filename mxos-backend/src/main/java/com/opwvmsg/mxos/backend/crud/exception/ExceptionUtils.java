/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.exception;

import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.InvalidSessionException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.exception.ServiceUnavailableException;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;

/**
 * Util class of controllers.
 * 
 * @author mxos-dev
 * 
 */
public final class ExceptionUtils {
    public static final String OX_CONTACT_NOT_FOUND_IN_CONTEXT15_ERROR_CODE = "CON-0125";
    private static final String OX_CONTACT_NOT_FOUND_ERROR_CODE = "CON-0174";
    public static final String OX_GET_GROUP_NOT_FOUND_ERROR_CODE = "CON-0261";
    public static final String OX_DELETE_GROUP_NOT_FOUND_ERROR_CODE = "SVL-0002";
    private static final String OX_EXPIRED_SESSION_ERROR_CODE = "SES-0203";
    private static final String OX_INVALID_SESSION_ERROR_CODE = "SES-0206";
    private static final String OX_INVALID_CREDENTIALS_ERROR_CODE = "LGI-0006";
    public static final String OX_CONTACT_IMAGE_NOT_FOUND_ERROR_CODE = "CON-IMG";
    public static final String OX_INVALID_CONTACT_IMAGE_ERROR_CODE = "CON-IN-IMG";
    public static final String NOT_FOUND_EXCEPTION_CATEGORY = "8";
    public static final String INVALID_ATTRIBUTE_EXCEPTION_CATEGORY = "1";
    public static final String CONNECTION_EXCEPTION_CATEGORY = "2";
    public static final String MSS_CONNECTION_ERROR_CODE = "1";

    public static final String OX_TASK_NOT_FOUND_IN_CONTEXT15_ERROR_CODE = "CON-0125";
    private static final String OX_TASK_NOT_FOUND_ERROR_CODE = "CON-0174";

    /**
     * Utility method for mapping from OX error to appropriate http status code.
     * 
     * @param error
     * @param e
     * @throws InvalidRequestException
     * @throws AuthorizationException
     * @throws ApplicationException
     * @throws InvalidSessionException
     * @throws NotFoundException
     * @throws MxOSException
     */
    public static void createMxOSExceptionFromAddressBookException(
            AddressBookError error, AddressBookException e)
            throws InvalidRequestException, AuthorizationException,
            ApplicationException, InvalidSessionException, NotFoundException,
            MxOSException {
        if (e.getCategory() != null) {
            int categoryCode = Integer.parseInt(e.getCategory());
            switch (categoryCode) {
            case 1:
                if (e.getCode().equals(OX_INVALID_CREDENTIALS_ERROR_CODE)) {
                    throw new AuthorizationException(error.name(), e);
                } else if (e.getCode().equals(
                        OX_INVALID_CONTACT_IMAGE_ERROR_CODE)) {
                    throw new InvalidRequestException(
                            AddressBookError.ABS_INVALID_SIZE_OF_CONTACTS_IMAGE.name(),
                            e);
                } else if (e.getCode().equals(
                        AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                .name())) {
                    throw new InvalidRequestException(
                            AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                    .name(),
                            e);
                } else if (e.getCode().equals(
                        AddressBookError.ABS_GROUPS_MEMBER_DOES_NOT_EXIST
                                .name())) {
                    throw new InvalidRequestException(
                            AddressBookError.ABS_GROUPS_MEMBER_DOES_NOT_EXIST
                                    .name(),
                            e);
                } else if (e.getCode().equals(
                        AddressBookError.ABS_INVALID_SORT_KEY.name())) {
                    throw new InvalidRequestException(
                            AddressBookError.ABS_INVALID_SORT_KEY.name(), e);
                } else if (e.getCode().equals(
                        AddressBookError.ABS_INVALID_SEARCH_TERM.name())) {
                    throw new InvalidRequestException(
                            AddressBookError.ABS_INVALID_SEARCH_TERM.name(), e);
                } else {
                    throw new InvalidRequestException(error.name(), e);
                }
            case 2:
                if (e.getCategory().equals(CONNECTION_EXCEPTION_CATEGORY)) {
                    throw new ServiceUnavailableException(error.name(), e);
                }
                throw new AuthorizationException(error.name(), e);
            case 3:
                throw new AuthorizationException(error.name(), e);
            case 4:
                if (e.getCode().equals(OX_EXPIRED_SESSION_ERROR_CODE)
                        || e.getCode().equals(OX_INVALID_SESSION_ERROR_CODE)) {
                    throw new InvalidSessionException(
                            AddressBookError.ABS_INVALID_SESSION.name(), e);
                } else {
                    throw new ApplicationException(error.name(), e);
                }
            case 5:
                throw new ApplicationException(error.name(), e);
            case 6:
                throw new ApplicationException(error.name(), e);
            case 7:
                throw new ApplicationException(error.name(), e);
            case 8:
                if (e.getCode().equals(
                        AddressBookError.ABS_CONTACT_NOT_FOUND.name())
                        || e.getCode().equals(
                                OX_CONTACT_NOT_FOUND_IN_CONTEXT15_ERROR_CODE)) {
                    throw new NotFoundException(
                            AddressBookError.ABS_CONTACT_NOT_FOUND.name(), e);
                } else if (e.getCode().equals(
                        OX_CONTACT_IMAGE_NOT_FOUND_ERROR_CODE)) {
                    throw new NotFoundException(
                            AddressBookError.ABS_CONTACTS_IMAGE_NOT_FOUND
                                    .name(),
                            e);
                } else if (e.getCode().equals(
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name())) {
                    throw new NotFoundException(
                            AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                            e);

                } else if (e.getCode().equals(
                        AddressBookError.ABS_GROUP_NOT_FOUND.name())
                        || e.getCode()
                                .equals(OX_GET_GROUP_NOT_FOUND_ERROR_CODE)
                        || e.getCode().equals(
                                OX_DELETE_GROUP_NOT_FOUND_ERROR_CODE)) {
                    throw new NotFoundException(
                            AddressBookError.ABS_GROUP_NOT_FOUND.name(), e);
                }
                throw new ApplicationException(error.name(), e);
            case 9:
                throw new ApplicationException(error.name(), e);
            case 10:
                throw new ApplicationException(error.name(), e);
            case 11:
                throw new ApplicationException(error.name(), e);
            case 12:
                throw new ApplicationException(error.name(), e);
            case 13:
                if (e.getCode().equals(OX_CONTACT_NOT_FOUND_ERROR_CODE)) {
                    throw new NotFoundException(
                            AddressBookError.ABS_CONTACT_NOT_FOUND.name(), e);
                }
                throw new ApplicationException(error.name(), e);
            default:
                throw new MxOSException(error.name(), e);
            }
        } else {
            throw new ApplicationException(error.name(), e);
        }
    }

    /**
     * Utility method for mapping from OX error to appropriate http status code.
     * 
     * @param error
     * @param e
     * @throws InvalidRequestException
     * @throws AuthorizationException
     * @throws ApplicationException
     * @throws InvalidSessionException
     * @throws NotFoundException
     * @throws MxOSException
     */
    public static void createMxOSExceptionFromTasksException(
            TasksError error, TasksException e)
            throws InvalidRequestException, AuthorizationException,
            ApplicationException, InvalidSessionException, NotFoundException,
            MxOSException {
        if (e.getCategory() != null) {
            int categoryCode = Integer.parseInt(e.getCategory());
            switch (categoryCode) {
            case 1:
                if (e.getCode().equals(OX_INVALID_CREDENTIALS_ERROR_CODE)) {
                    throw new AuthorizationException(error.name(), e);
                } else if (e.getCode().equals(
                        TasksError.TSK_INVALID_SORT_KEY.name())) {
                    throw new InvalidRequestException(
                            TasksError.TSK_INVALID_SORT_KEY.name(), e);
                } else if (e.getCode().equals(
                        TasksError.TSK_INVALID_SEARCH_TERM.name())) {
                    throw new InvalidRequestException(
                            TasksError.TSK_INVALID_SEARCH_TERM.name(), e);
                } else {
                    throw new InvalidRequestException(error.name(), e);
                }
            case 2:
                if (e.getCategory().equals(CONNECTION_EXCEPTION_CATEGORY)) {
                    throw new ServiceUnavailableException(error.name(), e);
                }
                throw new AuthorizationException(error.name(), e);
            case 3:
                throw new AuthorizationException(error.name(), e);
            case 4:
                if (e.getCode().equals(OX_EXPIRED_SESSION_ERROR_CODE)
                        || e.getCode().equals(OX_INVALID_SESSION_ERROR_CODE)) {
                    throw new InvalidSessionException(
                            TasksError.TSK_INVALID_SESSION.name(), e);
                } else {
                    throw new ApplicationException(error.name(), e);
                }
            case 5:
                throw new ApplicationException(error.name(), e);
            case 6:
                throw new ApplicationException(error.name(), e);
            case 7:
                throw new ApplicationException(error.name(), e);
            case 8:
                if (e.getCode().equals(
                        TasksError.TSK_TASK_NOT_FOUND.name())
                        || e.getCode().equals(
                                OX_TASK_NOT_FOUND_IN_CONTEXT15_ERROR_CODE)) {
                    throw new NotFoundException(
                            TasksError.TSK_TASK_NOT_FOUND.name(), e);
                }
                throw new ApplicationException(error.name(), e);
            case 9:
                throw new ApplicationException(error.name(), e);
            case 10:
                throw new ApplicationException(error.name(), e);
            case 11:
                throw new ApplicationException(error.name(), e);
            case 12:
                throw new ApplicationException(error.name(), e);
            case 13:
                if (e.getCode().equals(OX_TASK_NOT_FOUND_ERROR_CODE)) {
                    throw new NotFoundException(
                            TasksError.TSK_TASK_NOT_FOUND.name(), e);
                }
                throw new ApplicationException(error.name(), e);
            default:
                throw new MxOSException(error.name(), e);
            }
        } else {
            throw new ApplicationException(error.name(), e);
        }
    }

    /**
     * Utility method for mapping from Intermail error to appropriate MXOS
     * Errors
     * 
     * @param defaultErrorCode
     * @param iException
     * @throws MxOSException
     */
    public static void createApplicationExceptionFromIntermailException(
            String defaultErrorCode, IntermailException iException)
            throws ApplicationException {
        if (iException.getFormattedString().contains("MsDuplicateName")) {
            throw new ApplicationException(
                    FolderError.FLD_ALREADY_EXISTS.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsNotFound")) {
            throw new ApplicationException(MailboxError.MBX_NOT_FOUND.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsAlreadyExists")) {
            throw new ApplicationException(
                    MailboxError.MBX_ALREADY_EXISTS.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsMsgNotInFolder")) {
            throw new ApplicationException(MessageError.MSG_NOT_FOUND.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsMsgIDNotFound")) {
            throw new ApplicationException(MessageError.MSG_NOT_FOUND.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsMsgIDInvalid")) {
            throw new ApplicationException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains(
                "MsFolderDoesNotExist")) {
            throw new ApplicationException(FolderError.FLD_NOT_FOUND.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains(
                "MsFolderAlreadyExists")) {
            throw new ApplicationException(
                    FolderError.FLD_ALREADY_EXISTS.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsLimitMsgSize")) {
            throw new ApplicationException(
                    MessageError.MBX_QUOTA_LIMIT_ERROR.name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsLimitTotalSize")) {
            throw new ApplicationException(
                    MessageError.MBX_QUOTA_LIMIT_ERROR .name(),
                    iException.getMessage());
        } else if (iException.getFormattedString().contains("MsLimitNumMsgs")) {
            throw new ApplicationException(
                    MessageError.MBX_QUOTA_LIMIT_ERROR .name(),
                    iException.getMessage());
        }   
        throw new ApplicationException(defaultErrorCode,
                iException.getMessage());
    }

    /**
     * Default private constructor.
     */
    private ExceptionUtils() {
    }
}
