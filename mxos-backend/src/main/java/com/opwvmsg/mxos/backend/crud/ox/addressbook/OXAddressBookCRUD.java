/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.addressbook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.ox.OXAbstractHttpCRUD;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.ContactResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.JsonToAddressBookMapper;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.LoginResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.Response;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.UserResponse;
import com.opwvmsg.mxos.backend.crud.we.utils.Query;
import com.opwvmsg.mxos.backend.crud.we.utils.Query.Condition;
import com.opwvmsg.mxos.backend.crud.we.utils.Term;
import com.opwvmsg.mxos.backend.crud.we.utils.Visitor;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Class to implement methods OX specific methods for communicating and parsing
 * the response
 * 
 * @author mxos-dev
 * 
 */
public class OXAddressBookCRUD extends OXAbstractHttpCRUD implements
        IAddressBookCRUD {

    public enum Action {
        ADD_GROUP_MEMBER, DELETE_GROUP_MEMBER, DELETE_ALL_GROUP_MEMBER, ADD_EXTERNAL_GROUP_MEMBER, DELETE_EXTERNAL_GROUP_MEMBER, DELETE_ALL_EXTERNAL_GROUP_MEMBER;
    }

    enum SortOrder {
        ascending("asc"), descending("desc");

        private final String value;

        SortOrder(String value) {
            this.value = value;
        }

        public String convert() {
            return value;
        }
    }

    private static Logger logger = Logger.getLogger(OXAddressBookCRUD.class);
    private static final String USER_FOLDER_ID = "6";

    private static final String IS_GROUP_COLUMN = "602";
    private static final String GROUP_NOTE_COLUMNS = IS_GROUP_COLUMN
            + ",1,4,5,20,518,560,610,611,612,557,589,553,223";
    private static final String GROUP_BASE_COLUMNS = IS_GROUP_COLUMN
            + ",1,4,5,20,500,223";
    private static final String CONTACT_NAME_COLUMNS = "500,501,502,503,504,505,515";
    private static final String CONTACT_BASE_COLUMNS = "1,4,5,20,100,101,102,518,560,610,611,612,557,589,553,223";
    private static final String CONTACT_PERSONALINFO_COLUMNS = "512,516";
    private static final String CONTACT_PERSONALINFO_ADDRESS_COLUMNS = "506,507,508,509,510";
    private static final String CONTACT_PERSONALINFO_COMMUNICATION_COLUMNS = "548,549,550,552,556,566,567";
    private static final String CONTACT_PERSONALINFO_EVENT_COLUMNS = "511,517";
    private static final String CONTACT_WORKINFO_COLUMNS = "569,519,520,521,536,537";
    private static final String CONTACT_WORKINFO_ADDRESS_COLUMNS = "523,525,526,527,528";
    private static final String CONTACT_WORKINFO_COMMUNICATION_COLUMNS = "542,543,544,551,555";
    public static final String EXTERNAL_MEMBER_FIELD = "0";
    public static final String MEMBER_FIELD = "1";
    public static final String MEMBER_FIELD_ALTERNATE = "2";
    public static final String MEMBER_FIELD_OTHER = "3";
    public static final String SQR_BRCKET_OPEN = "[";
    public static final String SQR_BRCKET_CLOSE = "]";
    public static final String BRACKET_OPEN = "{";
    public static final String BRACKET_CLOSE = "}";
    public static final String SINGLE_QUOTE = "'";
    public static final String COMMA = ",";
    private static final String DEFAULT_FILTER_STRING = "{'filter':['>',{'field':'id'}, 0]}";
    public static final String FILTER_STRING = "'filter':";
    private final static String MODULE_STRING = "module";
    public static final String FIELD = "field";
    public static final String COLON = ":";
    public static final String MATCH = "=";
    public static final String NOT_MATCH = "<>";
    public static final String WILDCARD = "*";
    private static final String LINE_REGEXP = "\\r?\\n";

    private final String CONFIG_FOLDER_CONTACTS_STRING = "config/folder/contacts";
    private final String CONTACTS_STRING = "contacts";
    private final String MULTIPLE_STRING = "multiple";
    private final String IMAGE_STRING = "image/contact/picture";
    private final String NEW_STRING = "new";
    private final String DELETE_STRING = "delete";
    private final String GET_STRING = "get";
    private final String UPDATE_STRING = "update";
    private final String SEARCH_STRING = "advancedSearch";

    /**
     * Constructor.
     * 
     * @param baseURL - base url of mxos
     */
    public OXAddressBookCRUD(String baseURL) {
        webResource = getWebResource(baseURL);
    }

    @Override
    public String createContact(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(NEW_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));
        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.session.name(), paramList2);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList3);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList4);

        try {

            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;
            String jsonString = JsonToAddressBookMapper.mapFromContact(
                    attributes, getFolderId(params));
            final ClientResponse r = put(subUrl, paramsNew, jsonString);
            ContactBase contactBase = new ContactResponse().getContactBase(r);
            return contactBase.getContactId();
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void deleteContact(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);

        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList2);
        paramsNew.put(OXContactsProperty.session.name(), paramList3);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList4);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList5);

        try {

            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String folderId = getFolderId(params);
            if (folderId != null && folderId.equalsIgnoreCase(USER_FOLDER_ID)) {
                throw new AddressBookException(
                        "The contact with user details cannot be deleted");
            }

            String jsonString = JsonToAddressBookMapper.mapFromContactBase(
                    attributes, folderId);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    private String getFolderId(Map<String, List<String>> params) {
        String folderId = MxOSConstants.OX_DEFAULT_CONTACTS_FOLDER_ID;
        if (params.get(AddressBookProperty.folderId.name()) != null
                && !params.get(AddressBookProperty.folderId.name()).get(0)
                        .isEmpty()) {
            folderId = params.get(AddressBookProperty.folderId.name()).get(0);
        }
        return folderId;
    }

    @Override
    public ContactBase readContactBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactBase(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public Integer readContactFolderId(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONFIG_FOLDER_CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add("UTC");

        paramsNew.put(OXContactsProperty.session.name(), paramList1);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList2);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList3);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getFolderId(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public Name readContactName(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactName(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public byte[] readContactsActualImage(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        final String subUrl = IMAGE_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactActualImage(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public Image readContactsImage(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            Image image = new ContactResponse().getContactImage(r);
            String imageUrl = image.getImageUrl();
            if (null != imageUrl) {
                imageUrl = imageUrl.replace(AddressBookProperty.userId.name(),
                        params.get(AddressBookProperty.userId.name()).get(0));
                image.setImageUrl(imageUrl);
            }
            return image;
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public PersonalInfo readContactsPersonalInfo(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactsPersonalInfo(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public List<Event> readContactPersonalInfoEvents(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactsPersonalInfoEvents(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public Address readContactsPersonalInfoAddress(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactsPersonalInfoAddress(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public Communication readContactsPersonalInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse()
                    .getContactsPersonalInfoCommunication(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public WorkInfo readContactsWorkInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactsWorkInfo(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public Address readContactsWorkInfoAddress(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactsWorkInfoAddress(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public Communication readContactsWorkInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getContactsWorkInfoCommunication(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void rollback() throws ApplicationException {
        // Not supported for OX
    }

    @Override
    public void updateContactBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper.mapFromContactBase(
                    attributes, null);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void updateContactName(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactName(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void updateContactsImage(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        String actualImage = params.get(AddressBookProperty.actualImage.name())
                .get(0);

        try {

            ClientResponse resp;
            if (actualImage == null || actualImage.equals("")) {
                // delete image
                resp = put(subUrl, paramsNew, "{\"image1\":\"\"}");
                Response.validateResponse(resp);
            } else {
                final List<String> paramList = new ArrayList<String>();
                paramList.add("{}");
                params.put(OXContactsProperty.json.name(), paramList);
                resp = postMultipart(subUrl, paramsNew, params);
                Response.validateUploadResponse(resp);
            }
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    /**
     * API to update Contacts Personal Info details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * @throws IOException
     */
    @Override
    public void updateContactsPersonalInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactPersonalInfo(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    /**
     * API to update Contacts Personal Info Address details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * @throws IOException
     */
    @Override
    public void updateContactsPersonalInfoAddress(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactPersonalInfoAddress(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    /**
     * API to update Contacts Personal Info Communication details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * @throws IOException
     */
    @Override
    public void updateContactsPersonalInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactPersonalInfoCommunication(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    /**
     * API to update Contacts Work Info details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    @Override
    public void updateContactsWorkInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactWorkInfo(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    /**
     * API to update Contacts Work Info Address details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    @Override
    public void updateContactsWorkInfoAddress(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactWorkInfoAddress(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    /**
     * API to update Contacts Work Info Communication details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    @Override
    public void updateContactsWorkInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactWorkInfoCommunication(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void updateContactsPersonalInfoEvent(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.contactId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {

            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper
                    .mapFromContactPersonalInfoEvent(attributes);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public GroupBase readGroupsBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getGroupBase(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void updateGroupsBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper.mapFromGroupsBase(
                    attributes, null);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public List<Member> readGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getAllGroupMembers(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void deleteGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;
            List<Member> memberList = (List<Member>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.allMembers);

            List<ExternalMember> exMemberList = (List<ExternalMember>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allExternalMembers);

            String memberId = attributes.get(OXContactsProperty.id.name());

            boolean memberExists = false;

            for (Member mem : memberList) {
                if (mem.getMemberId().equalsIgnoreCase(memberId)) {
                    memberExists = true;
                }
            }

            if (!memberExists) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(), "");
            }

            String jsonString = JsonToAddressBookMapper.mapFromGroupMembers(
                    attributes, null, memberList, exMemberList,
                    Action.DELETE_GROUP_MEMBER);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void deleteAllGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            List<Member> memberList = (List<Member>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.allMembers);

            List<ExternalMember> exMemberList = (List<ExternalMember>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allExternalMembers);

            String jsonString = JsonToAddressBookMapper.mapFromGroupMembers(
                    attributes, null, memberList, exMemberList,
                    Action.DELETE_ALL_GROUP_MEMBER);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public String createGroupMembers(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;
            List<Member> memberList = (List<Member>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.allMembers);

            List<ExternalMember> extMemberList = (List<ExternalMember>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allExternalMembers);

            List<ContactBase> contactsList = (List<ContactBase>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allContactBase);

            String memberId = attributes.get(OXContactsProperty.id.name());
            String memberEmail = attributes.get(OXContactsProperty.mail.name());

            boolean contactExist = false;

            // Check if the memberId is a valid contact Id
            for (ContactBase base : contactsList) {
                if (base.getContactId().equalsIgnoreCase(memberId)) {
                    contactExist = true;
                }
            }
            if (!contactExist) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(), "");
            }

            // Check if the memberId already exists
            for (Member mem : memberList) {
                if (mem.getMemberId().equalsIgnoreCase(memberId)) {
                    if (mem.getEmail() != null
                            && (mem.getEmail().equalsIgnoreCase(memberEmail) || (memberEmail == null && mem
                                    .getEmail().equalsIgnoreCase(""))))
                        throw new AddressBookException(
                                AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                        .name(),
                                ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                                AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                        .name(), "");
                }
            }

            String jsonString = JsonToAddressBookMapper.mapFromGroupMembers(
                    attributes, null, memberList, extMemberList,
                    Action.ADD_GROUP_MEMBER);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
            return null;
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public List<ExternalMember> readGroupsExternalMember(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new ContactResponse().getAllGroupExternalMembers(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void deleteGroupsExternalMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            List<Member> memberList = (List<Member>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.allMembers);

            List<ExternalMember> exMemberList = (List<ExternalMember>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allExternalMembers);

            String memberName = attributes.get(OXContactsProperty.display_name
                    .name());

            boolean memberExists = false;

            for (ExternalMember mem : exMemberList) {
                if (mem.getMemberName().equalsIgnoreCase(memberName)) {
                    memberExists = true;
                }
            }
            if (!memberExists) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(), "");
            }

            String jsonString = JsonToAddressBookMapper.mapFromGroupMembers(
                    attributes, null, memberList, exMemberList,
                    Action.DELETE_EXTERNAL_GROUP_MEMBER);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void deleteAllGroupsExternalMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            List<Member> memberList = (List<Member>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.allMembers);

            List<ExternalMember> exMemberList = (List<ExternalMember>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allExternalMembers);

            String jsonString = JsonToAddressBookMapper.mapFromGroupMembers(
                    attributes, null, memberList, exMemberList,
                    Action.DELETE_ALL_EXTERNAL_GROUP_MEMBER);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public String createGroupExternalMembers(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.groupId.name()).get(0));

        // Add timestamp
        final List<String> paramList4 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList4.add(updateTime.toString());

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList4);
        paramsNew.put(OXContactsProperty.session.name(), paramList5);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList7);

        try {
            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            List<Member> memberList = (List<Member>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.allMembers);

            List<ExternalMember> exMemberList = (List<ExternalMember>) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allExternalMembers);

            String memberEmail = attributes.get(OXContactsProperty.mail.name());
            String memberName = attributes.get(OXContactsProperty.display_name
                    .name());

            for (ExternalMember mem : exMemberList) {
                if (mem.getMemberEmail().equalsIgnoreCase(memberEmail)
                        && mem.getMemberName().equalsIgnoreCase(memberName)) {
                    throw new AddressBookException(
                            AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                    .name(),
                            ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                            AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                    .name(), "");
                }
            }

            String jsonString = JsonToAddressBookMapper.mapFromGroupMembers(
                    attributes, null, memberList, exMemberList,
                    Action.ADD_EXTERNAL_GROUP_MEMBER);

            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
            return null;
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public String createGroup(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(NEW_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.session.name(), paramList2);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList3);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList4);

        try {

            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper.mapFromGroup(
                    attributes, getFolderId(params));

            final ClientResponse r = put(subUrl, paramsNew, jsonString);
            GroupBase groupBase = new ContactResponse().getGroupsBase(r);
            return groupBase.getGroupId();
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }

    }

    @Override
    public void deleteGroup(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);

        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList2);
        paramsNew.put(OXContactsProperty.session.name(), paramList3);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList4);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList5);

        try {

            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            String jsonString = JsonToAddressBookMapper.mapFromGroupsBase(
                    attributes, getFolderId(params));

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void deleteAllGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);

        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList2);
        paramsNew.put(OXContactsProperty.session.name(), paramList3);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList4);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList5);

        try {

            Map<String, String> attributes = ((OXAddressBookBackendState) mxosRequestState
                    .getBackendState().get(AddressBookDBTypes.ox.name())).attributes;

            List<GroupBase> groupBaseList = readAllGroupBase(mxosRequestState);

            String jsonString = JsonToAddressBookMapper.mapFromAllGroupsBase(
                    attributes, getFolderId(params), groupBaseList);

            put(subUrl, paramsNew, jsonString);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public List<GroupBase> readAllGroupBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(SEARCH_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();

        paramList6.add(GROUP_BASE_COLUMNS);

        final List<String> paramList9 = new ArrayList<String>();
        paramList9.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.columns.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList9);

        String sortKey = params.get(AddressBookProperty.sortKey.name()) != null ? params
                .get(AddressBookProperty.sortKey.name()).get(0) : null;

        if (sortKey != null) {
            final List<String> paramList7 = new ArrayList<String>();
            paramList7.add(OXAddressBookUtil.AddressBookProperty_Id
                    .get(sortKey));
            paramsNew.put(OXContactsProperty.sort.name(), paramList7);

        }

        String sortOrder = params.get(AddressBookProperty.sortOrder.name()) != null ? params
                .get(AddressBookProperty.sortOrder.name()).get(0) : null;

        if (sortOrder != null) {
            final List<String> paramList8 = new ArrayList<String>();
            if (SortOrder.ascending.name().equals(sortOrder))
                paramList8.add(SortOrder.ascending.convert());
            else
                paramList8.add(SortOrder.descending.convert());
            paramsNew.put(OXContactsProperty.order.name(), paramList8);
        }

        String queryString = params.get(AddressBookProperty.query.name()) != null ? params
                .get(AddressBookProperty.query.name()).get(0) : null;

        String filterString = DEFAULT_FILTER_STRING;
        if (queryString != null && queryString.length() > 0) {
            Query query = new Query();
            query.parse(queryString);

            filterString = createFilterString(query);
        }

        try {
            final ClientResponse r = put(subUrl, paramsNew, filterString);
            return new ContactResponse().getAllGroupBase(r);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public List<ContactBase> readAllContactBase(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(SEARCH_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();

        paramList6.add(GROUP_NOTE_COLUMNS);

        final List<String> paramList9 = new ArrayList<String>();
        paramList9.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.columns.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList9);

        String sortKey = params.get(AddressBookProperty.sortKey.name()) != null ? params
                .get(AddressBookProperty.sortKey.name()).get(0) : null;

        if (sortKey != null) {
            final List<String> paramList7 = new ArrayList<String>();
            paramList7.add(OXAddressBookUtil.AddressBookProperty_Id
                    .get(sortKey));
            paramsNew.put(OXContactsProperty.sort.name(), paramList7);

        }

        String sortOrder = params.get(AddressBookProperty.sortOrder.name()) != null ? params
                .get(AddressBookProperty.sortOrder.name()).get(0) : null;

        if (sortOrder != null) {
            final List<String> paramList8 = new ArrayList<String>();
            if (SortOrder.ascending.name().equals(sortOrder))
                paramList8.add(SortOrder.ascending.convert());
            else
                paramList8.add(SortOrder.descending.convert());
            paramsNew.put(OXContactsProperty.order.name(), paramList8);
        }

        String queryString = params.get(AddressBookProperty.query.name()) != null ? params
                .get(AddressBookProperty.query.name()).get(0) : null;

        String filterString = DEFAULT_FILTER_STRING;
        if (queryString != null && queryString.length() > 0) {
            Query query = new Query();
            query.parse(queryString);

            filterString = createFilterString(query);
        }

        try {
            final ClientResponse r = put(subUrl, paramsNew, filterString);
            return new ContactResponse().getAllContactBase(r);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void deleteAddressBook(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        List<ContactBase> contactBaseList = readAllContactBase(mxosRequestState);
        List<GroupBase> groupBaseList = readAllGroupBase(mxosRequestState);

        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);

        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.timestamp.name(), paramList2);
        paramsNew.put(OXContactsProperty.session.name(), paramList3);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList4);

        ObjectMapper objMapper = new ObjectMapper();
        ArrayNode arrayNode = objMapper.createArrayNode();
        try {
            for (ContactBase contact : contactBaseList) {

                String folderId = contact.getFolderId();

                // Shud not delete the contacts with user details
                if (folderId != null
                        && folderId.equalsIgnoreCase(USER_FOLDER_ID)) {
                    continue;
                }

                JsonToAddressBookMapper.mapFromDeleteAddressBook(
                        contact.getContactId(), folderId, arrayNode, objMapper);
            }
            for (GroupBase group : groupBaseList) {
                JsonToAddressBookMapper.mapFromDeleteAddressBook(
                        group.getGroupId(), group.getFolderId(), arrayNode,
                        objMapper);
            }
            final ClientResponse r = put(subUrl, paramsNew,
                    objMapper.writeValueAsString(arrayNode));
            Response.validateResponse(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public List<String> createMultipleContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();

        String input = inputParam.get(AddressBookProperty.contactsList.name())
                .get(0);
        int size = input.split(LINE_REGEXP).length;

        List<Map<String, String>> attributesList = ((OXAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.ox.name())).attributesList;
        final String subUrl = MULTIPLE_STRING;

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(inputParam.get(AddressBookProperty.sessionId.name())
                .get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(inputParam.get(AddressBookProperty.cookieString.name())
                .get(0));

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();
        paramsNew.put(OXContactsProperty.session.name(), paramList2);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList3);

        ObjectMapper objMapper = new ObjectMapper();
        ArrayNode arrNode = objMapper.createArrayNode();

        List<String> contactIds = new ArrayList<String>();

        try {
            for (int i = 0; i < size; i++) {
                Map<String, String> attribute = attributesList.get(i);

                JsonToAddressBookMapper.mapFromMultipleContact(attribute,
                        getFolderId(inputParam), arrNode, objMapper);
            }

            final ClientResponse r = put(subUrl, paramsNew,
                    objMapper.writeValueAsString(arrNode));
            new ContactResponse().getMultipleContactId(r, contactIds);

        } catch (final AddressBookException e) {
            deleteMultipleContacts(mxosRequestState, contactIds);
            throw new AddressBookException(e);
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }

        return contactIds;
    }

    private void deleteMultipleContacts(MxOSRequestState mxosRequestState,
            List<String> contactIds) throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();

        final String subUrl = MULTIPLE_STRING;

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(inputParam.get(AddressBookProperty.sessionId.name())
                .get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(inputParam.get(AddressBookProperty.cookieString.name())
                .get(0));

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();
        paramsNew.put(OXContactsProperty.session.name(), paramList2);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList3);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        ArrayNode arrNode = mapper.createArrayNode();

        try {
            for (String id : contactIds) {
                JsonToAddressBookMapper.mapFromMultipleDeleteContact(id,
                        getFolderId(inputParam), arrNode, mapper);
            }
            final ClientResponse r = put(subUrl, paramsNew,
                    mapper.writeValueAsString(arrNode));
            Response.validateResponse(r);
        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }

    }

    @Override
    public List<String> createMultipleGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();

        String input = inputParam.get(AddressBookProperty.groupsList.name())
                .get(0);
        int size = input.split(LINE_REGEXP).length;

        List<Map<String, String>> attributesList = ((OXAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.ox.name())).attributesList;
        final String subUrl = MULTIPLE_STRING;

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(inputParam.get(AddressBookProperty.sessionId.name())
                .get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(inputParam.get(AddressBookProperty.cookieString.name())
                .get(0));

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();
        paramsNew.put(OXContactsProperty.session.name(), paramList2);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList3);

        List<String> groupIds = new ArrayList<String>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        ArrayNode arrNode = mapper.createArrayNode();

        try {
            for (int i = 0; i < size; i++) {
                Map<String, String> attribute = attributesList.get(i);
                JsonToAddressBookMapper.mapFromMultipleGroup(attribute,
                        getFolderId(inputParam), arrNode, mapper);
            }
            final ClientResponse r = put(subUrl, paramsNew,
                    mapper.writeValueAsString(arrNode));
            new ContactResponse().getMultipleGroupId(r, groupIds);

        } catch (final AddressBookException e) {
            deleteMultipleContacts(mxosRequestState, groupIds);
            throw new AddressBookException(e);
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }

        return groupIds;
    }

    @Override
    public List<Contact> readAllContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = CONTACTS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(SEARCH_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList6 = new ArrayList<String>();

        paramList6.add(IS_GROUP_COLUMN + MxOSConstants.COMMA
                + CONTACT_NAME_COLUMNS + MxOSConstants.COMMA
                + CONTACT_BASE_COLUMNS + MxOSConstants.COMMA
                + CONTACT_PERSONALINFO_COLUMNS + MxOSConstants.COMMA
                + CONTACT_PERSONALINFO_ADDRESS_COLUMNS + MxOSConstants.COMMA
                + CONTACT_PERSONALINFO_COMMUNICATION_COLUMNS
                + MxOSConstants.COMMA + CONTACT_PERSONALINFO_EVENT_COLUMNS
                + MxOSConstants.COMMA + CONTACT_WORKINFO_COLUMNS
                + MxOSConstants.COMMA + CONTACT_WORKINFO_ADDRESS_COLUMNS
                + MxOSConstants.COMMA + CONTACT_WORKINFO_COMMUNICATION_COLUMNS);

        final List<String> paramList9 = new ArrayList<String>();
        paramList9.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.folder.name(), paramList2);
        paramsNew.put(OXContactsProperty.session.name(), paramList4);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList5);
        paramsNew.put(OXContactsProperty.columns.name(), paramList6);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList9);

        String sortKey = params.get(AddressBookProperty.sortKey.name()) != null ? params
                .get(AddressBookProperty.sortKey.name()).get(0) : null;

        if (sortKey != null) {
            final List<String> paramList7 = new ArrayList<String>();
            paramList7.add(OXAddressBookUtil.AddressBookProperty_Id
                    .get(sortKey));
            paramsNew.put(OXContactsProperty.sort.name(), paramList7);

        }

        String sortOrder = params.get(AddressBookProperty.sortOrder.name()) != null ? params
                .get(AddressBookProperty.sortOrder.name()).get(0) : null;

        if (sortOrder != null) {
            final List<String> paramList8 = new ArrayList<String>();
            if (SortOrder.ascending.name().equals(sortOrder))
                paramList8.add(SortOrder.ascending.convert());
            else
                paramList8.add(SortOrder.descending.convert());
            paramsNew.put(OXContactsProperty.order.name(), paramList8);
        }

        String queryString = params.get(AddressBookProperty.query.name()) != null ? params
                .get(AddressBookProperty.query.name()).get(0) : null;

        String filterString = DEFAULT_FILTER_STRING;
        if (queryString != null && queryString.length() > 0) {
            Query query = new Query();
            query.parse(queryString);

            filterString = createFilterString(query);
        }

        try {
            final ClientResponse r = put(subUrl, paramsNew, filterString);
            return new ContactResponse().getAllContacts(r);

        } catch (final AddressBookException e) {
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    private String createFilterString(Query query) {
        StringBuilder filterString = new StringBuilder();

        List<Visitor> visitorList = query.getVisitors();
        List<Condition> conditionList = query.getConditions();

        Iterator<Visitor> vIter = visitorList.iterator();
        Iterator<Condition> conIter = conditionList.iterator();

        StringBuilder term1String = new StringBuilder();

        // Get the 1st term
        if (vIter.hasNext()) {
            term1String.append(SQR_BRCKET_OPEN);
            getPrefixFilter((Term) vIter.next(), term1String);
        }

        // Append the 1st term to filter string
        filterString.append(term1String);

        while (conIter.hasNext()) {

            // Get the Condition
            Condition condition = conIter.next();
            StringBuilder conditionString = new StringBuilder();
            conditionString.append(SQR_BRCKET_OPEN);

            conditionString.append(SINGLE_QUOTE).append(condition.name())
                    .append(SINGLE_QUOTE).append(COMMA);

            // Prepend the condition to filter string
            filterString.insert(0, conditionString.toString());

            // Get the 2nd Term
            StringBuilder term2String = new StringBuilder().append(COMMA)
                    .append(SQR_BRCKET_OPEN);
            getPrefixFilter((Term) vIter.next(), term2String);
            term2String.append(SQR_BRCKET_CLOSE);

            // Append the 2nd term to filter string
            filterString.append(term2String);
        }
        // Prepend the default filter string
        filterString.insert(0, BRACKET_OPEN + FILTER_STRING);
        filterString.append(BRACKET_CLOSE);

        return filterString.toString();
    }

    private void getPrefixFilter(Term term, StringBuilder jsonString) {
        switch (term.getOperator()) {
        case CONTAIN:
        case STARTS_WITH:
        case ENDS_WITH:
        case MATCH:
            jsonString.append(SINGLE_QUOTE).append(MATCH).append(SINGLE_QUOTE)
                    .append(COMMA);
            break;
        case NOT_CONTAIN:
        case NOT_STARTS_WITH:
        case NOT_ENDS_WITH:
        case NOT_MATCH:
            jsonString.append(SINGLE_QUOTE).append(NOT_MATCH)
                    .append(SINGLE_QUOTE).append(COMMA);
            break;
        case LESS_THAN:
        case GREATER_THAN:
        case NOT_LESS_THAN:
        case NOT_GREATER_THAN:
        default:
            jsonString.append(SINGLE_QUOTE)
                    .append(term.getOperator().convert()).append(SINGLE_QUOTE)
                    .append(COMMA);
        }

        jsonString.append(BRACKET_OPEN).append(SINGLE_QUOTE).append(FIELD)
                .append(SINGLE_QUOTE).append(COLON);

        jsonString.append(SINGLE_QUOTE).append(term.getOperand())
                .append(SINGLE_QUOTE).append(BRACKET_CLOSE).append(COMMA);

        if (term.getOperator().name().equals(Term.Operator.CONTAIN.name())) {
            jsonString.append(SINGLE_QUOTE).append(WILDCARD)
                    .append(term.getValue()).append(WILDCARD)
                    .append(SINGLE_QUOTE);
        } else if (term.getOperator().name()
                .equals(Term.Operator.STARTS_WITH.name())) {
            jsonString.append(SINGLE_QUOTE).append(term.getValue())
                    .append(WILDCARD).append(SINGLE_QUOTE);
        } else if (term.getOperator().name()
                .equals(Term.Operator.ENDS_WITH.name())) {
            jsonString.append(SINGLE_QUOTE).append(WILDCARD)
                    .append(term.getValue()).append(SINGLE_QUOTE);
        } else {
            jsonString.append(SINGLE_QUOTE).append(term.getValue())
                    .append(SINGLE_QUOTE);
        }

        jsonString.append(SQR_BRCKET_CLOSE);
    }

    @Override
    public void moveMultipleContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = MULTIPLE_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add("UTC");

        paramsNew.put(OXContactsProperty.session.name(), paramList1);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList2);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList3);

        String folderId = params.get(AddressBookProperty.folderId.name())
                .get(0);
        Long updateTime = System.nanoTime();
        String moveToFolderId = params.get(
                AddressBookProperty.moveToFolderId.name()).get(0);

        JsonNode jsonNodes = null;
        try {
            final ClientResponse resp = put(
                    subUrl,
                    paramsNew,
                    generateJsonString(params, folderId, updateTime,
                            moveToFolderId));
            jsonNodes = Response.getMultipleJsonTree(resp);
            Response.validateMultipleResponse(jsonNodes);

        } catch (final AddressBookException e) {

            if (Response.getMultipleErrorCount(jsonNodes) < params.get(
                    AddressBookProperty.contactId.name()).size()) {
                // rollback since the multiple move has failed
                try {
                    put(subUrl,
                            paramsNew,
                            generateJsonString(params, moveToFolderId,
                                    updateTime, folderId));
                } catch (MxOSException ex) {
                    // log rollback exception to the stack trace and add it as
                    // sibling exception
                    logger.error("Rollback for contacts move failed");
                    // e.addSuppressed(ex);
                }
            }
            throw e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    @Override
    public void moveMultipleGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        final String subUrl = MULTIPLE_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add("UTC");

        paramsNew.put(OXContactsProperty.session.name(), paramList1);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList2);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList3);

        String folderId = params.get(AddressBookProperty.folderId.name())
                .get(0);
        Long updateTime = System.nanoTime();
        String moveToFolderId = params.get(
                AddressBookProperty.moveToFolderId.name()).get(0);

        JsonNode jsonNodes = null;
        try {
            final ClientResponse resp = put(
                    subUrl,
                    paramsNew,
                    generateJsonString(params, folderId, updateTime,
                            moveToFolderId));
            jsonNodes = Response.getMultipleJsonTree(resp);
            Response.validateMultipleResponse(jsonNodes);

        } catch (final AddressBookException e) {

            if (Response.getMultipleErrorCount(jsonNodes) < params.get(
                    AddressBookProperty.groupId.name()).size()) {
                // rollback since the multiple move has failed
                try {
                    put(subUrl,
                            paramsNew,
                            generateJsonString(params, moveToFolderId,
                                    updateTime, folderId));
                } catch (MxOSException ex) {
                    // log rollback exception to the stack trace and add it as
                    // sibling exception
                    logger.error("Rollback for contacts move failed");
                    // e.addSuppressed(ex);
                }
            }
            throw (AddressBookException) e;
        } catch (final Exception e) {
            throw new AddressBookException(e);
        }
    }

    private String generateJsonString(Map<String, List<String>> params,
            String folderId, Long updateTime, String moveToFolderId) {
        StringBuilder jsonString = new StringBuilder().append("[");

        List<String> list = new ArrayList<String>();
        if (params.get(AddressBookProperty.contactId.name()) != null) {
            list = params.get(AddressBookProperty.contactId.name());
        } else if (params.get(AddressBookProperty.groupId.name()) != null) {
            list = params.get(AddressBookProperty.groupId.name());
        }

        for (int i = 0; i < list.size(); i++) {
            String contactId = null;
            if (params.get(AddressBookProperty.contactId.name()) != null) {
                contactId = params.get(AddressBookProperty.contactId.name())
                        .get(i);
            } else if (params.get(AddressBookProperty.groupId.name()) != null) {

                contactId = params.get(AddressBookProperty.groupId.name()).get(
                        i);

            }
            if (i != 0) {
                jsonString.append(",");
            }
            jsonString.append("{\"").append(ACTION_STRING).append("\":\"")
                    .append(UPDATE_STRING).append("\"").append(",\"")
                    .append(OXContactsProperty.folder.name()).append("\":")
                    .append(folderId).append(",\"")
                    .append(OXContactsProperty.timestamp.name()).append("\":")
                    .append(updateTime.toString()).append(",\"")
                    .append(MODULE_STRING).append("\":\"")
                    .append(CONTACTS_STRING).append("\"").append(",\"")
                    .append(OXContactsProperty.id.name()).append("\":")
                    .append(contactId).append(",\"")
                    .append(OXContactsProperty.data.name()).append("\":{\"")
                    .append(OXContactsProperty.folder_id.name())
                    .append("\":\"").append(moveToFolderId).append("\"}}");
        }
        jsonString.append("]");
        return jsonString.toString();
    }

    public ExternalSession login(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        try {
            return loginOX(mxosRequestState);
        } catch (ComponentException e) {
            throw new AddressBookException(e);
        }
    }

    public void logout(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        try {
            logoutOX(mxosRequestState);
        } catch (ComponentException e) {
            throw new AddressBookException(e);
        }
    }

    public void validateUser(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        try {
            validateUserOX(mxosRequestState);
        } catch (ComponentException e) {
            throw new AddressBookException(e);
        }
    }

    private ClientResponse get(final String subURL,
            final Map<String, List<String>> params) throws MxOSException,
            AddressBookException {
        try {
            return getOX(subURL, params);
        } catch (ComponentException e) {
            throw (AddressBookException) e;
        }
    }

    private ClientResponse postMultipart(final String subURL,
            final Map<String, List<String>> queryParams,
            final Map<String, List<String>> params) throws MxOSException,
            AddressBookException, IOException {
        try {
            return postMultipartOX(subURL, queryParams, params);
        } catch (ComponentException e) {
            throw (AddressBookException) e;
        }
    }

    private ClientResponse put(final String subURL,
            final Map<String, List<String>> params, final String jsonString)
            throws MxOSException, AddressBookException {
        try {
            return putOX(subURL, params, jsonString);
        } catch (ComponentException e) {
            throw (AddressBookException) e;
        }
    }

    @Override
    protected ExternalSession getSessionFromResponse(final ClientResponse r)
            throws ComponentException {
        return new LoginResponse().getSession(r);
    }

    @Override
    protected String getUserIdFromResponse(ClientResponse resp)
            throws ComponentException {
        return new UserResponse().getUserId(resp);
    }
}
