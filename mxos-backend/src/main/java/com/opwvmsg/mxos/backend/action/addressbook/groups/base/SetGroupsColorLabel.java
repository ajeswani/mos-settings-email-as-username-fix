/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.addressbook.groups.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set colorLabel attribute of Groups Base object.
 * 
 * @author mxos-dev
 * 
 */

public class SetGroupsColorLabel implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetGroupsColorLabel.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetGroupsColorLabel action start."));
        }

        if (mxosRequestState.getInputParams().get(
                AddressBookProperty.colorLabel.name()) != null) {
            if (mxosRequestState.getInputParams().containsKey(
                    AddressBookProperty.groupsList.name())) {
                Object[] name = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.colorLabel.name()).toArray();

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttributes(mxosRequestState,
                                AddressBookProperty.colorLabel, name);
            } else {
                String name = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.colorLabel.name()).get(0);

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.colorLabel, name);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetGroupsColorLabel action end."));
        }
    }
}
