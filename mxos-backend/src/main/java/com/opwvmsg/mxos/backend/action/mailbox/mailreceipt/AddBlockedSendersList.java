/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to add blocked senders list.
 *
 * @author mxos-dev
 */
public class AddBlockedSendersList implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(AddBlockedSendersList.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddBlockedSendersList action start."));
        }
        try {
            final List<String> newBlockedSenders = requestState.getInputParams()
                    .get(MailboxProperty.blockedSender.name());

            List<String> blockedSendersList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.blockedSendersList);
            if (null == blockedSendersList ) {
                blockedSendersList = new ArrayList<String>();
            }
            if ((blockedSendersList.size() + newBlockedSenders.size()) > MxOSConfig
                    .getMaxBlockedSendersList()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_BLOCKED_SENDERS_REACHED_MAX_LIMIT
                                .name());
            }
            for (String blockedSender : newBlockedSenders) {
                if (!blockedSendersList.contains(blockedSender.toLowerCase())) {
                    blockedSendersList.add(blockedSender.toLowerCase());
                }
            }
            final String[] blockedSendersListToArray = blockedSendersList
                    .toArray(new String[blockedSendersList.size()]);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.blockedSendersList,
                            blockedSendersListToArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add blocked senders list.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_BLOCKED_SENDERS_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddBlockedSendersList action end."));
        }
    }
}
