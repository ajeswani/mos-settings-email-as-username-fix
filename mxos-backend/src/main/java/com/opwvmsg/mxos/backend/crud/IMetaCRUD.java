/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.crud;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;

/**
 * Interface to access metadata.
 * 
 * @author mxos-dev
 */
public interface IMetaCRUD extends ITransaction {

    /**
     * Method to create mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox create operation.
     * @throws MxOSException if any error.
     */
    void createMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to read mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox read operation.
     * @param base Mailbox Base POJO which will be populated with mailbox data.
     * @throws MxOSException if any error.
     */
    void readMailbox(final MxOSRequestState mxOSRequestStatus, final Base base)
            throws MxOSException;
    
    /**
     * Method to read mailbox access info.
     * 
     * @param mxosRequestState parameters for mailbox read operation.
     * @throws MxOSException if any error.
     */
    void readMailBoxAccessInfo(final MxOSRequestState mxOSRequestStatus)
            throws MxOSException;

    /**
     * Method to update mailbox access info.
     * 
     * @param mxosRequestState parameters for mailbox read operation.
     * @throws MxOSException if any error.
     */
    void updateMailBoxAccessInfo(MxOSRequestState mxosRequestState)
            throws MxOSException;
    
    /**
     * Method to update mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox update operation.
     * @throws MxOSException if any error.
     */
    void updateMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to hard delete mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox hard delete operation.
     * @throws MxOSException if any error.
     */
    void deleteMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to create folder metadata.
     * 
     * @param mxosRequestState parameters for folder create operation.
     * @param folderName name of the folder to be create
     * @param folderUUID string
     * @throws MxOSException Exception.
     */
    String createFolder(final MxOSRequestState mxosRequestState,
            final String folderName) throws MxOSException;

    /**
     * Method to read folder metadata.
     * 
     * @param mxosRequestState parameters for folder read operation.
     * @param folder Folder POJO to be filled.
     * @throws MxOSException Exception.
     */
    void readFolder(final MxOSRequestState mxosRequestState,
            final Folder folder) throws MxOSException;

    /**
     * Method to read all folders metadata in mailbox.
     * 
     * @param mxosRequestState parameters for read all folders operation.
     * @param folders List will be populated with each entry as folder POJO for
     *            each folder.
     * @throws MxOSException Exception.
     */
    void listFolders(final MxOSRequestState mxosRequestState,
            final List<Folder> folders) throws MxOSException;

    /**
     * Method to update folder metadata.
     * 
     * @param mxosRequestState parameters for update folder operation.
     * @throws MxOSException Exception.
     */
    void updateFolder(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to hard delete folder metadata.
     * 
     * @param mxosRequestState parameters for delete folder operation.
     * @throws MxOSException Exception.
     */
    void deleteFolder(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to create message metadata.
     * 
     * @param mxosRequestState
     * @param messageRecent
     * @return Message Id String
     * @throws MxOSException
     */
    String createMessage(final MxOSRequestState mxosRequestState,
            final boolean messageRecent) throws MxOSException;

    /**
     * Method to create message reference. In application there is no
     * transaction between message metadata and blob, message reference needs to
     * be created first to track orphans.
     * 
     * @param mxosRequestState parameters for create message ref operation.
     * @param sharingCount Message sharing count for de-dup purpose.
     * @throws MxOSException Exception.
     */
    void createMessageRef(final MxOSRequestState mxosRequestState,
            final int sharingCount) throws MxOSException;

    /**
     * Method to create message header.
     * 
     * @param mxosRequestState parameters for create message header operation.
     * @throws MxOSException Exception.
     */
    void createMessageHeader(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to read all message' metadata in folder.
     * 
     * @param mxosRequestState parameters this operation.
     * @param message populated with Message POJO.
     * @throws MxOSException Exception.
     */
    void readMessage(final MxOSRequestState mxosRequestState,
            final Message message) throws MxOSException;

    /**
     * Method to update message metadata.
     * 
     * @param mxosRequestState parameters this operation.
     * @param messageId message id.
     * @throws MxOSException Exception.
     */
    void updateMessage(final MxOSRequestState mxosRequestState,
            final String messageId) throws MxOSException;

    /**
     * Method to populate message flags.
     * 
     * @param mxosRequestState - parameters for message flag read.
     * @param messageId - Message id to read flags from.
     * @return
     * @throws MxOSException
     */
    Flags populateMessageFlags(MxOSRequestState mxosRequestState,
            String messageId) throws MxOSException;

    /**
     * Method to update messages flags.
     * 
     * @param mxosRequestState - parameters for message flag update.
     * @param String[] - Message ids to update flags to.
     * @return
     * @throws MxOSException
     */
    void updateMessageFlags(MxOSRequestState mxosRequestState, String[] messageId)
            throws MxOSException;

    /**
     * Method to hard delete message metadata.
     * 
     * @param mxosRequestState parameters this operation.
     * @param messageId message id.
     * @throws MxOSException Exception.
     */
    void deleteMessages(final MxOSRequestState mxosRequestState,
            final String[] messageIds) throws MxOSException;

    /**
     * Method to delete all the message from the given folder.
     * 
     * @param MxOSRequestState mxosRequestState
     * @throws MxOSException Exception.
     */
    public void deleteAllMessages(MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to copy message.
     * 
     * @param mxosRequestState parameters for copy message operation.
     * @param messageIds messageIds of messages that are to be copied.
     * @throws MxOSException Exception.
     */
    void copyMessages(final MxOSRequestState mxosRequestState,
            final String[] messageIds) throws MxOSException;

    /**
     * Method to copy all the messages.
     * 
     * @param mxosRequestState parameters for copy all messages operation.
     * @throws MxOSException Exception.
     */
    void copyAllMessages(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to move message.
     * 
     * @param mxosRequestState parameters for move message operation.
     * @param messageIds messageIds of messages that are to be moved.
     * @throws MxOSException Exception.
     */
    void moveMessages(final MxOSRequestState mxosRequestState,
            final String[] messageIds) throws MxOSException;

    /**
     * Method to move all the messages.
     * 
     * @param mxosRequestState parameters for move all messages operation.
     * @throws MxOSException Exception.
     */
    void moveAllMessages(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to read all message' Summary Header of the message
     * 
     * @param mxosRequestState parameters this operation.
     * @param messageMetaData MessageMetaData POJOs which will be populated
     * @param metadata MessageData
     * @throws MxOSException Exception.
     */
    void readMessageMetaData(
            final MxOSRequestState mxosRequestState,
            final String folderName,
            final Metadata metadata) throws MxOSException;
    
    /**
     * Method to read multiple message metadata of the message id's
     * 
     * @param mxosRequestState parameters this operation.
     * @param messageMetaData MessageMetaData POJOs which will be populated
     * @param metaMap MessageData MessageId mapped to Metadata
     * @throws MxOSException Exception.
     */
    void readMultiMessageMetaData(
            final MxOSRequestState mxosRequestState,
            final String folderName,
            final Map<String, Metadata> metaMap) throws MxOSException;

    /**
     * Method to update message metadata attributes.
     * 
     * @param mxosRequestState parameters this operation.
     * @throws MxOSException Exception.
     */
    void updateMessageMetaData(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to read all messages metadata in folder.
     * 
     * @param mxosRequestState parameters this operation.
     * @param folderName name of the folder
     * @param metadatas Map will be populated with MessageHeader POJO.
     * @throws MxOSException Exception.
     */
    void listMetadatasInFolder(
            final MxOSRequestState mxosRequestState,
            final String folderName,
            final Map<String, Metadata> metadatas) throws MxOSException;
    
    /**
     * Method to list all MessageUUIDs and UIDs for messages metadata inside
     * given folder.
     * 
     * @param mxosRequestState parameters this operation.
     * @param folderName name of the folder
     * @param metadatas Map will be populated with MessageUUIDs and UIDs.
     * @throws MxOSException Exception.
     */
    void listMetadatasUidsInFolder(final MxOSRequestState mxosRequestState,
            final String folderName, final Map<String, Metadata> metadatas)
            throws MxOSException;
    
    /**
     * Method to search sorted messages metadata in folder.
     * 
     * @param mxosRequestState parameters this operation.
     * @param folderName name of the folder
     * @param metadatas Map will be populated with MessageHeader POJO.
     * @throws MxOSException Exception.
     */
    void searchSortedMetadatasInFolder(
            final MxOSRequestState mxosRequestState,
            final String folderName,
            final Map<String, Map<String, Metadata>> metadatas)
            throws MxOSException;

    /**
     * Method to read all message' Header of the message.
     * 
     * @param mxosRequestState parameters this operation.
     * @param header POJO
     * @throws MxOSException Exception.
     */
    void readMessageHeader(
            final MxOSRequestState mxosRequestState,
            final Header header) throws MxOSException;

    /**
     * Method to read all messages headers in folder.
     * 
     * @param mxosRequestState parameters this operation.
     * @param messages List will be populated with MessageHeader POJO.
     * @throws MxOSException Exception.
     */
    void listHeadersInFolder(final MxOSRequestState mxosRequestState,
            final String folderName, Map<String, Header> messages)
            throws MxOSException;

    /**
     * Method to read all message' Body of the message.
     * 
     * @param mxosRequestState parameters this operation.
     * @param header Header POJOs which will be populated
     * @throws MxOSException Exception.
     */
    void readMessageBody(final MxOSRequestState mxosRequestState,
            final Body body) throws MxOSException;

    /**
     * Method to update mailbox last access time.
     * 
     * @param mxosRequestState parameters this operation.
     * @param lastAccessTime last access time.
     * @throws MxOSException Exception.
     */
    void updateMailboxLastAccessTime(final MxOSRequestState mxosRequestState,
            final long lastAccessTime) throws MxOSException;

    /**
     * Method to get mailbox last access time.
     * 
     * @param mxosRequestState
     * @return
     * @throws MxOSException
     */
    int getMailboxLastAccessTime(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to update message last access time.
     * 
     * @param mxosRequestState parameters this operation.
     * @param lastAccessTime last access time.
     * @throws MxOSException Exception.
     */
    void updateMessageLastAccessTime(final MxOSRequestState mxosRequestState,
            final long lastAccessTime) throws MxOSException;

    /**
     * Method to get MailboxInfo and FolderInfo object.
     * 
     * @param mxosRequestState parameters this operation.
     * @throws MxOSException Exception.
     */
    void readMailboxMetaInfo(final MxOSRequestState mxosRequestState)
            throws MxOSException;
    
    /**
     * Method to get all the message UID's for stateless list operation.
     * 
     * @param mxosRequestState parameters this operation.
     * @throws MxOSException Exception.
     */
    void listMessageUUIDs(final MxOSRequestState mxosRequestState)
            throws MxOSException, IntermailException;

    void updateFolderSubscribed(MxOSRequestState mxosRequestState)
            throws MxOSException;
    
    /**
     * Method to update message in mailbox.
     * 
     * @param mxosRequestState - parameters for message flag update.
     * @return
     * @throws MxOSException
     */
    void updateMessageBody(final MxOSRequestState mxosRequestState)
            throws MxOSException;
}
