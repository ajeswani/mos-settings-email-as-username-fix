/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.notify;

import static com.opwvmsg.mxos.data.enums.NotificationProperty.topic;
import static com.opwvmsg.mxos.data.enums.MxOSPOJOs.notify;
import static com.opwvmsg.mxos.data.enums.MxOSConstants.NOTIFY_MULTIMAP_ID;
import static com.opwvmsg.mxos.error.NotifyError.NTF_TOPIC_NOT_FOUND;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.notify.pojos.Notify;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;

/**
 * Action to read notify/topic
 * 
 * @author
 * 
 */
public class GetNotify implements MxOSBaseAction {

    private static Logger logger = Logger.getLogger(GetNotify.class);
    
    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetNotify action start.");
        }

        final String aTopic = requestState.getInputParams().get(topic.name())
                .get(0);

        final IDataStoreMultiMap<String, String> mMap = MxOSApp.getInstance()
                .getMultiMapDataStore();

        if (!mMap.containsKey(NOTIFY_MULTIMAP_ID, aTopic)) {
            /* given topic not found in DataStore */
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder("GetNotify failed. Topic ")
                        .append(aTopic).append(" not found."));
            }
            throw new NotFoundException(NTF_TOPIC_NOT_FOUND.name());
        }
        
        final List<String> subs = mMap.get(NOTIFY_MULTIMAP_ID, aTopic);

        final Notify aNotify = new Notify();
        aNotify.setTopic(aTopic);
        aNotify.setSubscriptions(subs);

        requestState.getDbPojoMap().setProperty(notify, aNotify);

        if (logger.isDebugEnabled()) {
            logger.debug("GetNotify action end.");
        }
    }

}
