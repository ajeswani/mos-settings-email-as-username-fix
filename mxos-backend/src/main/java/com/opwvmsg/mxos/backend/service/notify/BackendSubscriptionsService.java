/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.service.notify;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.notify.ISubscriptionsService;
import com.opwvmsg.mxos.utils.misc.Stats;

import static com.opwvmsg.mxos.interfaces.service.ServiceEnum.SubscriptionsService;
import static com.opwvmsg.mxos.interfaces.service.Operation.PUT;
import static com.opwvmsg.mxos.interfaces.service.Operation.GET;
import static com.opwvmsg.mxos.interfaces.service.Operation.DELETE;
import static com.opwvmsg.mxos.utils.misc.StatStatus.pass;
import static com.opwvmsg.mxos.utils.misc.StatStatus.fail;
import static com.opwvmsg.mxos.data.enums.MxOSPOJOs.subscriptions;

/**
 * Implements of {@link ISubscriptionsService} and inherits
 * {@link NotifySubsBaseBackend}
 * 
 * @author
 */
public class BackendSubscriptionsService implements ISubscriptionsService {
    
    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        final long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, SubscriptionsService, PUT);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(SubscriptionsService, PUT, t0, pass);
        } catch (MxOSException e) {
            Stats.stopTimer(SubscriptionsService, PUT, t0, fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        final long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, SubscriptionsService, GET);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(SubscriptionsService, GET, t0, pass);
            return (List<String>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(subscriptions);
        } catch (MxOSException e) {
            Stats.stopTimer(SubscriptionsService, GET, t0, fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        final long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, SubscriptionsService, DELETE);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(SubscriptionsService, DELETE, t0, pass);
        } catch (MxOSException e) {
            Stats.stopTimer(SubscriptionsService, DELETE, t0, fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        throw new UnsupportedOperationException(
                "UPDATE not supported for NotifySubscription.");
    }
    
}
