/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailstore;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set dataMap quota.
 * 
 * @author mxos-dev
 */
public class SetAccountQuotaFolder implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetAccountQuotaFolder.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetAccountQuotaFolder action start."));
        }
        try {
            final String newFolderQuota = requestState.getInputParams()
                    .get(MailboxProperty.folderQuota.name()).get(0);

            // validate folder quota string
            FolderQuotaValueMatcher matcher = new FolderQuotaValueMatcher();
            if (!matcher.isValid(newFolderQuota)) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_FOLDER_QUOTA.name());
            }

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.folderQuota,
                            newFolderQuota);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set account quota fodler.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_FOLDER_QUOTA.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetAccountQuotaFolder action end."));
        }
    }

    interface ValueMatcher {
        boolean isValid(String inputString);
    }

    static class FolderQuotaValueMatcher implements ValueMatcher {
        public enum FOLDER_QUOTA_ALLOWED_KEYWORDS {
            read, all, unread, firstread, type, priority, msgbytes, msgkb, msgmb, totbytes, totkb, totmb, msgcount, msgs, ageseconds, ageminutes, agehours, agedays, ageweeks, retrseconds, retrminutes, retrhours, retrdays, retrweeks
        }

        public boolean isValid(String folderQuota) {
            if (folderQuota == null || folderQuota.length() == 0)
                return true;

            String validateFolderQuota = folderQuota.toLowerCase();

            // Validation 1 : check if string contain at least one of the
            // allowed keywords
            for (FOLDER_QUOTA_ALLOWED_KEYWORDS keyword : FOLDER_QUOTA_ALLOWED_KEYWORDS
                    .values()) {
                if (validateFolderQuota.indexOf(keyword.name()) != -1)
                    return true;
            }
            // Validation 2 : check if string contain at least one of the
            // allowed keywords
            // TODO : Any other validations need to be added here as required
            return false;
        }
    }
}
