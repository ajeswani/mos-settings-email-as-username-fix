/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to search message meta data in a folder.
 *
 * @author mxos-dev
 */
public class ListMessagesMetaDataInFolder implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(ListMessagesMetaDataInFolder.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SearchMessagesMetaDataInFolder action start.");
        }
        
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {
            Map<String, Metadata> messagesMetaData =
                new LinkedHashMap<String, Metadata>();

            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();

            MessageServiceHelper.listMessageMetaDatasInFolder(metaCRUD,
                    requestState, messagesMetaData);

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageMetaData,
                    messagesMetaData);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while search messages meta data.", e);
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_SEARCH_MESSAGE_META_DATA.name(), e);
        } finally {
            try {
                if (metaCRUDPool != null && metaCRUD != null) {
                    metaCRUDPool.returnObject(metaCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SearchMessagesMetaDataInFolder action end.");
        }
    }
}
