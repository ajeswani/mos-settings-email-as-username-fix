/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get message metadata of a particular message.
 * 
 * @author mxos-dev
 */
public class GetMultiMessageMetaData implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(GetMultiMessageMetaData.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetMultiMessageMetaData action start.");
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            List<String> messageIdList = requestState.getInputParams().get(
                    MessageProperty.messageId.name());
            if (logger.isDebugEnabled()) {
                logger.debug("GetMessageMetaData action for multiple messages: "
                        + messageIdList);
            }
            final Map<String, Metadata> metaMap = MessageServiceHelper
                    .readMultiMessageMetadata(metaCRUD, requestState);

            final Long lastAccessTime = (Long) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.lastAccessTime);
            for (Entry<String, Metadata> entry : metaMap.entrySet()) {
                if (entry.getValue().getLastAccessedTime() == null
                        || entry.getValue().getLastAccessedTime().equals("")) {
                    entry.getValue().setLastAccessedTime(lastAccessTime);
                }
            }
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageMetaData,
                    metaMap);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get messages' metadata.", e);
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_META_DATA.name(), e);
        } finally {
            try {
                if (metaCRUDPool != null && metaCRUD != null) {
                    metaCRUDPool.returnObject(metaCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("GetMultiMessageMetaData action end.");
        }
    }
}
