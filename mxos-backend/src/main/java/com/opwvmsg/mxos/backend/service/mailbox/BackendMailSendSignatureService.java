/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendSignatureService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * MailSend Signature operations interface which will be exposed to
 * the client. This interface is responsible for doing Signature related
 * operations (like Read, Update etc.) directly in the database.
 * 
 * @author mxos-dev
 */
public class BackendMailSendSignatureService implements
        IMailSendSignatureService {

    private static Logger logger = Logger
            .getLogger(BackendMailSendSignatureService.class);

    /**
     * Default Constructor.
     */
    public BackendMailSendSignatureService() {
        logger.info("BackendMailSendSignatureService created...");
    }

    @Override
    public Long create(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailSendSignatureService,
                    Operation.PUT);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.PUT, t0, StatStatus.pass);
            return Long.parseLong(mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.signatureId));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.PUT, t0, StatStatus.fail);
            throw e;
        }

    }

    @Override
    public List<Signature> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailSendSignatureService,
                    Operation.GETALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.GETALL, t0, StatStatus.pass);
            return ((MailSend) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailSend)).getSignatures();
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.GETALL, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Signature readSignature(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailSendSignatureService,
                    Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.GET, t0, StatStatus.pass);
            return ((Signature) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.GET, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            if (inputParams == null || inputParams.size() <= 1) {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }

            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailSendSignatureService,
                    Operation.POST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.POST, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }

    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailSendSignatureService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.DELETE, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailSendSignatureService,
                    Operation.DELETE, t0, StatStatus.fail);
            throw e;
        }

    }

}
