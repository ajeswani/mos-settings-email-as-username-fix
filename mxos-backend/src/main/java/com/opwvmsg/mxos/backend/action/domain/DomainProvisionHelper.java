/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.domain;

import java.util.List;

import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Implementation class for Mailbox Provision.
 *
 * @author mxos-dev
 */
public class DomainProvisionHelper {

    /**
     * Helper method to create domain.
     *
     * @param provisionCURD provisionCURD
     * @param mxosRequestState mxosRequestState
     * @throws MxOSException Exception
     */
    public static void create(final IMailboxCRUD provisionCURD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        provisionCURD.createDomain(mxosRequestState);
    }

    /**
     * Helper Method to read domain.
     *
     * @param provisionCURD provisionCURD
     * @param domainName domainName
     * @return domain domain POJO
     * @throws MxOSException Exception
     */
    public static Domain read(final IMailboxCRUD provisionCURD,
            final String domainName) throws MxOSException {
        return provisionCURD.readDomain(domainName);
    }

    /**
     * Method to search Domains.
     *
     * @param provisionCURD provisionCURD
     * @param mxosRequestState mxosRequestState
     * @param searchMaxRows searchMaxRows
     * @param searchMaxTimeout searchMaxTimeout
     * @return List of Domain objects
     * @throws MxOSException Exception
     */
    public static List<Domain> search(final IMailboxCRUD provisionCURD,
            final MxOSRequestState mxosRequestState, final int searchMaxRows,
            final int searchMaxTimeout) throws MxOSException {
        return provisionCURD.searchDomain(mxosRequestState, searchMaxRows,
                searchMaxTimeout);
    }

    /**
     * Helper Method to update domain.
     *
     * @param provisionCURD provisionCURD
     * @param mxosRequestState mxosRequestState
     * @throws MxOSException Exception
     */
    public static void update(final IMailboxCRUD provisionCURD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        provisionCURD.updateDomain(mxosRequestState);
    }

    /**
     * Helper Method to delete Domain.
     *
     * @param provisionCURD provisionCURD
     * @param domainName domainName
     * @throws MxOSException Exception
     */
    public static void delete(final IMailboxCRUD provisionCURD,
            final String domainName) throws MxOSException {
        provisionCURD.deleteDomain(domainName);
    }
}
