/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.logging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxCustomProperty;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create mailbox via MAA.
 *
 * @author Aricent
 *
 */

public class CreateMailboxMaaLoggingAction extends MaaLoggingAction {
    private static Logger logger = Logger
            .getLogger(CreateMailboxMaaLoggingAction.class);

    /**
     * Action to make call to MAA.
     *
     * @param model
     *            model.
     * @throws Exception
     *             exception
     */
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Start : CreateMailboxMaaLoggingAction");
        }
        String userName = null;
        String domain = null;
        try {
            Map<String, String> params = new HashMap<String, String>();
            String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            if (email != null && !email.equals("")) {
                userName = ActionUtils.getUsernameFromEmail(email);
                domain = ActionUtils.getDomainFromEmail(email);
            }
            params.put(ARG1, userName);
            params.put(ARG2, domain);

            // Send the request parameter to MAA only if not null
            if (requestState.getInputParams().get(
                    MailboxProperty.maxNumAliases.name()) != null) {
                params.put(ARG3,
                        requestState.getInputParams()
                                .get(MailboxProperty.maxNumAliases.name())
                                .get(0));
            }

            if (requestState.getInputParams().get(
                    MailboxProperty.password.name()) != null) {
                params.put(ARG4,
                        requestState.getInputParams()
                                .get(MailboxProperty.password.name()).get(0));
            }

            if (requestState.getInputParams().get(
                    MailboxProperty.maxStorageSizeKB.name()) != null) {
                params.put(ARG5,
                        requestState.getInputParams()
                                .get(MailboxProperty.maxStorageSizeKB.name())
                                .get(0));
            }

            String customFields = requestState.getInputParams()
                    .get(MailboxProperty.customFields.name()).get(0);
            if (logger.isDebugEnabled()) {
                logger.debug("customFields :" + customFields);
            }
            Map<String, String> customAttributesMap = ActionUtils
                    .getMapFromString(requestState.getOperationName(),
                            customFields);
            String bgcFKey = customAttributesMap
                    .get(LDAPMailboxCustomProperty.bgcFKey.name());
            if (bgcFKey != null) {
                params.put(ARG6, bgcFKey);
            }

            if (requestState.getInputParams()
                    .get(MailboxProperty.locale.name()) != null) {
                params.put(ARG7,
                        requestState.getInputParams()
                                .get(MailboxProperty.locale.name()).get(0));
            }

            String bgcNewsPref = customAttributesMap
                    .get(LDAPMailboxCustomProperty.bgcNewsPref.name());
            if (bgcNewsPref != null) {
                params.put(ARG8, bgcNewsPref);
            }

            String bgcOptionsPriv = customAttributesMap.get("bgcOptionsPriv");
            if (bgcOptionsPriv != null) {
                params.put(ARG9, bgcOptionsPriv);
            }

            String bgcOptionsPref = customAttributesMap.get("bgcOptionsPref");
            if (bgcOptionsPref != null) {
                params.put(ARG10, bgcOptionsPref);
            }

            String bgcPSource = customAttributesMap
                    .get(LDAPMailboxCustomProperty.bgcPSource.name());
            if (bgcPSource != null) {
                params.put(ARG11, bgcPSource);
            }

            String bgcExpireDaysPriv = customAttributesMap
                    .get(LDAPMailboxCustomProperty.bgcExpireDaysPriv.name());
            if (bgcExpireDaysPriv != null) {
                params.put(ARG12, bgcExpireDaysPriv);
            }

            if (requestState.getInputParams().get("maxMsgSize") != null) {
                params.put(ARG13,
                        requestState.getInputParams().get("maxMsgSize").get(0));
            }

            String bgcExpireDaysPref = customAttributesMap
                    .get(LDAPMailboxCustomProperty.bgcExpireDaysPref.name());
            if (bgcExpireDaysPref != null) {
                params.put(ARG_EXPIRE_DAYS_PREF, bgcExpireDaysPref);
            }

            List<String> statusList = requestState.getInputParams().get(
                    MailboxProperty.status.name());
            String status = null;
            if (statusList != null) {
                status = statusList.get(0);
            }
            if (status != null) {
                params.put(ARG_STATUS, status);
            }

            List<String> cosIdList = requestState.getInputParams().get(
                    MailboxProperty.cosId.name());
            String cosId = null;
            if (cosIdList != null) {
                cosId = cosIdList.get(0);
            }
            logger.info("CosId in request : " + cosId);
            if (cosId != null) {
                params.put(ARG_COS_ID, cosId);
            }

            List<String> emailAliasList = requestState.getInputParams().get(
                    MailboxProperty.emailAlias.name());
            String emailAlias = null;
            if (emailAliasList != null) {
                emailAlias = emailAliasList.get(0);
            }
            if (userName.equals("")
                    && (emailAlias == null || emailAlias.equals(""))) {
                throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name());
            }
            
            String aliasUser = null;
            String aliasDomain = null;
            if (emailAlias != null && !emailAlias.equals("")) {
                aliasUser = ActionUtils.getUsernameFromEmail(emailAlias);
                aliasDomain = ActionUtils.getDomainFromEmail(emailAlias);
            }
            if (aliasUser != null) {
                params.put(ARG_ALIAS_USER, aliasUser);
            }
            if (aliasDomain != null) {
                params.put(ARG_ALIAS_DOMAIN, aliasDomain);
            }

            List<String> msisdnList = requestState.getInputParams().get(
                    MailboxProperty.msisdn.name());
            String msisdn = null;
            if (msisdnList != null) {
                msisdn = msisdnList.get(0);
            }
            if (msisdn != null) {
                params.put(ARG_MSISDN, msisdn);
            }

            LoggingResponseBean maaResponse = callMaa(
                    CREATE_ACCOUNT_LOGGING_SUB_URL, params);
            // Analyze logging response
            analyzeLoggingResponse(maaResponse);
            if (userName.equals("") && emailAlias != null) {
                requestState.getInputParams().put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                requestState.getInputParams().get(MailboxProperty.email.name())
                        .add(emailAlias);
            }
        } catch (MxOSException me) {
            throw me;
        } catch (Exception e) {
            logger.error(e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End : CreateMailboxMaaLoggingAction");
        }
        return;
    }
}
