/*
 /*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.auth;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to post-authenticate the user.
 * 
 * @author mxos-dev
 */
public class PostAuthenticate implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(PreAuthenticate.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("action start.");
        }
        if (logger.isDebugEnabled()) {
            logger.debug(" action end.");
        }
    }
}