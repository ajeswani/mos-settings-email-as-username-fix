package com.opwvmsg.mxos.backend.crud;

import java.util.List;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.opwvmsg.mxos.task.pojos.Details;
import com.opwvmsg.mxos.task.pojos.Recurrence;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;

/**
 * Address Book APIs for interfacing with Open-Xchange (OX) server.
 * 
 * @author ajeswani
 * 
 */
public interface ITasksCRUD extends ITransaction {

    /**
     * API to add or confirm a Task.
     * 
     * @param username
     * @param task
     * @param session
     * @return
     * @throws TasksException
     */
    public String confirmTask(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to add multiple tasks.
     * 
     * @param username
     * @param tasks
     * @param session
     * @return
     * @throws TasksException
     */
    public List<String> createMultipleTasks(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to add or update a Task.
     * 
     * @param username
     * @param task
     * @param session
     * @return
     * @throws TasksException
     */
    public String createTask(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to delete task.
     * 
     * @param username
     * @param session
     * @return
     * @throws TasksException
     */
    public void deleteTask(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to delete all tasks.
     * 
     * @param username
     * @param session
     * @return
     * @throws TasksException
     */
    public void deleteTasks(MxOSRequestState mxosRequestState, final List<String> taskIds)
            throws TasksException;

    /**
     * API to get list of Task details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public List<Task> readAllTasks(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to get list of Task IDs.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public List<String> readAllTaskIds(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to get Task base.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public TaskBase readTasksBase(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get Task participant.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public Participant readTasksParticipant(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get Task Details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public Details readTasksDetails(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get Task recurrence.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public Recurrence readTasksRecurrence(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task base.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksBase(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task external participant.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksExternalParticipant(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task participant.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksParticipant(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task progress.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksProgress(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task recurrence.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksRecurrence(MxOSRequestState requestState)
            throws TasksException;

    void logout(MxOSRequestState mxosRequestState) throws TasksException;

    ExternalSession login(MxOSRequestState mxosRequestState) throws TasksException;

    void validateUser(MxOSRequestState mxosRequestState) throws TasksException;

}
