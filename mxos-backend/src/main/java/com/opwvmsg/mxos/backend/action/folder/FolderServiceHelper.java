/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.folder;

import java.util.List;

import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Folder service helper APIs which has business logic and does actual operation
 * using CRUD APIs.
 *
 * @author mxos-dev
 */
public final class FolderServiceHelper {

    /**
     * Folder create helper which has business logic about how to create folder.
     * It uses metadata CRUD APIs to create folder.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for folder create
     *            operation. e.g. mailboxId, folderId etc.
     * @param folderName - folderName.
     * @throws MxOSException MxOSException.
     */
    public static void create(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState,
            final String folderName) throws MxOSException {

        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.status, 0);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.nextUid, MxOSConstants.BASE_UID);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.msgArrTimeLB, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.msgFistAccessedTimeLB, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.msgLastAccessedTimeLB, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.msgExpTimeLB, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.numMsgRead, 0);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.numMsgs, 0);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.sizeMsgRead, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.sizeMsgs, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                FolderProperty.uidValidity, System.currentTimeMillis());
        metaCRUD.createFolder(mxosRequestState, folderName);
    }

    /**
     * Folder read helper which has business logic about how to read folder. It
     * uses metadata CRUD APIs to read folder.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for folder read operation.
     *            e.g. mailboxId, folderId etc.
     * @param folderId - folderId.
     * @param folder Folder POJO which will be populated with folder data.
     * @throws MxOSException MxOSException.
     */

    public static void read(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState, Folder folder)
            throws MxOSException {
        metaCRUD.readFolder(mxosRequestState, folder);
    }

    /**
     * Folder read helper which has business logic about how to read all
     * folders. It uses metadata CRUD APIs to read folders.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for folder create
     *            operation. e.g. mailboxId, folderId etc.
     * @param folderId folderId.
     * @throws MxOSException MxOSException.
     */
    public static void list(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState,
            final List<Folder> folders) throws MxOSException {
        metaCRUD.listFolders(mxosRequestState, folders);
    }

    /**
     * Folder update helper which has business logic about how to create folder.
     * It uses metadata CRUD APIs to update folder.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for folder update
     *            operation. e.g. mailboxId, folderId etc.
     * @throws MxOSException MxOSException.
     */
    public static void update(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState)
        throws MxOSException {
        metaCRUD.updateFolder(mxosRequestState);
    }

    /**
     * Folder update helper which has business logic about how to create folder.
     * It uses metadata CRUD APIs to update folder.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for folder update
     *            operation. e.g. mailboxId, folderId etc.
     * @throws MxOSException MxOSException.
     */
    public static void updateFolderSubscribed(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState)
        throws MxOSException {
        metaCRUD.updateFolderSubscribed(mxosRequestState);
    }
    /**
     * Folder delete helper which has business logic about how to hard delete
     * folder. It uses metadata and blobdata CRUD APIs to delete folder.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for folder hard delete
     *            operation. e.g. mailboxId, folderId etc.
     * @throws MxOSException MxOSException.
     */
    public static void delete(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        metaCRUD.deleteFolder(mxosRequestState);
    }

    /**
     * Private constructor because utility class should not have constructor
     * exposed.
     */
    private FolderServiceHelper() {

    }
}
