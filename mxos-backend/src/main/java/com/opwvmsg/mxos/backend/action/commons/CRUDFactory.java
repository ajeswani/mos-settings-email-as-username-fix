/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/ConnectionPoolFactory.java#2 $
 */

package com.opwvmsg.mxos.backend.action.commons;

import org.apache.http.client.HttpClient;

import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.IAddressBookHelper;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.IBlobHelper;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaHelper;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.ITasksHelper;
import com.opwvmsg.mxos.backend.crud.http.GenericHttpClient;
import com.opwvmsg.mxos.backend.crud.http.GenericHttpConnectionPool;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPBackendState;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxHelper;
import com.opwvmsg.mxos.backend.crud.ldap.LdapConnectionPool;
import com.opwvmsg.mxos.backend.crud.ldap.LdapSearchConnectionPool;
import com.opwvmsg.mxos.backend.crud.maa.MAAConnectionPool;
import com.opwvmsg.mxos.backend.crud.mss.MssMetaBackendState;
import com.opwvmsg.mxos.backend.crud.mss.MssMetaCRUD;
import com.opwvmsg.mxos.backend.crud.mss.MssMetaConnectionPool;
import com.opwvmsg.mxos.backend.crud.mss.MssMetaHelper;
import com.opwvmsg.mxos.backend.crud.mss.MssSlMetaCRUD;
import com.opwvmsg.mxos.backend.crud.mss.MssSlMetaConnectionPool;
import com.opwvmsg.mxos.backend.crud.ox.addressbook.OXAddressBookBackendState;
import com.opwvmsg.mxos.backend.crud.ox.addressbook.OXAddressBookConnectionPool;
import com.opwvmsg.mxos.backend.crud.ox.addressbook.OXAddressBookHelper;
import com.opwvmsg.mxos.backend.crud.ox.settings.OXSettingsHttpConnectionPool;
import com.opwvmsg.mxos.backend.crud.ox.settings.OXSettingsMySQLConnectionPool;
import com.opwvmsg.mxos.backend.crud.ox.tasks.OXTasksBackendState;
import com.opwvmsg.mxos.backend.crud.ox.tasks.OXTasksConnectionPool;
import com.opwvmsg.mxos.backend.crud.ox.tasks.OXTasksHelper;
import com.opwvmsg.mxos.backend.crud.posix.PosixBlobCRUD;
import com.opwvmsg.mxos.backend.crud.rmi.MailboxConnectionPool;
import com.opwvmsg.mxos.backend.crud.we.WEAddressBookBackendState;
import com.opwvmsg.mxos.backend.crud.we.WEAddressBookConnectionPool;
import com.opwvmsg.mxos.backend.crud.we.WEAddressBookHelper;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.BlobDBTypes;
import com.opwvmsg.mxos.data.enums.DataStoreType;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.enums.TasksDBTypes;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.datastore.IDataStoreProvider;
import com.opwvmsg.mxos.utils.datastore.hazelcast.HazelcastDataStoreProvider;

/**
 * CRUD Factory class to create all the CRUD object pools.
 * 
 * @author mxos-dev
 */
public final class CRUDFactory {
    /**
     * Method to get Address Book CRUD pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<IAddressBookCRUD> getAddressBookCRUDPool(
            AddressBookDBTypes dbType) throws MxOSException {
        ICRUDPool<IAddressBookCRUD> addressBookObjectPool = null;
        if (dbType == AddressBookDBTypes.ox) {
            addressBookObjectPool = new OXAddressBookConnectionPool();
        } else if (dbType == AddressBookDBTypes.we) {
            addressBookObjectPool = new WEAddressBookConnectionPool();
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
        return addressBookObjectPool;
    }

    /**
     * Method to get Tasks CRUD pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<ITasksCRUD> getTasksCRUDPool(
            TasksDBTypes dbType) throws MxOSException {
        ICRUDPool<ITasksCRUD> tasksObjectPool = null;
        if (dbType == TasksDBTypes.ox) {
            tasksObjectPool = new OXTasksConnectionPool();
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
        return tasksObjectPool;
    }

    /**
     * Method to get RMI Meta CRUD pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<IRMIMailboxCRUD> getRMIMailboxCRUDPool(
            AddressBookDBTypes dbType) throws MxOSException {
        ICRUDPool<IRMIMailboxCRUD> rmiMailboxObjectPool = null;
        if (dbType == AddressBookDBTypes.ox) {
            rmiMailboxObjectPool = new MailboxConnectionPool();
        } else if (dbType == AddressBookDBTypes.we) {
            // dont throw any exception, this is a valid backend
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
        return rmiMailboxObjectPool;
    }

    /**
     * Method to get OX MySQL Settings CRUD pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<ISettingsCRUD> getSettingsMySQLCRUDPool(
            AddressBookDBTypes dbType) throws MxOSException {
        ICRUDPool<ISettingsCRUD> settingsObjectPool = null;
        if (dbType == AddressBookDBTypes.ox) {
            settingsObjectPool = new OXSettingsMySQLConnectionPool();
        } else if (dbType == AddressBookDBTypes.we) {
            // dont throw any exception, this is a valid backend
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
        return settingsObjectPool;
    }

    /**
     * Method to get Settings CRUD pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<ISettingsCRUD> getSettingsHttpCRUDPool(
            AddressBookDBTypes dbType) throws MxOSException {
        ICRUDPool<ISettingsCRUD> settingsObjectPool = null;
        if (dbType == AddressBookDBTypes.ox) {
            settingsObjectPool = new OXSettingsHttpConnectionPool();
        } else if (dbType == AddressBookDBTypes.we) {
            // dont throw any exception, this is a valid backend
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
        return settingsObjectPool;
    }

    /**
     * Method to get Meta Blob pool.
     * @param rmeVersion
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static IBlobCRUD getBlobCRUDPool(int rmeVersion, BlobDBTypes dbType)
            throws MxOSException {
        if (dbType == BlobDBTypes.posix) {
            return new PosixBlobCRUD();
        } else if (dbType == BlobDBTypes.mss) {
            if (rmeVersion == MxOSConstants.RME_MX8_VER) {
                return new MssMetaCRUD();
            } else {
                return new MssSlMetaCRUD();
            }
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
    }


    /**
     * Method to get Meta Blob Helper.
     * @param dbType
     * @return helper
     * @throws MxOSException if not found
     */
    public static IBlobHelper getBlobHelper(BlobDBTypes dbType) {
        return null;
    }

    /**
     * Method to get MAA Connection Pool.
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<HttpClient> getMAAConnectionPool()
            throws MxOSException {
        String isMAAEnabled = System.getProperty(SystemProperty.maaEnabled
                .name());
        if (Boolean.valueOf(isMAAEnabled)) {
            return new MAAConnectionPool();
        }
        return null;
    }


    /**
     * Method to get Notify Server Connection Pool.
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<GenericHttpClient> getNotifyCRUDPool()
            throws MxOSException {
        if (MxOSConfig.isNotifyHttpConPoolEnabled()) {
            return new GenericHttpConnectionPool(
                    MxOSConfig.getNotifyHttpPoolMaxSize());
        }
        return null;
    }

    /**
     * Method to get Meta Connection Pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<IMetaCRUD> getMetaCRUDPool(MetaDBTypes dbType)
            throws MxOSException {
        if (dbType == MetaDBTypes.mss) {
            return new MssMetaConnectionPool();
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
    }

    /**
     * Method to get Meta Stateless Connection Pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<IMetaCRUD> getMetaStatelessCRUDPool(
            final MetaDBTypes dbType)
            throws MxOSException {
        if (dbType == MetaDBTypes.mss) {
            return new MssSlMetaConnectionPool();
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
    }

    /**
     * Method to get Meta Helper.
     * @param dbType
     * @return Helper
     * @throws MxOSException if not found
     */
    public static IMetaHelper getMetaHelper(MetaDBTypes dbType) {
        if (dbType == MetaDBTypes.mss) {
            return new MssMetaHelper();
        }
        return null;
    }

    /**
     * Method to get Meta Backend State.
     * @param metaDbType
     * @return backendstate
     * @throws MxOSException if not found
     */
    public static IBackendState getMetaBackendState(MetaDBTypes dbType) {
        if (dbType == MetaDBTypes.mss) {
            return new MssMetaBackendState();
        }
        return null;
    }

    /**
     * Method to get Mailbox Backend State.
     * @param metaDbType
     * @return backendstate
     * @throws MxOSException if not found
     */
    public static IBackendState getMailboxBackendState(
            MailboxDBTypes dbType) {
        if (dbType == MailboxDBTypes.ldap) {
            return new LDAPBackendState();
        }
        return null;
    }

    /**
     * Method to get AddressBook Backend State.
     * @param metaDbType
     * @return backendstate
     * @throws MxOSException if not found
     */
    public static IBackendState getAddressBookBackendState(
            AddressBookDBTypes dbType) {
        if (dbType == AddressBookDBTypes.ox) {
            return new OXAddressBookBackendState();
        } else if (dbType == AddressBookDBTypes.we) {
            return new WEAddressBookBackendState();
        }
        return null;
    }

    /**
     * Method to get Tasks Backend State.
     * @param metaDbType
     * @return backendstate
     * @throws MxOSException if not found
     */
    public static IBackendState getTasksBackendState(
            TasksDBTypes dbType) {
        if (dbType == TasksDBTypes.ox) {
            return new OXTasksBackendState();
        }
        return null;
    }

    /**
     * Method to get Mailbox Backend Connection Pool.
     * @param dbTypeBackend
     * @return pool
     * @throws MxOSException if not found
     */
    public static ICRUDPool<IMailboxCRUD> getMailboxCRUDPool(
            MailboxDBTypes dbType) throws MxOSException {
        if (dbType == MailboxDBTypes.ldap) {
            return new LdapConnectionPool();
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
    }

    /**
     * Method to get Mailbox Backend Helper.
     * @param dbTypeBackend
     * @return helper
     * @throws MxOSException if not found
     */
    public static IMailboxHelper getMailboxHelper(
            MailboxDBTypes dbType) {
        if (dbType == MailboxDBTypes.ldap) {
            return new LDAPMailboxHelper();
        }
        return null;
    }

    public static IAddressBookHelper getAddressBookHelper(
            AddressBookDBTypes dbType) {
        if (dbType == AddressBookDBTypes.ox) {
            return new OXAddressBookHelper();
        } else if (dbType == AddressBookDBTypes.we) {
            return new WEAddressBookHelper();
        }
        return null;
    }

    public static ITasksHelper getTasksHelper(
            TasksDBTypes dbType) {
        if (dbType == TasksDBTypes.ox) {
            return new OXTasksHelper();
        }
        return null;
    }

    public static ICRUDPool<IMailboxCRUD> getMailboxSearchCRUDPool(
            MailboxDBTypes dbType) throws MxOSException {
        if (dbType == MailboxDBTypes.ldap) {
            return new LdapSearchConnectionPool();
        } else {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    dbType + " not implemented!");
        }
    }

    @SuppressWarnings("rawtypes")
    public static IDataStoreProvider<?, ?> getDataStoreProvider(
            DataStoreType dbType) throws MxOSException {
        if (dbType.equals(DataStoreType.hazelcast)) {
            return new HazelcastDataStoreProvider();
        }
        throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), dbType
                + " not implemented!");
    }

    /**
     * Singleton private constructor.
     */
    private CRUDFactory() {

    }
}
