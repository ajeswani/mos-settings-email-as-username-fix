/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.util.password.Password;
import com.opwvmsg.utils.paf.util.password.PasswordException;

/**
 * PasswordUtil class of mxos.
 * 
 * @author mxos-dev
 * 
 */
public final class PasswordUtil {
	
    private static final int BCRYPT_ALGO_INDEX = 3;
    private static final int BCRYPT_SALT_INDEX = 29;
  
    /**
     * Default private constructor.
     */
    private PasswordUtil() {

    }

    /**
     * Method to get KeyBIndex from config.
     * 
     * @return keyBList keyBList
     */
    private static int getKeyBDefaultIndex() throws MxOSException {
        return MxOSConfig.getCryptoKeyList().size() - 1;
    }

    /**
     * Method to get KeyB.
     * 
     * @param index index
     * @return keyB keyB
     */
    private static byte[] getKeyB(final int index) throws MxOSException {
        byte[] keyB = MxOSConfig.getCryptoKeyList().get(index);
        return keyB;
    }

    /**
     * Method to get KeyC.
     * 
     * @param keyA keyA
     * @param keyB keyB
     * @return keyC keyC
     */
    private static byte[] getKeyC(final byte[] keyA, final byte[] keyB) {
        final byte[] keyC = new byte[keyA.length];
        for (int i = 0; i < keyA.length; i++) {
            keyC[i] = (byte) (keyA[i] ^ keyB[i]);
        }
        return keyC;
    }

    /**
     * Method to get SecretKeySpec of given keyB index.
     * 
     * @param keyBIndex keyBIndex
     * @return SecretKeySpec SecretKeySpec
     * @throws Exception Exception
     */
    private static SecretKeySpec getSecretKeySpec(final int keyBIndex)
        throws MxOSException {
        final byte[] keyC = getKeyC(MxOSConfig.getKeyA(), getKeyB(keyBIndex));
        return new SecretKeySpec(keyC, "AES");
    }

    /**
     * Method to concat Arrays.
     * 
     * @param first array1
     * @param second array2
     * @return result resultarray
     */
    private static byte[] concatByteArrays(final byte[] first,
            final byte[] second) {
        final byte[] result =
                Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    /**
     * Method to concat Arrays.
     * 
     * @param array array1
     * @param splitIndex index to split
     * @return result resultarray
     * @throws InvalidRequestException
     */
    private static byte[][] splitByteArray(final byte[] array,
            final int splitIndex) throws InvalidRequestException {
        if (array == null || array.length < 16) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid cypher text");
        }
        if (splitIndex < 0 || splitIndex > array.length) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Index = " + splitIndex);
        }
        final byte[][] newArray;
        if (splitIndex == 0 || splitIndex == array.length) {
            newArray = new byte[1][];
            newArray[0] = array;
        } else {
            newArray = new byte[2][];
            newArray[0] = new byte[splitIndex];
            newArray[1] = new byte[array.length - splitIndex];
            for (int i = 0; i < splitIndex; i++) {
                newArray[0][i] = array[i];
            }
            for (int i = 0, j = splitIndex; j < array.length; i++, j++) {
                newArray[1][i] = array[j];
            }
        }
        return newArray;
    }

    /**
     * Method to encrypt the given plain text.
     * 
     * @param pwdProvider pwdProvider
     * @param pType pType
     * @param plainText plainText
     * @return cipherText cipherText
     * @throws PasswordException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws Exception incase of any error during encryption
     */
    public static String encryptPassword(final String passwordType,
            final String plainText) throws MxOSException, PasswordException,
        NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
        IllegalBlockSizeException, BadPaddingException {
        final Password passwordProvider =
                MxOSApp.getInstance().getPasswordProvider();
        final PasswordType pType = PasswordType.getTypeWithValue(passwordType);
        final String encodedPassword;

        if (passwordProvider == null || pType == null || plainText == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                    "To encrypt password, input parameters cannot be null.");
        } else {
            switch (pType) {
            case CLEAR:
                encodedPassword = plainText;
                break;
            case MD5:
            case UNIX:
            case SHA:
            case SSHA1:
                encodedPassword =
                        passwordProvider.hashPassword(plainText,
                                pType.getHashType());
                break;
            case CUSTOM1:
                final Cipher cipher = Cipher.getInstance("AES");
                final int defaultKeyBIndex = getKeyBDefaultIndex();
                // encryption pass
                cipher.init(Cipher.ENCRYPT_MODE,
                        getSecretKeySpec(defaultKeyBIndex));
                final byte[] cipherText = cipher.doFinal(plainText.getBytes());
                final byte[] cipherTestWithIndexB =
                        concatByteArrays(String
                                .format("%02d", defaultKeyBIndex).getBytes(),
                                cipherText);
                encodedPassword =
                        DatatypeConverter
                                .printBase64Binary(cipherTestWithIndexB);
                break;
            default:
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                        "Password type NOT supported");
            }
        }
        return encodedPassword;
    }

    /**
     * Method to decrypt the cypherText.
     * 
     * @param pwdProvider pwdProvider
     * @param pType pType
     * @param cypherText cypherText
     * @return plainText plainText
     * @throws Exception incase of any invalid cypherText
     */
    public static String dencryptPassword(final Password pwdProvider,
            final PasswordType pType, final String cypherText) throws Exception {
        final String plainPassword;

        if (pwdProvider == null || pType == null || cypherText == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                    "To decrypt password, input parameters cannot be null");
        } else {
            switch (pType) {
            case CLEAR:
            case MD5:
            case UNIX:
            case SHA:
            case SSHA1:
                plainPassword = cypherText;
                break;
            case CUSTOM1:
                final Cipher cipher = Cipher.getInstance("AES");
                // convert to byte array
                final byte[] cypherPasswordBytes =
                        DatatypeConverter.parseBase64Binary(cypherText);
                // get keyBindex from cypherPassword (first two bytes)
                final byte[][] cypherArray =
                        splitByteArray(cypherPasswordBytes, 2);
                final int keyBIndex =
                        Integer.parseInt(new String(cypherArray[0]));
                // get the remaining the cypherText
                // encryption pass
                cipher.init(Cipher.DECRYPT_MODE, getSecretKeySpec(keyBIndex));
                final byte[] plainText = cipher.doFinal(cypherArray[1]);
                plainPassword = new String(plainText);
                break;
            case BCRYPT:
                plainPassword = cypherText;
                break;
            default:
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                        "Password type NOT supported");
            }
        }
        return plainPassword;
    }

    /**
     * Method to verify user authentication.
     * 
     * @param pwdProvider pwdProvider
     * @param pType pType
     * @param dbPassword dbPassword
     * @param userPassword userPassword
     * @return isAuthorised isAuthorised
     * @throws Exception incase of any invalid cypherText
     */
    public static boolean isAuthorised(final Password pwdProvider,
            final PasswordType pType, final String dbPassword,
            final String userPassword) throws Exception {

        boolean isAuthorised = false;

        if (pwdProvider == null || pType == null || userPassword == null
                || dbPassword == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                    "To authorize user, input parameters cannot be null");
        } else {
            switch (pType) {
            case CLEAR:
                if (userPassword.equals(dbPassword)) {
                    isAuthorised = true;
                }
                break;
            case MD5:
            case UNIX:
            case SHA:
            case SSHA1:
                isAuthorised =
                        pwdProvider.checkPassword(dbPassword, userPassword,
                                pType.getHashType());
                break;
            case CUSTOM1:
                final Cipher cipher = Cipher.getInstance("AES");
                // convert to byte array
                final byte[] cypherPasswordBytes =
                        DatatypeConverter.parseBase64Binary(dbPassword);
                // get keyBindex from cypherPassword (first two bytes)
                final byte[][] cypherArray =
                        splitByteArray(cypherPasswordBytes, 2);
                final int keyBIndex =
                        Integer.parseInt(new String(cypherArray[0]));
                // get the remaining the cypherText
                // encryption pass
                cipher.init(Cipher.DECRYPT_MODE, getSecretKeySpec(keyBIndex));
                final byte[] plainText = cipher.doFinal(cypherArray[1]);
                final String plainPassword = new String(plainText);
                if (userPassword.equals(plainPassword)) {
                    isAuthorised = true;
                }
                break;
            case BCRYPT:
                String algo = dbPassword.substring(0, BCRYPT_ALGO_INDEX);

                if (!algo.equals("$2a")) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_PASSWORD.name(),
                            "Password is not bcrypted");
                }

                String salt;
                try {
                    salt = dbPassword.substring(0, BCRYPT_SALT_INDEX);

                } catch (Exception e) {

                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_PASSWORD.name(),
                            "Password is not bcrypted\n" + e.getMessage());
                }

                String hashed = EncryptionTool.encryptPassword(userPassword,
                        salt);

                isAuthorised = hashed.equals(dbPassword);

                break;
            default:
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                        "Password type NOT supported");
            }
        }
        return isAuthorised;
    }
}
