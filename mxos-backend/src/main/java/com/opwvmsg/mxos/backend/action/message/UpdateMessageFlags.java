/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.utils.paf.intermail.mail.Msg.MssFlagOrder;

/**
 * Action class to update Message Flags to MSS.
 * 
 * @author mxos-dev
 */
public class UpdateMessageFlags implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateMessageFlags.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMessageFlags action start.");
        }

        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        IBlobCRUD blobCRUD = null;

        try {
            Map<String, Boolean> flagsMap = (Map<String, Boolean>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.messageFlagsMap);
            String keywords = null;
            if (requestState.getInputParams().containsKey(
                    MessageProperty.keywords.name())) {
                keywords = requestState.getInputParams()
                        .get(MessageProperty.keywords.name()).get(0);
            }
			if ((flagsMap == null || flagsMap.isEmpty())
					&& (keywords == null || keywords.isEmpty())) {
				if (logger.isDebugEnabled()) {
					logger.debug("Invalid request cannot update...");
				}
				throw new InvalidRequestException(
						ErrorCode.GEN_BAD_REQUEST.name());
			}

            Flags flags = (Flags) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageFlags);

            for (Map.Entry<String, Boolean> entry: flagsMap.entrySet()) {
                MssFlagOrder msgFlag = MssFlagOrder.valueOf(entry.getKey()
                        .toLowerCase());
                switch (msgFlag) {
                case flagrecent:
                    flags.setFlagRecent(entry.getValue());
                    break;
                case flagseen:
                    flags.setFlagSeen(entry.getValue());
                    break;
                case flagans:
                    flags.setFlagAns(entry.getValue());
                    break;
                case flagflagged:
                    flags.setFlagFlagged(entry.getValue());
                    break;
                case flagdraft:
                    flags.setFlagDraft(entry.getValue());
                    break;
                case flagdel:
                    flags.setFlagDel(entry.getValue());
                    break;
                }
            }

            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            if (logger.isDebugEnabled()) {
                logger.debug("Updating message flags for Email: " + email);
            }
            List<String> messageIdsList = requestState.getInputParams()
            .get(MessageProperty.messageId.name());

            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                blobCRUD = CRUDUtils.getBlobCRUD(requestState);
            }

            String[] messageIds = messageIdsList
            .toArray(new String[messageIdsList.size()]);
            MessageServiceHelper.updateMessageFlags(metaCRUD, blobCRUD,
                    requestState, messageIds);

            // Commit/rollback is required for last access time.
            metaCRUD.commit();
            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                blobCRUD.commit();
            }
        } catch (final MxOSException e) {
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set message flags.", e);
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMessageFlags action end.");
        }
    }
}
