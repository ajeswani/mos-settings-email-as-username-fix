/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

public class UpdateAdminBlockedSendersList implements MxOSBaseAction {

    private static Logger logger = Logger
            .getLogger(UpdateAdminBlockedSendersList.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "UpdateAdminBlockedSendersList action start."));
            }
            try {
                String oldAdminBlockedSender = requestState.getInputParams()
                        .get(MailboxProperty.oldAdminBlockedSender.name())
                        .get(0);
                String newAdminBlockedSender = requestState.getInputParams()
                        .get(MailboxProperty.newAdminBlockedSender.name())
                        .get(0);
                List<String> existingAdminBlockedSendersList = (List<String>) requestState
                        .getDbPojoMap().getPropertyAsObject(
                                MxOSPOJOs.adminBlockedSendersList);
                if (existingAdminBlockedSendersList == null
                        || existingAdminBlockedSendersList.size() < 1
                        || !existingAdminBlockedSendersList
                                .contains(oldAdminBlockedSender.toLowerCase())) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_ADMIN_BLOCKED_SENDER_NOT_EXIST
                                    .name(),
                            "old admin blocked sender does not exist");
                }
                existingAdminBlockedSendersList.remove(oldAdminBlockedSender
                        .toLowerCase());
                existingAdminBlockedSendersList.add(newAdminBlockedSender
                        .toLowerCase());
                final String[] adminBlockedSendersListToArray = existingAdminBlockedSendersList
                        .toArray(new String[existingAdminBlockedSendersList
                                .size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.adminBlockedSendersList,
                                adminBlockedSendersListToArray);
            } catch (MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error(
                        "Error while updating admin blocked sender for update",
                        e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_ADMIN_BLOCKED_SENDERS_POST
                                .name(),
                        e);
            } catch (final Throwable t) {
                logger.error(
                        "Internal Error while updating admin blocked sender", t);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "UpdateAdminBlockedSendersList action end."));
            }
        } else {
            logger.error("Error while calling updateadminblockedsenderslist, group mailbox feature is disabled.");
            throw new InvalidRequestException(
                    MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
        }

    }

}
