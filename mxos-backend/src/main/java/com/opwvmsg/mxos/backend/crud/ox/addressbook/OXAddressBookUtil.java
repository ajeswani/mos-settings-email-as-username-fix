package com.opwvmsg.mxos.backend.crud.ox.addressbook;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;

public class OXAddressBookUtil {

    public static EnumMap<AddressBookProperty, OXContactsProperty> JSONToOX_AddressBook = new EnumMap<AddressBookProperty, OXContactsProperty>(
            AddressBookProperty.class);
    public static Map<String, String> AddressBookProperty_Id = new HashMap<String, String>();

    static {
        JSONToOX_AddressBook.put(AddressBookProperty.assistant,
                OXContactsProperty.assistant_name);
        JSONToOX_AddressBook.put(AddressBookProperty.assistantPhone,
                OXContactsProperty.telephone_assistant);
        JSONToOX_AddressBook.put(AddressBookProperty.categories,
                OXContactsProperty.categories);
        JSONToOX_AddressBook.put(AddressBookProperty.colorLabel,
                OXContactsProperty.color_label);
        JSONToOX_AddressBook.put(AddressBookProperty.companyName,
                OXContactsProperty.company);
        JSONToOX_AddressBook.put(AddressBookProperty.contactId,
                OXContactsProperty.id);
        JSONToOX_AddressBook.put(AddressBookProperty.groupId,
                OXContactsProperty.id);
        JSONToOX_AddressBook.put(AddressBookProperty.created,
                OXContactsProperty.creation_date);
        JSONToOX_AddressBook.put(AddressBookProperty.department,
                OXContactsProperty.department);
        JSONToOX_AddressBook.put(AddressBookProperty.displayName,
                OXContactsProperty.display_name);
        JSONToOX_AddressBook.put(AddressBookProperty.groupName,
                OXContactsProperty.display_name);
        JSONToOX_AddressBook.put(AddressBookProperty.memberName,
                OXContactsProperty.display_name);
        JSONToOX_AddressBook.put(AddressBookProperty.memberFolderId,
                OXContactsProperty.memberFolderId);
        JSONToOX_AddressBook.put(AddressBookProperty.mailField,
                OXContactsProperty.mail_field);
        JSONToOX_AddressBook.put(AddressBookProperty.employeeId,
                OXContactsProperty.employee_type);
        JSONToOX_AddressBook.put(AddressBookProperty.firstName,
                OXContactsProperty.first_name);
        JSONToOX_AddressBook.put(AddressBookProperty.folderId,
                OXContactsProperty.folder);
        JSONToOX_AddressBook.put(AddressBookProperty.folderId,
                OXContactsProperty.folder_id);
        JSONToOX_AddressBook.put(AddressBookProperty.moveToFolderId,
                OXContactsProperty.folder);
        JSONToOX_AddressBook.put(AddressBookProperty.isPrivate,
                OXContactsProperty.private_flag);
        JSONToOX_AddressBook.put(AddressBookProperty.lastName,
                OXContactsProperty.last_name);
        JSONToOX_AddressBook.put(AddressBookProperty.manager,
                OXContactsProperty.manager_name);
        JSONToOX_AddressBook.put(AddressBookProperty.maritalStatus,
                OXContactsProperty.marital_status);
        JSONToOX_AddressBook.put(AddressBookProperty.middleName,
                OXContactsProperty.second_name);
        JSONToOX_AddressBook.put(AddressBookProperty.nickName,
                OXContactsProperty.nickname);
        JSONToOX_AddressBook.put(AddressBookProperty.notes,
                OXContactsProperty.note);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoCity,
                OXContactsProperty.city_home);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoCountry,
                OXContactsProperty.country_home);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoEmail,
                OXContactsProperty.email2);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoFax,
                OXContactsProperty.fax_home);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoImAddress,
                OXContactsProperty.instant_messenger2);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoMobile,
                OXContactsProperty.cellular_telephone2);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoPhone1,
                OXContactsProperty.telephone_home1);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoPhone2,
                OXContactsProperty.telephone_home2);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoPostalCode,
                OXContactsProperty.postal_code_home);
        JSONToOX_AddressBook.put(
                AddressBookProperty.personalInfoStateOrProvince,
                OXContactsProperty.state_home);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoStreet,
                OXContactsProperty.street_home);
        JSONToOX_AddressBook.put(AddressBookProperty.personalInfoVoip,
                OXContactsProperty.telephone_ip);
        JSONToOX_AddressBook.put(AddressBookProperty.prefix,
                OXContactsProperty.title);
        JSONToOX_AddressBook.put(AddressBookProperty.sessionId,
                OXContactsProperty.session);
        JSONToOX_AddressBook.put(AddressBookProperty.spouseName,
                OXContactsProperty.spouse_name);
        JSONToOX_AddressBook.put(AddressBookProperty.suffix,
                OXContactsProperty.suffix);
        JSONToOX_AddressBook.put(AddressBookProperty.title,
                OXContactsProperty.position);
        JSONToOX_AddressBook.put(AddressBookProperty.updated,
                OXContactsProperty.last_modified);
        JSONToOX_AddressBook.put(AddressBookProperty.userId,
                OXContactsProperty.name);
        JSONToOX_AddressBook.put(AddressBookProperty.voip,
                OXContactsProperty.telephone_ip);
        JSONToOX_AddressBook.put(AddressBookProperty.webPage,
                OXContactsProperty.url);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoCity,
                OXContactsProperty.city_business);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoCountry,
                OXContactsProperty.country_business);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoEmail,
                OXContactsProperty.email1);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoFax,
                OXContactsProperty.fax_business);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoImAddress,
                OXContactsProperty.instant_messenger1);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoMobile,
                OXContactsProperty.cellular_telephone1);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoPhone1,
                OXContactsProperty.telephone_business1);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoPhone2,
                OXContactsProperty.telephone_business2);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoPostalCode,
                OXContactsProperty.postal_code_business);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoStateOrProvince,
                OXContactsProperty.state_business);
        JSONToOX_AddressBook.put(AddressBookProperty.workInfoStreet,
                OXContactsProperty.street_business);
        JSONToOX_AddressBook.put(AddressBookProperty.birthDate,
                OXContactsProperty.birthday);
        JSONToOX_AddressBook.put(AddressBookProperty.anniversaryDate,
                OXContactsProperty.anniversary);
        JSONToOX_AddressBook.put(AddressBookProperty.memberId,
                OXContactsProperty.id);
        JSONToOX_AddressBook.put(AddressBookProperty.memberEmail,
                OXContactsProperty.mail);
        JSONToOX_AddressBook.put(AddressBookProperty.sortKey,
                OXContactsProperty.sort);
        JSONToOX_AddressBook.put(AddressBookProperty.sortOrder,
                OXContactsProperty.order);

        // Custom Fields

        JSONToOX_AddressBook.put(AddressBookProperty.pager,
                OXContactsProperty.telephone_pager);
        JSONToOX_AddressBook.put(AddressBookProperty.yomiFirstName,
                OXContactsProperty.yomiFirstName);
        JSONToOX_AddressBook.put(AddressBookProperty.yomiLastName,
                OXContactsProperty.yomiLastName);
        JSONToOX_AddressBook.put(AddressBookProperty.yomiCompany,
                OXContactsProperty.yomiCompany);
        JSONToOX_AddressBook.put(AddressBookProperty.email3,
                OXContactsProperty.email3);
        JSONToOX_AddressBook.put(AddressBookProperty.fileName,
                OXContactsProperty.userfield19);
        JSONToOX_AddressBook.put(AddressBookProperty.uid,
                OXContactsProperty.uid);
        JSONToOX_AddressBook.put(AddressBookProperty.otherPhone,
                OXContactsProperty.telephone_other);

        // Contact Name columns
        AddressBookProperty_Id.put(AddressBookProperty.displayName.name(),
                "500");
        AddressBookProperty_Id.put(AddressBookProperty.firstName.name(), "501");
        AddressBookProperty_Id.put(AddressBookProperty.lastName.name(), "502");
        AddressBookProperty_Id
                .put(AddressBookProperty.middleName.name(), "503");
        AddressBookProperty_Id.put(AddressBookProperty.suffix.name(), "504");
        AddressBookProperty_Id.put(AddressBookProperty.prefix.name(), "505");
        AddressBookProperty_Id.put(AddressBookProperty.nickName.name(), "515");

        // Contact Base columns
        AddressBookProperty_Id.put(AddressBookProperty.contactId.name(), "1");
        AddressBookProperty_Id.put(AddressBookProperty.created.name(), "4");
        AddressBookProperty_Id.put(AddressBookProperty.updated.name(), "5");
        AddressBookProperty_Id
                .put(AddressBookProperty.categories.name(), "100");
        AddressBookProperty_Id.put(AddressBookProperty.isPrivate.name(), "101");
        AddressBookProperty_Id
                .put(AddressBookProperty.colorLabel.name(), "102");
        AddressBookProperty_Id.put(AddressBookProperty.notes.name(), "518");

        // Contacts PersonalInfo columns
        AddressBookProperty_Id.put(AddressBookProperty.maritalStatus.name(),
                "512");
        AddressBookProperty_Id
                .put(AddressBookProperty.spouseName.name(), "516");

        // Contacts PersonalInfo Address columns
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoStreet.name(), "506");
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoPostalCode.name(), "507");
        AddressBookProperty_Id.put(AddressBookProperty.personalInfoCity.name(),
                "508");
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoStateOrProvince.name(), "509");
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoCountry.name(), "510");

        // Contacts PersonalInfo Communication columns
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoPhone1.name(), "548");
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoPhone2.name(), "549");
        AddressBookProperty_Id.put(AddressBookProperty.personalInfoFax.name(),
                "550");
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoMobile.name(), "552");
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoEmail.name(), "556");
        AddressBookProperty_Id.put(
                AddressBookProperty.personalInfoImAddress.name(), "566");
        AddressBookProperty_Id.put(AddressBookProperty.personalInfoVoip.name(),
                "567");

        // Contacts PersonalInfo Events columns
        AddressBookProperty_Id.put(AddressBookProperty.birthDate.name(), "511");
        AddressBookProperty_Id.put(AddressBookProperty.anniversaryDate.name(),
                "517");

        // Contacts WorkInfo columns
        AddressBookProperty_Id
                .put(AddressBookProperty.department.name(), "519");
        AddressBookProperty_Id.put(AddressBookProperty.title.name(), "520");
        AddressBookProperty_Id
                .put(AddressBookProperty.employeeId.name(), "521");
        AddressBookProperty_Id.put(AddressBookProperty.manager.name(), "536");
        AddressBookProperty_Id.put(AddressBookProperty.assistant.name(), "537");

        // Contacts WorkInfo Address columns
        AddressBookProperty_Id.put(AddressBookProperty.workInfoStreet.name(),
                "523");
        AddressBookProperty_Id.put(
                AddressBookProperty.workInfoPostalCode.name(), "525");
        AddressBookProperty_Id.put(AddressBookProperty.workInfoCity.name(),
                "526");
        AddressBookProperty_Id.put(
                AddressBookProperty.workInfoStateOrProvince.name(), "527");
        AddressBookProperty_Id.put(AddressBookProperty.workInfoCountry.name(),
                "528");

        // Contacts WorkInfo Communication columns
        AddressBookProperty_Id.put(AddressBookProperty.workInfoPhone1.name(),
                "542");
        AddressBookProperty_Id.put(AddressBookProperty.workInfoPhone2.name(),
                "543");
        AddressBookProperty_Id.put(AddressBookProperty.workInfoFax.name(),
                "544");
        AddressBookProperty_Id.put(AddressBookProperty.workInfoMobile.name(),
                "551");
        AddressBookProperty_Id.put(AddressBookProperty.workInfoEmail.name(),
                "555");

        // Group Elements
        AddressBookProperty_Id.put(AddressBookProperty.groupId.name(), "1");
        AddressBookProperty_Id.put(AddressBookProperty.groupName.name(), "500");
    }

    /**
     * Default private constructor.
     */
    private OXAddressBookUtil() {

    }
}
