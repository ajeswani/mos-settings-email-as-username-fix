/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/exception/MxosErrorMapping.java#1 $
 */
package com.opwvmsg.mxos.backend.action.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * The MxosErrorMapping class is used to load error mappings configured.
 *
 * @author Aricent
 */
public final class MxosErrorMapping {
    private static Logger logger = Logger.getLogger(MxosErrorMapping.class);
    // Map of all the error mappings available
    private Map<String, ResponseErrorBean> errorMappings;

    /**
     * Default Constructor.
     * @param app name of the application
     * @throws MxOSException MxOSException
     */
    public MxosErrorMapping(String app) throws MxOSException {
        logger.debug("Loading Error Mappings xml and creating errorMappings");
        errorMappings = new LinkedHashMap<String, ResponseErrorBean>();
        loadErrorMappingFromXML(app);
    }

    /**
     * loadErrorMappingFromXML is to load the properties file.
     * @param app name of the application
     * @throws MxOSException throws in case of error in loading rules
     */
    protected void loadErrorMappingFromXML(final String app) throws MxOSException {
        // TODO: The mxos-error-codes.xml name should take from configuration
        final String errorCodesFileName = "error-codes.xml";
        final String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }
        String configDir = home + "/config/" + app + "/";
        
        InputStream is = null;
        try {
            List<File> files = (List<File>) FileUtils.listFiles(new File(
                configDir), new String[] { "xml" }, false);
            for (File file : files) {
                if (file.getCanonicalPath().endsWith(errorCodesFileName)) {
                    logger.debug("Loading errors file : "
                            + file.getCanonicalPath());
                    is = new FileInputStream(file.getCanonicalPath());

                    boolean ok = MxosErrorHandler.parse(is, errorMappings);
                    logger.info("loadErrorMappingFromXML ret : " + ok);
                    IOUtils.closeQuietly(is);
                }
            }
        } catch (Exception e) {
            logger.error("Error while loading error code mapping xml", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Error while loading error code mapping xml.");
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
    /**
     * Method to print printErrorMappings.
     */
    public void printErrorMappings() {
        for (Entry<String, ResponseErrorBean> entry
                : errorMappings.entrySet()) {
            logger.debug("Error mapping key is : " + entry.getKey());
            ResponseErrorBean bean = entry.getValue();
            logger.debug("bean values are - customErrorCode = "
                    + bean.getErrorCode() + ", mxosErrorCode = "
                    + bean.getMxosErrorCode() + ", message = "
                    + bean.getShortMessage());
        }
    }

    /**
     * Method to get ErrorMappings.
     * @return the errorMappings
     */
    public Map<String, ResponseErrorBean> getErrorMappings() {
        return errorMappings;
    }

    /**
     * Method to set ErrorMappings.
     * @param em the errorMappings to set
     */
    public void setErrorMappings(Map<String, ResponseErrorBean> em) {
        errorMappings = em;
    }
}
