/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ldap.MailboxType;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set mailbox type.
 *
 * @author mxos-dev
 */
public class SetType implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetType.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()
                && requestState.getInputParams().containsKey(
                        MailboxProperty.groupName.name())) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("SetType action start."));
            }
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.type.name())) {
                String type = requestState.getInputParams()
                        .get(MailboxProperty.type.name()).get(0);
                type = MailboxType.getByType(type).getValue();
                if (type.equalsIgnoreCase(MailboxType.GROUPADMIN.getValue())
                        || type.equalsIgnoreCase(MailboxType.GROUPMAILBOX
                                .getValue())) {
                    MxOSApp.getInstance()
                            .getMailboxHelper()
                            .setAttribute(requestState, MailboxProperty.type,
                                    type);
                } else {
                    throw new InvalidRequestException(
                            MailboxError.MBX_CREATE_TYPE_NOT_SUPPORTED
                                    .name());
                }
            } else {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState, MailboxProperty.type,
                                MailboxType.GROUPMAILBOX.getValue());
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("SetType action end."));
            }
        }
    }
}
