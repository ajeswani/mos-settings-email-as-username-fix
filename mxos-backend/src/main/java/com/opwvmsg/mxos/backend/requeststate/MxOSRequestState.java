/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.requeststate;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

public class MxOSRequestState {
    final protected Map<String, List<String>> inputParams;
    final private DataMap dbPojoMap;
    final private DataMap additionalParams;
    private Map<String, IBackendState> backendState;

    final protected ServiceEnum serviceName;
    final protected Operation operationType;
    final protected String operationName;

    public MxOSRequestState(Map<String, List<String>> inputParams,
            ServiceEnum serviceName, Operation operationType) {
        this.inputParams = inputParams;
        this.serviceName = serviceName;
        this.operationType = operationType;
        additionalParams = new DataMap();
        dbPojoMap = new DataMap();
        this.operationName = new StringBuffer().append(
                this.serviceName.name()).append(MxOSConstants.COLON)
                .append(this.operationType).toString();
    }

    /**
     * @return the inputParams
     */
    public Map<String, List<String>> getInputParams() {
        return inputParams;
    }

    /**
     * @return the dbPojoMap
     */
    public DataMap getDbPojoMap() {
        return dbPojoMap;
    }

    /**
     * @return the additionlParams
     */
    public DataMap getAdditionalParams() {
        return additionalParams;
    }

    /**
     * @return the serviceName
     */
    public ServiceEnum getServiceName() {
        return serviceName;
    }

    /**
     * @return the operationType
     */
    public Operation getOperationType() {
        return operationType;
    }

    /**
     * @return the backendState
     */
    public Map<String, IBackendState> getBackendState() {
        return backendState;
    }

    /**
     * @param backendState the backendState to set
     */
    public void setBackendState(Map<String, IBackendState> backendState) {
        this.backendState = backendState;
    }

    /**
     * @return the operationName
     */
    public String getOperationName() {
        return operationName;
    }
}
