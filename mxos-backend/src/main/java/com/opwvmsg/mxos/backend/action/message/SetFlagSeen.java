/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set flagSeen in Message Flags.
 * 
 * @author mxos-dev
 */
public class SetFlagSeen implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetFlagSeen.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetFlagSeen action start.");
        }

        try {
            Boolean flag = ActionUtils.getMessageFlagValue(requestState,
                    MessageProperty.flagSeen);
            ((Map<String, Boolean>) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageFlagsMap)).put(
                    MessageProperty.flagSeen.name(), flag);
        } catch (final Exception e) {
            logger.error("Error while setting the message flag.", e);
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SetFlagSeen action end.");
        }
    }
}
