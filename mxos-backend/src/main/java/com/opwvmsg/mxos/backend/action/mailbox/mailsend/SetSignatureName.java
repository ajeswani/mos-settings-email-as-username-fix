/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set name attribute of signature.
 * 
 * @author mxos-dev
 * 
 */

public class SetSignatureName implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetSignatureName.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSignatureName action start."));
        }

        if (requestState.getInputParams().get(
                MailboxProperty.signatureName.name()) != null) {

            Signature newSignature = (Signature) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);

            newSignature.setSignatureName(requestState.getInputParams()
                    .get(MailboxProperty.signatureName.name()).get(0));

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.signature,
                    newSignature);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSignatureName action end."));
        }
    }
}
