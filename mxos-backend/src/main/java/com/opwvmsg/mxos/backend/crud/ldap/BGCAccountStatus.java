/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * Class for AccountStatus.
 *
 * @author mxos-dev
 */
public enum BGCAccountStatus {

    open, closing, locked, closed, pending, inactive, active;

    private String key;

    /**
     * Method to get Enum from given value.
     *
     * @param value - value
     * @return AccountStatus - enum
     * @throws InvalidDataException - if enum not found
     */
    public static BGCAccountStatus getEnumByValue(final String value)
            throws InvalidRequestException {
        if (value == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                    "Invalid account status:" + value);
        }
        for (BGCAccountStatus status: values()) {
            if (status.name().equalsIgnoreCase(value)) {
                return status;
            }
        }
        throw new InvalidRequestException(
                MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                "Unsupported account status type:" + value);
    }

    /**
     * Method to get key as String.
     *
     * @return the key - short code
     */
    public String getKey() {
        return key;
    }
}
