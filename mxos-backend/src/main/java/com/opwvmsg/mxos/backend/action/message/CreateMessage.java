/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.ArrayList;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.mss.MssMetaCRUD.SystemFolder;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create Message.
 * 
 * @author mxos-dev
 */
public class CreateMessage implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateMessage.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateMessage action start."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        IBlobCRUD blobCRUD = null;
        try {
            if (!requestState.getInputParams().containsKey(
                    FolderProperty.folderName.name())) {
                requestState.getInputParams().put(
                        FolderProperty.folderName.name(),
                        new ArrayList<String>());
                requestState.getInputParams().get(
                        FolderProperty.folderName.name()).add(
                                SystemFolder.INBOX.name());
            }

            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                blobCRUD = CRUDUtils.getBlobCRUD(requestState);
            }
            final String messageUID =
                    MessageServiceHelper.create(metaCRUD, blobCRUD,
                            requestState);
            requestState.getDbPojoMap().setProperty(MessageProperty.messageId.name(),
                    messageUID);
            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                blobCRUD.commit();
            }
            metaCRUD.commit();
        } catch (final MxOSException e) {
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while create message.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(), e);

        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateMessage action end."));
        }
    }
}
