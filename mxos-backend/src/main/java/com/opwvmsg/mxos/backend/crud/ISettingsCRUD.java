/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Settings CRUD.
 * 
 * @author mxos-dev
 */
public interface ISettingsCRUD extends ITransaction {
    void close() throws Exception;

    /**
     * Create Setting.
     * 
     * @param mxosRequestState - user settings to be created.
     * @throws MxOSException - on user setting not created or any other error.
     */
    void createSetting(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Delete Setting.
     * 
     * @param setting - name of the setting to be deleted.
     * @throws MxOSException - on setting not found or any other error.
     */
    void deleteSetting(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Read Setting.
     * 
     * @param setting - name of the setting to be read.
     * @return Setting - on successful read.
     * @throws MxOSException - on domain not found or any other error.
     */
    String readSetting(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Update settings.
     * 
     * @param mxosRequestState - settings to be updated.
     * @throws MxOSException - on setting not found or any other error.
     */
    void updateSetting(final MxOSRequestState mxosRequestState)
            throws MxOSException;
}
