/*
/*
* Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
*
* The copyright to the computer software herein is the property of
* Openwave Systems Inc. The software may be used and/or copied only
* with the written permission of Openwave Systems Inc. or in accordance
* with the terms and conditions stipulated in the agreement/contract
* under which the software has been supplied.
*
* $Id:$
*/

package com.opwvmsg.mxos.backend.action.ox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
* Action class to logout user.
* 
* @author mxos-dev
*/
public class OXLogoutUser implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(OXLogoutUser.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Logout user action start."));
        }
        if (!requestState.getInputParams()
                .containsKey(ExternalProperty.entity.name())) {
            final String errMsg = "Param 'entity' is missing. Cannot Authenticate";
            logger.warn(errMsg);
            throw new MxOSException("code", errMsg);
        }
        final Entity entity = Entity.fromValue(requestState.getInputParams()
                .get(ExternalProperty.entity.name()).get(0));
        String backend = null;
        switch(entity) {
            case ADDRESS_BOOK:
                backend = MxOSConfig.getAddressBookBackend();
                break;
            case TASKS:
                backend = MxOSConfig.getTasksBackend();
                break;
        }
        // folderId only applies for OX backend
        if (null != backend && backend.equals(AddressBookDBTypes.ox.name())) {
            switch(entity) {
                case ADDRESS_BOOK:
                    logoutAddressBookCRUD(requestState);
                    break;
                case TASKS:
                    logoutTasksCRUD(requestState);
                    break;
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("LogoutUser action end."));
        }
    }
    /**
     * Logout using AddressBook CRUD pool.
     * @param requestState
     * @return
     * @throws MxOSException
     */
    private void logoutAddressBookCRUD(final MxOSRequestState requestState) throws MxOSException  {
        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;

        try {
            addressBookCRUDPool = MxOSApp.getInstance()
                    .getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();
            addressBookCRUD.logout(requestState);
        } catch (AddressBookException e) {
            logger.error("Error while logout.", e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_UNABLE_TO_LOGOUT, e);
        } catch (final Exception e) {
            logger.error("Error while logout.", e);
            throw new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
    }
    /**
     * Logout using Tasks CRUD pool.
     * @param requestState
     * @return
     * @throws MxOSException
     */
    private void logoutTasksCRUD(final MxOSRequestState requestState) throws MxOSException  {
        ICRUDPool<ITasksCRUD> tasksCRUDPool = null;
        ITasksCRUD tasksCRUD = null;
        try {
            tasksCRUDPool = MxOSApp.getInstance()
                    .getTasksCRUD();
            tasksCRUD = tasksCRUDPool.borrowObject();
            tasksCRUD.logout(requestState);
        } catch (TasksException e) {
            logger.error("Error while logout.", e);
            ExceptionUtils.createMxOSExceptionFromTasksException(
                    TasksError.TSK_UNABLE_TO_LOGOUT, e);
        } catch (final Exception e) {
            logger.error("Error while logout.", e);
            throw new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } finally {
            try {
                if (tasksCRUDPool != null && tasksCRUD != null) {
                    tasksCRUDPool.returnObject(tasksCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
    }
}
