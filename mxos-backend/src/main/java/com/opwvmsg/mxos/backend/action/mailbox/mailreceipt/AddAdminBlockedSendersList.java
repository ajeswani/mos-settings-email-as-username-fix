/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.intermail.mail.Mailbox;

public class AddAdminBlockedSendersList implements MxOSBaseAction {

    private static Logger logger = Logger
            .getLogger(AddAdminBlockedSendersList.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "AddAdminBlockedSendersList action start."));
            }
            try {
                List<String> existingAdminBlockedSendersList = (List<String>) requestState
                        .getDbPojoMap().getPropertyAsObject(
                                MxOSPOJOs.adminBlockedSendersList);

                List<String> newAdminBlockedSendersList = (List<String>) requestState
                        .getInputParams().get(
                                MailboxProperty.adminBlockedSendersList.name());

                if (existingAdminBlockedSendersList == null) {
                    existingAdminBlockedSendersList = new ArrayList<String>();
                }
                if (existingAdminBlockedSendersList.size()
                        + newAdminBlockedSendersList.size() > MxOSConfig
                        .getMaxAdminBlockedSendersList()) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_ADMIN_BLOCKED_SENDERS_REACHED_MAX_LIMIT
                                    .name(),
                            "admin blocked senders reached max limit");
                }
                for (String newAdminBlockedSender : newAdminBlockedSendersList) {
                    if (!existingAdminBlockedSendersList
                            .contains(newAdminBlockedSender.toLowerCase())) {
                        existingAdminBlockedSendersList
                                .add(newAdminBlockedSender.toLowerCase());
                    }
                }
                final String[] adminApprovedSendersListToArray = existingAdminBlockedSendersList
                        .toArray(new String[existingAdminBlockedSendersList
                                .size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.adminBlockedSendersList,
                                adminApprovedSendersListToArray);
            } catch (MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error(
                        "Error while creating admin blocked sender for create",
                        e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_ADMIN_BLOCKED_SENDERS_CREATE
                                .name(),
                        e);
            } catch (final Throwable t) {
                logger.error("Error while creating admin blocked sender", t);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "AddAdminBlockedSendersList action end."));
            }
        } else {
            logger.error("Error while calling addadminblockedsenderslist, group mailbox feature is disabled.");
            throw new InvalidRequestException(
                    MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
        }

    }
}
