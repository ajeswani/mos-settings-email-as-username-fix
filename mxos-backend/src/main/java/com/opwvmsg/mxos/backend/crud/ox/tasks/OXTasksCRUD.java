/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.crud.ox.OXAbstractHttpCRUD;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.JsonToTasksMapper;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.LoginResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.Response;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.TaskResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.UserResponse;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.data.enums.TasksDBTypes;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.opwvmsg.mxos.task.pojos.Details;
import com.opwvmsg.mxos.task.pojos.Recurrence;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Class to implement methods OX specific methods for communicating and parsing
 * the response
 * 
 * @author mxos-dev
 * 
 */
public class OXTasksCRUD extends OXAbstractHttpCRUD implements ITasksCRUD {

    enum SortOrder {
        ascending("asc"), descending("desc");

        private final String value;

        SortOrder(String value) {
            this.value = value;
        }

        public String convert() {
            return value;
        }
    }

    private final String TASKS_STRING = "tasks";
    private final String GET_STRING = "get";
    private final String ALL_STRING = "all";
    private final String DELETE_STRING = "delete";
    private final String NEW_STRING = "new";
    private static final String TASKS_BASE_COLUMNS = "1,20,200,309";

    /**
     * Constructor.
     * 
     * @param baseURL - base url of mxos
     */
    public OXTasksCRUD(String baseURL) {
        webResource = getWebResource(baseURL);
    }

    @Override
    public String confirmTask(MxOSRequestState mxosRequestState)
            throws TasksException {
        return null;
    }

    @Override
    public List<String> createMultipleTasks(MxOSRequestState mxosRequestState)
            throws TasksException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String createTask(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(NEW_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.cookieString.name()).get(
                0));

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(ExternalProperty.session.name(), paramList2);
        paramsNew.put(ExternalProperty.cookieString.name(), paramList3);

        try {

            Map<String, String> attributes = ((OXTasksBackendState) mxosRequestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskBase(
                    attributes, getFolderId(params));

            final ClientResponse r = put(subUrl, paramsNew, jsonString);
            TaskBase taskBase = new TaskResponse().getTaskBase(r);
            return taskBase.getTaskId();
        } catch (final TasksException e) {
            e.printStackTrace();
            throw e;
        } catch (final Exception e) {
            e.printStackTrace();
            throw new TasksException(e);
        }
    }

    @Override
    public void deleteTask(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);
        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.timestamp.name(), paramList2);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);

        try {
            final List<String> taskIds = new ArrayList<String>();
            taskIds.add(params.get(TasksProperty.taskId.name()).get(0));
            String jsonString = JsonToTasksMapper.mapFromTaskIds(
                    taskIds, getFolderId(params));

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }

    }

    @Override
    public void deleteTasks(MxOSRequestState mxosRequestState, final List<String> taskIds)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);
        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.timestamp.name(), paramList2);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);

        try {
            
            String jsonString = JsonToTasksMapper.mapFromTaskIds(
                    taskIds, getFolderId(params));

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }

    }

    private ClientResponse get(final String subURL,
            final Map<String, List<String>> params) throws MxOSException,
            TasksException {
        try {
            return getOX(subURL, params);
        } catch (ComponentException e) {
            throw (TasksException) e;
        }
    }

    private String getFolderId(Map<String, List<String>> params) {
        String folderId = MxOSConstants.OX_DEFAULT_TASKS_FOLDER_ID;
        if (params.get(TasksProperty.folderId.name()) != null
                && !params.get(TasksProperty.folderId.name()).get(0)
                        .isEmpty()) {
            folderId = params.get(TasksProperty.folderId.name()).get(0);
        }
        return folderId;
    }

    @Override
    protected ExternalSession getSessionFromResponse(final ClientResponse r)
            throws TasksException {
        return new LoginResponse().getSession(r);
    }

    @Override
    protected String getUserIdFromResponse(ClientResponse resp)
            throws TasksException {
        return new UserResponse().getUserId(resp);
    }

    @Override
    public ExternalSession login(MxOSRequestState mxosRequestState)
            throws TasksException {
        try {
            return loginOX(mxosRequestState);
        } catch (ComponentException e) {
            throw (TasksException) e;
        }
    }

    @Override
    public void logout(MxOSRequestState mxosRequestState) throws TasksException {
        try {
            logoutOX(mxosRequestState);
        } catch (ComponentException e) {
            throw (TasksException) e;
        }
    }

    private ClientResponse put(final String subURL,
            final Map<String, List<String>> params, final String jsonString)
            throws MxOSException, TasksException {
        try {
            return putOX(subURL, params, jsonString);
        } catch (ComponentException e) {
            throw (TasksException) e;
        }
    }

    @Override
    public List<String> readAllTaskIds(MxOSRequestState mxosRequestState)
            throws TasksException {

        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(ALL_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(TASKS_BASE_COLUMNS);

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.columns.name(), paramList2);
        paramsNew.put(OXTasksProperty.folder.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getAllTaskIds(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public List<Task> readAllTasks(MxOSRequestState mxosRequestState)
            throws TasksException {


        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(ALL_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(TASKS_BASE_COLUMNS);

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.columns.name(), paramList2);
        paramsNew.put(OXTasksProperty.folder.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getAllTasks(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }    
    }

    @Override
    public TaskBase readTasksBase(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.taskId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.folder.name(), paramList2);
        paramsNew.put(OXTasksProperty.id.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getTaskBase(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public Participant readTasksParticipant(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Details readTasksDetails(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Recurrence readTasksRecurrence(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void rollback() throws ApplicationException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksBase(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksExternalParticipant(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksParticipant(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksProgress(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksRecurrence(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub

    }

    @Override
    public void validateUser(MxOSRequestState mxosRequestState)
            throws TasksException {
        try {
            validateUserOX(mxosRequestState);
        } catch (ComponentException e) {
            throw (TasksException) e;
        }
    }
}
