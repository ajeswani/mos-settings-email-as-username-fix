/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.settings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPBackendState;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPUtils;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Class to implement methods OX specific methods for communicating and parsing
 * the response
 * 
 * @author mxos-dev
 * 
 */
public class OXSettingsMySQLCRUD implements ISettingsCRUD {
    private final static String INSERT_ALIAS_SQL;
    private final static String DELETE_ALL_ALIAS_SQL;
    private final static String UPDATE_EXTERNAL_ACCOUNT_ID_SQL;
    private final static String INSERT_EXTERNAL_INCOMING_ACCOUNT_SQL;
    private final static String INSERT_EXTERNAL_OUTGOING_ACCOUNT_SQL;
    private final static String DELETE_EXTERNAL_INCOMING_ACCOUNTS_SQL;
    private final static String DELETE_EXTERNAL_OUTGOING_ACCOUNTS_SQL;
    private final static String DELETE_EXTERNAL_ACCOUNTS_PROPERTIES_SQL;
    private final static String LINE_WRAP_SQL;
    private final static String AUTO_COLLECT_CONTACTS_WHILE_SENDING_SQL;
    private final static String AUTO_COLLECT_CONTACTS_WHILE_ALL_MAIL_SQL;
    private final static String AUTO_COLLECT_CONTACTS_WHILE_READING_SQL;
    private final static String REPLY_TO_SQL;
    private final static String FROM_ADDRESS_SQL;
    private final static String EXCLUDE_ORIGINAL_FROM_REPLY_SQL;
    private final static String INCLUDE_ORIGINAL_IN_REPLY_SQL;
    private final static String DISABLE_IMAGE_IN_HTML_SQL;
    private final static String ENABLE_IMAGE_IN_HTML_SQL;
    private final static String DISABLE_DELETE_MAIL_PERMANENT_SQL;
    private final static String ENABLE_DELETE_MAIL_PERMANENT_SQL;
    private final static String TIMEZONE_SQL;
    private final static String LANGUAGE_SQL;
    private final static String DB_WRITE_POOL_SQL;
    
    private static final int OX_CONNECTION_RETRY_COUNT = 3;
    private static final int OX_CONNECTION_RETRY_INTERVAL = 1000;

    static {

        INSERT_ALIAS_SQL = new StringBuilder()
                .append("INSERT INTO user_attribute ")
                .append("(cid, id, name, value) ")
                .append("SELECT cid, id, 'alias', ? ").append("FROM user ")
                .append("WHERE imapLogin=?").toString();
        DELETE_ALL_ALIAS_SQL = new StringBuilder().append("DELETE  a.* ")
                .append("FROM user_attribute as a ")
                .append("LEFT JOIN user as b ")
                .append("ON a.cid = b.cid and a.id = b.id and a.name='alias' ")
                .append("WHERE (b.imapLogin=?)").toString();
        UPDATE_EXTERNAL_ACCOUNT_ID_SQL = new StringBuilder()
                .append("UPDATE sequence_mail_service AS a ")
                .append("INNER JOIN user AS b ").append("ON a.cid = b.cid ")
                .append("SET a.id = ? ").append("WHERE (b.imapLogin=?)")
                .toString();
        INSERT_EXTERNAL_INCOMING_ACCOUNT_SQL = new StringBuilder()
                .append("INSERT INTO user_mail_account ")
                .append("(cid, id, user, name, url, login, password, primary_addr, default_flag, trash, sent, drafts, spam, confirmed_spam, confirmed_ham, spam_handler, unified_inbox, trash_fullname, sent_fullname, drafts_fullname, spam_fullname, confirmed_spam_fullname, confirmed_ham_fullname, personal, replyTo) ")
                .append("SELECT cid, ?, id, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'confirmed-spam', 'confirmed-ham', 'NoSpamHandler', 0, '', '', '', '', '', '', ?, null ")
                .append("FROM user ").append("WHERE imapLogin=?").toString();
        INSERT_EXTERNAL_OUTGOING_ACCOUNT_SQL = new StringBuilder()
                .append("INSERT INTO user_transport_account ")
                .append("(cid, id, user, name, url, login, password, send_addr, personal, replyTo, default_flag, unified_inbox) ")
                .append("SELECT cid, ?, id, ?, ?, ?, ?, ?, null, null, 0, 0 ")
                .append("FROM user ").append("WHERE imapLogin=?").toString();
        DELETE_EXTERNAL_INCOMING_ACCOUNTS_SQL = new StringBuilder()
                .append("DELETE  a.* ").append("FROM user_mail_account as a, ")
                .append("user as b ")
                .append("WHERE a.cid = b.cid and a.user = b.id ")
                .append("AND b.imapLogin=?").toString();
        DELETE_EXTERNAL_OUTGOING_ACCOUNTS_SQL = new StringBuilder()
                .append("DELETE  a.* ")
                .append("FROM user_transport_account as a, ")
                .append("user as b ")
                .append("WHERE a.cid = b.cid and a.user = b.id ")
                .append("AND b.imapLogin=?").toString();
        DELETE_EXTERNAL_ACCOUNTS_PROPERTIES_SQL = new StringBuilder()
                .append("DELETE  a.* ")
                .append("FROM user_mail_account_properties as a, ")
                .append("user as b ")
                .append("WHERE a.cid = b.cid and a.user = b.id ")
                .append("AND b.imapLogin=?").toString();
        LINE_WRAP_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET auto_linebreak = ? ")
                .append("WHERE (b.imapLogin=?)").toString();
        AUTO_COLLECT_CONTACTS_WHILE_SENDING_SQL = new StringBuilder()
                .append("UPDATE user_setting_server AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET contactCollectOnMailTransport = 0, ")
                .append("contactCollectOnMailAccess = 1 ")
                .append("WHERE (b.imapLogin=?)").toString();
        AUTO_COLLECT_CONTACTS_WHILE_ALL_MAIL_SQL = new StringBuilder()
                .append("UPDATE user_setting_server AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET contactCollectOnMailTransport = 1, ")
                .append("contactCollectOnMailAccess = 0 ")
                .append("WHERE (b.imapLogin=?)").toString();
        AUTO_COLLECT_CONTACTS_WHILE_READING_SQL = new StringBuilder()
                .append("UPDATE user_setting_server AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET contactCollectOnMailTransport = 0, ")
                .append("contactCollectOnMailAccess = 0 ")
                .append("WHERE (b.imapLogin=?)").toString();
        REPLY_TO_SQL = new StringBuilder()
                .append("UPDATE user_mail_account AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET replyTo = ? ").append("WHERE (b.imapLogin=?)")
                .toString();
        FROM_ADDRESS_SQL = new StringBuilder()
                .append("UPDATE prg_contacts AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.fid = 6 ")
                .append("SET field01 = ? ").append("WHERE (b.imapLogin=?)")
                .toString();
        EXCLUDE_ORIGINAL_FROM_REPLY_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits | (1 << 10)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        INCLUDE_ORIGINAL_IN_REPLY_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits & ~(1 << 10)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        DISABLE_IMAGE_IN_HTML_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits & ~(1 << 14)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        ENABLE_IMAGE_IN_HTML_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits | (1 << 14)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        DISABLE_DELETE_MAIL_PERMANENT_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits | (1 << 3)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        ENABLE_DELETE_MAIL_PERMANENT_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits & ~(1 << 3)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        TIMEZONE_SQL = new StringBuilder().append("UPDATE user ")
                .append("SET timeZone = ? ").append("WHERE imapLogin=?")
                .toString();
        LANGUAGE_SQL = new StringBuilder().append("UPDATE user ")
                .append("SET preferredLanguage = ? ")
                .append("WHERE imapLogin=?").toString();
        DB_WRITE_POOL_SQL = new StringBuilder()
                .append("SELECT url, db_schema, login, password ")
                .append("FROM configdb.login2context AS a, ")
                .append("configdb.context_server2db_pool AS b, ")
                .append("configdb.db_pool AS c ")
                .append("WHERE a.login_info=? ").append("and b.cid=a.cid ")
                .append("and c.db_pool_id=b.write_db_pool_id").toString();
    }
    private static final String NONE_STRING = "none";
    private static final String DEFAULT_IMAP_URL = "imap://localhost:25";
    private static final String MYSQL_JDBC_DRIVER = "com.mysql.jdbc.Driver";

    private static Logger logger = Logger.getLogger(OXSettingsMySQLCRUD.class);

    // MySQL URL for the server connection
    private String mysqlURL;
    private String userName;
    private String password;

    // Server connection used for accessing configdb database
    private Connection connection;

    // Sharded mysql db connection
    private Connection shardConnection;

    /**
     * Constructor
     * 
     * @throws Exception
     */
    public OXSettingsMySQLCRUD(String oxMySQLURL, String userName,
            String password) throws Exception {
        try {
            this.mysqlURL = oxMySQLURL;
            this.userName = userName;
            this.password = password;
            Class.forName(MYSQL_JDBC_DRIVER);

            // connect to the database using replication driver
            connection = DriverManager.getConnection(oxMySQLURL, userName,
                    password);
            if (connection != null) {
                connection.setAutoCommit(false);
                if (logger.isDebugEnabled()) {
                    logger.debug("OXSettingsCRUD object created.");
                }
            } else {
                throw new Exception(
                        "Error while connecting to OX MySQL database");
            }
        } catch (final CommunicationsException e) {
            logger.error(
                    "CommunicationsException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } catch (final MySQLNonTransientConnectionException e) {
            logger.error(
                    "MySQLNonTransientConnectionException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } catch (final Exception e) {
            logger.error("Exception while connecting to OX MySQL database", e);
            throw new MxOSException(ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void close() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Override
    public void commit() throws ApplicationException {
        try {
            shardConnection.commit();
        } catch (final Exception e) {
            logger.error("Error while commit to OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void createSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    private java.sql.ResultSet executeQuery(List<String> params, String sql)
            throws SQLException, ApplicationException {
        java.sql.PreparedStatement st;
        ResultSet result = null;
        int retryCount = 0;
        do {
            try {
                Thread.sleep(OX_CONNECTION_RETRY_INTERVAL);
                if (connection == null) {
                    connection = DriverManager.getConnection(mysqlURL,
                            userName, password);
                    if (connection != null) {
                        connection.setAutoCommit(false);
                        if (logger.isDebugEnabled()) {
                            logger.debug("OXSettingsMySQL connection created.");
                        }
                    } else {
                        retryCount++;
                        continue;
                    }
                }

                st = connection.prepareStatement(sql);
                int paramIndex = 1;
                for (String param : params) {
                    st.setString(paramIndex++, param);
                }
                result = st.executeQuery();
                retryCount = OX_CONNECTION_RETRY_COUNT;

            } catch (final CommunicationsException e) {
                logger.info("SQL CommunicationsException while OXSettingsMySQL connection creation to MYSQL. Trying to reconnect");
                connection = null;
                retryCount++;
            } catch (InterruptedException e) {
                logger.error("Error while retrying for the MYSQL Connection");
                throw new ApplicationException(
                        ErrorCode.OXS_CONNECTION_ERROR.name(),
                        "Error while retrying for the MYSQL Connection");
            }
        } while (retryCount < OX_CONNECTION_RETRY_COUNT);

        if (result == null) {
            logger.error("Error while calling update on OX MySQL database");
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name());
        }
        return result;
    }

    private java.sql.PreparedStatement executeUpdate(List<String> params,
            String sql) throws SQLException, ApplicationException {
        java.sql.PreparedStatement st;
        int result;
        st = shardConnection.prepareStatement(sql);

        int paramIndex = 1;
        for (String param : params) {
            st.setString(paramIndex++, param);
        }

        result = st.executeUpdate();
        if (result == -1) {
            logger.error("Error while calling update on OX MySQL database");
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name());
        }
        return st;
    }

    /**
     * Get connection to shard database.
     * 
     * @param mxosRequestState
     * @return
     * @throws MxOSException
     */
    private Connection getShardConnection(MxOSRequestState mxosRequestState)
            throws MxOSException {
        java.sql.PreparedStatement st = null;
        ResultSet rs = null;
        String oxMySQLURL = null;
        String oxMySQLUser = null;
        String oxMySQLPassword = null;
        if (logger.isDebugEnabled()) {
            logger.debug("OXSettingsCRUD getUserDatabaseConnection called.");
        }
        try {
            List<String> params = new ArrayList<String>();
            String email = mxosRequestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            String userName = ActionUtils.getArrayFromEmail(email)[0];
            params.add(userName);
            rs = executeQuery(params, DB_WRITE_POOL_SQL);

            while (rs.next()) {
                oxMySQLURL = rs.getString(1);
                if (oxMySQLURL == null) {
                    // the user is not provisioned in OX, throw exception
                    logger.error("Error while retrieving jdbcUrl from OX MySQL database, "
                            + "pls check if user is provisioned in AppSuite.");
                    throw new ApplicationException(
                            ErrorCode.OXS_CONNECTION_ERROR.name());
                }
                if (oxMySQLURL.contains("localhost")) {
                    if (mysqlURL.contains(rs.getString(2))) {
                        // reuse existing connection
                        return connection;
                    } else {
                        // connect to actual user database on same localhost
                        oxMySQLURL = mysqlURL.substring(mysqlURL
                                .lastIndexOf(MxOSConstants.FORWARD_SLASH) + 1)
                                + rs.getString(2);
                    }
                } else {
                    // connect to new mysql jdbc url
                    oxMySQLURL = oxMySQLURL
                            .substring(
                                    0,
                                    oxMySQLURL
                                            .lastIndexOf(MxOSConstants.FORWARD_SLASH) + 1)
                            + rs.getString(2);
                }

                oxMySQLUser = rs.getString(3);
                oxMySQLPassword = rs.getString(4);
            }
        } catch (final MySQLNonTransientConnectionException e) {
            logger.error("SQLError while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } catch (final CommunicationsException e) {
            logger.info(
                    "SQL CommunicationsException while update to OX MySQL database",
                    e);
        } catch (final Exception e) {
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (final Exception e) {
                logger.error("Error while closing rs, and st", e);
                throw new ApplicationException(
                        ErrorCode.OXS_CONNECTION_ERROR.name(), e);
            }
        }

        try {
            Class.forName(MYSQL_JDBC_DRIVER);

            // connect to the database using replication driver
            // TODO - use connection pool based on keyed object pool
            connection = DriverManager.getConnection(oxMySQLURL, oxMySQLUser,
                    oxMySQLPassword);
            if (connection != null) {
                connection.setAutoCommit(false);
                if (logger.isDebugEnabled()) {
                    logger.debug("OXSettingsCRUD object created.");
                }
            } else {
                throw new Exception(
                        "Error while connecting to OX MySQL database");
            }
        } catch (final CommunicationsException e) {
            logger.error(
                    "CommunicationsException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } catch (final MySQLNonTransientConnectionException e) {
            logger.error(
                    "MySQLNonTransientConnectionException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } catch (final Exception e) {
            logger.error("Exception while connecting to OX MySQL database", e);
        }
        return connection;
    }

    @Override
    public String readSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void rollback() throws ApplicationException {
        try {
            shardConnection.rollback();
        } catch (final Exception e) {
            logger.error("Error while rollback to OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void updateSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("OXSettingsCRUD updateSetting called.");
        }
        shardConnection = getShardConnection(mxosRequestState);
        java.sql.PreparedStatement st = null;
        try {
            IBackendState backendState = mxosRequestState.getBackendState()
                    .get(MailboxDBTypes.ldap.name());
            Attributes attrs = ((LDAPBackendState) backendState).attributes;
            List<String> params = new ArrayList<String>();
            String email = mxosRequestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            String userName = ActionUtils.getArrayFromEmail(email)[0];

            if (attrs != null && attrs.size() > 0) {
                Attribute attr = attrs.get(LDAPMailboxProperty.netmaillocale
                        .name());
                if (attr != null) {
                    String locale = attr.get().toString();
                    params.clear();
                    params.add(locale);
                    params.add(userName);
                    st = executeUpdate(params, LANGUAGE_SQL);
                }
                attr = attrs.get(LDAPMailboxProperty.msgtimezone.name());
                if (attr != null) {
                    String locale = attr.get().toString();
                    params.clear();
                    params.add(locale);
                    params.add(userName);
                    st = executeUpdate(params, TIMEZONE_SQL);
                }
                attr = attrs.get(LDAPMailboxProperty.netmailusetrash.name());
                if (attr != null) {
                    String sql;
                    String netMailUseTrash = attr.get().toString();
                    if (netMailUseTrash.equals("1")) {
                        sql = ENABLE_DELETE_MAIL_PERMANENT_SQL;
                    } else {
                        sql = DISABLE_DELETE_MAIL_PERMANENT_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    st = executeUpdate(params, sql);
                }
                attr = attrs
                        .get(LDAPMailboxProperty.netmailmsgviewprivacyfilter
                                .name());
                if (attr != null) {
                    String sql;
                    String netmailmsgviewprivacyfilter = attr.get().toString();
                    if (netmailmsgviewprivacyfilter.equals("TRUE")) {
                        sql = DISABLE_IMAGE_IN_HTML_SQL;
                    } else {
                        sql = ENABLE_IMAGE_IN_HTML_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    st = executeUpdate(params, sql);
                }
                attr = attrs
                        .get(LDAPMailboxProperty.mailincludeoriginal.name());
                if (attr != null) {
                    String sql;
                    String mailIncludeOriginal = attr.get().toString();
                    if (mailIncludeOriginal.equals("1")) {
                        sql = INCLUDE_ORIGINAL_IN_REPLY_SQL;
                    } else {
                        sql = EXCLUDE_ORIGINAL_FROM_REPLY_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    st = executeUpdate(params, sql);
                }
                attr = attrs.get(LDAPMailboxProperty.mailfrom.name());
                if (attr != null) {
                    String sql = FROM_ADDRESS_SQL;
                    String mailFrom = attr.get().toString();
                    params.clear();
                    params.add(mailFrom);
                    params.add(userName);
                    st = executeUpdate(params, sql);
                }
                attr = attrs.get(LDAPMailboxProperty.netMailAutoCollect.name());
                if (attr != null) {
                    String sql = "";
                    String netMailAutoCollect = attr.get().toString();
                    if (netMailAutoCollect.isEmpty()
                            || netMailAutoCollect.equals(NONE_STRING)) {
                        sql = AUTO_COLLECT_CONTACTS_WHILE_READING_SQL;
                    } else if (netMailAutoCollect.equals("all")) {
                        sql = AUTO_COLLECT_CONTACTS_WHILE_ALL_MAIL_SQL;
                    } else if (netMailAutoCollect.equals("to")) {
                        sql = AUTO_COLLECT_CONTACTS_WHILE_SENDING_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    st = executeUpdate(params, sql);
                }
                attr = attrs.get(LDAPMailboxProperty.mailreplyto.name());
                if (attr != null) {
                    String mailreplyto = attr.get().toString();
                    params.clear();
                    params.add(mailreplyto);
                    params.add(userName);
                    st = executeUpdate(params, REPLY_TO_SQL);
                }
                attr = attrs
                        .get(LDAPMailboxProperty.netmailweblinewidth.name());
                if (attr != null) {
                    String netmailweblinewidth = attr.get().toString();
                    if (StringUtils.isNumeric(netmailweblinewidth)) {
                        params.clear();
                        params.add(netmailweblinewidth);
                        params.add(userName);
                        st = executeUpdate(params, LINE_WRAP_SQL);
                    }
                }
                attr = attrs.get(LDAPMailboxProperty.netmailpopacctdetails
                        .name());
                if (attr != null) {
                    final NamingEnumeration<? extends Object> valueEnum = attr
                            .getAll();

                    List<MailAccount> mailAcctList = new ArrayList<MailAccount>();

                    while (valueEnum.hasMore()) {
                        String entry = (String) valueEnum.next();
                        mailAcctList.add(LDAPUtils.getMailAccounts(entry));
                    }

                    // delete all entries from user_mail_account
                    params.clear();
                    params.add(userName);
                    st = executeUpdate(params,
                            DELETE_EXTERNAL_INCOMING_ACCOUNTS_SQL);
                    st = executeUpdate(params,
                            DELETE_EXTERNAL_OUTGOING_ACCOUNTS_SQL);
                    st = executeUpdate(params,
                            DELETE_EXTERNAL_ACCOUNTS_PROPERTIES_SQL);

                    // store inbound external accounts
                    for (MailAccount mailAccount : mailAcctList) {
                        // insert into user_mail_account
                        int result;
                        st = shardConnection
                                .prepareStatement(INSERT_EXTERNAL_INCOMING_ACCOUNT_SQL);

                        st.setInt(1, mailAccount.getAccountId());
                        st.setString(2, mailAccount.getAccountName());
                        if (mailAccount.getAccountType() != null) {
                            st.setString(
                                    3,
                                    new StringBuilder(mailAccount
                                            .getAccountType().name())
                                            .append("://")
                                            .append(mailAccount
                                                    .getServerAddress())
                                            .append(":")
                                            .append(mailAccount.getServerPort())
                                            .toString());
                        } else {
                            st.setString(3, DEFAULT_IMAP_URL);
                        }
                        if (mailAccount.getEmailAddress() != null) {
                            st.setString(4, mailAccount.getEmailAddress());
                        } else {
                            st.setString(4, "");
                        }
                        st.setString(5, mailAccount.getAccountPassword());
                        if (mailAccount.getEmailAddress() != null) {
                            st.setString(6, mailAccount.getEmailAddress());
                        } else {
                            st.setString(6, "");
                        }
                        st.setInt(7, 0);
                        if (mailAccount.getTrashFolderName() != null) {
                            st.setString(8, mailAccount.getTrashFolderName());
                        } else {
                            st.setString(8, "Trash");
                        }
                        if (mailAccount.getEmailAddress() != null) {
                            st.setString(9, mailAccount.getSendFolderName());
                        } else {
                            st.setString(9, "SentMail");
                        }
                        if (mailAccount.getEmailAddress() != null) {
                            st.setString(10, mailAccount.getDraftsFolderName());
                        } else {
                            st.setString(10, "Drafts");
                        }
                        if (mailAccount.getEmailAddress() != null) {
                            st.setString(11, mailAccount.getSpamFolderName());
                        } else {
                            st.setString(11, "Junk Mail");
                        }
                        st.setString(12, mailAccount.getFromAddressInReply());
                        st.setString(13, userName);

                        result = st.executeUpdate();
                        if (result == -1) {
                            logger.error("Error while calling update on OX MySQL database");
                            throw new ApplicationException(
                                    ErrorCode.OXS_CONNECTION_ERROR.name());
                        }

                        // store outbound external account settings
                        st = shardConnection
                                .prepareStatement(INSERT_EXTERNAL_OUTGOING_ACCOUNT_SQL);

                        st.setInt(1, mailAccount.getAccountId());
                        st.setString(2, mailAccount.getAccountName());
                        StringBuilder smtpUrl = new StringBuilder();
                        if (mailAccount.getSmtpSSLEnabled() == BooleanType.YES) {
                            smtpUrl.append("smtps://");
                        } else {
                            smtpUrl.append("smtp://");
                        }
                        if (mailAccount.getMailSendSMTPAddress() != null) {
                            smtpUrl.append(mailAccount.getMailSendSMTPAddress());
                        } else {
                            smtpUrl.append("localhost");
                        }
                        smtpUrl.append(":");
                        if (mailAccount.getMailSendSMTPPort() != null) {
                            smtpUrl.append(mailAccount.getMailSendSMTPPort());
                        } else {
                            smtpUrl.append("25");
                        }
                        st.setString(3, smtpUrl.toString());
                        if (mailAccount.getEmailAddress() != null) {
                            st.setString(4, mailAccount.getEmailAddress());
                        } else {
                            st.setString(4, "");
                        }
                        st.setString(5, mailAccount.getAccountPassword());
                        if (mailAccount.getAccountUserName() != null) {
                            st.setString(6, mailAccount.getEmailAddress());
                        } else {
                            st.setString(6, "");
                        }
                        st.setString(7, userName);

                        result = st.executeUpdate();
                        if (result == -1) {
                            logger.error("Error while calling update on OX MySQL database");
                            throw new ApplicationException(
                                    ErrorCode.OXS_CONNECTION_ERROR.name());
                        }
                    }

                    // update sequence_mail_service with last accountId
                    st = shardConnection
                            .prepareStatement(UPDATE_EXTERNAL_ACCOUNT_ID_SQL);

                    st.setInt(1, mailAcctList.get(mailAcctList.size() - 1)
                            .getAccountId());
                    st.setString(2, userName);

                    int result = st.executeUpdate();
                    if (result == -1) {
                        logger.error("Error while calling update on OX MySQL database");
                        throw new ApplicationException(
                                ErrorCode.OXS_CONNECTION_ERROR.name());
                    }
                }
                attr = attrs.get(LDAPMailboxProperty.mailalternateaddress
                        .name());
                if (attr != null) {
                    params.clear();
                    params.add(userName);
                    st = executeUpdate(params, DELETE_ALL_ALIAS_SQL);
                    for (int i = 0; i < attr.size(); i++) {
                        String alias = attr.get(i).toString();
                        params.clear();
                        params.add(alias);
                        params.add(userName);
                        st = executeUpdate(params, INSERT_ALIAS_SQL);
                    }
                    params.clear();
                    params.add(email);
                    params.add(userName);
                    st = executeUpdate(params, INSERT_ALIAS_SQL);
                }
                commit();
            }
        } catch (final MySQLNonTransientConnectionException e) {
            rollback();
            logger.error("SQLError while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } catch (final CommunicationsException e) {
            rollback();
            logger.info(
                    "SQL CommunicationsException while update to OX MySQL database",
                    e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (final Exception e) {
                logger.error("Error while closing rs, and st", e);
                throw new ApplicationException(
                        ErrorCode.OXS_CONNECTION_ERROR.name(), e);
            }
        }
    }
}
