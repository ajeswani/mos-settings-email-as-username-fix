/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to update blocked senders list.
 *
 * @author mxos-dev
 */
public class UpdateBlockedSendersList implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(UpdateBlockedSendersList.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateBlockedSendersList action start."));
        }
        try {
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            final String oldBlockedSender = requestState.getInputParams()
                    .get(MailboxProperty.oldBlockedSender.name()).get(0);
            final String newBlockedSender = requestState.getInputParams()
                    .get(MailboxProperty.newBlockedSender.name()).get(0);

            List<String> blockedSendersList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.blockedSendersList);

            if (blockedSendersList == null) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_BLOCKED_SENDER.name());
            }
            if (!blockedSendersList.contains(oldBlockedSender.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_BLOCKED_SENDER_NOT_EXIST
                                .name());
            }
            if (blockedSendersList.remove(oldBlockedSender.toLowerCase())) {
                blockedSendersList.add(newBlockedSender.toLowerCase());
                final String[] blockedSenderArray = blockedSendersList
                        .toArray(new String[blockedSendersList.size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.blockedSendersList,
                                blockedSenderArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_BLOCKED_SENDER.name());
            }

        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while update blocked senders list.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_BLOCKED_SENDERS_POST.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateBlockedSendersList action end."));
        }
    }
}
