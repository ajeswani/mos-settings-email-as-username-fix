/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set future delivery enabled.
 *
 * @author mxos-dev
 */
public class SetFutureDeliveryEnabled implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetFutureDeliveryEnabled.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetFutureDeliveryEnabled action start."));
        }
        String futureDeliveryEnabled = requestState.getInputParams()
                .get(MailboxProperty.futureDeliveryEnabled.name()).get(0);

        try {
            if (futureDeliveryEnabled != null
                    && !futureDeliveryEnabled.equals("")) {
                futureDeliveryEnabled = Integer.toString(MxosEnums.BooleanType
                        .fromValue(futureDeliveryEnabled).ordinal());
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.futureDeliveryEnabled,
                            futureDeliveryEnabled);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set future delivery enabled.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_FUTURE_DELIVERY_ENABLED
                            .name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetFutureDeliveryEnabled action end."));
        }
    }
}
