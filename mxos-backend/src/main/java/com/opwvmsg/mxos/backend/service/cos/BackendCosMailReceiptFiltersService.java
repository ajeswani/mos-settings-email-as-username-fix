/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.service.cos;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptFiltersService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Mailbox mailReceipt filter operations interface which will be exposed to the
 * client. This interface is responsible for doing mailReceipt filter related
 * operations (like Create, Read, Update, Delete, etc.) directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class BackendCosMailReceiptFiltersService implements
        ICosMailReceiptFiltersService {

    private static Logger logger = Logger
            .getLogger(BackendCosMailReceiptFiltersService.class);

    /**
     * Default Constructor.
     */
    public BackendCosMailReceiptFiltersService() {
        logger.info("BackendCosMailReceiptFiltersService created...");
    }

    @Override
    public Filters read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.CosMailReceiptService,
                    Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.CosMailReceiptFiltersService,
                    Operation.GET, t0, StatStatus.pass);
            return ((MailReceipt) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailReceipt)).getFilters();
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.CosMailReceiptFiltersService,
                    Operation.GET, t0, StatStatus.fail);
            throw e;
        }
    }
}
