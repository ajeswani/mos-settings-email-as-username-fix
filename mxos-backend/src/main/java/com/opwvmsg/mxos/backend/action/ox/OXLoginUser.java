/*
 * /* Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied. $Id:$
 */

package com.opwvmsg.mxos.backend.action.ox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to login user.
 * 
 * @author mxos-dev
 */
public class OXLoginUser implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(OXLoginUser.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Login user action start."));
        }
        if (!requestState.getInputParams()
                .containsKey(ExternalProperty.entity.name())) {
            final String errMsg = "Param 'entity' is missing. Cannot Authenticate";
            logger.warn(errMsg);
            throw new MxOSException("code", errMsg);
        }
        final Entity entity = Entity.fromValue(requestState.getInputParams()
                .get(ExternalProperty.entity.name()).get(0));
        String backend = null;
        switch(entity) {
            case ADDRESS_BOOK:
                backend = MxOSConfig.getAddressBookBackend();
                break;
            case TASKS:
                backend = MxOSConfig.getTasksBackend();
                break;
        }
        // folderId only applies for OX backend
        ExternalSession session = null;
        if (null != backend && backend.equals(
                AddressBookDBTypes.ox.name())) {
            switch(entity) {
                case ADDRESS_BOOK:
                    session = loginAddressBookCRUD(requestState);
                    break;
                case TASKS:
                    session = loginTasksCRUD(requestState);
                    break;
            }
        } else {
            // Return an empty session for backward compatibility,
            // if this api is called by any client.
            session = new ExternalSession();
            session.setCookieString("");
            session.setSessionId("");
        }
        requestState.getDbPojoMap().setProperty(MxOSPOJOs.session, session);
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("LoginUser action end."));
        }
    }
    /**
     * Login using AddressBook CRUD pool.
     * @param requestState
     * @return
     * @throws MxOSException
     */
    private ExternalSession loginAddressBookCRUD(final MxOSRequestState requestState) throws MxOSException  {
        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;
        try {
            addressBookCRUDPool = MxOSApp.getInstance()
                    .getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();

            return addressBookCRUD.login(requestState);
        } catch (AddressBookException e) {
            logger.error("Error while login.", e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_UNABLE_TO_LOGIN, e);
            throw new MxOSException("Error while login.", e);
        } catch (final Exception e) {
            logger.error("Error while login.", e);
            throw new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
    }
    /**
     * Login using Tasks CRUD pool.
     * @param requestState
     * @return
     * @throws MxOSException
     */
    private ExternalSession loginTasksCRUD(final MxOSRequestState requestState) throws MxOSException  {
        ICRUDPool<ITasksCRUD> tasksCRUDPool = null;
        ITasksCRUD tasksCRUD = null;
        try {
            tasksCRUDPool = MxOSApp.getInstance().getTasksCRUD();
            tasksCRUD = tasksCRUDPool.borrowObject();

            return tasksCRUD.login(requestState);
        } catch (TasksException e) {
            logger.error("Error while login.", e);
            ExceptionUtils.createMxOSExceptionFromTasksException(
                    TasksError.TSK_UNABLE_TO_LOGIN, e);
            throw new MxOSException("Error while login.", e);
        } catch (final Exception e) {
            logger.error("Error while login.", e);
            throw new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } finally {
            try {
                if (tasksCRUDPool != null && tasksCRUD != null) {
                    tasksCRUDPool.returnObject(tasksCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
    }
}
