package com.opwvmsg.mxos.backend.crud.ox.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.IBackendState;

public class OXTasksBackendState implements IBackendState {
    public Map<String, String> attributes;
    public List<Map<String, String>> attributesList;

    public OXTasksBackendState() {
        attributes = new HashMap<String, String>();
        attributesList = new ArrayList<Map<String, String>>();
    }

    @Override
    public int getSize() {
        if (attributes != null) {
            return attributes.size();
        } else {
            return 0;
        }
    }

    public synchronized void initialize(int size) {
        for (int i = 0; i < size; i++) {
            attributesList.add(new HashMap<String, String>());
        }
    }
}
