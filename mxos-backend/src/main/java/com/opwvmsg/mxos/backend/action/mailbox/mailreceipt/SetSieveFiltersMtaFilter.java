/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to Set Sieve Filter.
 *
 * @author mxos-dev
 */
public class SetSieveFiltersMtaFilter implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetSieveFiltersMtaFilter.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSieveFiltersMtaFilter"
                    + " action start."));
        }
        String filter = requestState.getInputParams()
                .get(MailboxProperty.mtaFilter.name()).get(0);
        try {
            MxOSApp.getInstance()
                    .getMetaHelper()
                    .setAttribute(requestState, MailboxProperty.mtaFilter,
                            filter);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set seive filters.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                            .name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetSieveFiltersMtaFilter action end."));
        }
    }
}
