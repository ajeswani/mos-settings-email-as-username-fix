/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailstore;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set altmailsoftquotatotkb .
 * 
 * @author mxos-dev
 */
public class SetMobileMaxStorageSizeKBSoftLimit implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetMobileMaxStorageSizeKBSoftLimit.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMobileMaxStorageSizeKBSoftLimit action start."));
        }
        try {
            final String mobileMaxStorageSizeKBSoftLimit = requestState.getInputParams()
                    .get(MailboxProperty.mobileMaxStorageSizeKBSoftLimit.name()).get(0);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.mobileMaxStorageSizeKBSoftLimit,
                            mobileMaxStorageSizeKBSoftLimit);
        } catch (final NumberFormatException e) {
            logger.warn("Number Format error while mobileMaxStorageSizeKBSoftLimit.");
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT.name(), e);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set mobileMaxStorageSizeKBSoftLimit.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMobileMaxStorageSizeKBSoftLimit action end."));
        }
    }
}
