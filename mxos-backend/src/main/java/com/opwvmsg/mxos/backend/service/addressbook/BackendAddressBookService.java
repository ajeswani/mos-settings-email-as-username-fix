/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IAddressBookService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * AddressBook service exposed to client which is responsible for doing basic
 * addressbook related activities e.g create addressbook, delete addressbook,
 * etc. directly in the database.
 * 
 * @author mxos-dev
 */
public class BackendAddressBookService implements IAddressBookService {

    private static Logger logger = Logger
            .getLogger(BackendAddressBookService.class);

    /**
     * Default Constructor.
     */
    public BackendAddressBookService() {
        logger.info("BackendAddressBookService created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AddressBookService, Operation.PUT);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.AddressBookService, Operation.PUT, t0,
                    StatStatus.pass);
            return (Long) mxosRequestState.getDbPojoMap().getPropertyAsObject(
                    MxOSPOJOs.mailboxId);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AddressBookService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AddressBookService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.AddressBookService, Operation.DELETE,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AddressBookService, Operation.DELETE,
                    t0, StatStatus.fail);
            throw e;
        }
    }
}
