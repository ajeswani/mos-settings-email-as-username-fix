/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ox.settings;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

/**
 * This class provides connection pool for HTTP REST.
 * 
 * @author
 */
public class OXSettingsHttpConnectionPool implements ICRUDPool<ISettingsCRUD> {
    private static class RestConnectionPoolHolder {
        public static OXSettingsHttpConnectionPool instance = new OXSettingsHttpConnectionPool();
    }

    private static Logger logger = Logger
            .getLogger(OXSettingsHttpConnectionPool.class);

    public static final String MXOS_BASE_URL = "mxosBaseUrl";
    public static final String MXOS_CUSTOM = "custom";

    /**
     * Method to get Instance of RestConnectionPool object.
     * 
     * @return RestConnectionPool object
     * @throws Exception Exception
     */
    public static OXSettingsHttpConnectionPool getInstance() {
        return RestConnectionPoolHolder.instance;
    }

    private GenericObjectPool<OXSettingsHttpCRUD> objPool;

    /**
     * Constructor.
     * 
     * Configure mxos-host in hosts file.
     * 
     * @throws Exception Exception.
     */
    public OXSettingsHttpConnectionPool() {
        createMxosObjectPool();
    }

    /**
     * Method to borrow connection.
     * 
     * @return connection connection
     * @throws Exception in case no connection is available.
     */
    @Override
    public OXSettingsHttpCRUD borrowObject() throws MxOSException {
        OXSettingsHttpCRUD obj = null;
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    /**
     * Create mxosObjectPool to perform CRUD operation using REST.
     * 
     * @throws Exception Exception
     */
    private void createMxosObjectPool() {
        try {
            String baseUrl = null;
            if (System.getProperties().containsKey("oxHttpURL")) {
                baseUrl = System.getProperty("oxHttpURL");
            } else {
                // Default base url
                // to support this we should set mxos-host in /etc/hosts file.
                baseUrl = "http://localhost/ajax";
            }
            System.out.println("MxOS URL = " + baseUrl);
            int mxosMaxConnections;
            if (System.getProperties().containsKey(
                    SystemProperty.mxosMaxConnections.name())) {
                mxosMaxConnections = Integer.parseInt(System
                        .getProperty(SystemProperty.mxosMaxConnections.name()));
            } else {
                // Default max connections
                mxosMaxConnections = 10;
            }
            System.out.println("MxOS Max Connections = " + mxosMaxConnections);
            objPool = new GenericObjectPool<OXSettingsHttpCRUD>(
                    new OXSettingsHttpFactory(baseUrl), mxosMaxConnections);
            objPool.setMaxIdle(-1);
            System.out
                    .println("RESTMxosObjectPool Created with default values...");
            initializeJMXStats(mxosMaxConnections);
        } catch (Exception e) {
            System.out
                    .println("Problem occured while creating RESTMxosObjectPool with default values...");
            e.printStackTrace();
        }
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_OXHTTPSETTINGS.decrement();
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_OXHTTPSETTINGS.increment();
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.OXHTTPSETTINGS.setCount(count);
    }

    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub

    }

    /**
     * Method to return the connection back.
     * 
     * @param restCRUD restCRUD
     */

    @Override
    public void returnObject(ISettingsCRUD settingsCRUD) throws MxOSException {
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            objPool.returnObject((OXSettingsHttpCRUD) settingsCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while returning object.", e);
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }
    }
}
