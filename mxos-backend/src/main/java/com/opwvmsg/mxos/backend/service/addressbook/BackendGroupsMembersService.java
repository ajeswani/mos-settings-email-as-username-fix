/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsMembersService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Groups member service exposed to client which is responsible 
 * for doing basic activities e.g create, read, update, delete
 * 
 * @author mxos-dev
 */
public class BackendGroupsMembersService implements
 IGroupsMembersService {

    private static Logger logger = Logger
            .getLogger(BackendGroupsMembersService.class);

    /**
     * Default Constructor.
     */
    public BackendGroupsMembersService() {
        logger.info("BackendGroupsMembersService Service created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsMemberService, Operation.PUT);

            ActionUtils.setAddressBookBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.PUT, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
        return 0;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsMemberService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.DELETE,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.DELETE,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsMemberService,
                    Operation.DELETEALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsMemberService,
                    Operation.DELETEALL, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsMemberService,
                    Operation.DELETEALL, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Member read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsMemberService, Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.GET, t0,
                    StatStatus.pass);

            return ((Member) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.members));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public List<Member> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsMemberService,
                    Operation.GETALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.GETALL,
                    t0, StatStatus.pass);

            return ((List<Member>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allMembers));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsMemberService, Operation.GETALL,
                    t0, StatStatus.fail);
            throw e;
        }
    }

}
