/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;

/**
 * LDAP Search connection pool factory to access LDAP Data such as Mailbox
 * Profile, Domain and COS. This is required by Apache object pool.
 *
 * @author mxos-dev
 */
class LdapSearchCRUDFactory extends
        BasePoolableObjectFactory<LdapMailboxCRUD> {
    protected static Logger logger =
        Logger.getLogger(LdapSearchCRUDFactory.class);
    private Hashtable<String, String> env;

    @Override
    public LdapMailboxCRUD makeObject() throws Exception {
        return new LdapMailboxCRUD(this.env);
    }

    @Override
    public void destroyObject(LdapMailboxCRUD mailboxCRUD)
            throws Exception {
        mailboxCRUD.close();
    }

    @Override
    public boolean validateObject(final LdapMailboxCRUD ldapCRUD) {
        final boolean status = ldapCRUD.isConnected();
        // Reset the entire pool if one connection failed
        // This impact the performance in re-establishing the connection
        // for all the objects in the pool.
        if (!status) {
            try {
                ICRUDPool<IMailboxCRUD> provCRUDPool = MxOSApp.getInstance()
                        .getMailboxSearchCRUD();
                if (provCRUDPool != null) {
                    provCRUDPool.resetPool();
                }
            } catch (Exception e) {
                logger.error("Error while resetting LDAP Search pools.", e);
            }
        }
        return status;
    }
    

    public void setEnv(Hashtable<String, String> env) {
        this.env = env;
    }
}
