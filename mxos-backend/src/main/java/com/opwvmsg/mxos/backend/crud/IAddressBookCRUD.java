package com.opwvmsg.mxos.backend.crud;

import java.io.IOException;
import java.util.List;
import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;

/**
 * Address Book APIs for interfacing with Open-Xchange (OX) or 
 * WebEdge (WE) server.
 * 
 * @author ajeswani
 * 
 */
public interface IAddressBookCRUD extends ITransaction {

    /**
     * API to delete address book.
     * 
     * @param username
     * @param session
     * @return
     * @throws AddressBookException
     */
    public void deleteAddressBook(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to add or update a contact.
     * 
     * @param username
     * @param contact
     * @param session
     * @return
     * @throws AddressBookException
     */
    public String createContact(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to add multiple contacts.
     * 
     * @param username
     * @param contacts
     * @param session
     * @return
     * @throws AddressBookException
     */
    public List<String> createMultipleContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to move multiple contacts.
     * 
     * @param username
     * @param contacts
     * @param session
     * @return
     * @throws AddressBookException
     */
    public void moveMultipleContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to move multiple groups.
     * 
     * @param username
     * @param groups
     * @param session
     * @return
     * @throws AddressBookException
     */
    public void moveMultipleGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException;
    
    /**
     * API to get list of Contacts details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public List<Contact> readAllContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to delete a contact.
     * 
     * @param username
     * @param contact
     * @param session
     * @return
     * @throws AddressBookException
     */
    public void deleteContact(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * Login API logs the user into backend OX server.
     * 
     * @param params
     * @param url
     * @return
     * @throws AddressBookException
     * @throws IOException
     */
    public ExternalSession login(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to get Contacts Base details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public ContactBase readContactBase(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to get list of Contacts Base details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public List<ContactBase> readAllContactBase(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to get Contacts folder id.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public Integer readContactFolderId(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to get Contacts Name details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public Name readContactName(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to readContactPersonalInformation.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * 
     */
    public PersonalInfo readContactsPersonalInfo(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to readContactPersonalInfoEvents.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * 
     */
    public List<Event> readContactPersonalInfoEvents(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    // add group members

    // list

    // list groups

    // delete contact

    // get contact details

    // remove group contact

    // search contacts

    /**
     * API to read Contacts PersonalInfo Address.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * 
     */
    public Address readContactsPersonalInfoAddress(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to read Contacts PersonalInfo Communication.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * 
     */
    public Communication readContactsPersonalInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to read Contacts WorkInfo.
     * 
     * @param params
     * @return WorkInfo
     * @throws AddressBookException
     * 
     */
    public WorkInfo readContactsWorkInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to read Contacts WorkInfo Address.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * 
     */
    public Address readContactsWorkInfoAddress(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to read Contacts WorkInfo Communication.
     * 
     * @param params
     * @return Communication
     * @throws AddressBookException
     * @throws IOException from backend OX server.
     * 
     */
    public Communication readContactsWorkInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to read Contacts Image.
     * 
     * @param params
     * @return Image
     * @throws AddressBookException
     * @throws IOException from backend OX server.
     * 
     */
    public Image readContactsImage(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to read Contacts actual Image.
     * 
     * @param params
     * @return byte[]
     * @throws AddressBookException
     * @throws IOException from backend OX server.
     * 
     */
    public byte[] readContactsActualImage(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update Contacts Base details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void updateContactBase(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update Contacts Name details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void updateContactName(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update Contacts Personal Info details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void updateContactsPersonalInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update Contacts Personal Info Address details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void updateContactsPersonalInfoAddress(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to update Contacts Personal Info Communication details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void updateContactsPersonalInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to update Contacts Work Info details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    public void updateContactsWorkInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update Contacts Work Info Address details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    public void updateContactsWorkInfoAddress(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update Contacts Work Info Communication details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    public void updateContactsWorkInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to update Contacts Image details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    public void updateContactsImage(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update Contacts PersonalInfo events date details.
     * 
     * @param params
     * @return
     * @throws AddressBookException
     */
    public void updateContactsPersonalInfoEvent(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to read Contacts WorkInfo Address.
     * 
     * @param params
     * @return Address
     * @throws AddressBookException
     * @throws IOException from backend OX server.
     * 
     *             API to get Validate UserId.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     * @throws IOException
     */
    public void validateUser(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to create a group.
     * 
     * @param username
     * @param contact
     * @param session
     * @return
     * @throws AddressBookException
     */
    public String createGroup(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to create multiple groups.
     * 
     * @param username
     * @param group
     * @param session
     * @return
     * @throws AddressBookException
     */
    public List<String> createMultipleGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException;
    
    /**
     * API to delete a group.
     * 
     * @param username
     * @param contact
     * @param session
     * @return
     * @throws AddressBookException
     */
    public void deleteGroup(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to delete all groups.
     * 
     * @param username
     * @param contact
     * @param session
     * @return
     * @throws AddressBookException
     */
    public void deleteAllGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to get Groups Base details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public GroupBase readGroupsBase(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to update groups Base details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void updateGroupsBase(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to get Groups member details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public List<Member> readGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to delete Groups member details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void deleteGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to delete Groups member details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void deleteAllGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to add or update a group member.
     * 
     * @param username
     * @param contact
     * @param session
     * @return
     * @throws AddressBookException
     */
    public String createGroupMembers(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to get Groups external member details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public List<ExternalMember> readGroupsExternalMember(
            MxOSRequestState mxosRequestState) throws AddressBookException;

    /**
     * API to delete Groups member details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void deleteGroupsExternalMember(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to delete Groups member details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws AddressBookException
     */
    public void deleteAllGroupsExternalMember(MxOSRequestState mxosRequestState)
            throws AddressBookException;

    /**
     * API to create Group external members.
     * 
     * @param username
     * @param contact
     * @param session
     * @return
     * @throws AddressBookException
     */
    public String createGroupExternalMembers(MxOSRequestState mxosRequestState)
            throws AddressBookException;
    
    /**
     * API to list all the group base
     * 
     * @param username
     * @param contact
     * @return List<GroupBase>
     * @throws AddressBookException
     */
    public List<GroupBase> readAllGroupBase(
            MxOSRequestState mxosRequestState) throws AddressBookException;
    
    /**
     * Logout API logs the user out from backend OX server.
     * 
     * @param mxosRequestState
     * @throws AddressBookException
     */
    public void logout(MxOSRequestState mxosRequestState)
            throws AddressBookException;
    
}
