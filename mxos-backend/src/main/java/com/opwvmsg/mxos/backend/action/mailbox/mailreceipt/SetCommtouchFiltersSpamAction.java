/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set commtouch filters spam action.
 *
 * @author mxos-dev
 */
public class SetCommtouchFiltersSpamAction implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetCommtouchFiltersSpamAction.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCommtouchFiltersSpamAction action start."));
        }
        String spamAction = null;
        if (null != requestState.getInputParams().get(
                MailboxProperty.spamAction.name())) {
            spamAction = requestState.getInputParams()
                    .get(MailboxProperty.spamAction.name()).get(0);
        } else if (null != requestState.getInputParams().get(
                MailboxProperty.mailReceiptCommtouchSpamAction.name())) {
            spamAction = requestState.getInputParams()
                    .get(MailboxProperty.mailReceiptCommtouchSpamAction.name())
                    .get(0);
        }

        try {
            if (spamAction != null && !spamAction.equals("")) {
                spamAction = MxosEnums.CommtouchActionType
                        .fromValue(spamAction).toString();
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.commtouchSpamAction, spamAction);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set commontouch filters spam action.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_COMMTOUCH_FILTERS_SPAM_ACTION
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCommtouchFiltersSpamAction action end."));
        }
    }
}
