/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.webmailfeatures.addressbook;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.CreateContactsFromOutgoingEmails;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set WebMailFeatures.addressbook::SetCreateContactsFromOutgoingEmails.
 *
 * @author mxos-dev
 */
public class SetCreateContactsFromOutgoingEmails implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetCreateContactsFromOutgoingEmails.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCreateContactsFromOutgoingEmails action start."));
        }
        String createContactsFromOutgoingEmails = requestState.getInputParams()
                .get(MailboxProperty.createContactsFromOutgoingEmails.name())
                .get(0);
        String temp = createContactsFromOutgoingEmails;
        try {
            if (null != temp && !"".equals(temp)) {
                CreateContactsFromOutgoingEmails ccfoeEnum = CreateContactsFromOutgoingEmails
                        .fromValue(createContactsFromOutgoingEmails);

                switch (ccfoeEnum) {
                case DISABLED:
                    temp = "none";
                    break;

                case FOR_ALL_ADDRESSES:
                    temp = "all";
                    break;

                case ONLY_FOR_ADDRESSES_IN_TO_FIELD:
                    temp = "to";
                    break;
                }
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.createContactsFromOutgoingEmails,
                            temp);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set createContactsFromOutgoingEmails ", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_CREATE_CONTACTS_FROM_OUTGOING_EMAILS
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
      "SetCreateContactsFromOutgoingEmails action end."));
        }
    }
}
