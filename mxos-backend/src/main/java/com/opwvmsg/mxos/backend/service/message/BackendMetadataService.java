/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.message;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * MessageMetaData service exposed to client which is responsible for doing basic
 * MessageMetaData related activities.
 * 
 * @author mxos-dev
 */
public class BackendMetadataService implements IMetadataService {

    private static Logger logger = Logger
            .getLogger(BackendMetadataService.class);

    /**
     * Constructor.
     */
    public BackendMetadataService() {
        logger.info("BackendMessageMetaDataService created...");
    }

    @Override
    public Metadata read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MetadataService, Operation.GET);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.GET, t0,
                    StatStatus.pass);
            return (Metadata) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageMetaData);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Metadata> list(
            final Map<String, List<String>> inputParams) throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MetadataService, Operation.LIST);
            /*
             * mxosRequestState.getAdditionalParams().setProperty(
             * MessageProperty.searchquery, searchQuery);
             */
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.LIST, t0,
                    StatStatus.pass);
            return (Map<String, Metadata>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageMetaData);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.LIST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Metadata> listUIDs(
            final Map<String, List<String>> inputParams) throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MetadataService, Operation.LISTUID);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.LISTUID, t0,
                    StatStatus.pass);
            return (Map<String, Metadata>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageMetaData);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.LISTUID, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
    throws MxOSException{
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MetadataService, Operation.POST);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.POST, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }
    
    @Override
    public void updateMulti(final Map<String, List<String>> inputParams)
    throws MxOSException{
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MetadataService, Operation.UPDATEMULTI);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.POST, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }    

    @Override
    public Map<String, Metadata> readMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MetadataService,
                    Operation.GETMULTI);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.GETMULTI,
                    t0, StatStatus.pass);
            return (Map<String, Metadata>) mxosRequestState
            .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.messageMetaData);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.GETMULTI,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Map<String, Map<String, Metadata>> search(Map<String, List<String>> inputParams,
            SearchTerm searchTerm) throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MetadataService,
                    Operation.SEARCH);
            mxosRequestState.getAdditionalParams().setProperty(
                    MessageProperty.searchquery, searchTerm);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.SEARCH,
                    t0, StatStatus.pass);
            
            return (Map<String, Map<String, Metadata>>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageMetaData);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MetadataService, Operation.SEARCH,
                    t0, StatStatus.fail);
            throw e;
        }
    }
    
}
