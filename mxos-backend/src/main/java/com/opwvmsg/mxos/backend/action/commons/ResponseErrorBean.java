/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-data/src/main/java/com/openwave/mxos/data/ResponseErrorBean.java#1 $
 */
package com.opwvmsg.mxos.backend.action.commons;

/**
 * ResponseErrorBean class that contains the properties for error response.
 *
 * @author Aricent
 */
public class ResponseErrorBean {
    private static final long serialVersionUID = -6853412125675326144L;

    private String customErrorCode;

    private String mxosErrorCode;

    private String requestParams;

    private String operationType;

    private String shortMessage;

    private String longMessage;

    /**
     * @return the customErrorCode
     */
    public String getErrorCode() {
        if (customErrorCode != null && !customErrorCode.equals("")) {
            return customErrorCode;
        } else {
            return mxosErrorCode;
        }
    }

    /**
     * @param customErrorCode the customErrorCode to set
     */
    public void setCustomErrorCode(String customErrorCode) {
        this.customErrorCode = customErrorCode;
    }

    /**
     * @return the mxosErrorCode
     */
    public String getMxosErrorCode() {
        return mxosErrorCode;
    }

    /**
     * @param mxosErrorCode the mxosErrorCode to set
     */
    public void setMxosErrorCode(String mxosErrorCode) {
        this.mxosErrorCode = mxosErrorCode;
    }

    /**
     * @return the requestParams
     */
    public String getRequestParams() {
        return requestParams;
    }

    /**
     * @param requestParams the requestParams to set
     */
    public void setRequestParams(String requestParams) {
        this.requestParams = requestParams;
    }

    /**
     * @return the operationType
     */
    public String getOperationType() {
        return operationType;
    }

    /**
     * @param operationType the operationType to set
     */
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    /**
     * @return the shortMessage
     */
    public String getShortMessage() {
        return shortMessage;
    }

    /**
     * @param shortMessage the shortMessage to set
     */
    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    /**
     * @return the longMessage
     */
    public String getLongMessage() {
        return longMessage;
    }

    /**
     * @param longMessage the longMessage to set
     */
    public void setLongMessage(String longMessage) {
        this.longMessage = longMessage;
    }
}
