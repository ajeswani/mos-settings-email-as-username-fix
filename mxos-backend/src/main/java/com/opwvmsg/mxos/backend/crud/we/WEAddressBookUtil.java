/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.we;

import java.util.EnumMap;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.WEContactsProperty;

/**
 * 
 * 
 * @author mxos-dev
 * 
 */
public class WEAddressBookUtil {

    public static EnumMap<AddressBookProperty, WEContactsProperty> JSONToWE_AddressBook = new EnumMap<AddressBookProperty, WEContactsProperty>(
            AddressBookProperty.class);

    private static Logger logger = Logger.getLogger(WEAddressBookUtil.class);

    static {
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoFax,
                WEContactsProperty.HomeFax);
        JSONToWE_AddressBook.put(AddressBookProperty.companyName,
                WEContactsProperty.Company);
        JSONToWE_AddressBook.put(AddressBookProperty.contactId,
                WEContactsProperty.Id);
        JSONToWE_AddressBook.put(AddressBookProperty.department,
                WEContactsProperty.Department);
        JSONToWE_AddressBook.put(AddressBookProperty.displayName,
                WEContactsProperty.DisplayName);
        JSONToWE_AddressBook.put(AddressBookProperty.firstName,
                WEContactsProperty.FirstName);
        JSONToWE_AddressBook.put(AddressBookProperty.lastName,
                WEContactsProperty.LastName);
        JSONToWE_AddressBook.put(AddressBookProperty.middleName,
                WEContactsProperty.MiddleName);
        JSONToWE_AddressBook.put(AddressBookProperty.nickName,
                WEContactsProperty.Nickname);
        JSONToWE_AddressBook.put(AddressBookProperty.notes,
                WEContactsProperty.Notes);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoCity,
                WEContactsProperty.HomeCity);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoCountry,
                WEContactsProperty.HomeCountry);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoEmail,
                WEContactsProperty.AltEmail);
        JSONToWE_AddressBook.put(AddressBookProperty.buddyName,
                WEContactsProperty.BuddyName);
        JSONToWE_AddressBook.put(AddressBookProperty.imProvider,
                WEContactsProperty.ImProvider);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoImAddress,
                WEContactsProperty.ImProvider);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoMobile,
                WEContactsProperty.MobilePhone);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoPhone1,
                WEContactsProperty.HomePhone);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoPhone2,
                WEContactsProperty.OtherPhone);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoPostalCode,
                WEContactsProperty.HomeZip);
        JSONToWE_AddressBook.put(
                AddressBookProperty.personalInfoStateOrProvince,
                WEContactsProperty.HomeState);
        JSONToWE_AddressBook.put(AddressBookProperty.spouseName,
                WEContactsProperty.Spouse);
        JSONToWE_AddressBook.put(AddressBookProperty.title,
                WEContactsProperty.Title);
        // JSONToWE_AddressBook.put(AddressBookProperty.userId,
        // WEContactsProperty.name);
        JSONToWE_AddressBook.put(AddressBookProperty.webPage,
                WEContactsProperty.WorkUrl);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoCity,
                WEContactsProperty.WorkCity);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoCountry,
                WEContactsProperty.WorkCountry);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoEmail,
                WEContactsProperty.Email);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoFax,
                WEContactsProperty.WorkFax);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoPhone1,
                WEContactsProperty.WorkPhone);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoPostalCode,
                WEContactsProperty.WorkZip);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoStateOrProvince,
                WEContactsProperty.WorkState);

        // Groups
        JSONToWE_AddressBook.put(AddressBookProperty.memberId,
                WEContactsProperty.listMembers);
        JSONToWE_AddressBook.put(AddressBookProperty.groupName,
                WEContactsProperty.listName);
        JSONToWE_AddressBook.put(AddressBookProperty.groupId,
                WEContactsProperty.Id);
        JSONToWE_AddressBook.put(AddressBookProperty.groupDescription,
                WEContactsProperty.listDescription);
        // Events
        JSONToWE_AddressBook.put(AddressBookProperty.birthDay,
                WEContactsProperty.BirthdayDay);
        JSONToWE_AddressBook.put(AddressBookProperty.birthMonth,
                WEContactsProperty.BirthdayMonth);
        JSONToWE_AddressBook.put(AddressBookProperty.birthYear,
                WEContactsProperty.BirthdayYear);

        JSONToWE_AddressBook.put(AddressBookProperty.anniversaryDay,
                WEContactsProperty.AnniversaryDay);
        JSONToWE_AddressBook.put(AddressBookProperty.anniversaryMonth,
                WEContactsProperty.AnniversaryMonth);
        JSONToWE_AddressBook.put(AddressBookProperty.anniversaryYear,
                WEContactsProperty.AnniversaryYear);

        JSONToWE_AddressBook.put(AddressBookProperty.homeWebPage,
                WEContactsProperty.HomeUrl);
        JSONToWE_AddressBook.put(AddressBookProperty.children,
                WEContactsProperty.Children);
        JSONToWE_AddressBook.put(AddressBookProperty.pager,
                WEContactsProperty.Pager);
        JSONToWE_AddressBook.put(AddressBookProperty.personalInfoAddress,
                WEContactsProperty.HomeAddress1);
        JSONToWE_AddressBook.put(AddressBookProperty.workInfoAddress,
                WEContactsProperty.WorkAddress1);
    }

    /**
     * Default private constructor.
     */
    private WEAddressBookUtil() {

    }
}
