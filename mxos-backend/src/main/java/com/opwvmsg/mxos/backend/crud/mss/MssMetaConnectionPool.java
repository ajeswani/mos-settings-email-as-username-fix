/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.mss;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Implementation of MSS Meta CRUD connection pool.
 * 
 * @author mxos-dev
 */
public class MssMetaConnectionPool implements ICRUDPool<IMetaCRUD> {
    private static Logger logger = Logger
            .getLogger(MssMetaConnectionPool.class);

    private static GenericObjectPool<MssMetaCRUD> objPool;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public MssMetaConnectionPool() {
        logger.debug("MssMetaConnectionPool()...");
        int maxConnections = MxOSConfig.getMetaMaxConnections();
        objPool = new GenericObjectPool<MssMetaCRUD>(new MssMetaFactory(),
                maxConnections);
        initializeJMXStats(maxConnections);
    }

    @Override
    public IMetaCRUD borrowObject() throws MxOSException {
        IMetaCRUD obj = null;
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new ApplicationException(
                    ErrorCode.MSS_CONNECTION_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new ApplicationException(
                    ErrorCode.MSS_CONNECTION_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    @Override
    public void returnObject(IMetaCRUD metaCRUD) throws MxOSException {
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            objPool.returnObject((MssMetaCRUD) metaCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while returning object.", e);
            throw new ApplicationException(
                    ErrorCode.MSS_CONNECTION_ERROR.name(), e);
        }
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.MSSMETA.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_MSSMETA.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_MSSMETA.decrement();
    }

    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub

    }
}
