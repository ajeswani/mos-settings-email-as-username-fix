/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.domain;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.DomainType;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create Domain object.
 *
 * @author mxos-dev
 */
public class CreateDomain implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateDomain.class);

    /**
     * Action method to create Domain object.
     *
     * @param model model instance
     * @throws Exception in case of any error
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Create Domain action started."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        try {
            final String userDomainType =
                    requestState.getInputParams()
                    .get(DomainProperty.type.name()).get(0);
            String relayHost = "";

            if (requestState.getInputParams().containsKey(
                    DomainProperty.relayHost.name())) {
                relayHost =
                        requestState.getInputParams()
                    .get(DomainProperty.relayHost.name()).get(0);
            }
            String rewriteDomain = "";
            if (requestState.getInputParams().containsKey(
                    DomainProperty.alternateDomain.name())) {
                rewriteDomain =
                        requestState.getInputParams()
                                .get(DomainProperty.alternateDomain.name())
                                .get(0);
            }

            final DomainType domainType =
                    DomainType.fromValue(userDomainType);

            if (domainType == DomainType.NONAUTH
                    && (relayHost == null || relayHost.length() == 0)) {
                throw new InvalidRequestException(
                        DomainError.DMN_MISSING_RELAYHOST.name());
            } else if (domainType == DomainType.REWRITE
                    && (rewriteDomain == null || rewriteDomain.length() == 0)) {
                throw new InvalidRequestException(
                        DomainError.DMN_MISSING_ALTERNATEDOMAIN.name());
            }

            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            DomainProvisionHelper.create(mailboxCRUD, requestState);
            mailboxCRUD.commit();
        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while create domain.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Create Domain action end."));
        }
    }
}
