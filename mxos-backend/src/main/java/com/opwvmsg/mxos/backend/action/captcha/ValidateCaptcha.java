/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.captcha;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Validate Captcha.
 * 
 * @author mxos-dev
 */
public class ValidateCaptcha implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(ValidateCaptcha.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("ValidateCaptcha action start."));
        }
        try {
            int resetCounterValue = 0;
            Credentials credentials = null;
            int failedCaptchaAttempts = 0;
            final String captchaStatus = requestState.getInputParams()
                    .get(MailboxProperty.isValid.name()).get(0);
            try {
                credentials = (Credentials) requestState.getDbPojoMap()
                        .getPropertyAsObject(MxOSPOJOs.credentials);
                failedCaptchaAttempts = credentials
                        .getFailedCaptchaLoginAttempts();
            } catch (NullPointerException npEx) {
                logger.error("mandatory value in credentials are null", npEx);
                failedCaptchaAttempts = 0;
                // assuming the failedCaptchaAttempts value as 0.
            } finally {
                if (null == credentials) {
                    logger.error(new StringBuffer("credentials object in null"));
                    throw new MxOSException(
                            MailboxError.MBX_CREDENTIALS_UNABLE_TO_GET.name());
                }
            }
            final String now = new SimpleDateFormat(
                    System.getProperty(SystemProperty.userDateFormat.name()))
                    .format(Calendar.getInstance().getTime());
            if ("true".equalsIgnoreCase(captchaStatus)) {
                logger.debug("CAPTCHA is valid - Resetting the credential counters");
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.lastLoginAttemptDate, now);
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.failedLoginAttempts,
                                resetCounterValue + "");
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.failedCaptchaLoginAttempts,
                                resetCounterValue + "");
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.lastSuccessfulLoginDate, now);
            } else {
                logger.debug("CAPTCHA is not valid - Updating the credential counters");
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.lastLoginAttemptDate, now);
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.failedCaptchaLoginAttempts,
                                ++failedCaptchaAttempts + "");
            }
        } catch (Exception e) {
            logger.error("Error while validating captcha.", e);
            throw new MxOSException(MailboxError.CPT_UNABLE_TO_VALIDATE.name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("ValidateCaptcha action end."));
        }
    }
}
