/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.logging;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set OOO via MAA.
 *
 * @author Aricent
 *
 */

public class OOOMaaLoggingAction extends MaaLoggingAction {
    private static Logger logger = Logger.getLogger(OOOMaaLoggingAction.class);

    /**
     * Action to make call to MAA.
     *
     * @param model
     *            model.
     * @throws Exception
     *             exception
     */
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Start : OOOMaaLoggingAction");
        }
        try {
            String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);

            String userName = ActionUtils.getUsernameFromEmail(email);
            String domain = ActionUtils.getDomainFromEmail(email);
            Map<String, String> params = new HashMap<String, String>();
            params.put("alias", userName);
            params.put("domain", domain);
            if (requestState.getInputParams().get(
                    MailboxProperty.autoReplyMessage.name()) != null) {
                params.put("reply",
                        requestState.getInputParams()
                                .get(MailboxProperty.autoReplyMessage.name())
                                .get(0));
            }

            LoggingResponseBean maaResponse = callMaa(
                    OUT_OF_OFFICE_LOGGING_SUB_URL, params);
            // Analyze logging response
            analyzeLoggingResponse(maaResponse);
        } catch(MxOSException me) {
            logger.error("MXOSException: " +me);
            throw me;
        } catch (Exception e) {
            logger.error(e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End : OOOMaaLoggingAction");
        }
        return;
    }
}
