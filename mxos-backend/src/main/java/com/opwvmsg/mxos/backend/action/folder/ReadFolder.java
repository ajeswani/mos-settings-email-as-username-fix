/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.folder;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.mailbox.MailboxMetaServiceHelper;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to Read Folder Information.
 * 
 * @author mxos-dev
 */
public class ReadFolder implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(ReadFolder.class);

    /**
     * Action method to Read Folder.
     * 
     * @param model instance of model
     * @throws Exception throws in case of any errors
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("ReadFolder action started."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;

        try {

            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                if (requestState.getInputParams().get(
                        MailboxProperty.mailboxId.name()) != null) {
                    final Long mailboxId = Long.parseLong(requestState
                            .getInputParams()
                            .get(MailboxProperty.mailboxId.name()).get(0));
                    requestState.getAdditionalParams().setProperty(
                            MailboxProperty.mailboxId, mailboxId);
                }

                final int folderId = Integer.parseInt(requestState
                        .getInputParams().get(FolderProperty.folderId.name())
                        .get(0));
                requestState.getAdditionalParams().setProperty(
                        FolderProperty.folderId, folderId);
            }

            Folder folder = new Folder();
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();

            FolderServiceHelper.read(metaCRUD, requestState, folder);

            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                MailboxMetaServiceHelper.updateLastAccessTime(metaCRUD,
                        requestState);
                metaCRUD.commit();
            }
            
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.folder, folder);

        } catch (final MxOSException e) {
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while read folder.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    FolderError.FLD_UNABLE_TO_PERFORM_GET.name(), e);
        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("ReadFolder action end."));
        }
    }

    /*
     * TODO: This code prepares folder name with full path. Need to revisit this
     * code.
     */
    public static String getFolderPathName(Folder folder, List<Folder> folders) {
        String folderName = folder.getFolderName();
        /*
         * TODO: Need to revisit this code to generate folder path. Integer
         * parentFolderId = folder.getParentFolderId(); if (parentFolderId !=
         * null && parentFolderId > 0) { String parentFolderName =
         * getFolderPathName(parentFolderId, foldersData); if (parentFolderName
         * != null) { return parentFolderName + "/" + folderName; } }
         */
        return folderName;
    }

}
