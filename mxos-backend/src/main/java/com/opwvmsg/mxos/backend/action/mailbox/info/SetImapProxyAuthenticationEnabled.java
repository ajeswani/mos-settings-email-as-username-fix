/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.info;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set InternalInfo::imapProxyAuthenticationEnabled.
 *
 * @author mxos-dev
 */
public class SetImapProxyAuthenticationEnabled implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetImapProxyAuthenticationEnabled.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetImapProxyAuthenticationEnabled action start.");
        }
        final String imapProxyAuthEnabled = requestState.getInputParams()
                .get(MailboxProperty.imapProxyAuthenticationEnabled.name())
                .get(0);
        try {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.imapProxyAuthenticationEnabled,
                            imapProxyAuthEnabled);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set imap proxy auth. enabled.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_IMAP_PROXY_AUTH.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SetImapProxyAuthenticationEnabled action end.");
        }
    }
}
