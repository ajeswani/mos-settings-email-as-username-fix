/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ox.tasks;

import org.apache.commons.pool.BasePoolableObjectFactory;

/**
 * HTTP connection pool factory to talk over REST. This is required by Apache
 * object pool.
 * 
 * @author mxos-dev
 */
class OXTasksFactory extends BasePoolableObjectFactory<OXTasksCRUD> {
    private final String baseURL;

    /**
     * Constructor.
     * 
     * @param baseURL HTTP base URL.
     */
    public OXTasksFactory(String baseURL) {
        this.baseURL = baseURL;
    }

    @Override
    public void destroyObject(OXTasksCRUD restCRUD) throws Exception {
        restCRUD.close();
    }

    @Override
    public OXTasksCRUD makeObject() throws Exception {
        return new OXTasksCRUD(baseURL);
    }
}
