/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set cloudmark filters clean action.
 *
 * @author mxos-dev
 */
public class SetCloudmarkFiltersCleanAction implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetCloudmarkFiltersCleanAction.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCloudmarkFiltersCleanAction action start."));
        }
        String cleanAction = requestState.getInputParams()
                .get(MailboxProperty.cleanAction.name()).get(0);
        try {
            if (cleanAction != null && !cleanAction.equals("")) {
                cleanAction = MxosEnums.CloudmarkActionType.fromValue(
                        cleanAction).toString();
                if (cleanAction.equalsIgnoreCase(MxOSConstants.QUARANTINE)) {
                    if (null != requestState.getInputParams().get(
                            MailboxProperty.folderName.name())) {
                        String folderName = requestState.getInputParams()
                                .get(MailboxProperty.folderName.name()).get(0);
                        cleanAction += MxOSConstants.SPACE + folderName;
                    } else {
                        logger.error("Error while set cloudmark filters spam action.");
                        throw new InvalidRequestException(
                                MailboxError.FOLDER_NAME_REQUIRED.name());
                    }
                } else if (cleanAction.equalsIgnoreCase(MxOSConstants.TAG)) {
                    if (null != requestState.getInputParams().get(
                            MailboxProperty.tagText.name())) {
                        String tagText = requestState.getInputParams()
                                .get(MailboxProperty.tagText.name()).get(0);
                        cleanAction += MxOSConstants.SPACE + tagText;
                    } else {
                        logger.error("Error while set cloudmark filters spam action.");
                        throw new InvalidRequestException(
                                MailboxError.TAG_TEXT_REQUIRED.name());
                    }
                } else if (cleanAction.equalsIgnoreCase(MxOSConstants.HEADER)) {
                    if (null != requestState.getInputParams().get(
                            MailboxProperty.headerText.name())) {
                        String headerText = requestState.getInputParams()
                                .get(MailboxProperty.headerText.name()).get(0);
                        cleanAction += MxOSConstants.SPACE + headerText;
                    } else {
                        logger.error("Error while set cloudmark filters spam action.");
                        throw new InvalidRequestException(
                                MailboxError.HEADER_TEXT_REQUIRED.name());
                    }
                }
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.cleanAction,
                            cleanAction);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set cloudmark filters clean action.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_CLEAN_ACTION
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCloudmarkFiltersCleanAction action end."));
        }
    }
}
