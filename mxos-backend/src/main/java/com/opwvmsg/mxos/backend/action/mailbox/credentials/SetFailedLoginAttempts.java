/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update Failed Login Attempts.
 * 
 * @author mxos-dev
 */
public class SetFailedLoginAttempts implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetFailedLoginAttempts.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetFailedLoginAttempts action start."));
        }
        String failedLoginAttempts = requestState.getInputParams()
                .get(MailboxProperty.failedLoginAttempts.name()).get(0);
        if (failedLoginAttempts != null) {
            MxOSApp.getInstance()
                    .getMetaHelper()
                    .setAttribute(requestState,
                            MailboxProperty.failedLoginAttempts,
                            failedLoginAttempts);
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_FAILED_LOGIN_ATTEMPTS.name());
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetFailedLoginAttempts action end."));
        }
    }
}
