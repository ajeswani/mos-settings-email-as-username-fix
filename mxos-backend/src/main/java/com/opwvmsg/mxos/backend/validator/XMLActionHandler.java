/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.validator;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.stream.Location;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionFactory;

/**
 * The XMLActionHandler class is used to load the MxOS rules from rules
 * configuration.
 *
 * @author mxos-dev 
 */
public class XMLActionHandler {

    private static Logger logger = Logger.getLogger(XMLActionHandler.class);

    private static final String OPERATION = "operation";
    private static final String MANDATORY = "mandatory";
    private static final String OPTIONAL = "optional";
    private static final String PARAM = "param";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String ERRORCODE = "errorCode";
    private static final String PKG = "pkg";
    private static final String ID = "id";
    private static final String REF = "ref";
    private static final String CLASS = "class";

    private static final String ACTIONS = "actions";
    private static final String ACTION = "action";
    private static final String EXEC = "exec";
    private static final String RETURNS = "returns";

    /**
     * Default constructor.
     */
    protected XMLActionHandler() {
    }

    /**
     * Maps /operations/actionDefs/action@id versus Tuple.
     * It should be unique across the XML file.
     * It is referred as /operations/operation/actions/exec@ref
     */
    protected static final HashMap<String, Tuple> ACTION_MAP
        = new HashMap<String, Tuple>();

    /**
     * Parse XML by using local file.
     *
     * @param fileName
     *            Local File name
     * @param operations
     *            Map containing Operation rules
     * @return boolean
     * @throws Exception
     *             if any
     */
    public static boolean parse(String fileName,
            Map<String, OperationRule> operations) throws Exception {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = xmlif.createXMLStreamReader(fileName,
                new FileInputStream(fileName));
        return parse(xmlr, operations);
    }

    /**
     * Parse XML by using InputStream.
     *
     * @param is
     *            InputStream
     * @param operations
     *            Map containing Operation rules
     * @return boolean
     *            parsing success or not
     * @throws Exception
     *             if any
     */
    public static boolean parse(InputStream is,
            Map<String, OperationRule> operations) throws Exception {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = xmlif.createXMLStreamReader(is);
        return parse(xmlr, operations);
    }

    /**
     * Just a Tuple to store relevant action info.
     */
    private static class Tuple {
        private final String klass;  // / class name
        private final String pkg;    // / package name
        private final String id;     // / abbreviated name in XML file

        /** Mandatory argument for this action. */
        protected final Map<String, ParamTuple> args
            = new LinkedHashMap<String, ParamTuple>();
        /**
         * Constructor with params.
         * @param id - tuple id
         * @param pkg - package
         * @param klass - class name
         */
        public Tuple(String id, String pkg, String klass) {
            this.klass = klass;
            this.pkg = pkg;
            this.id = id;
        }
        /**
         * Returns fully qualified class name.
         * @return String full name of the class
         */
        public String getActionClass() {
            return getClassName(pkg, klass);
        }
        /**
         * Returns the String value.
         * @return String tuble with full name
         */
        public String toString() {
            return id + ':' + pkg + '.' + klass + ", args:" + args;
        }
        /**
         * Adds argument to this tuple.
         * @param name - name of the param
         * @param type - type of the param
         */
        public void addArg(String name, ParamTuple paramTuple) {
            args.put(name, paramTuple);
        }
        /**
         * Returns a cloned argument-map for the caller.
         * @return map of the params
         */
        public Map<String, ParamTuple> getArgs() {
            return new LinkedHashMap<String, ParamTuple>(args);
        }
    }
    /**
     * Just a ParamTuple to store relevant param info.
     */
    static class ParamTuple {
        private final String name;  // / param name
        private final String type;    // / param type
        private final String errorCode;     // / errorCode if param value

        /**
         * Constructor with params.
         * @param name - tuple name
         * @param type - type
         * @param errorCode - errorCode
         */
        public ParamTuple(String name, String type, String errorCode) {
            this.name = name;
            this.type = type;
            this.errorCode = errorCode;
        }
        /**
         * Method to return param name.
         * @return name name
         */
        public String getName() {
            return this.name;
        }
        /**
         * Method to return param type.
         * @return type type
         */
        public String getType() {
            return this.type;
        }
        /**
         * Method to return param errorCode.
         * @return errorCode errorCode
         */
        public String getErrorCode() {
            return this.errorCode;
        }
        /**
         * Returns the String value.
         * @return String tuble with full name
         */
        public String toString() {
            return name + ':' + type + ':' + errorCode;
        }
    }
    /**
     * Parse the XML reader.
     *
     * @param xmlr
     *            XMLReader
     * @param operations
     *            operaions map
     * @throws Exception in case of any error while parsing rules
     */
    private static boolean parse(XMLStreamReader xmlr,
            Map<String, OperationRule> operations) throws Exception {
        boolean ret = true;
        int eventType = 0;
        String curActionID = null;

        OperationRule curOp = null;
        boolean opMandatory = false;
        Map<String, ParamTuple> mandArgs = null;

    LOOP:
        try {
            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                    case XMLEvent.END_DOCUMENT:
                        break LOOP;
                    case XMLEvent.START_ELEMENT:
                        String name = xmlr.getLocalName();
                        if (OPERATION.equals(name)) {
                            String aName = xmlr.getAttributeValue(null, NAME);
                            String aErrorCode =
                                xmlr.getAttributeValue(null, ERRORCODE);
                            curOp = new OperationRule();
                            curOp.setErrorCode(aErrorCode);
                            operations.put(aName, curOp);
                        } else if (MANDATORY.equals(name)) {
                            opMandatory = true;
                            mandArgs = new LinkedHashMap<String, ParamTuple>();
                            curOp.getMandatoryParamRules().add(mandArgs);
                        } else if (OPTIONAL.equals(name)) {
                            opMandatory = true;
                            mandArgs = new LinkedHashMap<String, ParamTuple>();
                            curOp.getOptionalParamRules().add(mandArgs);
                        } else if (PARAM.equals(name)) {
                            String aName = xmlr.getAttributeValue(null, NAME);
                            String aType = xmlr.getAttributeValue(null, TYPE);
                            String aErrorCode =
                                xmlr.getAttributeValue(null, ERRORCODE);
                            ParamTuple pt =
                                new ParamTuple(aName, aType, aErrorCode);
                            if (opMandatory) {
                                mandArgs.put(aName, pt);
                            } else {
                                if (curActionID == null) {
                                    System.err.println("No curActionID is set");
                                    break;
                                }
                                Tuple tup = getAction(curActionID);
                                tup.addArg(aName, pt);
                            }
                        } else if (ACTIONS.equals(name)) {
                            opMandatory = false;
                            mandArgs = null;
                            curActionID = null;
                        } else if (ACTION.equals(name)) {
                            opMandatory = false;
                            curActionID = xmlr.getAttributeValue(null, ID);
                            String pkg = xmlr.getAttributeValue(null, PKG);
                            String klass = xmlr.getAttributeValue(null, CLASS);
                            registerAction(curActionID, pkg, klass);
                        } else if (MANDATORY.equals(name)) {
                            opMandatory = true;
                        } else if (RETURNS.equals(name)) {
                            String aName = xmlr.getAttributeValue(null, NAME);
                            curOp.setReturnObjectName(aName);
                        } else if (EXEC.equals(name)) {
                            curActionID = xmlr.getAttributeValue(null, REF);
                            Tuple tup = getAction(curActionID);
                            curOp.getActionRules().put(tup.getActionClass(),
                                    tup.getArgs());
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            ret = false;
            Location loc = xmlr.getLocation();
            logger.error("Error in parsing XML Actions:" + getLocInfo(loc), e);
            throw new Exception("Error while loading MxOS rules. Cause - " + e);
        }
        return ret;
    }

    /**
     * Return XML location info.
     * @return String - location info
     */
    private static String getLocInfo(Location loc) {
        return loc.getSystemId() + ", line:" + loc.getLineNumber() + ", col:"
                + loc.getColumnNumber() + ", off:" + loc.getCharacterOffset();
    }

    /**
     * Registers the BaseAction in local map.
     *
     * @param id
     *            referred by /operations/actionDefs/action@id
     * @param pkg
     *            referred by /operations/actionDefs/action@pkg
     * @param klass
     *            referred by /operations/actionDefs/action@class
     */
    private static void registerAction(String id, String pkg, String klass)
        throws Exception {
        Tuple old = ACTION_MAP.get(id);
        /*if (old != null) {
            throw new IllegalStateException("Old " + id
                    + " already define for:" + old + ", new one is:"
                    + getClassName(pkg, klass));
        }*/
        if (old == null) {
            old = new Tuple(id, pkg, klass);
            ActionFactory.getInstance().register(old.getActionClass());
            ACTION_MAP.put(id, old);
        } else {
            logger.info("Action " + id + " registered");
        }
    }

    /**
     * Gets the Tuple by id.
     *
     * @param id
     *            referred by /operations/actionDefs/action@id
     * @return Tuple mapped to this id
     */
    private static Tuple getAction(String id) {
        Tuple old = ACTION_MAP.get(id);
        if (old == null) {
            throw new IllegalArgumentException("Action-id:" + id
                    + " is not registered");
        }
        return old;
    }

    /**
     * Gets the class name.
     *
     * @param pkg
     *            Package prefix
     * @param klass
     *            Class name
     * @return Fully qualified class name
     */
    private static String getClassName(String pkg, String klass) {
        return pkg + '.' + klass;
    }

    /**
     * Method to dump the rules into logger (debug).
     * @param ops - list of operations
     */
    public static void dumpRules(Map<String, OperationRule> ops) {
        Iterator<Entry<String, OperationRule>> opIt = ops.entrySet().iterator();
        int i = 0;
        while (opIt.hasNext()) {
            Entry<String, OperationRule> opEn = opIt.next();
            String opName = opEn.getKey();
            logger.debug("Operation:" + opName);

            OperationRule op = opEn.getValue();
            logger.debug("Return:" + op.getReturnObjectName());
            List<Map<String, ParamTuple>> mandArgs = op.getMandatoryParamRules();
            i = 0;
            for (Map<String, ParamTuple> args : mandArgs) {
                i++;
                logger.debug("Mandatory Args Set-" + i + " : " + args);
            }
            Map<String, Map<String, ParamTuple>> actions = op.getActionRules();
            logger.debug("No Actions:" + actions.size());
            Iterator<Entry<String, Map<String, ParamTuple>>> actIt = actions
                    .entrySet().iterator();
            i = 0;
            while (actIt.hasNext()) {
                Entry<String, Map<String, ParamTuple>> actEn = actIt.next();
                i++;
                String actName = actEn.getKey();
                Map<String, ParamTuple> actArgs = actEn.getValue();
                logger.debug(i + ". Action : " + actName + ", args:" + actArgs);
            }
        }
    }
}
