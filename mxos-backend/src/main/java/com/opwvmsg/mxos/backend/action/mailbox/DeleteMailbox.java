/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to delete mailbox.
 * 
 * @author mxos-dev
 */
public class DeleteMailbox implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteMailbox.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteMailbox action start."));
        }
        // Delete in OX first, if backend is set to OX
        Boolean appSuiteIntegrated = Boolean.parseBoolean(System
                .getProperty(SystemProperty.appSuiteIntegrated.name()));
        if (appSuiteIntegrated) {
            if (MxOSConfig.getAddressBookBackend().equals(
                    AddressBookDBTypes.ox.name())) {
                ICRUDPool<IRMIMailboxCRUD> rmiMailboxCRUDPool = null;
                IRMIMailboxCRUD rmiMailboxCRUD = null;
                logger.info("Start of delete the context from OX");
                try {
                    rmiMailboxCRUDPool = MxOSApp.getInstance()
                            .getRMIMailboxCRUD();

                    rmiMailboxCRUD = rmiMailboxCRUDPool.borrowObject();
                    rmiMailboxCRUD.deleteMailbox(requestState);
                    logger.info("Success deleting the context from OX");
                } catch (final MxOSException e) {
                    logger.warn("Error in deleting the mailbox from OX");
                    logger.debug("Error in deleting the mailbox from OX", e);
                    if (rmiMailboxCRUD != null) {
                        rmiMailboxCRUD.rollback();
                        logger.info("Rollback success for deleting the context from OX");
                    }
                } catch (Exception e) {
                    logger.warn("Error while delete mailbox in OX.");
                    logger.debug("Error while delete mailbox in OX.", e);
                    if (rmiMailboxCRUD != null) {
                        rmiMailboxCRUD.rollback();
                        logger.info("Rollback success for deleting the context from OX");
                    }
                } finally {
                    try {
                        if (rmiMailboxCRUDPool != null
                                && rmiMailboxCRUD != null) {
                            rmiMailboxCRUDPool.returnObject(rmiMailboxCRUD);
                        }
                    } catch (final MxOSException e) {
                        logger.warn("Error while returning Rmi CRUD.");
                        logger.debug("Error while returning Rmi CRUD.", e);
                    }
                }
            }
        }

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {

            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();

            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            Boolean deleteOnlyOnMss = null;
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.deleteOnlyOnMss.name())) {
                deleteOnlyOnMss = Boolean.parseBoolean(requestState
                        .getInputParams()
                        .get(MailboxProperty.deleteOnlyOnMss.name()).get(0));
            }
            logger.info("Deleting Mailbox from MSS for account: " + email);
            if (deleteOnlyOnMss != null && deleteOnlyOnMss == true) {
                logger.info("Deleting and recreating from MSS only for account: "
                        + email);
                // Delete (and Recreate) from MSS
                deleteAndRecreateMailboxOnMSS(email, requestState, metaCRUD);
                metaCRUD.commit();
            } else {
                // Delete from MSS
                deleteMailboxFromMSS(email, requestState, metaCRUD);
                // Delete from LDAP
                deleteMailboxFromLDAP(requestState, mailboxCRUD);
                mailboxCRUD.commit();
                metaCRUD.commit();
            }
            logger.info("Delete Mailbox SUCCESS of: " + email);
        } catch (final MxOSException e) {
            logger.error("Error while deleting mailbox.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while deleting mailbox.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } finally {
            try {
                if (metaCRUDPool != null && metaCRUD != null) {
                    metaCRUDPool.returnObject(metaCRUD);
                }

                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                }

            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteMailbox action end."));
        }
    }

    /**
     * Method to delete mailbox from LDAP database.
     * 
     * @param email email
     * @param mailboxCRUD mailboxCRUD
     * @throws Exception if any
     */
    private void deleteMailboxFromLDAP(MxOSRequestState requestState,
            final IMailboxCRUD mailboxCRUD) throws Exception {
        try {
            logger.info("Deleting Mailbox from LDAP");
            mailboxCRUD.deleteMailbox(requestState);
            logger.info("Deleted Mailbox from LDAP");
        } catch (final MxOSException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        }
    }

    /**
     * Method to delete and recreate mailbox on MSS.
     * 
     * @param requestState requestState
     * @param metaCRUD metaCRUD
     * @throws Exception if any
     */
    private void deleteAndRecreateMailboxOnMSS(final String email,
            final MxOSRequestState requestState, final IMetaCRUD metaCRUD)
            throws Exception {
        try {
            // Delete Mailbox from MSS
            deleteMailboxFromMSS(email, requestState, metaCRUD);
            // Check for recreation
            Boolean recreateOnMss = null;
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.recreateOnMss.name())) {
                recreateOnMss = Boolean.parseBoolean(requestState
                        .getInputParams()
                        .get(MailboxProperty.recreateOnMss.name()).get(0));
            }
            if (recreateOnMss != null && recreateOnMss == true) {
                // Recreate the Mailbox on MSS
                logger.info("Re-creating the Mailbox on MSS of: " + email);
                metaCRUD.createMailbox(requestState);
                logger.info("Re-created the Mailbox on MSS of: " + email);

            }
        } catch (Exception e) {
            if (!e.getMessage().contains("MsNotFound")) {
                throw e;
            }
        }
    }

    /**
     * Method to delete mailbox from MSS.
     * 
     * @param requestState requestState
     * @param metaCRUD metaCRUD
     * @throws Exception if any
     */
    private void deleteMailboxFromMSS(final String email,
            final MxOSRequestState requestState, final IMetaCRUD metaCRUD)
            throws Exception {
        try {
            logger.info("Deleting Mailbox from MSS");
            metaCRUD.deleteMailbox(requestState);
            logger.info("Deleted Mailbox from MSS");
        } catch (Exception e) {
            if (!e.getMessage().contains("MsNotFound")) {
                throw e;
            }
        }
    }
}
