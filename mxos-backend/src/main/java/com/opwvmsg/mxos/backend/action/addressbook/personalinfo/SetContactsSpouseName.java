/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.addressbook.personalinfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set spouseName attribute of Contact PersonalInfo object.
 * 
 * @author mxos-dev
 * 
 */

public class SetContactsSpouseName implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetContactsSpouseName.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetContactsSpouseName action start."));
        }

        if (mxosRequestState.getInputParams().containsKey(
                AddressBookProperty.contactsList.name())) {
            Object[] name = mxosRequestState.getInputParams()
                    .get(AddressBookProperty.spouseName.name()).toArray();

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttributes(mxosRequestState,
                            AddressBookProperty.spouseName, name);
        } else {
            String name = mxosRequestState.getInputParams()
                    .get(AddressBookProperty.spouseName.name()).get(0);

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(mxosRequestState,
                            AddressBookProperty.spouseName, name);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetContactsSpouseName action end."));
        }
    }
}
