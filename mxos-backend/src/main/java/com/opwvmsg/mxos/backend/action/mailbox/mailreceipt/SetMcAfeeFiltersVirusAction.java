/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set McAfee filters virus action.
 *
 * @author mxos-dev
 */
public class SetMcAfeeFiltersVirusAction implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetMcAfeeFiltersVirusAction.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetMcAfeeFiltersVirusAction action start."));
        }
        String virusAction = null;
        if (null != requestState.getInputParams().get(
                MailboxProperty.virusAction.name())) {
            virusAction = requestState.getInputParams()
                    .get(MailboxProperty.virusAction.name()).get(0);
        } else if (null != requestState.getInputParams().get(
                MailboxProperty.mailReceiptMcAfeeVirusAction.name())) {
            virusAction = requestState.getInputParams()
                    .get(MailboxProperty.mailReceiptMcAfeeVirusAction.name())
                    .get(0);
        }

        try {
            if (virusAction != null && !virusAction.equals("")) {
                virusAction = MxosEnums.McAfeeVirusActionType.fromValue(
                        virusAction).toString();
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.mcAfeeVirusAction, virusAction);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set mcafee filters virus action.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_MCAFEE_FILTERS_VIRUS_ACTION
                            .name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetMcAfeeFiltersVirusAction action end."));
        }
    }
}
