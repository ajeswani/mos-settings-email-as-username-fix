/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.InvalidNameException;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import javax.naming.SizeLimitExceededException;
import javax.naming.TimeLimitExceededException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.InvalidAttributeIdentifierException;
import javax.naming.directory.InvalidAttributeValueException;
import javax.naming.directory.InvalidAttributesException;
import javax.naming.directory.NoSuchAttributeException;
import javax.naming.directory.SchemaViolationException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * LDAP Implementation class for Mailbox Account CRUD.
 * 
 * @author mxos-dev
 */
public class LdapMailboxCRUD implements IMailboxCRUD {
    private static Logger logger = Logger.getLogger(LdapMailboxCRUD.class);
    private final DirContext ldapContext;
    private boolean status = false;
    private Map<String, Attributes> groupMap = new LinkedHashMap<String, Attributes>();
    private final boolean validateWithPinging;

    /**
     * Default Constructor.
     * 
     * @param env env
     * @throws Exception Exception incase of any error on connection.
     */
    public LdapMailboxCRUD(final Hashtable<String, String> env)
            throws Exception {
        try {
            // Create one initial context (Get connection from pool)
            ldapContext = new InitialDirContext(env);
            if (ldapContext == null) {
                this.status = false;
                logger.error("Problem with LDAP Connection.");
                throw new Exception(ErrorCode.LDP_CONNECTION_ERROR.name()
                        + "Problem with LDAP Connection.");
            }
            this.status = true;
            validateWithPinging = java.lang.Boolean.parseBoolean(System.getProperty(
                    SystemProperty.ldapConnectionValidationWithPinging.name(), MxOSConstants.TRUE));
        } catch (final NamingException e) {
            ConnectionErrorStats.LDAP.increment();
            logger.error("Error while initializing LDAP CRUD.", e);
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * This method is used to check if the ldapContext connection is still
     * properly connected and configured. This should work with no changes on
     * most LDAP servers and provide the fastest way to validate the
     * ldapContext.
     * 
     * @return
     */
    public boolean isConnected() {
        if (validateWithPinging) {
            logger.debug("Sending dummy request to LDAP");
            final SearchControls sc = new SearchControls(
                    SearchControls.ONELEVEL_SCOPE, 1,
                    MxOSConfig.getLdapConnectTimeout(),
                    new String[] { "objectclass" }, true, false);
            try {
                ldapContext.search("", "objectclass=*", sc);
                this.status = true;
            } catch (Exception e) {
                logger.warn("DirContext '" + ldapContext
                        + "' failed validation with an exception.", e);
                this.status = false;
            }
        }
    	return this.status;
    }

    /**
     * Method to close the ldap connection.
     * 
     * @throws MxOSException
     */
    public void close() throws MxOSException {
        try {
            if (ldapContext != null) {
                ldapContext.close();
                this.status = false;
            }
        } catch (NamingException e) {
            logger.error("Error while closing LDAP CRUD.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void createDomain(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        final String domain = mxosRequestState.getInputParams()
                .get(DomainProperty.domain.name()).get(0);
        final String[] dc = domain.split("\\.");
        try {
            String fulldc = "";
            for (int i = dc.length - 1; i >= 0; i--) {
                if (fulldc.length() == 0) {
                    fulldc = "dc=" + dc[i];
                } else {
                    fulldc = "dc=" + dc[i] + "," + fulldc;
                }
                Attributes attr = null;
                if (i != 0) {
                    attr = LDAPUtils.buildDomainParentAttributes(dc[i]);
                    try {
                        executeLDAPCreate(fulldc.toString(), null, attr);
                    } catch (final NameAlreadyBoundException e) {
                        logger.warn(e);
                    } catch (final InvalidAttributeIdentifierException e) {
                        logger.warn(e);
                    }
                } else {
                    final IBackendState backendState = mxosRequestState
                            .getBackendState().get(MailboxDBTypes.ldap.name());
                    LDAPUtils
                            .buildDomainAttributes(
                                    ((LDAPBackendState) backendState).attributes,
                                    dc[i]);
                    executeLDAPCreate(fulldc.toString(), null,
                            ((LDAPBackendState) backendState).attributes);
                }
            }
        } catch (NameAlreadyBoundException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_ALREADY_EXISTS.name(), e);
        } catch (InvalidAttributeIdentifierException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (InvalidAttributeValueException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (InvalidNameException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (Exception e) {
            logger.error("LDAP error while create domain.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * 
     * This method fetches the LDAP attributes for the mailbox with cos caching
     * support.
     * 
     * @param fulldc
     * @param attributeNames
     * @param isCosDataRequired
     * @param cosMap
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    private Attributes executeLDAPRetrieve(final String searchFilter,
            final String[] attributeNames, final boolean doCosCaching,
            Map<String, Attributes> cosMap, final boolean isGroupCacheReq)
            throws MxOSException, NamingException {
        final long t0 = Stats.startTimer();
        Attributes attrs = null;
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                    attributeNames, true, false);
            NamingEnumeration<SearchResult> results = null;
            results = ldapContext.search("", searchFilter, sc);
            if (results != null && results.hasMore()) {
                final SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    attrs = searchResult.getAttributes();

                    String nameInNamespace = searchResult.getNameInNamespace();
                    if (logger.isDebugEnabled()) {
                        logger.debug("NameInNamespace : " + nameInNamespace);
                    }
                    if (MxOSConfig.isGroupMailboxEnabled()) {
                        // Check and retrieve family mailbox attributes
                        Attributes groupAttrs = retrieveFamilyMailboxAttrs(
                                attrs, nameInNamespace, isGroupCacheReq);
                        attrs = addAttrsToExistingAttrs(attrs, groupAttrs);
                    }
                    Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime, Operation.GET, t0,
                            StatStatus.pass);
                    Attribute cosIdAttr = attrs
                            .get(LDAPMailboxProperty.adminpolicydn.name());
                    if (cosIdAttr != null && cosIdAttr.get() != null) {
                        Attributes cosAttrs = getCosAttributes(cosIdAttr.get()
                                .toString(), attributeNames);
                        attrs = addAttrsToExistingAttrs(attrs, cosAttrs);
                    }
                }
            }
            return attrs;
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP retrieve.", e);
            Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime, Operation.GET, t0,
                    StatStatus.fail);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Time out error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime, Operation.GET, t0,
                    StatStatus.fail);
            throw new ApplicationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime, Operation.GET, t0,
                    StatStatus.fail);
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Error while readMailbox", e);
            Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        } catch (final Exception e) {
            logger.error("Error in LDAP retrieve.", e);
            Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime, Operation.GET, t0,
                    StatStatus.fail);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    public Attributes executeLDAPRetrieve(final String fulldc,
            final String[] attributeNames) throws MxOSException,
            NamingException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Executing LDAP retrieve with fulldc - " + fulldc);
            }
            Attributes result = ldapContext.getAttributes(fulldc,
                    attributeNames);
            return result;
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP retrieve.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Time out error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Error while readMailbox", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error in LDAP retrieve.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    private Attributes executeLDAPRetrieve(String searchFilter,
            boolean isGroupAttrsReq) throws MxOSException, NamingException {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing LDAP retrieve with filter - "
                    + searchFilter);
        }
        Attributes attrs = null;
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                    LDAPUtils.mailboxAttributes, true, false);
            NamingEnumeration<SearchResult> results = null;
            results = ldapContext.search("", searchFilter, sc);
            if (results != null && results.hasMore()) {
                final SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    attrs = searchResult.getAttributes();

                    if (isGroupAttrsReq) {
                        String groupName = searchResult.getNameInNamespace();
                        if (logger.isDebugEnabled()) {
                            logger.debug("GroupName : " + groupName);
                        }
                        // Check and retrieve family mailbox attributes
                        Attributes groupAttrs = retrieveFamilyMailboxAttrs(
                                attrs, groupName, false);
                        attrs = addAttrsToExistingAttrs(attrs, groupAttrs);

                        // Check and retrieve derived group admin attributes
                        if (attrs.get(LDAPMailboxProperty.mailfamilymailbox
                                .name()) != null) {
                            Attribute attr = attrs
                                    .get(LDAPMailboxProperty.mailfamilymailbox
                                            .name());
                            String mailFamilyMailbox = (String) attr.get();
                            if (mailFamilyMailbox.equals(MailboxType.GROUPADMIN
                                    .getValue())) {
                                String dn = LDAPUtils
                                        .getGroupDnFromGroupName(groupName);
                                Attributes derivedAttrs = readAdminAllocationsAttributes(dn);
                                attrs = addAttrsToExistingAttrs(attrs,
                                        derivedAttrs);
                            }
                        }
                    }

                    // Retrieve COS attributes
                    Attribute cosIdAttr = attrs
                            .get(LDAPMailboxProperty.adminpolicydn.name());
                    if (cosIdAttr != null && cosIdAttr.get() != null) {
                        Attributes cosAttrs = getCosAttributes(cosIdAttr.get()
                                .toString(), LDAPUtils.mailboxAttributes);
                        attrs = addAttrsToExistingAttrs(attrs, cosAttrs);
                    }
                }
            }
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP retrieve.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Timeout error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while LDAP retrieve", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error in LDAP retrieve.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
        return attrs;
    }

    private Attributes executeLDAPRetrieveWithoutCOS(String searchFilter)
            throws MxOSException, NamingException {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing LDAP retrieve without"
                    + " COS and with filter - " + searchFilter);
        }
        Attributes attrs = null;
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                    LDAPUtils.mailboxAttributes, true, false);
            NamingEnumeration<SearchResult> results = null;
            results = ldapContext.search("", searchFilter, sc);
            if (results != null && results.hasMore()) {
                final SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    attrs = searchResult.getAttributes();
                }
            }
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP retrieve without COS.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Timeout error in LDAP retrieve without COS.", e);
            ConnectionErrorStats.LDAP.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP retrieve without COS.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP retrieve without COS.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while LDAP retrieve without COS.", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error in LDAP retrieve without COS.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
        return attrs;
    }

    private Attributes executeLDAPRetrieveWithoutGroupAttributes(String searchFilter)
            throws MxOSException, NamingException {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing LDAP retrieve without"
                    + " GroupAttributes and with filter - " + searchFilter);
        }
        Attributes attrs = null;
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                    LDAPUtils.mailboxAttributes, true, false);
            NamingEnumeration<SearchResult> results = null;
            results = ldapContext.search("", searchFilter, sc);
            if (results != null && results.hasMore()) {
                final SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    attrs = searchResult.getAttributes();
                }
                
                // Retrieve COS attributes
                Attribute cosIdAttr = attrs
                        .get(LDAPMailboxProperty.adminpolicydn.name());
                if (cosIdAttr != null && cosIdAttr.get() != null) {
                    Attributes cosAttrs = getCosAttributes(cosIdAttr.get()
                            .toString(), LDAPUtils.mailboxAttributes);
                    attrs = addAttrsToExistingAttrs(attrs, cosAttrs);
                }
            }
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP retrieve without GroupAttributes.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Timeout error in LDAP retrieve without GroupAttributes.", e);
            ConnectionErrorStats.LDAP.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP retrieve without GroupAttributes.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while LDAP retrieve without GroupAttributes.", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error in LDAP retrieve without GroupAttributes.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
        return attrs;
    }

    private NamingEnumeration<SearchResult> executeLDAPSearch(
            final String ldapBaseDn, final String formattedLdapFilter,
            final SearchControls sc) throws MxOSException, NamingException {
        try {
            return ldapContext.search(ldapBaseDn, formattedLdapFilter, sc);
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP search.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Time out error in LDAP search.", e);
            ConnectionErrorStats.LDAPSEARCH.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP search.", e);
            ConnectionErrorStats.LDAPSEARCH.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP search.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while LDAP search", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error in LDAP search.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    private void executeLDAPCreate(final String name, final Object obj,
            final Attributes attributes) throws MxOSException, NamingException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Executing LDAP create with name - " + name);
            }
            ldapContext.bind(name, obj, attributes);
        } catch (NameAlreadyBoundException e) {
            logger.warn("Error in LDAP create. Entry already exists");
            throw e;
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP create.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Time out error in LDAP create.", e);
            ConnectionErrorStats.LDAP.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP create.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP create.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final SchemaViolationException e) {
            logger.warn("Error in LDAP create.", e);
            throw new InvalidRequestException(
                    ErrorCode.LDP_SCHEMA_VIOLATION.name(), e);
        } catch (final NamingException e) {
            logger.warn("Error in LDAP create.", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error in LDAP create.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    private void executeLDAPUpdate(final String mailboxDn,
            final Attributes attributes) throws MxOSException, NamingException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Executing LDAP update with dn - " + mailboxDn);
            }
            ldapContext.modifyAttributes(mailboxDn,
                    DirContext.REPLACE_ATTRIBUTE, attributes);
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP update.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Time out error in LDAP update.", e);
            ConnectionErrorStats.LDAP.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP update.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP update.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final SchemaViolationException e) {
            logger.warn("Error in LDAP update.", e);
            throw new InvalidRequestException(
                    ErrorCode.LDP_SCHEMA_VIOLATION.name(), e);
        } catch (final NamingException e) {
            logger.warn("Error while updateMailbox", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error in LDAP update.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    public Attributes retrieveFamilyMailboxAttrs(Attributes attrs,
            String nameSpace, boolean isGroupCachingReq) throws MxOSException,
            NamingException {
        String dn = LDAPUtils.getGroupDnFromGroupName(nameSpace);
        if (logger.isDebugEnabled()) {
            logger.debug("dn : " + dn);
        }

        Attributes familyAttrs = new BasicAttributes();
        String mailFamilyMailbox = null;
        if (attrs.get(LDAPMailboxProperty.mailfamilymailbox.name()) != null) {
            mailFamilyMailbox = attrs
                    .get(LDAPMailboxProperty.mailfamilymailbox.name()).get()
                    .toString();
            if (logger.isDebugEnabled()) {
                logger.debug("mailFamilyMailbox : " + mailFamilyMailbox);
            }
            if (mailFamilyMailbox.equals(MailboxType.GROUPMAILBOX.getValue())
                    || mailFamilyMailbox.equals(MailboxType.GROUPADMIN
                            .getValue())) {
                if (isGroupCachingReq && groupMap.containsKey(nameSpace)) {
                    familyAttrs = groupMap.get(dn);
                } else {
                    // Get group attributes to get adminRealmDn
                    Attributes groupAttrs = getGroupMailboxAttributes(dn);
                    if (logger.isDebugEnabled()) {
                        logger.debug("groupAttrs : " + groupAttrs);
                    }
                    familyAttrs = addAttrsToExistingAttrs(familyAttrs,
                            groupAttrs);

                    // Get group admin attributes if adminRealmDn exists
                    if (groupAttrs.get(LDAPGroupMailboxProperty.adminRealmdn
                            .name()) != null) {
                        String adminRealmDn = groupAttrs
                                .get(LDAPGroupMailboxProperty.adminRealmdn
                                        .name()).get().toString();
                        if (logger.isDebugEnabled()) {
                            logger.debug("adminRealmDn : " + adminRealmDn);
                        }
                        Attributes adminAttrs = getGroupAdminAttributes(adminRealmDn);
                        if (logger.isDebugEnabled()) {
                            logger.debug("adminAttributes : " + adminAttrs);
                        }
                        familyAttrs = addAttrsToExistingAttrs(familyAttrs,
                                adminAttrs);
                    }
                    if (isGroupCachingReq) {
                        groupMap.put(dn, familyAttrs);
                    }
                }

            }
        }
        return familyAttrs;
    }
    
    /**
     * Get all cos attributes using adminPolicyDn.
     * 
     * @param adminPolicyDn
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes getCosAttributes(String adminPolicyDn,
            String[] attributeNames) throws MxOSException, NamingException {

        Attributes cosAttrs = null;
        boolean getAllCosAttributes = false;

        if (attributeNames.equals(LDAPUtils.mailboxAttributes)) {
            getAllCosAttributes = true;
        }

        boolean cosCachingEnabled = java.lang.Boolean.parseBoolean(System
                .getProperty(SystemProperty.ldapCosCachingEnabled.name()));

        if (cosCachingEnabled) {
            Attributes attributes = MxOSApp
                    .getInstance()
                    .getLdapCosCache()
                    .getCosAttributes(adminPolicyDn, getAllCosAttributes, this,
                            attributeNames);
            return attributes;
        } else {
            cosAttrs = executeLDAPRetrieve(adminPolicyDn, attributeNames);
            logger.info("cosAttrs fetched from LDAP Successfully");
        }
        return cosAttrs;
    }
    

    /**
     * 
     * @param groupName
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes getGroupMailboxAttributes(String groupDn)
            throws MxOSException, NamingException {
        Attributes groupAttrs = executeLDAPRetrieve(groupDn,
                LDAPUtils.groupMailboxAttributes);
        return groupAttrs;
    }

    /**
     * 
     * @param adminRealmDn
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes getGroupAdminAttributes(String adminRealmDn)
            throws MxOSException, NamingException {
        Attributes adminAttrs = new BasicAttributes();
        String groupAdminLdapFilter = "objectclass=*";
        final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        SearchControls groupAdminSc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, 0, maxTimeOut,
                LDAPUtils.groupAdminMailboxAttributes, true, false);
        NamingEnumeration<SearchResult> groupAdminSearchResult = null;
        groupAdminSearchResult = ldapContext.search(adminRealmDn,
                groupAdminLdapFilter, groupAdminSc);
        while (groupAdminSearchResult != null
                && groupAdminSearchResult.hasMore()) {
            SearchResult searchResults = groupAdminSearchResult.next();
            if (searchResults != null && searchResults.getAttributes() != null) {
                adminAttrs = addAttrsToExistingAttrs(adminAttrs,
                        searchResults.getAttributes());
            }
        }
        return adminAttrs;
    }

    /**
     * Utility method to add new attributes to the given attributes and return.
     * 
     * @param attrs
     * @param newAttrs
     * @return
     * @throws NamingException
     */
    public Attributes addAttrsToExistingAttrs(Attributes attrs,
            Attributes newAttrs) throws NamingException {
        if (attrs != null && newAttrs != null) {
            final NamingEnumeration<? extends Attribute> newAttrsEnum = newAttrs
                    .getAll();
            while (newAttrsEnum.hasMore()) {
                final Attribute attr = newAttrsEnum.next();
                if (attr != null && attrs.get(attr.getID()) == null) {
                    attrs.put(attr);
                }
            }
        }
        return attrs;
    }

    @Override
    public Domain readDomain(final String domainName) throws MxOSException {
        String fulldc = LDAPUtils.getDomainSearchQuery(domainName);
        final Attributes domainResult;
        String currentAttribute = null;
        NamingEnumeration<? extends Attribute> namingEnum = null;
        try {
            domainResult = executeLDAPRetrieve(fulldc,
                    LDAPUtils.domainAttributes);
            if (domainResult != null) {
                final Domain domain = new Domain();

                namingEnum = domainResult.getAll();
                while (namingEnum.hasMore()) {
                    final Attribute attr = namingEnum.next();
                    currentAttribute = attr.getID();
                    final LDAPDomainProperty ldapDomainProperty = LDAPDomainProperty
                            .valueOf(attr.getID().toLowerCase());
                    if (ldapDomainProperty == LDAPDomainProperty.domainname) {
                        domain.setDomain(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.mailrewritedomain) {
                        domain.setAlternateDomain(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.mailwildcardaccount) {
                        domain.setDefaultMailbox(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.mailrelayhost) {
                        domain.setRelayHost(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.domaintype) {
                        domain.setType(LDAPDomainType.fromLdap(attr.get()
                                .toString()));
                    } else {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                "Invalid domain attribute name retrieved from "
                                        + "LDAP:" + currentAttribute
                                        + ", input parameters:" + domainName);
                    }
                }
                return domain;
            } else {
                // We assume domainResult being null means domain is not
                // present.
                throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                        "Domain not found." + domainName);
            }
        } catch (final IllegalArgumentException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Invalid domain attribute name retrieved from LDAP:"
                            + currentAttribute + domainName);
        } catch (final NoSuchAttributeException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Domain not found " + domainName);
        } catch (final NameNotFoundException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Domain not found " + domainName);
        } catch (final NamingException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Domain not found " + domainName);
        } finally {
            if (namingEnum != null) {
                try {
                    namingEnum.close();
                } catch (NamingException e) {
                    throw new ApplicationException(
                            ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public List<Domain> searchDomain(final MxOSRequestState mxosRequestState,
            final int searchMaxRows, final int searchMaxTimeout)
            throws MxOSException {
        final String formattedLdapFilter = mxosRequestState.getInputParams()
                .get(MailboxProperty.query.name()).get(0);
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, searchMaxRows, searchMaxTimeout,
                LDAPUtils.domainAttributes, true, false);

        // TODO: If email is provided by user then directly do get query instead
        // of search.
        NamingEnumeration<SearchResult> results = null;
        try {
            results = executeLDAPSearch("", formattedLdapFilter, sc);

            final List<Domain> domains = new ArrayList<Domain>();
            try {
                while (results != null && results.hasMore()) {
                    final SearchResult searchResult = results.next();
                    final Domain domain = new Domain();
                    final Attributes attributes = searchResult.getAttributes();
                    final NamingEnumeration<? extends Attribute> namingEnum = attributes
                            .getAll();
                    while (namingEnum.hasMore()) {
                        final Attribute attr = namingEnum.next();
                        final LDAPDomainProperty ldapDomainProperty = LDAPDomainProperty
                                .valueOf(attr.getID().toLowerCase());
                        if (ldapDomainProperty == LDAPDomainProperty.domainname) {
                            domain.setDomain(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.mailrewritedomain) {
                            domain.setAlternateDomain(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.mailwildcardaccount) {
                            domain.setDefaultMailbox(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.mailrelayhost) {
                            domain.setRelayHost(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.domaintype) {
                            domain.setType(LDAPDomainType.fromLdap(attr.get()
                                    .toString()));
                        } else {
                            throw new ApplicationException(
                                    ErrorCode.GEN_INTERNAL_ERROR.name(),
                                    "Invalid domain attribute name retrieved from"
                                            + "LDAP:" + attr.getID()
                                            + ", input parameters:");
                        }
                    }
                    namingEnum.close();
                    domains.add(domain);
                }
            } catch (SizeLimitExceededException e) {
                // Nothing to do.
            } catch (TimeLimitExceededException e) {
                // Nothing to do.
            }
            return domains;
        } catch (Exception e) {
            logger.error("Error while search domain", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    @Override
    public void updateDomain(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String fulldc = LDAPUtils
                    .getDomainSearchQuery(mxosRequestState.getInputParams()
                            .get(DomainProperty.domain.name()).get(0));
            final IBackendState backendState = mxosRequestState
                    .getBackendState().get(MailboxDBTypes.ldap.name());
            final Attributes attrs = ((LDAPBackendState) backendState).attributes;
            executeLDAPUpdate(fulldc, attrs);
        } catch (InvalidNameException e) {
            logger.warn("Exception while updateDomain", e);
            throw new InvalidRequestException(DomainError.DMN_NOT_FOUND.name(),
                    e);
        } catch (NamingException e) {
            logger.warn("Exception while updateDomain", e);
            throw new ApplicationException(
                    DomainError.DMN_UNABLE_TO_PERFORM_UPDATE.name(), e);
        }
    }

    @Override
    public void deleteDomain(final String domainName) throws MxOSException {
        String fulldc = LDAPUtils.getDomainSearchQuery(domainName);
        try {
            ldapContext.unbind(fulldc);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while deleteDomain", e);
            throw new InvalidRequestException(DomainError.DMN_NOT_FOUND.name(),
                    e);
        } catch (NamingException e) {
            logger.warn("Exception while deleteDomain", e);
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_DELETE.name(), e);
        } catch (Exception e) {
            logger.error("Error while delete domain", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * buildAttributes method creates Attributes object with given attributes.
     * 
     * @return BasicAttribute Attributes
     */
    public static void getDefaultCosAttributes(Attributes attr) {
        BasicAttribute ocattr = new BasicAttribute("objectclass");
        String[] objectclasses = MxOSConfig.getCosObjectClasses();
        for (String oc : objectclasses) {
            ocattr.add(oc);
        }
        attr.put(ocattr);
    }

    /**
     * buildAttributes method creates Attributes object with given attributes.
     * 
     * @return BasicAttribute Attributes
     */
    public static void getDefaultMailboxAttributes(Attributes attr) {
        BasicAttribute ocattr = new BasicAttribute("objectclass");
        String[] objectclasses = MxOSConfig.getMailboxObjectClasses();
        for (String oc : objectclasses) {
            ocattr.add(oc);
        }
        attr.put(ocattr);
    }

    @Override
    public void createMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            // If groupName and type exists in the request parameters create group HOH
            // and if only groupName exists then create child(sub) mailbox in the given group
            // else create a normal mailbox
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.groupName.name())
                    && mxosRequestState.getInputParams().containsKey(
                            MailboxProperty.type.name())) {
                String type = mxosRequestState.getInputParams()
                        .get(MailboxProperty.type.name()).get(0);
                if (type.equals(MailboxType.GROUPADMIN.getType())) {
                    logger.info("Creating groupAdmin mailbox");
                    createFamilyMailboxHOH(mxosRequestState);
                    logger.info("Creating groupAdmin mailbox success");
                } else if (type.equals(MailboxType.GROUPMAILBOX.getType())) {
                    logger.info("Creating groupMailbox(sub mailbox) mailbox");
                    createChildMailbox(mxosRequestState);
                    logger.info("Creating groupMailbox(sub mailbox) mailbox success");
                }
            } else {
                logger.info("Creating mailbox");
                createNormalMailbox(mxosRequestState);
                logger.info("Creating mailbox success");
            }
        } catch (NameAlreadyBoundException e) {
            logger.warn("Mailbox already exists in LDAP and skipping LDAP mailbox creation");
            throw new InvalidRequestException(
                    MailboxError.MBX_ALREADY_EXISTS.name(), e);
        } catch (InvalidAttributeValueException e) {
            logger.warn("Failed due to invalid attribute value", e);
            throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (NameNotFoundException e) {
            logger.warn("Invalid Data provided", e);
            if (mxosRequestState.getDbPojoMap()
                    .getProperty(MxOSPOJOs.groupName) != null) {
                throw new InvalidRequestException(
                        MailboxError.MBX_GROUP_NOT_EXISTS.name(), e);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            }
        } catch (InvalidAttributesException e) {
            logger.warn("Invalid Data provided", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while createMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while createMailbox", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while createMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     *
     * @param mxosRequestState
     * @throws MxOSException
     * @throws NamingException
     */
    private void createNormalMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException, NamingException {
        String mailboxDn = LDAPUtils.getMailboxDn(mxosRequestState
                .getInputParams());
        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        getDefaultMailboxAttributes(((LDAPBackendState) backendState).attributes);
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;
        try {
            final Long mailboxId = (Long) attrs.get(
                    LDAPMailboxProperty.mailboxid.name()).get();
            attrs.put(LDAPMailboxProperty.mailboxid.name(),
                    mailboxId.toString());
        } catch (NamingException e) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(),
                    e);
        }
        executeLDAPCreate(mailboxDn, null,
                ((LDAPBackendState) backendState).attributes);
    }

    /**
     *
     * @param mxosRequestState
     * @throws MxOSException
     * @throws NamingException
     */
    private void createChildMailbox(
            final MxOSRequestState mxosRequestState) throws MxOSException,
            NamingException {
        String mailboxDn = LDAPUtils.getDnForGroupMailbox(mxosRequestState
                .getInputParams(),
                mxosRequestState.getDbPojoMap()
                        .getProperty(MxOSPOJOs.groupName));
        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        getDefaultMailboxAttributes(((LDAPBackendState) backendState).attributes);
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;
        try {
            final Long mailboxId = (Long) attrs.get(
                    LDAPMailboxProperty.mailboxid.name()).get();
            attrs.put(LDAPMailboxProperty.mailboxid.name(),
                    mailboxId.toString());
        } catch (NamingException e) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(),
                    e);
        }
        executeLDAPCreate(mailboxDn, null,
                ((LDAPBackendState) backendState).attributes);
    }

    /**
     * Create 
     * @param mxosRequestState
     * @throws MxOSException
     * @throws NamingException
     */
    private void createFamilyMailboxHOH(final MxOSRequestState mxosRequestState)
            throws MxOSException, NamingException {
        try {
            // Create place holders map
            Map<String, String> replacePlaceholdersMap
                = createPlaceHoldersMap(mxosRequestState);
            // Get the defaultHOHMap by replacing the place holders from
            // default-group-HOH.properties configuration file
            Map<String, Map<String, List<String>>> defaultHOHMap = LDAPUtils
                    .getDefaultHOHMap(replacePlaceholdersMap);

            if (logger.isDebugEnabled()) {
                logger.debug("Default HOH Map : " + defaultHOHMap);
            }

            /* CREATE adminRealmDn */
            try {
                logger.info("Creating adminRealmDn");
                Attributes createAdminRealmDnAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ADMINREALMDN.name()));
                String adminRealmDn = createAdminRealmDnAttrs.get("dn").get()
                        .toString();
                createAdminRealmDnAttrs.remove("dn");
                executeLDAPCreate(adminRealmDn, null, createAdminRealmDnAttrs);
                logger.info("Create adminRealmDn done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* CREATE adminAllocations */
            try {
                logger.info("Creating admin allocations");
                Attributes createAdminAllocsAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ADMINALLOCS.name()));
                String adminAllocsDn = createAdminAllocsAttrs.get("dn").get()
                        .toString();
                createAdminAllocsAttrs.remove("dn");
                executeLDAPCreate(adminAllocsDn, null, createAdminAllocsAttrs);
                logger.info("Create adminAllocations done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* CREATE roleHOH */
            try {
                logger.info("Creating roleHOH");
                Attributes createRoleHOHAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ROLEHOH.name()));
                String roleHOHDn = createRoleHOHAttrs.get("dn").get()
                        .toString();
                createRoleHOHAttrs.remove("dn");
                executeLDAPCreate(roleHOHDn, null, createRoleHOHAttrs);
                logger.info("Create roleHOH done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* CREATE organizationalUnit */
            logger.info("Creating organizationalUnit");
            try {
                Attributes organizationalUnitAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ORGANIZATIONALUNIT.name()));
                String organizationalUnitDn = organizationalUnitAttrs.get("dn")
                        .get().toString();
                organizationalUnitAttrs.remove("dn");
                executeLDAPCreate(organizationalUnitDn, null,
                        organizationalUnitAttrs);
                logger.info("Create organizationalUnit done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* UPDATE organizationUnit */
            logger.info("Updating organizationUnit with self-read ACI");
            Attributes selfReadACIAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.SELFREADACL.name()));
            String selfReadACIDn = selfReadACIAttrs.get("dn").get().toString();
            selfReadACIAttrs.remove("dn");
            executeLDAPUpdate(selfReadACIDn, selfReadACIAttrs);
            logger.info("Updating organizationUnit with self-read ACI done..!");

            /* UPDATE relateAdminRealm */
            logger.info("Updating relateAdminRealm");
            Attributes relateAdminRealmAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.RELATEADMINREALM.name()));
            String relateAdminRealmDn = relateAdminRealmAttrs.get("dn").get()
                    .toString();
            relateAdminRealmAttrs.remove("dn");
            executeLDAPUpdate(relateAdminRealmDn, relateAdminRealmAttrs);
            logger.info("Updating relateAdminRealm done..!");

            /* UPDATE memberAccessACI */
            logger.info("Updating memberAccessACI");
            Attributes memberAccessACIAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.MEMBERACCESSACI.name()));
            String memberAccessACIDn = memberAccessACIAttrs.get("dn").get()
                    .toString();
            memberAccessACIAttrs.remove("dn");
            executeLDAPUpdate(memberAccessACIDn, memberAccessACIAttrs);
            logger.info("Updating memberAccessACI done..!");

            /* UPDATE adminConstraints */
            logger.info("Updating adminConstraints");
            Attributes adminConstraintsAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.ADMINCONSTRAINTS.name()));
            String adminConstraintsDn = adminConstraintsAttrs.get("dn").get()
                    .toString();
            adminConstraintsAttrs.remove("dn");
            executeLDAPUpdate(adminConstraintsDn, adminConstraintsAttrs);
            logger.info("Updating adminConstraints done..!");

            /* CREATE Group Admin */
            logger.info("Creating groupHOH");
            Attributes groupHOHAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.GROUPHOH.name()));
            String groupHOHDn = groupHOHAttrs.get("dn").get().toString();
            groupHOHAttrs.remove("dn");
            executeLDAPCreate(groupHOHDn, null, groupHOHAttrs);
            logger.info("Create groupHOH done...!");

            /* UPDATE adminRealmDn */
            logger.info("Updating memberGroupHOH");
            Attributes groupHOHMemberAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.MEMBERGROUPHOH.name()));
            String groupHOHMemberDn = groupHOHMemberAttrs.get("dn").get()
                    .toString();
            groupHOHMemberAttrs.remove("dn");
            executeLDAPUpdate(groupHOHMemberDn, groupHOHMemberAttrs);
            logger.info("Updating memberGroupHOH done..!");

        } catch (NamingException e) {
            throw e;
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        }
    }

    /**
     *
     * @param defaultHOHMap
     * @return
     */
    private Attributes getDefaultHOHAtts(Map<String, List<String>> defaultHOHMap) {
        if(defaultHOHMap == null) {
            return null;
        }
        Attributes attrs = new BasicAttributes();
        Iterator<String> keysItr = defaultHOHMap.keySet().iterator();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            List<String> values = defaultHOHMap.get(key);
            if (values.size() > 1) {
                Attribute attr = new BasicAttribute(key);
                Iterator<String> valuesItr = values.iterator();
                while(valuesItr.hasNext()) {
                    attr.add(valuesItr.next());
                }
                attrs.put(attr);
            } else {
                Attribute attr = new BasicAttribute(key);
                attr.add(values.get(0));
                attrs.put(attr);
            }
        }
        return attrs;
    }
    
    private Map<String, String> createPlaceHoldersMap(
            final MxOSRequestState mxosRequestState) throws MxOSException {

        Map<String, String> replacePlaceholdersMap = new HashMap<String, String>();
        
        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;
        final String mailboxId;
        final String status;
        final String password;
        final String passwordStoreType;
        final String adminpolicydn;
        try {
            final Long id = (Long) attrs.get(
                    LDAPMailboxProperty.mailboxid.name()).get();
            mailboxId = id.toString();
            status = (String) attrs.get(
                    LDAPMailboxProperty.mailboxstatus.name()).get();
            password = (String) attrs.get(
                    LDAPMailboxProperty.mailpassword.name()).get();
            passwordStoreType = (String) attrs.get(
                    LDAPMailboxProperty.mailpasswordtype.name()).get();
            adminpolicydn = (String) attrs.get(
                    LDAPMailboxProperty.adminpolicydn.name()).get();
            
        } catch (Exception e) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(),
                    e);
        }

        final String group = mxosRequestState.getInputParams()
                .get(MailboxProperty.groupName.name()).get(0);
        final String origanizationalUnit = LDAPUtils
                .getOrganizationalUnitForGroup(group);
        final String groupName = group.split("\\.")[0];
        final String email = mxosRequestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // Optional params
        final String userNameAsEmail = System
                .getProperty(SystemProperty.storeUserNameAsEmail.name());
        final String[] token = email.split(MxOSConstants.AT_THE_RATE);
        final String userName = java.lang.Boolean.valueOf(userNameAsEmail) ? email
                : token[0];

        final String messageStoreHost = mxosRequestState.getAdditionalParams()
                .getProperty(MailboxProperty.messageStoreHost.name());
        final String autoReplyHost = mxosRequestState.getAdditionalParams()
                .getProperty(MailboxProperty.autoReplyHost.name());

        replacePlaceholdersMap.put("{groupName}", groupName);
        replacePlaceholdersMap.put("{mailboxId}", mailboxId);
        replacePlaceholdersMap.put("{mailboxStatus}", status);
        replacePlaceholdersMap.put("{password}", password);
        replacePlaceholdersMap.put("{passwordStoreType}", passwordStoreType);
        replacePlaceholdersMap.put("{cosId}", adminpolicydn);
        replacePlaceholdersMap.put("{email}", email);
        replacePlaceholdersMap.put("{userName}", userName);
        replacePlaceholdersMap.put("{organizationalUnit}", origanizationalUnit);
        replacePlaceholdersMap.put("{messageStoreHost}", messageStoreHost);
        replacePlaceholdersMap.put("{autoReplyHost}", autoReplyHost);
        return replacePlaceholdersMap;
    }

    @Override
    public Mailbox readMailbox(final String email) throws MxOSException {
        try {
            final Map<String, Attributes> cosMap = new LinkedHashMap<String, Attributes>();
            final Attributes mailboxResult = executeLDAPRetrieve(
                    getFormatFilter(email),
                    LDAPUtils.mailboxConfiguredAttributes, true, cosMap, true);
            if (mailboxResult != null) {
                final Mailbox mailbox = new Mailbox();
                mailbox.setBase(new Base());
                mailbox.setWebMailFeatures(new WebMailFeatures());
                mailbox.setCredentials(new Credentials());
                mailbox.setGeneralPreferences(new GeneralPreferences());
                mailbox.setMailSend(new MailSend());
                mailbox.setMailReceipt(new MailReceipt());
                mailbox.setMailAccess(new MailAccess());
                mailbox.setMailStore(new MailStore());
                mailbox.setInternalInfo(new InternalInfo());
                mailbox.setSmsServices(new SmsServices());
                mailbox.setSocialNetworks(new SocialNetworks());
                mailbox.setExternalAccounts(new ExternalAccounts());
                LDAPUtils.populateMailbox(mailbox, mailboxResult);
                return mailbox;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailbox", e);
            throw e;
        } catch (NamingException e) {
            logger.warn("Exception while readMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while readMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }
    @Override
    public List<Mailbox> readMultipleMailboxes(final List<String> list)
            throws MxOSException {
        List<Mailbox> mailboxes = new ArrayList<Mailbox>();
        try {
            final Map<String, Attributes> cosMap = new LinkedHashMap<String, Attributes>();
            for (String email : list) {
                try {
                    final Attributes mailboxResult = executeLDAPRetrieve(
                            getFormatFilter(email),
                            LDAPUtils.mailboxConfiguredAttributes, true,
                            cosMap, true);
                    if (mailboxResult != null) {
                        final Mailbox mailbox = new Mailbox();
                        mailbox.setBase(new Base());
                        mailbox.setWebMailFeatures(new WebMailFeatures());
                        mailbox.setCredentials(new Credentials());
                        mailbox.setGeneralPreferences(new GeneralPreferences());
                        mailbox.setMailSend(new MailSend());
                        mailbox.setMailReceipt(new MailReceipt());
                        mailbox.setMailAccess(new MailAccess());
                        mailbox.setMailStore(new MailStore());
                        mailbox.setInternalInfo(new InternalInfo());
                        mailbox.setSmsServices(new SmsServices());
                        mailbox.setSocialNetworks(new SocialNetworks());
                        mailbox.setExternalAccounts(new ExternalAccounts());
                        LDAPUtils.populateMailbox(mailbox, mailboxResult);
                        mailboxes.add(mailbox);
                    } else {
                        logger.warn(new StringBuffer().append("Mailbox ")
                                .append(email).append(" not found"));
                    }
                } catch (MxOSException e) {
                    // Skip and Do next if mailbox is not available or not
                    // found.
                    logger.error("Error while getting Mailbox for email, "
                            + email + " while multiple GET for mailbox", e);
                }
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while readMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
        return mailboxes;
    }

    @Override
    public List<Base> searchMailbox(final MxOSRequestState mxosRequestState,
            final int maxSearchRows, final int maxSearchTimeout)
            throws MxOSException {
        final String formattedLdapFilter = mxosRequestState.getInputParams()
                .get(MailboxProperty.query.name()).get(0);
        final String ldapBaseDn;
        if (mxosRequestState.getInputParams().get(MailboxProperty.scope.name()) != null) {
            ldapBaseDn = LDAPUtils.getScopeSearchQuery(mxosRequestState
                    .getInputParams().get(MailboxProperty.scope.name()).get(0));
        } else
            ldapBaseDn = "";
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, maxSearchRows, maxSearchTimeout,
                LDAPUtils.mailboxAttributes, true, false);

        NamingEnumeration<SearchResult> results = null;
        // COS Caching
        final Map<String, Attributes> cosCache = new HashMap<String, Attributes>();
        final long t0 = Stats.startTimer();
        try {
            results = executeLDAPSearch(ldapBaseDn, formattedLdapFilter, sc);
            final List<Base> bases = new ArrayList<Base>();
            try {
                while (results != null && results.hasMore()) {
                    final SearchResult searchResult = results.next();
                    if (searchResult != null
                            && searchResult.getAttributes() != null) {
                        Attributes attrs = searchResult.getAttributes();

                        String groupName = searchResult.getNameInNamespace();
                        if (logger.isDebugEnabled()) {
                            logger.debug("GroupName : " + groupName);
                        }
                        if (MxOSConfig.isGroupMailboxEnabled()) {
                            // Check and retrieve family mailbox attributes
                            Attributes groupAttrs = retrieveFamilyMailboxAttrs(
                                    attrs, groupName, true);
                            attrs = addAttrsToExistingAttrs(attrs, groupAttrs);
                        }
                        Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime, Operation.SEARCH, t0,
                                StatStatus.pass);
                        final Attribute cosIdAttr = attrs
                                .get(LDAPMailboxProperty.adminpolicydn.name());
                        if (cosIdAttr != null && cosIdAttr.get() != null) {
                            final Attributes cosResult;
                            // check the cos is already exists in cosCache
                            // read otherwise.
                            if (cosCache
                                    .containsKey(cosIdAttr.get().toString())) {
                                cosResult = cosCache.get(cosIdAttr.get()
                                        .toString());
                            } else {
                                cosResult = getCosAttributes(cosIdAttr.get()
                                        .toString(), LDAPUtils.mailboxAttributes);
                                cosCache.put(cosIdAttr.get().toString(),
                                        cosResult);
                            }
                            if (cosResult != null) {
                                // add cos data into mailbox data
                                attrs = addAttrsToExistingAttrs(attrs,
                                        cosResult);
                            }
                        }
                        final Base base = new Base();
                        try {
                            LDAPUtils.populateBase(base, attrs);
                            bases.add(base);
                        } catch (Throwable e) {
                            logger.error("Problem while populating Mailbox : "
                                    + base);
                        }
                    }
                } // End of while
            } catch (SizeLimitExceededException e) {
                // Nothing to do.
            } catch (TimeLimitExceededException e) {
                // Nothing to do.
            }
            return bases;
        } catch (NamingException e) {
            logger.warn("Exception while searchMailbox", e);
            Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime, Operation.SEARCH, t0,
                    StatStatus.fail);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SEARCH.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while searchMailbox", e);
            Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime, Operation.SEARCH, t0,
                    StatStatus.fail);
            throw e;
        } catch (Exception e) {
            logger.error("Error while searchMailbox", e);
            Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime, Operation.SEARCH, t0,
                    StatStatus.fail);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    /**
     * Retrieve all attributes in the given ldapBaseDn.
     * 
     * @param ldapBaseDn.
     * @return attributes
     * @throws MxOSException.
     */

    public Attributes readAdminAllocationsAttributes(final String ldapBaseDn)
            throws MxOSException {
        NamingEnumeration<SearchResult> results = null;
        Attributes attrs = null;
        // COS Caching
        final Map<String, Attributes> cosCache = new HashMap<String, Attributes>();
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 0, maxTimeOut,
                    LDAPUtils.mailboxAttributes, true, false);
            if (ldapBaseDn != null) {
                results = executeLDAPSearch(ldapBaseDn, "objectclass=*", sc);
            }
            int adminNumUsers = 0;
            List<String> subMailboxArray = new ArrayList<String>();
            long adminUsedStorageSizeKB = 0;
            try {
                while (results != null && results.hasMore()) {
                    final SearchResult searchResult = results.next();
                    if (searchResult != null
                            && searchResult.getAttributes() != null) {
                        attrs = searchResult.getAttributes();
                        final Attribute cosIdAttr = attrs
                                .get(LDAPMailboxProperty.adminpolicydn.name());
                        if (cosIdAttr != null && cosIdAttr.get() != null) {
                            final Attributes cosResult;
                            // check the cos is already exists in cosCache
                            // read otherwise.
                            if (cosCache
                                    .containsKey(cosIdAttr.get().toString())) {
                                cosResult = cosCache.get(cosIdAttr.get()
                                        .toString());
                            } else {
                                cosResult = getCosAttributes(cosIdAttr.get()
                                        .toString(), LDAPUtils.mailboxAttributes);
                                cosCache.put(cosIdAttr.get().toString(),
                                        cosResult);
                            }
                            if (cosResult != null) {
                                // add cos data into mailbox data
                                attrs = addAttrsToExistingAttrs(attrs,
                                        cosResult);
                            }
                        }
                        if (MxOSConfig.isGroupAdminIncluded()
                                || (attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                                .name()) != null && attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                                .name())
                                        .get()
                                        .toString()
                                        .equalsIgnoreCase(
                                                MailboxType.GROUPMAILBOX
                                                        .getValue()))) {
                            if (attrs.get(LDAPMailboxProperty.mail.name()) != null) {
                                adminNumUsers++;
                                subMailboxArray.add(attrs
                                        .get(LDAPMailboxProperty.mail.name())
                                        .get().toString());
                            }
                            if (attrs.get(LDAPMailboxProperty.mailquotatotkb
                                    .name()) != null) {
                                adminUsedStorageSizeKB = adminUsedStorageSizeKB
                                        + Long.parseLong(attrs
                                                .get(LDAPMailboxProperty.mailquotatotkb
                                                        .name()).get()
                                                .toString());
                            }
                        }
                    }

                } // End of while
                if (logger.isDebugEnabled()) {
                    logger.debug("value of adminNumUsers is :" + adminNumUsers);
                    logger.debug("value of derived mailwquota is :"
                            + adminUsedStorageSizeKB);
                }
                attrs.put(LDAPGroupAdminMailboxProperty.adminnumusers.name(),
                        adminNumUsers);
                attrs.put(
                        LDAPGroupAdminMailboxProperty.adminusedstoragekb.name(),
                        adminUsedStorageSizeKB);
            } catch (SizeLimitExceededException e) {
                logger.warn("Exception get groupAdminAllocation attributes", e);
                throw e;
            } catch (TimeLimitExceededException e) {
                logger.warn("Exception get groupAdminAllocation attributes", e);
                throw e;
            }
            return attrs;
        } catch (NamingException e) {
            logger.warn("Exception while get groupAdminAllocation attributes",
                    e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while get groupAdminAllocation attributes",
                    e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while get groupAdminAllocation attributes", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    /**
     * Search Credentials objects for mail/maillogin/mailalternateaddress.
     * 
     * @param mxosRequestState - input request state.
     * @return Map<String, Credentials>.
     * @throws MxOSException on Credentials(s) not found or any other error.
     */
    @Override
    public Credentials searchCredentialsForEmailAndUsername(
            final MxOSRequestState requestState) throws MxOSException {

        Credentials cred = new Credentials();
        List<Credentials> credentials = new ArrayList<Credentials>();
        requestState.getInputParams().put(MailboxProperty.query.name(),
                new ArrayList<String>());
        int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        String email = null;
        String filter = null;
        StringBuilder filterEmail = new StringBuilder();
        StringBuilder filterUsername = new StringBuilder();
        filterEmail = filterEmail.append("{")
                .append(MailboxProperty.email.name()).append("}");
        filterUsername = filterUsername.append("{")
                .append(MailboxProperty.userName.name()).append("}");

        // check if email is used for authentication
        // try {
        if (requestState.getInputParams().get(MailboxProperty.email.name()) != null) {
            try {
                email = requestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0);
                filter = MxOSConstants.QUERY_SEARCH_CRITERIA_USING_MAILORMAILALTERNATEADDRESS
                        .replace(filterEmail.toString(), email);
                requestState.getInputParams().get(MailboxProperty.query.name())
                        .add(filter);
                credentials = retrieveCredentials(requestState);
            } catch (NameNotFoundException e) {
                logger.info("DN lookup for " +email +" failed; trying LDAP search with mail or mailAlternateAddress " +filter);
                credentials = searchCredentials(requestState,
                        MxOSConstants.MAX_SEARCH_ROWS, maxTimeOut);
            }
            if (null == credentials || credentials.isEmpty()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_NOT_FOUND.name());
            } else {
                cred = credentials.get(0);
            }
            // use username in filtering condition if username is used for
            // authentication instead of email
        } else {
            final String userName = requestState.getInputParams()
                    .get(MailboxProperty.userName.name()).get(0);
            filter = MxOSConstants.QUERY_SEARCH_CRITERIA_USING_MAILLOGIN
                    .replace(filterUsername.toString(), userName);
            requestState.getInputParams().get(MailboxProperty.query.name())
                    .clear();
            requestState.getInputParams().get(MailboxProperty.query.name())
                    .add(filter);
            credentials = searchCredentials(requestState,
                    MxOSConstants.MAX_SEARCH_ROWS, maxTimeOut);
            if (null == credentials || credentials.isEmpty()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_NOT_FOUND.name());
            } else {
                cred = credentials.get(0);
            }

        }
        return cred;
    }

    public List<Credentials> retrieveCredentials(
            final MxOSRequestState mxosRequestState) throws MxOSException,
            NameNotFoundException {

        final List<Credentials> credentials = new ArrayList<Credentials>();
        // COS Caching
        final Map<String, Attributes> cosCache = new HashMap<String, Attributes>();
        try {
            Attributes attrs = executeLDAPRetrieveCredentialsForAuthenticate(
                    LDAPUtils.getMailboxDn(mxosRequestState.getInputParams()
                            .get(MailboxProperty.email.name()).get(0)),
                    LDAPUtils.mailboxAttributes);
            final Attribute cosIdAttr = attrs
                    .get(LDAPMailboxProperty.adminpolicydn.name());
            if (cosIdAttr != null && cosIdAttr.get() != null) {
                final Attributes cosResult;
                // check if cos already exists in cosCache
                // read otherwise.
                if (cosCache.containsKey(cosIdAttr.get().toString())) {
                    cosResult = cosCache.get(cosIdAttr.get().toString());
                } else {
                    cosResult = getCosAttributes(cosIdAttr.get().toString(),
                            LDAPUtils.mailboxAttributes);
                    cosCache.put(cosIdAttr.get().toString(), cosResult);
                }
                if (cosResult != null) {
                    // add cos data into mailbox data
                    attrs = addAttrsToExistingAttrs(attrs, cosResult);
                }
            }
            final Base base = new Base();
            final Credentials cred = new Credentials();
            final MssLinkInfo info = new MssLinkInfo();
            LDAPUtils.populateBase(base, attrs);
            if (mxosRequestState.getInputParams().get(
                    MailboxProperty.email.name()) != null)
                mxosRequestState.getInputParams()
                        .get(MailboxProperty.email.name()).clear();
            mxosRequestState.getInputParams().put(MailboxProperty.email.name(),
                    new ArrayList<String>());
            mxosRequestState.getInputParams().get(MailboxProperty.email.name())
                    .add(base.getEmail());
            LDAPUtils.populateMSSLinkInfo(info, attrs);
            mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.mssLinkInfo,
                    info);
            LDAPUtils.populateCredentials(cred, attrs);
            credentials.add(cred);
            return credentials;
        } catch (NameNotFoundException e) {
            logger.warn("Exception while searching credentials.");
            throw e;
        } catch (NamingException e) {
            logger.warn("Exception while searching Credentials", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SEARCH_CREDENTIALS.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while searching Credentials", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while searching Credentials", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        }
    }

    private Attributes executeLDAPRetrieveCredentialsForAuthenticate(
            final String fulldc, final String[] attributeNames)
            throws MxOSException, NamingException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Executing LDAP retrieve with domain - " + fulldc);
            }
            Attributes result = ldapContext.getAttributes(fulldc,
                    attributeNames);
            return result;
        } catch (final AuthenticationException e) {
            logger.error("Error in LDAP retrieve.", e);
            throw new AuthorizationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.error("Time out error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw e;
        } catch (final CommunicationException e) {
            this.status = false;
            logger.error("Error in LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP retrieve.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Error while reading Mailbox.");
            throw e;
        } catch (final Exception e) {
            logger.error("Error in LDAP retrieve.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<Credentials> searchCredentials(
            final MxOSRequestState mxosRequestState, final int maxSearchRows,
            final int maxSearchTimeout) throws MxOSException {
        final String formattedLdapFilter = mxosRequestState.getInputParams()
                .get(MailboxProperty.query.name()).get(0);
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, maxSearchRows, maxSearchTimeout,
                LDAPUtils.mailboxAttributes, true, false);

        NamingEnumeration<SearchResult> results = null;
        final List<Credentials> credentials = new ArrayList<Credentials>();
        // COS Caching
        final Map<String, Attributes> cosCache = new HashMap<String, Attributes>();
        try {
            results = executeLDAPSearch("", formattedLdapFilter, sc);
            try {
                while (results != null && results.hasMore()) {
                    final SearchResult searchResult = results.next();
                    if (searchResult != null
                            && searchResult.getAttributes() != null) {
                        Attributes attrs = searchResult.getAttributes();
                        final Attribute cosIdAttr = attrs
                                .get(LDAPMailboxProperty.adminpolicydn.name());
                        if (cosIdAttr != null && cosIdAttr.get() != null) {
                            final Attributes cosResult;
                            // check if cos already exists in cosCache
                            // read otherwise.
                            if (cosCache
                                    .containsKey(cosIdAttr.get().toString())) {
                                cosResult = cosCache.get(cosIdAttr.get()
                                        .toString());
                            } else {
                                cosResult = getCosAttributes(cosIdAttr.get()
                                        .toString(), LDAPUtils.mailboxAttributes);
                                cosCache.put(cosIdAttr.get().toString(),
                                        cosResult);
                            }
                            if (cosResult != null) {
                                // add cos data into mailbox data
                                attrs = addAttrsToExistingAttrs(attrs,
                                        cosResult);
                            }
                        }
                        final Base base = new Base();
                        final Credentials cred = new Credentials();
                        final MssLinkInfo info = new MssLinkInfo();
                        try {
                            LDAPUtils.populateBase(base, attrs);
                            if (mxosRequestState.getInputParams().get(
                                    MailboxProperty.email.name()) != null)
                                mxosRequestState.getInputParams()
                                        .get(MailboxProperty.email.name())
                                        .clear();
                            mxosRequestState.getInputParams().put(
                                    MailboxProperty.email.name(),
                                    new ArrayList<String>());
                            mxosRequestState.getInputParams()
                                    .get(MailboxProperty.email.name())
                                    .add(base.getEmail());
                            LDAPUtils.populateMSSLinkInfo(info, attrs);
                            mxosRequestState.getDbPojoMap()
                            .setProperty(MxOSPOJOs.mssLinkInfo, info);
                            LDAPUtils.populateCredentials(cred, attrs);
                            credentials.add(cred);
                        } catch (NameNotFoundException e) {
                            logger.warn(
                                    "Exception while populating credentials", e);
                            throw new InvalidRequestException(
                                    MailboxError.MBX_NOT_FOUND.name(), e);
                        } catch (Exception e) {
                            logger.error("Problem in populating credentials : "
                                    + cred);
                        }
                    }
                } // End of while
            } catch (SizeLimitExceededException e) {
                logger.warn("Exception while searching Credentials", e);
                throw e;
            } catch (TimeLimitExceededException e) {
                logger.warn("Exception while searching Credentials", e);
                throw e;
            }
            return credentials;
        } catch (NamingException e) {
            logger.warn("Exception while searching Credentials", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SEARCH_CREDENTIALS.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while searching Credentials", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while searching Credentials", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    @Override
    public Credentials readRawCredentials(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final Credentials credentials = new Credentials();
                LDAPUtils.populateRawCredentials(credentials, attrs);
                return credentials;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading raw Credentials", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void updateMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            String mailboxDn = null;
            if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.groupAdminAllocCountsDn) != null) {
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.groupAdminAllocCountsDn);
            } else if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.adminRealmDn) != null) {
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.adminRealmDn);
            } else if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.familyMailboxDn) != null) {
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.familyMailboxDn);
            } else {
                mailboxDn = LDAPUtils.getMailboxDn(mxosRequestState
                        .getInputParams());
            }
            final IBackendState backendState = mxosRequestState
                    .getBackendState().get(MailboxDBTypes.ldap.name());
            final Attributes attrs = ((LDAPBackendState) backendState).attributes;
            if (attrs == null || attrs.size() == 0) {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }
            executeLDAPUpdate(mailboxDn, attrs);
        } catch (InvalidNameException e) {
            logger.warn("Exception while updateMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while updateMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while updateMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_UPDATE.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while updateMailbox", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while updateMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    private void executeLDAPDelete(final String mailboxDn) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing LDAP delete with dn - " + mailboxDn);
        }
        ldapContext.unbind(mailboxDn);
    }

    @Override
    public void deleteMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            String mailboxDn = null;
            if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.familyMailboxDn) != null) {
                if (mxosRequestState.getDbPojoMap().getProperty(MxOSPOJOs.type) != null) {
                    if (mxosRequestState
                            .getDbPojoMap()
                            .getProperty(MxOSPOJOs.type)
                            .equalsIgnoreCase(MailboxType.GROUPADMIN.getValue())) {
                        logger.error("Delete not allowed for group admin account.");
                        throw new InvalidRequestException(
                                MailboxError.MBX_UNABLE_TO_DELETE_GROUP_ADMIN
                                        .name(),
                                "Mailbox delete not allowed for group admin(HOH) account.");
                    }
                }
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.familyMailboxDn);
            } else {
                mailboxDn = LDAPUtils.getMailboxDn(mxosRequestState
                        .getInputParams());
            }
            executeLDAPDelete(mailboxDn);
        } catch (final NoSuchAttributeException e) {
            logger.warn("Exception while deleteMailbox", e);
            throw new NotFoundException(ErrorCode.LDP_NO_SUCH_ATTRIBUTE.name(),
                    e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while deleteMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.warn("Exception while deleteMailbox", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final CommunicationException e) {
            logger.warn("Exception while deleteMailbox", e);
            this.status = false;
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP delete.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while deleteMailbox", e);
            throw new ApplicationException(ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final MxOSException e) {
            logger.error("Error while deleteMailbox", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error while deleteMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void commit() throws ApplicationException {
        // Not required for LDAP

    }

    @Override
    public void rollback() throws ApplicationException {
        // Not required for LDAP
    }

    @Override
    public boolean authenticate(String email, String password)
            throws MxOSException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String readMailboxDn(final MxOSRequestState requestState)
            throws MxOSException {
        String[] attributes = new String[] { LDAPMailboxProperty.mailfamilymailbox
                .name() };
        Attributes attrs = null;
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 1, maxTimeOut, attributes,
                    true, false);
            NamingEnumeration<SearchResult> results = null;
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            results = ldapContext.search("", getFormatFilter(email), sc);
            if (results != null && results.hasMore()) {
                final SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    attrs = searchResult.getAttributes();
                    String mailFamilyMailbox = null;
                    if (attrs.get(LDAPMailboxProperty.mailfamilymailbox.name()) != null) {
                        mailFamilyMailbox = attrs
                                .get(LDAPMailboxProperty.mailfamilymailbox
                                        .name()).get().toString();
                        if (logger.isDebugEnabled()) {
                            logger.debug("mailFamilyMailbox : "
                                    + mailFamilyMailbox);
                        }
                        if (mailFamilyMailbox.equals(MailboxType.GROUPMAILBOX
                                .getValue())
                                || mailFamilyMailbox
                                        .equals(MailboxType.GROUPADMIN
                                                .getValue())) {
                            String nameInNamespace = searchResult
                                    .getNameInNamespace();
                            if (logger.isDebugEnabled()) {
                                logger.debug("NameInNamespace : "
                                        + nameInNamespace);
                            }
                            requestState.getDbPojoMap().setProperty(
                                    MxOSPOJOs.familyMailboxDn, nameInNamespace);
                            requestState.getDbPojoMap().setProperty(
                                    MxOSPOJOs.type, mailFamilyMailbox);
                            return nameInNamespace;
                        }
                    }
                }
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readGroupMailboxDn", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readGroupMailboxDn", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET_GROUP_MALIBOX_DN.name(), e);
        } catch (Exception e) {
            logger.error("Error while readGroupMailboxDn", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
        return null;
    }

    @Override
    public String readAdminRealmDn(String email) throws MxOSException {
        String[] attributes = new String[] { LDAPMailboxProperty.mailfamilymailbox
                .name() };
        Attributes attrs = null;
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 1, maxTimeOut, attributes,
                    true, false);
            NamingEnumeration<SearchResult> results = null;
            results = ldapContext.search("", getFormatFilter(email), sc);
            if (results != null && results.hasMore()) {
                final SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    attrs = searchResult.getAttributes();
                    String mailFamilyMailbox = null;
                    if (attrs.get(LDAPMailboxProperty.mailfamilymailbox.name()) != null) {
                        mailFamilyMailbox = attrs
                                .get(LDAPMailboxProperty.mailfamilymailbox
                                        .name()).get().toString();
                        if (logger.isDebugEnabled()) {
                            logger.debug("mailFamilyMailbox : "
                                    + mailFamilyMailbox);
                        }
                        if (mailFamilyMailbox.equals(MailboxType.GROUPADMIN
                                .getValue())) {
                            String nameInNamespace = searchResult
                                    .getNameInNamespace();
                            if (logger.isDebugEnabled()) {
                                logger.debug("NameInNamespace : "
                                        + nameInNamespace);
                            }
                            Attributes groupAttrs = getGroupMailboxAttributes(LDAPUtils
                                    .getGroupDnFromGroupName(nameInNamespace));
                            if (groupAttrs != null
                                    && groupAttrs
                                            .get(LDAPGroupMailboxProperty.adminRealmdn
                                                    .name()) != null) {
                                return groupAttrs
                                        .get(LDAPGroupMailboxProperty.adminRealmdn
                                                .name()).get().toString();
                            }
                        } else {
                            throw new InvalidRequestException(
                                    MailboxError.MBX_NOT_GROUP_ADMIN_ACCOUNT
                                            .name());
                        }
                    } else {
                        throw new InvalidRequestException(
                                MailboxError.MBX_NOT_GROUP_ACCOUNT.name());
                    }
                }
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readAdminRealmDn", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readAdminRealmDn", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET_ADMIN_REALM_DN.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readAdminRealmDn", e);
            throw e;
        }
        return null;
    }

    @Override
    public Base readMailboxBase(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), MxOSConfig.isGroupMailboxEnabled());
            if (attrs != null) {
                final Base base = new Base();
                base.setAllowedDomains(new ArrayList<String>());
                base.setEmailAliases(new ArrayList<String>());
                LDAPUtils.populateBase(base, attrs);
                return base;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailboxBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public WebMailFeatures readWebMailFeatures(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final WebMailFeatures wmf = new WebMailFeatures();
                LDAPUtils.populateWebMailFeatures(wmf, attrs);
                return wmf;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readWebMailFeatures", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readWebMailFeatures", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readWebMailFeatures", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readWebMailFeatures", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public WebMailFeatures readCosWebMailFeatures(String cosId)
            throws MxOSException {
        try {
            final WebMailFeatures wmf = new WebMailFeatures();
            final Attributes attrs = executeLDAPRetrieve(getCosFilter(cosId),
                    false);
            if (null == attrs) {
                throw new NameNotFoundException();
            }
            LDAPUtils.populateWebMailFeatures(wmf, attrs);
            return wmf;
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosWebMailFeatures", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosWebMailFeatures", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosWebMailFeatures", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading WebMailFeatures for Cos.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public Credentials readCredentials(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final Credentials credentials = new Credentials();
                LDAPUtils.populateCredentials(credentials, attrs);
                return credentials;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCredentials", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCredentials", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCredentials", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCredentials", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public GeneralPreferences readGeneralPreferences(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final GeneralPreferences gp = new GeneralPreferences();
                LDAPUtils.populateGeneralPreferences(gp, attrs);
                return gp;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readGeneralPreferences", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readGeneralPreferences", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readGeneralPreferences", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readGeneralPreferences", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailSend readMailSend(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final MailSend ms = new MailSend();
                LDAPUtils.populateMailSend(ms, attrs);
                return ms;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailSend", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailSend", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailSend", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailSend", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailReceipt readMailReceipt(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), MxOSConfig.isGroupMailboxEnabled());
            if (attrs != null) {
                final MailReceipt mr = new MailReceipt();
                LDAPUtils.populateMailReceipt(mr, attrs);
                return mr;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailReceipt", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailReceipt", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailReceipt", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailReceipt", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailAccess readMailAccess(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final MailAccess ma = new MailAccess();
                ma.setAllowedIPs(new ArrayList<String>());
                LDAPUtils.populateMailAccess(ma, attrs);
                return ma;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailAccess", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailAccess", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailAccess", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailAccess", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailStore readMailStore(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), MxOSConfig.isGroupMailboxEnabled());
            if (attrs != null) {
                final MailStore ms = new MailStore();
                LDAPUtils.populateMailStore(ms, attrs);
                return ms;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailStore", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailStore", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailStore", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailStore", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public SmsServices readSmsServices(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final SmsServices smsServices = new SmsServices();
                LDAPUtils.populateSmsServices(smsServices, attrs);
                return smsServices;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readSmsServices", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readSmsServices", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readSmsServices", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readSmsServices", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public InternalInfo readInternalInfo(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final InternalInfo info = new InternalInfo();
                info.setMessageEventRecords(new MessageEventRecords());
                LDAPUtils.populateInternalInfo(info, attrs);
                return info;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readInternalInfo", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readInternalInfo", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readInternalInfo", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readInternalInfo", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MessageEventRecords readMessageEventRecords(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final MessageEventRecords msgEventRecords = new MessageEventRecords();
                LDAPUtils.populateMessageEventRecords(msgEventRecords, attrs);
                return msgEventRecords;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMessageEventRecords", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMessageEventRecords", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMessageEventRecords", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMessageEventRecords", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public InternalInfo readCosInternalInfo(String cosId) throws MxOSException {
        try {
            final InternalInfo info = new InternalInfo();
            info.setMessageEventRecords(new MessageEventRecords());
            final Attributes attrs = executeLDAPRetrieve(getCosFilter(cosId),
                    false);
            if (null == attrs) {
                throw new NameNotFoundException();
            }
            LDAPUtils.populateInternalInfo(info, attrs);
            info.setVersion(null);
            return info;
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosInternalInfo", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosInternalInfo", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosInternalInfo", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading InternalInfo for Cos.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MessageEventRecords readCosMessageEventRecords(String cosId)
            throws MxOSException {
        try {
            final MessageEventRecords mers = new MessageEventRecords();
            final Attributes attrs = executeLDAPRetrieve(getCosFilter(cosId),
                    false);
            if (null == attrs) {
                throw new NameNotFoundException();
            }
            LDAPUtils.populateMessageEventRecords(mers, attrs);
            return mers;
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMessageEventRecords", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMessageEventRecords", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMessageEventRecords", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading MessageEventRecords for Cos.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MssLinkInfo readMSSLinkInfo(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieveWithoutGroupAttributes(getFormatFilter(email));
            if (attrs != null) {
                final MssLinkInfo info = new MssLinkInfo();
                LDAPUtils.populateMSSLinkInfo(info, attrs);
                return info;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMSSLinkInfo", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMSSLinkInfo", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMSSLinkInfo", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMSSLinkInfo", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public SocialNetworks readSocialNetworks(String email) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final SocialNetworks sn = new SocialNetworks();
                sn.setSocialNetworkSites(new ArrayList<SocialNetworkSite>());
                LDAPUtils.populateSocialNetworks(sn, attrs);
                return sn;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readSocialNetworks", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readSocialNetworks", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readSocialNetworks", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readSocialNetworks", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    private static String getFormatFilter(final String email) {
        String filter = System.getProperty(SystemProperty.ldapReadMailboxFilter
                .name());
        return filter.replace("{" + MailboxProperty.email.name() + "}", email);
    }

    private static String getCosFilter(final String cosId) {
        return "(&(objectclass=adminPolicy)(cn=" + cosId + "))";
    }

    /**
     * Method to dump attributes.
     * 
     * @param attrs attrs
     */
    public void dumpAttributes(Attributes attrs) {
        try {
            final NamingEnumeration<? extends Attribute> namingEnum = attrs
                    .getAll();
            while (namingEnum != null && namingEnum.hasMore()) {
                final Attribute attr = namingEnum.next();
                logger.debug("Key = " + attr.getID() + ", Value = "
                        + attr.get().toString());
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void createCos(MxOSRequestState requestState) throws MxOSException {
        try {
            final String cosDn = LDAPUtils.getCosDn(requestState
                    .getInputParams());
            IBackendState backendState = requestState.getBackendState().get(
                    MailboxDBTypes.ldap.name());
            getDefaultCosAttributes(((LDAPBackendState) backendState).attributes);
            // To dump the attributes.
            // dumpAttributes(((LDAPBackendState) backendState).attributes);
            executeLDAPCreate(cosDn, null,
                    ((LDAPBackendState) backendState).attributes);
        } catch (NameAlreadyBoundException e) {
            logger.warn("Exception while createCos", e);
            throw new InvalidRequestException(
                    CosError.COS_ALREADY_EXISTS.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while createCos", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_CREATE.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while createCos", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while createCos", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }

    }

    @Override
    public Base readCosBase(String cosId) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final Base base = new Base();
                LDAPUtils.populateBase(base, attrs);
                // type should not passed in cos base api.
                base.setType(null);
                return base;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosBase", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosBase", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosBase", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public SocialNetworks readCosSocialNetworks(String cosId)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final SocialNetworks sn = new SocialNetworks();
                LDAPUtils.populateSocialNetworks(sn, attrs);
                return sn;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosSocialNetworks", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosSocialNetworks", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosSocialNetworks", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void updateCos(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosDn = LDAPUtils.getCosDn(mxosRequestState
                    .getInputParams());
            final IBackendState backendState = mxosRequestState
                    .getBackendState().get(MailboxDBTypes.ldap.name());
            final Attributes attrs = ((LDAPBackendState) backendState).attributes;
            if (attrs == null || attrs.size() == 0) {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }
            executeLDAPUpdate(cosDn, attrs);
            //cosMap.put(cosDn, attrs);
        } catch (InvalidNameException e) {
            logger.warn("Exception while updateCos", e);
            throw new InvalidRequestException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while updateCos", e);
            throw new InvalidRequestException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while updateCos", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_UPDATE.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while updateCos", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while updateCos", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void deleteCos(String cosId) throws MxOSException {
        try {
            final String cosDn = LDAPUtils.getCosDn(cosId);
            executeLDAPDelete(cosDn);
            //cosMap.remove(cosDn);
        } catch (final NoSuchAttributeException e) {
            logger.warn("Exception while deleteCos", e);
            throw new NotFoundException(ErrorCode.LDP_NO_SUCH_ATTRIBUTE.name(),
                    e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while deleteCos", e);
            throw new InvalidRequestException(CosError.COS_NOT_FOUND.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.warn("Exception while deleteCos", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final CommunicationException e) {
            logger.warn("Exception while deleteCos", e);
            this.status = false;
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP deleteCOS.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final InvalidAttributeValueException e) {
            logger.warn("Exception while deleteCos", e);
            throw new InvalidRequestException(CosError.COS_COSID_IN_USE.name(),
                    e);
        } catch (final NamingException e) {
            logger.warn("Exception while deleteCos", e);
            throw new ApplicationException(ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final Exception e) {
            logger.error("Error while deleteCos", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<Base> searchCos(MxOSRequestState mxosRequestState,
            int maxSearchRows, int maxSearchTimeout) throws MxOSException {
        final String formattedLdapFilter = mxosRequestState.getInputParams()
                .get(MailboxProperty.query.name()).get(0);
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, maxSearchRows, maxSearchTimeout,
                LDAPUtils.mailboxAttributes, true, false);

        NamingEnumeration<SearchResult> results = null;
        try {
            if (MxOSConfig.getLdapCosBaseDn().contains(MxOSConstants.COMMA))
                results = executeLDAPSearch(
                        MxOSConfig.getLdapCosBaseDn().substring(
                                MxOSConfig.getLdapCosBaseDn().indexOf(
                                        MxOSConstants.COMMA)+1),
                        formattedLdapFilter, sc);
            else
                results = executeLDAPSearch(MxOSConfig.getLdapCosBaseDn(),
                        formattedLdapFilter, sc);
            final List<Base> bases = new ArrayList<Base>();
            try {
                while (results != null && results.hasMore()) {
                    final SearchResult searchResult = results.next();
                    if (searchResult != null
                            && searchResult.getAttributes() != null) {
                        final Base base = new Base();
                        Attribute cosId = searchResult.getAttributes().get(
                                MailboxProperty.Cn.name());
                        if (cosId != null) {
                            base.setCosId(cosId.get().toString());
                        }
                        try {
                            LDAPUtils.populateBase(base,
                                    searchResult.getAttributes());
                            bases.add(base);
                        } catch (Throwable e) {
                            logger.error("Problem while populating Cos : "
                                    + base);
                        }
                    }
                }// End of while
            } catch (SizeLimitExceededException e) {
                // Nothing to do.
            } catch (TimeLimitExceededException e) {
                // Nothing to do.
            }
            return bases;
        } catch (NamingException e) {
            logger.warn("Exception while searchCos", e);
            throw new InvalidRequestException(
            		CosError.COS_UNABLE_TO_PERFROM_SEARCH.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while searchCos", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while search cos", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    @Override
    public ExternalAccounts readExternalAccounts(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final ExternalAccounts ea = new ExternalAccounts();
                LDAPUtils.populateExternalAccounts(ea, attrs);
                return ea;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readExternalAccounts", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readExternalAccounts", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readExternalAccounts", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readExternalAccounts", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public ExternalAccounts readCosExternalAccounts(String cosId)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final ExternalAccounts extAcc = new ExternalAccounts();
                LDAPUtils.populateExternalAccounts(extAcc, attrs);
                return extAcc;
            } else {
                throw new NotFoundException(
                        CosError.COS_EXTERNAL_ACCOUNT_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosExternalAccounts", e);
            throw new NotFoundException(
                    CosError.COS_EXTERNAL_ACCOUNT_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosExternalAccounts", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosExternalAccounts", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosExternalAccounts", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }

    }

    @Override
    public SmsServices readCosSmsServices(String cosId) throws MxOSException {

        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);

            if (attrs != null) {
                final SmsServices smsServices = new SmsServices();
                LDAPUtils.populateSmsServices(smsServices, attrs);
                return smsServices;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }

        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosSmsServices", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosSmsServices", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosSmsServices", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosSmsServices", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailAccess readCosMailAccess(String cosId) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);

            if (attrs != null) {
                final MailAccess ma = new MailAccess();
                LDAPUtils.populateMailAccess(ma, attrs);
                return ma;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailAccess", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailAccess", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailAccess", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailAccess COS", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailStore readCosMailStore(String cosId) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final MailStore ms = new MailStore();
                LDAPUtils.populateMailStore(ms, attrs);
                return ms;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailStore", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailStore", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailStore", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosMailStore", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailReceipt readCosMailReceipt(String cosId) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final MailReceipt mr = new MailReceipt();
                LDAPUtils.populateMailReceipt(mr, attrs);
                return mr;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailReceipt", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailReceipt", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailReceipt", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public GeneralPreferences readCosGeneralPreferences(String cosId)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final GeneralPreferences generalPreferences = new GeneralPreferences();
                LDAPUtils.populateGeneralPreferences(generalPreferences, attrs);
                return generalPreferences;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosGeneralPreferences", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosGeneralPreferences", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosGeneralPreferences", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosPreferences", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public Credentials readCosCredentials(String cosId) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final Credentials credentials = new Credentials();
                LDAPUtils.populateCredentials(credentials, attrs);
                return credentials;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosCredentials", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosCredentials", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosCredentials", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosCredentials", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailSend readCosMailSend(String cosId) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            final Attributes attrs = executeLDAPRetrieve(cosFilter, false);
            if (attrs != null) {
                final MailSend ms = new MailSend();
                LDAPUtils.populateMailSend(ms, attrs);
                return ms;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailSend", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailSend", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailSend", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readForwardingAddresses(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateForwardingAddresses(list, attrs);
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readForwardingAddresses", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readForwardingAddresses", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readForwardingAddresses", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readForwardingAddresses", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAddressesForLocalDelivery(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateAddressesForLocalDelivery(list, attrs);
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readAddressesForLocalDelivery", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readAddressesForLocalDelivery", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readAddressesForLocalDelivery", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readAddressesForLocalDelivery", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAllowedSendersList(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateAllowedSendersList(list, attrs);
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readAllowedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readAllowedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readAllowedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readAllowedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readBlockedSendersList(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateBlockedSendersList(list, attrs);
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readBlockedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readSieveFiltersBlockedSendersList(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), false);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateSieveFiltersBlockedSendersList(list, attrs);
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readSieveFiltersBlockedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readSieveFiltersBlockedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readSieveFiltersBlockedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readSieveFiltersBlockedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public AdminControl readAdminControl(String email) throws MxOSException {
        try {
            final AdminControl adminControl = new AdminControl();
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), true);
            if (attrs != null) {
                LDAPUtils.populateAdminControl(adminControl, attrs);
                return adminControl;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readAdminControl", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readAdminControl", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readAdminControl", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readAdminControl", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAdminBlockedSendersList(String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), true);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateAdminBlockedSendersList(list, attrs);
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readBlockedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAdminApprovedSendersList(final String email)
            throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getFormatFilter(email), true);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateAdminApprovedSendersList(list, attrs);
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readApprovedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readApprovedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readApprovedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readApprovedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public GroupAdminAllocations readGroupAdminAllocations(final String email)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(getFormatFilter(email), true);
            if (attrs != null) {
                final GroupAdminAllocations groupAdminAllocations = new GroupAdminAllocations();
                LDAPUtils.populateGroupAdminAllocations(groupAdminAllocations,
                        attrs);
                return groupAdminAllocations;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readGroupAdminAllocations", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readGroupAdminAllocations", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readGroupAdminAllocations", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readGroupAdminAllocations", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }
}
