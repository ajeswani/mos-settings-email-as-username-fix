/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.ldap.PasswordUtil;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.PasswordStoreType;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to update password.
 * 
 * @author mxos-dev
 */
public class SetPassword implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetPassword.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetPassword action start."));
        }
        String pwdType = null;
        Boolean encPwd = null;
        Boolean pwdTypeFlag = null;
        // Create or Update Mailbox Request
        if (ActionUtils.isCreateMailboxOperation(requestState
                .getOperationName())) {
            // passwordStoreType
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.passwordStoreType.name())) {
                pwdType = requestState.getInputParams()
                        .get(MailboxProperty.passwordStoreType.name()).get(0);
                pwdTypeFlag = true;
            } else {
                pwdType = MxOSConfig.getDefaultPasswordStoreType();
            }
            // if preEncryptedPassword attribute is present
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.preEncryptedPasswordAllowed.name())) {
                encPwd = Boolean
                        .valueOf(requestState
                                .getInputParams()
                                .get(MailboxProperty.preEncryptedPasswordAllowed
                                        .name()).get(0));
                // If passwordStoreType is not bcrypt then only do the following
                if (!pwdType.equalsIgnoreCase(PasswordStoreType.BCRYPT.name())) {
                    if (encPwd) {
                        if (pwdTypeFlag != null
                                && !pwdType
                                        .equalsIgnoreCase(MxOSConfig
                                                .getDefaultPreEncyrptedPasswordStoreType())) {
                            throw new ApplicationException(
                                    MailboxError.MBX_INVALID_PRENCRYPTED_PASSWORD_STORE_TYPE
                                            .name());
                        } else {
                            pwdType = MxOSConfig
                                    .getDefaultPreEncyrptedPasswordStoreType();
                        }
                    }
                }
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.passwordStoreType, pwdType);
        } else {
            // Check for passwordType from the input and if present set
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.passwordStoreType.name())) {
                pwdType = requestState.getInputParams()
                        .get(MailboxProperty.passwordStoreType.name()).get(0);
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.passwordStoreType, pwdType);
            } else {
                // else take the password store type from the ldap account
                ICRUDPool<IMailboxCRUD> provCRUDPool = null;
                IMailboxCRUD mailboxCRUD = null;
                final Credentials credentials;
                try {
                    provCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                    mailboxCRUD = provCRUDPool.borrowObject();
                    String email = requestState.getInputParams()
                            .get(MailboxProperty.email.name()).get(0);
                    credentials = mailboxCRUD.readCredentials(email);
                } catch (final MxOSException e) {
                    throw e;
                } catch (final Exception e) {
                    logger.error("Error while set password.", e);
                    throw new NotFoundException(
                            MailboxError.MBX_NOT_FOUND.name(), e);
                } finally {
                    if (provCRUDPool != null && mailboxCRUD != null) {
                        try {
                            provCRUDPool.returnObject(mailboxCRUD);
                        } catch (final MxOSException e) {
                            throw new ApplicationException(
                                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                        }
                    }
                }

                if (credentials != null) {
                    pwdType = credentials.getPasswordStoreType().toString();
                } else {
                    throw new ApplicationException(
                            MailboxError.MBX_NOT_FOUND.name());
                }
            }
        }
        try {
            String newPassword = requestState.getInputParams()
                    .get(MailboxProperty.password.name()).get(0);
            // if passwordStoreType is bcrypt
            if (pwdType.equalsIgnoreCase(PasswordStoreType.BCRYPT.name())) {

                if (requestState.getInputParams().containsKey(
                        MailboxProperty.preEncryptedPasswordAllowed.name())) {
                    encPwd = Boolean.valueOf(requestState
                            .getInputParams()
                            .get(MailboxProperty.preEncryptedPasswordAllowed
                                    .name()).get(0));

                    if (encPwd != null && !encPwd) {
                        throw new MxOSException(
                                MailboxError.MBX_BCRYPT_ENCRYPTION_NOT_SUPPORTED
                                        .name());
                    }
                }

            } else {
                // check for preEncryptedPassword in request if present bypass
                // encryptPassword
                if (encPwd == null || !encPwd) {
                    newPassword = PasswordUtil.encryptPassword(pwdType,
                            newPassword);
                }
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.password,
                            newPassword);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set password.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_PASSWORD.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetPassword action end."));
        }
    }
}
