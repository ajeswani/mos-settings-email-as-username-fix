/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * mailbox related activities e.g create mailbox, delete mailbox, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class BackendMailboxService implements IMailboxService {

    private static Logger logger = Logger
            .getLogger(BackendMailboxService.class);

    /**
     * Default Constructor.
     */
    public BackendMailboxService() {
        logger.info("BackendMailboxService created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailboxService, Operation.PUT);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MailboxService, Operation.PUT, t0,
                    StatStatus.pass);
            return (Long) mxosRequestState.getDbPojoMap().getPropertyAsObject(
                    MxOSPOJOs.mailboxId);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailboxService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailboxService, Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MailboxService, Operation.DELETE, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailboxService, Operation.DELETE, t0,
                    StatStatus.fail);
            throw e;
        }

    }

    @Override
    public Mailbox read(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailboxService, Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MailboxService, Operation.GET, t0,
                    StatStatus.pass);
            return (Mailbox) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailbox);
        } catch (final MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailboxService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Mailbox> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailboxesService, Operation.LIST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.MailboxesService, Operation.LIST, t0,
                    StatStatus.pass);
            return (List<Mailbox>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailboxes);
        } catch (final MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailboxesService, Operation.LIST, t0,
                    StatStatus.fail);
            throw e;
        }

    }
}
