/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to get mailbox dn.
 *
 * @author mxos-dev
 *
 */
public class GetMailboxDn implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMailboxDn.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("GetMailboxDn action start."));
            }
            ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;

            try {
                mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                final String familyMailboxDn = mailboxCRUD
                        .readMailboxDn(requestState);
                requestState.getDbPojoMap().setProperty(
                        MxOSPOJOs.familyMailboxDn, familyMailboxDn);
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer("mailboxDn : ")
                            .append(requestState.getDbPojoMap().getProperty(
                                    MxOSPOJOs.familyMailboxDn)));
                }
            } catch (final MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error("Error while get mailbox dn.", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_GET_GROUP_MALIBOX_DN.name(),
                        e);
            } finally {
                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    try {
                        mailboxCRUDPool.returnObject(mailboxCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                "Error in finally clause while returing "
                                        + "Provisioning CRUD pool:"
                                        + e.getMessage());
                    }
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("GetMailboxDn action end."));
            }
        }
    }
}
