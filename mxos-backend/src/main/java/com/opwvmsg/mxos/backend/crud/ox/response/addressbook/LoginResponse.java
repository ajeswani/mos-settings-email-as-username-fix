package com.opwvmsg.mxos.backend.crud.ox.response.addressbook;

import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.ox.response.JsonToSessionMapper;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.sun.jersey.api.client.ClientResponse;

public class LoginResponse extends Response {

    public ExternalSession getSession(ClientResponse resp) throws ComponentException {
        JsonNode root = getTree(resp);
        String sessionId = "";
        if (root == null
                || root.path(OXContactsProperty.session.name()).isMissingNode()) {
            return null;
        } else {
            sessionId = root.path(OXContactsProperty.session.name())
                    .getTextValue();
        }

        ExternalSession session = JsonToSessionMapper.mapToSession(resp, sessionId);

        return session;
    }
}
