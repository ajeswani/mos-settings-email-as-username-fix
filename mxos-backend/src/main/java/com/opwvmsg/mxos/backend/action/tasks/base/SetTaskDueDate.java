/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.tasks.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set dueDate attribute of Task Base object.
 * 
 * @author mxos-dev
 * 
 */

public class SetTaskDueDate implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetTaskDueDate.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("action start."));
        }

        final String name = mxosRequestState.getInputParams()
                .get(TasksProperty.dueDate.name()).get(0);
        
        MxOSApp.getInstance()
                .getTasksHelper()
                .setAttribute(mxosRequestState, TasksProperty.dueDate,
                        name);

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("action end."));
        }
    }
}
