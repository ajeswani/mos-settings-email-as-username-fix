/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.domain;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to load all the Domain default properties from
 * default-domain.properties while creating a domain.
 *
 * @author mxos-dev
 */
public class LoadDomainFromProperties implements MxOSBaseAction {
    private static final Logger logger = Logger
            .getLogger(LoadDomainFromProperties.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "LoadDomainFromProperties action start."));
        }
        // Load properties from ${MXOS_HOME}/config/default-domain.properties
        try {
            String home = System.getProperty(MxOSConstants.MXOS_HOME);
            Properties config = null;
            InputStream in = null;
            try {
                in = new FileInputStream(home
                        + "/config/default-domain.properties");
                config = new Properties();
                try {
                    config.load(in);
                } catch (IOException e) {
                    logger.warn("Error while load properties.");
                    throw new MxOSException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            } catch (FileNotFoundException e) {
                logger.warn("properties file not found.");
                throw new MxOSException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            } finally {
                IOUtils.closeQuietly(in);
            }
            // get helper
            IMailboxHelper helper = MxOSApp.getInstance()
                    .getMailboxHelper();
            Iterator<Object> keys = config.keySet().iterator();
            // for each property
            String key;
            while (keys.hasNext()) {
                // set key and property value in helper
                key = (String) keys.next();
                helper.setAttribute(requestState, key, config.getProperty(key));
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while load domain from properties.", e);
            throw new ApplicationException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "LoadDomainFromProperties action end."));
        }
    }
}
