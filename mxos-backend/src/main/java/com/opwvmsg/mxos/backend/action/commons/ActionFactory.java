/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.commons;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action registry which holds all the actions defined for MxOS. It is
 * responsible for calling action one after another for given operation.
 *
 * @author mxos-dev
 *
 */
public class ActionFactory {
    private static Logger logger = Logger.getLogger(ActionFactory.class);

    private static class ActionFactoryHolder {
        public static ActionFactory instance = new ActionFactory();
    }
    
    protected static final HashMap<String, MxOSBaseAction> actionRegistory =
            new HashMap<String, MxOSBaseAction>();

    /**
     * Default constructor.
     */
    private ActionFactory() {
    }

    public static ActionFactory getInstance() {
        return ActionFactoryHolder.instance;
    }

    /**
     * Method to register each action into ActionRegistry.
     * 
     * @param fqClassName name of the action class
     * @throws Exception Exception
     */
    public void register(final String fqClassName) throws Exception {
        try {
            logger.debug("Registering : " + fqClassName);
            final Class<?> kls = Class.forName(fqClassName);
            final MxOSBaseAction action = (MxOSBaseAction) kls.newInstance();
            actionRegistory.put(fqClassName, action);
        } catch (final ClassNotFoundException e) {
            logger.error("Can't create class " + fqClassName, e);
            throw e;
        } catch (final Exception e) {
            logger.error("Can't instantiate class " + fqClassName, e);
            throw e;
        }
    }

    /**
     * Method to execute all the actions.
     * 
     * @param actionIds - queue of actions
     * @param model - instance of model
     * @throws MxOSException MxOSException
     */
    public void exec(final List<String> actionIds,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        logger.debug("Total actions - " + actionIds);
        for (final String actionId : actionIds) {
            logger.debug("action - " + actionId);
            final MxOSBaseAction actionObject = actionRegistory.get(actionId);
            try {
                actionObject.run(mxosRequestState);
            } catch (final MxOSException e) {
                throw e;
            }
        }
    }
}
