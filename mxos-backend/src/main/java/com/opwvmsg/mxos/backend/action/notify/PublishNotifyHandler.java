/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.notify;

import static com.opwvmsg.mxos.data.enums.MxOSConstants.NOTIFY_MULTIMAP_ID;
import static org.apache.http.HttpStatus.SC_GONE;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.http.GenericHttpClient;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Thread to publish the message(JSON) to given subscription/callbackURL via
 * NotifyConnectionPool
 * 
 * @author mxos-dev
 */
class PublishNotifyHandler implements Runnable {
    private static final Logger logger = Logger
            .getLogger(PublishNotifyHandler.class);

    /**
     * Thread pool for Publishing
     */
    private static final ExecutorService THREAD_POOL;

    static {
        /* ThreadFactory for NotifyPublish THREAD_POOL */
        final ThreadFactory tf = new ThreadFactory() {

            final AtomicInteger threadNumber = new AtomicInteger(1);

            @Override
            public Thread newThread(Runnable r) {
                final Thread t = new Thread(r, String.format("NotifyWorker-%s",
                        threadNumber.getAndIncrement()));
                if (t.isDaemon())
                    t.setDaemon(false);
                if (t.getPriority() != Thread.NORM_PRIORITY)
                    t.setPriority(Thread.NORM_PRIORITY);
                return t;
            }

        };

        /* creating thread pool */
        THREAD_POOL = Executors.newFixedThreadPool(
                MxOSConfig.getNotifyThreadPoolMaxSize(), tf);
    }

    /**
     * Removes given topic and subscription entry from data store.
     * 
     * @param aTopic
     * @param aSubscription
     */
    private static void purgeEntry(String aTopic, String aSubscription) {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuilder("Topic=").append(aTopic)
                    .append(" and Subscription=").append(aSubscription)
                    .append(", entry removed from mOS memory data store."));
        }

        MxOSApp.getInstance().getMultiMapDataStore()
                .removeEntry(NOTIFY_MULTIMAP_ID, aTopic, aSubscription);
    }

    /**
     * Puts the publish task into thread pool for execution.
     * 
     * @param task - PublishNotifyHandler
     */
    public static void publish(final PublishNotifyHandler task) {
        if (logger.isDebugEnabled()) {
            logger.debug(task + " added to thread pool.");
        }
        THREAD_POOL.execute(task);
    }

    /**************************************************************************/

    /* instance variables */
    private final String topic;
    private final String callBackURL;
    private final String message;

    /**
     * Constructor
     * 
     * @param topic
     * @param aCallBackURL - subscription to publish
     * @param params - variables for HttpPost body
     */
    public PublishNotifyHandler(final String notifyTopic,
            final String aCallBackURL, final String notifyMessage) {
        callBackURL = aCallBackURL;
        message = notifyMessage;
        topic = notifyTopic;
    }

    @Override
    public void run() {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Notify STARTED for Topic=")
                    .append(topic).append(" and callBackURL=")
                    .append(callBackURL).append(" with Message=")
                    .append(message));
        }

        /* verifying if callback ulr is valid */
        URI uri;
        try {
            uri = new URI(callBackURL);
        } catch (URISyntaxException e1) {
            logger.error(new StringBuilder(
                    "Notify INVALID Callback URL for topic=").append(topic)
                    .append(" and callBackURL=").append(callBackURL));
            purgeEntry(topic, callBackURL);
            return;
        }

        /* getting connection obj and POSTing request */
        final ICRUDPool<GenericHttpClient> pool = MxOSApp.getInstance()
                .getNotifyConnectionPool();
        if (pool == null) {
            logger.error("NotifyConnectionPool diabled!", new MxOSException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Inactive NotifyConnectionPool!"));
            return;
        }

        GenericHttpClient nClient = null;
        try {
            nClient = pool.borrowObject();

            final HttpResponse response = nClient.executePost(uri, message,
                    ContentType.APPLICATION_JSON);
            final int sc = response.getStatusLine().getStatusCode();

            /* handling the response */
            final String log = "Notify ENDED - ResponseStatusCode=%s received for topic=%s and subscription=%s";
            if (sc == SC_GONE) {
                if (logger.isEnabledFor(Level.WARN)) {
                    logger.warn(String.format(log, sc, topic, callBackURL));
                }
                /* remove entry from data store */
                purgeEntry(topic, callBackURL);
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format(log, sc, topic, callBackURL));
                }
            }
        } catch (ClientProtocolException e) {
            logger.warn("ClientProtocolException while awaiting HttpResponse.",
                    e);
            ConnectionErrorStats.NOTIFY.increment();
        } catch (SocketTimeoutException e) {
            logger.warn("No data received while awaiting HttpResponse.", e);
            ConnectionErrorStats.NOTIFY.increment();
        } catch (ConnectTimeoutException e) {
            logger.warn("ConnectTimeoutException while awaiting HttpResponse.",
                    e);
            purgeEntry(topic, callBackURL);
            ConnectionErrorStats.NOTIFY.increment();
        } catch (org.apache.commons.httpclient.ConnectTimeoutException e) {
            logger.warn("ConnectTimeoutException while awaiting HttpResponse.",
                    e);
            purgeEntry(topic, callBackURL);
            ConnectionErrorStats.NOTIFY.increment();
        } catch (IOException e) {
            logger.warn("IOException while awaiting HttpResponse.", e);
            ConnectionErrorStats.NOTIFY.increment();
        } catch (Exception e) {
            logger.warn("Exception while awaiting HttpResponse.", e);
            ConnectionErrorStats.NOTIFY.increment();
        } finally {
            if (nClient != null) {
                try {
                    pool.returnObject(nClient);
                } catch (MxOSException e) {
                    logger.error(
                            "Could not return NotifyHttpClient back to pool.",
                            e);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "PublishNotifyHandler: Topic=".concat(topic)
                .concat(", Subscription=").concat(callBackURL);
    }

}
