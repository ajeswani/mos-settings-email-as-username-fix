/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.utils.paf.intermail.mail.AccessInfo;

/**
 * Action class to get Credentials object.
 *
 * @author mxos-dev
 *
 */
public class GetCredentials implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetCredentials.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetCredentials action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;

        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            final Credentials credentials = mailboxCRUD.readCredentials(email);

            metaCRUD.readMailBoxAccessInfo(requestState);

            AccessInfo accessInfo = (AccessInfo) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.accessInfo);

            credentials.setFailedLoginAttempts((int) accessInfo
                    .getNumFailedLoginAttempts());
            credentials.setLastFailedLoginDate(new SimpleDateFormat(System
                    .getProperty(SystemProperty.userDateFormat.name()))
                    .format(new Date(accessInfo.getLastFailedLoginTime())));
            credentials.setLastSuccessfulLoginDate(new SimpleDateFormat(System
                    .getProperty(SystemProperty.userDateFormat.name()))
                    .format(new Date(accessInfo.getLastSuccessfulLoginTime())));
            credentials.setLastLoginAttemptDate(new SimpleDateFormat(System
                    .getProperty(SystemProperty.userDateFormat.name()))
                    .format(new Date(accessInfo.getLastLoginTime())));

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.credentials,
                    credentials);
            mailboxCRUD.commit();
            metaCRUD.commit();
        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get credentails.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    MailboxError.MBX_CREDENTIALS_UNABLE_TO_GET.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                            + "Provisioning CRUD pool:" + e.getMessage());
                }
            }
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Meta CRUD pool:" + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetCredentials action end."));
        }
    }
}
