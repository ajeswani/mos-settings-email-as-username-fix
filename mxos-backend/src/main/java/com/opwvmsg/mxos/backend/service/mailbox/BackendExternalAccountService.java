/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalAccountsService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Mailbox external account operations interface which will be exposed to the
 * client. This interface is responsible for doing mailbox external account
 * related operations (like Read, Update etc.) directly in the database.
 * 
 * @author mxos-dev
 */
public class BackendExternalAccountService implements IExternalAccountsService {

    private static Logger logger = Logger
            .getLogger(BackendExternalAccountService.class);

    /**
     * Default Constructor.
     */
    public BackendExternalAccountService() {
        logger.info("BackendExternalAccountService created...");
    }

    @Override
    public ExternalAccounts read(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ExternalAccountService,
                    Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ExternalAccountService, Operation.GET,
                    t0, StatStatus.pass);
            return (ExternalAccounts) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.externalAccounts);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ExternalAccountService, Operation.GET,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ExternalAccountService,
                    Operation.POST);
            if (inputParams == null || inputParams.size() < 1) {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ExternalAccountService, Operation.POST,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ExternalAccountService, Operation.POST,
                    t0, StatStatus.fail);
            throw e;
        }

    }
}
