/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesAddressBookService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Mailbox Web Mail Features operations interface which will be exposed to the
 * client. This interface is responsible for doing WebMailFeatures related
 * operations (like Read, Update etc.) directly in the database.
 * 
 * @author mxos-dev
 */
public class BackendWebMailFeaturesAddressBookService implements
        IWebMailFeaturesAddressBookService {

  private static Logger logger = Logger
            .getLogger(BackendWebMailFeaturesAddressBookService.class);

    /**
     * Default Constructor.
     */
    public BackendWebMailFeaturesAddressBookService() {
        logger.info("BackendWebMailFeaturesAddressBookService created...");
    }

    @Override
    public AddressBookFeatures read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.WebMailFeaturesAddressBookService,
                    Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.WebMailFeaturesAddressBookService,
                    Operation.GET, t0, StatStatus.pass);

            WebMailFeatures wmf = (WebMailFeatures) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.webMailFeatures);

            return (AddressBookFeatures) wmf.getAddressBookFeatures();
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.WebMailFeaturesAddressBookService,
                    Operation.GET, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.WebMailFeaturesAddressBookService,
                    Operation.POST);
            if (inputParams == null || inputParams.size() <= 1) {
                ExceptionUtils.createInvalidRequestException(mxosRequestState);
            }
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.WebMailFeaturesAddressBookService, Operation.POST,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.WebMailFeaturesAddressBookService, Operation.POST,
                    t0, StatStatus.fail);
            throw e;
        }
    }
}
