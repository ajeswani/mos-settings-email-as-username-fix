/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.notify;

import static com.opwvmsg.mxos.data.enums.NotificationProperty.subscription;
import static com.opwvmsg.mxos.data.enums.NotificationProperty.topic;
import static com.opwvmsg.mxos.error.NotifyError.NTF_INVALID_SUBSCRIPTION;
import static com.opwvmsg.mxos.data.enums.MxOSConstants.NOTIFY_MULTIMAP_ID;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;

/**
 * Action to create notify subscriptions
 * 
 * @author
 */
public class PutSubscriptions implements MxOSBaseAction {

    private static Logger logger = Logger.getLogger(PutSubscriptions.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("PutSubscriptions action start.");
        }

        final String aTopic = requestState.getInputParams().get(topic.name())
                .get(0);
        final String sub = requestState.getInputParams()
                .get(subscription.name()).get(0);

        if (sub.isEmpty()) {
            /* no subscriptions provided for given topic */
            if (logger.isDebugEnabled()) {
                logger.debug("No subscriptions provided for topic " +aTopic);
            }
            throw new InvalidRequestException(NTF_INVALID_SUBSCRIPTION.name());
        }

        final IDataStoreMultiMap<String, String> mMap = MxOSApp.getInstance()
                .getMultiMapDataStore();
        mMap.putEntry(NOTIFY_MULTIMAP_ID, aTopic, sub);

        if (logger.isDebugEnabled()) {
            logger.debug("PutSubscriptions action end.");
        }
    }

}
