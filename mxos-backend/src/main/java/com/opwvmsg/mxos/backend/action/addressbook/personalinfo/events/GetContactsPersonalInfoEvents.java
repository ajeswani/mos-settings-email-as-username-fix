/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.personalinfo.events;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get Contact personalInfo events object.
 * 
 * @author mxos-dev
 * 
 */
public class GetContactsPersonalInfoEvents implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(GetContactsPersonalInfoEvents.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "GetContactsPersonalInfoEvents action start."));
        }
        try {
            List<Event> event = (List<Event>) requestState.getDbPojoMap()
                    .getPropertyAsObject(
                            MxOSPOJOs.contactsPersonalInfoAllEvents);

            String type = requestState.getInputParams()
                    .get(AddressBookProperty.type.name()).get(0);

            for (Event eventElement : event) {
                if (eventElement.getType().name().equalsIgnoreCase(type)) {
                    requestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.contactsPersonalInfoEvents, eventElement);
                }
            }
        } catch (final Exception e) {
            logger.error("Error while get personalInfo events.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "GetContactsPersonalInfoEvents action end."));
        }
    }
}
