/*
p * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.addressbook.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set contacts other phone attribute of Contact PersonalInfo object.
 * 
 * @author mxos-dev
 * 
 */

public class SetContactsOtherPhone implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetContactsOtherPhone.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetContactsOtherPhone action start."));
        }
        if (mxosRequestState.getInputParams().get(
                AddressBookProperty.otherPhone.name()) != null) {
            if (mxosRequestState.getInputParams().containsKey(
                    AddressBookProperty.contactsList.name())) {
                Object[] otherPhone = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.otherPhone.name())
                        .toArray();

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttributes(mxosRequestState,
                                AddressBookProperty.otherPhone, otherPhone);
            } else {
                String otherPhone = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.otherPhone.name())
                        .get(0);
                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.otherPhone, otherPhone);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetContactsOtherPhone action end."));
        }
    }
}
