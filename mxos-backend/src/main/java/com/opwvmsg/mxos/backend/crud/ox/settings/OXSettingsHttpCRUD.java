package com.opwvmsg.mxos.backend.crud.ox.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.exception.LoginUserException;
import com.opwvmsg.mxos.backend.crud.exception.SettingsException;
import com.opwvmsg.mxos.backend.crud.ox.OXAbstractHttpCRUD;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.LoginResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.UserResponse;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.sun.jersey.api.client.ClientResponse;

public class OXSettingsHttpCRUD extends OXAbstractHttpCRUD implements
        ISettingsCRUD {

    private static Logger logger = Logger.getLogger(OXSettingsHttpCRUD.class);
    private static final String DATA_STRING = "data";
    private static final String NULL_STRING = "null";
    private static final String MISC_STRING = "misc";
    private static final String ABOVE_STRING = "above";
    private static final String BELOW_STRING = "below";
    private static final String INSERTION_STRING = "insertion";
    private static final String MAIL_MODULE = "io.ox/mail";
    private static final String SIGNATURE_STRING = "signature";
    private static final String CONTENT_STRING = "content";
    private static final String DISPLAYNAME_STRING = "displayname";
    private static final String MODULE_STRING = "module";
    private static final String TYPE_STRING = "type";
    private static final String SNIPPET_STRING = "snippet";
    private static final String ACTION_STRING = "action";
    private static final String NEW_STRING = "new";
    private static final String UPDATE_STRING = "update";
    private static final String DELETE_STRING = "delete";
    private static final String ALL_STRING = "all";
    private static final String OX_HTTP_URL_STRING = "oxHttpURL";
    private static final String PASSWORD_STRING = "test";

    /**
     * Constructor.
     * 
     * @param baseURL - base url of mxos
     */
    public OXSettingsHttpCRUD(String baseURL) {
        webResource = getWebResource(baseURL);
    }

    @Override
    public void commit() throws ApplicationException {
        // do nothing
    }

    @Override
    public void createSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Map<String, List<String>> params = mxosRequestState
                    .getInputParams();
            String email = mxosRequestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            //String userName = ActionUtils.getArrayFromEmail(email)[0];
            String userNameAsEmail = System
                    .getProperty(SystemProperty.storeUserNameAsEmail.name());
            final String[] token = email.split(MxOSConstants.AT_THE_RATE);
            final String userName = Boolean.valueOf(userNameAsEmail) ? email
                    : token[0];          
            Signature signature = (Signature) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder(" Signature : ")
                        .append(signature));
            }
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());

                ExternalSession session = loginOX(mxosRequestState);

                if (session != null) {
                    // Next, add the signature
                    if (signature != null) {
                        String signatureId = createSignature(mxosRequestState,
                                signature, session);
                        mxosRequestState.getDbPojoMap().setProperty(
                                MxOSPOJOs.signatureId, signatureId);
                    }
                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_CONNECTION_ERROR.name());
                }
            }
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        }
    }

    private String createSignature(MxOSRequestState mxosRequestState,
            Signature signature, ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(NEW_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));

        JSONObject json = new JSONObject();

        try {
            json.put(TYPE_STRING, SIGNATURE_STRING);
            json.put(MODULE_STRING, MAIL_MODULE);
            json.put(DISPLAYNAME_STRING, signature.getSignatureName());
            json.put(CONTENT_STRING, signature.getSignature());
            if (signature.getAddSignatureInReplyType() != null) {
                JSONObject jsonChild = new JSONObject();
                switch (signature.getAddSignatureInReplyType()) {
                case BEFORE_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, ABOVE_STRING);
                    break;
                case AFTER_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, BELOW_STRING);
                    break;
                default:
                }
                json.put(MISC_STRING, jsonChild);
            }

            final ClientResponse response = put(SNIPPET_STRING, paramsNew,
                    json.toString());
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                return object.getString(DATA_STRING);
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name());
        }
    }

    private String updateSignature(MxOSRequestState mxosRequestState,
            Signature signature, ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(UPDATE_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));
        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(signature.getSignatureId().toString());
        paramsNew.put(OXContactsProperty.id.name(), paramList3);

        JSONObject json = new JSONObject();
        try {

            json.put(TYPE_STRING, SIGNATURE_STRING);
            json.put(MODULE_STRING, MAIL_MODULE);
            json.put(DISPLAYNAME_STRING, signature.getSignatureName());
            json.put(CONTENT_STRING, signature.getSignature());
            if (signature.getAddSignatureInReplyType() != null) {
                JSONObject jsonChild = new JSONObject();
                switch (signature.getAddSignatureInReplyType()) {
                case BEFORE_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, ABOVE_STRING);
                    break;
                case AFTER_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, BELOW_STRING);
                    break;
                default:
                }
                json.put(MISC_STRING, jsonChild);
            }

            final ClientResponse response = put(SNIPPET_STRING, paramsNew,
                    json.toString());
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                return object.getString(DATA_STRING);
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name());
        }
    }

    private String deleteSignature(MxOSRequestState mxosRequestState,
            Signature signature, ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(DELETE_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));

        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        try {
            json.put(OXContactsProperty.id.name(),
                    jsonArray.put(signature.getSignatureId()));

            final ClientResponse response = put(SNIPPET_STRING, paramsNew,
                    jsonArray.toString());
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                return object.getString(DATA_STRING);
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name());
        }
    }

    private List<Signature> listSignature(MxOSRequestState mxosRequestState,
            ExternalSession session) throws MxOSException {

        List<Signature> signatures = new ArrayList<Signature>();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(ALL_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));

        try {
            final ClientResponse response = get(SNIPPET_STRING, paramsNew);
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                JSONArray objArray = (JSONArray) object.get(DATA_STRING);
                for (int i = 0; i < objArray.length(); i++) {
                    Signature signature = new Signature();
                    object = objArray.getJSONObject(i);
                    if (!object.isNull("id")) {
                        if (!object.getString("id").equals(NULL_STRING)) {
                            signature.setSignatureId(object.getInt("id"));
                        }
                    }
                    if (!object.isNull("displayname")) {
                        signature.setSignatureName(object
                                .getString("displayname"));
                    }
                    if (!object.isNull("content")) {
                        signature.setSignature(object.getString("content"));
                    }
                    if (!object.isNull(MISC_STRING)) {
                        JSONObject miscObj = (JSONObject) object.get("misc");
                        if (!miscObj.isNull(INSERTION_STRING)) {
                            if (miscObj.getString(INSERTION_STRING)
                                    .equalsIgnoreCase(BELOW_STRING)) {
                                signature
                                        .setAddSignatureInReplyType(SignatureInReplyType.AFTER_ORIGINAL_MESSAGE);
                            } else {
                                signature
                                        .setAddSignatureInReplyType(SignatureInReplyType.BEFORE_ORIGINAL_MESSAGE);
                            }
                        }
                    }
                    signatures.add(signature);
                }
                return signatures;
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name());
        }
    }

    @Override
    public void deleteSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Map<String, List<String>> params = mxosRequestState
                    .getInputParams();
            String email = mxosRequestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            //String userName = ActionUtils.getArrayFromEmail(email)[0];
            String userNameAsEmail = System
                    .getProperty(SystemProperty.storeUserNameAsEmail.name());
            final String[] token = email.split(MxOSConstants.AT_THE_RATE);
            final String userName = Boolean.valueOf(userNameAsEmail) ? email
                    : token[0];
          
            Signature signature = (Signature) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder(" Signature : ")
                        .append(signature));
            }
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());
                ExternalSession session = loginOX(mxosRequestState);

                if (session != null) {
                    // Next, list the signatures
                    if (signature != null) {
                        deleteSignature(mxosRequestState, signature, session);
                    }
                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_CONNECTION_ERROR.name());
                }
            }
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void rollback() throws ApplicationException {
        // do nothing
    }

    @Override
    public void updateSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Map<String, List<String>> params = mxosRequestState
                    .getInputParams();
            String email = mxosRequestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            //String userName = ActionUtils.getArrayFromEmail(email)[0];
            String userNameAsEmail = System
                    .getProperty(SystemProperty.storeUserNameAsEmail.name());
            final String[] token = email.split(MxOSConstants.AT_THE_RATE);
            final String userName = Boolean.valueOf(userNameAsEmail) ? email
                    : token[0];
          
            Signature signature = (Signature) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder(" Signature : ")
                        .append(signature));
            }
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());

                ExternalSession session = loginOX(mxosRequestState);

                if (session != null) {
                    // Next, add the signature
                    if (signature != null) {
                        updateSignature(mxosRequestState, signature, session);

                    }
                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_CONNECTION_ERROR.name());
                }
            }
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public String readSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Map<String, List<String>> params = mxosRequestState
                    .getInputParams();
            String email = mxosRequestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            //String userName = ActionUtils.getArrayFromEmail(email)[0];
            String userNameAsEmail = System
                    .getProperty(SystemProperty.storeUserNameAsEmail.name());
            final String[] token = email.split(MxOSConstants.AT_THE_RATE);
            final String userName = Boolean.valueOf(userNameAsEmail) ? email
                    : token[0];
          
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());
                ExternalSession session = loginOX(mxosRequestState);

                if (session != null) {
                    // Next, list the signatures
                    List<Signature> signatures = listSignature(
                            mxosRequestState, session);
                    mxosRequestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.allSignatures, signatures);
                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_CONNECTION_ERROR.name());
                }
            }
            return null;
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_CONNECTION_ERROR.name(), e);
        }
    }

    private ClientResponse get(final String subURL,
            final Map<String, List<String>> params) throws MxOSException,
            SettingsException {
        try {
            return getOX(subURL, params);
        } catch (ComponentException e) {
            throw (SettingsException) e;
        }
    }
    
    private ClientResponse put(final String subURL,
            final Map<String, List<String>> params, final String jsonString)
            throws MxOSException, SettingsException {
        try {
            return putOX(subURL, params, jsonString);
        } catch (ComponentException e) {
            throw (SettingsException) e;
        }
    }

    @Override
    protected ExternalSession getSessionFromResponse(final ClientResponse r)
            throws LoginUserException {
        try {
            return new LoginResponse().getSession(r);
        } catch (ComponentException e) {
            throw new LoginUserException(e);
        }
    }

    @Override
    protected String getUserIdFromResponse(ClientResponse resp)
            throws LoginUserException {
        try {
            return new UserResponse().getUserId(resp);
        } catch (ComponentException e) {
            throw new LoginUserException(e);
        }
    }
}
