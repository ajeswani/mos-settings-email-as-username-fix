/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaConnectionPool.java#2 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;

import javax.naming.directory.DirContext;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Implementation of LDAP CRUD connection pool.
 * 
 * @author mxos-dev
 */
public class LdapSearchConnectionPool extends LdapConnectionPool {
    public LdapSearchConnectionPool() throws MxOSException {
        super();
    }

    protected static Logger logger = Logger
            .getLogger(LdapSearchConnectionPool.class);
    private LdapSearchCRUDFactory ldapSearchCRUDFactory;

    /**
     * Method to load Pool.
     */
    @Override
    protected void loadPool(Hashtable<String, String> env, int maxSize) {
        if (logger.isDebugEnabled()) {
            logger.debug("creating search connection pool having maxsize :"
                    + maxSize);
        }
        ldapSearchCRUDFactory = new LdapSearchCRUDFactory();
        ldapSearchCRUDFactory.setEnv(env);
        objPool = new GenericObjectPool<LdapMailboxCRUD>(ldapSearchCRUDFactory,
                maxSize);
        objPool.setMaxIdle(-1);
        objPool.setTestOnBorrow(true);
        logger.debug("LdapSearchConnectionPool:loadPool Exited");
    }


    @Override
    public synchronized IMailboxCRUD borrowObject() throws MxOSException {
        IMailboxCRUD obj = null;
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        for (int i = 0; i < numHosts; i++) {
            try {
                logger.info("# Trying Host" + i + " : "
                        + env.get(DirContext.PROVIDER_URL));
                obj = objPool.borrowObject();
                logger.info("# Borrow object success on Host : " + i + " : "
                        + env.get(DirContext.PROVIDER_URL));
            } catch (Exception e) {
                logger.warn("# Error in borrowing object..");
                getLdapUrl();
                ldapSearchCRUDFactory.setEnv(env);
            }
            if (obj != null) {
                break;
            }
            try {
                Thread.sleep(MxOSConfig.getLdapConnRetryInterval());
            } catch (InterruptedException e) {
                logger.warn("# Thread Interrupted.." + e.getMessage());
                throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                        "Thread Interrupted.." + e.getMessage());
            }
        }
        if (obj == null) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down. try again");
        }
        incrementJMXStats();
        return obj;
    }

    @Override
    protected void loadLdapHosts() {
        String ldap = "ldap://";
        hosts = MxOSConfig.getLdapSearchServers();
        // Consider Master LDAP server to use for Search APIs
        // if there is no
        if (hosts == null || hosts.length == 0) {
            logger.warn("Error Loading SearchConnectionPool...");
            logger.warn("Loading Master LDAP for SearchConnectionPool...");
            hosts = MxOSConfig.getLdapServers();
        }

        if (numHosts == 0) {
            numHosts = hosts.length;
        }

        urls = new String[hosts.length];
        for (int i = 0; i < hosts.length; i++) {
            if (!hosts[i].startsWith(ldap)) {
                urls[i] = ldap + hosts[i];
            } else {
                urls[i] = hosts[i];
            }
        }
    }

    @Override
    protected void initializeJMXStats(long count) {
        ConnectionStats.LDAPSEARCH.setCount(count);
    }

    @Override
    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_LDAPSEARCH.increment();
    }

    @Override
    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_LDAPSEARCH.decrement();
    }

}
