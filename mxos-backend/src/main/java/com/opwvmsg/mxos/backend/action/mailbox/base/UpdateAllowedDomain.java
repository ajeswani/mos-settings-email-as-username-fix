/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.domain.DomainProvisionHelper;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to update Allowed Domain.
 *
 * @author mxos-dev
 */
public class UpdateAllowedDomain implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateAllowedDomain.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateAllowedDomain action start."));
        }
        try {
            final String oldAllowedDomain = requestState.getInputParams()
                    .get(MailboxProperty.oldAllowedDomain.name()).get(0);
            final String newAllowedDomain = requestState.getInputParams()
                    .get(MailboxProperty.newAllowedDomain.name()).get(0);
            final List<String> allowedDomainsList;
    
            ICRUDPool<IMailboxCRUD> provCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;
    
            final Base base;
            try {
                provCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = provCRUDPool.borrowObject();
                base = ActionUtils.readMailboxProvisioning(requestState,
                        mailboxCRUD);
            } catch (final MxOSException e) {
                throw new InvalidRequestException(
                        ErrorCode.GEN_INTERNAL_ERROR.name());
            } catch (final Exception e) {
                logger.error("Error while update allowed domain.", e);
                throw new InvalidRequestException(
                        ErrorCode.GEN_INTERNAL_ERROR.name());
            } finally {
                if (provCRUDPool != null && mailboxCRUD != null) {
                    try {
                        provCRUDPool.returnObject(mailboxCRUD);
                    } catch (final MxOSException e) {
                        throw new InvalidRequestException(
                                ErrorCode.GEN_INTERNAL_ERROR.name());
                    }
                }
            }
            if (base == null) {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }    
            if (base.getAllowedDomains() != null) {
                allowedDomainsList = base.getAllowedDomains();
            } else {
                allowedDomainsList = new ArrayList<String>();
            }
            if (allowedDomainsList == null || allowedDomainsList.size() == 0) {
                throw new InvalidRequestException(
                        MailboxError.MBX_OLD_ALLOWED_DOMAIN_NOT_FOUND.name());
            }
            if (!allowedDomainsList.contains(oldAllowedDomain.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError.MBX_OLD_ALLOWED_DOMAIN_NOT_FOUND.name());
            }
            if (allowedDomainsList.contains(newAllowedDomain.toLowerCase())) {
                // Even though newAllowedDomain already exist..we are updating
                // the value with newAllowedDomain to avoid the error thrown 
                // in UpdateMailbox, if we return from here.
            }
            try {
                // Check for newDomain exists or not.
                DomainProvisionHelper.read(mailboxCRUD, newAllowedDomain);
            } catch (MxOSException e) {
                throw new NotFoundException(
                        MailboxError.MBX_INVALID_NEW_ALLOWED_DOMAIN.name());
            }
            if (allowedDomainsList.remove(oldAllowedDomain.toLowerCase())) {
                allowedDomainsList.add(newAllowedDomain.toLowerCase());
                final String[] allowedDomainsArray = allowedDomainsList
                        .toArray(new String[allowedDomainsList.size()]);

                MxOSApp.getInstance().getMailboxHelper()
                        .setAttribute(requestState, MailboxProperty.allowedDomain,
                                allowedDomainsArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_ALLOWEDDOMAINS_UNABLE_TO_POST.name());
            }
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while update allowed domain.", e);
            throw new ApplicationException(
                    MailboxError.MBX_ALLOWEDDOMAINS_UNABLE_TO_POST.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateAllowedDomain action end."));
        }
    }
}
