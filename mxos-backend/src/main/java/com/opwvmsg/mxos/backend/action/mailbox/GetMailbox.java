/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;

/**
 * Action class to get Mailbox object.
 * 
 * @author mxos-dev
 * 
 */
public class GetMailbox implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMailbox.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailbox action start."));
        }
        ActionUtils.getMailbox(requestState);

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailbox action end."));
        }
    }
}
