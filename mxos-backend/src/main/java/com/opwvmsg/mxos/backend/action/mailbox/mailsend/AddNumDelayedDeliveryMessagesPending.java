/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to add delayed delivery messages pending.
 * 
 * @author mxos-dev
 */
public class AddNumDelayedDeliveryMessagesPending implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(AddNumDelayedDeliveryMessagesPending.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddNumDelayedDeliveryMessagesPending action start."));
        }

        List<String> newNumDelayedDeliveryMessagesPendingList = requestState
                .getInputParams().get(
                        MailboxProperty.numDelayedDeliveryMessagesPending
                                .name());

        try {

            List<String> numDelayedDeliveryMessagesPendingList = new ArrayList<String>();
            final MailSend ms = (MailSend) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailSend);

            if (ms != null) {
                if (ms.getNumDelayedDeliveryMessagesPending() != null) {
                    numDelayedDeliveryMessagesPendingList = ms
                            .getNumDelayedDeliveryMessagesPending();
                } else {
                    numDelayedDeliveryMessagesPendingList = new ArrayList<String>();
                }
            }

            if (newNumDelayedDeliveryMessagesPendingList != null) {
                for (String deliveryMessagesPending : newNumDelayedDeliveryMessagesPendingList) {
                    // add to list if already not exists
                    if (!numDelayedDeliveryMessagesPendingList
                            .contains(deliveryMessagesPending.toLowerCase())) {
                        numDelayedDeliveryMessagesPendingList
                                .add(deliveryMessagesPending);
                    }
                }
            }

            String[] delayedDeliveryMessagesPendingArray = numDelayedDeliveryMessagesPendingList
                    .toArray(new String[numDelayedDeliveryMessagesPendingList
                            .size()]);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.numDelayedDeliveryMessagesPending,
                            delayedDeliveryMessagesPendingArray);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error(
                    "Error while adding num delayed delivery messages pending.",
                    e);
            throw new ApplicationException(
                    MailboxError.MBX_DELAYED_DELIVERY_MESSAGES_PENDING_UNABLE_TO_CREATE
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddNumDelayedDeliveryMessagesPending action end."));
        }
    }
}
