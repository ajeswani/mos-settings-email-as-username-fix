/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ox.addressbook;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

/**
 * This class provides connection pool for HTTP REST.
 * 
 * @author
 */
public class OXAddressBookConnectionPool implements ICRUDPool<IAddressBookCRUD> {
    private static class RestConnectionPoolHolder {
        public static OXAddressBookConnectionPool instance = new OXAddressBookConnectionPool();
    }

    private static Logger logger = Logger
            .getLogger(OXAddressBookConnectionPool.class);

    public static final String MXOS_BASE_URL = "mxosBaseUrl";
    public static final String MXOS_CUSTOM = "custom";

    /**
     * Method to get Instance of RestConnectionPool object.
     * 
     * @return RestConnectionPool object
     * @throws Exception Exception
     */
    public static OXAddressBookConnectionPool getInstance() {
        return RestConnectionPoolHolder.instance;
    }

    private GenericObjectPool<OXAddressBookCRUD> objPool;

    /**
     * Constructor.
     * 
     * Configure mxos-host in hosts file.
     * 
     * @throws Exception Exception.
     */
    public OXAddressBookConnectionPool() {
        createMxosObjectPool();
    }

    /**
     * Method to borrow connection.
     * 
     * @return connection connection
     * @throws Exception in case no connection is available.
     */
    @Override
    public OXAddressBookCRUD borrowObject() throws MxOSException {
        OXAddressBookCRUD obj = null;
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    /**
     * Create mxosObjectPool to perform CRUD operation using REST.
     * 
     * @throws Exception Exception
     */
    private void createMxosObjectPool() {
        try {
            String baseUrl = null;
            if (System.getProperties().containsKey("oxHttpURL")) {
                baseUrl = System.getProperty("oxHttpURL");
            } else {
                // Default base url
                // to support this we should set mxos-host in /etc/hosts file.
                baseUrl = "http://localhost/ajax";
            }
            logger.info("MxOS URL = " + baseUrl);
            int mxosMaxConnections;
            if (System.getProperties().containsKey(
                    SystemProperty.mxosMaxConnections.name())) {
                mxosMaxConnections = Integer.parseInt(System
                        .getProperty(SystemProperty.mxosMaxConnections.name()));
            } else {
                // Default max connections
                mxosMaxConnections = 10;
            }
            logger.info("MxOS Max Connections = " + mxosMaxConnections);
            objPool = new GenericObjectPool<OXAddressBookCRUD>(
                    new OXAddressBookFactory(baseUrl), mxosMaxConnections);
            objPool.setMaxIdle(-1);
            logger.info("RESTMxosObjectPool Created with default values...");
            initializeJMXStats(mxosMaxConnections);
        } catch (Exception e) {
            logger.error("Problem occured while creating RESTMxosObjectPool with default values...", e);
            // exception is thrown only during borrow or return case
        }
    }

    /**
     * Method to return the connection back.
     * 
     * @param restCRUD restCRUD
     */

    @Override
    public void returnObject(IAddressBookCRUD addressBookCRUD)
            throws MxOSException {
        logger.info("# Active Connections in Pool : " + objPool.getNumActive());
        logger.info("# Idle Connections in Pool : " + objPool.getNumIdle());
        try {
            objPool.returnObject((OXAddressBookCRUD) addressBookCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while returning object.", e);
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.OXADDRESSBOOK.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_OXADDRESSBOOK.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_OXADDRESSBOOK.decrement();
    }

    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub

    }
}
