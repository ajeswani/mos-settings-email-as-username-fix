/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set signature for mail send.
 * 
 * @author mxos-dev
 */
public class SetSignature implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetSignature.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSignature action start."));
        }

        String signature = "";
        if (requestState.getInputParams().get(MailboxProperty.signature.name()) != null) {

            signature = requestState.getInputParams()
                    .get(MailboxProperty.signature.name()).get(0);

            String appSuiteIntegrated = System
                    .getProperty(SystemProperty.appSuiteIntegrated.name());
            if (appSuiteIntegrated != null
                    && appSuiteIntegrated.trim().equalsIgnoreCase(
                            MxOSConstants.TRUE)) {
                if (MxOSConfig.getAddressBookBackend().equals(
                        AddressBookDBTypes.ox.name())) {
                    Signature newSignature = (Signature)requestState.getDbPojoMap().getPropertyAsObject(MxOSPOJOs.signature);
                    if(newSignature == null)
                     newSignature = new Signature();

                    newSignature.setSignature(signature);

                    requestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.signature, newSignature);
                }
            } else {

                try {
                    MxOSApp.getInstance()
                            .getMetaHelper()
                            .setAttribute(requestState,
                                    MailboxProperty.signature, signature);
                } catch (final MxOSException e) {
                    throw e;
                } catch (final Exception e) {
                    logger.error("Error while set signature.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), e);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSignature action end."));
        }
    }
}
