/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Belgacom-mxos/bgc-mxos-lib/src/main/java/com/openwave/mxos/data/LoggingActionType.java#1 $
 */
package com.opwvmsg.mxos.backend.action.logging;

import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 *
 * @author Aricent
 *
 */
public enum LoggingActionType {

    Status("status"),
    Pref("pref"),
    Password("password"),
    Forward("forward"),
    AddAlias("addalias"),
    UpdateAlias("udpatealias"),
    DeleteAlias("deletealias"),
    OutOfOfficeSetReply("outofoffice"),
    CreateMailbox("createmailbox");

    private static final Map<String, LoggingActionType>
    BGC_LOGGING_ACTIONS_MAP = new HashMap<String, LoggingActionType>();

    private String action;

    static {
        BGC_LOGGING_ACTIONS_MAP.put(Status.action, Status);
        BGC_LOGGING_ACTIONS_MAP.put(Pref.action, Pref);
        BGC_LOGGING_ACTIONS_MAP.put(Password.action, Password);
        BGC_LOGGING_ACTIONS_MAP.put(Forward.action, Forward);
        BGC_LOGGING_ACTIONS_MAP.put(AddAlias.action, AddAlias);
        BGC_LOGGING_ACTIONS_MAP.put(UpdateAlias.action, UpdateAlias);
        BGC_LOGGING_ACTIONS_MAP.put(DeleteAlias.action, DeleteAlias);
        BGC_LOGGING_ACTIONS_MAP.put(OutOfOfficeSetReply.action,
                OutOfOfficeSetReply);
        BGC_LOGGING_ACTIONS_MAP.put(CreateMailbox.action,
                CreateMailbox);
    }

    /**
     *
     * @param action
     *            action
     */
    private LoggingActionType(String action) {
        this.action = action;
    }

    /**
     * Method to get Status enum from key.
     *
     * @param action
     *            action
     * @return CustomAccountStatusEnum CustomAccountStatusEnum
     * @throws InvalidDataException
     *             if enum not found for given data
     */
    public static LoggingActionType getStatusEnum(String action)
        throws InvalidRequestException {
        if (action == null) {
            //TODO:Define Error Code
            throw new InvalidRequestException("Invalid data supplied");
        } else {
            if (BGC_LOGGING_ACTIONS_MAP.get(action.toLowerCase()) != null) {
                return BGC_LOGGING_ACTIONS_MAP.get(action.toLowerCase());
            } else {
                throw new InvalidRequestException("Invalid data supplied");
            }
        }
    }

    /**
     * Returns the corresponding {@link String}.
     *
     * @return action name
     */
    public String getActionAsString() {
        return action;
    }
}
