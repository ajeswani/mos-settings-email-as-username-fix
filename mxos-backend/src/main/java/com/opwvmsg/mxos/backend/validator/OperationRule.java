/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.validator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.validator.XMLActionHandler.ParamTuple;


/**
 * The OperationRule holds the rules for all request parameters of single
 * operation.
 * @author mxos-dev
 */
public class OperationRule {
    // List of options with the respective mandatory parameters
    private List<Map<String, ParamTuple>> mandatoryParamRules;
    // List of options with the respective optional parameters
    private List<Map<String, ParamTuple>> optionalParamRules;
    // List of options with the respective mandatory parameters
    private Map<String, Map<String, ParamTuple>> actionRules;
    // Stores the return type of the operation
    private String returnObjectName;
    // Operation Level errorCode (if mandatory params not passed)
    private String errorCode;

    /**
     * Default Constructor.
     */
    public OperationRule() {
        mandatoryParamRules = new ArrayList<Map<String, ParamTuple>>();
        optionalParamRules = new ArrayList<Map<String, ParamTuple>>();
        actionRules = new LinkedHashMap<String, Map<String, ParamTuple>>();
    }
    /**
     * @return the actionRules
     */
    public Map<String, Map<String, ParamTuple>> getActionRules() {
        return actionRules;
    }

    /**
     * @param actionRules the actionRules to set
     */
    public void setActionRules(Map<String,
            Map<String, ParamTuple>> actionRules) {
        this.actionRules = actionRules;
    }
    /**
     * @return the MandatoryRules
     */
    public List<Map<String, ParamTuple>> getMandatoryParamRules() {
        return mandatoryParamRules;
    }
    /**
     * @return the OptionalRules
     */
    public List<Map<String, ParamTuple>> getOptionalParamRules() {
        return optionalParamRules;
    }
 
    /**
     * @return the returnObjectName
     */
    public String getReturnObjectName() {
        return returnObjectName;
    }
    
    /**
     * @param returnObjectName the returnObjectName to set
     */
    public void setReturnObjectName(String returnObjectName) {
        this.returnObjectName = returnObjectName;
    }
    /**
     * method to get Error code.
     * @return errorCode errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }
    /**
     * method to set Error code.
     * @param errorCode errorCode
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
