/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.externalaccount;

import java.util.List;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update the mailAccounts.
 * 
 * @author mxos-dev
 */
public class CheckValidExternalMailAccount implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(CheckValidExternalMailAccount.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("IsAccountIdExisting action start."));
        }
        try {
            List<MailAccount> mailAccountList = null;
            ExternalAccounts ea = (ExternalAccounts) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.externalAccounts);
            mailAccountList = ea.getMailAccounts();

            String accountId = requestState.getInputParams()
                    .get(MailboxProperty.accountId.name()).get(0);

            if (accountId == null)
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ACCOUNT_ID.name());

            boolean exists = false;

            MailAccount newMa = new MailAccount();
            if (mailAccountList != null) {
                for (MailAccount ma : mailAccountList) {
                    if (ma.getAccountId() == Integer.parseInt(accountId)) {
                        newMa = ma;
                        exists = true;
                    }
                }
                if (exists == false) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_MAIL_ACCOUNT_NOT_FOUND.name());
                }
            }

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailAccount,
                    newMa);

        } catch (InvalidRequestException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while checking if accountId is existing.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_MAIL_ACCOUNTS.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("IsAccountIdExisting action end."));
        }
    }
}
