/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to delete an AllowedDomains.
 * 
 * @author mxos-dev
 */
public class DeleteAllowedDomain implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteAllowedDomain.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteAllowedDomain action start."));
        }
        final String allowedDomainToRemove =
                requestState.getInputParams()
                .get(MailboxProperty.allowedDomain.name()).get(0);

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        final Base base;
        try {
            mailboxCRUDPool =
                    MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            base =
                    ActionUtils.readMailboxProvisioning(requestState,
                    mailboxCRUD);
        } catch (final Exception e) {
            logger.error("Error while delete allowed domain.", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        try {
            if (base == null) {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }

            List<String> allowedDomainsList = null;
            boolean domainToDeleteExist = false;
            if (base.getAllowedDomains() != null) {
                allowedDomainsList = base.getAllowedDomains();
                if (allowedDomainsList.contains(allowedDomainToRemove)) {
                    domainToDeleteExist = true;
                }
            }
            if (!domainToDeleteExist) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ALLOWED_DOMAIN.name());
            }

            final List<String> aliasesList = base.getEmailAliases();

            if (aliasesList != null) {
                final Set<String> domainArray = new HashSet<String>();
                final Iterator<String> iterator = aliasesList.iterator();
                while (iterator.hasNext()) {
                    final String s = iterator.next();
                    final String[] temp = s.split(MxOSConstants.AT_THE_RATE);
                    if (temp.length == 2) {
                        domainArray.add(temp[1]);
                    } else {
                        throw new InvalidRequestException(
                                DomainError.DMN_INVALID_NAME.name());
                    }
                }
                if (domainArray.contains(allowedDomainToRemove)) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_ALLOWED_DOMAIN.name());
                }
            }

            if (!allowedDomainsList.remove(allowedDomainToRemove)) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ALLOWED_DOMAIN.name());
            }

            final String[] allowedDomainesArray = allowedDomainsList
                    .toArray(new String[allowedDomainsList.size()]);
            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.allowedDomain,
                            allowedDomainesArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while delete allowed domain.", e);
            throw new ApplicationException(
                    MailboxError.MBX_ALLOWEDDOMAINS_UNABLE_TO_DELETE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteAllowedDomain action end."));
        }
    }
}
