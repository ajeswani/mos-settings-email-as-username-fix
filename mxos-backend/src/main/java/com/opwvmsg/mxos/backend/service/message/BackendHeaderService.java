/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.message;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Message Header service exposed to client which is responsible for doing basic
 * message Header related activities e.g read Message Header, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class BackendHeaderService implements
        com.opwvmsg.mxos.interfaces.service.message.IHeaderService {

    private static Logger logger = Logger
            .getLogger(BackendHeaderService.class);

    /**
     * Constructor.
     * 
     * @param metaObjectPool Object pool to talk to metadata.
     * @param blobCRUD Object pool to talk to blobdata.
     * @param serviceProperties Service properties like soft deleted, etc.
     */
    public BackendHeaderService() {
        logger.info("BackendMessageHeaderService created...");
    }

    @Override
    public Header read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.HeaderService,
                    Operation.GET);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.HeaderService, Operation.GET,
                    t0, StatStatus.pass);
            return (Header) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.headerSummary);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.HeaderService, Operation.GET,
                    t0, StatStatus.fail);
            throw e;
        }
    }

}
