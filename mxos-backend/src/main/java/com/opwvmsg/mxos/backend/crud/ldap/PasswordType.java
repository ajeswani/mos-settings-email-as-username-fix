/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.utils.paf.util.password.HashType;

/**
 * 
 * @author mxos-dev
 * 
 */
public enum PasswordType {

    CLEAR(HashType.CLEAR, "clear"), MD5(HashType.MD5, "md5-po"), UNIX(
            HashType.UNIX, "unix"), SHA(HashType.SHA, "sha1"), SSHA1(
            HashType.SSHA1, "ssha1"), CUSTOM1(HashType.CUSTOM1, "custom1"), CUSTOM2(
            HashType.CUSTOM2, "custom2"), CUSTOM3(HashType.CUSTOM3, "custom3"), CUSTOM4(
            HashType.CUSTOM4, "custom4"), CUSTOM5(HashType.CUSTOM5, "custom5"), BCRYPT(
            HashType.BCRYPT, "bcrypt");

    private HashType type;

    private String typeText;

    private static final Map<String, PasswordType> TYPE_MAP =
            new HashMap<String, PasswordType>();

    static {
        TYPE_MAP.put("C", CLEAR);
        TYPE_MAP.put("U", UNIX);
        TYPE_MAP.put("H", SHA);
        TYPE_MAP.put("S", SSHA1);
        TYPE_MAP.put("1", CUSTOM1);
        TYPE_MAP.put("2", CUSTOM2);
        TYPE_MAP.put("3", CUSTOM3);
        TYPE_MAP.put("4", CUSTOM4);
        TYPE_MAP.put("5", CUSTOM5);
        TYPE_MAP.put("M", MD5);
        TYPE_MAP.put("B", BCRYPT);
    }

    /**
     * 
     * @param type type
     * @param typeText typeText
     */
    private PasswordType(final HashType type, final String typeText) {
        this.type = type;
        this.typeText = typeText;
    }

    /**
     * 
     * @param typeText typeText
     */
    private PasswordType(final String typeText) {
        this.typeText = typeText;
    }

    /**
     * Converts AccountProperty#MailPasswordType to IntermailPasswordType.
     * 
     * @param str AccountProperty#MailPasswordType
     * @return the corresponding enum object
     */
    public static final PasswordType getTypeWithKey(final String key)
        throws InvalidRequestException {
        if (key != null) {
            final PasswordType result = TYPE_MAP.get(key.toUpperCase());
            if (result != null) {
                return result;
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                        "Invalid password type found : " + key);
            }
        }
        return CLEAR;
    }

    /**
     * 
     * @param str TypeText
     * @return Type Enum
     */
    public static final PasswordType getTypeWithValue(final String value)
        throws InvalidRequestException {
        if (value == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                    "Password type value is null");
        }
        for (final Entry<String, PasswordType> entry : TYPE_MAP.entrySet()) {
            if (entry.getValue().getText().equalsIgnoreCase(value)) {
                return entry.getValue();
            }
        }
        throw new InvalidRequestException(
                MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                "Unsupported password type:" + value);
    }

    /**
     * 
     * @param key autoMode
     * @return automode Enum
     */
    public static String getPasswordTypeKey(final String key)
        throws InvalidRequestException {
        if (key == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                    "Invalid passwordStoreType:" + key);
        }
        for (final Entry<String, PasswordType> entry : TYPE_MAP.entrySet()) {
            if (entry.getValue().getText().equalsIgnoreCase(key)) {
                return entry.getKey();
            }
        }
        throw new InvalidRequestException(
                MailboxError.MBX_INVALID_PASSWORD_STORETYPE.name(),
                "Unsupported passwordStoreType:" + key);
    }

    /**
     * Returns the corresponding HashType.
     * 
     * @return password hash type
     */
    public final HashType getHashType() {
        return type;
    }

    /**
     * Returns complete password type description as text.
     * 
     * @return String text
     */
    public final String getText() {
        return typeText;
    }
}
