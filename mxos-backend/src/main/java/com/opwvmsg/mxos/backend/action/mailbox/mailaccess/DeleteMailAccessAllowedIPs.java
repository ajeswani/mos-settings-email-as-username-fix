/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailaccess;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to delete Allowed IPs for MailAccess Object.
 *
 * @author Aricent
 */
public class DeleteMailAccessAllowedIPs implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(DeleteMailAccessAllowedIPs.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteMailAccessAllowedIPs action start."));
        }

        final String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);
        final String allowedIP = requestState.getInputParams()
                .get(MailboxProperty.allowedIP.name()).get(0);
        final List<String> allowedIPList;
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final MailAccess ma = mailboxCRUD.readMailAccess(email);
            if (null == ma.getAllowedIPs() || ma.getAllowedIPs().isEmpty()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_MAILACCESS_ALLOWED_IP_NOT_EXIST.name());
            } else {
                allowedIPList = ma.getAllowedIPs();
            }
            if (!allowedIPList.contains(allowedIP)) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_MAILACCESS_ALLOWED_IP.name());
            }

            if (allowedIPList.remove(allowedIP)) {
                final String[] allowedIPArray = allowedIPList
                        .toArray(new String[allowedIPList.size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState, MailboxProperty.allowedIP,
                                allowedIPArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_DELETE
                                .name());
            }
        } catch (MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while delete mail access allowed IPs.", e);
            throw new ApplicationException(
                    MailboxError.MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_DELETE.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new InvalidRequestException(
                            ErrorCode.GEN_INTERNAL_ERROR.name());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteMailAccessAllowedIPs action end."));
        }
    }
}
