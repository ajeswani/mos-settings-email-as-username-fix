/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

/**
 * Property class for Mailbox.
 *
 * @author mxos-dev
 *
 */
public enum LDAPMailboxProperty {
    //Mailbox Base
    mailStatusChangeDate, mailNotificationStatus, smsCredentialMSISDN, smsCredentialMSISDNStatus,
    smsCredentialMSISDNStatusChangeDate,

    maillogin, domainname, mail, mailboxid,
    adminpolicydn, cn, sn, mailfamilymailbox,
    mailmaxalternateaddresses, mailboxstatus, mailallowedaliasdomains,
    mailmaxallowedaliasdomains, mailalternateaddress, autoreplymessage,
    mailquotamaxmsgs, mailquotatotkb, mailsoftquotamaxmsgs, mailsoftquotatotkb,
    altmailQuotaMaxMsgs, altmailQuotaTotKB, altmailsoftquotamaxmsgs, altmailsoftquotatotkb,
    mailquotathreshold, mailquotabouncenotify, mailfolderquota,
    netmailInfoStoreAccessAllowed, netmailInfoStoreQuotaMB, maillargemailboxplatform,

    // WebMailFeatures attributes
    netmailrmcos, netmailFullFeaturesEnabled, netmailLastFullFeaturesConnectionDate,mailwebmailchangepassword,
    
    // WebMailFeaturesAddressBook attributes
    mailWebMailAddressBookLimit, netmailAddressbookContactListMax, mailWebMailAddressBookListLimit, netMailAbEntriesPerPage,
    netMailAutoCollect,

    //Credentials
    mailpasswordtype, mailpassword, mailpasswordrecovery, mailpasswordrecoverypreference,
    smspasswordrecoverymsisdn, smspasswordrecoverymsisdnstatus,
    smspasswordrecoverymsisdnstatuschangedate, mailpasswordrecoveryemail, 
    mailpasswordrecoveryemailstatus, mailLastLoginAttemptDate, mailLastSuccessfulLoginDate, mailFailedLoginAttempts,
    mailMaxFailedLoginAttempts, mailMaxFailedCaptchaLoginAttempts, mailFailedCaptchaLoginAttempts,

    // GeneralPreferences
    msglocale, msgcharset, netmaillocale, msgtimezone,
    netmailusetrash, uservariantsavailable,
    netmailvariant, netmailmta, netmailwebapp, mailparentalcontrol,

    // MailAccess
    mailpopaccess, mailallowtheseips, mailapopaccess, mailimapaccess,
    mailpopsslaccess, mailimapsslaccess, mailsmtpaccess, mailsmtpsslaccess,
    mailsmtpauth, mailwebmailaccess,
    mailwebmailsslaccess, mailmobilemailaccess, netmailrmactivesyncenabled,

    // MailSend attributes
    mailfrom, mailfuturedeliveryenabled, mailfuturedeliverymax,
    mailfuturedeliveryquota, netmailactivealias, mailreplyto,
    netmaildefaulteditor, mailincludeoriginal, netmailreplyformat,
    mailautosave, mailwebmailusesignature, netmailsignaturebeforereply,
    netmailautospellcheck, mailwebmailconfirmdelete,
    netmailrequestreturnreceipt, mailFutureDeliveryPending, mailquotasendermaxmsgkb,
    mailwebmailattachsizelimit, mailwebmailattachlimit, mailwebdisplay,
    mailwebmailmsgattachlimit,
    // MailSend BmiFilters
    mailbmimailwalloutboundspamactionverb, mailbmimailwalloutboundactioninfo,
    // MailSend CommtouchFilters
    mailcommtouchoutboundspamaction, mailcommtouchoutboundvirusaction,
    // MailSend McAfeeFilters
    mailmcafeeoutboundvirusaction,

    // MailReceipt attributes
    maildeliveryaccess, mailforwarding, maildeliveryoption,
    mailforwardingaddress,mailmaxforwardingaddresses, netmailmsgviewprivacyfilter, netmailpreferredmsgview,
    netmailweblinewidth, netmailmsglistcols, netmailmsgsperpage,
    netmailEnablePreview, netmailsendreturnreceipt, maildeliveryscope,
    mailscoperestrictedaddress, mailautoreplymode, mailquotamaxmsgkb,
    altmailQuotaMaxMsgKB,
    // MailReceipt SenderBlocking
    mailblocksendersaccess, mailblocksendersactive, mailrejectaction,
    mailrejectInfo, mailapprovedsenderslist, mailblockedsenderslist,
    mailblocksenderspabactive, mailblocksenderspabaccess,
    // MailReceipt SieveFilters
    mailmtafilterperuser, maildeniedsender, maildeniedsenderaction,
    maildeniedsendermessage, mailmtafilter,
    // MailReceipt BmiFilters
    mailbmimailwallspamactionverb, mailbmimailwallactioninfo,
    // MailReceipt CommtouchFilters
    mailcommtouchinboundspamaction, mailcommtouchinboundvirusaction,
    // MailReceipt CloudmarkFilters
    asfenabled, asfthresholdpolicy, asfdeliveryactiondirty,
    asfdeliveryactionclean, asfdeliveryactionsuspect,
    // MailReceipt McAfeeFilters
    mailmcafeeinboundvirusaction,

    // SmsServicess attributes
    smsServicesAllowed, smsServicesMSISDN, smsServicesMSISDNStatus,
    smsServicesMSISDNStatusChangeDate, smsOnline, smsOnlineInternationalAllowed,
    smsOnlineInternationalEnabled, smsOnlineMaxSMSPerDay,
    smsOnlineConcatenatedSMSAllowed, smsOnlineConcatenatedSMSEnabled,
    smsOnlineMaxSMSSegments, smsCaptchaMessagesThreshold,
    smsCaptchaDurationThreshold, smsBasicNotifications, smsAdvancedNotifications,

    // Internal Info Attributes 
    netmailcurrentver, mailselfcare, mailselfcaressl, mailmessagestore,
    mailsmtprelayhost, mailimapproxyhost, mailpopproxyhost, mailautoreplyhost,
    imapproxyauthenticate, remotecalltracingenabled, mailintermanager,
    mailintermanagerssl, netmailenablevoice, netmailenablefax,
    mailldapaccess, addressbookprovider, mersenabled, mersmaxheaderlength,
    mersfilesizekb, mailRealm,

    // SocialNetworks
    netmailsocialnetworkintegrationallowed,
    netmailSocialNetworkSite,

    //ExternalAccounts
    netmailexternalaccounts,
    netmailpopemailacctselect,
    netmailpopacctdetails
}
