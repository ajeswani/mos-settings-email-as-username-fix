/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.gp;

import java.util.SimpleTimeZone;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class SetTimeZone implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetTimeZone.class);
    private static final int MINUTE = 60 * 1000;
    private static final int HOUR = 60 * MINUTE;

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetTimeZone action start."));
        }
        final String newTimeZone = requestState.getInputParams()
                .get(MailboxProperty.timezone.name()).get(0);

        if (!validateTimeZone(newTimeZone)) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_TIMEZONE.name());
        }

        //String posixTimeZone = getPosixId(newTimeZone);

        if (newTimeZone != null) {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.timezone,
                            newTimeZone);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetTimeZone action end."));
        }
    }

    private static boolean validateTimeZone(String givenTimeZone) {
        for (String timezone : TimeZone.getAvailableIDs()) {
            if (givenTimeZone.equalsIgnoreCase(timezone)) {
                return true;
            }
        }
        return false;
    }

    public static String getPosixId(String olsonID) {
        TimeZone tz = SimpleTimeZone.getTimeZone(olsonID);
        String stdName = tz.getDisplayName(false, TimeZone.SHORT);
        String dstName = "";
        if (tz.useDaylightTime()) {
            dstName = tz.getDisplayName(true, TimeZone.SHORT);
        }
        int hour = tz.getRawOffset() / HOUR;
        int minute = Math.abs(tz.getRawOffset() / MINUTE - hour * 60);

        String posix;
        if (minute == 0) {
            posix = stdName + (-hour) + dstName;
        } else {
            posix = stdName + (-hour) + ":" + minute + dstName;
        }
        return posix;
    }
}
