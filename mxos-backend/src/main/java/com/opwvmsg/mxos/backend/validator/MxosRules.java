/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.validator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * The MxosRules class is used to load Validation Properties file at startup of
 * application.
 *
 * @author mxos-dev
 */
public final class MxosRules {
    private static Logger logger = Logger.getLogger(MxosRules.class);
    // Map of all the Operations and their rules
    private Map<String, OperationRule> operations;

    /**
     * Default Constructor.
     *
     * @param app name of the application
     * @throws MxOSException exception
     */
    public MxosRules(String app) throws MxOSException {
        logger.debug("Loading Validaton properties and creation of ruleMap");
        operations = new LinkedHashMap<String, OperationRule>();
        loadRulesFromXML(app);
    }

    /**
     * @return the rulesMap
     */
    public Map<String, OperationRule> getOperations() {
        return operations;
    }

    /**
     * @param operations the rulesMap to set
     */
    public void setOperations(Map<String, OperationRule> operations) {
        this.operations = operations;
    }

    /**
     * loadProperties is to load the properties file.
     *
     * @param app name of the application
     * @throws MxOSException throws in case of error in loading rules
     */
    protected void loadRulesFromXML(String app) throws MxOSException {
        // TODO: The validation-rules file name should take from configuration
        String rulesFileName = "-mxos-rules.xml";
        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }
        String configDir = home + "/config/" + app + "/";
        InputStream is = null;
        final Map<String, OperationRule> ops = getOperations();
        try {
            final String loadRulesOrder = MxOSConfig.getLoadRulesOrder();
            if (loadRulesOrder == null || loadRulesOrder.equals("")) {
                // throw MxOS Application Exception
                // Configure loadRulesOrder in mxos.properties
                throw new RuntimeException(
                        "loadRulesOrder is missing mxos.properties");
            }
            String[] objects = loadRulesOrder.split(",");
            for (String object : objects) {
                String fileName = configDir + object.trim() + rulesFileName;
                System.out.println("Loging Rule :" + fileName);
                File file = new File(fileName);
                if (file != null
                        && file.getCanonicalPath().endsWith(rulesFileName)) {
                    logger.debug("Loading rules file : "
                            + file.getCanonicalPath());
                    is = new FileInputStream(file.getCanonicalPath());
                    boolean ok = false;
                    ok = XMLActionHandler.parse(is, ops);
                    logger.info("loadRulesFromXML ret : " + ok);
                    IOUtils.closeQuietly(is);
                }
            }
        } catch (final Exception e) {
            logger.error("Error while load rules from xml.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e.getMessage());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
}
