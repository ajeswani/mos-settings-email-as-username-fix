/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.domain;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.DomainType;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set Domain type.
 * 
 * @author mxos-dev
 * 
 */
public class SetDomainType implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetDomainType.class);

    /**
     * Action method to set Domain type.
     *
     * @param model model instance
     * @throws Exception in case of any error
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetDomainType action start."));
        }
        try {
            final DomainType domainType = DomainType.fromValue(
                    requestState.getInputParams()
                    .get(DomainProperty.type.name()).get(0));
            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, DomainProperty.type,
                            domainType);
        } catch (MxOSException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_INVALID_TYPE.name(), e);
        } catch (final Exception e) {
            logger.error("Error while set domain type.", e);
            throw new ApplicationException(
                    DomainError.DMN_INVALID_TYPE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetDomainType action end."));
        }
    }
}
