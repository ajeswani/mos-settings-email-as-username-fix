/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.we.utils;

import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.we.WEAddressBookUtil;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;

public class Filter implements Visitor {

    enum Type implements EnumConverter {
        ALL(".*"), ABC("^[ABC].*"), DEF("^[DEF].*"), GHI("^[GHI].*"), JKL(
                "^[JKL].*"), MNO("^[MNO].*"), PQRS("^[PQRS].*"), TUV("^[TUV].*"), WXYZ(
                "^[WXYZ].*"), OTHER("^[^A-Z].*");

        private final String value;

        Type(String value) {
            this.value = value;
        }

        @Override
        public String convert() {
            return value;
        }
    }

    private Type filterType;

    private static final AddressBookProperty filterKey = AddressBookProperty.displayName;
    private static Logger logger = Logger.getLogger(Filter.class);

    @Override
    public void parse(String input) throws AddressBookException {
        // parse input into filter type
        try {
            filterType = Type.valueOf(input.toUpperCase());
        } catch (IllegalArgumentException ile) {
            // should not happen, added as failsafe
            logger.error("Exception occurred : ", ile);
            throw new AddressBookException(
                    AddressBookError.ABS_INVALID_FILTER.name(), ile);
        }
    }

    @Override
    public boolean visit(Map<String, String> valueMap) {

        if (valueMap.containsKey(WEAddressBookUtil.JSONToWE_AddressBook.get(
                filterKey).name())) {
            String displayName = valueMap
                    .get(WEAddressBookUtil.JSONToWE_AddressBook.get(filterKey)
                            .name());
            return displayName.toUpperCase().matches(filterType.convert());
        }
        return false;
    }
}
