package com.opwvmsg.mxos.backend.crud.ox;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

public abstract class OXAbstractHttpCRUD {

    enum ImageFormat {
        BMP(".bmp"), GIF(".gif"), JPEG(".jpg"), PNG(".png");
        final String type;

        private ImageFormat(final String type) {
            this.type = type;
        };
    }

    private static Logger logger = Logger.getLogger(OXAbstractHttpCRUD.class);
    private static final String DEFAULT_LOOPBACK_ADDRESS = "127.0.0.1";
    private static final String IMAGE_FILE_PREFIX = "mos-";

    protected WebResource webResource;
    protected final String LOGIN_STRING = "login";
    protected final String LOGOUT_STRING = "logout";
    protected final String USER_STRING = "user";
    protected final String GETUSER_STRING = "get";
    protected final String ACTION_STRING = "action";
    protected static final String OPEN_XCHANGE_PUBLIC_SESSION_STRING = "open-xchange-public-session";
    protected static final String JSESSIONID_STRING = "JSESSIONID";
    protected static final String OPEN_XCHANGE_SECRET_STRING = "open-xchange-secret-";
    private static final int DEFAULT_OX_HTTP_CONNECTION_TIMEOUT = 2000;
    private static final int DEFAULT_OX_HTTP_READ_TIMEOUT = 2000;

    /**
     * Method to convert Map<String, List<String>> to MultivaluedMap. This is a
     * temparary solution to convert map to multivaluedmap.
     * 
     * @param params Map<String, List<String>>
     * @return MultivaluedMap<String, String> map
     */
    protected static MultivaluedMap<String, String> getMultivaluedMap(
            final Map<String, List<String>> params) {
        final MultivaluedMap<String, String> map = new MultivaluedMapImpl();
        for (Entry<String, List<String>> entry : params.entrySet()) {
            List<String> values = new ArrayList<String>();
            for (String value : entry.getValue()) {
                values.add(value);
            }
            map.put(entry.getKey(), values);
        }
        return map;
    }

    /**
     * Method to get Web Resources.
     * 
     * @param subURL - Base URL for REST request.
     * @return webResource object
     */
    protected static WebResource getWebResource(final String baseURL) {
        final ClientConfig config = new DefaultClientConfig();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
                Boolean.TRUE);
        Client client = Client.create(config);

        String connectionTimeoutString = System
                .getProperty(SystemProperty.oxHttpConnectionTimeout.name());
        String readTimeoutString = System
                .getProperty(SystemProperty.oxHttpReadTimeout.name());
        int connectionTimeout = DEFAULT_OX_HTTP_CONNECTION_TIMEOUT;
        int readTimeout = DEFAULT_OX_HTTP_READ_TIMEOUT;
        try {
            connectionTimeout = Integer.parseInt(connectionTimeoutString);
        } catch (NumberFormatException nfe) {
            logger.error(new StringBuilder("Error in parsing ")
                    .append(SystemProperty.oxHttpConnectionTimeout)
                    .append(" property, using default value [")
                    .append(DEFAULT_OX_HTTP_CONNECTION_TIMEOUT).append("]"));

        }

        try {
            readTimeout = Integer.parseInt(readTimeoutString);
        } catch (NumberFormatException nfe) {
            logger.error(new StringBuilder("Error in parsing ")
                    .append(SystemProperty.oxHttpReadTimeout)
                    .append(" property, using default value [")
                    .append(DEFAULT_OX_HTTP_READ_TIMEOUT).append("]"));

        }
        client.setConnectTimeout(connectionTimeout);
        client.setReadTimeout(readTimeout);
        final WebResource webResource = client.resource(baseURL);
        //For Grizzly support
        webResource.accept(MediaType.APPLICATION_JSON).header("User-Agent","OWM mOS Client");
        webResource.accept(MediaType.APPLICATION_JSON);
        return webResource;
    }

    /**
     * Close HTTP resource.
     */
    public void close() {
        webResource.delete();
    }

    public void commit() throws ApplicationException {
        // Not supported for OX
    }

    /**
     * Does HTTP-REST DELETE request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP DELETE parameters (will be appended to URL) in
     *            key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     * @throws ComponentException
     */
    protected ClientResponse deleteOX(final String subURL,
            final Map<String, List<String>> params) throws MxOSException,
            ComponentException {
        final ClientResponse response = webResource.path(subURL)
                .queryParams(getMultivaluedMap(params))
                .delete(ClientResponse.class);
        validateResponseOX(response);
        return response;
    }

    /**
     * Does HTTP-REST GET request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP GET parameters (will be appended to URL) in
     *            key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     * @throws ComponentException
     */
    protected ClientResponse getOX(final String subURL,
            final Map<String, List<String>> params) throws MxOSException,
            ComponentException {

        WebResource.Builder builder = webResource.path(subURL)
                .queryParams(getMultivaluedMap(params)).getRequestBuilder();
        setCookies(params, builder);

        final ClientResponse response = builder.get(ClientResponse.class);
        validateResponseOX(response);
        return response;
    }

    protected String getHostIpAddress() {
        String ipAddress;
        try {
            InetAddress ip = InetAddress.getLocalHost();
            ipAddress = ip.getHostAddress();
        } catch (UnknownHostException e) {
            logger.error("Error in getting host ip address, pls set hostname correctly, using default");
            ipAddress = DEFAULT_LOOPBACK_ADDRESS;
        }
        return ipAddress;
    }

    /**
     * Utility to return image type.
     * 
     * @param data
     * @return
     * @throws IOException
     * @throws ComponentException
     */
    protected String getImageType(byte[] data) throws IOException,
            ComponentException {
        String format = null;
        ImageInputStream iis = ImageIO
                .createImageInputStream(new ByteArrayInputStream(data));
        Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
        if (iter.hasNext()) {
            ImageReader reader = iter.next();
            format = reader.getFormatName();
        } else {
            throw new ComponentException(
                    AddressBookError.ABS_INVALID_SIZE_OF_CONTACTS_IMAGE.name(),
                    ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                    ExceptionUtils.OX_INVALID_CONTACT_IMAGE_ERROR_CODE, "");
        }
        if (format == null || format.equals("wbmp")) {
            // wbmp is not supported by OX.
            throw new ComponentException(
                    AddressBookError.ABS_INVALID_SIZE_OF_CONTACTS_IMAGE.name(),
                    ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                    ExceptionUtils.OX_INVALID_CONTACT_IMAGE_ERROR_CODE, "");
        }
        return ImageFormat.valueOf(format.toUpperCase()).type;
    }

    /**
     * Does HTTP-REST POST request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP POST body parameters in key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     * @throws ComponentException
     */
    protected ClientResponse postOX(final String subURL,
            final Map<String, List<String>> queryParams,
            final Map<String, List<String>> params) throws MxOSException,
            ComponentException {
        final ClientResponse response;
        if (System.getProperty(SystemProperty.oxAuthBypassKey.name()) != null
                && params.get(System.getProperty(SystemProperty.oxAuthBypassKey
                        .name())) != null) {
            response = webResource
                    .queryParams(getMultivaluedMap(queryParams))
                    .path(subURL)
                    .header(MxOSConstants.OX_AUTH_BYPASS_HEADER,
                            getHostIpAddress())
                    .post(ClientResponse.class, getMultivaluedMap(params));
        } else {
            response = webResource.queryParams(getMultivaluedMap(queryParams))
                    .path(subURL)
                    .post(ClientResponse.class, getMultivaluedMap(params));
        }
        validateResponseOX(response);
        return response;
    }

    /**
     * Does HTTP-REST POST request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP POST body parameters in key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     * @throws ComponentException
     * @throws IOException
     */
    protected ClientResponse postMultipartOX(final String subURL,
            final Map<String, List<String>> queryParams,
            final Map<String, List<String>> params) throws MxOSException,
            ComponentException, IOException {
        WebResource.Builder builder = webResource.path(subURL)
                .queryParams(getMultivaluedMap(queryParams))
                .getRequestBuilder().type(MediaType.MULTIPART_FORM_DATA);

        String actualImage = params.get(AddressBookProperty.actualImage.name())
                .get(0);
        FormDataMultiPart multiPart = new FormDataMultiPart();

        if (null != actualImage && !"".equals(actualImage)) {
            byte[] data = Base64.decodeBase64(actualImage.getBytes());

            String suffix = getImageType(data);

            File file = File.createTempFile(IMAGE_FILE_PREFIX, suffix);
            FileUtils.writeByteArrayToFile(file, data);
            multiPart.bodyPart(new FileDataBodyPart(OXContactsProperty.file
                    .name(), file));
        }

        String jsonValue = params.get(OXContactsProperty.json.name()).get(0);
        multiPart.bodyPart(new FormDataBodyPart(OXContactsProperty.json.name(),
                jsonValue));

        setCookies(params, builder);
        final ClientResponse response = builder.post(ClientResponse.class,
                multiPart);
        validateResponseOX(response);
        return response;
    }

    /**
     * Does HTTP-REST PUT request.
     * 
     * @param subURL - SubURL to make REST request.
     * @param params - HTTP PUT body parameters in key-value format inside map.
     * @return Returns HTTP ClientResponse.
     * @throws MxOSException Mxos Exception.
     * @throws ComponentException
     */
    protected ClientResponse putOX(final String subURL,
            final Map<String, List<String>> params, final String jsonString)
            throws MxOSException, ComponentException {
        WebResource.Builder builder = webResource.path(subURL)
                .queryParams(getMultivaluedMap(params)).getRequestBuilder();
        builder.accept(MediaType.APPLICATION_JSON);
        setCookies(params, builder);
        final ClientResponse response = builder.put(ClientResponse.class,
                jsonString);
        validateResponseOX(response);
        return response;
    }

    public ExternalSession loginOX(MxOSRequestState mxosRequestState)
            throws ComponentException {

        final String subUrl = LOGIN_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(LOGIN_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.userId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.password.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(MxOSConstants.OX_CLIENT_ID);

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.name.name(), paramList2);
        paramsNew.put(OXContactsProperty.password.name(), paramList3);
        paramsNew.put(OXContactsProperty.client.name(), paramList4);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList5);

        try {
            final ClientResponse r = postOX(subUrl, paramsNew, params);
            return getSessionFromResponse(r);
        } catch (final ComponentException e) {
            throw e;
        } catch (final Exception e) {
            throw new ComponentException(e);
        }
    }

    private void setCookie(WebResource.Builder builder, String cookieStr,
            int fromIndex) {
        int toIndex = cookieStr.indexOf(';', fromIndex);
        String[] cookie = null;
        if (toIndex != -1) {
            cookie = cookieStr.substring(fromIndex, toIndex).split("=");
        } else {
            cookie = cookieStr.substring(fromIndex).split("=");
        }
        if (cookie != null && cookie[0] != null && cookie[1] != null) {
            // For Grizzly support
            builder.header("Cookie", cookie[0] + "=" + cookie[1]);
        }
    }

    private void setCookies(final Map<String, List<String>> params,
            WebResource.Builder builder) {
        List<String> cookies = params.get(AddressBookProperty.cookieString
                .name());
        if (cookies != null) {
            for (String cookieStr : cookies) {
                if (cookieStr != null) {
                    int fromIndex = cookieStr
                            .indexOf(OPEN_XCHANGE_SECRET_STRING);
                    if (fromIndex != -1) {
                        setCookie(builder, cookieStr, fromIndex);
                    }
                    fromIndex = cookieStr.indexOf(JSESSIONID_STRING);
                    if (fromIndex != -1) {
                        setCookie(builder, cookieStr, fromIndex);
                    }
                    fromIndex = cookieStr
                            .indexOf(OPEN_XCHANGE_PUBLIC_SESSION_STRING);
                    if (fromIndex != -1) {
                        setCookie(builder, cookieStr, fromIndex);
                    }

                }
            }
        }
    }

    protected void validateResponseOX(final ClientResponse response)
            throws ComponentException {
        if (response == null) {
            throw new ComponentException("Response is null");
        }
        if (response.getStatus() != javax.ws.rs.core.Response.Status.OK
                .getStatusCode()) {
            throw new ComponentException("Response status is not ok");
        }
    }

    public void logoutOX(MxOSRequestState mxosRequestState)
            throws ComponentException {

        final String subUrl = LOGIN_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(LOGOUT_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add("UTC");
        paramsNew.put(OXContactsProperty.timezone.name(), paramList4);

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.session.name(), paramList2);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList3);

        try {
            getOX(subUrl, paramsNew);
        } catch (final ComponentException e) {
            throw e;
        } catch (final Exception e) {
            throw new ComponentException(e);
        }
    }

    public void validateUserOX(MxOSRequestState mxosRequestState)
            throws ComponentException {
        final String subUrl = USER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GETUSER_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(AddressBookProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(AddressBookProperty.cookieString.name()).get(
                0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXContactsProperty.session.name(), paramList2);
        paramsNew.put(AddressBookProperty.cookieString.name(), paramList3);
        paramsNew.put(OXContactsProperty.timezone.name(), paramList4);

        try {
            ClientResponse resp = getOX(subUrl, paramsNew);
            String loggedInUserId = getUserIdFromResponse(resp);

            if (!params.get(AddressBookProperty.userId.name()).get(0)
                    .equals(loggedInUserId)) {
                throw new ComponentException(
                        AddressBookError.ABS_INVALID_USERNAME.name());
            }
        } catch (final ComponentException e) {
            throw e;
        } catch (final Exception e) {
            throw new ComponentException(e);
        }
    }

    protected abstract String getUserIdFromResponse(ClientResponse resp) throws ComponentException;

    protected abstract ExternalSession getSessionFromResponse(final ClientResponse r) throws ComponentException;
}
