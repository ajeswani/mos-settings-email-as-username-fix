/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksRecurrenceService;
import com.opwvmsg.mxos.task.pojos.Recurrence;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts Base service exposed to client which is responsible for doing basic
 * base related activities e.g read base, update base, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class BackendTasksDetailsService implements ITasksRecurrenceService {

    private static Logger logger = Logger
            .getLogger(BackendTasksDetailsService.class);

    /**
     * Default Constructor.
     */
    public BackendTasksDetailsService() {
        logger.info("BackendTasksProgressService Service created...");
    }

    @Override
    public Recurrence read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksExternalParticipantService,
                    Operation.GET);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.TasksDetailsService, Operation.GET,
                    t0, StatStatus.pass);

            return ((Recurrence) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.details));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksDetailsService, Operation.GET,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksDetailsService,
                    Operation.POST);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.TasksDetailsService, Operation.POST,
                    t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksDetailsService, Operation.POST,
                    t0, StatStatus.fail);
            throw e;
        }
    }
}
