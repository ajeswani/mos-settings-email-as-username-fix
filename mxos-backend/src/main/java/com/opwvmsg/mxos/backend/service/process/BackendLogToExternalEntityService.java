/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.process;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ILogToExternalEntityService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Log To External Entity Service exposed to client which is responsible for
 * logging the events to External Interfaces
 * 
 * @author mxos-dev
 */
public class BackendLogToExternalEntityService implements
        ILogToExternalEntityService {
    private static Logger logger = Logger
            .getLogger(BackendLogToExternalEntityService.class);

    /**
     * Constructor.
     */
    public BackendLogToExternalEntityService() {
        logger.info("BackendLogToExternalEntityService is created");
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "BackendLogToExternalEntityService:process() start"));
        }
        long t0 = Stats.startTimer();
        try {

            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.LogToExternalEntityService,
                    Operation.POST);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.LogToExternalEntityService,
                    Operation.POST, t0, StatStatus.pass);

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "BackendLogToExternalEntityService:process() End"));
            }
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.LogToExternalEntityService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }

    }
}
