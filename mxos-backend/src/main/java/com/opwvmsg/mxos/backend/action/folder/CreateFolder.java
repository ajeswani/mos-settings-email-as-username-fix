/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.folder;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.mailbox.MailboxMetaServiceHelper;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create Folder.
 * 
 * @author mxos-dev
 */
public class CreateFolder implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateFolder.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateFolder action started."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {

            if (requestState.getInputParams().get(
                    MailboxProperty.mailboxId.name()) != null) {
                final Long mailboxId = Long.parseLong(requestState
                        .getInputParams().get(MailboxProperty.mailboxId.name())
                        .get(0));
                requestState.getAdditionalParams().setProperty(
                        MailboxProperty.mailboxId, mailboxId);
            }
            final String folderName = requestState.getInputParams()
                    .get(FolderProperty.folderName.name()).get(0);

            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();

            FolderServiceHelper.create(metaCRUD, requestState, folderName);

            MailboxMetaServiceHelper.updateLastAccessTime(metaCRUD,
                    requestState);

            metaCRUD.commit();
        } catch (final MxOSException e) {
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while create folder.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), e);

        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateFolder action End."));
        }
    }
}
