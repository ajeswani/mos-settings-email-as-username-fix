/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.tasks;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Task;

/**
 * Action class to List Tasks object.
 * 
 * @author mxos-dev
 * 
 */
public class ListTasks implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(ListTasks.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetTasksBase action start."));
        }

        ICRUDPool<ITasksCRUD> tasksCRUDPool = null;
        ITasksCRUD tasksCRUD = null;

        try {
            tasksCRUDPool = MxOSApp.getInstance().getTasksCRUD();
            tasksCRUD = tasksCRUDPool.borrowObject();

            List<Task> res = tasksCRUD
                    .readAllTasks(requestState);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.allTasks,
                    res);
        } catch (TasksException e) {
            logger.error("Error while list tasks.", e);
            ExceptionUtils.createMxOSExceptionFromTasksException(
                    TasksError.TSK_UNABLE_TO_LISTTASKS, e);
        } catch (final Exception e) {
            logger.error("Error while get base.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (tasksCRUDPool != null && tasksCRUD != null) {
                    tasksCRUDPool.returnObject(tasksCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetTasksBase action end."));
        }
    }
}
