/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.intermail.mail.Mailbox;

public class AddAdminApprovedSendersList implements MxOSBaseAction {

    private static Logger logger = Logger
            .getLogger(AddAdminApprovedSendersList.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "AddAdminApprovedSendersList action start."));
            }
            try {
                List<String> existingAdminApprovedSendersList = (List<String>) requestState
                        .getDbPojoMap().getPropertyAsObject(
                                MxOSPOJOs.adminApprovedSendersList);

                List<String> newAdminApprovedSendersList = (List<String>) requestState
                        .getInputParams()
                        .get(MailboxProperty.adminApprovedSendersList.name());

                if (existingAdminApprovedSendersList == null) {
                    existingAdminApprovedSendersList = new ArrayList<String>();
                }
                if (existingAdminApprovedSendersList.size()
                        + newAdminApprovedSendersList.size() > MxOSConfig
                        .getMaxAdminApprovedSendersList()) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_ADMIN_APPROVED_SENDERS_REACHED_MAX_LIMIT
                                    .name(),
                            "admin approved senders reached max limit");
                }
                for (String newAdminApprovedSender : newAdminApprovedSendersList) {
                    if (!existingAdminApprovedSendersList
                            .contains(newAdminApprovedSender.toLowerCase())) {
                        existingAdminApprovedSendersList
                                .add(newAdminApprovedSender.toLowerCase());
                    }
                }
                final String[] adminApprovedSendersListToArray = existingAdminApprovedSendersList
                        .toArray(new String[existingAdminApprovedSendersList
                                .size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.adminApprovedSendersList,
                                adminApprovedSendersListToArray);
            } catch (MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error(
                        "Error while adding admin Approved sender for create",
                        e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_CREATE
                                .name(), e);
            } catch (final Throwable t) {
                logger.error("Error while adding admin approved senders.", t);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "AddAdminApprovedSendersList action end."));
            }
        } else {
            logger.error("Error while calling addadminapprovedsenderslist, group mailbox feature is disabled.");
            throw new InvalidRequestException(
                    MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
        }

    }
}
