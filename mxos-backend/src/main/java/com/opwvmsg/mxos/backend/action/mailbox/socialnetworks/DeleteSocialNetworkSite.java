/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.socialnetworks;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class DeleteSocialNetworkSite implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(DeleteSocialNetworkSite.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteSocialNetworkSite action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD provCRUD = null;
        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);
        String socialNetworkSite = requestState.getInputParams()
                .get(MailboxProperty.socialNetworkSite.name()).get(0);
        List<SocialNetworkSite> socialNetworkSiteList = null;
        try {
            if (socialNetworkSite == null) {
                throw new MxOSException(
                        MailboxError.MBX_SOCIALNETWORKS_MISSING_PARAMS.name());
            }
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            provCRUD = mailboxCRUDPool.borrowObject();
            SocialNetworks sn = provCRUD.readSocialNetworks(email);
            socialNetworkSiteList = sn.getSocialNetworkSites();
            SocialNetworkSite[] objSNSArray = new SocialNetworkSite[socialNetworkSiteList
                    .size() + 1];
            int index = 0;
            boolean isSitePresent = false;
            if (socialNetworkSiteList != null) {
                for (SocialNetworkSite sns : socialNetworkSiteList) {
                    if (!sns.getSocialNetworkSite().equalsIgnoreCase(
                            socialNetworkSite)) {
                        objSNSArray[index] = sns;
                        index++;
                    } else {
                        isSitePresent = true;
                    }
                }
            }
            if (isSitePresent == false) {
                throw new InvalidRequestException(
                        MailboxError.MBX_SOCIALNETWORK_SNS_NOTFOUND.name());
            }

            String[] strSNSArray = ActionUtils
                    .socialNetArrayToStringArray(objSNSArray);

            if (strSNSArray != null) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.socialNetworkSite, strSNSArray);
            }
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while delete social network site.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_DELETE_SOCIALNETWORKSSITE.name(),
                    e);
        } finally {
            if (mailboxCRUDPool != null && provCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(provCRUD);
                } catch (MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteSocialNetworkSite action end."));
        }
    }
}
