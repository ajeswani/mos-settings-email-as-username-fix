/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts service exposed to client which is responsible for doing basic name
 * related activities e.g read name, update name, etc. directly in the database.
 * 
 * @author mxos-dev
 */
public class BackendContactsService implements IContactsService {

    private static Logger logger = Logger
            .getLogger(BackendContactsService.class);

    private static final String LINE_REGEXP = "\\r?\\n";

    /**
     * Default Constructor.
     */
    public BackendContactsService() {
        logger.info("BackendContactsService Service created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsService, Operation.PUT);

            ActionUtils.setAddressBookBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.PUT, t0,
                    StatStatus.pass);
            return Long.parseLong(mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.contactId));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsService, Operation.POST);

            String input = inputParams.get(
                    AddressBookProperty.contactsList.name()).get(0);

            // convert input param to multiple requests
            String[] requests = input.split(LINE_REGEXP);

            // check if number of requests is not 0
            if (requests != null && requests.length == 0) {
                // throw exception
                throw new MxOSException(
                        AddressBookError.ABS_INVALID_MULTIPLE_CONTACTS.name());
            }
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.SEPARATOR);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUALS);
                    if (!inputParams.containsKey(paramKV[0])) {
                        inputParams.put(paramKV[0], new ArrayList<String>(
                                requests.length));
                    }
                }
            }

            // parse each line and extract the input params per request
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.SEPARATOR);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUALS);
                    for (String inputParam : inputParams.keySet()) {
                        if (inputParam.equals(paramKV[0]) && paramKV.length > 1) {
                            if (!inputParam.equals(AddressBookProperty.userId
                                    .name())) {
                                // pad with nulls
                                for (int j = inputParams.get(inputParam).size(); j < i; j++) {
                                    inputParams.get(inputParam).add(null);
                                }
                                // add new value at current index
                                inputParams.get(inputParam).add(paramKV[1]);
                            }
                        }
                    }
                }
            }

            for (String inputParam : inputParams.keySet()) {
                if (!inputParam.equals(AddressBookProperty.userId.name())) {
                    if (inputParams.get(inputParam).size() < requests.length) {
                        for (int i = inputParams.get(inputParam).size(); i < requests.length; i++) {
                            inputParams.get(inputParam).add(null);
                        }
                    }
                }
            }

            ActionUtils.setAddressBookBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.POST, t0,
                    StatStatus.pass);
            return (List<Long>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.contactIdList);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsService, Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.DELETE, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.DELETE, t0,
                    StatStatus.fail);
            throw e;
        }

    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Contact> list(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsService, Operation.GETALL);
            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsService, Operation.GETALL, t0,
                    StatStatus.pass);

            return ((List<Contact>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allContacts));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.GETALL, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void moveMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsService, Operation.MOVE);
            String input = inputParams.get(
                    AddressBookProperty.contactsList.name()).get(0);

            // convert input param to multiple requests
            String[] requests = input.split(LINE_REGEXP);

            // check if number of requests is not 0
            if (requests != null && requests.length == 0) {
                // throw exception
                throw new MxOSException(
                        AddressBookError.ABS_INVALID_MULTIPLE_MOVE_CONTACTS
                                .name());
            }
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.SEPARATOR);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUALS);
                    if (inputParams.get(paramKV[0]) == null) {
                        inputParams.put(paramKV[0], new ArrayList<String>(
                                requests.length));
                    }
                    inputParams.get(paramKV[0]).add(paramKV[1]);
                }
            }

            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsService, Operation.MOVE, t0,
                    StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsService, Operation.MOVE, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
