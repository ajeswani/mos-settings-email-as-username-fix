/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.cos;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create Cos.
 *
 * @author mxos-dev
 */
public class CreateCos implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateCos.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateCos action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool = MxOSApp.getInstance()
                    .getMailboxCRUD();

            // Create in LDAP
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            mailboxCRUD.createCos(requestState);

            mailboxCRUD.commit();
        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while cos create.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } finally {
            try {
                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateCos action end."));
        }
    }
}
