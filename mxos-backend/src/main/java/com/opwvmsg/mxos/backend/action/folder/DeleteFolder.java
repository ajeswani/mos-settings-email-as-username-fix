/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.folder;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to Delete Message.
 * 
 * @author mxos-dev
 */
public class DeleteFolder implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteFolder.class);

    /**
     * Action method to create Message.
     * 
     * @param model instance of model
     * @throws Exception throws in case of any errors
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteFolder action started."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        IBlobCRUD blobCRUD = null;

        try {
            if (requestState.getInputParams().get(
                    MailboxProperty.mailboxId.name()) != null) {
                final Long mailboxId =
                        Long.parseLong(requestState.getInputParams()
                                .get(MailboxProperty.mailboxId.name()).get(0));
                requestState.getAdditionalParams().setProperty(
                        MailboxProperty.mailboxId, mailboxId);
            }

            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();

            blobCRUD = CRUDUtils.getBlobCRUD(requestState);
            
            FolderServiceHelper.delete(metaCRUD, blobCRUD,
                    requestState);
            
            requestState.getAdditionalParams().setProperty(
                    FolderProperty.status, MxOSConstants.SOFT_DELETE);
            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                FolderServiceHelper.update(metaCRUD, requestState);
                if (!MxOSApp.getInstance().isSoftDelete()) {
                    FolderServiceHelper
                            .delete(metaCRUD, blobCRUD, requestState);
                }
            }
            blobCRUD.commit();
            metaCRUD.commit();
        } catch (final MxOSException e) {
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while delete folder.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            throw new ApplicationException(
                    FolderError.FLD_UNABLE_TO_PERFORM_DELETE.name(), e);
        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteFolder action end."));
        }
    }
}