/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

public class DeleteAdminApprovedSendersList implements MxOSBaseAction {

    private static Logger logger = Logger
            .getLogger(DeleteAdminApprovedSendersList.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "DeleteAdminApprovedSendersList action start."));
            }
            try {
                String adminApprovedSender = requestState.getInputParams()
                        .get(MailboxProperty.adminApprovedSendersList.name())
                        .get(0);
                List<String> existingAdminApprovedSendersList = (List<String>) requestState
                        .getDbPojoMap().getPropertyAsObject(
                                MxOSPOJOs.adminApprovedSendersList);
                if (existingAdminApprovedSendersList == null
                        || existingAdminApprovedSendersList.size() < 1
                        || !existingAdminApprovedSendersList
                                .contains(adminApprovedSender.toLowerCase())) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_ADMIN_APPROVED_SENDER_NOT_EXIST
                                    .name(),
                            "admin Approved sender does not exist");
                }
                existingAdminApprovedSendersList.remove(adminApprovedSender
                        .toLowerCase());
                final String[] adminApprovedSendersListToArray = existingAdminApprovedSendersList
                        .toArray(new String[existingAdminApprovedSendersList
                                .size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.adminApprovedSendersList,
                                adminApprovedSendersListToArray);
            } catch (MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error(
                        "Error while deleting admin Approved sender for delete",
                        e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_DELETE
                                .name(), e);
            } catch (final Throwable t) {
                logger.error(
                        "Internal Error while deleting admin Approved sender",
                        t);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "DeleteAdminApprovedSendersList action end."));
            }
        } else {
            logger.error("Error while calling deleteadminapprovedsenderslist, group mailbox feature is disabled.");
            throw new InvalidRequestException(
                    MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
        }

    }

}
