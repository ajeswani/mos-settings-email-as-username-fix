package com.opwvmsg.mxos.backend.crud;

import java.util.List;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

import org.apache.log4j.Logger;

/**
 * This class provides static methods to handle CRUD level data handling.
 * 
 * @author niwakami
 */
public class CRUDUtils {
    private static Logger logger = Logger.getLogger(CRUDUtils.class);

    /**
     * Get IMetaCRUD pool for the given protocol stored in the requestState object.   
     * @param requestState input request state.  The object must include necessary information for resolving protocol. 
     * @return IMetaCRUD pool object.
     */
    public static ICRUDPool<IMetaCRUD> getMetaCRUDPool(MxOSRequestState requestState) throws MxOSException {
        CRUDProtocol protocol = getCRUDProtocol(requestState);
        return MxOSApp.getInstance().getMetaCRUD(protocol);
    }
    
    public static IBlobCRUD getBlobCRUD(MxOSRequestState requestState) throws MxOSException {
        CRUDProtocol protocol = getCRUDProtocol(requestState);
        return MxOSApp.getInstance().getBlobCRUD(protocol);
    }
    
    /**
     * Gets CRUD backend protocol from the given requestState object.  If the protocol information is missing
     * in the object, this method tries to resolve it from the given information. 
     * @param requestState input request state
     * @return CRUD backend protocol
     */
    public static CRUDProtocol getCRUDProtocol(MxOSRequestState requestState) throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) requestState.getDbPojoMap().getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        return getCRUDProtocol(info);
    }
    
    public static CRUDProtocol getCRUDProtocol(MssLinkInfo info) throws MxOSException {
        CRUDProtocol protocol = (CRUDProtocol)info.getAdditionalProperties().get(CRUDProtocol.class.getName());
        if (protocol == null) {
            protocol = resolveBackendProtocol(info);
        }
        return protocol;
    }
    
    /**
     * Stores CRUD backend protocol to the MssLinkInfo object.
     * 
     * @param backendProtocol the backend protocol.
     * @param mssLinkInfo the MssLinkInfo object to store the value.
     */
    public static void storeBackendProtocol(final CRUDProtocol backendProtocol, MssLinkInfo mssLinkInfo) {
        mssLinkInfo.setAdditionalProperties(CRUDProtocol.class.getName(), backendProtocol);
    }

    /**
     * Resolve the CRUD backend protocol and put the result into the given mssLinkInfo object.
     * Necessary information for resolving should be availalbe in the input object.
     * 
     * @param mssLinkInfo exchanges the information.  NOTE: This method would put the resolved protocol value to this object.
     * @throws MxOSException when necessary information for resolving is not availalbe in the mssLinkInfo.
     */
    public static CRUDProtocol resolveBackendProtocol(MssLinkInfo mssLinkInfo) throws MxOSException {
        
        CRUDProtocol protocol = null;
        if (MxOSApp.getInstance().getMetaDBType() == MetaDBTypes.mss) {
            List<String> hosts = mssLinkInfo.getMessageStoreHosts();
            if (hosts == null) {
                throw new MxOSException(ErrorCode.GEN_INVALID_DATA.name(), "Hosts information is not availalbe in the given mssLinkInfo.");
            }
            String host = hosts.get(0);
            if (host == null || host.isEmpty()) {
                throw new MxOSException(ErrorCode.GEN_INVALID_DATA.name(), "Host name is empty.");
            }
            protocol = resolveBackendProtocolByHostName(host);
        }
        else {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), "Unknown MetaDB type: " + MxOSApp.getInstance().getMetaDBType());
        }
        
        if (logger.isDebugEnabled()) {
            logger.debug("CRUDProtocol = " + protocol);
        }
        storeBackendProtocol(protocol, mssLinkInfo);
        return protocol;
    }
    
    public static CRUDProtocol resolveBackendProtocolByHostName(final String host) {
        String serviceProtocol = MxOSConfig.getString(host, "mss", "serviceProtocol", "rme");
        CRUDProtocol protocol = CRUDProtocol.rme;
        if (serviceProtocol.equals("httprme")) {
            protocol = CRUDProtocol.httprme;
        }
        return protocol;
    }
}
