/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to get AdminControl object.
 * 
 * @author mxos-dev
 * 
 */
public class GetAdminControl implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetAdminControl.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("GetAdminControl action start."));
            }
            ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;
            ICRUDPool<IMetaCRUD> metaCRUDPool = null;
            IMetaCRUD metaCRUD = null;

            try {
                mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                final String email = requestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0);
                final AdminControl adminControl = mailboxCRUD
                        .readAdminControl(email);
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.adminControl,
                        adminControl);
            } catch (final MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error("Error while get admin control.", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_ADMINCONTROL_GET.name(), e);
            } catch (final Throwable t) {
                logger.error("Error while get admin control.", t);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
            } finally {
                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    try {
                        mailboxCRUDPool.returnObject(mailboxCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                "Error in finally clause while returing "
                                        + "Mailbox CRUD pool:" + e.getMessage());
                    }
                }
                if (metaCRUDPool != null && metaCRUD != null) {
                    try {
                        metaCRUDPool.returnObject(metaCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                "Error in finally clause while returing "
                                        + "Meta CRUD pool:" + e.getMessage());
                    }
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("GetAdminControl action end."));
            }
        } else {
            logger.error("Error while get admin control, group mailbox feature is disabled.");
            throw new InvalidRequestException(
                    MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
        }
    }
}
