/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.we;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.we.utils.EnumConverter;
import com.opwvmsg.mxos.backend.crud.we.utils.Filter;
import com.opwvmsg.mxos.backend.crud.we.utils.Query;
import com.opwvmsg.mxos.backend.crud.we.utils.Sort;
import com.opwvmsg.mxos.backend.crud.we.utils.Visitor;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums.MaritalStatus;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.enums.WEContactsProperty;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.ApplicationException;

/**
 * Class to implement methods WET specific methods for communicating and parsing
 * the response
 * 
 * @author mxos-dev
 * 
 */
public class WEAddressBookCRUD implements IAddressBookCRUD {

    enum Action {
        CREATE_CONTACT, CREATE_MULTIPLE_CONTACTS, UPDATE_CONTACT, DELETE_CONTACT, CREATE_GROUP, CREATE_MULTIPLE_GROUPS, UPDATE_GROUP_BASE, ADD_GROUP_MEMBER, DELETE_GROUP_MEMBER, DELETE_GROUP, DELETE_ALL_GROUP;
    }

    enum ImProvider implements EnumConverter {
        aim("aol.com"), icq("icq.com"), msn("msn.com"), yim("yahoo.com");

        private final String value;

        ImProvider(String value) {
            this.value = value;
        }

        @Override
        public String convert() {
            return value;
        }
    }

    private static final String LINE_REGEXP = "\\r?\\n";

    private static final String FIELD_REGEXP = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

    private static Logger logger = Logger.getLogger(WEAddressBookCRUD.class);

    private static final String generateContactId(final String userId) {
        long uid = UUID.randomUUID().getLeastSignificantBits() & (-1L >>> 32);
        return Long.toString(System.currentTimeMillis() + uid);
    }

    @Override
    public void commit() throws ApplicationException {
        // Not supported for WE
    }

    @Override
    public String createContact(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();
        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        // Breakup incoming IM Address to BuddyName and ImProvider
        String imEmailAddress = null;
        if (inputParam.get(AddressBookProperty.personalInfoImAddress.name()) != null) {
            imEmailAddress = inputParam.get(
                    AddressBookProperty.personalInfoImAddress.name()).get(0);
        } else if (inputParam.get(AddressBookProperty.imAddress.name()) != null) {
            imEmailAddress = inputParam.get(
                    AddressBookProperty.imAddress.name()).get(0);
        }

        if (imEmailAddress != null && !imEmailAddress.isEmpty()) {

            if (WEAddressBookHelper.getImProviderFromEmail(imEmailAddress) != null) {
                attributes.put(WEContactsProperty.BuddyName.name(),
                        WEAddressBookHelper
                                .getBuddyNameFromEmail(imEmailAddress));

                attributes.put(
                        WEContactsProperty.ImProvider.name(),
                        WEAddressBookHelper.getImProviderFromEmail(
                                imEmailAddress).name());
            }
        }

        attributes.put(
                WEContactsProperty.Id.name(),
                generateContactId(inputParam.get(
                        AddressBookProperty.userId.name()).get(0)));

        attributes.put(WEContactsProperty.EntryType.name(),
                MxOSConstants.WE_CONTACT_ENTRY_TYPE);

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.CREATE_CONTACT);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);

        return attributes.get(WEContactsProperty.Id.name());
    }

    @Override
    public String createGroup(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();
        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        attributes.put(
                WEContactsProperty.Id.name(),
                generateContactId(inputParam.get(
                        AddressBookProperty.userId.name()).get(0)));

        attributes.put(
                WEContactsProperty.Id.name(),
                generateContactId(inputParam.get(
                        AddressBookProperty.userId.name()).get(0)));
        attributes.put(WEContactsProperty.EntryType.name(),
                MxOSConstants.WE_GROUP_ENTRY_TYPE);

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.CREATE_CONTACT);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);

        return attributes.get(WEContactsProperty.Id.name());
    }

    @Override
    public String createGroupExternalMembers(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
        return null;
    }

    @Override
    public String createGroupMembers(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();
        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        String inputMemberId = inputParam.get(
                AddressBookProperty.memberId.name()).get(0);
        attributes.put(WEContactsProperty.listMembers.name(), inputMemberId);

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.ADD_GROUP_MEMBER);

        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);

        return attributes.get(AddressBookProperty.memberId.name());
    }

    @Override
    public void deleteAllGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = new HashMap<String, String>();

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.DELETE_ALL_GROUP);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);
    }

    @Override
    public void deleteAllGroupsExternalMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
    }

    @Override
    public void deleteAllGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        attributes.put(WEContactsProperty.listMembers.name(), null);

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.DELETE_GROUP_MEMBER);

        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);
    }

    @Override
    public void deleteContact(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.DELETE_CONTACT);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);
    }

    @Override
    public void deleteGroup(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.DELETE_GROUP);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);
    }

    @Override
    public void deleteGroupsExternalMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
    }

    @Override
    public void deleteGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();
        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        attributes.put(WEContactsProperty.listMembers.name(),
                inputParam.get(AddressBookProperty.memberId.name()).get(0));

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.DELETE_GROUP_MEMBER);

        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);
    }

    private String enquote(String value) {
        if (value != null) {
            if (value.contains(MxOSConstants.DOUBLE_QUOTE)) {
                value = value.replaceAll(MxOSConstants.DOUBLE_QUOTE,
                        MxOSConstants.ESCAPED_DOUBLE_QUOTE);
            }
            if (value.contains(MxOSConstants.COMMA)) {
                // escape the value before storing it
                value = new StringBuilder().append(MxOSConstants.DOUBLE_QUOTE)
                        .append(value).append(MxOSConstants.DOUBLE_QUOTE)
                        .toString();
            }
        }
        return value;
    }

    /**
     * This method parses the incoming text i.e. multiline input into
     * List<Map<String, String>, where the list is for each row stored as a map
     * of comma separated values with header name as key.
     * 
     * @param attributes
     * @param originalText
     * @return
     * @throws AddressBookException
     */
    private String generateAutoReplyText(
            List<Map<String, String>> attributesList, String originalText,
            Action action) throws AddressBookException {

        // 1. Read existing contacts
        List<Map<String, String>> rowList = readExistingContacts(originalText,
                null, null, null, null);

        // 2. Perform appropriate action on contacts
        Map<String, String> attributes = attributesList != null ? attributesList
                .get(0) : null;

        switch (action) {
        case CREATE_CONTACT:
        case CREATE_GROUP:
            Map<String, String> newRow = new TreeMap<String, String>();
            for (WEContactsProperty key : WEContactsProperty.values()) {
                if (attributes.containsKey(key.name())) {
                    newRow.put(key.toString(), attributes.get(key.name()));
                } else {
                    newRow.put(key.toString(), "");
                }
            }
            // Add new contact
            rowList.add(newRow);
            break;
        case CREATE_MULTIPLE_CONTACTS:
        case CREATE_MULTIPLE_GROUPS:
            for (Map<String, String> attributesRow : attributesList) {
                Map<String, String> newAttributesRow = new TreeMap<String, String>();
                for (WEContactsProperty key : WEContactsProperty.values()) {
                    if (attributes.containsKey(key.name())) {
                        newAttributesRow.put(key.toString(),
                                attributesRow.get(key.name()));
                    } else {
                        newAttributesRow.put(key.toString(), "");
                    }
                }
                // Add new contact
                rowList.add(newAttributesRow);
            }
            break;
        case UPDATE_CONTACT:
            String contactId = attributes.get(WEContactsProperty.Id.name());

            // Check if the contact id is a valid CONTACT Id
            if (!validate(rowList, contactId,
                    MxOSConstants.WE_CONTACT_ENTRY_TYPE)) {
                throw new AddressBookException(
                        AddressBookError.ABS_CONTACT_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_CONTACT_NOT_FOUND.name(), "");
            }

            for (Map<String, String> row : rowList) {
                if (contactId.equalsIgnoreCase(row.get(WEContactsProperty.Id
                        .name()))) {

                    // Update values
                    for (String key : attributes.keySet()) {
                        row.put(key, attributes.get(key));
                    }
                }
            }
            break;
        case UPDATE_GROUP_BASE:
            String groupId = attributes.get(WEContactsProperty.Id.name());

            // Check if the group id is a valid GROUP Id
            if (!validate(rowList, groupId, MxOSConstants.WE_GROUP_ENTRY_TYPE)) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUP_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUP_NOT_FOUND.name(), "");
            }

            for (Map<String, String> row : rowList) {
                if (groupId.equalsIgnoreCase(row.get(WEContactsProperty.Id
                        .name()))) {

                    // Update values
                    for (String key : attributes.keySet()) {
                        if (key.equalsIgnoreCase(WEContactsProperty.listName
                                .name())) {
                            row.put(WEContactsProperty.listName.toString(),
                                    attributes.get(key));
                        } else if (key
                                .equalsIgnoreCase(WEContactsProperty.listDescription
                                        .name())) {
                            row.put(WEContactsProperty.listDescription
                                    .toString(), attributes.get(key));
                        } else {
                            row.put(key, attributes.get(key));
                        }
                    }
                }
            }
            break;
        case DELETE_GROUP:
            String delGroupId = attributes.get(WEContactsProperty.Id.name());

            // Check if the group id is a valid GROUP Id
            if (!validate(rowList, delGroupId,
                    MxOSConstants.WE_GROUP_ENTRY_TYPE)) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUP_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUP_NOT_FOUND.name(), "");
            }

            int idx = 0;
            for (Map<String, String> row : rowList) {
                if (delGroupId.equalsIgnoreCase(row.get(WEContactsProperty.Id
                        .name()))) {
                    break;
                }
                idx++;
            }

            rowList.remove(idx);
            break;
        case DELETE_ALL_GROUP:

            List<Integer> allgroupIndexes = new ArrayList<Integer>();
            int mapIndex = 0;

            // Fetch indexes of groups
            for (Map<String, String> row : rowList) {
                if (row.get(WEContactsProperty.EntryType.toString())
                        .equalsIgnoreCase(MxOSConstants.WE_GROUP_ENTRY_TYPE)) {
                    allgroupIndexes.add(Integer.valueOf(mapIndex));
                }
                mapIndex++;
            }

            // Remove all the groups from the list and start removing
            // from the rowList from the last value
            for (int i = allgroupIndexes.size() - 1; i >= 0; i--) {
                rowList.remove(allgroupIndexes.get(i).intValue());
            }
            break;

        case ADD_GROUP_MEMBER:
            String addMemberId = attributes.get(WEContactsProperty.listMembers
                    .name());

            // Check if the member id is found
            if (!validate(rowList, addMemberId,
                    MxOSConstants.WE_CONTACT_ENTRY_TYPE)) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(), "");
            }

            for (Map<String, String> row : rowList) {
                if (attributes
                        .get(WEContactsProperty.Id.name())
                        .equalsIgnoreCase(row.get(WEContactsProperty.Id.name()))) {

                    // Update values
                    for (String key : attributes.keySet()) {
                        if (key.equalsIgnoreCase(WEContactsProperty.listMembers
                                .name())) {

                            String members = row
                                    .get(WEContactsProperty.listMembers
                                            .toString());

                            // Check if the member id already exists
                            String[] values = members.split(";");

                            for (String value : values) {
                                if (value.equalsIgnoreCase(addMemberId)) {
                                    throw new AddressBookException(
                                            AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                                    .name(),
                                            ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                                            AddressBookError.ABS_GROUPS_MEMBER_ALREADY_EXISTS
                                                    .name(), "");
                                }

                            }

                            StringBuilder str = new StringBuilder();
                            if (!"".equals(members)) {
                                str.append(members);
                                str.append(";");
                            }
                            str.append(addMemberId);
                            row.put(WEContactsProperty.listMembers.toString(),
                                    str.toString());
                        }
                    }
                }
            }
            break;

        case DELETE_CONTACT:
            String delContactId = attributes.get(WEContactsProperty.Id.name());

            // Check if the contact id is a valid CONTACT Id
            if (!validate(rowList, delContactId,
                    MxOSConstants.WE_CONTACT_ENTRY_TYPE)) {
                throw new AddressBookException(
                        AddressBookError.ABS_CONTACT_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_CONTACT_NOT_FOUND.name(), "");
            }

            int index = 0;
            for (Map<String, String> row : rowList) {
                if (delContactId.equalsIgnoreCase(row.get(WEContactsProperty.Id
                        .name()))) {
                    break;
                }
                index++;
            }

            rowList.remove(index);
            // fall through to delete member from all groups
            attributes.put(WEContactsProperty.listMembers.name(), delContactId);

        case DELETE_GROUP_MEMBER:

            if (action != Action.DELETE_CONTACT
                    && null != attributes.get(WEContactsProperty.listMembers
                            .name())) {
                String deleteMemberId = attributes
                        .get(WEContactsProperty.listMembers.name());

                // Check if the member id is found
                if (!validate(rowList, deleteMemberId,
                        MxOSConstants.WE_CONTACT_ENTRY_TYPE)) {
                    throw new AddressBookException(
                            AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                            ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                            AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                            "");
                }

            }
            for (Map<String, String> row : rowList) {
                if ((action == Action.DELETE_CONTACT || attributes.get(
                        WEContactsProperty.Id.name()).equalsIgnoreCase(
                        row.get(WEContactsProperty.Id.name())))) {

                    // only sort through list entries
                    if (row.get(WEContactsProperty.EntryType.toString())
                            .equals(MxOSConstants.WE_GROUP_ENTRY_TYPE)) {
                        // Update values
                        for (String key : attributes.keySet()) {
                            if (key.equalsIgnoreCase(WEContactsProperty.listMembers
                                    .name())) {

                                String memberIds = row
                                        .get(WEContactsProperty.listMembers
                                                .toString());
                                StringBuilder newMemberString = new StringBuilder(
                                        "");

                                String deleteMemberId = attributes
                                        .get(WEContactsProperty.listMembers
                                                .name());

                                // DeletAll will have the deleteMemberId = null
                                if (null != deleteMemberId) {

                                    boolean memberPresent = false;

                                    String[] values = memberIds.split(";");
                                    for (String value : values) {
                                        if (!value
                                                .equalsIgnoreCase(deleteMemberId)) {
                                            newMemberString.append(value + ";");
                                        } else {
                                            memberPresent = true;
                                        }
                                    }

                                    if (action == Action.DELETE_GROUP_MEMBER
                                            && !memberPresent) {
                                        throw new AddressBookException(
                                                AddressBookError.ABS_GROUPS_MEMBER_DOES_NOT_EXIST
                                                        .name(),
                                                ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                                                AddressBookError.ABS_GROUPS_MEMBER_DOES_NOT_EXIST
                                                        .name(), "");
                                    }

                                    if ("".equals(newMemberString.toString())) {
                                        row.put(WEContactsProperty.listMembers
                                                .toString(), newMemberString
                                                .toString());
                                    } else {// Remove the last ; added
                                        row.put(WEContactsProperty.listMembers
                                                .toString(),
                                                newMemberString
                                                        .substring(
                                                                0,
                                                                newMemberString
                                                                        .lastIndexOf(";"))
                                                        .toString());
                                    }
                                } else {
                                    // DeletAll
                                    row.put(WEContactsProperty.listMembers
                                            .toString(), newMemberString
                                            .toString());
                                }
                            }
                        }
                    }
                }
            }
            break;
        }

        // 3. Update the contacts
        String delim = "";
        StringBuilder builder = new StringBuilder();
        for (WEContactsProperty weAttribute : WEContactsProperty.values()) {
            builder.append(delim).append(weAttribute.toString());
            delim = MxOSConstants.COMMA;
        }
        builder.append("\n");

        for (Map<String, String> rowMap : rowList) {
            delim = "";
            for (WEContactsProperty weAttribute : WEContactsProperty.values()) {
                String value = "";
                value = rowMap.get(weAttribute.toString());
                if (value != null) {
                    builder.append(delim).append(enquote(value));
                } else {
                    builder.append(delim);
                }
                delim = MxOSConstants.COMMA;
            }
            builder.append("\n");
        }

        return builder.toString();
    }

    @Override
    public ExternalSession login(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
        return null;
    }

    private List<Member> populateGroupMembers(String member)
            throws AddressBookException {
        List<Member> memberList = new ArrayList<Member>();

        if (null != member && !"".equals(member)) {
            String[] memberIDArray = member.split(";");

            for (String value : memberIDArray) {

                Member memberEntry = new Member();
                memberEntry.setMemberId(value);
                memberList.add(memberEntry);

            }
        }
        return memberList;
    }

    @Override
    public List<ContactBase> readAllContactBase(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        Map<String, List<String>> inputParams = mxosRequestState
                .getInputParams();

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        List<ContactBase> contacts = new ArrayList<ContactBase>();

        List<Map<String, String>> rows = readAllEntries(attributes,
                inputParams, WEAddressBookHelper.getAutoReplyText(
                        autoReplyHost, mailboxId, realm), false);
        for (Map<String, String> row : rows) {
            ContactBase base = new ContactBase();
            base.setContactId(row.get(WEContactsProperty.Id.name()));
            if (!StringUtils.isEmpty(row.get(WEContactsProperty.Notes.name())))
                base.setNotes(row.get(WEContactsProperty.Notes.name()));
            if (!StringUtils.isEmpty(row.get(WEContactsProperty.Pager.name())))
                base.setCustomFields(row.get(WEContactsProperty.Pager.name()));
            contacts.add(base);
        }
        return contacts;
    }

    /**
     * This method parses the incoming text i.e. multiline input into
     * List<Map<String, String>, where the list is for each row stored as a map
     * of comma separated values with header name as key.
     * 
     * @param attributes
     * @param originalText
     * @return
     * @throws AddressBookException
     */
    private List<Map<String, String>> readAllEntries(
            Map<String, String> attributes,
            Map<String, List<String>> inputParams, String originalText,
            boolean groupsOnly) throws AddressBookException {

        String queryString = inputParams.get(AddressBookProperty.query.name()) != null ? inputParams
                .get(AddressBookProperty.query.name()).get(0) : null;
        String sortKey = inputParams.get(AddressBookProperty.sortKey.name()) != null ? inputParams
                .get(AddressBookProperty.sortKey.name()).get(0) : null;
        String sortOrder = inputParams
                .get(AddressBookProperty.sortOrder.name()) != null ? inputParams
                .get(AddressBookProperty.sortOrder.name()).get(0) : null;
        String filter = inputParams.get(AddressBookProperty.filter.name()) != null ? inputParams
                .get(AddressBookProperty.filter.name()).get(0) : null;

        // Read existing contacts
        List<Map<String, String>> rowList = readExistingContacts(originalText,
                queryString, sortKey, sortOrder, filter);

        // List to be returned, it will never be null
        List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();

        String entryType = groupsOnly ? MxOSConstants.WE_GROUP_ENTRY_TYPE
                : MxOSConstants.WE_CONTACT_ENTRY_TYPE;

        for (Map<String, String> row : rowList) {
            // Return the contact with correct Id
            if (entryType.equalsIgnoreCase(row.get(WEContactsProperty.EntryType
                    .toString()))) {
                entryList.add(row);
            }
        }
        return entryList;
    }

    @Override
    public List<GroupBase> readAllGroupBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = new HashMap<String, String>();
        Map<String, List<String>> inputParams = mxosRequestState
                .getInputParams();

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        List<GroupBase> groups = new ArrayList<GroupBase>();

        List<Map<String, String>> rows = readAllEntries(attributes,
                inputParams, WEAddressBookHelper.getAutoReplyText(
                        autoReplyHost, mailboxId, realm), true);
        for (Map<String, String> row : rows) {
            GroupBase base = new GroupBase();
            base.setGroupId(row.get(WEContactsProperty.Id.name()));
            if (!StringUtils.isEmpty(row.get(WEContactsProperty.listName
                    .toString()))) {
                base.setGroupName(row.get(WEContactsProperty.listName
                        .toString()));
            }
            groups.add(base);
        }
        return groups;
    }

    @Override
    public ContactBase readContactBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));
        ContactBase base = new ContactBase();
        base.setContactId(map.get(WEContactsProperty.Id.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Notes.name())))
            base.setNotes(map.get(WEContactsProperty.Notes.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Pager.name())))
            base.setCustomFields(map.get(WEContactsProperty.Pager.name()));
        return base;
    }

    @Override
    public Integer readContactFolderId(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
        return null;
    }

    @Override
    public Name readContactName(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));
        Name name = new Name();
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.DisplayName.name())))
            name.setDisplayName(map.get(WEContactsProperty.DisplayName.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.FirstName.name())))
            name.setFirstName(map.get(WEContactsProperty.FirstName.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.LastName.name())))
            name.setLastName(map.get(WEContactsProperty.LastName.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.MiddleName.name())))
            name.setMiddleName(map.get(WEContactsProperty.MiddleName.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Nickname.name())))
            name.setNickName(map.get(WEContactsProperty.Nickname.name()));

        return name;
    }

    @Override
    public List<Event> readContactPersonalInfoEvents(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));

        List<Event> eventList = new ArrayList<Event>();

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone
                .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));

        if ((null != map.get(WEContactsProperty.AnniversaryDay.name()) && !""
                .equals(map.get(WEContactsProperty.AnniversaryDay.name())))
                || (null != map.get(WEContactsProperty.AnniversaryMonth.name()) && !""
                        .equals(map.get(WEContactsProperty.AnniversaryMonth
                                .name())))
                || (null != map.get(WEContactsProperty.AnniversaryYear.name()) && !""
                        .equals(map.get(WEContactsProperty.AnniversaryYear
                                .name())))) {

            Event anniversary = new Event();
            anniversary.setType(Event.Type.ANNIVERSARY);

            int day = Integer.parseInt(map
                    .get(WEContactsProperty.AnniversaryDay.name()));

            int month = Integer.parseInt(map
                    .get(WEContactsProperty.AnniversaryMonth.name()));
            int year = Integer.parseInt(map
                    .get(WEContactsProperty.AnniversaryYear.name()));

            GregorianCalendar anniversaryCal = new GregorianCalendar(year,
                    month - 1, day);
            anniversaryCal.setTimeZone(TimeZone
                    .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));

            anniversary.setDate(simpleDateFormat.format(anniversaryCal
                    .getTime()));
            eventList.add(anniversary);

        }

        if ((null != map.get(WEContactsProperty.BirthdayDay.name()) && !""
                .equals(map.get(WEContactsProperty.BirthdayDay.name())))
                || (null != map.get(WEContactsProperty.BirthdayMonth.name()) && !""
                        .equals(map.get(WEContactsProperty.BirthdayMonth.name())))
                || (null != map.get(WEContactsProperty.BirthdayYear.name()) && !""
                        .equals(map.get(WEContactsProperty.BirthdayYear.name())))) {

            Event birthday = new Event();
            birthday.setType(Event.Type.BIRTHDAY);

            int day = Integer.parseInt(map.get(WEContactsProperty.BirthdayDay
                    .name()));

            int month = Integer.parseInt(map
                    .get(WEContactsProperty.BirthdayMonth.name()));
            int year = Integer.parseInt(map.get(WEContactsProperty.BirthdayYear
                    .name()));

            GregorianCalendar birthdayCal = new GregorianCalendar(year,
                    month - 1, day);
            birthdayCal.setTimeZone(TimeZone
                    .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));

            birthday.setDate(simpleDateFormat.format(birthdayCal.getTime()));
            eventList.add(birthday);

        }

        return eventList;
    }

    @Override
    public byte[] readContactsActualImage(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
        return null;
    }

    @Override
    public Image readContactsImage(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
        return null;
    }

    @Override
    public PersonalInfo readContactsPersonalInfo(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));

        PersonalInfo personalInfo = new PersonalInfo();
        String spouseName = map.get(WEContactsProperty.Spouse.name());
        if (spouseName != null && !StringUtils.isEmpty(spouseName)
                && !spouseName.isEmpty()) {
            personalInfo.setMaritalStatus(MaritalStatus.MARRIED);
            personalInfo.setSpouseName(spouseName);
        }

        String url = map.get(WEContactsProperty.HomeUrl.name());
        if (url != null && !url.isEmpty() && !StringUtils.isEmpty(url)) {
            personalInfo.setHomeWebpage(url);
        }

        String children = map.get(WEContactsProperty.Children.name());
        if (children != null && !children.isEmpty()
                && !StringUtils.isEmpty(children)) {
            personalInfo.setChildren(children);
        }

        Address homeAddress = new Address();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeCity.name())))
            homeAddress.setCity(map.get(WEContactsProperty.HomeCity.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeState.name())))
            homeAddress.setStateOrProvince(map.get(WEContactsProperty.HomeState
                    .name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.HomeCountry.name())))
            homeAddress.setCountry(map.get(WEContactsProperty.HomeCountry
                    .name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeZip.name())))
            homeAddress
                    .setPostalCode(map.get(WEContactsProperty.HomeZip.name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.HomeAddress1.name())))
            homeAddress.setUnstructuredAddress(map
                    .get(WEContactsProperty.HomeAddress1.name()));
        personalInfo.setAddress(homeAddress);

        Communication homeCommunication = new Communication();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.AltEmail.name())))
            homeCommunication.setEmail(map.get(WEContactsProperty.AltEmail
                    .name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeFax.name())))
            homeCommunication
                    .setFax(map.get(WEContactsProperty.HomeFax.name()));
        if (map.get(WEContactsProperty.BuddyName.name()) != null) {
            String buddyName = "";
            String imProvider = "";

            if (!StringUtils.isEmpty(map.get(WEContactsProperty.BuddyName
                    .name())))
                buddyName = map.get(WEContactsProperty.BuddyName.name());
            if (!StringUtils.isEmpty(map.get(WEContactsProperty.ImProvider
                    .name())))
                imProvider = map.get(WEContactsProperty.ImProvider.name());
            if (StringUtils.isEmpty(buddyName)) {
                if (!StringUtils.isEmpty(imProvider))
                    homeCommunication.setImAddress(imProvider);
            } else {
                if (!StringUtils.isEmpty(WEAddressBookHelper
                        .getEmailFromBuddyNameAndImProvider(buddyName,
                                imProvider)))
                    homeCommunication.setImAddress(WEAddressBookHelper
                            .getEmailFromBuddyNameAndImProvider(buddyName,
                                    imProvider));
            }
        } else {
            if (!StringUtils.isEmpty(map.get(WEContactsProperty.ImProvider
                    .name())))
                homeCommunication.setImAddress(map
                        .get(WEContactsProperty.ImProvider.name()));
        }
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.MobilePhone.name())))
            homeCommunication.setMobile(map.get(WEContactsProperty.MobilePhone
                    .name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomePhone.name())))
            homeCommunication.setPhone1(map.get(WEContactsProperty.HomePhone
                    .name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.OtherPhone.name())))
            homeCommunication.setPhone2(map.get(WEContactsProperty.OtherPhone
                    .name()));
        personalInfo.setCommunication(homeCommunication);

        // Event
        List<Event> eventSet = new ArrayList<Event>();

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone
                .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));

        if ((null != map.get(WEContactsProperty.AnniversaryDay.name()) && !""
                .equals(map.get(WEContactsProperty.AnniversaryDay.name())))
                || (null != map.get(WEContactsProperty.AnniversaryMonth.name()) && !""
                        .equals(map.get(WEContactsProperty.AnniversaryMonth
                                .name())))
                || (null != map.get(WEContactsProperty.AnniversaryYear.name()) && !""
                        .equals(map.get(WEContactsProperty.AnniversaryYear
                                .name())))) {

            Event anniversary = new Event();
            anniversary.setType(Event.Type.ANNIVERSARY);

            int day = Integer.parseInt(map
                    .get(WEContactsProperty.AnniversaryDay.name()));

            int month = Integer.parseInt(map
                    .get(WEContactsProperty.AnniversaryMonth.name()));
            int year = Integer.parseInt(map
                    .get(WEContactsProperty.AnniversaryYear.name()));

            GregorianCalendar anniversaryCal = new GregorianCalendar(year,
                    month - 1, day);
            anniversaryCal.setTimeZone(TimeZone
                    .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));

            anniversary.setDate(simpleDateFormat.format(anniversaryCal
                    .getTime()));
            eventSet.add(anniversary);

        }

        if ((null != map.get(WEContactsProperty.BirthdayDay.name()) && !""
                .equals(map.get(WEContactsProperty.BirthdayDay.name())))
                || (null != map.get(WEContactsProperty.BirthdayMonth.name()) && !""
                        .equals(map.get(WEContactsProperty.BirthdayMonth.name())))
                || (null != map.get(WEContactsProperty.BirthdayYear.name()) && !""
                        .equals(map.get(WEContactsProperty.BirthdayYear.name())))) {

            Event birthday = new Event();
            birthday.setType(Event.Type.BIRTHDAY);

            int day = Integer.parseInt(map.get(WEContactsProperty.BirthdayDay
                    .name()));

            int month = Integer.parseInt(map
                    .get(WEContactsProperty.BirthdayMonth.name()));
            int year = Integer.parseInt(map.get(WEContactsProperty.BirthdayYear
                    .name()));

            GregorianCalendar birthdayCal = new GregorianCalendar(year,
                    month - 1, day);
            birthdayCal.setTimeZone(TimeZone
                    .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));

            birthday.setDate(simpleDateFormat.format(birthdayCal.getTime()));
            eventSet.add(birthday);

        }

        personalInfo.setEvents(eventSet);

        return personalInfo;
    }

    @Override
    public Address readContactsPersonalInfoAddress(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));
        Address address = new Address();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeCity.name())))
            address.setCity(map.get(WEContactsProperty.HomeCity.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeState.name())))
            address.setStateOrProvince(map.get(WEContactsProperty.HomeState
                    .name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.HomeCountry.name())))
            address.setCountry(map.get(WEContactsProperty.HomeCountry.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeZip.name())))
            address.setPostalCode(map.get(WEContactsProperty.HomeZip.name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.HomeAddress1.name())))
            address.setUnstructuredAddress(map
                    .get(WEContactsProperty.HomeAddress1.name()));
        return address;
    }

    @Override
    public Communication readContactsPersonalInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));

        Communication communication = new Communication();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.AltEmail.name())))
            communication.setEmail(map.get(WEContactsProperty.AltEmail.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomeFax.name())))
            communication.setFax(map.get(WEContactsProperty.HomeFax.name()));
        if (map.get(WEContactsProperty.BuddyName.name()) != null) {
            String buddyName = "";
            String imProvider = "";
            if (!StringUtils.isEmpty(map.get(WEContactsProperty.BuddyName
                    .name())))
                buddyName = map.get(WEContactsProperty.BuddyName.name());
            if (!StringUtils.isEmpty(map.get(WEContactsProperty.ImProvider
                    .name())))
                imProvider = map.get(WEContactsProperty.ImProvider.name());
            if (StringUtils.isEmpty(buddyName)) {
                if (!StringUtils.isEmpty(imProvider))
                    communication.setImAddress(imProvider);
            } else {
                if (!StringUtils.isEmpty(WEAddressBookHelper
                        .getEmailFromBuddyNameAndImProvider(buddyName,
                                imProvider)))
                    communication.setImAddress(WEAddressBookHelper
                            .getEmailFromBuddyNameAndImProvider(buddyName,
                                    imProvider));
            }
        } else {
            if (!StringUtils.isEmpty(map.get(WEContactsProperty.ImProvider
                    .name())))
                communication.setImAddress(map
                        .get(WEContactsProperty.ImProvider.name()));
        }
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.MobilePhone.name())))
            communication.setMobile(map.get(WEContactsProperty.MobilePhone
                    .name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.HomePhone.name())))
            communication
                    .setPhone1(map.get(WEContactsProperty.HomePhone.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.OtherPhone.name())))
            communication.setPhone2(map.get(WEContactsProperty.OtherPhone
                    .name()));
        return communication;
    }

    @Override
    public WorkInfo readContactsWorkInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));

        WorkInfo workInfo = new WorkInfo();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Company.name())))
            workInfo.setCompanyName(map.get(WEContactsProperty.Company.name()));

        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Department.name())))
            workInfo.setDepartment(map.get(WEContactsProperty.Department.name()));

        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Title.name())))
            workInfo.setTitle(map.get(WEContactsProperty.Title.name()));

        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkUrl.name())))
            workInfo.setWebPage(map.get(WEContactsProperty.WorkUrl.name()));

        Address workAddress = new Address();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkCity.name())))
            workAddress.setCity(map.get(WEContactsProperty.WorkCity.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkState.name())))
            workAddress.setStateOrProvince(map.get(WEContactsProperty.WorkState
                    .name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.WorkCountry.name())))
            workAddress.setCountry(map.get(WEContactsProperty.WorkCountry
                    .name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkZip.name())))
            workAddress
                    .setPostalCode(map.get(WEContactsProperty.WorkZip.name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.WorkAddress1.name())))
            workAddress.setUnstructuredAddress(map
                    .get(WEContactsProperty.WorkAddress1.name()));

        workInfo.setAddress(workAddress);

        Communication workCommunication = new Communication();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Email.name())))
            workCommunication
                    .setEmail(map.get(WEContactsProperty.Email.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkFax.name())))
            workCommunication
                    .setFax(map.get(WEContactsProperty.WorkFax.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkPhone.name())))
            workCommunication.setPhone1(map.get(WEContactsProperty.WorkPhone
                    .name()));
        workInfo.setCommunication(workCommunication);

        return workInfo;
    }

    @Override
    public Address readContactsWorkInfoAddress(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));
        Address address = new Address();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkCity.name())))
            address.setCity(map.get(WEContactsProperty.WorkCity.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkState.name())))
            address.setStateOrProvince(map.get(WEContactsProperty.WorkState
                    .name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.WorkCountry.name())))
            address.setCountry(map.get(WEContactsProperty.WorkCountry.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkZip.name())))
            address.setPostalCode(map.get(WEContactsProperty.WorkZip.name()));
        if (!StringUtils
                .isEmpty(map.get(WEContactsProperty.WorkAddress1.name())))
            address.setUnstructuredAddress(map
                    .get(WEContactsProperty.WorkAddress1.name()));
        return address;
    }

    @Override
    public Communication readContactsWorkInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm));
        Communication communication = new Communication();
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.Email.name())))
            communication.setEmail(map.get(WEContactsProperty.Email.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkFax.name())))
            communication.setFax(map.get(WEContactsProperty.WorkFax.name()));
        if (!StringUtils.isEmpty(map.get(WEContactsProperty.WorkPhone.name())))
            communication
                    .setPhone1(map.get(WEContactsProperty.WorkPhone.name()));
        return communication;
    }

    /**
     * This method parses the incoming text i.e. multiline input into
     * List<Map<String, String>, where the list is for each row stored as a map
     * of comma separated values with header name as key.
     * 
     * @param attributes
     * @param originalText
     * @return
     * @throws AddressBookException
     */
    private Map<String, String> readEntry(Map<String, String> attributes,
            String originalText) throws AddressBookException {
        return readEntry(attributes, originalText,
                MxOSConstants.WE_CONTACT_ENTRY_TYPE);
    }

    private Map<String, String> readEntry(Map<String, String> attributes,
            String originalText, String entryType) throws AddressBookException {

        // Read existing contacts
        List<Map<String, String>> rowList = readExistingContacts(originalText,
                null, null, null, null);

        String contactId = attributes.get(WEContactsProperty.Id.name());

        // Check if the contact id is a valid CONTACT Id
        if (!validate(rowList, contactId, entryType)) {
            if (entryType.equalsIgnoreCase(MxOSConstants.WE_CONTACT_ENTRY_TYPE)) {
                throw new AddressBookException(
                        AddressBookError.ABS_CONTACT_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_CONTACT_NOT_FOUND.name(), "");
            } else if (entryType
                    .equalsIgnoreCase(MxOSConstants.WE_GROUP_ENTRY_TYPE)) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUP_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUP_NOT_FOUND.name(), "");

            }
        }
        // Return the contact with correct Id
        for (Map<String, String> row : rowList) {
            if (contactId
                    .equalsIgnoreCase(row.get(WEContactsProperty.Id.name()))) {
                return row;
            }
        }
        return null;
    }

    private List<Map<String, String>> readExistingContacts(String originalText,
            String queryString, String sortKey, String sortOrder, String filter)
            throws AddressBookException {

        String[] lines = originalText.split(LINE_REGEXP);

        List<Map<String, String>> rowList = new ArrayList<Map<String, String>>();
        if (lines.length > 0) {
            List<String> headerList = Arrays.asList(lines[0]
                    .split(FIELD_REGEXP));

            // setup query parser
            Visitor visitor = null;
            if (queryString != null && queryString.length() > 0) {
                visitor = new Query();
                visitor.parse(queryString);
            }

            for (int i = 1; i < lines.length; i++) {
                String[] values = lines[i].split(FIELD_REGEXP);
                Map<String, String> valueMap = new TreeMap<String, String>();
                for (int j = 0; j < values.length; j++) {
                    valueMap.put(headerList.get(j), unenquote(values[j]));
                }
                // run query before adding to the list
                if (visitor == null) {
                    rowList.add(valueMap);
                } else {
                    if (visitor.visit(valueMap)) {
                        rowList.add(valueMap);
                    }
                }
            }
        }

        List<Map<String, String>> resultList = null;
        // filter the entries
        if (filter != null) {
            Visitor visitor = new Filter();
            visitor.parse(filter);
            resultList = new ArrayList<Map<String, String>>();
            for (int i = 0; i < rowList.size(); i++) {
                if (visitor.visit(rowList.get(i))) {
                    resultList.add(rowList.get(i));
                }
            }
        } else {
            resultList = rowList;
        }

        // sort list using sortKey, sortOrder
        Sort sort = new Sort(sortKey, sortOrder);
        return sort.process(resultList);
    }

    @Override
    public GroupBase readGroupsBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), MxOSConstants.WE_GROUP_ENTRY_TYPE);

        if (null != map
                && map.get(WEContactsProperty.EntryType.toString())
                        .equalsIgnoreCase(MxOSConstants.WE_GROUP_ENTRY_TYPE)) {
            GroupBase base = new GroupBase();
            base.setGroupId(map.get(WEContactsProperty.Id.name()));
            if (!StringUtils.isEmpty(map.get(WEContactsProperty.listName
                    .toString()))) {
                base.setGroupName(map.get(WEContactsProperty.listName
                        .toString()));
            }
            if (!StringUtils.isEmpty(map.get(WEContactsProperty.listDescription
                    .toString()))) {
                base.setGroupDescription(map
                        .get(WEContactsProperty.listDescription.toString()));
            }
            return base;
        } else {
            throw new AddressBookException(
                    AddressBookError.ABS_GROUP_NOT_FOUND.name(),
                    ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                    AddressBookError.ABS_GROUP_NOT_FOUND.name(), "");
        }
    }

    @Override
    public List<ExternalMember> readGroupsExternalMember(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        // Not supported for WE
        return null;
    }

    @Override
    public List<Member> readGroupsMember(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        Map<String, String> map = readEntry(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), MxOSConstants.WE_GROUP_ENTRY_TYPE);

        String memberIds = map.get(WEContactsProperty.listMembers.toString());

        return populateGroupMembers(memberIds);
    }

    @Override
    public void rollback() throws ApplicationException {
        // Not supported for WE
    }

    private String unenquote(String value) {
        if (value != null) {
            if (value.startsWith(MxOSConstants.DOUBLE_QUOTE)
                    && value.endsWith(MxOSConstants.DOUBLE_QUOTE)) {
                value = value.substring(1, value.length() - 1);
            }
            if (value.contains(MxOSConstants.ESCAPED_DOUBLE_QUOTE)) {
                value = value.replaceAll(MxOSConstants.ESCAPED_DOUBLE_QUOTE,
                        MxOSConstants.DOUBLE_QUOTE);
            }
        }
        return value;
    }

    @Override
    public void updateContactBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_CONTACTBASE_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactName(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_CONTACTNAME_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactsImage(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
    }

    @Override
    public void updateContactsPersonalInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_PERSONALINFO_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactsPersonalInfoAddress(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_PERSONALINFO_ADDRESS_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactsPersonalInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();
        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        // Breakup incoming IM Address to BuddyName and ImProvider
        String imEmailAddress = null;
        if (inputParam.get(AddressBookProperty.personalInfoImAddress.name()) != null) {
            imEmailAddress = inputParam.get(
                    AddressBookProperty.personalInfoImAddress.name()).get(0);
        } else if (inputParam.get(AddressBookProperty.imAddress.name()) != null) {
            imEmailAddress = inputParam.get(
                    AddressBookProperty.imAddress.name()).get(0);
        }

        if (imEmailAddress != null && !imEmailAddress.isEmpty()) {

            if (WEAddressBookHelper.getImProviderFromEmail(imEmailAddress) != null) {
                attributes.put(WEContactsProperty.BuddyName.name(),
                        WEAddressBookHelper
                                .getBuddyNameFromEmail(imEmailAddress));

                attributes.put(
                        WEContactsProperty.ImProvider.name(),
                        WEAddressBookHelper.getImProviderFromEmail(
                                imEmailAddress).name());
            } else {
                attributes.put(WEContactsProperty.BuddyName.name(), null);
            }
        }

        updateWEAutoReply(
                mxosRequestState,
                AddressBookError.ABS_PERSONALINFO_COMMUNICATION_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactsPersonalInfoEvent(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_PERSONALINFO_EVENTS_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactsWorkInfo(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_WORKINFO_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactsWorkInfoAddress(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_WORKINFO_ADDRESS_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateContactsWorkInfoCommunication(
            MxOSRequestState mxosRequestState) throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_WORKINFO_COMMUNICATION_UNABLE_TO_UPDATE);
    }

    @Override
    public void updateGroupsBase(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        updateWEAutoReply(mxosRequestState,
                AddressBookError.ABS_GROUPBASE_UNABLE_TO_UPDATE,
                Action.UPDATE_GROUP_BASE);
    }

    private void updateWEAutoReply(MxOSRequestState mxosRequestState,
            AddressBookError error) throws AddressBookException {
        updateWEAutoReply(mxosRequestState, error, Action.UPDATE_CONTACT);
    }

    private void updateWEAutoReply(MxOSRequestState mxosRequestState,
            AddressBookError error, Action action) throws AddressBookException {
        Map<String, String> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributes;

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        List<Map<String, String>> attributesList = new ArrayList<Map<String, String>>();
        attributesList.add(attributes);

        String text = generateAutoReplyText(attributesList,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), action);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);
    }

    private boolean validate(List<Map<String, String>> rowList,
            String memberId, String entryType) {

        // The given member Id should be a contact Id......If groupId is given
        // as memberId exception should be thrown
        for (Map<String, String> row : rowList) {
            if (memberId
                    .equalsIgnoreCase(row.get(WEContactsProperty.Id.name()))
                    && row.get(WEContactsProperty.EntryType.toString())
                            .equalsIgnoreCase(entryType)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void validateUser(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
    }

    @Override
    public void deleteAddressBook(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                "");
    }

    @Override
    public List<String> createMultipleContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();

        String input = inputParam.get(AddressBookProperty.contactsList.name())
                .get(0);
        int size = input.split(LINE_REGEXP).length;

        List<String> contactIds = new ArrayList<String>();
        List<Map<String, String>> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributesList;

        for (int i = 0; i < size; i++) {

            Map<String, String> map = attributes.get(i);
            // Breakup incoming IM Address to BuddyName and ImProvider
            String imEmailAddress = null;
            if (inputParam
                    .get(AddressBookProperty.personalInfoImAddress.name()) != null) {
                imEmailAddress = inputParam.get(
                        AddressBookProperty.personalInfoImAddress.name())
                        .get(i);
            } else if (inputParam.get(AddressBookProperty.imAddress.name()) != null) {
                imEmailAddress = inputParam.get(
                        AddressBookProperty.imAddress.name()).get(i);
            }

            if (imEmailAddress != null && !imEmailAddress.isEmpty()) {

                if (WEAddressBookHelper.getImProviderFromEmail(imEmailAddress) != null) {
                    map.put(WEContactsProperty.BuddyName.name(),
                            WEAddressBookHelper
                                    .getBuddyNameFromEmail(imEmailAddress));
                    attributes.add(map);

                    map.put(WEContactsProperty.ImProvider.name(),
                            WEAddressBookHelper.getImProviderFromEmail(
                                    imEmailAddress).name());
                    attributes.add(map);
                }
            }

            String contactId = generateContactId(inputParam.get(
                    AddressBookProperty.userId.name()).get(0));
            map.put(WEContactsProperty.Id.name(), contactId);
            contactIds.add(contactId);

            map.put(WEContactsProperty.EntryType.name(),
                    MxOSConstants.WE_CONTACT_ENTRY_TYPE);
        }

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        String text = generateAutoReplyText(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.CREATE_MULTIPLE_CONTACTS);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);

        return contactIds;
    }

    @Override
    public List<String> createMultipleGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();

        String input = inputParam.get(AddressBookProperty.groupsList.name())
                .get(0);
        int size = input.split(LINE_REGEXP).length;

        List<String> groupIds = new ArrayList<String>();
        List<Map<String, String>> attributes = ((WEAddressBookBackendState) mxosRequestState
                .getBackendState().get(AddressBookDBTypes.we.name())).attributesList;

        for (int i = 0; i < size; i++) {

            Map<String, String> map = attributes.get(i);

            String groupId = generateContactId(inputParam.get(
                    AddressBookProperty.userId.name()).get(0));
            map.put(WEContactsProperty.Id.name(), groupId);
            groupIds.add(groupId);

            map.put(WEContactsProperty.EntryType.name(),
                    MxOSConstants.WE_GROUP_ENTRY_TYPE);
        }

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String autoReplyHost = info.getAutoReplyHost();
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("AutoReplyHost : ").append(
                    autoReplyHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        String text = generateAutoReplyText(attributes,
                WEAddressBookHelper.getAutoReplyText(autoReplyHost, mailboxId,
                        realm), Action.CREATE_MULTIPLE_CONTACTS);
        WEAddressBookHelper.setAutoReplyText(autoReplyHost, mailboxId, realm,
                text);

        return groupIds;
    }

    @Override
    public void logout(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
    }

    @Override
    public List<Contact> readAllContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void moveMultipleContacts(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
    }

    @Override
    public void moveMultipleGroups(MxOSRequestState mxosRequestState)
            throws AddressBookException {
        // Not supported for WE
    }
}
