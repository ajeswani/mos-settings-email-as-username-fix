package com.opwvmsg.mxos.backend.crud.ox.response.addressbook;

import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.ox.response.JsonToSessionMapper;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.sun.jersey.api.client.ClientResponse;

public class UserResponse extends Response {

    public String getUserId(ClientResponse resp) throws ComponentException {
        JsonNode root = getTree(resp);

        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        return JsonToSessionMapper.mapToUserId(root);
    }
}
