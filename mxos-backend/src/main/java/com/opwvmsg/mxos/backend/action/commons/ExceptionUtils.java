/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.commons;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Util class of controllers.
 * 
 * @author mxos-dev
 * 
 */
public final class ExceptionUtils {
    /**
     * Method to create a new InvalidRequestException.
     * 
     * @param mxosRequestState
     * @return
     */
    public static void createInvalidRequestException(
            final MxOSRequestState mxosRequestState) throws MxOSException {
        final MxOSException e = new InvalidRequestException(
                ErrorCode.GEN_BAD_REQUEST.name());
        e.setRequestParams(mxosRequestState.getInputParams().toString());
        e.setOperationType(mxosRequestState.getOperationName());
        ExceptionUtils.prepareMxOSErrorObject(e);
        throw e;
    }

    public static void prepareMxOSErrorObject(MxOSException e) {
        final MxosErrorMapping errorMapping = MxOSApp.getInstance()
                .getErrorMapping();
        // TODO: Remove ResponseErrorBean LEAPFROG-97
        final ResponseErrorBean responseErrorBean = errorMapping
                .getErrorMappings().get(e.getCode());
        if (responseErrorBean != null) {
            e.setShortMessage(responseErrorBean.getShortMessage());
            e.setCode(responseErrorBean.getErrorCode());
        }
        return;
    }

    /**
     * Default private constructor.
     */
    private ExceptionUtils() {
    }
}
