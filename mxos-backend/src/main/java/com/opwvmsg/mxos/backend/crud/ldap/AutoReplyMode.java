/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * Enum for AutoReply Mode.
 * @author mxos-dev
 *
 */
public enum AutoReplyMode implements DataMap.Property {
    None("none"), Vacation("vacation"), Reply("reply"), Echo("echo");

    private static final Map<String, AutoReplyMode> AUTO_REPLY_MODE_MAP
        = new HashMap<String, AutoReplyMode>();

    private String mode;

    static {
        AUTO_REPLY_MODE_MAP.put("N", None);
        AUTO_REPLY_MODE_MAP.put("V", Vacation);
        AUTO_REPLY_MODE_MAP.put("R", Reply);
        AUTO_REPLY_MODE_MAP.put("E", Echo);
    }

    private AutoReplyMode(String mode) {
        this.mode = mode;
    }

    /**
     *
     * @param key mode
     * @return mode enum
     * @throws InvalidDataException if enum not found for given data
     */
    public static AutoReplyMode getEnum(final String key)
        throws InvalidRequestException {
        if (key == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                    "Invalid account status:" + key);
        } else {
            return AUTO_REPLY_MODE_MAP.get(key);
        }
    }

    /**
     *
     * @param key autoMode
     * @return automode Enum
     * @throws InvalidDataException if enum not found for given data
     */
    public static String getAutoReplyModeValue(final String key)
        throws InvalidRequestException  {
        if (key == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_AUTO_REPLY_MODE.name(),
                    "Invalid autoreply mode:" + key);
        } else {
            if (AUTO_REPLY_MODE_MAP.get(key) != null) {
                return AUTO_REPLY_MODE_MAP.get(key).getMode();
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_AUTO_REPLY_MODE.name(),
                        "Unsupported autoreply mode:" + key);
            }
        }
    }
    /**
    *
    * @param key autoMode
    * @return automode Enum
     * @throws InvalidDataException if enum not found for given data
    */
    public static String getAutoReplyModeKey(final String key)
        throws InvalidRequestException {
        if (key == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_AUTO_REPLY_MODE.name(),
                    "Invalid autoreply mode:" + key);
        }
        for (final Entry<String, AutoReplyMode> entry : AUTO_REPLY_MODE_MAP
                .entrySet()) {
            if (entry.getValue().getMode().equalsIgnoreCase(key)) {
                return entry.getKey();
            }
        }
        throw new InvalidRequestException(
                MailboxError.MBX_INVALID_AUTO_REPLY_MODE.name(),
                "Unsupported autoreply mode:" + key);
    }

    /**
     * Returns the corresponding {@link String}.
     *
     * @return password hash type
     */
    public String getMode() {
        return mode;
    }
}
