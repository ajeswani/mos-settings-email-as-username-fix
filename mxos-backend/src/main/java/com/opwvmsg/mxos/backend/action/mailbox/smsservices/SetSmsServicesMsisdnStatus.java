/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.smsservices;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set sms services msisdn status.
 *
 * @author mxos-dev
 */
public class SetSmsServicesMsisdnStatus implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetSmsServicesMsisdnStatus.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetSmsServicesMsisdnStatus action start."));
        }
        String smsServicesMsisdnStatus = requestState.getInputParams()
                .get(MailboxProperty.smsServicesMsisdnStatus.name()).get(0);
        try {
            if (smsServicesMsisdnStatus != null
                    && !smsServicesMsisdnStatus.equals("")) {
                smsServicesMsisdnStatus = Integer
                        .toString(MxosEnums.SmsServicesMsisdnStatusType
                                .fromValue(smsServicesMsisdnStatus).ordinal());
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.smsServicesMsisdnStatus,
                            smsServicesMsisdnStatus);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set SMS services msisdn status.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_SMS_SERVICES_MSISDN_STATUS
                            .name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetSmsServicesMsisdnStatus action end."));
        }
    }
}
