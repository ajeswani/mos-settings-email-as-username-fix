/*
/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.groups.members;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create group members.
 * 
 * @author mxos-dev
 */
public class CreateGroupMembers implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateGroupMembers.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateGroupMembers action start."));
        }

        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;

        try {
            addressBookCRUDPool = MxOSApp.getInstance().getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();

            String name = mxosRequestState.getInputParams()
                    .get(AddressBookProperty.groupId.name()).get(0);

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(mxosRequestState,
                            AddressBookProperty.groupId, name);

            String memberId = mxosRequestState.getInputParams()
                    .get(AddressBookProperty.memberId.name()).get(0);

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(mxosRequestState,
                            AddressBookProperty.memberId, memberId);

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.memberName.name()) != null) {
                String memberName = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.memberName.name()).get(0);

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.memberName, memberName);
            }

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.memberEmail.name()) != null) {
                String memberEmail = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.memberEmail.name()).get(0);

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.memberEmail, memberEmail);
            }

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.memberFolderId.name()) != null) {
                String memberEmail = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.memberFolderId.name()).get(0);

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.memberFolderId, memberEmail);
            }
            
            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.mailField.name()) != null) {
                String mailField = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.mailField.name()).get(0);

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.mailField, mailField);
            }

            String member = addressBookCRUD
                    .createGroupMembers(mxosRequestState);
            mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.memberId,
                    member);
        } catch (AddressBookException e) {
            logger.error("Error while creating group members.", e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_GROUPS_MEMBERS_UNABLE_TO_PUT, e);
        } catch (final Exception e) {
            logger.error("Error while creating group members.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateGroupMembers action end."));
        }
    }
}
