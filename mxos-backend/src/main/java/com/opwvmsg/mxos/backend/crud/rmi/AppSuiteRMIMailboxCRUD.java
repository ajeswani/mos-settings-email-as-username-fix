/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.rmi;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIClientSocketFactory;
import java.util.HashSet;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.apache.log4j.Logger;

import com.openexchange.admin.rmi.OXContextInterface;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.exceptions.ContextExistsException;
import com.openexchange.admin.rmi.exceptions.DatabaseUpdateException;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.NoSuchContextException;
import com.openexchange.admin.rmi.exceptions.StorageException;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPBackendState;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

public class AppSuiteRMIMailboxCRUD implements IRMIMailboxCRUD {

    private static Logger logger = Logger
            .getLogger(AppSuiteRMIMailboxCRUD.class);

    private static final int DEFAULT_RMI_CLIENT_TIMEOUT = 2000;
    private static final int DEFAULT_RMI_READ_TIMEOUT = 2000;
    private static final long DEFAULT_OX_MAX_QUOTA = 1000L;
    private static final String DEFAULT_OX_TIME_ZONE = "EST5EDT";
    private static final String DEFAULT_OX_LOCALE = "en_US";
    private static final String DEFAULT_IMAP_MOS_URL = "mos://localhost:8081";
    private static final String DEFAULT_SMTP_MOS_URL = "mos://localhost:8081";
    private static final String DEFAULT_OX_USER_ACCESS = "opwvall";
    private OXContextInterface contextInterface;

    /**
     * Constructor.
     * 
     * @param baseURL - base url of RMI registry server.
     * @throws NotBoundException
     * @throws IOException
     */
    public AppSuiteRMIMailboxCRUD(String host, int port)
            throws NotBoundException, IOException {
        String connectionTimeoutString = System
                .getProperty(SystemProperty.rmiClientTimeout.name());
        int connectionTimeout = DEFAULT_RMI_CLIENT_TIMEOUT;
        try {
            connectionTimeout = Integer.parseInt(connectionTimeoutString);
        } catch (NumberFormatException nfe) {
            logger.error(new StringBuilder("Error parsing ")
                    .append(SystemProperty.rmiClientTimeout)
                    .append(" property, using default value [")
                    .append(DEFAULT_RMI_CLIENT_TIMEOUT).append("]"));

        }

        String readTimeoutString = System
                .getProperty(SystemProperty.rmiReadTimeout.name());
        int readTimeout = DEFAULT_RMI_READ_TIMEOUT;
        try {
            readTimeout = Integer.parseInt(readTimeoutString);
        } catch (NumberFormatException nfe) {
            logger.error(new StringBuilder("Error parsing ")
                    .append(SystemProperty.rmiReadTimeout)
                    .append(" property, using default value [")
                    .append(DEFAULT_RMI_READ_TIMEOUT).append("]"));

        }
        RMIClientSocketFactoryWithTimeout socketFactory = new RMIClientSocketFactoryWithTimeout(
                connectionTimeout, readTimeout);
        socketFactory.createSocket(host, port);
        Registry registry = LocateRegistry.getRegistry(host, port,
                socketFactory);
        contextInterface = (OXContextInterface) registry
                .lookup(OXContextInterface.RMI_NAME);
    }

    public void close() {
        // do nothing as RMI remote object will be
        // destroyed by DGC after its lease expires.
    }

    @Override
    public void commit() throws ApplicationException {
        // not supported
    }

    @Override
    public void createMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {

        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;

        final String email = mxosRequestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        String name = null;
        String firstName = null;
        String lastName = null;
        String password = null;
        Long quota = null;
        Integer oxMaxFileUploadSize = null;
        String variant = null;
        String locale = null;
        String timeZone = null;
        String oxImapUrl = null;
        String oxSmtpUrl = null;
        String mailboxId = null;
        String oxSetMaxFileUploadSize = null;
        Boolean enableSpamFilter = null;
        try {
            name = (String) attrs.get(LDAPMailboxProperty.maillogin.name())
                    .get();
            password = (String) attrs.get(
                    LDAPMailboxProperty.mailpassword.name()).get();
            if (attrs.get(LDAPMailboxProperty.mailboxid.name()) != null) {
                mailboxId = (String) attrs.get(
                        LDAPMailboxProperty.mailboxid.name()).get();
            }
            firstName = (String) attrs.get(LDAPMailboxProperty.cn.name()).get();
            lastName = (String) attrs.get(LDAPMailboxProperty.sn.name()).get();
            // Read quota
            try {
                String quotaString = System.getProperty(SystemProperty.oxMaxQuota
                        .name());
                quota = Long.parseLong(quotaString);
            } catch (NumberFormatException nfe) {
                logger.warn(new StringBuilder("Error parsing ")
                        .append(SystemProperty.oxMaxQuota)
                        .append(" property, using default value [")
                        .append(DEFAULT_OX_MAX_QUOTA).append("]"));
                quota = DEFAULT_OX_MAX_QUOTA;
            }
            logger.debug("oxMaxQuota = " + quota);
            // Read oxSetMaxFileUploadSize
            try {
                    oxSetMaxFileUploadSize = System.getProperty(
                    SystemProperty.oxSetMaxFileUploadSize.name(), "false");
            } catch (NumberFormatException e) {
                logger.warn("Invalid oxMaxFileUploadSize provided, taking default value");
                oxSetMaxFileUploadSize = "false";
            }
            logger.debug("oxSetMaxFileUploadSize = " + oxSetMaxFileUploadSize);
            // Read oxMaxFileUploadSize
            try {
                oxMaxFileUploadSize = Integer.parseInt(System.getProperty(
                    SystemProperty.oxMaxFileUploadSize.name(), "10"));
            } catch (NumberFormatException e) {
                logger.warn("Invalid oxMaxFileUploadSize provided, taking default value");
                oxMaxFileUploadSize = 10;
            }
            logger.debug("oxMaxFileUploadSize = " + oxMaxFileUploadSize);
            // read variant
            if (attrs.get(LDAPMailboxProperty.netmailvariant.name()) != null) {
                variant = (String) attrs.get(
                        LDAPMailboxProperty.netmailvariant.name()).get();
            }
            logger.debug("netmailvariant = " + variant);
            // read locale
            if (attrs.get(LDAPMailboxProperty.netmaillocale.name()) != null) {
                locale = (String) attrs.get(
                        LDAPMailboxProperty.netmaillocale.name()).get();
            } else {
                locale = System.getProperty(
                        SystemProperty.oxDefaultLocale.name(), DEFAULT_OX_LOCALE);
            }
            logger.debug("locale = " + locale);
            // read timeZone
            if (attrs.get(LDAPMailboxProperty.msgtimezone.name()) != null) {
                timeZone = (String) attrs.get(LDAPMailboxProperty.msgtimezone.name()).get();
            } else {
                timeZone = System.getProperty(
                        SystemProperty.oxDefaultTimezone.name(), DEFAULT_OX_TIME_ZONE);
            }
            logger.debug("timeZone = " + timeZone);
            // read URLs
            oxImapUrl = System.getProperty(SystemProperty.oxImapUrl.name(), DEFAULT_IMAP_MOS_URL);
            logger.debug("oxImapUrl = " + oxImapUrl);
            oxSmtpUrl = System.getProperty(SystemProperty.oxSmtpUrl.name(), DEFAULT_SMTP_MOS_URL);
            logger.debug("oxSmtpUrl = " + oxSmtpUrl);
            enableSpamFilter = Boolean.parseBoolean(System.getProperty(
                    SystemProperty.oxSpamFilterEnabled.name(), MxOSConstants.TRUE));
            logger.debug("enableSpamFilter = " + enableSpamFilter);
        } catch (NamingException e) {
            logger.warn("Invalid Data provided", e);
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(), e);
        }
        Context context = new Context();
        context.setName(name);
        context.addLoginMapping(mailboxId);
        context.setMaxQuota(quota);

        final boolean oxSSOEnabled = Boolean.parseBoolean(System.getProperty(
                SystemProperty.oxSSOEnabled.name(), MxOSConstants.FALSE));
        logger.debug("oxSSOEnabled = " + oxSSOEnabled);
        
        User user = new User();
        user.setName(name);
        if (oxSSOEnabled) {
            // if SSO Enabled at OX
            user.setDisplay_name(name);
            user.setGiven_name(name);
            user.setSur_name(name);
            user.setPassword(name);
        } else {
            user.setDisplay_name(new StringBuilder(firstName)
                    .append(MxOSConstants.SPACE).append(lastName).toString());
            user.setGiven_name(firstName);
            user.setSur_name(lastName);
            user.setPassword(name);
        }
        user.setPrimaryEmail(email);
        user.setEmail1(email);
        user.setImapLogin(name);
        user.setUserAttribute("owm", "userSettingStatus", "retry");
        
        if (Boolean.parseBoolean(oxSetMaxFileUploadSize)){
            user.setUploadFileSizeLimit(oxMaxFileUploadSize);
        }

        final boolean oxPLMNAddressEnabled = Boolean.parseBoolean(System.getProperty(
                SystemProperty.oxPLMNAddressEnabled.name(), "true"));
        logger.debug("oxPLMNAddressEnabled = " + oxPLMNAddressEnabled);
        if (oxPLMNAddressEnabled) {
            if (logger.isDebugEnabled())
                logger.debug("Defalut Sender Address conversion for:" + name);
            try {
                Long.parseLong(name);
                if (logger.isDebugEnabled())
                    logger.debug("Converted Defalut Sender Address is:<" + name
                            + "/TYPE=PLMN>");
                user.setDefaultSenderAddress(name + "/TYPE=PLMN");
                final HashSet<String> aliases = new HashSet<String>();
                aliases.add(name);
                aliases.add(name + "/TYPE=PLMN");
                user.setAliases(aliases);
            } catch (NumberFormatException e) {
                logger.error("Mail Login is not numeric so not changing defaultSenderAddress"
                        + name);
            }
        }
        logger.info(" Inside mOS Authenticator: getUser, for user:" + name);
        user.setLanguage(locale);
        // LDAP msgtimezone attribute is always in POSIX format
        // TimeZones calculations not required  
        user.setTimezone(timeZone);

        user.setImapServer(oxImapUrl);
        user.setSmtpServer(oxSmtpUrl);
        user.setGui_spam_filter_enabled(enableSpamFilter);

        final Credentials auth = new Credentials();
        auth.setLogin(System.getProperty(SystemProperty.oxAdminMaster.name()));
        auth.setPassword(System
                .getProperty(SystemProperty.oxAdminMasterPassword.name()));

        final String oxUserAccess = System.getProperty(
                SystemProperty.oxUserAccess.name(), DEFAULT_OX_USER_ACCESS);
        logger.debug("oxUserAccess = " + oxUserAccess);
        try {
            contextInterface.create(context, user, oxUserAccess, auth);
        } catch (RemoteException e) {
            logger.warn("Exception while createMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (StorageException e) {
            logger.warn("Storage exception occurred", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (InvalidCredentialsException e) {
            logger.warn("Invalid admin credentials", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (InvalidDataException e) {
            logger.warn("Invalid Data provided", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (ContextExistsException e) {
            logger.warn("Mailbox already exists", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_ALREADY_EXISTS.name(), e);
        }
    }
    private String getTheme(String variant) {
        String theme = "";
        // TODO May be some scope for improvement in the logic below
        // For now the objective is to have some value in user's theme
        String themeDef = System
                .getProperty(SystemProperty.oxOpenwaveVariantPrefix.name()
                        + "." + variant);
        if (themeDef != null) {
            String[] themeParts = themeDef.split(",");
            if (themeParts != null && themeParts.length == 2) {
                theme = "\"theme\":{\"name\":\"" + themeParts[0]
                        + "\",\"path\":\"" + themeParts[1] + "\"}";
            }
        }
        return theme;
    }

    @Override
    public void deleteMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {

        final String email = mxosRequestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // Optional params
        String userNameAsEmail = System
                .getProperty(SystemProperty.storeUserNameAsEmail.name());

        final String[] token = email.split(MxOSConstants.AT_THE_RATE);
        final String userName = Boolean.valueOf(userNameAsEmail) ? email
                .replace(MxOSConstants.AT_THE_RATE, MxOSConstants.PERIOD)
                : token[0];
        final String domain = token[1];
        if (!ActionUtils.isDomainValid(email, domain)) {
            throw new InvalidRequestException(
                    DomainError.DMN_INVALID_NAME.name());
        }

        final Credentials auth = new Credentials();
        auth.setLogin(System.getProperty(SystemProperty.oxAdminMaster.name()));
        auth.setPassword(System
                .getProperty(SystemProperty.oxAdminMasterPassword.name()));
        try {
            Context[] context = contextInterface.list(userName, auth);
            if (context.length == 1) {
                contextInterface.delete(context[0], auth);
            } else {
                logger.warn("None OR Multiple context found in OX, delete aborted.");
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_DELETE.name());
            }
        } catch (RemoteException e) {
            logger.warn("Exception while deleteMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } catch (StorageException e) {
            logger.warn("Storage exception occurred", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } catch (InvalidCredentialsException e) {
            logger.warn("Invalid admin credentials", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } catch (InvalidDataException e) {
            logger.warn("Invalid Data provided", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } catch (NoSuchContextException e) {
            logger.warn("Context does not exist", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } catch (DatabaseUpdateException e) {
            logger.warn("Database update error", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        }
    }

    @Override
    public void readMailbox(MxOSRequestState mxOSRequestStatus, Base base)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void rollback() throws ApplicationException {
        // not supported
    }

    @Override
    public void updateMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub
    }
}

class RMIClientSocketFactoryWithTimeout implements RMIClientSocketFactory,
        Serializable {
    /**
     * Generated serial id
     */
    private static final long serialVersionUID = -7715892777748778406L;
    private int connectionTimeout;
    private int readTimeout;

    public RMIClientSocketFactoryWithTimeout(int connectionTimeout, int readTimeout) {
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        Socket s = new Socket();
        s.connect(new InetSocketAddress(host, port), connectionTimeout);
        s.setSoTimeout(readTimeout);
        return s;
    }
}
