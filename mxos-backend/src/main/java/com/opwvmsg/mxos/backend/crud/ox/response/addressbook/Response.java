/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.response.addressbook;

import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

/**
 * Abstract class to handle exceptions and converting json returned by OX into a
 * parseable tree
 * 
 * @author mxos-dev
 * 
 */
public abstract class Response {

    private static Pattern regex = Pattern.compile(".*\\((\\{.*?\\})\\).*");

    public static int getMultipleErrorCount(JsonNode jsonTree) {
        int count = 0;
        if (jsonTree.isArray()) {
            Iterator<JsonNode> nodes = jsonTree.getElements();
            while (nodes.hasNext()) {
                if (!nodes.next().path(OXContactsProperty.error.name())
                        .isMissingNode()) {
                    count++;
                }
            }
        }
        return count;
    }

    public static JsonNode getMultipleJsonTree(ClientResponse resp)
            throws ClientHandlerException, UniformInterfaceException,
            AddressBookException {
        String str = resp.getEntity(String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        JsonNode jsonTree = null;
        try {
            jsonTree = mapper.readTree(str);
        } catch (JsonProcessingException e) {
            throw new AddressBookException(e);
        } catch (IOException e) {
            throw new AddressBookException(e);
        }
        return jsonTree;
    }

    public static void validateMultipleResponse(JsonNode jsonTree)
            throws AddressBookException {

        // jsonTree should be list of error nodes
        if (jsonTree.isArray()) {
            Iterator<JsonNode> nodes = jsonTree.getElements();
            while (nodes.hasNext()) {
                validateResponse(nodes.next());
            }
        } else {
            throw new AddressBookException(
                    AddressBookError.ABS_CONTACTS_MULTIPLE_UNABLE_TO_MOVE
                            .name());
        }
    }

    public static void validateResponse(ClientResponse resp)
            throws AddressBookException {

        JsonNode jsonTree = getMultipleJsonTree(resp);

        validateResponse(jsonTree);
    }

    private static void validateResponse(JsonNode jsonTree)
            throws AddressBookException {
        if (!jsonTree.path(OXContactsProperty.error.name()).isMissingNode()) {

            int i = 0;
            JsonNode errorParams = jsonTree
                    .path(OXContactsProperty.error_params.name());
            Object[] parameters = new Object[errorParams.size()];

            for (JsonNode node : errorParams) {
                parameters[i++] = node.asText();
            }

            String message = jsonTree.path(OXContactsProperty.error.name())
                    .asText();

            message = String.format(message.replaceAll("\\$d", "\\$s"),
                    parameters);

            String category = jsonTree.path(OXContactsProperty.category.name())
                    .asText();
            String code = jsonTree.path(OXContactsProperty.code.name())
                    .asText();
            String errorId = jsonTree.path(OXContactsProperty.error_id.name())
                    .asText();

            throw new AddressBookException(message, category, code, errorId);
        }
    }

    public static void validateUploadResponse(ClientResponse resp)
            throws AddressBookException {

        String str = resp.getEntity(String.class);
        // inside str, we need to search for ({ and })
        // and capture the first group to check success/failure.
        Matcher matcher = regex.matcher(str);
        if (!matcher.find()) {
            throw new AddressBookException(
                    AddressBookError.ABS_INVALID_SIZE_OF_CONTACTS_IMAGE.name());
        }
        str = matcher.group(1);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        
        JsonNode jsonTree = null;
        try {
            jsonTree = mapper.readTree(str);
        } catch (JsonProcessingException e) {
            throw new AddressBookException(e);
        } catch (IOException e) {
            throw new AddressBookException(e);
        }

        validateResponse(jsonTree);
    }

    protected JsonNode getTree(ClientResponse resp) throws AddressBookException {
        JsonNode jsonTree = getMultipleJsonTree(resp);

        validateResponse(jsonTree);

        return jsonTree;
    }

}
