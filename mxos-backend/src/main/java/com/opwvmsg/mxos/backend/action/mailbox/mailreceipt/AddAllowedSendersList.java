/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to add allowed senders list.
 * 
 * @author mxos-dev
 */
public class AddAllowedSendersList implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(AddAllowedSendersList.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddAllowedSendersList action start."));
        }
        try {
            final List<String> newAllowedSenders = requestState.getInputParams()
                    .get(MailboxProperty.allowedSender.name());

            List<String> allowedSendersList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allowedSendersList);
            if (null == allowedSendersList) {
                allowedSendersList = new ArrayList<String>();
            }
            if ((allowedSendersList.size() + newAllowedSenders.size()) > MxOSConfig
                    .getMaxAllowedSendersList()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_ALLOWED_SENDERS_REACHED_MAX_LIMIT
                                .name());
            }
            for (String allowedSender : newAllowedSenders) {
                if (!allowedSendersList.contains(allowedSender.toLowerCase())) {
                    allowedSendersList.add(allowedSender.toLowerCase());
                }
            }
            final String[] allowedSendersListToArray = allowedSendersList
                    .toArray(new String[allowedSendersList.size()]);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.allowedSendersList,
                            allowedSendersListToArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add allowed sendrs list.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_ALLOWED_SENDERS_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddAllowedSendersList action end."));
        }
    }
}
