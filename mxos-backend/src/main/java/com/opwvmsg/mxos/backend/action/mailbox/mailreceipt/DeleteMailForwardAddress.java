/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to delete forwarding address.
 * 
 * @author mxos-dev
 */
public class DeleteMailForwardAddress implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(DeleteMailForwardAddress.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteMailForwardAddress action start."));
        }
        try {
            final String removeFwdAddress =
                    requestState.getInputParams()
                            .get(MailboxProperty.forwardingAddress.name())
                            .get(0);

            final List<String> fwdAddressList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.forwardingAddresses);

            if (fwdAddressList == null ||
                    !fwdAddressList.remove(removeFwdAddress.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_FORWARDING_ADDRESS.name());
            }
            final String[] mailFwdAddressesArray =
                fwdAddressList.toArray(new String[fwdAddressList.size()]);

            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.forwardingAddress,
                            mailFwdAddressesArray);
    
            requestState.getDbPojoMap().setProperty(
                    MxOSPOJOs.fwdAddressList, fwdAddressList);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while delete mail forward address.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_FORWARD_ADDRESS_DELETE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteMailForwardAddress action end."));
        }
    }
}
