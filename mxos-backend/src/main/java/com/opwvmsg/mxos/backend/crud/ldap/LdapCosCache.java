/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

/**
 * Ldap Cos Cache that holds all the attributes of the COS based on the
 * adminPolicyDn.
 * 
 * @author mxos-dev
 */
public class LdapCosCache {
    private static Logger logger = Logger.getLogger(LdapCosCache.class);

    private Map<String, CosAttributes> cosCache;
    private int ldapCosCacheFetchCounterRefresh;

    /**
     * Default constructor.
     */
    public LdapCosCache() {
        cosCache = new ConcurrentHashMap<String, CosAttributes>();
        ldapCosCacheFetchCounterRefresh = Integer.parseInt(System
                .getProperty(SystemProperty.ldapCosCacheFetchRefreshCount
                        .name()));
    }

    /**
     * Fetches the Cos Attributes from Cache. If the number of fetches, it will
     * get the attributes from LDAP and updates the Cache
     * 
     * @return the cosCache
     * @throws NamingException, MxOSException
     */
    public Attributes getCosAttributes(final String cosId,
            final boolean getAllAttributes,
            final LdapMailboxCRUD ldapMailboxCRUD, final String[] attributeNames)
            throws MxOSException, NamingException {
        if ((null == cosId) || (cosId.isEmpty())) {
            logger.error("Invalid CosId received");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Empty CosId received");
        }

        if (null == ldapMailboxCRUD) {
            logger.error("Invalid LDAP Mailbox CRUD Object passed");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Invalid LDAP Mailbox CRUD Object");
        }

        if (null == attributeNames) {
            logger.error("Invalid attributes array passed");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Invalid attributes array passed");
        }

        String cosKeyName = null;
        if (true == getAllAttributes) {
            cosKeyName = cosId + "ALL";
        } else {
            cosKeyName = cosId + "MAILBOX";
        }

        Attributes attributes = null;

        if (cosCache.containsKey(cosKeyName)) {
            synchronized (cosCache) {
                if (cosCache.get(cosKeyName).incrementAndGetFetchCount() > ldapCosCacheFetchCounterRefresh) {
                    logger.info("Cos "
                            + cosId
                            + " present in the Cache but fetch counter exceeded configured Value "
                            + ldapCosCacheFetchCounterRefresh
                            + ". Refreshing the cache");
                    attributes = ldapMailboxCRUD.executeLDAPRetrieve(cosId,
                            attributeNames);
                    cosCache.remove(cosKeyName);
                    if (attributes != null) {
                        logger.info("Refreshed the Cache Successfully for cosId "
                                + cosId);
                        cosCache.put(cosKeyName, new CosAttributes(attributes));
                    }
                } else {
                    attributes = cosCache.get(cosKeyName).getAttributes();
                    logger.info("Cos "
                            + cosId
                            + " present in the Cache. Successfully fetched the attrbutes from Cache. Fetch Counter : "
                            + cosCache.get(cosKeyName).getNoOfFetches());
                }
            }
        } else {
            logger.info("Cos " + cosId
                    + " not present in the Cache. Retrieving from LDAP");
            synchronized (cosKeyName) {
                attributes = ldapMailboxCRUD.executeLDAPRetrieve(cosId,
                        attributeNames);
                if (attributes != null)
                    cosCache.put(cosKeyName, new CosAttributes(attributes));
            }
        }

        return attributes;
    }

    /**
     * @return the cosCache
     */
    public Map<String, CosAttributes> getCosCache() {
        return cosCache;
    }

}