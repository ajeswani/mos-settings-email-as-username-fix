/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.groups.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update Group base object.
 * 
 * @author mxos-dev
 * 
 */
public class UpdateGroupsBase implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateGroupsBase.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateGroupsBase action start."));
        }

        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;

        try {
            addressBookCRUDPool = MxOSApp.getInstance().getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();

            String name = mxosRequestState.getInputParams()
                    .get(AddressBookProperty.groupId.name()).get(0);

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(mxosRequestState,
                            AddressBookProperty.groupId, name);

            addressBookCRUD.updateGroupsBase(mxosRequestState);
        } catch (AddressBookException e) {
            if (e.getCode()
                    .equals(ExceptionUtils.OX_CONTACT_NOT_FOUND_IN_CONTEXT15_ERROR_CODE)) {
                e.setCode(AddressBookError.ABS_GROUP_NOT_FOUND.name());
            }
            logger.error("Error while update base.", e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_GROUPBASE_UNABLE_TO_UPDATE, e);
        } catch (final Exception e) {
            logger.error("Error while update base.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateGroupsBase action end."));
        }
    }
}
