/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to adding address for local delivery.
 * 
 * @author mxos-dev
 */
public class AddAddressesForLocalDelivery implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(AddAddressesForLocalDelivery.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddAddressesForLocalDelivery action start."));
        }
        try {
            final List<String> newAddressesFLD = requestState.getInputParams()
                    .get(MailboxProperty.addressForLocalDelivery.name());
            List<String> addressesFLDList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.addressesForLocalDelivery);

            if (null == addressesFLDList) {
                addressesFLDList = new ArrayList<String>();
            }

            if ((addressesFLDList.size() + newAddressesFLD.size()) > MxOSConfig
                    .getMaxAddressesForLocalDelivery()) {
                throw new InvalidRequestException(
                        MailboxError
                        .MBX_LOCAL_DELIVERY_ADDRESS_REACHED_MAX_LIMIT
                                .name());
            }
            for (String addressesFLD : newAddressesFLD) {
                if (!addressesFLDList.contains(addressesFLD.toLowerCase())) {
                    addressesFLDList.add(addressesFLD.toLowerCase());
                }
            }
            final String[] addressesFLDToArray = addressesFLDList
                    .toArray(new String[addressesFLDList.size()]);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.addressesForLocalDelivery,
                            addressesFLDToArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add address.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_LOCAL_DELIVERY_ADDRESS_CREATE
                            .name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddAddressesForLocalDelivery action end."));
        }
    }
}
