/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.saml;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.saml.ISamlService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * SAML service exposed to client which is responsible for doing SAML related
 * activities e.g getSAMLRequest, decodeSAMLResponse, etc.
 * 
 * @author mxos-dev
 */
public class BackendSamlService implements ISamlService {

    private static Logger logger = Logger.getLogger(BackendSamlService.class);

    /**
     * Default Constructor.
     */
    public BackendSamlService() {
        logger.info("BackendSamlService created...");
    }

    @Override
    public String getSAMLRequest(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.SamlService, Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.SamlService, Operation.GET, t0,
                    StatStatus.pass);

            return (String) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.samlRequest);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.SamlService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Map<String, String> decodeSAMLAssertion(
            Map<String, List<String>> inputParams) throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.SamlService, Operation.POST);
            if (inputParams == null || inputParams.size() < 1) {
                ExceptionUtils.createInvalidRequestException(mxosRequestState);
            }
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.SamlService, Operation.POST, t0,
                    StatStatus.pass);

            return (Map<String, String>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.samlAttributes);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.SamlService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
