/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set all the required params used in create mailbox.
 *
 * @author mxos-dev
 */
public class SetCreateMailboxParams implements MxOSBaseAction {
    private static final Logger logger = Logger
            .getLogger(SetCreateMailboxParams.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetCreateMailboxParams action start.");
        }
        // Mandatory params
        final String email =
            requestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0);
        // Optional params
        String userNameAsEmail = System
                .getProperty(SystemProperty.storeUserNameAsEmail.name());
        final String[] token = email.split(MxOSConstants.AT_THE_RATE);
        final String userName = Boolean.valueOf(userNameAsEmail) ? email
                : token[0];
        final String domain = token[1];
        if (!ActionUtils.isDomainValid(email, domain)) {
            throw new InvalidRequestException(
                    DomainError.DMN_INVALID_NAME.name());
        }
        // mailboxId
        final Long mailboxId;
        if (requestState.getInputParams().containsKey(
                MailboxProperty.mailboxId.name())) {
            mailboxId =
                    Long.parseLong(requestState.getInputParams()
                            .get(MailboxProperty.mailboxId.name()).get(0));
        } else {
            // Generate mailboxID
            mailboxId = Long.parseLong(ActionUtils.generateMailboxId(userName));
        }
        // status
        final String status;
        if (requestState.getInputParams().containsKey(
                MailboxProperty.status.name())) {
            status = requestState.getInputParams()
                    .get(MailboxProperty.status.name()).get(0);
        } else {
            status = MxOSConfig.getDefaultMailboxStatus();
        }
        // cosId
        final String cosId = requestState.getInputParams()
                .get(MailboxProperty.cosId.name()).get(0);
        // messageStoreHosts
        final String messageStoreHost;
        if (requestState.getInputParams().containsKey(
                MailboxProperty.messageStoreHost.name())) {
            messageStoreHost = requestState.getInputParams().get(
                    MailboxProperty.messageStoreHost.name()).get(0);
        } else {
            // Get MSS Host name from
            messageStoreHost = ActionUtils.getMssHost();
        }

        requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailboxId,
                mailboxId);
        requestState.getDbPojoMap().setProperty(MxOSPOJOs.status,
                status);
        /*
         * TODO: MailboxId/FolderId/MessageId can come either as part of input
         * parameters or it can be system generated additional params. CRUD
         * layer don't have knowledge from where to pick mailboxId. One solution
         * is to put if-else in CRUD to pick mailboxId. For now we are copying
         * mailboxId only to additional params to avoid if-else in CRUD layer
         * and think about better approach to solve this issue in future with
         * following assumption: 1) Only copying mailboxId/folderId/messageId,
         * so there won't be much GC overhead. 2) If our load test proves that
         * this copy is overhead then we can easily enhance CRUD layer by
         * putting if-else or some with some other approach.
         */
        // realm
        String realm;
        if (requestState.getInputParams().containsKey(
                MailboxProperty.realm.name())) {
            realm = requestState.getInputParams()
                    .get(MailboxProperty.realm.name()).get(0);
            realm = realm.replaceAll("/", "");
            requestState.getAdditionalParams().setProperty(
                    MailboxProperty.realm, realm);
        }
        requestState.getAdditionalParams().setProperty(
                MailboxProperty.mailboxId, mailboxId);
        requestState.getAdditionalParams().setProperty(
                MailboxProperty.messageStoreHost, messageStoreHost);
        requestState.getAdditionalParams().setProperty(
                MailboxProperty.autoReplyHost, messageStoreHost);

        try {
            IMailboxHelper helper = MxOSApp.getInstance()
                    .getMailboxHelper();
            helper.setAttribute(requestState, MailboxProperty.userName,
                    userName);
            helper.setAttribute(requestState, MailboxProperty.mailboxId,
                    mailboxId);
            helper.setAttribute(requestState, MailboxProperty.email, email);
            helper.setAttribute(requestState, MailboxProperty.allowedDomain,
                    ActionUtils.getDomainFromEmail(email));
            helper.setAttribute(requestState, MailboxProperty.status, status);
            helper.setAttribute(requestState, MailboxProperty.cosId, cosId);
            helper.setAttribute(requestState,
                    MailboxProperty.messageStoreHost, messageStoreHost);
            helper.setAttribute(requestState,
                    MailboxProperty.autoReplyHost, messageStoreHost);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while create mailbox params.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCreateMailboxParams action end."));
        }
    }
}
