/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaConnectionPool.java#2 $
 */

package com.opwvmsg.mxos.backend.crud.http;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

/**
 * Implementation of Generic connection pool for handling HTTP operations. Used
 * in sending Http-post towards IMAP Servers for publish requests received from
 * MSS.
 * 
 * @author mxos-dev
 */
public class GenericHttpConnectionPool implements ICRUDPool<GenericHttpClient> {
    private static Logger logger = Logger.getLogger(GenericHttpConnectionPool.class);

    private GenericObjectPool<GenericHttpClient> objPool;

    /**
     * Constructor.
     * @param maxPoolSize - Max no. of Http clients to be pooled
     * 
     * @throws Exception Exception.
     */
    public GenericHttpConnectionPool(final int maxPoolSize) throws MxOSException {
        loadPool(maxPoolSize);
    }
    
    /**
     * Method to load Pool.
     */
    private void loadPool(final int maxSize) {
        objPool = new GenericObjectPool<GenericHttpClient>(
                new GenericHttpConnectionFactory(), maxSize);

        initializeJMXStats(maxSize);
        objPool.setMaxIdle(-1);
    }

    @Override
    public GenericHttpClient borrowObject() throws MxOSException {
        try {
            final GenericHttpClient obj = objPool.borrowObject();
            incrementJMXStats();
            return obj;
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    @Override
    public void returnObject(GenericHttpClient client)
            throws MxOSException {
        try {
            /* cleanup before returning the object to pool for reuse */
            client.cleanup();
            objPool.returnObject(client);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while return object object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.NOTIFY.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_NOTIFY.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_NOTIFY.decrement();
    }

    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub
        
    }
}
