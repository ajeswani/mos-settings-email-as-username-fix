/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoAddressService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts Name service exposed to client which is responsible for doing basic
 * name related activities e.g read name, update name, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class BackendContactsPersonalInfoAddressService implements
        IContactsPersonalInfoAddressService {

    private static Logger logger = Logger
            .getLogger(BackendContactsPersonalInfoAddressService.class);

    /**
     * Default Constructor.
     */
    public BackendContactsPersonalInfoAddressService() {
        logger.info("BackendContactsPersonalInfoAddressService Service created...");
    }

    @Override
    public Address read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams,
                    ServiceEnum.ContactsPersonalInfoAddressService,
                    Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoAddressService,
                    Operation.GET, t0, StatStatus.pass);

            return ((Address) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.contactsPersonalInfoAddress));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoAddressService,
                    Operation.GET, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams,
                    ServiceEnum.ContactsPersonalInfoAddressService,
                    Operation.POST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoAddressService,
                    Operation.POST, t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoAddressService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }
}
