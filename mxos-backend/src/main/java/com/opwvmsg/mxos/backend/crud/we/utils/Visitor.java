/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.we.utils;

import java.util.Map;

import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;

public interface Visitor {
    public void parse(String input) throws AddressBookException;

    public boolean visit(Map<String, String> valueMap);
}
