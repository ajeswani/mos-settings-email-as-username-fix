/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to create signatures.
 * 
 * @author mxos-dev
 */
public class AddSignature implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(AddSignature.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddSignature action start."));
        }

        try {
            MailSend mailSend = (MailSend) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailSend);

            Signature newSignature = (Signature) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);
            if (mailSend != null) {
                List<Signature> signatureList = mailSend.getSignatures();
                if (signatureList == null) {
                    signatureList = new ArrayList<Signature>();
                }
                signatureList.add(newSignature);
                mailSend.setSignatures(signatureList);
            }

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.signature,
                    newSignature);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailSend,
                    mailSend);

            // Update in OX if backend is set to OX
            String appSuiteIntegrated = System
                    .getProperty(SystemProperty.appSuiteIntegrated.name());
            if (appSuiteIntegrated != null
                    && appSuiteIntegrated.trim().equalsIgnoreCase(
                            MxOSConstants.TRUE)) {
                if (MxOSConfig.getAddressBookBackend().equals(
                        AddressBookDBTypes.ox.name())) {
                    ICRUDPool<ISettingsCRUD> oxSettingsCRUDPool = null;
                    ISettingsCRUD oxSettingsCRUD = null;

                    try {
                        // call http update methods
                        oxSettingsCRUDPool = MxOSApp.getInstance()
                                .getOXSettingsHttpCRUD();

                        oxSettingsCRUD = oxSettingsCRUDPool.borrowObject();
                        oxSettingsCRUD.createSetting(requestState);
                        if (logger.isDebugEnabled()) {
                            logger.debug(new StringBuffer(
                                    "Create Setting Http update done..!"));
                        }
                        oxSettingsCRUD.commit();
                    } catch (final MxOSException e) {
                        if (oxSettingsCRUD != null) {
                            oxSettingsCRUD.rollback();
                        }
                        throw e;
                    } catch (Exception e) {
                        logger.error("Error while create setting in OX.", e);
                        if (oxSettingsCRUD != null) {
                            oxSettingsCRUD.rollback();
                        }
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                    } finally {
                        try {
                            if (oxSettingsCRUDPool != null
                                    && oxSettingsCRUD != null) {
                                oxSettingsCRUDPool.returnObject(oxSettingsCRUD);
                            }
                        } catch (final MxOSException e) {
                            throw new ApplicationException(
                                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error while add signature.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_PUT_MAIL_SEND_SIGNATURE.name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddSignature action end."));
        }
    }
}
