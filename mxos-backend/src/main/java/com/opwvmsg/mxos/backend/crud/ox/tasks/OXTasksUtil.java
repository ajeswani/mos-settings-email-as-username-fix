package com.opwvmsg.mxos.backend.crud.ox.tasks;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;

public class OXTasksUtil {

    public static EnumMap<TasksProperty, OXTasksProperty> JSONToOX_Tasks = new EnumMap<TasksProperty, OXTasksProperty>(
            TasksProperty.class);
    public static Map<String, String> Tasks_Id = new HashMap<String, String>();

    static {
        JSONToOX_Tasks.put(TasksProperty.userId, OXTasksProperty.name);
        // task base property mapping
        JSONToOX_Tasks.put(TasksProperty.taskId, OXTasksProperty.id);
        JSONToOX_Tasks.put(TasksProperty.folderId, OXTasksProperty.folder);
        JSONToOX_Tasks.put(TasksProperty.folderId, OXTasksProperty.folder_id);
        JSONToOX_Tasks.put(TasksProperty.name, OXTasksProperty.title);
        JSONToOX_Tasks.put(TasksProperty.owner, OXTasksProperty.organizer);
        JSONToOX_Tasks.put(TasksProperty.priority, OXTasksProperty.priority);
        JSONToOX_Tasks.put(TasksProperty.status, OXTasksProperty.status);
        JSONToOX_Tasks.put(TasksProperty.name, OXTasksProperty.title);
        JSONToOX_Tasks.put(TasksProperty.isPrivate, OXTasksProperty.private_flag);
        JSONToOX_Tasks.put(TasksProperty.colorLabel, OXTasksProperty.color_label);
        JSONToOX_Tasks.put(TasksProperty.categories, OXTasksProperty.categories);
        JSONToOX_Tasks.put(TasksProperty.notes, OXTasksProperty.note);
        JSONToOX_Tasks.put(TasksProperty.createdDate, OXTasksProperty.creation_date);
        JSONToOX_Tasks.put(TasksProperty.updatedDate, OXTasksProperty.last_modified);
        JSONToOX_Tasks.put(TasksProperty.startDate, OXTasksProperty.start_date);
        JSONToOX_Tasks.put(TasksProperty.dueDate, OXTasksProperty.end_date);
        JSONToOX_Tasks.put(TasksProperty.completedDate, OXTasksProperty.date_completed);
        JSONToOX_Tasks.put(TasksProperty.reminderDate, OXTasksProperty.alarm);
        JSONToOX_Tasks.put(TasksProperty.progress, OXTasksProperty.percent_completed);
        
        JSONToOX_Tasks.put(TasksProperty.sortKey, OXTasksProperty.sort);
        JSONToOX_Tasks.put(TasksProperty.sortOrder, OXTasksProperty.order);
        JSONToOX_Tasks.put(TasksProperty.moveToFolderId, OXTasksProperty.folder);

        // Task Base columns
        Tasks_Id.put(TasksProperty.taskId.name(), "1");
        Tasks_Id.put(TasksProperty.folderId.name(), "20");
        Tasks_Id.put(TasksProperty.name.name(), "200");
        Tasks_Id.put(TasksProperty.owner.name(), "224");
        Tasks_Id.put(TasksProperty.priority.name(), "309");
        Tasks_Id.put(TasksProperty.status.name(), "300");
        Tasks_Id.put(TasksProperty.isPrivate.name(), "101");
        Tasks_Id.put(TasksProperty.colorLabel.name(), "102");
        Tasks_Id.put(TasksProperty.categories.name(), "100");
        Tasks_Id.put(TasksProperty.notes.name(), "203");
        
        Tasks_Id.put(TasksProperty.createdDate.name(), "4");
        Tasks_Id.put(TasksProperty.updatedDate.name(), "5");

        Tasks_Id.put(TasksProperty.startDate.name(), "201");
        Tasks_Id.put(TasksProperty.dueDate.name(), "202");
        Tasks_Id.put(TasksProperty.completedDate.name(), "315");
        Tasks_Id.put(TasksProperty.reminderDate.name(), "204");
        Tasks_Id.put(TasksProperty.progress.name(), "301");
    }

    /**
     * Default private constructor.
     */
    private OXTasksUtil() {

    }
}
