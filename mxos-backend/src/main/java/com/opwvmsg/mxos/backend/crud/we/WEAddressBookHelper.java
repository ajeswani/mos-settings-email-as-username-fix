/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.we;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IAddressBookHelper;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.we.WEAddressBookCRUD.ImProvider;
import com.opwvmsg.mxos.backend.crud.we.utils.ReverseEnumMap;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.replystore.Reply;
import com.opwvmsg.utils.paf.intermail.replystore.ops.ReadReply;
import com.opwvmsg.utils.paf.intermail.replystore.ops.UpdateReply;


/**
 * Class to implement methods WE AddressBook Helper Methods 
 * the response
 * 
 * @author mxos-dev
 * 
 */
public class WEAddressBookHelper implements IAddressBookHelper {

    private static Logger logger = Logger.getLogger(WEAddressBookHelper.class);

    public static String getAutoReplyText(String autoReplyHost, String mailboxId, String realm)
            throws AddressBookException {
        CRUDProtocol protocol = CRUDUtils.resolveBackendProtocolByHostName(autoReplyHost);
        RmeDataModel rmeDataModel = (protocol == CRUDProtocol.httprme) ? RmeDataModel.Leopard : RmeDataModel.CL; 
        ReadReply readReplyOp = new ReadReply(autoReplyHost, mailboxId, realm,
                Reply.WEBEDGE_ADDRESSBOOK, rmeDataModel);
        try {
            readReplyOp.execute();
        } catch (IntermailException e) {
            logger.error("Exception while connecting to AddressBook provider",
                    e);
            throw new AddressBookException(
                    AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                    ExceptionUtils.CONNECTION_EXCEPTION_CATEGORY,
                    ExceptionUtils.MSS_CONNECTION_ERROR_CODE,
                    AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(), e);
        }
        Reply reply = readReplyOp.getReply();
        return reply.getText();
    }

    // Returns buddy name
    public static String getBuddyNameFromEmail(String email) {
        return email.substring(0, email.indexOf(MxOSConstants.AT_THE_RATE));
    }

    // Return email address from buddy name and provider
    public static String getEmailFromBuddyNameAndImProvider(String buddyName,
            String imProvider) throws AddressBookException {
        return new StringBuilder()
                .append(buddyName)
                .append(MxOSConstants.AT_THE_RATE)
                .append(ReverseEnumMap.lookup(ImProvider.class, imProvider)
                        .convert()).toString();
    }

    // Returns provider based on email domain
    public static ImProvider getImProviderFromEmail(String email) {
        String domain = email.substring(
                email.indexOf(MxOSConstants.AT_THE_RATE) + 1).toLowerCase();
        ReverseEnumMap<ImProvider> map = new ReverseEnumMap<ImProvider>(
                ImProvider.class);
        return (ImProvider) map.get(domain);
    }

    public static void setAutoReplyText(String autoReplyHost, String mailboxId, String realm,
            String text) throws AddressBookException {
        CRUDProtocol protocol = CRUDUtils.resolveBackendProtocolByHostName(autoReplyHost);
        RmeDataModel rmeDataModel = (protocol == CRUDProtocol.httprme) ? RmeDataModel.Leopard : RmeDataModel.CL; 
        if (rmeDataModel == RmeDataModel.CL) {
            ReadReply readReplyOp = new ReadReply(autoReplyHost, mailboxId, realm,
                    Reply.WEBEDGE_ADDRESSBOOK, rmeDataModel);
            try {
                readReplyOp.execute();
            } catch (IntermailException e) {
                logger.error(
                        "Exception while connecting to AddressBook provider", e);
                throw new AddressBookException(
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        ExceptionUtils.CONNECTION_EXCEPTION_CATEGORY,
                        ExceptionUtils.MSS_CONNECTION_ERROR_CODE,
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        e);
            }
            Reply reply = readReplyOp.getReply();
            reply.setText(text);
            UpdateReply updateReplyOp = new UpdateReply(autoReplyHost,
                    mailboxId, realm, reply, rmeDataModel);
            try {
                updateReplyOp.execute();
            } catch (IntermailException e) {
                logger.error(
                        "Exception while connecting to AddressBook provider", e);
                throw new AddressBookException(
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        ExceptionUtils.CONNECTION_EXCEPTION_CATEGORY,
                        ExceptionUtils.MSS_CONNECTION_ERROR_CODE,
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        e);
            }
        } else {
            try {
                Reply reply = new Reply();
                reply.setText(text);
                reply.setType(Reply.WEBEDGE_ADDRESSBOOK);
                UpdateReply updateReplyOp = null;
                if (!text.isEmpty()) {
                    updateReplyOp = new UpdateReply(autoReplyHost, mailboxId, realm,
                            1, reply, rmeDataModel);
                } else {
                    updateReplyOp = new UpdateReply(autoReplyHost, mailboxId, realm,
                            2, reply, rmeDataModel);
                }
                updateReplyOp.execute();
            } catch (IOException e) {
                logger.error(
                        "Exception while connecting to AddressBook provider", e);
                throw new AddressBookException(
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        ExceptionUtils.CONNECTION_EXCEPTION_CATEGORY,
                        ExceptionUtils.MSS_CONNECTION_ERROR_CODE,
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        e);
            } catch (IntermailException e) {
                logger.error(
                        "Exception while connecting to AddressBook provider", e);
                throw new AddressBookException(
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        ExceptionUtils.CONNECTION_EXCEPTION_CATEGORY,
                        ExceptionUtils.MSS_CONNECTION_ERROR_CODE,
                        AddressBookError.ABS_PROVIDER_CONNECTION_ERROR.name(),
                        e);
            }
        }
    }

    private Map<String, String> getAttributes(MxOSRequestState requestState) {
        IBackendState backendState = requestState.getBackendState().get(
                AddressBookDBTypes.we.name());
        if (backendState == null) {
            backendState = new WEAddressBookBackendState();
            requestState.getBackendState().put(AddressBookDBTypes.we.name(),
                    backendState);
        }
        return ((WEAddressBookBackendState) backendState).attributes;
    }

    @Override
    public void setAttribute(final MxOSRequestState mxosRequestState,
            final AddressBookProperty key, final Object value)
            throws InvalidRequestException {
        if (WEAddressBookUtil.JSONToWE_AddressBook.get(key) != null) {
            getAttributes(mxosRequestState).put(
                    WEAddressBookUtil.JSONToWE_AddressBook.get(key).name(),
                    value.toString());
        }
    }

    private List<Map<String, String>> getAttributesList(
            MxOSRequestState requestState, int size) {
        IBackendState backendState = requestState.getBackendState().get(
                AddressBookDBTypes.we.name());
        if (backendState == null) {
            backendState = new WEAddressBookBackendState();
            requestState.getBackendState().put(AddressBookDBTypes.we.name(),
                    backendState);
        }
        WEAddressBookBackendState state = ((WEAddressBookBackendState) backendState);
        if (state.attributesList.size() == 0) {
            state.initialize(size);
        }
        return state.attributesList;
    }

    @Override
    public void setAttributes(final MxOSRequestState mxosRequestState,
            final AddressBookProperty key, final Object[] values)
            throws InvalidRequestException {
        for (int i = 0; i < values.length; i++) {
            if (WEAddressBookUtil.JSONToWE_AddressBook.get(key) != null) {
                Map<String, String> map = getAttributesList(mxosRequestState,
                        values.length).get(i);
                map.put(WEAddressBookUtil.JSONToWE_AddressBook.get(key).name(),
                        (String) values[i]);
            }
        }
    }
}
