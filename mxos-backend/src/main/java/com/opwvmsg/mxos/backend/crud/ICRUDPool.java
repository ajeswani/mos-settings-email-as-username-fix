/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/IConnectionPool.java#1 $
 */

package com.opwvmsg.mxos.backend.crud;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * CRUD pool interface.
 *
 * @param <T> This describes CRUD type.
 * @author Jaydeep
 */
public interface ICRUDPool<T> {

    /**
     * Method to get CRUD object.
     *
     * @return CRUD object.
     * @throws Exception
     *             Exception.
     */
    T borrowObject() throws MxOSException;

    /**
     * Method to return CRUD object back to the pool.
     *
     * @param CRUDObject
     *            CRUD object which will be submitted to pool.
     * @throws Exception
     *             Exception.
     */
    void returnObject(T CRUDObject) throws MxOSException;
    
    /**
     * 
     * Method to reset the connection pools on disconnected status.
     * @throws Exception
     */
    void resetPool() throws MxOSException;
}

