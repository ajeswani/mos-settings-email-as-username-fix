/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.validator.XMLActionHandler.ParamTuple;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchema;

/**
 * The MxosValidator class is used to validate all request parameters.
 * 
 * @author mxos-dev
 */
public final class InputValidator {
    private static Logger logger = Logger.getLogger(InputValidator.class);

    /**
     * Default private Constructor.
     */
    private InputValidator() {
    }

    /**
     * populateActions based on request params.
     * 
     * @param requestBean request
     * @return map of operation rules
     */
    public static List<String> populateActions(
            final Map<String, List<String>> inputParams, String operationName)
        throws MxOSException {
        final List<String> actionQueue = new ArrayList<String>();

        final Map<String, OperationRule> operations =
                MxOSApp.getInstance().getRules().getOperations();
        final OperationRule operationRule = operations.get(operationName);
        if (operationRule
                .getActionRules() == null || operationRule
                .getActionRules().entrySet() == null) {
            StringBuffer sb =  new StringBuffer(
                    "Actions not found for the operation : " + operationName);
            if (logger.isDebugEnabled()) {
                logger.debug(sb);
            }
            throw new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), sb.toString());
        }

        for (Entry<String, Map<String, ParamTuple>> actionRule : operationRule
                .getActionRules().entrySet()) {

            if (actionRule.getValue().isEmpty()) {
                actionQueue.add(actionRule.getKey());
            } else {
                for (final Entry<String, ParamTuple> ruleParam : actionRule
                        .getValue().entrySet()) {
                    if (inputParams.containsKey(ruleParam.getKey())) {
                        // Do not add the same action if it is already added.
                        if (!actionQueue.contains(actionRule.getKey())) {
                            actionQueue.add(actionRule.getKey());
                        }
                    }
                }
            }
        }
        return actionQueue;
    }

    /**
     * Method to validate the given Request Object.
     * 
     * @param requestBean RequestBean
     * @param responseBean ResponseBean
     * @return boolean Returns true if the given data is valid.
     * @throws InvalidRequestException
     * @throws Exception incase of error during validation
     */
    public static void validateInputParams(
            final Map<String, List<String>> inputParams,
            final String operationName) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "validateAndExecuteService() start for : " + operationName));
        }
        final Map<String, OperationRule> operations =
                MxOSApp.getInstance().getRules().getOperations();
        final OperationRule operationRule = operations.get(operationName);
        if (operationRule == null) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name());
        }

        // Validate main params of respective Operation
        boolean oneMandatoryPairIsValid = false;
        for (final Map<String, ParamTuple> mainParamRule : operationRule
                .getMandatoryParamRules()) {
            boolean allParamPresent = true;
            for (final Entry<String, ParamTuple> requiredParam : mainParamRule
                    .entrySet()) {
                if (inputParams.containsKey(requiredParam.getKey())) {
                    if (inputParams.get(requiredParam.getKey()) == null ||
                            inputParams.get(requiredParam.getKey()).size() == 0) {
                        throw new InvalidRequestException(
                                requiredParam.getValue().getErrorCode());
                    }
                    for (int i = 0; i < inputParams.get(requiredParam.getKey()).size(); i++) {
                        boolean isValid =
                                jsonValidate(MxOSApp.getInstance()
                                        .getApplicationName(), requiredParam
                                        .getValue().getType(),
                                        inputParams.get(requiredParam.getKey())
                                                .get(i));
                        if (!isValid) {
                            throw new InvalidRequestException(
                                    requiredParam.getValue().getErrorCode());
                        }
                    }
                } else {
                    allParamPresent = false;
                    break;
                }
            }
            if (allParamPresent) {
                oneMandatoryPairIsValid = true;
            }
        }
        if (!oneMandatoryPairIsValid) {
            throw new InvalidRequestException(operationRule.getErrorCode());
        }
        // validate optional params
        if (operationRule.getOptionalParamRules().size() > 0) {
            // Validate the optional params of respective Operation
            // Should validate only if the optional param is exists in request
            for (final Map<String, ParamTuple> optParamRule : operationRule
                    .getOptionalParamRules()) {
                for (final Entry<String, ParamTuple> optParam : optParamRule
                        .entrySet()) {
                    if (inputParams.containsKey(optParam.getKey())) {
                        if (inputParams.get(optParam.getKey()) == null ||
                                inputParams.get(optParam.getKey()).size() == 0) {
                            throw new InvalidRequestException(
                                    optParam.getValue().getErrorCode());
                        }
                        for (int i = 0; i < inputParams.get(optParam.getKey()).size(); i++) {
                            boolean isValid =
                                    jsonValidate(MxOSApp.getInstance()
                                            .getApplicationName(), optParam
                                            .getValue().getType(),
                                            inputParams.get(optParam.getKey())
                                                    .get(i));
                            if (!isValid) {
                                throw new InvalidRequestException(
                                        optParam.getValue().getErrorCode());
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * Method to validate the given param and value against the schema.
     * 
     * @param param Parameter name
     * @param value Parameter value
     * @return boolean Returns true if the given value is true against param
     *         schema.
     * @throws Exception in case of any error during json validation
     */
    private static boolean jsonValidate(String appName, String param,
            String value) throws MxOSException {
        // TODO: Instead of returning boolean, just throw exception.
        if (param == null || param.equals("") || value == null) {
            logger.debug("Param or value are null or empty. param = " + param);
            return false;
        }
        // do not display password
        if (logger.isDebugEnabled()) {
            if (param.contains("password")) {
                logger.debug("jsonValidate --> " + param + " = XXXXXXXXXXXX ");
            } else {
                logger.debug("jsonValidate --> " + param + " = " + value);
            }
        }

        /*
         * Avoid re-reading of files again and again.
         */
        final JSONSchema schema = MxOSApp.getInstance().getSchema(param);
        if (value.contains("\\")) {
            value = value.replace("\\", "\\\\");
        }
        if (value.contains("\"")) {
            value = value.replace("\"", "\\\"");
        }
        // Validates a JSON Instance object
        final StringBuffer jsonParam = new StringBuffer();
        jsonParam.append(MxOSConstants.DOUBLE_QUOTE);
        jsonParam.append(value);
        jsonParam.append(MxOSConstants.DOUBLE_QUOTE);

        // logger.debug("jsonParam --> " + jsonParam);
        final List<String> errors = schema.validate(jsonParam.toString());
        // boolean intValid = false;
        if (errors != null && errors.size() > 0) {
            // Display the eventual errors
            if (logger.isDebugEnabled()) {
                for (String s : errors) {
                    logger.debug("Validation Error = " + s);
                }
            }
            return false;
        }

        return true;
    }
}
