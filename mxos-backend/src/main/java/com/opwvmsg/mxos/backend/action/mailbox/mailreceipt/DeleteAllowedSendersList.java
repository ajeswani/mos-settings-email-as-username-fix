/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to delete allowed senders list.
 *
 * @author mxos-dev
 */
public class DeleteAllowedSendersList implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(DeleteAllowedSendersList.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteAllowedSendersList action start."));
        }
        try {
            final String removeAllowedSender = requestState.getInputParams()
                    .get(MailboxProperty.allowedSender.name()).get(0);

            List<String> allowedSendersList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.allowedSendersList);

            if (allowedSendersList == null) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ALLOWED_SENDER.name());
            } 
            if (!allowedSendersList.remove(removeAllowedSender.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ALLOWED_SENDER.name());
            }
            final String[] allowedSendersArray = allowedSendersList
                    .toArray(new String[allowedSendersList.size()]);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.allowedSendersList,
                            allowedSendersArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while delete allowed senders list.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_ALLOWED_SENDERS_DELETE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteAllowedSendersList action end."));
        }
    }
}
