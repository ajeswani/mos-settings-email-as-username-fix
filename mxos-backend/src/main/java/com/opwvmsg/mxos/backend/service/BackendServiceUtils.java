/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.validator.InputValidator;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.HandlerStats;

/**
 * Util class of BackendServices.
 * 
 * @author mxos-dev
 */
public final class BackendServiceUtils {
    private static Logger logger = Logger.getLogger(BackendServiceUtils.class);

    /**
     * Default private constructor.
     */
    private BackendServiceUtils() {

    }

    /**
     * Method to validate and execute the given operations.
     * 
     * @param mxosRequestState - request params
     * @param operationName - Name of the operation to be performed
     * @throws MxOSException - on any exception
     */
    public static void validateAndExecuteService(
            final MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            HandlerStats.ACTIVE_REQUESTS.increment();
            if (MxOSApp.getInstance().enforceValidation()) {
                try {
                    InputValidator.validateInputParams(
                            mxosRequestState.getInputParams(),
                            mxosRequestState.getOperationName());
                } catch (MxOSException e) {
                    logger.warn("Validation Failed.", e);
                    throw e;
                }
            }
            final List<String> actionsQueue = InputValidator.populateActions(
                    mxosRequestState.getInputParams(),
                    mxosRequestState.getOperationName());
            MxOSApp.getInstance().getActionFactory()
                    .exec(actionsQueue, mxosRequestState);
        } catch (final MxOSException e) {
            if (e.getCode() == null || e.getCode().equals("")) {
                e.setCode(ErrorCode.GEN_INTERNAL_ERROR.name());
            }
            // password hide implementation
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.password.name())) {
                List<String> passwordList = mxosRequestState.getInputParams()
                        .get(MailboxProperty.password.name());
                passwordList.clear();
                passwordList.add("XXXXX");
            }
            e.setRequestParams(mxosRequestState.getInputParams().toString());
            e.setOperationType(mxosRequestState.getOperationName());
            ExceptionUtils.prepareMxOSErrorObject(e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error while validation.", e);
            ApplicationException e1 = new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            ExceptionUtils.prepareMxOSErrorObject(e1);
            throw e1;
        } finally {
            HandlerStats.ACTIVE_REQUESTS.decrement();
        }
    }

}
