/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/data/BooleanEnum.java#1 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * @author Aricent
 * 
 */
public enum Boolean implements DataMap.Property {
    Yes("yes"), No("no");

    private static final Map<String, Boolean> BOOLEAN_MAP = new HashMap<String, Boolean>();

    private String mode;

    static {
        // Used for MailAccess Enabled
/*        BOOLEAN_MAP.put("all", Yes);
        BOOLEAN_MAP.put("trusted", Yes);
        BOOLEAN_MAP.put("none", No);
*/
        BOOLEAN_MAP.put("1", Yes);
        BOOLEAN_MAP.put("0", No);

/*        // Used for copyOnForward
        BOOLEAN_MAP.put("P", Yes);
        BOOLEAN_MAP.put("N", No);*/

    }

    private Boolean(String mode) {
        this.mode = mode;
    }

    /**
     * 
     * @param str
     *            mode
     * @return mode enum
     * @throws InvalidRequestException
     *             if enum not found for given data
     */
    public static Boolean getEnum(String str)
            throws InvalidRequestException {
        if (str == null) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied");
        } else {
            return BOOLEAN_MAP.get(str);
        }
    }

    /**
     * 
     * @param str
     *            mode
     * @return mode enum
     * @throws InvalidRequestException
     *             if enum not found for given data
     */
    public static String getString(String str) throws InvalidRequestException {
        if (str == null) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied");
        } else {
            if (BOOLEAN_MAP.get(str) != null) {
                return BOOLEAN_MAP.get(str).getMode();
            } else {
                throw new InvalidRequestException(
                        ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied");
            }
        }
    }

    /**
     * 
     * @param str
     *            mode
     * @return Mode Enum
     * @throws InvalidRequestException
     *             if enum not found for given data
     */
    public static String getModeKey(String str) throws InvalidRequestException {
        if (str == null) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied");
        }
        for (Entry<String, Boolean> entry : BOOLEAN_MAP.entrySet()) {
            if (entry.getValue().getMode().equalsIgnoreCase(str)) {
                return entry.getKey();
            }
        }
        throw new InvalidRequestException(
                ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied");
    }

    /**
     * Returns the corresponding {@link String}.
     * 
     * @return password hash type
     */
    public String getMode() {
        return mode;
    }
}
