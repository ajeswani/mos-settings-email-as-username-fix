/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:  $
 */

package com.opwvmsg.mxos.backend.crud.mss;

import java.io.ByteArrayInputStream;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.MessagingException;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MailDateFormat;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.Folder;
import com.opwvmsg.utils.paf.intermail.mail.Inventory;
import com.opwvmsg.utils.paf.intermail.mail.Mailbox;
import com.opwvmsg.utils.paf.intermail.mail.Msg;
import com.opwvmsg.utils.paf.intermail.mail.Msg.MssFlagOrder;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.mail.ops.CopyMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMsg;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.GetLastAccessTime;
import com.opwvmsg.utils.paf.intermail.mail.ops.MoveMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.ReadFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.ReadMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.ReadMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.RenameFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.UpdateMessageBlob;
import com.opwvmsg.utils.paf.intermail.mail.ops.UpdateMsgs;
import com.opwvmsg.utils.paf.intermail.replystore.Reply;
import com.opwvmsg.utils.paf.intermail.replystore.ops.ReadReply;
import com.opwvmsg.utils.paf.intermail.replystore.ops.UpdateReply;
import com.opwvmsg.utils.paf.util.Utilities;
import org.apache.commons.codec.binary.Base64;

/**
 * Mss CRUD APIs to access metadata.
 * 
 * @author Satyam
 */
public class MssMetaCRUD implements IMetaCRUD, IBlobCRUD {

    private static Logger logger = Logger.getLogger(MssMetaCRUD.class);

    protected static final int ONE_K = 1024;
    protected static final double HALF = 0.5;
    protected static final String HEADER_TO = "To";
    protected static final String HEADER_FROM = "From";
    protected static final String HEADER_CC = "Cc";
    protected static final String HEADER_SUBJECT = "Subject";

    protected static final String HEADER_BCC = "bcc";
    protected static final String HEADER_SENDER = "sender";
    protected static final String HEADER_REPLYTO = "replyto";
    protected static final String HEADER_INREPLYTO = "inreplyto";
    protected static final String HEADER_MESSAGEID = "msgid";
    protected static final String HEADER_DATE = "date";
    protected static final String HEADER_BODYSTRUCTURE = "bodystructure";

    protected static final String COMMA_SEPERATOR = ",";
    protected final Map<MssMetaCRUD.SystemFolder, String> systemFolders;
    protected final List<String> systemfolders;
    
    // TODO: HACK: Remove this when we remove mmsCustomExpireTime() method.
    protected final String InvalidExpireTime = "-1";

    /**
     * Default system folders.
     */
    public enum SystemFolder {
        INBOX, DRAFTS, SentMail, Trash
    }

    protected Map<SystemFolder, String> sys2fid;
    protected Map<String, SystemFolder> fid2sys;

    protected RmeDataModel myDataModel;

    /**
     * Constructor.
     * 
     * @param javaMailProps javaMailProps
     * @param systemFolders systemFolders
     * @throws Exception Exception.
     */
    public MssMetaCRUD() {
        this.systemFolders = new HashMap<MssMetaCRUD.SystemFolder, String>();
        for (SystemFolder folder : SystemFolder.values()) {
            systemFolders.put(folder, folder.name());
        }
        setSystemFolderMapping(systemFolders);
        systemfolders = MxOSConfig.getDefaultFolders();
        myDataModel = RmeDataModel.CL;
    }

    /**
     * Get Root Folder.
     * 
     * @return returns Folder
     * @throws MessagingException Throws Jamil Mail Exception.
     */
    protected Folder getRootFolder(MxOSRequestState mxosRequestState)
            throws Throwable {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String id = String.valueOf(info.getMailboxId());
        String pathName = "/";
        if (logger.isDebugEnabled()) {
            logger.debug("Read Mailbox: Reading Mailbox folders for mailbox"
                    + "id: " + id);
        }
        boolean optionSupressMers = getBooleanOption(mxosRequestState,
                FolderProperty.optionSupressMers.name());
        ReadFolder op = new ReadFolder(mssHost, id, pathName,
                Mailbox.ACCESS_GENERIC_END_USER,
                ReadFolder.FOLDER_SORT_NATURAL, getOptions(true, false, true,
                        optionSupressMers), null, null, -1, -1, false, true);
        op.execute(myDataModel);
        return op.getFolder();
    }

    /**
     * 
     * @param mxosRequestState
     * @param folderName
     * @return
     * @throws Throwable
     */
    protected Folder getFolder(MxOSRequestState mxosRequestState,
            String folderName) throws Throwable {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        boolean optionSupressMers = getBooleanOption(mxosRequestState,
                FolderProperty.optionSupressMers.name());
        ReadFolder op = new ReadFolder(host, mailboxId, folderName, accessId,
                ReadFolder.FOLDER_SORT_NATURAL, getOptions(true, false, true,
                        optionSupressMers), null, null, -1, -1, false, true);
        op.execute(myDataModel);
        return op.getFolder();
    }

    /**
     * Populate each Folder recursively to a list of folder beans.
     * 
     * @param folder intermail folder
     * @param result list of folders
     */
    protected void populateFolderInventory(Folder folder,
            List<com.opwvmsg.mxos.message.pojos.Folder> result) {
        if (folder != null) {
            com.opwvmsg.mxos.message.pojos.Folder beanFolder = new com.opwvmsg.mxos.message.pojos.Folder();
            if (!folder.getPathName().equals(MxOSConstants.FORWARD_SLASH)) {
                beanFolder.setFolderName(MxOSConstants.FORWARD_SLASH
                        + folder.getPathName());
                beanFolder.setNextUID((long) folder.getNextUID());
                beanFolder.setUidValidity((long) folder.getUIDValidity());
                beanFolder.setNumMessages(folder.getNumMsgs());
                beanFolder.setNumReadMessages(folder.getNumMsgs()
                        - folder.getNumUnreadMessages());
                beanFolder.setNumUnreadMessages(folder.getNumUnreadMessages());
                beanFolder.setFolderSizeBytes((long) (folder.getNumBytes()));
                Inventory[] invArray = folder.getInventoriesArray();
                for (int j = 1; j < invArray.length; j++) {
                    Inventory inv = invArray[j];
                    if (invArray[j].getUnread() == true) {
                        beanFolder.setFolderSizeReadBytes(folder.getNumBytes()
                                - invArray[j].getMsgBytes());
                        beanFolder.setFolderSizeUnreadBytes(invArray[j]
                                .getMsgBytes());
                    }
                    if (invArray[j].getUnread() == false) {
                        beanFolder.setFolderSizeReadBytes(invArray[j]
                                .getMsgBytes());
                        beanFolder.setFolderSizeUnreadBytes(folder.getNumBytes()
                                - invArray[j].getMsgBytes());
                    }
                }

                result.add(beanFolder);
            }
            Folder[] subFolders = folder.getSubFoldersArray();
            if (subFolders != null && subFolders.length > 0) {
                for (Folder subFolder : subFolders) {
                    populateFolderInventory(subFolder, result);
                }
            }
        }
    }

    /**
     * Sets system folder mapping.
     * 
     * @param map system folder mapping
     */
    protected void setSystemFolderMapping(Map<SystemFolder, String> map) {
        sys2fid = map;

        // create reverse lookup map
        fid2sys = new HashMap<String, SystemFolder>();
        for (Map.Entry<SystemFolder, String> entry : sys2fid.entrySet()) {
            fid2sys.put(entry.getValue(), entry.getKey());
        }
    }

    /**
     * method to convert to kb by usning math.ceil.
     */
    public long toKB(long byteCount) {
        return (long) Math.ceil(((double) byteCount) / ONE_K - HALF);
    }

    /**
     * Returns the system folder corresponding to the specified folder object.
     * 
     * @param folderName folderName String
     * @return corresponding system folder, null if it's not a system folder
     */
    protected SystemFolder toSystemFolder(String folderName) {
        return fid2sys.get(folderName);
    }

    /**
     * @return the systemFolders
     */
    public Map<MssMetaCRUD.SystemFolder, String> getSystemFolders() {
        return systemFolders;
    }

    /**
     * Gets the mailbox object returned from the MSS.
     * 
     * @return Mailbox object for mailbox id
     * @throws Exception Throws Exception.
     */
    public Mailbox getMailBox(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            String id = mxosRequestState.getAdditionalParams().getProperty(
                    MailboxProperty.mailboxId);
            String mssHost = mxosRequestState.getAdditionalParams()
                    .getProperty(MailboxProperty.messageStoreHost);
            if (logger.isDebugEnabled()) {
                logger.debug("Getting mailbox object for mailbox id: " + id);
            }
            ReadMailbox op = new ReadMailbox(mssHost, id);
            op.execute(myDataModel);
            return op.getMailbox();
        } catch (IntermailException e) {
            logger.error("Exception while getting mailbox.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while get mailbox.", e);
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_GET.name(), e);
        }
    }

    /**
     * 
     * @param recurse expects boolean value
     * @param recentOnly expects boolean value
     * @param noMessageUids expects boolean value
     * @return returns int foe Options
     */
    protected int getOptions(boolean recurse, boolean recentOnly,
            boolean noMessageUids, boolean suppressMers) {
        boolean useSimpleInventory = true;
        // boolean suppressMers = false;
        int options = ReadFolder.READFOLDER_NOMESSAGEIDS;
        if (useSimpleInventory) {
            options |= ReadFolder.READFOLDER_SIMPLE_INVENTORY;
        }
        if (suppressMers) {
            options |= ReadFolder.READFOLDER_SUPPRESSMERS;
        }
        if (recurse) {
            options |= ReadFolder.READFOLDER_RECURSIVE;
        }
        if (recentOnly) {
            options |= ReadFolder.READFOLDER_RECENT;
        }
        if (noMessageUids) {
            options |= ReadFolder.READFOLDER_NOMESSAGEDETAILS;
        }
        return options;
    }

    /**
     * Get AutoReply Message from MSS.
     * 
     * @param dataMap DataMap object
     * @return String autoReplyMessage
     * @throws Exception in case of any communication failure with MSS.
     */
    protected String getAutoReplyText(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String autoReplyHost = info.getAutoReplyHost();
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("AutoReplyHost : ")
                        .append(autoReplyHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            String autoReplyMessage = "";
            ReadReply op = new ReadReply(autoReplyHost, mailboxId, realm,
                    Reply.AUTOREPLY, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            autoReplyMessage = reply.getText();
            return autoReplyMessage;
        } catch (IntermailException e) {
            logger.error("Error while get auto-reply text.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_AUTO_REPLY_MESSAGE.name(), e);
        } catch (Exception e) {
            logger.error("Error while get auto-reply text.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_AUTO_REPLY_MESSAGE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while get auto-reply text.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_AUTO_REPLY_MESSAGE.name(), t);
        }
    }

    /**
     * Set AutoReply Message to MSS.
     * 
     * @param mxosRequestState MxOSRequestState object
     * @param autoReplyMessage message to set
     * @throws Exception in case of any communication failure with MSS.
     */
    protected void setAutoReplyText(MxOSRequestState mxosRequestState,
            String autoReplyMessage) throws Exception {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String autoReplyHost = info.getAutoReplyHost();
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("AutoReplyHost : ")
                        .append(autoReplyHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            ReadReply op = new ReadReply(autoReplyHost, mailboxId, realm,
                    Reply.AUTOREPLY, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            if (reply != null) {
                reply.setText(autoReplyMessage);
                UpdateReply updateOpr = new UpdateReply(autoReplyHost,
                        mailboxId, realm, reply, myDataModel);
                updateOpr.execute(myDataModel);
            }
        } catch (IntermailException e) {
            logger.error("Error while set auto-reply text.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE.name(), e);
        } catch (Exception e) {
            logger.error("Error while set auto-reply text.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while set auto-reply text.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE.name(), t);
        }
    }

    /**
     * Get MTA Filter from MSS.
     * 
     * @param dataMap DataMap object
     * @return String MTA filter
     * @throws Exception in case of any communication failure with MSS.
     */
    protected String getMTAFilter(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append(
                        "Reading filter from host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            String filter = "";
            ReadReply op = new ReadReply(host, mailboxId, realm,
                    Reply.USERFILTER, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            filter = reply.getText();
            return filter;
        } catch (IntermailException e) {
            logger.error("Error while get MTA filter.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_MAILRECEIPT_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while get MTA filter.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_MAILRECEIPT_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while get MTA filter.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_MAILRECEIPT_GET.name(), t);
        }
    }

    /**
     * Get Signature from MSS.
     * 
     * @param dataMap DataMap object
     * @return String signature
     * @throws Exception in case of any communication failure with MSS.
     */
    protected String getSignature(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost = info.getAutoReplyHost();
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("MssHost : ").append(
                        mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            String signature = null;
            if (logger.isDebugEnabled()) {
                logger.debug("Reading signature from host : " + mssHost);
            }
            ReadReply op = new ReadReply(mssHost, mailboxId, realm,
                    Reply.SIGNATURE, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            signature = reply.getText();
            return signature;
        } catch (IntermailException e) {
            logger.error("Error while get signature.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_SIGNATURE.name(), e);
        } catch (Exception e) {
            logger.error("Error while get signature.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_SIGNATURE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while get signature.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_SIGNATURE.name(), t);
        }
    }

    /**
     * Set MTA Filter to MSS.
     * 
     * @param mxosRequestState MxOSRequestState object
     * @param MTA filter to set
     * @throws Exception in case of any communication failure with MSS.
     */
    protected void setMTAFilter(MxOSRequestState mxosRequestState, String filter)
            throws Exception {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            ReadReply op = new ReadReply(host, mailboxId, realm,
                    Reply.USERFILTER, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            if (reply != null) {
                reply.setText(filter);
                UpdateReply updateOpr = new UpdateReply(host, mailboxId, realm,
                        reply, myDataModel);
                updateOpr.execute(myDataModel);
            }
        } catch (IntermailException e) {
            logger.error("Error while set MTA filter.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                            .name(),
                    e);
        } catch (Exception e) {
            logger.error("Error while set MTA filter.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                            .name(),
                    e);
        } catch (Throwable t) {
            logger.error("Error while set MTA filter.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                            .name(),
                    t);
        }
    }

    /**
     * Set Signature to MSS.
     * 
     * @param mxosRequestState MxOSRequestState object
     * @param signature message to set
     * @throws Exception in case of any communication failure with MSS.
     */
    protected void setSignature(MxOSRequestState mxosRequestState,
            String signature) throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost = info.getMessageStoreHosts().get(0);
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("MssHost : ").append(
                        mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            ReadReply op = new ReadReply(mssHost, mailboxId, realm,
                    Reply.SIGNATURE, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            if (reply != null) {
                reply.setText(signature);
                UpdateReply updateOpr = new UpdateReply(mssHost, mailboxId,
                        realm, reply, myDataModel);
                updateOpr.execute(myDataModel);
            }
        } catch (IntermailException e) {
            logger.error("Error while set signature.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), e);
        } catch (Exception e) {
            logger.error("Error while set signature.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while set signature.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), t);
        }
    }

    /**
     * Get RM Filter from MSS.
     * 
     * @param dataMap DataMap object
     * @return String RM filter
     * @throws Exception in case of any communication failure with MSS.
     */
    protected String getRMFilter(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append(
                        "Reading filter from host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            String filter = "";
            ReadReply op = new ReadReply(host, mailboxId, realm,
                    Reply.WEBEDGE_META_FILTERS, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            filter = reply.getText();
            return filter;
        } catch (IntermailException e) {
            logger.error("Error while get RM filter.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_MAILRECEIPT_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while get RM filter.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_MAILRECEIPT_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while get RM filter.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_MAILRECEIPT_GET.name(), t);
        }
    }

    /**
     * Set RM Filter to MSS.
     * 
     * @param mxosRequestState MxOSRequestState object
     * @param RM filter to set
     * @throws Exception in case of any communication failure with MSS.
     */
    protected void setRMFilter(MxOSRequestState mxosRequestState, String filter)
            throws Exception {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            ReadReply op = new ReadReply(host, mailboxId, realm,
                    Reply.WEBEDGE_META_FILTERS, myDataModel);
            op.execute(myDataModel);
            Reply reply = op.getReply();
            if (reply != null) {
                reply.setText(filter);
                UpdateReply updateOpr = new UpdateReply(host, mailboxId, realm,
                        reply, myDataModel);
                updateOpr.execute(myDataModel);
            }
        } catch (IntermailException e) {
            logger.error("Error while set RM filter.", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                            .name(),
                    e);
        } catch (Exception e) {
            logger.error("Error while set RM filter.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                            .name(),
                    e);
        } catch (Throwable t) {
            logger.error("Error while set RM filter.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                            .name(),
                    t);
        }
    }

    @Override
    public void commit() throws ApplicationException {
        // TODO Auto-generated method stub

    }

    @Override
    public void rollback() throws ApplicationException {
        // TODO Auto-generated method stub

    }

    @Override
    public void createMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub
        logger.info("Creating Mailbox in MSS");
        try {
            // check for msslinkinfo
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost;
            String mailboxId;
            if (info != null) {
                mssHost = getMssHost(info.getMessageStoreHosts());
                mailboxId = String.valueOf(info.getMailboxId());
            } else {
                mssHost = mxosRequestState.getAdditionalParams().getProperty(
                        MailboxProperty.messageStoreHost.name());
                mailboxId = mxosRequestState.getAdditionalParams().getProperty(
                        MailboxProperty.mailboxId);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(
                        mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
            }
            CreateMailbox cmbox = new CreateMailbox(mssHost, mailboxId, null,
                    false, 0);
            cmbox.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("mss CreateMailBox method : creation is success");
            }
        } catch (IntermailException e) {
            logger.error("Error while Create mailbox ", e);
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString() != null
                    && e.getFormattedString().contains("MsAlreadyExists")) {
                throw new MxOSException(MailboxError.MBX_ALREADY_EXISTS.name(),
                        e);
            } else {
                throw new MxOSException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            }
        } catch (Exception e) {
            logger.error("Error while create mailbox.", e);
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while create mailbox.", t);
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_CREATE.name(), t);
        }
    }

    @Override
    public void readMailbox(MxOSRequestState mxOSRequestStatus,
            com.opwvmsg.mxos.data.pojos.Base base) throws MxOSException {
        boolean isMailSendOper = ActionUtils
                .isGetMailSendOperation(mxOSRequestStatus.getOperationName());
        // setAccount(mxOSRequestStatus);
        if (isMailSendOper) {
            // Read signature is maintained in the readMailbox method to avoid
            // the overhead of refactoring the Interfaces and its
            // implementations.
            // TODO Need to move out to readSignature at the time next refactor.
            String signature = getSignature(mxOSRequestStatus);
            if (logger.isDebugEnabled()) {
                logger.debug("Signature: " + signature);
            }
            mxOSRequestStatus.getAdditionalParams().setProperty(
                    MailboxProperty.signature, signature);
        } else {
            String autoReplyMessage = getAutoReplyText(mxOSRequestStatus);
            String mtaFilter = getMTAFilter(mxOSRequestStatus);
            String rmFilter = getRMFilter(mxOSRequestStatus);
            if (logger.isDebugEnabled()) {
                logger.debug("autoReplyMessage: " + autoReplyMessage);
                logger.debug("filter: " + mtaFilter);
            }
            mxOSRequestStatus.getAdditionalParams().setProperty(
                    MailboxProperty.autoReplyMessage, autoReplyMessage);
            mxOSRequestStatus.getAdditionalParams().setProperty(
                    MailboxProperty.mtaFilter, mtaFilter);
            mxOSRequestStatus.getAdditionalParams().setProperty(
                    MailboxProperty.rmFilter, rmFilter);
        }
    }

    @Override
    public void updateMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

        if (mxosRequestState != null) {
            // Set the auto reply if auto reply request param exists
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.autoReplyMessage.name())) {
                try {
                    setAutoReplyText(
                            mxosRequestState,
                            mxosRequestState
                                    .getInputParams()
                                    .get(MailboxProperty.autoReplyMessage
                                            .name()).get(0));
                } catch (Exception e) {
                    logger.error(
                            "Error while setting auto reply message(MSS).", e);
                }
            }
            // Set the filter for the account if filter request param exists
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.mtaFilter.name())) {
                try {
                    setMTAFilter(
                            mxosRequestState,
                            mxosRequestState.getInputParams()
                                    .get(MailboxProperty.mtaFilter.name())
                                    .get(0));
                } catch (Exception e) {
                    logger.error("Error while setting MTA filter(MSS).", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                                    .name(), e);
                }
            }
            // Set the filter for the account if filter request param exists
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.rmFilter.name())) {
                try {
                    setRMFilter(
                            mxosRequestState,
                            mxosRequestState.getInputParams()
                                    .get(MailboxProperty.rmFilter.name())
                                    .get(0));
                } catch (Exception e) {
                    logger.error("Error while setting RM filter(MSS).", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                                    .name(), e);
                }
            }
            // Set the signature for the account if signature exists
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.signature.name())) {
                try {
                    setSignature(
                            mxosRequestState,
                            mxosRequestState.getInputParams()
                                    .get(MailboxProperty.signature.name())
                                    .get(0));
                } catch (Exception e) {
                    logger.error("Error while setting signature.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), e);
                }
            }
        }

    }

    @Override
    public void deleteMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("mss DeleteMailBox method : starting");
        }
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(mssHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        boolean suppressMers = true;
        if (mxosRequestState.getInputParams().containsKey(
                MailboxProperty.suppressMers.name())) {
            suppressMers = Boolean.valueOf(mxosRequestState.getInputParams()
                    .get(MailboxProperty.suppressMers.name()).get(0));
        }

        boolean keepAutoReply = false;
        if (mxosRequestState.getInputParams().containsKey(
                MailboxProperty.keepAutoReply.name())) {
            keepAutoReply = Boolean.parseBoolean(mxosRequestState
                    .getInputParams().get(MailboxProperty.keepAutoReply.name())
                    .get(0));
        }

        boolean disableBlobHardDelete = false;
        if (mxosRequestState.getInputParams().containsKey(
                MailboxProperty.disableBlobHardDelete.name())) {
            disableBlobHardDelete = Boolean.parseBoolean(mxosRequestState
                    .getInputParams()
                    .get(MailboxProperty.disableBlobHardDelete.name()).get(0));
        }

        DeleteMailbox del = new DeleteMailbox(mssHost, mailboxId, realm,
                suppressMers, keepAutoReply, disableBlobHardDelete, myDataModel);
        try {
            del.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("mss DeleteMailBox method : deletion is success");
            }
        } catch (IntermailException e) {
            logger.error("Error while deleting mailbox ", e);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } catch (Exception e) {
            logger.error("Error while delete mailbox.", e);
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while delete mailbox.", t);
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_DELETE.name(), t);
        }
    }

    @Override
    public String createFolder(MxOSRequestState mxosRequestState,
            String folderName) throws MxOSException {
        // Remove first char slash if exists
        if (folderName.startsWith(MxOSConstants.FORWARD_SLASH)) {
            folderName = folderName.substring(1);
        }
        // INBOX shall be created during mailbox creation in MSS and it cannot
        // be deleted. Also INBOX cannot be case sensitive. Hence it is not
        // allowed to be created.
        if (MxOSConstants.INBOX.equalsIgnoreCase(folderName)
                || systemfolders.contains(folderName)) {
            throw new ApplicationException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), folderName
                            + " is not allowed to be created");
        }
        if (folderName.contains(MxOSConstants.FORWARD_SLASH)) {
            String[] folders = folderName.split(MxOSConstants.FORWARD_SLASH);
            if (folders.length == 0) {
                logger.warn(folderName + " is not allowed to create");
                throw new InvalidRequestException(
                        FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(),
                        folderName + " is not allowed for creation");
            }
            if (folders[0].equals(MxOSConstants.FORWARD_SLASH)
                    || (!systemfolders.contains(folders[0]) && MxOSConstants.INBOX
                            .equalsIgnoreCase(folders[0]))) {
                logger.warn(folderName + " is not allowed to create");
                throw new InvalidRequestException(
                        FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(),
                        folders[0] + " is not allowed for creation");
            }
        }
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(mssHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }

        boolean optionSupressMers = getBooleanOption(mxosRequestState,
                FolderProperty.optionSupressMers.name());
        int options = 0;
        if (optionSupressMers) {
            options = CreateFolder.CREATE_FOLDER_SUPPRESS_MERS;
        }
        CreateFolder createFolder = new CreateFolder(mssHost, mailboxId,
                folderName, accessId, options);
        try {
            createFolder.execute(myDataModel);
            mxosRequestState.getDbPojoMap().setProperty(
                    FolderProperty.folderId, "");
            if (logger.isDebugEnabled()) {
                logger.debug("Successfully created the folder : " + folderName);
            }
        } catch (IntermailException e) {
            logger.error("Error while creating folder .", e);
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), e);
        }
        return "";
    }

    @Override
    public void updateFolder(MxOSRequestState mxosRequestState)
            throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(mssHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }
        String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);
        String newfolderName = mxosRequestState.getInputParams()
                .get(FolderProperty.toFolderName.name()).get(0);

        if (MxOSConstants.INBOX.equalsIgnoreCase(folderName)
                || systemfolders.contains(folderName)) {
            throw new ApplicationException(
                    FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(), folderName
                            + " is not allowed to be updated");
        }
        boolean optionSupressMers = getBooleanOption(mxosRequestState,
                FolderProperty.optionSupressMers.name());
        int options = 0;
        if (optionSupressMers) {
            options = RenameFolder.RENAME_FOLDER_SUPPRESS_MERS;
        }
        RenameFolder renameFolder = new RenameFolder(mssHost, mailboxId,
                folderName, newfolderName, accessId, options);
        try {
            renameFolder.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("Successfully renamed the folder : " + folderName
                        + " to " + newfolderName);
            }
        } catch (IntermailException e) {
            logger.error("Error while renaming folder .", e);
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(), e);
        }
    }

    @Override
    public void deleteFolder(MxOSRequestState mxosRequestState)
            throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(mssHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }
        String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);
        boolean optionSupressMers = getBooleanOption(mxosRequestState,
                FolderProperty.optionSupressMers.name());
        int options = 0;
        if (optionSupressMers) {
            options = DeleteFolder.DELETE_FOLDER_SUPPRESS_MERS;
        }
        boolean force = false;
        if (mxosRequestState.getInputParams().get(FolderProperty.force.name()) != null)
            force = getForceParam(mxosRequestState.getInputParams()
                    .get(FolderProperty.force.name()).get(0));

        DeleteFolder deleteFolder = new DeleteFolder(mssHost, mailboxId,
                folderName, force, accessId, options);

        try {
            deleteFolder.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("Successfully deleted the folder : " + folderName);
            }
        } catch (IntermailException e) {
            logger.error("Error while deleting folder .", e);
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_DELETE.name(), e);
        }
    }

    /**
     * Force parameter of request state.
     * 
     * @param isAdmin access Id
     * @return
     */
    protected boolean getForceParam(String strForce) {
        if (strForce != null && (Boolean.parseBoolean(strForce) == true)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void createMessageRef(MxOSRequestState mxosRequestState,
            int sharingCount) throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public String createMessage(MxOSRequestState mxosRequestState,
            boolean messageRecent) throws MxOSException {

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String host = null;
        if (mxosRequestState.getInputParams().containsKey(
                MailboxProperty.messageStoreHost.name())) {
            host = mxosRequestState.getInputParams()
                    .get(MailboxProperty.messageStoreHost.name()).get(0);
        } else {
            host = getMssHost(info.getMessageStoreHosts());
        }
        final String mailboxId = String.valueOf(info.getMailboxId());
        final int accessId = getAccessId(info.getIsAdmin());

        final String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);
        final String text = mxosRequestState.getInputParams()
                .get(MxOSPOJOs.message.name()).get(0);
        final String fromAddress = mxosRequestState.getInputParams()
                .get(MxOSPOJOs.receivedFrom.name()).get(0);

        String isPrivate = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.isPrivate.name())) {
            isPrivate = mxosRequestState.getInputParams()
                    .get(MessageProperty.isPrivate.name()).get(0);
        } else
            isPrivate = System
                    .getProperty(SystemProperty.createMessageIsPrivateMsg
                            .name());

        int imapUid = -1;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.uid.name())) {
            imapUid = Integer.parseInt(mxosRequestState.getInputParams()
                    .get(MessageProperty.uid.name()).get(0));
        }

        final String options = System
                .getProperty(SystemProperty.createMessageLegacyRMEOptions
                        .name());

        final String flags = getFlagsString((Flags) mxosRequestState
                .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.messageFlags));

        String[] userFlags = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.keywords.name())) {
            userFlags = mxosRequestState.getInputParams()
                    .get(MessageProperty.keywords.name()).get(0).split(",");
        }
        final String legalUserName = System
                .getProperty(SystemProperty.legalUsername.name());
        final String legalPeerIp = System
                .getProperty(SystemProperty.legalPeerIp.name());
        String msgBase64Encoded = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.msgBase64Encoded.name())) {
            msgBase64Encoded = mxosRequestState.getInputParams()
                    .get(MessageProperty.msgBase64Encoded.name()).get(0);
        }
        byte[] messageArr = null;
        if (Boolean.parseBoolean(msgBase64Encoded)) {
            Base64 base64 = new Base64();
            messageArr = base64.decode(text.getBytes());
        } else {
            messageArr = text.getBytes();
        }

        Map<String, List<String>> inputParams = mxosRequestState
                .getInputParams();

        long arrivalTime = 0;
        final String arrivalName = MessageProperty.arrivalTime.name();
        if (inputParams.containsKey(arrivalName)) {
            arrivalTime = Long.parseLong(inputParams.get(arrivalName).get(0));
        }

        String expireTime = null;
        final String expireTimeName = MessageProperty.expireTime.name();
        if (inputParams.containsKey(expireTimeName)) {
            expireTime = inputParams.get(expireTimeName).get(0);
            if (expireTime.equals(InvalidExpireTime)) {
                expireTime = mmsCustomExpireTime(messageArr, arrivalTime);
            }
        }
        if (expireTime == null) {
            expireTime = System
                    .getProperty(SystemProperty.createMessageExpireTime.name());
        }
        
        if (logger.isDebugEnabled()) {
            logger.debug("messageStoreHost : " + host + "\tmailboxId : "
                    + mailboxId + "\texpireTime : " + expireTime
                    + "\tisPrivate : " + isPrivate + "\tflags : " + flags
                    + "\tlegalUserName : " + legalUserName + "\tlegalPeerIp : "
                    + legalPeerIp);
        }

        try {
            CreateMsg cm = new CreateMsg(host, mailboxId, folderName,
                    messageArr, fromAddress, flags, userFlags,
                    Boolean.parseBoolean(isPrivate), imapUid, arrivalTime,
                    Long.parseLong(expireTime), accessId,
                    Integer.parseInt(options), legalUserName, legalPeerIp);

            cm.execute(myDataModel);
            return cm.getMsg().getMsgId();
        } catch (IntermailException e) {
            truncateRequestMessage(mxosRequestState, text);
            logger.error("Error while create message.", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (Exception e) {
            truncateRequestMessage(mxosRequestState, text);
            logger.error("Error while create message.", e);
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(),
                    e.getMessage());
        } catch (Throwable t) {
            truncateRequestMessage(mxosRequestState, text);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(), t);
        }
        return null;
    }

    /**
     * Custom expiration time handling for SoftBank Mx8->Mx9 and Mx9->Mx8
     * migration. For both directions, the message uses X-Mms-Expiry header
     * value (as RFC822 date) if valid one exists. Otherwise, in Mx9 case, no
     * expiration time is set.
     * 
     * In Mx8 case, expirationTime = arrivalTime +
     * msgExpirationTimeSeconds(cfg).
     * 
     * TODO: HACK: This functionality is a one-off feature used only for SBM
     * message migration. To keep code maintenance simple, we should remove this
     * feature from here whenever possible.
     * 
     * @param messageArr message text as byte array
     * @return expiration time
     */
    protected String mmsCustomExpireTime(final byte[] messageArr, long arrivalTime) {
        String result = null;
        try {
            ByteArrayInputStream is = new ByteArrayInputStream(messageArr);
            InternetHeaders headers = new InternetHeaders(is);
            
            // First, find a valid x-mms-expiry header
            String[] expiry = (headers != null) ? headers
                    .getHeader("x-mms-expiry") : null;
            MailDateFormat format = new MailDateFormat();
            Date date = (expiry != null && expiry.length > 0) ? format
                    .parse(expiry[0]) : null;

            if (date != null) { // found x-mms-expiry
                result = String.valueOf(date.getTime() / 1000);
            } else {
                // TODO: HACK: We are using myDataModel to distinguish Mx8 and
                // Mx9. But this is not a real Mx version indicator.
                if (myDataModel == RmeDataModel.Leopard) { // Mx9
                    result = "0";
                } else { // Mx8
                    // expireTime = arrivalTime + msgExpirationTimeSeconds
                    int msgExpirationTimeSeconds;
                    try {
                        msgExpirationTimeSeconds = MxOSConfig.getInt("",
                                "imapserv", "msgExpirationTimeSeconds");
                    } catch (final ConfigException cex) {
                        // silently sets default when the key is missing
                        // TODO: MxOSConfig class needs more graceful
                        // default handling
                        msgExpirationTimeSeconds = 2592000;
                    }
                    if (arrivalTime > 0) {
                        result = String.valueOf(arrivalTime
                                + msgExpirationTimeSeconds);
                    } else {
                        String[] arrival = (headers != null) ? headers
                                .getHeader("received") : null;
                        if (arrival != null && arrival.length > 0) {
                            int pos = arrival[0].indexOf(';');
                            if (pos >= 0 && pos + 1 < arrival[0].length()) {
                                date = format.parse(arrival[0],
                                        new ParsePosition(pos));
                            }
                        }
                        if (date != null) {
                            result = String.valueOf(date.getTime() / 1000
                                    + msgExpirationTimeSeconds);
                        }
                    }
                }
            }
        } catch (final MessagingException mex) {
            logger.error(
                    "Failed parsing message header. Giving up header analysis.",
                    mex);
        } catch (java.text.ParseException pex) {
            logger.error(
                    "Failed parsing message header. Giving up header analysis.",
                    pex);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("expire=").append(result));
        }
        return result != null ? result : "0";
    }

    @Override
    public void createMessageHeader(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void listMetadatasInFolder(MxOSRequestState mxosRequestState,
            final String folderName, Map<String, Metadata> metadatas)
            throws MxOSException {

        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }
            final int offset = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageOffset.name()));
            final int length = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageLength.name()));
            final String legarUserName = System
                    .getProperty(SystemProperty.readMessageLAUsername.name());
            final String legalPeerIp = System
                    .getProperty(SystemProperty.readMessageLAPeerIp.name());

            int[] uid = null;
            ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName, uid,
                    null, null, accessId, ReadMsgs.READ_MSGS_GET_HDRS
                            | ReadMsgs.READ_MSGS_GET_MIME, offset, length,
                    legarUserName, legalPeerIp, null, null, null);
            readMsgs.execute(myDataModel);
            Msg[] msgs = readMsgs.getMessages();
            for (Msg msg : msgs) {
                // For each message from RME create a message POJO and update
                // the messages list
                Metadata metadata = new Metadata();
                metadatas.put(msg.getMsgId(), populateMetaData(msg, metadata));
            }
        } catch (IntermailException e) {
            logger.error("Error while reading messages from folder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), t);
        }
    }

    @Override
    public void listMetadatasUidsInFolder(MxOSRequestState mxosRequestState,
            final String folderName, Map<String, Metadata> metadatas)
            throws MxOSException {

        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }
            final String legarUserName = System
                    .getProperty(SystemProperty.readMessageLAUsername.name());
            final String legalPeerIp = System
                    .getProperty(SystemProperty.readMessageLAPeerIp.name());

            int[] uid = null;
            ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName, uid,
                    null, null, accessId, ReadMsgs.READ_MSGS_GET_HDRS, 0, 0,
                    legarUserName, legalPeerIp, null, null, null);
            readMsgs.execute(myDataModel);
            Msg[] msgs = readMsgs.getMessages();
            for (Msg msg : msgs) {
                // For each message from RME create a Metadata POJO and update
                // the metadata list
                Metadata metadata = new Metadata();
                metadata.setUid(new Long(msg.getUid()));
                metadatas.put(msg.getMsgId(), metadata);
            }
        } catch (IntermailException e) {
            logger.error("Error while reading messages meta data uids list", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST
                            .name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages meta data uids list", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST
                            .name(),
                    e);
        } catch (Throwable t) {
            logger.error("Error while reading messages meta data uids list", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST
                            .name(),
                    t);
        }
    }
    
    @Override
    public void listHeadersInFolder(MxOSRequestState mxosRequestState,
            final String folderName, Map<String, Header> headers)
            throws MxOSException {

        try {

            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }

            final int offset = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageOffset.name()));
            final int length = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageLength.name()));
            final String legarUserName = System
                    .getProperty(SystemProperty.readMessageLAUsername.name());
            final String legalPeerIp = System
                    .getProperty(SystemProperty.readMessageLAPeerIp.name());

            int[] uid = null;
            ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName, uid,
                    null, null, accessId, ReadMsgs.READ_MSGS_GET_HDRS
                            | ReadMsgs.READ_MSGS_GET_MIME, offset, length,
                    legarUserName, legalPeerIp, null, null, null);
            readMsgs.execute(myDataModel);
            Msg[] msgs = readMsgs.getMessages();
            for (Msg msg : msgs) {
                // For each message from RME create a message POJO and update
                // the messages map
                headers.put(msg.getMsgId(), populateMessageHeader(msg));
            }
        } catch (IntermailException e) {
            logger.error("Error while reading messages from folder-"
                    + folderName, e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages from folder-"
                    + folderName, e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages from folder-"
                    + folderName, t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), t);
        }
    }

    @Override
    public void updateMessage(MxOSRequestState mxosRequestState,
            String messageId) throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteMessages(MxOSRequestState mxosRequestState,
            String[] messageIds) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("DeleteMessage: starting delete" + " message"));
        }
        try {
            final String folderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.folderName.name()).get(0);

            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }
            boolean optionFolderIsHint = getBooleanOption(mxosRequestState,
                    MessageProperty.optionFolderIsHint.name());
            boolean optionMultipleOk = getBooleanOption(mxosRequestState,
                    MessageProperty.optionMultipleOk.name());
            boolean optionSupressMers = getBooleanOption(mxosRequestState,
                    MessageProperty.optionSupressMers.name());
            int options = getDeleteOptions(optionFolderIsHint,
                    optionMultipleOk, optionSupressMers);

            if (logger.isDebugEnabled()) {
                for (String messageId : messageIds) {
                    logger.debug(new StringBuffer().append(
                            "Deleting messsage with messageId-").append(
                            messageId));
                }
            }
            DeleteMsgs delmsgs = new DeleteMsgs(host, mailboxId, folderName,
                    accessId, options, messageIds);
            delmsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                        .append("DeleteMessage: Success"));
            }
        } catch (IntermailException e) {
            logger.error("Error while deleting message from folder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE.name(), e);
        } catch (Exception e) {
            logger.error("Error while deleting message from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while deleting message from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE.name(), t);
        }
    }

    @Override
    public void deleteAllMessages(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("deleteAllMessages: starting delete all messages");
        }
        final String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }
            boolean optionFolderIsHint = getBooleanOption(mxosRequestState,
                    MessageProperty.optionFolderIsHint.name());
            boolean optionMultipleOk = getBooleanOption(mxosRequestState,
                    MessageProperty.optionMultipleOk.name());
            boolean optionSupressMers = getBooleanOption(mxosRequestState,
                    MessageProperty.optionSupressMers.name());
            int options = getDeleteOptions(optionFolderIsHint,
                    optionMultipleOk, optionSupressMers);

            DeleteMsgs delmsgs = null;
            List<com.opwvmsg.mxos.message.pojos.Folder> folders = new ArrayList<com.opwvmsg.mxos.message.pojos.Folder>();
            Folder root = getFolder(mxosRequestState, folderName);
            populateFolderInventory(root, folders);
            for (final com.opwvmsg.mxos.message.pojos.Folder folder : folders) {
                delmsgs = new DeleteMsgs(host, mailboxId,
                        folder.getFolderName(), accessId, options, null);
                delmsgs.execute(myDataModel);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("deleteAllMessages: Success");
            }
        } catch (IntermailException e) {
            logger.error("Error while deleting all messages from folder-"
                    + folderName, e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE_ALL.name(), e);
        } catch (Exception e) {
            logger.error("Error while deleting all messages from folder-"
                    + folderName, e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE_ALL.name(), e);
        } catch (Throwable t) {
            logger.error("Error while deleting all messages from folder-"
                    + folderName, t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE_ALL.name(), t);
        }
    }

    @Override
    public void copyMessages(MxOSRequestState mxosRequestState,
            String[] messageIds) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("CopyMessage: starting copy message"));
        }
        try {
            final String srcFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.srcFolderName.name()).get(0);
            final String toFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.toFolderName.name()).get(0);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }

            boolean optionFolderIsHint = getBooleanOption(mxosRequestState,
                    MessageProperty.optionFolderIsHint.name());
            boolean optionMultipleOk = getBooleanOption(mxosRequestState,
                    MessageProperty.optionMultipleOk.name());
            boolean optionSupressMers = getBooleanOption(mxosRequestState,
                    MessageProperty.optionSupressMers.name());
            int options = getCopyOptions(optionFolderIsHint, optionMultipleOk,
                    optionSupressMers);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("Copying messages from folder - ")
                        .append(srcFolderName).append(" , to folder - ")
                        .append(toFolderName));
                for (String messageId : messageIds) {
                    logger.debug(new StringBuffer(
                            "Copying message with MessageId : ")
                            .append(messageId));
                }
            }
            CopyMsgs copyMsgs = new CopyMsgs(host, mailboxId, srcFolderName,
                    toFolderName, accessId, options, messageIds);
            copyMsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("CopyMessage: Success "));
            }
        } catch (IntermailException e) {
            logger.error("Error while copying message from folder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY.name(), e);
        } catch (Exception e) {
            logger.error("Error while copying message from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY.name(), e);
        } catch (Throwable t) {
            logger.error("Error while copying message from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY.name(), t);
        }
    }

    @Override
    public void copyAllMessages(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("copyAllMessage: starting copy all messages"));
        }
        try {
            final String srcFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.srcFolderName.name()).get(0);
            final String toFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.toFolderName.name()).get(0);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }

            boolean optionFolderIsHint = getBooleanOption(mxosRequestState,
                    MessageProperty.optionFolderIsHint.name());
            boolean optionMultipleOk = getBooleanOption(mxosRequestState,
                    MessageProperty.optionMultipleOk.name());
            boolean optionSupressMers = getBooleanOption(mxosRequestState,
                    MessageProperty.optionSupressMers.name());
            int options = getCopyOptions(optionFolderIsHint, optionMultipleOk,
                    optionSupressMers);
            // Copy all the messages from the given folder to destination
            // folder
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "Copying all the messages from folder - ")
                        .append(srcFolderName).append(", to folder - ")
                        .append(toFolderName));
            }
            CopyMsgs copyMsgs = new CopyMsgs(host, mailboxId, srcFolderName,
                    toFolderName, null, accessId, options);
            copyMsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                        .append("CopyAllMessage: Success "));
            }
        } catch (IntermailException e) {
            logger.error("Error while copying all messages from folder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY_ALL.name(), e);
        } catch (Exception e) {
            logger.error("Error while copying all messages from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY_ALL.name(), e);
        } catch (Throwable t) {
            logger.error("Error while copying all messages from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY_ALL.name(), t);
        }
    }

    @Override
    public void moveMessages(MxOSRequestState mxosRequestState,
            String[] messageIds) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("MoveMessage: starting move message"));
        }
        try {
            final String srcFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.srcFolderName.name()).get(0);
            final String toFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.toFolderName.name()).get(0);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }

            boolean optionFolderIsHint = getBooleanOption(mxosRequestState,
                    MessageProperty.optionFolderIsHint.name());
            boolean optionMultipleOk = getBooleanOption(mxosRequestState,
                    MessageProperty.optionMultipleOk.name());
            boolean optionSupressMers = getBooleanOption(mxosRequestState,
                    MessageProperty.optionSupressMers.name());
            int options = getCopyOptions(optionFolderIsHint, optionMultipleOk,
                    optionSupressMers);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("Moving messages from folder - ")
                        .append(srcFolderName).append(" , to folder - ")
                        .append(toFolderName));
                for (String messageId : messageIds) {
                    logger.debug(new StringBuffer(
                            "Moving message with MessageId : ")
                            .append(messageId));
                }
            }
            MoveMsgs moveMsgs = new MoveMsgs(host, mailboxId, srcFolderName,
                    toFolderName, accessId, options, messageIds);
            moveMsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("MoveMessage: Success "));
            }
        } catch (IntermailException e) {
            logger.error("Error while moving message from folder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE.name(), e);
        } catch (Exception e) {
            logger.error("Error while moving message from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while moving message from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE.name(), t);
        }
    }

    @Override
    public void moveAllMessages(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("MoveAllMessage: starting move all messages"));
        }
        try {
            final String srcFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.srcFolderName.name()).get(0);
            final String toFolderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.toFolderName.name()).get(0);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }

            boolean optionFolderIsHint = getBooleanOption(mxosRequestState,
                    MessageProperty.optionFolderIsHint.name());
            boolean optionMultipleOk = getBooleanOption(mxosRequestState,
                    MessageProperty.optionMultipleOk.name());
            boolean optionSupressMers = getBooleanOption(mxosRequestState,
                    MessageProperty.optionSupressMers.name());
            int options = getCopyOptions(optionFolderIsHint, optionMultipleOk,
                    optionSupressMers);
            // Move all the messages from the given folder to destination
            // folder
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "Moving all the messages from folder - ")
                        .append(srcFolderName).append(", to folder - ")
                        .append(toFolderName));
            }
            MoveMsgs moveMsgs = new MoveMsgs(host, mailboxId, srcFolderName,
                    toFolderName, null, accessId, options);
            moveMsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                        .append("MoveAllMessage: Success "));
            }
        } catch (IntermailException e) {
            logger.error("Error while moving all messages from folder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE_ALL.name(), e);
        } catch (Exception e) {
            logger.error("Error while moving all messages from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE_ALL.name(), e);
        } catch (Throwable t) {
            logger.error("Error while moving all messages from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE_ALL.name(), t);
        }
    }

    @Override
    public Flags populateMessageFlags(MxOSRequestState mxosRequestState,
            String messageId) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("populating Message Flags...");
        }
        try {
            final String[] docIds = new String[] { messageId };
            final String legalUserName = System
                    .getProperty(SystemProperty.legalUsername.name());
            final String legalPeerIp = System
                    .getProperty(SystemProperty.legalPeerIp.name());

            final String folderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.folderName.name()).get(0);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }
            ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName,
                    docIds, null, null, accessId, ReadMsgs.READ_MSGS_GET_MIME,
                    0, 0, legalUserName, legalPeerIp, null, null, null);

            try {
                readMsgs.execute(myDataModel);
            } catch (IntermailException e) {
                logger.error("Error while read message.", e);
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                        "Failed to read the message flags, " + e.getMessage());
            }

            if ((readMsgs == null) || (readMsgs.getMessages() == null)) {
                logger.info("Failed to read the message. readMsgs is null");
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                        "Failed to read the message flags");
            }

            Msg[] msgs = readMsgs.getMessages();

            if (msgs.length != 1 && msgs[0] != null) {
                logger.info("Failed to read the message.");
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                        "No message found");
            }
            Flags flags = getFlagsFromMessage(msgs[0]);

            if (logger.isDebugEnabled()) {
                logger.debug("populateMessageFlags: Success ");
            }
            return flags;
        } catch (Exception e) {
            logger.error("Error while populating message flags.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while populating message flags.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), t);
        }
    }

    @Override
    public void updateMessageFlags(MxOSRequestState mxosRequestState,
            final String[] messageIds) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("updating Message Flags...");
        }
        try {

            final String folderName = mxosRequestState.getInputParams()
                    .get(FolderProperty.folderName.name()).get(0);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }
            Flags flags = (Flags) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageFlags);

            UpdateMsgs updateMsgs = new UpdateMsgs(host, mailboxId, folderName,
                    messageIds, getFlagsString(flags), accessId,
                    UpdateMsgs.UPDATE_MSGS_UPDATE_ONLY, null, null, null);

            try {
                updateMsgs.execute(myDataModel);
            } catch (IntermailException e) {
                logger.error("Error while update the message flags.", e);
                ExceptionUtils
                        .createApplicationExceptionFromIntermailException(
                                MessageError.MSG_UNABLE_TO_PERFORM_UPDATE
                                        .name(), e);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("updateMessageFlags: Success ");
            }
        } catch (Exception e) {
            logger.error("Error while updating message flags.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while updating message flags.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), t);
        }
    }

    @Override
    public void updateMailboxLastAccessTime(MxOSRequestState mxosRequestState,
            long lastAccessTime) throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateMessageLastAccessTime(MxOSRequestState mxosRequestState,
            long lastAccessTime) throws MxOSException {
        // TODO Auto-generated method stub

    }

    /**
     * Close database connection.
     * 
     * @throws Exception Throws Exception.
     */
    public void close() throws Exception {
        // DO Nothing.
    }

    /**
     * Populate Folder info to the Folder POJO.
     * 
     * @param folderPojo Folder POJO
     * @param interMailFolder intermail folder
     */
    protected void populateFolder(
            com.opwvmsg.mxos.message.pojos.Folder folderPojo,
            Folder interMailFolder) {
        if (null != interMailFolder.getPathName())
            folderPojo.setFolderName(MxOSConstants.FORWARD_SLASH
                    + interMailFolder.getPathName());
        folderPojo.setNextUID((long) interMailFolder.getNextUID());
        folderPojo.setUidValidity((long) interMailFolder.getUIDValidity());
        folderPojo.setNumMessages(interMailFolder.getNumMsgs());
        folderPojo
                .setNumReadMessages((interMailFolder.getNumMsgs() - interMailFolder
                        .getNumUnreadMessages()));
        folderPojo.setNumUnreadMessages(interMailFolder.getNumUnreadMessages());
        folderPojo.setFolderSizeBytes(interMailFolder.getNumBytes());
        Inventory[] invArray = interMailFolder.getInventoriesArray();
        for (int j = 0; j < invArray.length; j++) {
            if (invArray[j].getUnread() == true) {
                folderPojo.setFolderSizeReadBytes(interMailFolder.getNumBytes()
                        - invArray[j].getMsgBytes());
                folderPojo.setFolderSizeUnreadBytes(invArray[j].getMsgBytes());
            }
            if (invArray[j].getUnread() == false) {
                folderPojo.setFolderSizeReadBytes(invArray[j].getMsgBytes());
                folderPojo.setFolderSizeUnreadBytes(interMailFolder.getNumBytes()
                        - invArray[j].getMsgBytes());
            }
        }
    }

    @Override
    public void readFolder(MxOSRequestState mxosRequestState,
            com.opwvmsg.mxos.message.pojos.Folder folder) throws MxOSException {
        final MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        CRUDProtocol protocol = CRUDUtils.getCRUDProtocol(info);
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
            logger.debug(new StringBuffer().append("Protocol : ").append(
                    protocol));
        }

        final String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);
        boolean optionSupressMers = getBooleanOption(mxosRequestState,
                FolderProperty.optionSupressMers.name());
        ReadFolder readFolder = new ReadFolder(host, mailboxId, folderName,
                accessId, ReadFolder.FOLDER_SORT_NATURAL, getOptions(true,
                        false, true, optionSupressMers), null, null, -1, -1,
                false, true);
        try {
            readFolder.execute(myDataModel);
        } catch (IntermailException e) {
            logger.error("Error while read folder.", e);
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_GET.name(), e);
        }

        populateFolder(folder, readFolder.getFolder());
    }

    @Override
    public void listFolders(final MxOSRequestState mxosRequestState,
            List<com.opwvmsg.mxos.message.pojos.Folder> folders)
            throws MxOSException {
        try {
            // TODO Auto-generated method stub
            Folder root = getRootFolder(mxosRequestState);
            if (folders == null) {
                folders = new ArrayList<com.opwvmsg.mxos.message.pojos.Folder>();
            }
            populateFolderInventory(root, folders);
            return;
        } catch (IntermailException e) {
            logger.error("Exception in readAllFolder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_LIST.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading all folders.", e);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_LIST.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading all folders.", t);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_LIST.name(), t);
        }
    }

    /**
     * Method to populate Message POJO using Msg object returned from RME.
     * 
     * @param msg message from RME
     * @return Message POJO
     */
    protected void populateMessage(Msg msg, Message message) {
        Header header = populateMessageHeader(msg);
        message.setHeader(header);
        Metadata metadata = new Metadata();
        message.setMetadata(populateMetaData(msg, metadata));
        Body body = populateMessageBody(msg);
        message.setBody(body);
    }

    /**
     * Method to populate MessageHeader POJO using Msg object returned from RME.
     * 
     * @param msg message from RME
     * @return Message POJO
     */
    protected Header populateMessageHeader(Msg msg) {
        Header messageHeader = new Header();
        for (String header : msg.getHeadersArray()) {
            logger.debug("\nheader value from msg.getHeadersArray():  "
                    + header);
        }
        try {
            if (logger.isDebugEnabled()) {
                logger.debug(msg.toString());
            }
            String headerBlob = Utilities.arrayToDsv(msg.getHeadersArray(),
                    String.valueOf('\r') + String.valueOf('\n'));
            if ((headerBlob != "") && (headerBlob.length() != 0)) {
                messageHeader.setHeaderBlob(headerBlob);
            }
        } catch (Exception e) {
            logger.error("Error while populate message header.", e);
        }
        return messageHeader;
    }

    /**
     * Method to populate MessageMetaData POJO using Msg object returned from
     * RME.
     * 
     * @param msg message from RME
     * @return Message POJO
     */
    protected Metadata populateMetaData(Msg msg, Metadata metaData) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug(msg.toString());
            }
        } catch (Exception e) {
            logger.error("Error while populate meta data.", e);
        }

        try {
            metaData.setUid(new Long(msg.getUid()));
            String priority = msg.getMsgPriority();
            if (priority != null && !priority.equals("")) {
                metaData.setPriority(msg.getMsgPriority());
            }
            metaData.setSize(new Long(msg.getSizeInBytes()));
            // read header information
            /*
             * metaData.setTo(msg.getHeader(HEADER_TO, COMMA_SEPERATOR));
             * metaData.setFrom(msg.getHeader(HEADER_FROM, COMMA_SEPERATOR));
             * metaData.setCc(msg.getHeader(HEADER_CC, COMMA_SEPERATOR));
             */
            metaData.setSubject(msg.getHeader(HEADER_SUBJECT, COMMA_SEPERATOR));
            if (msg.getCustomFlags() != null) {
                List<String> keywords = new ArrayList<String>();
                for (int i = 0; i < msg.getCustomFlags().length; i++) {
                    keywords.add(msg.getCustomFlags()[i]);
                }
                metaData.setKeywords(keywords);
            }
        } catch (NumberFormatException nfe) {
            logger.warn("Number format exception occured.", nfe);
        } catch (Exception e) {
            logger.error("Error while populate meta data.", e);
        }

        Flags flags = getFlagsFromMessage(msg);
        metaData.setType(msg.getMsgType());
        metaData.setArrivalTime(msg.getArrivalTime());
        metaData.setExpireOn(msg.getMsgExpireTime());
        metaData.setFlagAns(flags.getFlagAns());
        metaData.setFlagBounce(flags.getFlagBounce());
        metaData.setFlagDel(flags.getFlagDel());
        metaData.setFlagDraft(flags.getFlagDraft());
        metaData.setFlagFlagged(flags.getFlagFlagged());
        metaData.setFlagPriv(flags.getFlagPriv());
        metaData.setFlagRecent(flags.getFlagRecent());
        metaData.setFlagSeen(flags.getFlagSeen());
        metaData.setFlagUnread(flags.getFlagUnread());
        return metaData;
    }

    /**
     * Method to populate Body POJO using Msg object returned from RME.
     * 
     * @param msg message from RME
     * @return Body POJO
     */
    protected Body populateMessageBody(Msg msg) {
        Body msgBody = new Body();
        String text = null;
        if (MxOSConfig.isMsgBodyBlobBase64Encoded()) {
            Base64 base64 = new Base64();
            text = new String(base64.encode(msg.getText()));
        } else {
            text = new String(msg.getText());
        }
        if ((text != null) && (text.length() != 0)) {
            msgBody.setMessageBlob(text);
        }
        return msgBody;
    }

    @Override
    public void readMessage(final MxOSRequestState mxosRequestState,
            Message message) throws MxOSException {

        final String legalUserName = System
                .getProperty(SystemProperty.legalUsername.name());
        final String legalPeerIp = System
                .getProperty(SystemProperty.legalPeerIp.name());
        int offset = 0;
        int length = 0;
        if (mxosRequestState.getInputParams()
                .get(MessageProperty.offset.name()) != null) {
            offset = Integer.parseInt(mxosRequestState.getInputParams()
                    .get(MessageProperty.offset.name()).get(0));
        } else {
            offset = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageOffset.name()));
        }
        if (mxosRequestState.getInputParams()
                .get(MessageProperty.length.name()) != null) {
            length = Integer.parseInt(mxosRequestState.getInputParams()
                    .get(MessageProperty.length.name()).get(0));
        } else {
            length = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageLength.name()));
        }

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }

        String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);

        String[] docIds = new String[1];
        docIds[0] = mxosRequestState.getInputParams()
                .get(MessageProperty.messageId.name()).get(0);

        ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName, docIds,
                null, null, accessId, ReadMsgs.READ_MSGS_GET_TEXT
                        | ReadMsgs.READ_MSGS_GET_HDRS
                        | ReadMsgs.READ_MSGS_GET_MIME, offset, length,
                legalUserName, legalPeerIp, null, null, null);

        try {
            readMsgs.execute(myDataModel);
        } catch (IntermailException e) {
            logger.error("Error while read message.", e);
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        }

        if ((readMsgs == null) || (readMsgs.getMessages() == null)) {
            logger.info("Failed to read the message. readMsgs is null");
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                    "Failed to read the message");
        }

        Msg[] msgs = readMsgs.getMessages();

        if (msgs.length != 1) {
            logger.info("Failed to read the message.");
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                    "No message found");
        }
        populateMessage(msgs[0], message);
    }

    /**
     * Access Id based on the isAdmin parameter of request state.
     * 
     * @param isAdmin access Id
     * @return
     */
    protected int getAccessId(TrueOrFalse isAdmin) {
        if (TrueOrFalse.FALSE == isAdmin) {
            return Mailbox.ACCESS_GENERIC_END_USER;
        } else {
            return Mailbox.ACCESS_ADMIN;
        }
    }

    /**
     * Message store host attribute which is retrieved from mssLinkInfo.
     * 
     * @param mssHosts list of mssHosts
     * @return mssHost
     */
    protected String getMssHost(List<String> mssHosts) {
        String mssHost = null;
        if (mssHosts != null && mssHosts.size() > 0) {
            mssHost = mssHosts.get(0);
        }
        return mssHost;
    }

    /**
     * 
     * @param mxosRequestState request state
     * @param param input parameter
     * @return boolean value
     */
    protected boolean getBooleanOption(MxOSRequestState mxosRequestState,
            String param) {
        boolean value = false;
        if (mxosRequestState.getInputParams().get(param) != null) {
            value = Boolean.parseBoolean(mxosRequestState.getInputParams()
                    .get(param).get(0));
        }
        return value;
    }

    /**
     * 
     * @param optionFolderIsHint expects boolean value
     * @param optionMultipleOk expects boolean value
     * @param optionSupressMers expects boolean value
     * @return int options
     */
    protected int getDeleteOptions(boolean optionFolderIsHint,
            boolean optionMultipleOk, boolean optionSupressMers) {
        int options = 0;
        if (optionFolderIsHint) {
            options |= DeleteMsgs.DELETE_MSGS_FOLDER_IS_HINT;
        }
        if (optionMultipleOk) {
            options |= DeleteMsgs.DELETE_MSGS_MULTIPLE_OK;
        }
        if (optionSupressMers) {
            options |= DeleteMsgs.DELETE_MSGS_SUPPRESS_MERS;
        }
        return options;
    }

    /**
     * 
     * @param optionFolderIsHint expects boolean value
     * @param optionMultipleOk expects boolean value
     * @param optionSupressMers expects boolean value
     * @return int options
     */
    protected int getCopyOptions(boolean optionFolderIsHint,
            boolean optionMultipleOk, boolean optionSupressMers) {
        int options = 0;
        if (optionFolderIsHint) {
            options |= CopyMsgs.COPY_MSGS_FOLDER_IS_HINT;
        }
        if (optionMultipleOk) {
            options |= CopyMsgs.COPY_MSGS_MULTIPLE_OK;
        }
        if (optionSupressMers) {
            options |= CopyMsgs.COPY_MSGS_SUPPRESS_MERS;
        }
        return options;
    }

    @Override
    public int getMailboxLastAccessTime(MxOSRequestState mxosRequestState)
            throws MxOSException {

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }

        try {
            GetLastAccessTime lastAccessTime = new GetLastAccessTime(host,
                    mailboxId, accessId);
            lastAccessTime.execute(myDataModel);
            logger.error("lastAccessTime.getLastAccessTime() in MssMetaCRUD : "
                    + lastAccessTime.getLastAccessTime());
            // return Last Access Time
            return lastAccessTime.getLastAccessTime();
        } catch (IntermailException e) {
            logger.error("Error while retrieving the last access time.", e);
            ConnectionErrorStats.MSS.increment();
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while retrieving the last access time.", e);
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                    "Improper Data Received");
        } catch (Throwable t) {
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), t);
        }
    }

    @Override
    public void readMessageHeader(final MxOSRequestState mxosRequestState,
            Header header) throws MxOSException {

        final String legalUserName = System
                .getProperty(SystemProperty.legalUsername.name());
        final String legalPeerIp = System
                .getProperty(SystemProperty.legalPeerIp.name());

        final MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }

        final String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);

        String[] docIds = new String[1];
        docIds[0] = mxosRequestState.getInputParams()
                .get(MessageProperty.messageId.name()).get(0);

        ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName, docIds,
                null, null, accessId, ReadMsgs.READ_MSGS_GET_HDRS, 0, 0,
                legalUserName, legalPeerIp, null, null, null);

        try {
            readMsgs.execute(myDataModel);
        } catch (IntermailException e) {
            logger.error("Error while read message.", e);
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        }

        if ((readMsgs.getMessages() == null)
                || (readMsgs.getMessages().length != 1)) {
            logger.info("No message found");
            throw new ApplicationException(MessageError.MSG_NOT_FOUND.name(),
                    "No message found");
        }
        Msg[] msgs = readMsgs.getMessages();
        header = populateMessageHeader(msgs[0]);
        mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.headerSummary,
                header);
    }

    protected static Flags getFlagsFromMessage(Msg msg) {
        Flags flags = new Flags();
        // Setting message flags
        flags.setFlagRecent(msg.getFlag(Msg.MssFlagOrder.flagrecent));
        flags.setFlagSeen(msg.getFlag(Msg.MssFlagOrder.flagseen));
        flags.setFlagAns(msg.getFlag(Msg.MssFlagOrder.flagans));
        flags.setFlagFlagged(msg.getFlag(Msg.MssFlagOrder.flagflagged));
        flags.setFlagDraft(msg.getFlag(Msg.MssFlagOrder.flagdraft));
        flags.setFlagDel(msg.getFlag(Msg.MssFlagOrder.flagdel));
        flags.setFlagUnread(!msg.getFlag(Msg.MssFlagOrder.flagseen));
        return flags;
    }

    protected static String getFlagsString(Flags flags) {
        StringBuilder strFlags = new StringBuilder();
        for (MssFlagOrder order : MssFlagOrder.values()) {
            Boolean bool = null;
            switch (order) {
            case flagrecent:
                bool = flags.getFlagRecent();
                break;
            case flagseen:
                bool = flags.getFlagSeen();
                break;
            case flagans:
                bool = flags.getFlagAns();
                break;
            case flagflagged:
                bool = flags.getFlagFlagged();
                break;
            case flagdraft:
                bool = flags.getFlagDraft();
                break;
            case flagdel:
                bool = flags.getFlagDel();
                break;
            }
            strFlags.append((null == bool ? Boolean.FALSE : bool).toString()
                    .toUpperCase().charAt(0));
            bool = null;
        }
        return strFlags.toString();
    }

    /**
     * Method to truncate the Request Map, if the request contains long
     * messages.
     * 
     * @param mxosRequestState mxosRequestState
     * @param text text
     */
    public static void truncateRequestMessage(
            final MxOSRequestState mxosRequestState, final String text) {
        if (text != null && text.length() > 50) {
            // Truncating the message from input params.
            // This is to avoid sending the whole message back to
            // the client in case of any error
            mxosRequestState.getInputParams().get(MxOSPOJOs.message.name())
                    .clear();
            mxosRequestState.getInputParams().get(MxOSPOJOs.message.name())
                    .add(truncate(text, 50, "..."));
        }
    }

    /**
     * Truncate the message as per given details and append with tail.
     * 
     * @param original original text
     * @param maxSize maxSize
     * @param tail tail String
     * @return truncated text
     */
    public static String truncate(final String original, final int maxSize,
            final String tail) {
        String temp;
        if (original != null && original.length() > maxSize) {
            temp = new StringBuffer().append(original.substring(0, maxSize))
                    .append(tail).toString();
        } else {
            temp = original;
        }
        return temp;
    }

    @Override
    public void readMailboxMetaInfo(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void createBlob(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void readBlob(MxOSRequestState mxosRequestState, Body body)
            throws MxOSException {
        final String legalUserName = System
                .getProperty(SystemProperty.legalUsername.name());
        final String legalPeerIp = System
                .getProperty(SystemProperty.legalPeerIp.name());

        final MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }

        final String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);

        String[] docIds = new String[1];
        docIds[0] = mxosRequestState.getInputParams()
                .get(MessageProperty.messageId.name()).get(0);
        int offset = 0;
        if (mxosRequestState.getInputParams()
                .get(MessageProperty.offset.name()) != null) {
            offset = Integer.parseInt(mxosRequestState.getInputParams()
                    .get(MessageProperty.offset.name()).get(0));
        } else {
            offset = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageOffset.name()));
        }
        int length = 0;
        if (mxosRequestState.getInputParams()
                .get(MessageProperty.length.name()) != null) {
            length = Integer.parseInt(mxosRequestState.getInputParams()
                    .get(MessageProperty.length.name()).get(0));
        } else {
            length = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageLength.name()));
        }

        ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName, docIds,
                null, null, accessId, ReadMsgs.READ_MSGS_GET_TEXT, offset,
                length, legalUserName, legalPeerIp, null, null, null);

        try {
            readMsgs.execute(myDataModel);
        } catch (IntermailException e) {
            logger.error("Error while read message.", e);
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        }

        if ((readMsgs.getMessages() == null)
                || (readMsgs.getMessages().length != 1)) {
            logger.info("No message found");
            throw new ApplicationException(MessageError.MSG_NOT_FOUND.name(),
                    "No message found");
        }
        Msg[] msgs = readMsgs.getMessages();
        body = populateMessageBody(msgs[0]);
        mxosRequestState.getDbPojoMap()
                .setProperty(MxOSPOJOs.messageBody, body);
    }

    @Override
    public void deleteBlob(MxOSRequestState mxosRequestState, String messageId)
            throws MxOSException {
        // TODO Auto-generated method stub
    }

    @Override
    public void readMessageMetaData(final MxOSRequestState mxosRequestState,
            final String folderName, Metadata metadata) throws MxOSException {

        final String legalUserName = System
                .getProperty(SystemProperty.readMessageLAUsername.name());
        final String legalPeerIp = System
                .getProperty(SystemProperty.readMessageLAPeerIp.name());

        final MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }

        String[] docIds = null;
        int[] uid = null;

        if (mxosRequestState.getInputParams().get(
                MessageProperty.messageId.name()) != null) {
            docIds = new String[1];
            docIds[0] = mxosRequestState.getInputParams()
                    .get(MessageProperty.messageId.name()).get(0);
        }

        ReadMsgs readMsgs = null;

        if (docIds != null)
            readMsgs = new ReadMsgs(host, mailboxId, folderName, docIds, null,
                    null, accessId, ReadMsgs.READ_MSGS_GET_HDRS
                            | ReadMsgs.READ_MSGS_GET_MIME, 0, 0, legalUserName,
                    legalPeerIp, null, null, null);
        else
            readMsgs = new ReadMsgs(host, mailboxId, folderName, uid, null,
                    null, accessId, ReadMsgs.READ_MSGS_GET_HDRS
                            | ReadMsgs.READ_MSGS_GET_MIME, 0, 0, legalUserName,
                    legalPeerIp, null, null, null);
        try {
            readMsgs.execute(myDataModel);
            if (logger.isDebugEnabled())
                logger.debug("Successfully fetched messages in folder : "
                        + folderName);
        } catch (IntermailException e) {
            logger.error("Error while reading Messages From MSS.", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        }

        if ((readMsgs.getMessages() == null)
                || (readMsgs.getMessages().length < 1)) {
            logger.info("No message found");
            throw new ApplicationException(MessageError.MSG_NOT_FOUND.name(),
                    "No message found");
        }

        Msg[] msgs = readMsgs.getMessages();
        populateMetaData(msgs[0], metadata);
        return;
    }

    @Override
    public void readMultiMessageMetaData(MxOSRequestState mxosRequestState,
            String folderName, Map<String, Metadata> metaMap)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Reading Folder: " + folderName + ", For: " + metaMap);
        }

        final String legalUserName = System
                .getProperty(SystemProperty.readMessageLAUsername.name());
        final String legalPeerIp = System
                .getProperty(SystemProperty.readMessageLAPeerIp.name());

        final MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }

        final List<String> msgIds = mxosRequestState.getInputParams().get(
                MessageProperty.messageId.name());
        final String[] docIds = msgIds.toArray(new String[msgIds.size()]);
        int[] uid = null;

        ReadMsgs readMsgs = null;
        if (docIds != null) {
            readMsgs = new ReadMsgs(host, mailboxId, folderName, docIds, null,
                    null, accessId, ReadMsgs.READ_MSGS_GET_HDRS
                            | ReadMsgs.READ_MSGS_GET_MIME, 0, 0, legalUserName,
                    legalPeerIp, null, null, null);
        } else {
            readMsgs = new ReadMsgs(host, mailboxId, folderName, uid, null,
                    null, accessId, ReadMsgs.READ_MSGS_GET_HDRS
                            | ReadMsgs.READ_MSGS_GET_MIME, 0, 0, legalUserName,
                    legalPeerIp, null, null, null);
        }

        try {
            readMsgs.execute(myDataModel);
            if (logger.isDebugEnabled())
                logger.debug("Successfully fetched messages in folder : "
                        + folderName);
        } catch (IntermailException e) {
            logger.error("Error while reading Messages From MSS.", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        }

        if ((readMsgs.getMessages() == null)
                || (readMsgs.getMessages().length < 1)) {
            logger.info("No message found");
            throw new ApplicationException(MessageError.MSG_NOT_FOUND.name(),
                    "No message found");
        }

        final Msg[] msgs = readMsgs.getMessages();
        /* fill the data into result map */
        for (int i = 0; i < docIds.length; i++) {
            Metadata metadata = new Metadata();
            metaMap.put(docIds[i], populateMetaData(msgs[i], metadata));
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Result Map<MessageId, Metadata> = " + metaMap);
        }
    }

    @Override
    public void updateMessageMetaData(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub
    }

    @Override
    public void readMessageBody(MxOSRequestState mxosRequestState, Body body)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void listMessageUUIDs(MxOSRequestState mxosRequestState)
            throws MxOSException, IntermailException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateFolderSubscribed(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void searchSortedMetadatasInFolder(
            MxOSRequestState mxosRequestState, String folderName,
            Map<String, Map<String, Metadata>> searchMetadatas)
            throws MxOSException {
        // Map<String, Metadata> metadatas
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = String.valueOf(info.getMailboxId());
            int accessId = getAccessId(info.getIsAdmin());
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("AccessId : ").append(
                        accessId));
            }
            final SearchTerm searchTerm = (SearchTerm) mxosRequestState
                    .getAdditionalParams().getPropertyAsObject(
                            MessageProperty.searchquery);
            final int offset = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageOffset.name()));
            final int length = Integer.parseInt(System
                    .getProperty(SystemProperty.readMessageLength.name()));
            final String legarUserName = System
                    .getProperty(SystemProperty.readMessageLAUsername.name());
            final String legalPeerIp = System
                    .getProperty(SystemProperty.readMessageLAPeerIp.name());

            int[] uid = null;

            /*
             * this method is invoked for "list" also hence sortKey won't be
             * present
             */
            String sortKey = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.sortKey.name())) {
                sortKey = mxosRequestState.getInputParams()
                        .get(MailboxProperty.sortKey.name()).get(0);
            }

            if (sortKey != null && sortKey.equals(MessageProperty.uid.name())) {
                // validate sliceStart , sliceEnd , sliceCount values and then
                // populate uid
                final String sliceStart;
                if (mxosRequestState.getInputParams().containsKey(
                        MailboxProperty.sliceStart.name())) {
                    sliceStart = mxosRequestState.getInputParams()
                            .get(MailboxProperty.sliceStart.name()).get(0);
                } else {
                    sliceStart = null;
                }
                final String sliceEnd;
                if (mxosRequestState.getInputParams().containsKey(
                        MailboxProperty.sliceEnd.name())) {
                    sliceEnd = mxosRequestState.getInputParams()
                            .get(MailboxProperty.sliceEnd.name()).get(0);
                } else {
                    sliceEnd = null;
                }

                if (sliceStart == null || sliceEnd == null
                        || sliceStart.isEmpty() || sliceEnd.isEmpty()) {
                    logger.error("sliceStart and sliceEnd param must be provided for uid search");
                    throw new InvalidRequestException(
                            ErrorCode.GEN_INVALID_DATA.name(),
                            "sliceStart and sliceEnd param must be provided for uid search");
                }

                final String sliceCount;
                if (mxosRequestState.getInputParams().containsKey(
                        MailboxProperty.sliceCount.name())) {
                    sliceCount = mxosRequestState.getInputParams()
                            .get(MailboxProperty.sliceCount.name()).get(0);
                } else {
                    sliceCount = null;
                }

                if (sliceCount == null || sliceCount.isEmpty()) {
                    logger.error("sliceCount param must be provided for uid search");
                    throw new InvalidRequestException(
                            ErrorCode.GEN_INVALID_DATA.name(),
                            "sliceCount param must be provided for uid search");
                }
                int uidCount = Integer.valueOf(sliceCount);
                if (uidCount != 1) {
                    throw new InvalidRequestException(
                            ErrorCode.GEN_INVALID_DATA.name(),
                            "sliceCount value must be 1 for uid search");
                }
                int uidStart = Integer.valueOf(sliceStart);
                int uidEnd = Integer.valueOf(sliceEnd);

                if (uidStart != uidEnd) {
                    logger.error("sliceStart and sliceEnd must be the same for uid search");
                    throw new InvalidRequestException(
                            ErrorCode.GEN_INVALID_DATA.name(),
                            "sliceStart and sliceEnd must be the same for uid search");
                }
                uid = new int[1];
                uid[0] = uidStart;

                if (logger.isDebugEnabled()) {
                    logger.debug("Slice Start value: " + sliceStart);
                    logger.debug("Slice End value: " + sliceEnd);
                    logger.debug("Slice Count value: " + sliceCount);
                }
            }
            ReadMsgs readMsgs = new ReadMsgs(host, mailboxId, folderName, uid,
                    null, null, accessId, ReadMsgs.READ_MSGS_GET_HDRS
                            | ReadMsgs.READ_MSGS_GET_MIME, offset, length,
                    legarUserName, legalPeerIp, null, null, null);
            readMsgs.execute(myDataModel);
            Msg[] msgs = readMsgs.getMessages();
            Map<String, Metadata> metadatas = new LinkedHashMap<String, Metadata>();
            for (Msg msg : msgs) {
                // For each message from RME create a metadata POJO and
                // update the metadata list
                Metadata metadata = new Metadata();
                metadatas.put(msg.getMsgId(), populateMetaData(msg, metadata));
            }
            if (searchTerm != null) {
                try {
                    for (Entry<String, Metadata> entry : metadatas.entrySet()) {
                        if (!searchTerm.match(entry.getValue())) {
                            metadatas.remove(entry.getKey());
                        }
                    }
                } catch (Exception e) {
                    logger.error("Error while reading messages from folder", e);
                    throw new MxOSException(
                            MessageError.INVALID_MESSAGE_SEARCH_QUERY.name(), e);
                }
            }
            // update the search metadata list
            searchMetadatas.put("0", metadatas);

        } catch (final NumberFormatException e) {
            logger.error("Invalid numbers provided as parameters", e);
            throw new MxOSException(ErrorCode.GEN_INVALID_DATA.name(),
                    "sliceCount, sliceStart, sliceEnd param must be number for uid search");
        } catch (final InvalidRequestException e) {
            logger.error(e.getMessage());
            throw new MxOSException(ErrorCode.GEN_INVALID_DATA.name(),
                    e.getMessage());
        } catch (IntermailException e) {
            logger.error("Error while reading messages from folder", e);
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), t);
        }
    }

    @Override
    public void updateMessageBody(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("mss updateMessage method : starting");
        }
        int[] uids = null;
        List<String> uid = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.uid.name())) {
            uid = mxosRequestState.getInputParams().get(
                    MessageProperty.uid.name());
			uids = new int[] { Integer.parseInt(uid.get(0)) };
            
        } else {
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(),
                    "message uid is mandatory for CL");
        }
        
        String text = mxosRequestState.getInputParams()
                .get(MessageProperty.message.name()).get(0);
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = String.valueOf(info.getMailboxId());
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }
        final String folderName = mxosRequestState.getInputParams()
                .get(FolderProperty.folderName.name()).get(0);
      
        final String legarUserName = System
                .getProperty(SystemProperty.readMessageLAUsername.name());
        final String legalPeerIp = System
                .getProperty(SystemProperty.readMessageLAPeerIp.name());

        UpdateMessageBlob update = new UpdateMessageBlob(host, mailboxId,
                folderName, uids, accessId,
                UpdateMessageBlob.UPDATE_MSGS_UPDATE_ONLY, legarUserName,
                legalPeerIp, text);
        try {
            update.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("mss updateMessage method : updation is success");
            }

        } catch (IntermailException e) {
            logger.error("Error while update mailbox ", e);
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString().contains("MsUpdateFailwithRollback")) {
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_UPDATE_ROLLBACK_SUCCESS
                                .name(),
                        e.getMessage());
            } else if (e.getFormattedString().contains(
                    "MsUpdateFailwithoutRollback")) {
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_UPDATE_ROLLBACK_FAILED
                                .name(),
                        e.getMessage());
            } else {
                throw new MxOSException(
                        MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
            }
        } catch (Exception e) {
            logger.error("Error while update mailbox.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while update mailbox.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), t);
        }
    }

    @Override
    public void readMailBoxAccessInfo(MxOSRequestState mxOSRequestStatus)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateMailBoxAccessInfo(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }
		
}
