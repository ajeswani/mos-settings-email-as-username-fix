/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.domain;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.DomainType;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create Domain rewrite object.
 *
 * @author mxos-dev
 */
public class SetMailRewriteDomain implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetMailRewriteDomain.class);

    /**
     * Action method to set Domain rewrite.
     *
     * @param model model instance
     * @throws Exception in case of any error
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMailRewriteDomain action start."));
        }
        try {
            final String mailRewriteDomain = requestState.getInputParams()
                    .get(DomainProperty.alternateDomain.name()).get(0);

            final DomainType domainType = DomainType.fromValue(
                    requestState.getInputParams()
                    .get(DomainProperty.type.name()).get(0));

            if (domainType == DomainType.REWRITE) {
                MxOSApp.getInstance().getMailboxHelper()
                        .setAttribute(requestState,
                                DomainProperty.alternateDomain,
                                mailRewriteDomain);
            } else {
                throw new InvalidRequestException(
                        DomainError.DMN_INVALID_REWRITTENDOMAIN.name());
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set mail rewrite domain.", e);
            throw new ApplicationException(
                    DomainError.DMN_INVALID_REWRITTENDOMAIN.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMailRewriteDomain action end."));
        }
    }
}
