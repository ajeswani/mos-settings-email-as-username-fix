package com.opwvmsg.mxos.backend.crud.we.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.error.AddressBookError;

/**
 * Utility class to lookup and get enum by value
 * @author mxos-dev
 *
 * @param <V>
 */
public class ReverseEnumMap<V extends Enum<V> & EnumConverter> {
    
    // lookup the value (not name) of an enum
    public static <E extends Enum<E>> E lookup(Class<E> e, String id)
            throws AddressBookException {
        E result;
        try {
            result = Enum.valueOf(e, id);
        } catch (IllegalArgumentException ile) {
            // shouldn't happen, as ImProvider must be a valid value.
            logger.error("Exception occurred : ", ile);
            throw new AddressBookException(
                    AddressBookError.ABS_PERSONALINFO_INVALID_IM_ADDRESS.name(),
                    ile);
        }
        return result;
    }

    private Map<String, V> map = new HashMap<String, V>();

    private static Logger logger = Logger.getLogger(ReverseEnumMap.class);

    public ReverseEnumMap(Class<V> valueType) {
        for (V v : valueType.getEnumConstants()) {
            map.put(v.convert(), v);
        }
    }

    // get the enum by value
    public V get(String num) {
        return map.get(num);
    }
}
