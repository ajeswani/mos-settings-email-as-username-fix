/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.

 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.cos;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
* Action class to get Cos attributes of ExternalAccounts object.
* 
* @author mxos-dev
* 
*/
public class GetCosExternalAccount implements MxOSBaseAction  {
	private static Logger logger = Logger.getLogger(GetCosExternalAccount.class);

	@Override
	public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetCosExternalAccount action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool =
                MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String cosId = requestState.getInputParams().get(
                    MailboxProperty.cosId.name()).get(0); 
            final ExternalAccounts externalAccount =
                    mailboxCRUD.readCosExternalAccounts(cosId);
            if (externalAccount == null) {
                throw new NotFoundException(CosError.COS_EXTERNAL_ACCOUNT_NOT_FOUND.name(),
                        "Cos ExternalAccount not found");
            }
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.externalAccounts, externalAccount);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get cos external account.", e);
            throw new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                            + "Provisioning CRUD pool:" + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetCosExternalAccount action end."));
        }
    }
}
