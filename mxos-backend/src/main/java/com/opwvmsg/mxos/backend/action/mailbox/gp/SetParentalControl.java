/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.gp;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class SetParentalControl implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetParentalControl.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetParentalControl action start."));
        }
        final String parentalControl = requestState.getInputParams()
                .get(MailboxProperty.parentalControlEnabled.name()).get(0);

        if (parentalControl != null) {
            if (parentalControl.equalsIgnoreCase(MxosEnums.BooleanType.YES
                    .name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.parentalControlEnabled,
                                Integer.toString(MxosEnums.BooleanType.YES
                                        .ordinal()));
            } else if (parentalControl
                    .equalsIgnoreCase(MxosEnums.BooleanType.NO.name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.parentalControlEnabled,
                                Integer.toString(MxosEnums.BooleanType.NO
                                        .ordinal()));
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PARENTALCONTROL.name());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetParentalControl action end."));
        }
    }
}
