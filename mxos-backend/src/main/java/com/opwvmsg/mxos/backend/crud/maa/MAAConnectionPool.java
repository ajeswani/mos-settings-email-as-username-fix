/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaConnectionPool.java#2 $
 */

package com.opwvmsg.mxos.backend.crud.maa;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.http.client.HttpClient;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

/**
 * Implementation of MAA connection pool.
 * 
 * @author mxos-dev
 */
public class MAAConnectionPool implements ICRUDPool<HttpClient> {
    protected static Logger logger = Logger.getLogger(MAAConnectionPool.class);

    protected GenericObjectPool<HttpClient> objPool;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public MAAConnectionPool() throws MxOSException {
        loadPool();
    }
    /**
     * Method to load Pool.
     */
    private void loadPool() {
        String maxSize = System.getProperty(SystemProperty.maaPoolMaxSize
                .name());
        String initSize = System.getProperty(SystemProperty.maaPoolInitSize
                .name());

        if (maxSize == null || maxSize.equals("")) {
            maxSize = "10"; // Default pool size
        }
        objPool = new GenericObjectPool<HttpClient>(
                new MAAConnectionFactory(), Integer.parseInt(maxSize));

        initializeJMXStats(Long.parseLong(maxSize));
        objPool.setMaxIdle(-1);
    }

    @Override
    public HttpClient borrowObject() throws MxOSException {
        try {
            HttpClient obj = objPool.borrowObject();
            incrementJMXStats();
            return obj;
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    @Override
    public void returnObject(HttpClient client)
            throws MxOSException {
        try {
            objPool.returnObject((HttpClient) client);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while return object object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.MAA.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_MAA.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_MAA.decrement();
    }
    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub
        
    }
}
