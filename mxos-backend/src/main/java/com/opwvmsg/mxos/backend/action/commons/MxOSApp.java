/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpClient;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.IAddressBookHelper;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.IBlobHelper;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaHelper;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.ITasksHelper;
import com.opwvmsg.mxos.backend.crud.http.GenericHttpClient;
import com.opwvmsg.mxos.backend.crud.ldap.LdapCosCache;
import com.opwvmsg.mxos.backend.external.ldap.ExternalLdapObject;
import com.opwvmsg.mxos.backend.external.ldap.ExternalLdapConnectionPool;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.service.BackendMxOSContext;
import com.opwvmsg.mxos.backend.validator.MxosRules;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.BlobDBTypes;
import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.data.enums.DataStoreType;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.enums.TasksDBTypes;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.jmx.setup.RegisterMBeans;
import com.opwvmsg.mxos.utils.config.ConfigService;
import com.opwvmsg.mxos.utils.config.ConfigServiceImpl;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.datastore.IClusterIdGenerator;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMap;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;
import com.opwvmsg.mxos.utils.datastore.IDataStoreProvider;
import com.opwvmsg.mxos.utils.misc.CompressionType;
import com.opwvmsg.mxos.utils.misc.Stats;
import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchema;
import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaUtil;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.mail.GroupHeartBeatListener;
import com.opwvmsg.utils.paf.intermail.mail.MssHostsTableHandler;
import com.opwvmsg.utils.paf.util.password.intermail.IntermailPassword;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * mailbox related activities e.g create mailbox, delete mailbox, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class MxOSApp {
    public static final String MXOS_APP = "MXOS-2.0";

    private static class MxOSAppHolder {
        public static MxOSApp instance = new MxOSApp();
    }

    private static Logger logger;

    public static MxOSApp getInstance() {
        return MxOSAppHolder.instance;
    }

    public static void stopInstance() {
        try {
            String isJMXEnabled = System.getProperty(SystemProperty.jmxEnabled
                    .name());


            boolean enabled = Boolean.valueOf(isJMXEnabled);
            if (enabled) {
                RegisterMBeans.stopMonitoring();
            }
            /* shut down IMDB if created before stopping MxOS */
            if (MxOSAppHolder.instance.dataStoreProvider != null) {
                MxOSAppHolder.instance.dataStoreProvider.shutdownDataStore();
            }
        } catch (InstanceNotFoundException e) {
            throw new RuntimeException(
                    "Exception while stopping monitoring services", e);
        } catch (MalformedObjectNameException e) {
            throw new RuntimeException(
                    "Exception while stopping monitoring services", e);
        } catch (MBeanRegistrationException e) {
            throw new RuntimeException(
                    "Exception while stopping monitoring services", e);
        }
    }


    private IMxOSContext context = null;
    private ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
    private ICRUDPool<ExternalLdapObject> externalLdapPool = null;
    private ICRUDPool<IMailboxCRUD> mailboxSearchCRUDPool = null;
    private ICRUDPool<IMetaCRUD> metaCRUDPool = null;
    private ICRUDPool<IMetaCRUD> metaSlCRUDPool = null;
    private Map<CRUDProtocol, ICRUDPool<IMetaCRUD>> metaCRUDPoolMap;
    private ICRUDPool<HttpClient> maaConnectionPool = null;
    private IBlobCRUD blobCRUD = null;
    private IBlobCRUD blobSlCRUD = null;
    private ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
    private ICRUDPool<ITasksCRUD> tasksCRUDPool = null;
    private ICRUDPool<IRMIMailboxCRUD> rmiMailboxCRUDPool = null;
    private ICRUDPool<ISettingsCRUD> settingsMySQLCRUDPool = null;
    private ICRUDPool<ISettingsCRUD> settingsHttpCRUDPool = null;
    private ICRUDPool<GenericHttpClient> notifyConnectionPool = null;

    private Map<String, JSONSchema> jsonAttributes;
    private MailboxDBTypes mailboxDBType;
    private MetaDBTypes metaDBType;
    private BlobDBTypes blobDBType;
    private AddressBookDBTypes addressBookDBType;
    private TasksDBTypes tasksDBType;

    IntermailPassword passwordProvider = null;
    private IMailboxHelper mailboxHelper = null;
    private IAddressBookHelper addressBookHelper = null;
    private ITasksHelper tasksHelper = null;
    private IMetaHelper metaHelper = null;

    private IBlobHelper blobHelper = null;
    private MxosRules mxosRules = null;
    private MxosErrorMapping mxosErrorMapping = null;

    private IDataStoreProvider<?, ?> dataStoreProvider = null;

    private static String applicationName;

    private boolean enforceValidation = true;

    private static ActionFactory actionFactory = null;

    private boolean softDelete = true;
    private int messageType = 0;
    private int messagePriority = 0;
    private int messageDeliverNDR = 0;
    private CompressionType compressionType;

    private String[] defaultSystemFolders = null;
    private int searchMaxRows = 0;

    private int searchMaxTimeOut = 0;
    private LdapCosCache ldapCosCache = null;

    private MxOSApp() {
        try {
            init();
            logger.info("MxOS System Folders loaded...");
        } catch (Exception e) {
            logger.error("ERROR while loading MxOS", e);
            e.printStackTrace();
        }
    }

    public void destroy() {
        // TODO Auto-generated method stub

    }

    public boolean enforceValidation() {
        return enforceValidation;
    }

    public final ActionFactory getActionFactory() {
        return actionFactory;
    }

    public ICRUDPool<IAddressBookCRUD> getAddressBookCRUD() {
        return addressBookCRUDPool;
    }

    public ICRUDPool<ITasksCRUD> getTasksCRUD() {
        return tasksCRUDPool;
    }

    public ICRUDPool<IRMIMailboxCRUD> getRMIMailboxCRUD() {
        return rmiMailboxCRUDPool;
    }

    public ICRUDPool<ISettingsCRUD> getOXSettingsMySQLCRUD() {
        return settingsMySQLCRUDPool;
    }

    public ICRUDPool<ISettingsCRUD> getOXSettingsHttpCRUD() {
        return settingsHttpCRUDPool;
    }

    public AddressBookDBTypes getAddressBookDBType() {
        return addressBookDBType;
    }

    public TasksDBTypes getTasksDBType() {
        return tasksDBType;
    }

    /**
     * Get the Name of the Application.
     * 
     * @return name name
     * @throws MxOSException exception
     */
    public String getApplicationName() {
        return applicationName;
    }

    public IBlobCRUD getBlobCRUD(CRUDProtocol protocol) {
        return (protocol == CRUDProtocol.httprme) ? blobSlCRUD : blobCRUD;
    }

    public BlobDBTypes getBlobDBType() {
        return blobDBType;
    }

    public IBlobHelper getBlobHelper() {
        return blobHelper;
    }

    public CompressionType getCompressionType() {
        return compressionType;
    }

    @SuppressWarnings("rawtypes")
    public IDataStoreMap getMapDataStore() {
        return dataStoreProvider.getMapDataStore();
    }

    @SuppressWarnings("rawtypes")
    public IDataStoreMultiMap getMultiMapDataStore() {
        return dataStoreProvider.getMultiMapDataStore();
    }

    public IClusterIdGenerator getClusterIdGenerator() {
        return dataStoreProvider.getClusterIdGenerator();
    }

    /**
     * @return the mxOSContext
     */
    public IMxOSContext getContext() {
        return context;
    }

    public long getDefaultMessageExpiry() {
        return System.currentTimeMillis() + MxOSConfig.getMessageExpSecOffset();
    }

    public int getDeliverNDR() {
        return messageDeliverNDR;
    }

    /**
     * Method to get Error Code Object.
     * 
     * @return MxosErrorMapping MxosErrorMapping
     * @throws MxOSException if requested object NOT found in application
     *             context.
     */
    public MxosErrorMapping getErrorMapping() {
        return mxosErrorMapping;
    }

    public ICRUDPool<HttpClient> getMAAConnectionPool() {
        return maaConnectionPool;
    }

    public ICRUDPool<GenericHttpClient> getNotifyConnectionPool() {
        return notifyConnectionPool;
    }

    public int getMessagePriority() {
        return messagePriority;
    }

    public int getMessageType() {
        return messageType;
    }

    public IBackendState getMetaBackendState() {
        return CRUDFactory.getMetaBackendState(metaDBType);
    }

    public ICRUDPool<IMetaCRUD> getMetaCRUD(CRUDProtocol protocol) {
        return (protocol == null) ? metaCRUDPool : metaCRUDPoolMap.get(protocol);
    }

    public MetaDBTypes getMetaDBType() {
        return metaDBType;
    }

    // ICRUDPool<IMetaCRUD> metaCRUDPool = null;
    // IBlobCRUD blobCRUD = null;

    public IMetaHelper getMetaHelper() {
        return metaHelper;
    }

    public IntermailPassword getPasswordProvider() {
        return passwordProvider;
    }

    public IBackendState getMailboxBackendState() {
        return CRUDFactory.getMailboxBackendState(mailboxDBType);
    }

    public IBackendState getAddressBookBackendState() {
        return CRUDFactory.getAddressBookBackendState(addressBookDBType);
    }

    public IBackendState getTasksBackendState() {
        return CRUDFactory.getTasksBackendState(tasksDBType);
    }

    public ICRUDPool<IMailboxCRUD> getMailboxCRUD() {
        return mailboxCRUDPool;
    }


    public ICRUDPool<ExternalLdapObject> getCURLdapPool() {
        return externalLdapPool;
    }

    public MailboxDBTypes getMailboxDBType() {
        return mailboxDBType;
    }

    public IMailboxHelper getMailboxHelper() {
        return mailboxHelper;
    }

    public IAddressBookHelper getAddressBookHelper() {
        return addressBookHelper;
    }

    public ITasksHelper getTasksHelper() {
        return tasksHelper;
    }

    public ICRUDPool<IMailboxCRUD> getMailboxSearchCRUD() {
        return mailboxSearchCRUDPool;
    }

    /**
     * Method to get Rules Object.
     * 
     * @return MxosRules MxosRules
     * @throws MxOSException if requested object NOT found in application
     *             context.
     */
    public MxosRules getRules() {
        return mxosRules;
    }

    public JSONSchema getSchema(String attributeName) {
        return jsonAttributes.get(attributeName);
    }

    public int getSearchMaxRows() {
        return searchMaxRows;
    }

    public int getSearchMaxTimeOut() {
        return searchMaxTimeOut;
    }

    public String[] getSystemFolders() {
        return defaultSystemFolders;
    }

    public void init() throws MxOSException, MalformedObjectNameException,
            InstanceAlreadyExistsException, MBeanRegistrationException,
            NotCompliantMBeanException {
        // load mxos.properties
        loadMxOSProperties();
        // log4j is not yet initialized...
        // logging into console
        System.out.println("mxos.properties loaded...");
        applicationName = System.getProperty(MxOSConstants.DEFAULT_APP);
        // load log4j configuration
        loadLog4jConfig();
        logger.info("Log4j loaded...");
        dumpSystemProperties();
        // JSON attrib validation
        validateJSONAttributes();
        logger.info("JSON attributes validated...");
        // load from config.db
        loadConfig();
        logger.info("Config loaded...");
        // load jmx
        loadJMX();
        String isJMXEnabled = System.getProperty(SystemProperty.jmxEnabled
                .name());
        boolean enabled = Boolean.valueOf(isJMXEnabled);
        if (enabled) {
            logger.info("JMX loaded...");
        } else {
            logger.info("JMX has been disabled...");
        }
        // load MxOS Context
        loadContext();
        logger.info("MxOS Context loaded...");
        // load RME Version - Currently we are not reading it from mxos.properties file
        // loadRmeVersion();
        // load MxOS error codes
        loadMxOSErrorsCodes();
        logger.info("MxOS Errors Codes loaded...");
        // load stats
        loadStats();
        logger.info("MxOS Stats loaded...");
        // return if STUB context
        if (context == null || !(context instanceof BackendMxOSContext)) {
            return;
        }
        // load MxOS rules
        loadMxOSRulesAndActions();
        logger.info("MxOS Rules loaded...");

        // Load JSON schema attributes to avoid re-reading of files everytime
        loadJsonAttributes();
        logger.info("MxOS Json Attributes loaded...");

        loadDataSourceConnectionPools();
        logger.info("MxOS Connection Pools loaded...");

        loadDataStore();
        logger.info("MxOS Data Store loaded...");

        /*
         * TODO: Do reflection here so that tomorrow if we want to change
         * password provider then no need to change the code.
         */
        passwordProvider = new IntermailPassword();

        /*
         * This is for performance optimization.
         */
        enforceValidation = !(MxOSConfig.isTrustedClient());

        actionFactory = ActionFactory.getInstance();

        softDelete = MxOSConfig.isEnableSoftDelete();

        messageType = MxOSConfig.getMessageType();
        messagePriority = MxOSConfig.getMessagePriority();
        messageDeliverNDR = MxOSConfig.getDeliverNDR();
        compressionType = CompressionType.valueOf(MxOSConfig
                .getCompressionType());

        searchMaxRows = MxOSConfig.getSearchMaxRows();
        searchMaxTimeOut = MxOSConfig.getSearchMaxTimeOut();

        defaultSystemFolders = MxOSConfig.getSystemFolders();
        logger.info("MxOS System Folders loaded...");

        initMssHostsTableHandler();
        logger.info("MssHostsTableHandler initialized...");

        initActiveMSSList();
        logger.info("initActiveMSSList initialized...");        
        
        boolean cosCachingEnabled = java.lang.Boolean.parseBoolean(System
                .getProperty(SystemProperty.ldapCosCachingEnabled.name()));
        if (cosCachingEnabled) {
            ldapCosCache = new LdapCosCache();
            logger.info("LDAP Cos Cache initialized...");
        }

    }

    private void initMssHostsTableHandler() {
        MssHostsTableHandler.getInstance();
    }

    private void initActiveMSSList() {
        String[] clusterHashmap = MxOSConfig.getClusterHashMap();
        if (clusterHashmap == null || clusterHashmap.length < 1) {
            logger.info("MSS deployment is stateful");
        } else {
            boolean disableRedirectToSurrogateMSS = MxOSConfig
                    .getDisableRedirectToSurrogateMSS();
            boolean enableActiveList = MxOSConfig.getEnableActiveList();

            if (disableRedirectToSurrogateMSS == false
                    && enableActiveList == true) {
                try {
                    GroupHeartBeatListener groupHeartBeatListener = GroupHeartBeatListener
                            .getInstance();
                    GroupHeartBeatListener.setStatus(true);
                    groupHeartBeatListener.start();
                } catch (Exception e) {
                    logger.error(
                            "An exception occured while initializing activeMSSList ",
                            e);
                }
            }
        }
    }

    public boolean isSoftDelete() {
        return softDelete;
    }

    /**
     * Method to load configurations from config.db (imconfedit).
     * 
     * @throws ConfigException MxOSException
     */
    public void loadConfig() {
        try {
            // create instance of config.
            ConfigService conf = new ConfigServiceImpl();
            conf.getConfig();
        } catch (Exception e) {
            e.printStackTrace();
            boolean loadStubContext = Boolean.parseBoolean(System
                    .getProperty(SystemProperty.loadStubContextOnConfigError
                            .name()));
            if (loadStubContext) {
                final String error = "Error while connecting Config.db server, loading STUB Context";
                System.out.println(error);
                System.out.flush();
                return;
            } else {
                final String error = "Error while connecting Config.db server";
                System.out.println(error);
                System.out.flush();
                System.exit(-1);
            }
        }
    }

    /**
     * Method to load MxOS Context.
     * 
     * @throws ConfigException MxOSException
     */
    public void loadContext() throws MxOSException {
        System.out.println("MxOS Context Loading...");
        Properties p = new Properties();
        String contextId = MxOSConfig.getContextType();
        System.out.println("Context ID : " + contextId);
        p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE, contextId);
        context = MxOSContextFactory.getInstance().getContext(MXOS_APP, p);
    }

    /**
     * Method to getBean.
     * 
     * @param beanName beanName.
     * @return beanObject beanObject
     */
    public void loadDataSourceConnectionPools() throws MxOSException {
        mailboxDBType = MailboxDBTypes.valueOf(MxOSConfig
                .getProvisioningBackend());
        metaDBType = MetaDBTypes.valueOf(MxOSConfig.getMetaBackend());
        blobDBType = BlobDBTypes.valueOf(MxOSConfig.getBlobBackend());
        addressBookDBType = AddressBookDBTypes.valueOf(MxOSConfig
                .getAddressBookBackend());
        tasksDBType = TasksDBTypes.valueOf(MxOSConfig
                .getTasksBackend());
        mailboxCRUDPool = CRUDFactory.getMailboxCRUDPool(mailboxDBType);

        String loadExternalLdapPool = System.getProperty(SystemProperty.loadExternalLdapPool.name());
        boolean enabled = Boolean.valueOf(loadExternalLdapPool);
        if (enabled) {
        	externalLdapPool=new ExternalLdapConnectionPool();
        }
        mailboxSearchCRUDPool = CRUDFactory
                .getMailboxSearchCRUDPool(mailboxDBType);
        
        metaCRUDPool = CRUDFactory.getMetaCRUDPool(metaDBType);
        metaSlCRUDPool = CRUDFactory.getMetaStatelessCRUDPool(metaDBType);
        metaCRUDPoolMap = new HashMap<CRUDProtocol, ICRUDPool<IMetaCRUD>>();
        metaCRUDPoolMap.put(CRUDProtocol.rme, metaCRUDPool);
        metaCRUDPoolMap.put(CRUDProtocol.httprme, metaSlCRUDPool);
        
        maaConnectionPool = CRUDFactory.getMAAConnectionPool();
        addressBookCRUDPool = CRUDFactory
                .getAddressBookCRUDPool(addressBookDBType);
        tasksCRUDPool = CRUDFactory
                .getTasksCRUDPool(tasksDBType);
        rmiMailboxCRUDPool = CRUDFactory
                .getRMIMailboxCRUDPool(addressBookDBType);
        settingsMySQLCRUDPool = CRUDFactory.getSettingsMySQLCRUDPool(addressBookDBType);
        settingsHttpCRUDPool = CRUDFactory.getSettingsHttpCRUDPool(addressBookDBType);
        notifyConnectionPool = CRUDFactory.getNotifyCRUDPool();
        blobCRUD = CRUDFactory.getBlobCRUDPool(MxOSConstants.RME_MX8_VER, blobDBType);
        blobSlCRUD = CRUDFactory.getBlobCRUDPool(MxOSConstants.RME_MX9_VER, blobDBType);
        mailboxHelper = CRUDFactory.getMailboxHelper(mailboxDBType);
        addressBookHelper = CRUDFactory.getAddressBookHelper(addressBookDBType);
        tasksHelper = CRUDFactory.getTasksHelper(tasksDBType);
        metaHelper = CRUDFactory.getMetaHelper(metaDBType);
        blobHelper = CRUDFactory.getBlobHelper(blobDBType);
    }

    /**
     * Loads the Data store
     * 
     * @throws MxOSException
     */
    private void loadDataStore() throws MxOSException {
        final DataStoreType dataStoreType = DataStoreType.getValue(MxOSConfig
                .getNotifyBackend());
        dataStoreProvider = CRUDFactory.getDataStoreProvider(dataStoreType);
    }

    /**
     * Method to load JMX configurations. <i>${MXOS_HOME}/config/log4j.xml</i>.
     * 
     * @throws MxOSException MxOSException
     */
    public void loadJMX() throws MxOSException {
        String isJMXEnabled = System.getProperty(SystemProperty.jmxEnabled
                .name());
        boolean enabled = Boolean.valueOf(isJMXEnabled);
        if (enabled) {
            try {
                RegisterMBeans.startMonitoring();
            } catch (Exception e) {
                throw new RuntimeException("JMX monitoring startup failed", e);
            }
        }
    }

    private void loadJsonAttributes() throws MxOSException {
        jsonAttributes = new HashMap<String, JSONSchema>();
        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        // Get json schema path from System properties.
        String jsonAttributesPath = System.getProperty("jsonschema.path");
        if (jsonAttributesPath == null || jsonAttributesPath.equals("")) {
            jsonAttributesPath = home + "/config/" + getApplicationName()
                    + "/schema/attributes/";
        }
        File folder = new File(jsonAttributesPath);
        File[] listOfFiles = folder.listFiles();
        String attributeName = null;
        try {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    attributeName = listOfFiles[i].getName();
                    logger.info("Loading Json Attribute : " + attributeName);
                    JSONSchema attributeSchema = JSONSchemaUtil.getJSONSchema(
                            getApplicationName(), attributeName);
                    jsonAttributes.put(attributeName, attributeSchema);
                }
            }
        } catch (Exception e) {
            final String msg = "ERROR while loading json attribute : "
                    + attributeName;
            System.out.println(msg);
            logger.error(msg, e);
            throw new RuntimeException(msg);
        }
    }

    /**
     * Method to load log4j configurations.
     * <i>${MXOS_HOME}/config/log4j.xml</i>.
     * 
     * @throws MxOSException MxOSException
     */
    public void loadLog4jConfig() throws MxOSException {
        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }
        String app = getApplicationName();
        if (app == null || app.equals("")) {
            throw new RuntimeException("APP_NAME is not set in web.xml");
        }
        String log4jFile = home + "/config/log4j.xml";
        // init log4j configuration from property file
        DOMConfigurator.configure(log4jFile);
        logger = Logger.getLogger(MxOSApp.class);
    }

    /**
     * Method to load mxos-error-codes.xml.
     * <i>${MXOS_HOME}/config/${app.name}/mxos-error-codes.xml</i>.
     * 
     * @throws MxOSException MxOSException
     */
    public void loadMxOSErrorsCodes() throws MxOSException {
        // Loading of Validation Properties file and creation of errorMap
        // and startup.
        try {
            String app = getApplicationName();
            if (app == null || app.equals("")) {
                throw new RuntimeException("APP_NAME is not set in web.xml");
            }
            mxosErrorMapping = new MxosErrorMapping(app);
        } catch (MxOSException e) {
            throw new RuntimeException("Problem while loading "
                    + "Error code mappings");
        }
    }

    /**
     * Method to load mxos.properties.
     * <i>${MXOS_HOME}/config/${app.name}.properties</i>.
     * 
     * @throws MxOSException MxOSException
     */
    public void loadMxOSProperties() throws MxOSException {

        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }
        System.out.println("MXOS_HOME is " + home);
        Properties config = null;
        InputStream in = null;
        try {
            in = new FileInputStream(home + "/config/mxos.properties");
            config = new Properties();
            try {
                config.load(in);
            } catch (IOException e) {
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        } catch (FileNotFoundException e1) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e1);
        }

        try {
            Iterator<Object> keys = config.keySet().iterator();
            while (keys.hasNext()) {
                final String key = (String) keys.next();
                final String value = config.getProperty(key).trim();
                System.setProperty(key.trim(), value);
            }
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**

     * Method to dump System Properites. This would be useful for debug, in case
     * of any system startup problems
     */
    protected void dumpSystemProperties() {
        try {
            logger.info("Dump of System.Properties: ");
            Properties ps = System.getProperties();
            Enumeration<?> e = ps.propertyNames();
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                logger.info(key + " --- " + ps.getProperty(key));
            }
        } catch (Exception e) {
            logger.warn("Could not able to dump System Properties.", e);
        }
    }

    /**
     * 
     * This method validated the MailboxReturnLdapAttributes and exit the system
     * if illegal attributes are mentioned in the mxos.properties file for
     * getMailboxReturnLdapAttributes.
     * 
     * @param value
     */
    private void validateJSONAttributes() {
        String value = System.getProperty("getMailboxReturnLdapAttributes");
        if (null != value && value.indexOf(MxOSConstants.COMMA) != -1) {
            String[] attribList = value.split(MxOSConstants.COMMA);
            logger.info("JSON attribute validation started...");
            for (String jsonAttrib : attribList) {
                String failsString = jsonAttrib;
                try {
                    MailboxProperty.valueOf(jsonAttrib);
                } catch (IllegalArgumentException e) {
                    logger.error(e.getLocalizedMessage());
                    logger.info("Attribute "
                            + failsString
                            + " is not avaialble in the list of JSON attributes.");
                    if (failsString.endsWith("s")) {
                        logger.info("If multi valued attribute, "
                                + "the specific attribute is valid without the "
                                + "character 's' at the end. ");
                    }
                    logger.info("Assuming the attribute " + failsString
                            + " as a custom attribute.");
                }
            }
        }
    }

    /**
     * Method to load mxos-rules.xml.
     * <i>${MXOS_HOME}/config/${app.name}/mxos-rules.xml</i>.
     * 
     * @throws MxOSException MxOSException
     */
    public void loadMxOSRulesAndActions() throws MxOSException {
        // Loading of Validation Properties file and creation of rulesMap
        // and startup.
        try {
            String app = getApplicationName();
            if (app == null || app.equals("")) {
                throw new RuntimeException("APP_NAME is not set in web.xml");
            }
            mxosRules = new MxosRules(app);
        } catch (MxOSException e) {
            throw new RuntimeException("Problem while loading "
                    + "Validation Properties and creation of ruleMap");
        }
    }

    /**
     * Method to load Stats.
     * 
     * @throws MxOSException MxOSException
     */
    public void loadStats() throws MxOSException {
        // start stats thread
        // Generate stats log for every 100 requests OR 10 mins, whichever
        // occurs first.
        boolean isStatsEnabled = MxOSConfig.isStatsEnabled();
        if (isStatsEnabled) {
            int timeDuration = MxOSConfig.getStatsTimeDuration();
            Stats.init(timeDuration);
        }
    }

    /**
     * @return the ldapCosCache
     */
    public LdapCosCache getLdapCosCache() {
        return ldapCosCache;
    }
}